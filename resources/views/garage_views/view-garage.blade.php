@extends('layouts.app')
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@section('template_title')
    My Garage
@endsection
@section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="/css/listing.css">
    <link rel="stylesheet" type="text/css" href="/css/garage.css">
@endsection
@section('content')
    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">My Garage</h1>
                <div style="text-align:center; color:#fff;"><a href="/" style="color:#fff;">Home</a> / <a href="/my-account" style="color:#fff;">My Dashboard</a> / Garage</div>
            </div>
        </div>
    </div>
    <section>
        <div class="container">
            @if(app('request')->input('m'))
                <div class="alert alert-warning">
                    <div class="col-lg-12 text-center">
                        The vehicle has been removed from your garage.
                    </div>
                </div>
            @endif
            @if(app('request')->input('c'))
                @if(app('request')->input('c') == 1)
                    <div class="alert alert-warning">
                        <div class="col-lg-12 text-center">
                            You can only have a maximum of 3 vehicle in your garage. Please delete one to be able to add more.
                        </div>
                    </div>
                @elseif(app('request')->input('c') == 2)
                    <div class="alert alert-warning">
                        <div class="col-lg-12 text-center">
                            You already have this car in your garage. You can view the details below.
                        </div>
                    </div>
                @endif

            @endif
            @if(app('request')->input('notifications') == 1)
                <div class="alert alert-success" role="alert">
                    <div class="col-lg-12 text-center">
                        Your preferences have been saved
                    </div>
                </div>
            @endif
        </div>
    </section>


    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center mb-5">

                    <p><strong>Welcome to your Auto Lease Compare garage! </strong>Here you can:</p>
                    <p>
                        <i class="fa fa-check"></i> Save and compare your favourite lease deals all in one place.<br>
                        <i class="fa fa-check"></i> Compare technical specifications and historical price changes.<br>
{{--                        <i class="fa fa-check"></i> We will send you a notification when your saved deal drops in price!<br>--}}
                    </p>
                    <p>Hot lease deals are often on a limited stock so can sell out super quickly. <strong>Be quick to enquire, as they can be gone by tomorrow!</strong></p>


                    @if(isset($garageContent) && !is_null($garageContent) && count($garageContent)>0)
                        @php
                            $url = '';
                            $i = 0;
                            foreach ($garageContent as $model) {
                                if ($i == 0) {
                                    $url .= '?car[]=';
                                }else{
                                    $url .= '&car[]=';
                                }

                                $url .= $model->prices_id . '-';
                                $url .= $model->annual_mileage . '-';
                                $url .= $model->term . '-';
                                $url .= $model->deposit_value . '-';
                                $url .= $model->finance_type . '-';
                                $url .= $model->database;

                                $i++;
                            }
                        @endphp
                        <div class="row">
                            <div class="col-md-6 mt-4 text-md-right">
                                <a class="button b1" data-toggle="modal" data-target="#Historymodal" href="/quote/pricehistory/garage/{{$url}}">
                                    <i class="fa fa-line-chart"></i> View Price History
                                </a>
                            </div>
                            <div class="col-md-6 mt-4 text-md-left">
                                <a class="button b2 garageSpecsButton" href="#tech">
                                    <i class="fa fa-car"></i> Compare Specs
                                </a>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="container garageContainer">


                    <div class="row row-eq-height">
                        @if(isset($garageContent) && !is_null($garageContent) && count($garageContent)>0)
                            @foreach($garageContent as $model)
                                <div class="col-md-4 mb-3">

                                    <div style="background: #fff; height: 100%">
                                        <div class="row result-row  text-center">
                                            <div class="col-md-12">

                                                @if($model->status != 2)
                                                    @if($model->database == 'LCV')
                                                        <a href="/quote/enquiry/{{$model->prices_id}}/lcv" class="rowcars">
                                                            @else
                                                                <a href="/quote/enquiry/{{$model->prices_id}}/cars" class="rowcars">
                                                                    @endif
                                                                    @endif

                                                                    @if($model->status != 2)
                                                                        @if($model->in_stock =='1')<div class="stocktag instock"><p>In Stock</p></div> @else <div class="stocktag"><p>Factory Order</p></div> @endif
                                                                    @else
                                                                        <div class="stocktag expiredTag"><p> EXPIRED </p></div>
                                                                    @endif


                                                                    <img src="{{$model->cap->imageid}}" class="img-fluid">

                                                                    @if($model->status != 2)
                                                                </a>
                                                    @endif
                                            </div>
                                            <div class="col-md-12 pt-2">
                                                @if($model->status != 2)
                                                    @if($model->database=='LCV')
                                                        <a href="/quote/enquiry/{{$model->prices_id}}/lcv" class="rowcars">
                                                            @else
                                                                <a href="/quote/enquiry/{{$model->prices_id}}/cars" class="rowcars">
                                                                    @endif
                                                                    @endif

                                                                    @if(isset($model->cap->maker))
                                                                        <h3>{{$model->cap->maker}} {{$model->cap->range}}<br/><span class="der">{{$model->cap->derivative}}</span></h3>
                                                                    @endif
                                                                    @if(isset($model->cap->fuel))
                                                                        <h6 class="features">{{$model->cap->fuel}} | {{$model->cap->transmission}} | {{$model->cap->bodystyle}} </h6>
                                                                    @endif

                                                                    @if(isset($model->deposit_value))
                                                                        <h6>Profile: {{$model->deposit_value}} + {{$model->term-1}}</h6>
                                                                    @endif
                                                                    @if(isset($model->annual_mileage))
                                                                        <h6>Annual Mileage: {{$model->annual_mileage}}</h6>
                                                                    @endif

                                                                    @if($model->status != 2)
                                                                </a>
                                                                @if($model->database=='LCV')
                                                                    <a href="/quote/enquiry/{{$model->prices_id}}/lcv" class="rowcars">
                                                                        @else
                                                                            <a href="/quote/enquiry/{{$model->prices_id}}/cars" class="rowcars">
                                                                                @endif
                                                                                @endif
                                                                                @if(isset($model->monthly_payment))
                                                                                    <h6><span class="resultprice">£{{$model->monthly_payment}}</span> P/M</h6>
                                                                                @endif
                                                                                @if(isset($model->deposit_months))
                                                                                    <h6>£{{$model->deposit_months + $model->document_fee}} <small>Total Upfront</small></h6>
                                                                                @endif
                                                                                @if(isset($model->average_cost))
                                                                                    <h6 style="margin-bottom: 0px;">£{{$model->average_cost}} <small>Avg Monthly Cost</small></h6>
                                                                                @endif
                                                                                <small> All Prices @if($model->finance_type =='P') Inc @else Ex @endif VAT</small><br/>
                                                                                @if($model->status != 2)
                                                                            </a><br>
                                                                            @if($model->database=='LCV')
                                                                                <a href="/quote/enquiry/{{$model->prices_id}}/lcv" class="rowcars">
                                                                                    @else
                                                                                        <a href="/quote/enquiry/{{$model->prices_id}}/cars" class="rowcars">
                                                                                            @endif
                                                                                            <button id="enquireTodayButton" type="button" class="button b3"><i class="fa fa-envelope"></i>  Enquire Today</button>
                                                                                        </a>
                                                                                        <br><br>
                                                                                        @else
                                                                                            <br>

                                                                                            @php
                                                                                                $url = "?maker[]=" . $model->cap->maker;
                                                                                                $url .= "&range[]=" . $model->cap->range;
                                                                                                $url .= "&trim[]=" . $model->cap->range . "_" . $model->cap->trim;
                                                                                            @endphp

                                                                                            @if($model->database == 'LCV')
                                                                                                @if($model->finance_type == 'B')
                                                                                                    <a href="/business-vans/{{$url}}&finance_type_show=B&finance_type=B&database=LCV" class="rowcars">
                                                                                                        @else
                                                                                                            <a href="/personal-vans/{{$url}}&finance_type_show=P&finance_type=P&database=LCV" class="rowcars">
                                                                                                                @endif

                                                                                                                @else
                                                                                                                    @if($model->finance_type == 'B')
                                                                                                                        <a href="/business/{{$url}}&finance_type_show=B&finance_type=B&database=CARS" class="rowcars">
                                                                                                                            @else
                                                                                                                                <a href="/personal/{{$url}}&finance_type_show=P&finance_type=P&database=CARS" class="rowcars">
                                                                                                                                    @endif
                                                                                                                                    @endif
                                                                                                                                    <button id="enquireTodayButton" type="button" class="button b2"><i class="fa fa-search"></i>  Search for similar</button>
                                                                                                                                </a><br><br>
                                                                                                                            @endif

                                                                                                                            <a href="/my-account/garage/delete/{{$model->prices_id}}/{{$model->database}}" class="addToGarageListing"><i class="fa fa-car"></i> Delete from garage</a>
                                                                                                                            <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($loop->count ==  2)
                                    @if($loop->last)
                                        <div class="col-md-4 bays mb-3">
                                            <div class="bays"><i class="fa fa-car" style="font-size: 48px;"></i><br/>Garage Bay Available<br/> <span style="font-size:18px;">Add some more cars to your garage</span><br/><a href="/personal" class="text-white"><i class="fa fa-plus-circle"></i></a></div>
                                        </div>
                                    @endif
                                @endif
                                @if($loop->count ==  1)
                                    @if($loop->last)
                                        <div class="col-md-4 bays mb-3">
                                            <div class="bays"><i class="fa fa-car" style="font-size: 48px;"></i><br/>Garage Bay Available<br/> <span style="font-size:18px;">Add some more cars to your garage</span><br/><a href="/personal" class="text-white"><i class="fa fa-plus-circle"></i></a></div>
                                        </div>

                                        <div class="col-md-4 bays mb-3">
                                            <div class="bays"><i class="fa fa-car" style="font-size: 48px;"></i><br/>Garage Bay Available<br/> <span style="font-size:18px;">Add some more cars to your garage</span><br/><a href="/personal" class="text-white"><i class="fa fa-plus-circle"></i></a></div>
                                        </div>
                                    @endif
                                @endif

                            @endforeach

                        @else

                            <div class="col-md-4 mb-3">
                                <div class="bays"><i class="fa fa-car" style="font-size: 48px;"></i><br/>Garage Bay Available<br/> <span style="font-size:18px;">Add some more cars to your garage</span><br/><a href="/personal" class="text-white"><i class="fa fa-plus-circle"></i></a></div>
                            </div>
                            <div class="col-md-4 mb-3"><div class="row result-row  text-center"><div class="col-md-12"><div class="bays"><i class="fa fa-car" style="font-size: 48px;"></i><br>Garage Bay Available<br> <span style="font-size: 18px;">Add some more cars to your garage</span><br><a href="/personal" class="text-white"><i class="fa fa-plus-circle"></i></a></div></div></div></div>
                            <div class="col-md-4 mb-3"><div class="row result-row  text-center"><div class="col-md-12"><div class="bays"><i class="fa fa-car" style="font-size: 48px;"></i><br>Garage Bay Available<br> <span style="font-size: 18px;">Add some more cars to your garage</span><br><a href="/personal" class="text-white"><i class="fa fa-plus-circle"></i></a></div></div></div></div>

                        @endif
                    </div>
                </div>
            </div>
{{--            <div class="row">--}}
{{--                <div class="col-sm-12 text-center">--}}
{{--                    <p>--}}
{{--                        Checking this box will enable us to notify you when vehicles in your garage prices drop!--}}
{{--                        <br>--}}
{{--                        <label for="garage_price_notification">--}}
{{--                            Enable Price Drop Notifications--}}
{{--                        </label>--}}
{{--                        @php--}}
{{--                            $user = Auth::user();--}}
{{--                        @endphp--}}
{{--                        <input type="checkbox" id="garage_price_notification" name="garage_price_notification" @if($user->garage_price_notification == 1) checked="checked" @endif>--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>

    </section>
    <!-- History Modal -->
    <div class="modal fade" id="Historymodal" tabindex="-1" role="dialog" aria-labelledby="Historymodal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Compare Price History</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="text-align:center;">
                    <canvas id="forecast" width="400" height="250"></canvas>
                    {{--                    <button id="0" type="button" class="chartbtn buttonslim b2">Daily</button>--}}
                    {{--                    <button id="1" type="button" class="chartbtn buttonslim b2">Weekly</button>--}}
                    {{--                    <button id="2" type="button" class="chartbtn buttonslim b2">Monthly</button>--}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="button b3" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <section id="tech">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-5">
                    @if(isset($garageContent) && !is_null($garageContent) && count($garageContent)>0)
                        <h2 class="mt-3 mb-4" style="text-align:center">Compare Technical Information</h2>
                        <div class="garageTable d-none d-md-block">
                            <div class="row no-gutters garagetable1" style="background: #fff;">
                                <div class="col col-sm-3 nopadding" style="border-right: 2px solid #fc5185;">
                                    <div class="carName">

                                    </div>
                                    <div class='carSpec altRow'><strong>CC</strong></div>
                                    <div class='carSpec'><strong>Power</strong></div>
                                    <div class='carSpec altRow'><strong>Top Speed</strong></div>
                                    <div class='carSpec'><strong>0-62mph</strong></div>
                                    <div class='carSpec altRow'><strong>Urban MPG</strong></div>
                                    <div class='carSpec'><strong>Length</strong></div>
                                    <div class='carSpec altRow'><strong>Width</strong></div>
                                    <div class='carSpec'><strong>Height</strong></div>
                                    <div class='carSpec altRow'><strong>Fuel Capacity</strong></div>
                                    <div class='carSpec'><strong>Insurance Band</strong></div>
                                    <div class='carSpec altRow'><strong>Manufacturer Warranty (Years)</strong></div>
                                    <div class='carSpec '><strong>Manufacturer Warranty (Miles)</strong></div>
                                </div>

                                @foreach($garageContent as $model)
                                    <div class="col col-sm-3 text-center nopadding" style="border-right: 2px solid #fc5185;">
                                        <div class="carName">
                                            {{$model->cap->maker}} {{$model->cap->range}}<br/><span class="der">{{$model->cap->derivative}}</span>
                                        </div>
                                        @foreach($model->techData as $techData)

                                            @if(isset($techData->cc) && $model->cap->fuel != 'Electric')
                                                <div class='carSpec altRow'>{{$techData->cc}} CC</div>
                                            @else
                                                <div class='carSpec altRow'>N/A</div>
                                            @endif

                                            @if(isset($techData->enginepower_bhp))
                                                <div class='carSpec '>{{$techData->enginepower_bhp}} BHP</div>
                                            @else
                                                <div class='carSpec '>N/A</div>
                                            @endif

                                            @if(isset($techData->topspeed))
                                                <div class='carSpec altRow'>{{$techData->topspeed}} MPH</div>
                                            @else
                                                <div class='carSpec altRow'>N/A</div>
                                            @endif


                                            @php if (isset($techData->{'0to62'})) {
                                                echo "<div class='carSpec '>" . $techData->{'0to62'} . " SECS</div>";
                                            }else{
                                                echo "<div class='carSpec '>N/A</div>";
                                            }
                                            @endphp

                                            @if(isset($techData->mpg_urban))
                                                <div class='carSpec altRow'>{{$techData->mpg_urban}} MPG</div>
                                            @else
                                                <div class='carSpec altRow'>N/A</div>
                                            @endif
                                            @if(isset($techData->length))
                                                <div class='carSpec '>{{$techData->length}} mm</div>
                                            @else
                                                <div class='carSpec '>N/A</div>
                                            @endif
                                            @if(isset($techData->width))
                                                <div class='carSpec altRow'>{{$techData->width}} mm</div>
                                            @else
                                                <div class='carSpec altRow'>N/A</div>
                                            @endif

                                            @if(isset($techData->height))
                                                <div class='carSpec '>{{$techData->height}} mm</div>
                                            @else
                                                <div class='carSpec '>N/A</div>
                                            @endif
                                            @if(isset($techData->fueltankcapacity) && $model->cap->fuel != 'Electric')
                                                <div class='carSpec altRow'>{{$techData->fueltankcapacity}} Litres</div>
                                            @else
                                                <div class='carSpec altRow'>N/A</div>
                                            @endif

                                            @if(isset($techData->insurancegroup1))
                                                <div class='carSpec '>{{$techData->insurancegroup1}} </div>
                                            @else
                                                <div class='carSpec '>N/A</div>
                                            @endif
                                            @if(isset($techData->standardmanwarranty_years))
                                                <div class='carSpec altRow'>{{$techData->standardmanwarranty_years}} Years</div>
                                            @else
                                                <div class='carSpec altRow'>N/A</div>
                                            @endif

                                            @if(isset($techData->standardmanwarrstandardmanwarranty_mileageanty_years))
                                                <div class='carSpec '>{{$techData->standardmanwarranty_mileage}} Miles</div>
                                            @else
                                                <div class='carSpec '>N/A</div>
                                            @endif
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="mobileGarageTable d-block d-md-none">
                            <div class="tableContent">
                                <div class="mobileGarageTableHeaderBlock">
                                    <div class='mobileGarageTableHeader'></div>
                                    <div class='mobileGarageTableHeader'><strong>CC</strong></div>
                                    <div class='mobileGarageTableHeader'><strong>Power</strong></div>
                                    <div class="mobileGarageTableHeader"><strong>Top Speed</strong></div>
                                    <div class="mobileGarageTableHeader"><strong>0-62mph</strong></div>
                                    <div class="mobileGarageTableHeader"><strong>Urban MPG</strong></div>
                                    <div class="mobileGarageTableHeader"><strong>Length</strong></div>
                                    <div class="mobileGarageTableHeader"><strong>Width</strong></div>
                                    <div class="mobileGarageTableHeader"><strong>Height</strong></div>
                                    <div class="mobileGarageTableHeader"><strong>Fuel Capacity</strong></div>
                                    <div class="mobileGarageTableHeader"><strong>Insurance Band</strong></div>
                                    <div class="mobileGarageTableHeader"><strong>Manufacturer Warranty (Years)</strong></div>
                                    <div class="mobileGarageTableHeader"><strong>Manufacturer Warranty (Miles)</strong></div>

                                </div>
                                <div class="mobileGarageTableContents">
                                    @foreach($garageContent as $model)

                                        <div class="mobileGarageRow" style="">
                                            <div class="carName mobileGarageContent">
                                                {{$model->cap->maker}} {{$model->cap->range}}<br/><span class="der">{{$model->cap->derivative}}</span>
                                            </div>
                                            @foreach($model->techData as $techData)

                                                @if(isset($techData->cc) && $model->cap->fuel != 'Electric')
                                                    <div class='carSpec altRow mobileGarageContent'>{{$techData->cc}} CC</div>
                                                @else
                                                    <div class='carSpec altRow mobileGarageContent'>N/A</div>
                                                @endif

                                                @if(isset($techData->enginepower_bhp))
                                                    <div class='carSpec mobileGarageContent'>{{$techData->enginepower_bhp}} BHP</div>
                                                @else
                                                    <div class='carSpec mobileGarageContent'>N/A</div>
                                                @endif

                                                @if(isset($techData->topspeed))
                                                    <div class='carSpec altRow mobileGarageContent'>{{$techData->topspeed}} MPH</div>
                                                @else
                                                    <div class='carSpec altRow mobileGarageContent'>N/A</div>
                                                @endif


                                                @php if (isset($techData->{'0to62'})) {
                                                echo "<div class='carSpec mobileGarageContent'>" . $techData->{'0to62'} . " SECS</div>";
                                            }else{
                                                echo "<div class='carSpec mobileGarageContent'>N/A</div>";
                                            }
                                                @endphp

                                                @if(isset($techData->mpg_urban))
                                                    <div class='carSpec altRow mobileGarageContent'>{{$techData->mpg_urban}} MPG</div>
                                                @else
                                                    <div class='carSpec altRow mobileGarageContent'>N/A</div>
                                                @endif
                                                @if(isset($techData->length))
                                                    <div class='carSpec mobileGarageContent'>{{$techData->length}} mm</div>
                                                @else
                                                    <div class='carSpec mobileGarageContent'>N/A</div>
                                                @endif
                                                @if(isset($techData->width))
                                                    <div class='carSpec altRow mobileGarageContent'>{{$techData->width}} mm</div>
                                                @else
                                                    <div class='carSpec altRow mobileGarageContent'>N/A</div>
                                                @endif

                                                @if(isset($techData->height))
                                                    <div class='carSpec mobileGarageContent'>{{$techData->height}} mm</div>
                                                @else
                                                    <div class='carSpec mobileGarageContent'>N/A</div>
                                                @endif
                                                @if(isset($techData->fueltankcapacity) && $model->cap->fuel != 'Electric')
                                                    <div class='carSpec altRow mobileGarageContent'>{{$techData->fueltankcapacity}} Litres</div>
                                                @else
                                                    <div class='carSpec altRow mobileGarageContent'>N/A</div>
                                                @endif

                                                @if(isset($techData->insurancegroup1))
                                                    <div class='carSpec mobileGarageContent'>{{$techData->insurancegroup1}} </div>
                                                @else
                                                    <div class='carSpec mobileGarageContent'>N/A</div>
                                                @endif
                                                @if(isset($techData->standardmanwarranty_years))
                                                    <div class='carSpec altRow mobileGarageContent'>{{$techData->standardmanwarranty_years}} Years</div>
                                                @else
                                                    <div class='carSpec altRow mobileGarageContent'>N/A</div>
                                                @endif

                                                @if(isset($techData->standardmanwarrstandardmanwarranty_mileageanty_years))
                                                    <div class='carSpec mobileGarageContent'>{{$techData->standardmanwarranty_mileage}} Miles</div>
                                                @else
                                                    <div class='carSpec mobileGarageContent'>N/A</div>
                                                @endif
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>


                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>

    </section>

@endsection
@section('footer_scripts')
    <script>
        $(document).ready(function () {

            $("#Historymodal").on("show.bs.modal", function (e) {
                var link = $(e.relatedTarget);
                $(this).find(".modal-body").load(link.attr("href"));
            });
            $('body').on('click', '#garage_price_notification', function () {
                if($(this).is(":checked"))
                {
                    value = 1;
                }else{
                    value = 0;
                }

                $.ajax(
                    {
                        type: "get",
                        data: 'checkbox=' + value,
                        url: '/my-account/garage/switchPriceDropNotifications',
                        success: function (html) {
                            window.location.href="/my-account/garage?notifications=1";
                        }
                    }
                )
            });
        });
    </script>
@endsection
