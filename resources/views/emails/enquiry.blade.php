<?php if ($model->finance_type === 'P') {
    $financeType = 'Personal';
    $monthlyPayment = $model->price . " Per Month Inc VAT";
    $documentFee = $model->document_fee . " Inc VAT";
    $depositMonths = $model->deposit_months . " Inc VAT";
} else {
    $financeType = 'Business';
    $monthlyPayment = $model->price . " Per Month Ex VAT";
    $documentFee = $model->document_fee . " Ex VAT";
    $depositMonths = $model->deposit_months . " Ex VAT";
}
?>
        <!DOCTYPE html>
<html
        xmlns="http://www.w3.org/1999/xhtml"
        xmlns:v="urn:schemas-microsoft-com:vml"
        xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <title></title>
    <!--[if !mso]>
    <!-- -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--
    <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">  #outlook a { padding: 0; }  .ReadMsgBody { width: 100%; }  .ExternalClass { width: 100%; }  .ExternalClass * { line-height:100%; }  body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }  table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }  img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }  p { display: block; margin: 13px 0; }</style>
    <!--[if !mso]>
    <!-->
    <style type="text/css">  @media only screen and (max-width:480px) {    @-ms-viewport { width:320px; }    @viewport { width:320px; }  }</style>
    <!--
    <![endif]-->
    <!--[if mso]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if lte mso 11]>
    <style type="text/css">  .outlook-group-fix {    width:100% !important;  }</style>
    <![endif]-->
    <!--[if !mso]>
    <!-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
    <style type="text/css">        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);    </style>
    <!--
    <![endif]-->
    <style type="text/css">  @media only screen and (min-width:480px) {    .mj-column-per-100 { width:100%!important; }  }</style>
</head>
<body style="background:#ffffff;">
<div class="mj-container" style="background-color:#ffffff;">
    <!--[if mso | IE]>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div style="margin:0px auto;max-width:600px;background:#FFFFFF;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;" align="center" border="0">
            <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align:top;width:600px;">
                    <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tbody>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
                                    <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0">
                                        <tbody>
                                        <tr>
                                            <td style="width:354px;">
                                                <a href="https://www.autoleasecompare.com/" target="_blank">
                                                    <img alt="Auto Lease Compare" height="auto" src="https://www.autoleasecompare.com/images/autoleasecompare.jpg" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="354">
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->
    <!--[if mso | IE]>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div style="margin:0px auto;max-width:600px;background:#FFFFFF;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;" align="center" border="0">
            <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align:top;width:600px;">
                    <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" style="vertical-align:top;" width="100%" border="0">
                            <tbody>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:10px 19px 19px 19px;" align="center">
                                    <div style="cursor:auto;color:#000000;font-family:Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;">
                                        <p>Hi
                                            <?=$model->deal_id; ?>,
                                        </p>
                                        <meta charset="utf-8">
                                        <p dir="ltr">You have received a new customer enquiry;</p>
                                        <table style="width:100%;"></table>
                                        <table style="width:100%;">
                                            <tbody>
                                            <tr>
                                                <th style="border: 1px solid #333;background: #333;color: #fff;"></th>
                                                <td colspan="1" style="border: 1px solid #333;background: #333;color: #fff;">Deal Details</td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Enquiry Id</td>
                                                <td>ALC
                                                    <?=$model->id?>
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Personal / Business</td>
                                                <td>
                                                    <?= $financeType ?>
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Deal Id</td>
                                                <td>
                                                    <?=$model->model_id?>
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Stock Status</td>
                                                <td>
                                                    <?php if($pricing->in_stock == 0) {
                                                        echo "Factory Order";
                                                    }else{
                                                        echo "In Stock";
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Car, Make, Model, Trim</td>
                                                <td>
                                                    <?=$model->make . ' ' . $model->model ?>
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Transmission</td>
                                                <td>
                                                    <?=$model->transmission ?>
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Doors</td>
                                                <td>
                                                    <?=$cap->doors ?>
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Fuel Type</td>
                                                <td>
                                                    <?=$model->fuel_type ?>
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Initial Rental</td>
                                                <td>(£
                                                    <?=$pricing->deposit_months ?>)
                                                    <?=$pricing->deposit_value ?> Months Upfront
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Term (contract length)</td>
                                                <td>
                                                    <?=$pricing->term ?> Months
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Annual Mileage</td>
                                                <td>
                                                    <?=$pricing->annual_mileage ?> Miles P/A
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Monthly Rental</td>
                                                <td>£
                                                    <?=$monthlyPayment ?>
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Admin Fee</td>
                                                <td>£
                                                    <?= $documentFee ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="border: 1px solid #333;background: #333;color: #fff;"></th>
                                                <td colspan="1" style="border: 1px solid #333;background: #333;color: #fff;">Customer Details</td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Customer Name</td>
                                                <td>
                                                    <?=$model->name?>
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Contact Preference</td>
                                                <td>
                                                    <?=$contact_preference?>
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Email</td>
                                                <td>
                                                    <?=$model->email?>
                                                </td>
                                            </tr>
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Phone number</td>
                                                <td>
                                                    <a href="tel:
																															<?=$model->phone_number?>">
                                                        <?=$model->phone_number?>
                                                    </a>
                                                </td>
                                            </tr>
                                            @if($financeType == 'Business')
                                                <tr style="border: 1px solid;">
                                                    <td style="border-right: 1px solid;">Business Name</td>
                                                    <td>
                                                        <?=$model->business_name?>
                                                    </td>
                                                </tr>
                                                <tr style="border: 1px solid;">
                                                    <td style="border-right: 1px solid;">Business Type</td>
                                                    <td>
                                                        <?=$model->business_type?>
                                                    </td>
                                                </tr>
                                            @endif
                                            <tr style="border: 1px solid;">
                                                <td style="border-right: 1px solid;">Customer Message</td>
                                                <td>
                                                    <?=nl2br($model->enquiry)?>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:15px 15px 15px 15px;" align="center">
                                    <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;">
                                        <p>
                                            <meta charset="utf-8">
                                        </p>
                                        <p dir="ltr">Please contact this customer as soon as possible (ideally within 24 hours) to increase your chances of a conversion. Once you have sent a quote to the customer, please update the status in your dashboard to &#x2018;Quote Sent&#x2019;.</p>
                                        <p>If
                                            <?=$model->name?> purchases a lease vehicle with yourselves, please remember to update the status of this enquiry to &#x201C;Sold&#x201D; within your dashboard on our website (within 24 hours).
                                        </p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
                                    <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:separate;" align="center" border="0">
                                        <tbody>
                                        <tr>
                                            <td style="border:0px solid #000;border-radius:9px;color:#fff;cursor:auto;padding:9px 26px;" align="center" valign="middle" bgcolor="#fc5185">
                                                <a href="https://www.autoleasecompare.com/?login=1" style="text-decoration:none;color:#fff;font-family:Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Login To Dashboard</a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div style="margin:0px auto;max-width:600px;background:#FFFFFF;">
                        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;" align="center" border="0">
                            <tbody>
                            <tr>
                                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:2px 0px 2px 0px;">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="vertical-align:top;width:600px;">
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:center;width:100%;">
                                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="word-wrap:break-word;font-size:0px;padding:0px 15px 0px 15px;" align="center">
                                                    <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;">
                                                        <p>Have a great day,
                                                            <br>
                                                            <strong>Auto Lease Compare Team</strong>
                                                            <br>
                                                            <span style="color:#fc5185;">
																																							<strong>www.autoleasecompare.com</strong>
																																						</span>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="word-wrap:break-word;font-size:0px;padding:0px 15px 0px 15px;" align="center">
                                                    <div>
                                                        <!--[if mso | IE]>
                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center">
                                                            <tr>
                                                        <td>
                                                        <![endif]-->
                                                        <table role="presentation" cellpadding="0" cellspacing="0" style="float:none;display:inline-table;" align="left" border="0">
                                                            <tbody>
                                                            <tr>
                                                                <td style="padding:4px;vertical-align:middle;">
                                                                    <table role="presentation" cellpadding="0" cellspacing="0" style="background:none;border-radius:3px;width:30px;" border="0">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="font-size:0px;vertical-align:middle;width:30px;height:30px;">
                                                                                <a href="https://www.instagram.com/auto_lease_compare/">
                                                                                    <img alt="instagram" height="30" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/instagram.png" style="display:block;border-radius:3px;" width="30">
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso | IE]>
                                                        </td>
                                                        <td>
                                                        <![endif]-->
                                                        <table role="presentation" cellpadding="0" cellspacing="0" style="float:none;display:inline-table;" align="left" border="0">
                                                            <tbody>
                                                            <tr>
                                                                <td style="padding:4px;vertical-align:middle;">
                                                                    <table role="presentation" cellpadding="0" cellspacing="0" style="background:none;border-radius:3px;width:30px;" border="0">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="font-size:0px;vertical-align:middle;width:30px;height:30px;">
                                                                                <a href="https://facebook.com/autoleasecompare">
                                                                                    <img alt="facebook" height="30" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/facebook.png" style="display:block;border-radius:3px;" width="30">
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso | IE]>
                                                        </td>
                                                        <td>
                                                        <![endif]-->
                                                        <table role="presentation" cellpadding="0" cellspacing="0" style="float:none;display:inline-table;" align="left" border="0">
                                                            <tbody>
                                                            <tr>
                                                                <td style="padding:4px;vertical-align:middle;">
                                                                    <table role="presentation" cellpadding="0" cellspacing="0" style="background:none;border-radius:3px;width:30px;" border="0">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="font-size:0px;vertical-align:middle;width:30px;height:30px;">
                                                                                <a href="https://www.linkedin.com/company/auto-lease-compare">
                                                                                    <img alt="linkedin" height="30" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/linkedin.png" style="display:block;border-radius:3px;" width="30">
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso | IE]>
                                                        </td>
                                                        <td>
                                                        <![endif]-->
                                                        <table role="presentation" cellpadding="0" cellspacing="0" style="float:none;display:inline-table;" align="left" border="0">
                                                            <tbody>
                                                            <tr>
                                                                <td style="padding:4px;vertical-align:middle;">
                                                                    <table role="presentation" cellpadding="0" cellspacing="0" style="background:none;border-radius:3px;width:30px;" border="0">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="font-size:0px;vertical-align:middle;width:30px;height:30px;">
                                                                                <a href="https://twitter.com/compare_lease">
                                                                                    <img alt="twitter" height="30" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/twitter.png" style="display:block;border-radius:3px;" width="30">
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso | IE]>
                                                        </td>
                                                        </tr>
                                                        </table>
                                                        <![endif]-->
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="word-wrap:break-word;font-size:0px;padding:0px 15px 0px 15px;" align="left">
                                                    <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;">
                                                        <p align="center">You have received this email because you have registered with autoleasecompare.com</p>
                                                        <p align="center">Auto Lease Compare Ltd - Registered in England and Wales company number:
                                                            11519767 | Auto Lease Compare Ltd is authorised and regulated by the Financial
                                                            Conduct Authority: 822040 | Data Protection Registration number: ZA469390 | VAT
                                                            Number: 307521233| We are a credit broker not a lender and may introduce you to
                                                            businesses who also act as credit brokers or a number of companies offering
                                                            consumer hire or consumer hire-purchase.</p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
    </div>
</body>
</html>