<?php if ($model->finance_type === "P") {
    $monthlyPayment = $model->price . " Per Month Inc VAT";
} else {
    $monthlyPayment = $model->price . " Per Month Ex VAT";
}
?>
        <!DOCTYPE html>
<html
        xmlns="http://www.w3.org/1999/xhtml"
        xmlns:v="urn:schemas-microsoft-com:vml"
        xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <title></title>
    <!--[if !mso]>
    <!-- -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--
       <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="date=no">
    <meta name="format-detection" content="address=no">
    <meta name="supported-color-schemes" content="light">
    <meta name="color-scheme" content="light only">
    <style type="text/css">
        #outlook a {
            padding: 0;
        }
        .ReadMsgBody {
            width: 100%;
        }
        .ExternalClass {
            width: 100%;
        }
        .ExternalClass * {
            line-height: 100%;
        }
        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            background: #fff!important;
            background-image: linear-gradient(#ffffff,#ffffff) !important;
        }
        table, td {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }
        p {
            display: block;
            margin: 13px 0;
        }
        @media (prefers-color-scheme: dark) {
            .darkmode { background-color: #1e1e1e !important; }
            .darkmode p { color: #ffffff !important; }
        }
    </style>
    <!--[if !mso]>
    <!-->
    <style type="text/css">  @media only screen and (max-width: 480px) {
            @-ms-viewport {
                width: 320px;
            }    @viewport {
                width: 320px;
            }
        }</style>
    <!--
    <![endif]-->
    <!--[if mso]>
    <xml>
    <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if lte mso 11]>
    <style type="text/css">  .outlook-group-fix {
        width: 100% !important;
    }
    </style>
    <![endif]-->
    <!--[if !mso]>
    <!-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
    <style type="text/css">        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);    </style>
    <!--
    <![endif]-->
    <style type="text/css">  @media only screen and (min-width: 480px) {
            .mj-column-per-100 {
                width: 100% !important;
            }
        }
    </style>
</head>
<body style="background:#ffffff;">
<div class="mj-container darkmode" style="background-color:#ffffff;">
    <!--[if mso | IE]>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
    <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div style="margin:0px auto;max-width:600px;background:#FFFFFF;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;" align="center" border="0">
            <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="vertical-align:top;width:600px;">
                    <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tbody>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center">
                                    <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0">
                                        <tbody>
                                        <tr>
                                            <td style="width:354px;">
                                                <a href="https://www.autoleasecompare.com/" target="_blank">
                                                    <img alt="Auto Lease Compare" height="auto" src="https://www.autoleasecompare.com/images/autoleasecompare.jpg" style="border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:100%;height:auto;" width="354">
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->
    <!--[if mso | IE]>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
    <tr>
        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div style="margin:0px auto;max-width:600px;background:#FFFFFF;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;" align="center" border="0">
            <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="vertical-align:top;width:600px;">
                    <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" style="vertical-align:top;" width="100%" border="0">
                            <tbody>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:0px 15px 0px 15px;" align="center">
                                    <div style="cursor:auto;color:#000000;font-family:Arial, sans-serif;font-size:11px;line-height:1.5;text-align:center;">
                                        <p>
      <span style="font-size:12px;">Hi
      <?=$model->name?>,
      </span>
                                        </p>
                                        <p>
                                            <span style="font-size:12px;">Thank you for using Auto Lease Compare to find your next lease vehicle, you are one step closer to driving your new car!</span>
                                        </p>
                                        <p><strong>
                                                <?= $model->make . " " . $model->model;?>
                                            </strong></p>
                                        <a href="https://www.autoleasecompare.com/?login=1" style="text-decoration:none;background:#fc5185;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">
                                            <img src="{{$image}}" alt="{{$model->maker}} {{$model->range}} {{$model->derivative}}" style="width: 100%; max-width: 400px;">
                                        </a>
                                        <p>
                                          <span style="font-weight: bold; font-size:12px;">Your enquiry with an Initial Rental of  <?php echo $model->profile . ' at £' . $monthlyPayment; ?>
                                          is now on its way to
                                          <span style="color: #fc5185;">
                                          <?=$model->deal_id ?>
                                          </span>
                                          and one of their team members will be in touch with you as soon possible.
                                          </span>
                                        </p>
                                        <p>
                                      <span style="font-size:12px;">
                                      You can view and track your enquiries within your <a href="https://www.autoleasecompare.com/?login=1" target="_blank">ALC dashboard.</a>
                                      </span>
                                        </p>
                                        <span style="font-size:12px;">
                                            You can call <b style="color: #fc5185;">{{$dealer->name }}</b>  on <b style="color: #fc5185;">{{$dealer->phone}}</b> to speak with them directly. Their opening hours are <b style="color: #fc5185;">{{ $dealer->opening_hours }}</b>.
                                       </span>
                                        <p>
                                        <span style="font-size:12px;">Your Auto Lease Compare Enquiry ID is: ALC-{{$quote->id}}</span>
                                        </p>
                                        <br/>
                                        <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:separate;" align="center" border="0">
                                            <tbody>
                                            <tr>
                                                <td align="center">
                                                    <?php
                                                    if($quote->created_by) {
                                                    ?>
                                                    <a href="https://www.autoleasecompare.com/?login=1" style="border-radius: 9px; padding: 9px 26px;text-decoration:none;background:#fc5185;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Login To Dashboard</a>
                                                    <?php
                                                    }else{
                                                    ?>
                                                    <a href="https://www.autoleasecompare.com/?register=1" style="border-radius: 9px; padding: 9px 26px;text-decoration:none;background:#fc5185;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%;text-transform:none;margin:0px;" target="_blank">Register Today</a>
                                                    <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <br>
                                        <span style="font-size:12px;"> If you have not heard from the leasing provider within 48 hours, or you require any assistance at all – please reach out to us at <u><a style="color: #fc5185;" href = "mailto:info@autoleasecompare.com"><b>info@autoleasecompare.com</b></a></u> and we will be happy to assist you. </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="word-wrap:break-word;font-size:0px;padding:0px 0px 0px 0px;" align="center"></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <br>
                                    <a href="https://www.autoleasecompare.com/sell-your-car">
                                        <img src="https://www.autoleasecompare.com/images/sellyourcarhorizontal.jpg" width="600">
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div style="margin:0px auto;max-width:600px;background:#FFFFFF;">
                        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FFFFFF;" align="center" border="0">
                            <tbody>
                            <tr>
                                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:2px 0px 2px 0px;">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="vertical-align:top;width:600px;">
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tbody>
                                            <tr>
                                                <td style="word-wrap:break-word;font-size:0px;padding:0px 15px 0px 15px;" align="center">
                                                    <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;">
                                                        <p>Have a great day,
                                                            <br>
                                                            <strong>Auto Lease Compare Team</strong>
                                                            <br>
                                                            <span style="color:#fc5185;">
      <strong>www.autoleasecompare.com</strong>
      </span>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="word-wrap:break-word;font-size:0px;padding:0px 15px 0px 15px;" align="center">
                                                    <div>
                                                        <!--[if mso | IE]>
                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="undefined">
                                                        <tr>
                                                        <td>
                                                        <![endif]-->
                                                        <table role="presentation" cellpadding="0" cellspacing="0" style="float:none;display:inline-table;" align="left" border="0">
                                                            <tbody>
                                                            <tr>
                                                                <td style="padding:4px;vertical-align:middle;">
                                                                    <table role="presentation" cellpadding="0" cellspacing="0" style="background:none;border-radius:3px;width:30px;" border="0">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="font-size:0px;vertical-align:middle;width:30px;height:30px;">
                                                                                <a href="https://www.instagram.com/auto_lease_compare/">
                                                                                    <img alt="instagram" height="30" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/instagram.png" style="display:block;border-radius:3px;" width="30">
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso | IE]>
                                                        </td>
                                                        <td>
                                                        <![endif]-->
                                                        <table role="presentation" cellpadding="0" cellspacing="0" style="float:none;display:inline-table;" align="left" border="0">
                                                            <tbody>
                                                            <tr>
                                                                <td style="padding:4px;vertical-align:middle;">
                                                                    <table role="presentation" cellpadding="0" cellspacing="0" style="background:none;border-radius:3px;width:30px;" border="0">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="font-size:0px;vertical-align:middle;width:30px;height:30px;">
                                                                                <a href="https://facebook.com/autoleasecompare">
                                                                                    <img alt="facebook" height="30" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/facebook.png" style="display:block;border-radius:3px;" width="30">
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso | IE]>
                                                        </td>
                                                        <td>
                                                        <![endif]-->
                                                        <table role="presentation" cellpadding="0" cellspacing="0" style="float:none;display:inline-table;" align="left" border="0">
                                                            <tbody>
                                                            <tr>
                                                                <td style="padding:4px;vertical-align:middle;">
                                                                    <table role="presentation" cellpadding="0" cellspacing="0" style="background:none;border-radius:3px;width:30px;" border="0">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="font-size:0px;vertical-align:middle;width:30px;height:30px;">
                                                                                <a href="https://www.linkedin.com/company/auto-lease-compare">
                                                                                    <img alt="linkedin" height="30" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/linkedin.png" style="display:block;border-radius:3px;" width="30">
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso | IE]>
                                                        </td>
                                                        <td>
                                                        <![endif]-->
                                                        <table role="presentation" cellpadding="0" cellspacing="0" style="float:none;display:inline-table;" align="left" border="0">
                                                            <tbody>
                                                            <tr>
                                                                <td style="padding:4px;vertical-align:middle;">
                                                                    <table role="presentation" cellpadding="0" cellspacing="0" style="background:none;border-radius:3px;width:30px;" border="0">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="font-size:0px;vertical-align:middle;width:30px;height:30px;">
                                                                                <a href="https://twitter.com/compare_lease">
                                                                                    <img alt="twitter" height="30" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/outlined/twitter.png" style="display:block;border-radius:3px;" width="30">
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso | IE]>
                                                        </td>
                                                        </tr>
                                                        </table>
                                                        <![endif]-->
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="word-wrap:break-word;font-size:0px;padding:0px 15px 0px 15px;" align="left">
                                                    <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;">
                                                        <?php
                                                        if($quote->created_by) {
                                                        ?><p align="center">You have received this email because you have registered with autoleasecompare.com</p><?php
                                                        }else{
                                                        ?><p align="center">You have received this email because you have submitted an enquiry on autoleasecompare.com</p><?php
                                                        }
                                                        ?>
                                                        <p align="center">This is an automated email. Please do not reply to this message.
                                                            Auto Lease Compare Ltd - Registered in England and Wales company number:
                                                            11519767 | Auto Lease Compare Ltd is authorised and regulated by the Financial
                                                            Conduct Authority: 822040 | Data Protection Registration number: ZA469390 | VAT
                                                            Number: 307521233| We are a credit broker not a lender and may introduce you to
                                                            businesses who also act as credit brokers or a number of companies offering
                                                            consumer hire or consumer hire-purchase.</p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
    </div>
</body>
</html>