@extends('layouts.app')
@section('template_title')
    Privacy
@endsection
@section('content')

    <div class="container-fluid banner">
        <div class="row">
            <div class="col-md-12">
                <h1>Privacy Policy</h1>
            </div>
        </div>
    </div>

    <div class="container listingContainer">
        <div class="row">

                <div class="col-md-12">
<p>
                    We are Auto Lease Compare Ltd (Company Number 11519767, Financial Conduct Authority Number: 822040, Data Protection Registration number ZA469390 and whose registered office is 160 Kemp House, City Road, London, EC1V 2NX [‘we’, ’us’ or ‘our’]). Our customers privacy is of utmost importance - please read this Privacy Policy [‘the Policy’] carefully as it contains important information about how we will use your personal data.
<br>
                    For the purpose of The EU General Data Protection Regulation [‘GDPR’], we are the ‘Controller’ (i.e. the company who is responsible for, and controls the processing of, your personal data). As we act as a credit intermediary, we undertake a number of financial tasks that relate to consumer credit. Our firm’s lawful basis for processing your personal data is done so under a Legitimate Interest - Article 6(1)(f) - “the processing is necessary for your legitimate interests or the legitimate interests of a third party unless there is a good reason to protect the individual's personal data which overrides those legitimate interests.”
<br>
                    This Privacy Policy applies only to your use of our website. Please note that we have no control over how your data is stored, or used by other leasing companies and we advise you to check the privacy policies of any such websites before providing any data to them.
<br>
                    This Policy was last updated January 2019.
</p>
                    <h2>Leasing Company data</h2>
<p>                    This privacy statement is primarily aimed at users of our website. However, if you work for a leasing company with whom we partner, please read the below.
<br>
                    We will collect your business details and your employee details as well as other information relevant to our professional relationship with you, including a photo to display on the website. We will also maintain a record of your communications with us and information around any leads generated via our website.
<br>
                    We may collect information about your employees, from you directly, from user(s) of our website, or from publicly available sources (e.g. your leasing company website).
<br>
                    We will use this information for the purposes of developing and maintaining our partnership with you. If you would like to update or delete information on your profile on our website, please visit your Auto Lease Compare Dashboard or feel free to contact us at <a href="mailto:info@autoleasecompare.com">info@autoleasecompare.com</a>
</p>
                    <h2>Personal data we may collect</h2>
                    <p> We collect personal information from you when you provide it to us directly and through your use of our website. This information may include:</p>
                    <ul>
                        <li style="font-weight: 400;">Information you provide to us when you create an account with us, which will include your name, email address and password;</li>
                        <li style="font-weight: 400;">Information you provide when using our website, for example your telephone number, searches, reviews and comments, messages sent to the leasing companies via our enquiry form;</li>
                        <li style="font-weight: 400;">You are able to update your personal information at any time by visiting your Auto Lease Compare Dashboard. </li>
                        <li style="font-weight: 400;">Any correspondence you have with leasing companies via our website;</li>
                        <li style="font-weight: 400;">Records of your interactions with Auto Lease Compare Ltd (e.g. if you contact our support team). We maintain a record of enquiries which we then forward to the relevant leasing company on behalf of the customer, at their own discretion;</li>
                        <li style="font-weight: 400;">Information you provide to us when you enter a competition or participate in a survey; </li>
                        <li style="font-weight: 400;">Information collected automatically, using cookies and other tracking technologies (e.g. which pages you viewed and whether you clicked on a link in one of our email updates). We may also collect information about the device you use to access our website.</li>
                    </ul>
                    <p>We may also receive confirmation from one of our leasing partners if you lease a car from them, for our accounting purposes and to manage the leasing company’s account with us.</p>
                    <h2>How we use personal data</h2>
                    <p>Depending on how you use our website, your interactions with us, and the permissions you give us, the purposes for which we use your personal information include:</p>
                    <ul>
                        <li style="font-weight: 400;">To fulfill requests submitted via the website and maintain your online account;</li>
                        <li style="font-weight: 400;">To contact you about your use of the website and send you hot offer - supplying you with information by email that you have opted-in to (you may unsubscribe or opt-out at any time by using the unsubscribe link within all emails sent). </li>
                        <li style="font-weight: 400;">Where you contact a leasing company via our website, Auto Lease Compare may telephone or email you to confirm whether you leased a car from that leasing company;</li>
                        <li style="font-weight: 400;">To manage and respond to any queries or complaints to our customer service team;</li>
                        <li style="font-weight: 400;">To fulfil our obligations to the leasing companies we work with;</li>
                        <li style="font-weight: 400;">To improve and maintain the website, and monitor its usage;</li>
                        <li style="font-weight: 400;">For market research, e.g. we may contact you for feedback about your experience on our website;</li>
                        <li style="font-weight: 400;">To send you marketing messages, where we have your consent or are otherwise permitted to do so;</li>
                        <li style="font-weight: 400;">For security purposes, to investigate fraud and where necessary to protect ourselves and third parties;</li>
                        <li style="font-weight: 400;">To personalise the website to you and show you content we think you will be most interested in, based on your account information and your history on the website; and</li>
                        <li style="font-weight: 400;">To comply with our legal and regulatory obligations.</li>
                    </ul>
                    <h2>Disclosure of personal data</h2>
                    <p>We share users’ personal information with third parties in the following circumstances:</p>
                    <ul>
                        <li style="font-weight: 400;">With leasing companies, to pass on your interest or specific query - we will only pass on the information you have provided when you send an enquiry to the leasing company for the purpose of them providing you with a personalised quote. </li>
                        <li style="font-weight: 400;">With our suppliers and service providers, for example we use a service provider to contact you via telephone to confirm whether you leased a car via our website;</li>
                        <li style="font-weight: 400;">With our professional and legal advisors;</li>
                        <li style="font-weight: 400;">With third parties engaged in fraud prevention and detection;</li>
                        <li style="font-weight: 400;">With law enforcement or other governmental authorities, in response to a lawful request or court order;</li>
                        <li style="font-weight: 400;">With any regulators within whose jurisdiction we operate within;</li>
                        <li style="font-weight: 400;">In the event that we are involved in a merger or sale of any business assets, the personal information of our users may be disclosed to a potential buyer. In this event, we will make reasonable attempts to ensure the buyer will be bound by the terms of this Privacy Policy; and</li>
                        <li style="font-weight: 400;">Otherwise where we have your consent or are otherwise legally permitted to do so.</li>
                    </ul>
                    <p>Please be aware that we will not sell or otherwise disclose any personal data provided to us to any other third party.</p>
                    <h2>Marketing and opting-in</h2>
                    <p>If you have opted-in to receive our marketing material, we may use your personal information to send you marketing by email.
<br>
                    The nature of these marketing communications relate to information on products, services, promotions and special offers which we believe may be of interest to you or others. If you or others would prefer not receive any further direct marketing communications form us, it is possible to opt out at any time by clicking the unsubscribe button on the email communications, alternatively please email <a href="mailto:info@autoleasecompare.com">info@autoleasecompare.com</a>
<br>
                    You may also see adverts for our website on third party websites, including on social media. Where you see an ad on social media, this may be because we have engaged the social network to show ads to our users, or others who match the demographic profile of our users.
                    </p>
                    <h2>Cookies</h2>
                    <p>Our website uses cookies and similar technologies to provide certain functionality to the website, to understand and measure its performance, and to deliver targeted advertising.</p>
                    <h2>Keeping data secure</h2>
                    <p>We currently safeguard personal data by storing it on a CRM protected by password and shall ensure that we use no lesser technical and organisational measures to safeguard personal data which is disclosed to us. Whilst we will take every effort to safeguard such personal data, you acknowledge that the use of the internet is not entirely secure and therefore we cannot guarantee the security of your personal data.</p>
                    <h2>Monitoring</h2>
                    <p>We may monitor and record communications with you (such as telephone conversations and emails) for the purpose of quality assurance, training, fraud prevention or compliance purposes.</p>
                    <h2>Information about other individuals</h2>
                    <p>If you give us information about others, you confirm that the other third party person has appointed you to act on his/her behalf. This is also relevant where others are concerned if you indeed ask another to act on your behalf as a third party.
<br>
                        Under the third party authorisation, the other person can:</p>
                    <ul>
                        <li style="font-weight: 400;">Give consent on his/her behalf to the processing of his or her personal data for the purposes and reasons set out in this Policy; and</li>
                        <li style="font-weight: 400;">Receive on his/her behalf any data protection notices.</li>
                    </ul>
                    <p>Such authorisation will remain in place until this has been revoked, either by verbal or written communication.</p>
                    <h2>Use of Google Analytics Advertising</h2>
                    <p>We use Google Analytics Advertising Features (‘GAAF’) through our website, which means that certain information about the traffic on our website is collected. In light of using GAAF, We will not facilitate the merging of personally-identifiable information with non-personally identifiable information collected through GAAF unless we receive your express consent to that merger.
<br>
                        Furthermore, we are hereby notifying you that:</p>
                    <ul>
                        <li style="font-weight: 400;">The specific GAAF feature(s) which we have implemented are:
                            <ul>
                                <li style="font-weight: 400;">Remarketing with Google Analytics</li>
                                <li style="font-weight: 400;">Google Display Network Impression Reporting</li>
                                <li style="font-weight: 400;">Google Analytics Demographics and Interest Reporting</li>
                            </ul>
                        </li>
                        <li style="font-weight: 400;">We use first-party cookies (such as GAAF cookies) or other first-party identifiers, and third-party cookies (such as advertising cookies) or other third-party identifiers together and that this is done in the ways detailed under the sub-heading ‘Use of First &amp; Third Party Cookies and Identifiers’ below; and</li>
                        <li style="font-weight: 400;">You can opt-out of the GAAF you use, including through Ads Settings, Ad Settings for mobile apps, or any other available means such as the Google Analytics currently available opt-outs accessible via tools.google.com/dlpage/gaoutput</li>
                    </ul>
                    <h2>Lawful Basis</h2>
                    <p>We will only process your personal data providing you have your consent for us to do, Under the provisions of the GDPR, our firm’s lawful basis for processing personal data is based on a Legitimate Interest.</p>
                    <h2>Data Retention </h2>
                    <p>We will not keep your personal data for any longer than is necessary in light of the reason(s) for which it was first collected. We will retain personal information for the duration that you want to receive marketing communications from us or hold an Account with us.
<br>
                        If you have provided personal information for the purposes of receiving a quote from an advertiser then this legitimate interest relates to a legal requirement for the firm to hold your personal data and financial information on record for up to a total of six years. This six year period satisfies the requirement of our regulator, The Financial Conduct Authority and is also in line with other financial industry retention periods.</p>
                    <h2>International Transfers</h2>
<p>                    In the normal course of business there may be a need for Auto Lease Compare or any other firm associated to the business, usually when processing an application for credit, personal data would have to be transferred outside of the European Economic Area (EEA) where those countries do not typically have the same protections and safeguards in place for the protection of personal data to those countries within the EEA.
<br>
    Auto Lease Compare may deal with a number of large, international corporations where data is likely to be transferred in this way. Assurances and processes will always be put in place and considered before any international transfer to a non-EEA country is undertaken to ensure the protection and security of the personal data.</p>
                    <h2>Your Rights</h2>
                    <p>                    You have certain rights in respect of your personal information, including the right to access, correct, to cease processing and request the erasure of your personal information.
<br>
                    You also have the right to object to your personal information being used for certain purposes, including to send you marketing - which you have the option to opt-out of.
<br>
                        We will comply with any requests to exercise your rights in accordance with applicable law. Please be aware, however, that there are a number of limitations to these rights, and there may be circumstances where we are not able to comply with your request. To any requests regarding your personal information, or if you have any questions or concerns regarding your personal information, please contact us using the details below. You are also entitled to contact the UK’s supervisory authority for data protection, the Information Commissioner’s Office, or your local supervisory authority.</p>
                    <h2>How Can I Access My Personal Data?</h2>
                    <p>If you want to know what personal data we have about you, you can ask us for details of that personal data and for a copy of it (where any such personal data is held). This is known as a “subject access request”. All subject access requests should be sent to <a href="mailto:info@autoleasecompare.com">info@autoleasecompare.com</a> with the subject ‘Data Subject Access Request’.
<br>
                    There is not normally any charge for a subject access request. If your request is ‘manifestly unfounded or excessive’ (for example, if you make repetitive requests) a fee may be charged to cover our administrative costs in responding.
<br>
                        We will respond to your subject access request within one month of receiving it. Normally, we aim to provide a complete response, including a copy of your personal data within that time. In some cases, however, particularly if your request is more complex, more time may be required up to a maximum of three months from the date we receive your request. You will be kept fully informed of our progress.</p>
                    <h2>Changes to this Privacy Policy</h2>
                    <p>We may change this Privacy Notice from time to time. This may be necessary, for example, if the law changes, or if we change our business in a way that affects personal data protection.
<br>
                        Any changes will be immediately posted on Our Site and you will be deemed to have accepted the terms of the Privacy Policy on your first use of Our Site following the alterations. We recommend that you check this page regularly to keep up-to-date.</p>
                    <h2>Contact Us</h2>
                    <p>If you have any queries on any aspect of our Privacy Policy, please contact us by email at <a href="mailto:info@autoleasecompare.com">info@autoleasecompare.com</a>.</p>
                </div>

        </div>
    </div>
@endsection
@section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="/css/listing.css">
@endsection