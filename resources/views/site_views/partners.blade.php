@extends('layouts.app')
@section('template_title')
    Become a partner
@endsection
@section('content')

    <div class="container-fluid banner">
        <div class="row">
            <div class="col-md-12">
                <h1>Become a partner</h1>
            </div>
        </div>
    </div>

    <div class="container listingContainer">
        @if(app('request')->input('s'))
            <div id="message">
                <div class="alert alert-success">
                    Thank you, your message has been sent to the Auto Lease Compare Team and they will be in touch as soon as possible
                </div>
            </div>
        @endif

            @if(app('request')->input('e'))
                <div id="message">
                    <div class="alert alert-danger">
                        There was a problem submitting your information. Please try again or email <a href="mailto:info@autoleasecompare.com">info@autoleasecompare.com</a>
                    </div>
                </div>
            @endif

        <div class="row">
            <div class="col-md-8 offset-md-2 text-center">
                <h2>Want to be a partner?</h2>
                <p>If you’re interested in being our partner so you can advertise your vehicle offers on our website, we’d really love to hear from you. Please complete and submit the enquiry form below and a member of our sales team will get in touch with you to discuss your requirements.
                </p>
            </div>



            <form action="/site/partners-submit" method="post" style="width: 100%;padding-left: 20px;padding-right: 20px;">
                @csrf
                <div class="row">
                    <div class="col-md-6">

                        <label for="name" class="control-label">Full Name</label>
                        
                        <input required type="text" id="name" name="name" class="form-control" placeholder="Your name..">

                        <label for="job" class="control-label">Job Title</label>
                        <input type="text" id="job" name="job" class="form-control">

                        <label for="company" class="control-label">Company</label>
                        <input required type="text" id="company" name="company" class="form-control">

                        <label for="companyweb" class="control-label">Company Website</label>
                        <input type="text" id="companyweb" name="companyweb" class="form-control">


                        <label for="email" class="control-label">Email</label>
                        <input required type="email" id="email" name="email" class="form-control">

                        <label for="phone" class="control-label">Phone Number</label>
                        <input required type="tel" id="phone" name="phone" class="form-control">

                        <label for="companyno" class="control-label">Company Number</label>
                        <input required type="text" id="companyno" name="companyno" class="form-control">
                    </div>

                    <div class="col-md-6">

                        <label for="fcano" class="control-label">FCA Number</label>
                        <input required type="text" id="fcano" name="fcano" class="form-control">

                        <label for="bvrla" class="control-label">BVRLA Membership number</label>
                        <input required type="text" id="bvrla" name="bvrla" class="form-control">

                        <label for="subject" class="control-label">Your Message</label>
                        <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px; margin-bottom:20px;" class="form-control"></textarea>

                        <label for="cars" class="control-label">How many cars are in our logo? (anti-spam requirement)</label>
                        <input required type="number" id="cars" name="cars" class="form-control">
<br>
                        <input type="checkbox" name="terms" id="terms" required> <label for="terms">I accept the Terms and Conditions & Privacy Policy</label>

                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-right">
                        <input type="submit" value="Submit" class="button b1 mb-3">
                    </div>
                </div>
            </form>


        </div>
        @endsection
@section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="/css/listing.css">
@endsection

