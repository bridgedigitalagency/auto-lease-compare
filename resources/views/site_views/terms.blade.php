@extends('layouts.app')
@section('template_title')
Terms &amp; Conditions
@endsection
@section('content')

    <div class="container-fluid banner">
        <div class="row">
            <div class="col-md-12">
                <h1>Terms &amp; Conditions</h1>
            </div>
        </div>
    </div>

    <div class="container listingContainer">
        <div class="row">
            <div class="col-md-12">
                <p>
                    We are Auto Lease Compare Ltd (Company Number 11519767, Financial Conduct Authority Number: 822040, Data Protection Registration number ZA469390 and whose registered office is 160 Kemp House, City Road, London, EC1V 2NX [‘we’, ’us’ or ‘our’]). This policy will cover the terms and conditions that will govern your use of our website.
                    <br>
                By using this website you are agreeing to comply with and be bound by the following terms and conditions of use in full and that you agree to comply with them which together with our Privacy Policy to govern Auto Lease Compares relationship with you in relation to this website.
                    <br>
                If you do not agree to these terms, you are not permitted to use our website.
                </p>
                <h2>We may suspend our website</h2>
                <p>Our website is made available to you, free of charge. We do not guarantee that our sites, or any content on it, will always be available or uninterrupted. We may suspend or withdraw the availability of all or any part of our site for business and operational reasons.</p>
                <h2>Keeping your account details safe</h2>
                <p>If you create an account, or one is created for you, you must keep your login details (username and password) confidential. You must not disclose this information to any third party.
                    <br>
                We reserve the right to disable any user profile on our website, whether created by you or allocated by us, at any time without any prior notice or explanation required, if in our reasonable opinion you failed to comply with any of these provisions of these terms of use.
                    <br>
                    If you feel your user profile is at risk, you must promptly notify us of this at <a href="mailto:info@autoleasecompare.com">info@autoleasecompare.com</a> containing as much details as possible.</p>
                <h2>Finding deals on our website</h2>
                <p>Auto Lease Compare does not lease any cars directly. We enable lease companies to provide you with multiple offers through our comparison tool. If you decide to lease a car from a lease company, you will be entering into an agreement with that company. We are not a party to that agreement.
                    <br>
                    The deals and prices that we display on our website cannot be guaranteed. Although we work with our partners to ensure the most up-to-date information is presented correctly, we cannot guarantee the offer on our website is in fact the offer you will receive. Once you send an enquiry to a leasing company through our website the leasing company will be in touch with you directly to discuss your selected deal.
                    <br>
                    We are not obliged to review the deals on our website, but may do so as part of our quality checks and commitment to the customer experience we offer or, if you ask us to. We, or the leasing company may choose to edit or remove deals at any time.</p>
                <h2>Using material on our website</h2>
                <p>We are the owner or the licensee of all intellectual property rights in our sites, and in the material published on it. Those works are protected by copyright laws and treaties around the world. All such rights are reserved.
                    <br>
                    You may download extracts of any page(s) from our website for four personal use.
                    <br>
                    You must not modify these downloaded copies in any way and you must not use any graphics separately from any accompanying text.
                    <br>
                    Our status as the authors of content on our website must always be acknowledged.
                    <br>
                    You must not use any downloaded content from our website for commercial use without obtaining our explicit permission beforehand.
                    <br>
                    You may not, except with our express written permission, distribute or commercially exploit the content. Nor may you transmit it or store it in any other website or other form of electronic retrieval system.
                    <br>
                    If you are in breach of any of the terms identified in these terms of use, your right to use our website will cease immediately and you must destroy any copies of the materials you have made.</p>
                <h2>Our responsibility for loss or damage</h2>
                <p>We do not exclude or limit in any way our liability to you where it would be unlawful to do so. This includes liability for death or personal injury caused by our negligence of our employees, agents or subcontractors and for fraud or fraudulent misrepresentation.
                    <br>
                    The information contained in this website is for general information purposes only. The information is provided by Auto Lease Compare and whilst we do our best to keep the information up-to-date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk. In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of or in connection with the use of this website.
                    <br>
                    We exclude all implied conditions, warranties, representations or other terms that may apply to our website or any content on it.
                    <br>
                    We will not be liable to you for any loss or damage, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, even if foreseeable, arising under or in connection with:</p>
                <ul>
                    <li style="font-weight: 400;">Use of, or inability to use, our website; or</li>
                    <li style="font-weight: 400;">Use of, or any reliance on, any content displayed on our website</li>
                </ul>
                <p>In particular, we will not be liable for:</p>
                <ul>
                    <li style="font-weight: 400;">Loss of profits, sales, business or revenue;</li>
                    <li style="font-weight: 400;">Business interruption;</li>
                    <li style="font-weight: 400;">Loss of anticipated savings;</li>
                    <li style="font-weight: 400;">Loss of business opportunity, goodwill or reputation; or</li>
                    <li style="font-weight: 400;">Any indirect or consequential loss or damage</li>
                </ul>

                <h2>Viruses</h2>
                <p>We cannot guarantee that our website will be secure of free from bugs or viruses. You are responsible for configuring your own technology to protect yourself from viruses.
                    <br>
                    You must not misuse our website by knowingly introducing viruses, trojans, worms, logic bombs, keyloggers or other material that is malicious or technologically harmful.
                    <br>
                    You must not attempt to gain unauthorised access to our website, the server on which our website is hosted or any server, computer or database connected to our website.
                    <br>
                    We will report any such breach to the relevant law enforcement authorities and we will cooperate fully with those authorities by disclosing your identity to them. In the event of such a breach, we will revoke your access to our website and your rights to access out website will cease immediately.</p>
            </div>
        </div>
    </div>
@endsection
@section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="/css/listing.css">
@endsection