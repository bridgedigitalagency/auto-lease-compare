@extends('layouts.app')
@section('template_title')
    Leasing Information
@endsection
@section('content')
    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">Leasing Information</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / Leasing Information</div>
            </div>
        </div>
    </div>
    <section>
        <div class="container">
            <div class="row ">



                <div class="col-md-12">

                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">What is leasing?</a>
                            <a class="nav-item nav-link" id="nav-how-it-works-tab" data-toggle="tab" href="#nav-how-it-works" role="tab" aria-controls="nav-how-it-works" aria-selected="false">How it works</a>
                            <a class="nav-item nav-link" id="nav-benefits-tab" data-toggle="tab" href="#nav-benefits" role="tab" aria-controls="nav-benefits" aria-selected="false">Benefits of leasing</a>
                            <a class="nav-item nav-link" id="nav-faq-tab" data-toggle="tab" href="#nav-faq" role="tab" aria-controls="nav-faq" aria-selected="false">FAQs</a>
                        </div>
                    </nav>

                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <p class="pt-3">Love having a new car every few years? Crave that new car smell? Then leasing, also known as Personal Contract Hire (PCH) or Business Contract Hire (BCH), is the most affordable, convenient and hassle-free way to drive a brand new car.</p>
                            <p>Leasing a car is just like renting anything else. If you rent a house or flat, you pay a deposit, then you use it for an agreed period paying a set amount each month. Once the contract ends, the property reverts back to the landlord.</p>
                            <p>It’s the same with vehicle leasing - you chose which brand new car you want, how long you want the car for (2, 3 or 4 years), what payment you can afford upfront (normally 1, 3, 6, 9 or 12 months initial payment) and how many miles you think you will drive. Then find fixed monthly payments that suit your budget. It’s a long term rental agreement so you won’t be purchasing the car, you simply return it at the end of the lease term and get another brand new one if you’d like. Simple and hassle free!</p>
                        </div>
                        <div class="tab-pane fade" id="nav-how-it-works" role="tabpanel" aria-labelledby="nav-how-it-works-tab">
                            <p>Auto Lease Compare allows you to <strong>compare lease deals and get behind the wheel of your brand new car</strong> in just a few simple steps, putting the best prices available at your fingertips in a matter of seconds.</p>

                            <h3>1. Search for your perfect car</h3>
                            <p>Compare lease prices quickly and easily. Select the make, model, specifications and the initial rental payment which suits you. Compare millions of deals from top leasing companies to find the best price and package to suit your needs.</p>
                            <h3>2. Send an enquiry or call direct</h3>
                            <p>Through our website you can send an enquiry or call the leasing company directly (free of charge) to discuss all the details and agree on a personalised deal.</p>
                            <h3>3. Place your order & take delivery</h3>
                            <p>Once you are happy, place your order with the leasing company. Vehicles can be delivered straight to your door (free of charge mainland UK) or collected. That’s why the leasing providers office location doesn’t matter – they will do the hard work for you!</p>
                            <h3>4. Monthly payments & return car</h3>
                            <p>Pay a set amount each month, generally over 1, 2, 3 or 4 years (depending on the contract length of the lease agreement). At the end of the contract, simply return the vehicle to the leasing provider (they will collect the car free of charge), then, get a brand new lease car if you like – simple!</p>
                            <p>Still got a question? Take a look at our FAQ section for more information about car leasing.</p>
                        </div>

                        <div class="tab-pane fade" id="nav-benefits" role="tabpanel" aria-labelledby="nav-benefits-tab">
                            @include('partials.benefits')
                        </div>
                        <div class="tab-pane fade" id="nav-faq" role="tabpanel" aria-labelledby="nav-faq-tab">
                            @include('partials.faqs')
                        </div>
                    </div>

                </div>



            </div>
        </div>

    </section>
@endsection
@section('template_linked_css')
    @php $rand = rand(0,99999); @endphp
    <link rel="stylesheet" type="text/css" href="/css/pages.css?s={{$rand}}">
@endsection
@push('scripts')
    <script src="https://kit.fontawesome.com/f2e99604d5.js" crossorigin="anonymous"></script>
    <script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "WebPage",
    "name": "Personal Car Leasing",
    "description": "Get the best personal car lease deals from Auto Lease Comparison at unbeatable and affordable prices. View our deals online and book now."}
</script>
@endpush