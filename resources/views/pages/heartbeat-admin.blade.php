@extends('layouts.app')
@section('template_title') HEARTBEAT @endsection
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@push('scripts')
@endpush
@section('content')

    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">Site Stats</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / <a href="/my-account/admin-tools/">Admin Tools</a> / Site Stats</div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <br><br>
                <table class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Check performed</th>
                        <th scope="col">Result</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Number of Deals (Cars):</th>
                        <td>{{number_format($numberOfDeals)}}</td>

                    </tr>
                    <tr>
                        <th scope="row">Number of Deals (Vans):</th>
                        <td>{{number_format($numberOfVanDeals)}}</td>

                    </tr>
                    <tr>
                        <th scope="row">Number of Dealers:</th>
                        <td>{{number_format($numberOfDealers)}}</td>

                    </tr>
                    <tr>
                        <th scope="row">Number of Caps (cars):</th>
                        <td>{{number_format($numberOfCaps)}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Number of Caps (vans):</th>
                        <td>{{number_format($numberOfVanCaps)}}</td>
                    </tr>

                    <tr>
                        <th scope="row">Number of Users:</th>
                        <td>{{number_format($numberOfUsers)}}</td>

                    </tr>
                    <tr>
                        <th scope="row">Number of users with deals in garage:</th>
                        <td>{{number_format($totalGarage)}}</td>

                    </tr>
                    <tr>
                        <th scope="row">Number of DWV enquiries:</th>
                        <td>{{number_format($dwv)}}</td>
                    </tr>
                </tbody>
            </table>

                <h2>Enquiries (Total)</h2>
                <table class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Check performed</th>
                        <th scope="col">Emails</th>
                        <th scope="col">Calls</th>
                        <th scope="col">Total</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Number of Enquiries Today:</th>
                        <td>{{number_format($todaysDeals + $todaysVanDeals)}}</td>
                        <td>{{number_format($todaysCalls)}}</td>
                        <td>{{number_format($todaysDeals + $todaysVanDeals + $todaysCalls)}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Number of Enquiries Yesterday:</th>
                        <td>{{number_format($yesterdaysDeals + $yesterdaysVanDeals)}}</td>
                        <td>{{number_format($yesterdaysCalls)}}</td>
                        <td>{{number_format($yesterdaysDeals + $yesterdaysVanDeals + $yesterdaysCalls)}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Number of Enquiries This Month:</th>
                        <td>{{number_format($thisMonthDeals + $thisMonthVanDeals)}}</td>
                        <td>{{number_format($thisMonthCalls)}}</td>
                        <td>{{number_format($thisMonthDeals + $thisMonthCalls + $thisMonthVanDeals)}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Number of Enquiries Last Month:</th>
                        <td>{{number_format($lastMonthDeals + $lastMonthVanDeals)}}</td>
                        <td>{{number_format($lastMonthCalls)}}</td>
                        <td>{{number_format($lastMonthDeals + $lastMonthCalls + $lastMonthVanDeals)}}</td>
                    </tr>
                    </tbody>
                </table>

                <h2>Enquiries (Cars)</h2>
                <table class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Check performed</th>
                        <th scope="col">Emails</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Number of Enquiries Today:</th>
                        <td>{{number_format($todaysDeals)}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Number of Enquiries This Month:</th>
                        <td>{{number_format($thisMonthDeals)}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Number of Enquiries Last Month:</th>
                        <td>{{number_format($lastMonthDeals)}}</td>
                    </tr>
                    </tbody>
                </table>

                <h2>Enquiries (Vans)</h2>
                <table class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Check performed</th>
                        <th scope="col">Emails</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Number of Enquiries Today:</th>
                        <td>{{number_format($todaysVanDeals)}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Number of Enquiries This Month:</th>
                        <td>{{number_format($thisMonthVanDeals)}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Number of Enquiries Last Month:</th>
                        <td>{{number_format($lastMonthVanDeals)}}</td>
                    </tr>
                    </tbody>
                </table>

                    <h2>Brokers that provide van deals</h2>
                <table class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Broker Name</th>
                        <th scope="col">Number of deals</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($vBrokers as $k=>$v)
                        <tr>
                            <th scope="row">{{$k}}</th>
                            <td>{{number_format($v)}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>


            </div>
        </div>
    </div>

    </div>
@endsection