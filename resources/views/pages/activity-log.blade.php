@extends('layouts.app')
@section('template_title') Activity Log @endsection
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="/css/listing.css">
    <style>
        td span {
            cursor: help;
        }
    </style>
@endsection
@push('scripts')
    <script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
    </script>
@endpush
@section('content')
    <div class="container-fluid banner">
        <div class="row justify-content-md-center">
            <div class="col-lg-6 col-md-12"><h1>Activity Log</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Username</th>
                        <th scope="col">Status</th>
                        <th scope="col">Quote Id</th>
                        <th scope="col">Updated At</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($activity as $act)
                            <tr>
                                <th scope="row">{{$act->id}}</th>
                                <td>
                                    @php
                                        $user = unserialize($act->user);
                                        if($user['name']) {
                                            if(strpos($user['name'], '@')) {
                                            echo "<span data-toggle='tooltip' data-placement='top' title='" . $user['fullname'] . " -  " . $user['email'] . "'> " . $user['fullname'] . "</span>";
                                            }else{
                                                echo "<span data-toggle='tooltip' data-placement='top' title='" . $user['name'] . " -  " . $user['email'] . "'> " . $user['name'] . "</span>";
                                            }

                                        }
                                    @endphp
                                </td>
                                <td>
                                    @php
                                        switch ($act->status) {
                                            case 1:
                                                echo 'Quote Sent';
                                                break;
                                            case 2:
                                                echo 'Sold';
                                                break;
                                            case 3:
                                                echo 'Pending Payment';
                                                break;
                                            case 4:
                                                echo 'Paid';
                                                break;
                                            case 5:
                                                echo 'Closed';
                                                break;
                                            case 6:
                                                echo 'Open';
                                                break;
                                            case 7:
                                                echo 'Withdrawn';
                                            break;
                                        }
                                    @endphp
                                </td>
                                <td>{{$act->quote_id}}</td>
                                <td>{{$act->created_at}}</td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection