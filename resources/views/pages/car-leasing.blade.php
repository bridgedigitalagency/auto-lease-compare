@extends('layouts.app')
@section('template_title')Car Leasing & Lease Deals @endsection
@section('extra_meta_tags')
@endsection
@section('template_description')
    <meta name="description" content="Browse our latest & best car leasing deals. Compare vehicle lease contracts from a range of providers to find the best deal for you.">
@endsection

@push('scripts')
    @php
    $rand = rand(0,9999);
    @endphp

    <link rel="stylesheet" href="/css/pages.css?s={{$rand}}">
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <script>
        $(document).ready(function() {
            $('#carousel').slick({
                autoplay: true,
                arrows: false,
                dots: false,
                draggable: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                touchMove: true,
                swipe: true,
                swipeToSlide: true,
                infinite: true,
                prevArrow: '',
                nextArrow: '',
                mobileFirst: true
            });
        });
    </script>
@endpush
@section('content')

    <div class="container-fluid b3">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">Car Leasing Deals</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / Car Leasing Deals</div>
            </div>
        </div>
    </div>

    <div class="whiteBg">
<div class="container car-leasing-content">
    <div class="row">
        <div class="col bg-white pt-md-5 pt-3 pb-md-4 pb-3 text-center">
            <h2>Popular Car Leasing Deals</h2>
        </div>
    </div>


    <div class="row">
        <div class="col">
            <div id="carousel">
            @foreach($deals as $deal)
                    <div class="col-12 mb-3 mt-3 relative-link">
                        <div class=" result-row1">

                            <a href="/personal/?maker[]={{ $deal->maker }}&range[]={{$deal->range}}&trim[]={{$deal->range}}_{{$deal->trim}}&listing_type=P&finance_type=P">

                                @if(isset($deal->imageid) && !is_null($deal->imageid))
                                    <img src="/images/capimages/{{ $deal->imageid }}.jpg" class="img-fluid" alt="{{ $deal->maker }} {{ $deal->model }} {{ $deal->derivative }}" onerror="this.src='https://www.autoleasecompare.com/images/comingsooncar.jpg'">
                                @else
                                    <img src="https://www.autoleasecompare.com/images/comingsooncar.jpg" class="img-fluid" alt="{{ $deal->maker }} {{ $deal->model }} {{ $deal->derivative }}">
                                @endif

                                <div class="relative text-center">
                                    <div class="relative-title"><p>{{ $deal->maker }} {{ $deal->model }} {{ $deal->derivative }}</p></div>
                                    <div class="mb-2"></div>
                                    <button type="submit" class="button b3"><i class="fa fa-search"></i> Find deals</button>
                                    <div class="mb-2"></div>
                                </div>
                            </a>

                        </div>
                    </div>
            @endforeach
            </div>

        </div>
    </div>
</div>
    </div>

<div class="container car-leasing-content searchContainer">
    <div class="row">
        <div class="col text-center pt-4 pb-3">
            <h3>Find Your Perfect Lease Today</h3>
        </div>
    </div>
<div class="row pb-5">
    <div class="col search pb-2">
        <form action="#" method="get" class="" id="carSearch">
            <div class="row">
                <div class="col-md-12 col-md-push-3">
                    <div class="row">
                        <div class="col-md-6 col-xs-6 mb-2">
                            <div class="select">
                                <select id="maker" class="w-100 form-control pricing_select" name="maker">
                                    <option value="" selected="">Any Make</option>
                                    @foreach($makers as $maker)
                                        <option value="{{$maker->maker}}">{{$maker->maker}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6 mb-2">
                            <div class="select">
                                <select id="range" class="w-100 form-control pricing_select" name="range[]">
                                    <option value="" selected="">Any Model</option>
                                </select></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 mb-2">
                    <div class="finance_type">
                        @if(isset($database) && $database == 'LCV')
                            <label><input type="radio" name="finance-type" value="b" checked=""> Business</label>
                            <label><input type="radio" name="finance-type" value="p"> Personal</label>
                        @else
                            <label><input type="radio" name="finance-type" value="p" checked=""> Personal</label>
                            <label><input type="radio" name="finance-type" value="b"> Business</label>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12 col-xs-12">
                        <button type="submit" class="button b3"><i class="fa fa-search"></i> Find deals</button>
                        @if($database == 'LCV')
                            <input type="hidden" name="database" value="LCV">
                            <p><a style="margin-top: 10px; display: block; color: #000;" href="/">Looking for a car?</a></p>
                        @else
                            <input type="hidden" name="database" value="CARS">
                            <p><a style="margin-top: 10px; display: block; color: #000;" href="/vans">Looking for a van?</a></p>
                        @endif
                    </div>
                </div>
            </div>
        </form>
    </div>
    </div>

</div>

    <div class="whiteBg">
        <div class="container car-leasing-content pt-5">
    <div class="row">
        <div class="col">
            <h2>What is car leasing and who is it for?</h2>
            <p>Car leasing is one of the most easy and affordable ways to get your hands on a brand new car. Car leasing, sometimes also known as contract hire, is essentially a rental for a vehicle. You pay an upfront deposit on and then a monthly fee for the duration of the contract you took out the lease broker.</p>
            <p>Auto Lease Compare is one the UK favourite comparison websites for finding the best car leasing deals and offers.</p>
            <p>Because you’re leasing a brand new car, leasing removes the need to worry about MOTs for 2-3 years, you also get access to all the latest driving technologies in vehicle performance, fuel efficiency and comfort. Another benefit to leasing a car is that you are not tied to the vehicle, at the end of your contract you can choose to simply return the car, or take out a lease on a new car.</p>
            <h2> Cheap car leasing deals</h2>
            <p>Using our intuitive lookup functionality, we find the most affordable car leasing deals in the UK for you to compare and find the best vehicle for you. No matter what your budget is, we will find the best deal for you to help keep your costs down to a minimum.</p>
            <h2> Best personal car leasing deals</h2>
            <p>Whether you’re looking for a versatile hatchback for commuting, a practical family-sized SUV, or an executive saloon, we can help you find the perfect car for you. We work closely with a whole range of car leasing companies to bring you the best deals.</p>


            <h3> How does car leasing with Auto Lease Compare work?</h3>
            <p>You can browse our website of personal car leasing, or look for lease deals for a specific make and model. Then you can compare different lease deals from a wide range of leasing providers to find the best car lease deals for you. You can then make an enquiry through us or call the leasing provider directly to get a more personalised quote. We make leasing a car as hassle free as possible, simply choose the vehicle you want to lease, the length of time you want to lease it for and how many miles you plan to drive it for, then select the best lease deal that suits your needs and budget. You can also find insurance and any optional extras through us.</p>
            <h3> Does Auto Lease Compare supply the vehicles?</h3>
            <p>Auto Lease Compare does not provide the vehicles in the leasing contracts. We just allow you to find and compare car leasing deals from the wide range of leasing providers on the market.</p>
            <h3> Why should I choose Auto Lease Compare for car leasing?</h3>
            <p>Through our website you can find the best car leasing deals on the market. We take all the stress and hassle out of car leasing so you can focus on enjoying driving your brand new car. As well as finding the best lease deals that are out there, you can also find quick and easy insurance for your new lease car through our partners Go Compare.</p>
            <h3> More car leasing FAQs</h3>
            <p>If you want to know more about how car leasing works and the benefits of it, check out our extensive frequently asked questions.</p>
        </div>
    </div>
        </div></div>


    <section class="browseLeaseType manufacturerSearch pt-md-5 pt-1 pb-mb-5 pb-3">
        <div class="container">
            <div class="text-center pb-3 ">
                <h2 class="w900">Browse By Lease Type</h2>
            </div>
            <div class="row text-white about text-center">
                <div class="col-md-6 mb-4">

                <a href="/personal/" class="text-white">
                        <div class="boxes" style="padding:100px 0px;background-image: url(/images/personal-car-leasing.jpg); background-position: right center;background-size: cover;">
                            <h2>
                                Personal Car Leasing

                            </h2>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 mb-4">
                    <a href="/business/" class="text-white">
                        <div class="boxes" style="padding:100px 0px;background-image: url(/images/business-car-leasing.jpg); background-position: right center;background-size: cover;">
                            <h2>
                                Business Car Leasing

                            </h2>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 mb-4">
                    <a href="/performance/" class="text-white">
                        <div class="boxes" style="padding:100px 0px;background-image: url(/images/performance-car-leasing.jpg); background-position: right center;background-size: cover;">
                            <h2>Performance Car Leasing</h2>
                        </div>
                    </a>
                </div>
                <div class="col-md-6">
                    <a href="/hybrid/" class="text-white">
                        <div class="boxes" style="padding:100px 0px;background-image: url(/images/hybrid-car-leasing.jpg); background-position: right center;background-size: cover;">
                            <h2>Hybrid & Electric Car Leasing</h2>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </section>
</div>
@endsection