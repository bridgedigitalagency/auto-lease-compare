@extends('layouts.app')

@section('template_title')
    Vehicle Repair
@endsection
@section('template_description')

@endsection
@section('content')
    <section id="vehicleRepair">
        <div class="container-fluid banner">
            <div class="row">
                <div class="col-md-12">
                    <h1>Vehicle Repair</h1>
                </div>
            </div>
        </div>
        <br>
        <form action="/post-vehicle-repair" method="POST" enctype="multipart/form-data" id="vehicleRepair">
            @csrf
            <div class="container">
                @if( app('request')->input('s') == '1')
                    <div class="row">
                        <div class="col">
                            <div class="alert alert-success topSuccess">
                                <h2>Thank you!</h2>
                                <p>Your enquiry has been sent to DWV.</p>
                                <p>DWV will be in touch with you as soon as possible.</p>

                            </div>
                        </div>
                    </div>
                @endif
                @if(app('request')->input('e'))
                    <div class="alert alert-danger">
                        <h4>Following errors occurred:</h4>
                        @if( app('request')->input('e') == 1)
                            <p>The image you uploaded is not valid. Please ensure the image is either jpg or png filetype. If problems persist then please use the live chat service and we'll be glad to help you.</p>
                        @endif
                    </div>
                @endif
                <div class="row">
                    <div class="col headerContent">
                        <div class="row">
                            <div class="col-12 col-md-8">
                                <h2>About DWV SMART Repair Solutions</h2>
                                <p>DWV is one of the largest specialist mobile SMART, dent removal and alloy wheel repairers in the UK. This means that if you need minor paint scratches, scuffs, stone chips, dents or alloy wheels repairing - they can fix your vehicle directly at your home or office with great customer service and competitive pricing.</p>
                                <p><strong>Please fill in the form below to receive a customised quote from DWV. </strong></p>
                            </div>
                            <div class="col-12 col-md-4 text-center">
                                <img src="images/dwvlogo.svg" style="max-width: 300px; margin-left: 30px;"><br>
                                <img src="/images/trustpilot/trustpilot-5.png" style="max-width: 150px; margin: 10px 0;"><br>
                                <a href="https://uk.trustpilot.com/review/www.dwv.co.uk" target="_blank" style="color: #000">Rated Excellent on Trustpilot</a>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="row">

                    <div class="col-12 col-md-6">
                        <h2>Your Details</h2>
                        <div class="form-group">
                            <label for="name">Name *</label>
                            <input type="text" name="name" class="form-control" required @if(!is_null(Request::old('name'))) value="{{Request::old('name')}}" @endif>
                        </div>
                        <div class="form-group">
                            <label for="email">Email Address *</label>
                            <input type="email" name="email" class="form-control" required @if(!is_null(Request::old('email'))) value="{{Request::old('email')}}" @endif>
                        </div>

                        <div class="form-group">
                            <label for="phone">Phone Number *</label>
                            <input type="tel" name="phone" class="form-control" required  onkeypress="return isNumberKey(event)"  @if(!is_null(Request::old('phone'))) value="{{Request::old('phone')}}" @endif>
                        </div>

                        <div class="form-group">
                            <label for="address">1st line of address *</label>
                            <input type="text" name="address" class="form-control" required @if(!is_null(Request::old('address'))) value="{{Request::old('address')}}" @endif>
                        </div>
                        <div class="form-group">
                            <label for="postcode">Postcode *</label>
                            <input type="text"  name="postcode" class="form-control" required @if(!is_null(Request::old('postcode'))) value="{{Request::old('postcode')}}" @endif>
                        </div>
                        <div class="form-group">
                            <h2>Damage Type</h2>
                            <p>Please select the type of damage you have (You can select more than one)</p>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="damageSelect[]" id="inlineCheckbox1" value="alloy">
                                <label class="form-check-label" for="inlineCheckbox1">Alloy Wheel Repairs</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="damageSelect[]" id="inlineCheckbox2" value="dent">
                                <label class="form-check-label" for="inlineCheckbox2">Dent Repairs</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="damageSelect[]" id="inlineCheckbox3" value="paint">
                                <label class="form-check-label" for="inlineCheckbox3">Paint Repairs</label>
                            </div>
                        </div>

                        <h2>The Damage</h2>
                        <div class="form-group fileFields">
                            <p>To get an accurate estimate, please provide 2 pictures of the damage to your vehicle:</p>
                            <ol>
                                <li>A close up image of the damage in detail.</li>
                                <li>A reference image 2 metres away from the damage.</li>
                            </ol>
                            <label for="damage">The Damage</label>
                            <input type="file" id="damage" name="damage" class="form-control-file" @if(!is_null(Request::old('damage'))) value="{{Request::old('damage')}}" @endif>
                            <div style="display: none" class="alert-danger alert" id="damageError">File too big, please select a file less than 8mb</div>
                            <div style="display: none" class="alert-danger alert" id="damageFileError">File is incorrect type. Please upload jpg or png.</div>

                            <label for="reference">Reference Image</label>
                            <input type="file" id="reference" name="reference" class="form-control-file" @if(!is_null(Request::old('reference'))) value="{{Request::old('reference')}}" @endif>
                            <div style="display: none" class="alert-danger alert" id="referenceError">File too big, please select a file less than 8mb</div>
                            <div style="display: none" class="alert-danger alert" id="referenceFileError">File is incorrect type. Please upload jpg or png.</div>
                            <p>8mb size limit per image. File types: jpg or png</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <h2>Vehicle Details</h2>
                        <div class="form-group">
                            <label for="registration">Registration *</label>
                            <input type="text" name="registration" class="form-control" required @if(!is_null(Request::old('registration'))) value="{{Request::old('registration')}}" @endif>
                        </div>
                        <div class="form-group">
                            <label for="make">Make *</label>
                            <input type="text" name="make" class="form-control" required @if(!is_null(Request::old('make'))) value="{{Request::old('make')}}" @endif>
                        </div>
                        <div class="form-group">
                            <label for="model">Model *</label>
                            <input type="text" name="model" class="form-control" required @if(!is_null(Request::old('model'))) value="{{Request::old('model')}}" @endif>
                        </div>
                        <div class="form-group">
                            <h2>Additional Details</h2>
                            <p>Please provide any additional information here regarding your damage that you may think will be of use to the Technician eg: colour codes / total length of damage etc...</p>
                            <textarea name="additional" class="form-control" rows="10"> @if(!is_null(Request::old('additional'))){{Request::old('additional')}}@endif</textarea>
                        </div>

                        <div class="form-group">
                            <input type="submit" value="Send" class="button b3" id="submitButton">
                        </div>

                    </div>

                </div>
            </div>
        </form>
    </section>
@endsection
@section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="/css/listing.css">
    <link rel="stylesheet" type="text/css" href="/css/pages.css">
@endsection

@push('scripts')
    <script src="js/pages.js"></script>
@endpush