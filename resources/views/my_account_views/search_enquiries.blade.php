
            @foreach($quotes as $quote)
                @if(Auth::check() && Auth::user()->hasRole('admin'))
                    @include('partials.enquiries-admin')
                @elseif(Auth::check() && Auth::user()->hasRole('dealer'))
{{--                    @include('partials.enquiries-admin')--}}
                    @include('partials.enquiries-dealer')
                @elseif(Auth::check() && Auth::user()->hasRole('user'))
                    @include('partials.enquiries-user')
                @endif
            @endforeach
