@extends('layouts.app')
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@section('template_title')
    View email templates
@endsection

@section('content')

    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">View Email templates</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / <a href="/my-account" style="color:#fff;">My Dashboard</a> / View emails</div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Sent</th>
                        <th scope="col">Sent to</th>
                        <th scope="col">View</th>
                    </tr>
                    </thead>
                    <tbody>
                    {{--                    <tr>--}}
                    {{--                        <th scope="row">Customer Quote Sold</th>--}}
                    {{--                        <td>When customer has marked quote sold</td>--}}
                    {{--                        <td>Auto Lease Compare</td>--}}
                    {{--                        <td><a href="/view-emails/view/customer_quote_sold">View</a></td>--}}
                    {{--                    </tr>--}}
                    <tr>
                        <th scope="row">Enquiry</th>
                        <td>When customer makes an enquiry</td>
                        <td>Broker</td>
                        <td><a href="/view-emails/view/enquiry">View</a></td>
                    </tr>
                    <tr>
                        <th scope="row">Enquiry-customer</th>
                        <td>When an customer makes an enquiry</td>
                        <td>Customer</td>
                        <td><a href="/view-emails/view/enquiry-customer">View</a></td>
                    </tr>
                    <tr>
                        <th scope="row">Enquiry Customer Google Review</th>
                        <td>Not yet active - When an customer makes an enquiry, once trustpiliot reivews have been maxed this template will be sent instead</td>
                        <td>Customer</td>
                        <td><a href="/view-emails/view/enquiry-customer-google-review">View</a></td>
                    </tr>
                    <tr>
                        <th scope="row">Garage Price Update</th>
                        <td>When a garage entry price drops</td>
                        <td>Customer</td>
                        <td><a href="/view-emails/view/garage_price_update">View</a></td>
                    </tr>
                    <tr>
                        <th scope="row">Partner Enquiry</th>
                        <td>When a user sends an enquiry to become a partner</td>
                        <td>Auto Lease Compare</td>
                        <td><a href="/view-emails/view/partner-enquiry">View</a></td>
                    </tr>
                    <tr>
                        <th scope="row">Phone Enquiry</th>
                        <td>When a user makes a call to a broker</td>
                        <td>Brokers</td>
                        <td><a href="/view-emails/view/phone_enquiry">View</a></td>
                    </tr>
                    <tr>
                        <th scope="row">Quote Sent</th>
                        <td>When a dealer has sent a quote and marked the enquiry as such</td>
                        <td>Customer</td>
                        <td><a href="/view-emails/view/quote_sent">View</a></td>
                    </tr>
                    <tr>
                        <th scope="row">Quote Sold</th>
                        <td>When a dealer has marked a quote as sold</td>
                        <td>Customer</td>
                        <td><a href="/view-emails/view/quote_sold">View</a></td>
                    </tr>
                    <tr>
                        <th scope="row">Welcome</th>
                        <td>When a user registers</td>
                        <td>Customer</td>
                        <td><a href="/view-emails/view/welcome">View</a></td>
                    </tr>
                    </tbody>

                </table>
            </div>
        </div>
    </div>


@endsection


@section('footer_scripts')

@endsection


@section('template_linked_css')

@endsection