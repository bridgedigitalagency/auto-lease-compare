<div class="col-sm-12">
    <h2>Administration</h2>

    <div class="row">
        <div class="col-md-3" style="margin-bottom:20px;">
            <a href="/my-account/admin-tools" style="color:#fff; text-align:center;"><div style="background: #68d155;"><i class="fa fa-download" aria-hidden="true"></i><br>Admin Tools</div></a>
        </div>
        <div class="col-md-3" style="margin-bottom:20px;">
            <a href="/my-account/enquiries/call-log" style="color:#fff; text-align:center;"><div style="background: #ca3ad1;"><i class="fa fa-tachometer" aria-hidden="true"></i><br>Call Log</div></a>
        </div>
        <div class="col-md-3" style="margin-bottom:20px;">
            <a href="/my-account/dealers" style="color:#fff; text-align:center;"><div style="background: #20639b;"><i class="fa fa-users" aria-hidden="true"></i><br> Dealers</div></a>
        </div>
        <div class="col-md-3" style="margin-bottom:20px;">
            <a href="/activity-log" style="color:#fff; text-align:center;"><div style="background: #ff7800;"><i class="fa fa-tachometer" aria-hidden="true"></i><br>Activity Log</div></a>
        </div>
        <div class="col-md-3" style="margin-bottom:20px;">
            <a href="/my-account/export-users" style="color:#fff; text-align:center;"><div style="background: #47d1c0;"><i class="fa fa-download" aria-hidden="true"></i><br>Export Users</div></a>
        </div>
        <div class="col-md-3" style="margin-bottom:20px;">
            <a href="/my-account/export-quotes" style="color:#fff; text-align:center;"><div style="background: #68d155;"><i class="fa fa-download" aria-hidden="true"></i><br>Export Quotes</div></a>
        </div>
        <div class="col-md-3" style="margin-bottom:20px;">
            <a href="/my-account/export-dwv" style="color:#fff; text-align:center;"><div style="background: #68d155;"><i class="fa fa-download" aria-hidden="true"></i><br>Export DWV</div></a>
        </div>
    </div>

</div>
<div class="col-sm-12">
    <h2>User Options</h2>

</div>