<h2>Welcome {{Auth::user()->name}}</h2>
<ul>
    <li>Enquiries from customers should be answered as quickly as possible (ideally within 24 hours of enquiry being made).</li>
    <li>All communication to customers must include the following:</li>
    <ul>
        <li>Subject to include "Your Auto Lease Compare Enquiry ID ALC####"</li>
        <li>Body of the email to include “Thank you for your recent enquiry through Auto Lease Compare Enquiry ID ALC####”</li>
    </ul>
    <li>All communication must <u>exclude</u> any other promotion or marketing materials e.g. “Refer a friend”</li>
    <li>To be compliant with GDPR, our customers personal data shall not be used for anything other than to provide a personalised quote. A customer may not receive your marketing materials <u>unless</u> they have freely consented to do so upon making contact with them (opt-in).</li>
    <li>You should have full access to and knowledge of the admin portal system (training available).</li>
    <li>Enquiries which you have sent a quote to the customer, change status to ‘Quote Sent’ (within 24 hours)</li>
    <li>Enquiries which lead to a sale must be marked as ‘Sold’ on the Auto Lease Compare admin portal (within 24 hours)</li>
    <li>This still applies if the customer chooses to purchase a different lease vehicle than the one originally enquired for or if the customer purchases the lease in another persons name e.g. their partners or family members.</li>
    <li>Enquiries which are on longer active, change status to ‘Withdraw Enquiry’ which will close the enquiry and provide a reason for closing.</li>
    <li>The Auto Lease Compare partners who fail to follow this process will be looked at on an individual basis, which may result in data feeds being frozen or the partnership being terminated.</li>
    <li>If the designated sales team member is due to be out of the office, it is your responsibility to update the email address within your ALC Dashboard so that any enquiries in the interim are responded to within 24 hours, please also make us aware of the change.</li>
</ul>




<div class="col-md-3" style="margin-bottom:20px;">
    <a href="/my-account/enquiries/call-log" style="color:#fff; text-align:center;"><div style="background: #ca3ad1;"><i class="fa fa-tachometer" aria-hidden="true"></i><br>Call Log</div></a>
</div>
<div class="col-md-3" style="margin-bottom:20px;">
    <a href="/my-account/export-quotes" style="color:#fff; text-align:center;"><div style="background: #68d155;"><i class="fa fa-download" aria-hidden="true"></i><br>Export Quotes</div></a>
</div>
<div class="col-md-3" style="margin-bottom:20px;">
    <a href="/my-account/my-pricing" style="color:#fff; text-align:center;"><div style="background: #47d1c0;"><i class="fa fa-download" aria-hidden="true"></i><br>My Pricing</div></a>
</div>
<div class="col-md-3" style="margin-bottom:20px;">
    <a href="/my-account/add-price" style="color:#fff; text-align:center;"><div style="background: #ff7800;"><i class="fa fa-tachometer" aria-hidden="true"></i><br>Add New Deal</div></a>
</div>
