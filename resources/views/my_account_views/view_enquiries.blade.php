@extends('layouts.app')
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@section('template_title')
    My Enquiries
@endsection
@section('content')
    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">My Enquiries</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / <a href="/my-account" style="color:#fff;">My Dashboard</a> / Enquiries</div>
            </div>
        </div>
    </div>
    <div class="container">
        @if(Auth::check() && Auth::user()->hasRole(['admin','dealer']) )
        <div class="row">
            <div class="col-sm-3">
                <form method="GET">
                    <label for="from">Sort by status</label>
                    <select id="status" class="form-control" name="status">
                        <option value="999">Show All</option>

                        <option value="6" @if( Request::get('status')=='6') selected="selected" @endif>Open</option>
                        <option value="1" @if( Request::get('status')=='1') selected="selected" @endif>Quote Sent</option>
                        <option value="2" @if( Request::get('status')=='2') selected="selected" @endif>Mark Sold</option>
                        @role('admin')
                            <option value="3" @if( Request::get('status')=='3') selected="selected" @endif>Pending Payment</option>
                            <option value="4" @if( Request::get('status')=='4') selected="selected" @endif>Paid</option>
                            <option value="5" @if( Request::get('status')=='5') selected="selected" @endif>Closed</option>
                        @endrole
                    </select>
                    <button class="btn btn-success" style="margin-top:30px;" type="submit" style="width:100%;"> Apply Filters</button>

                </form>
            </div>
            <div class="col-sm-9">
                <form method="get" action="#" id="enqSearch">
                    <label for="from">Search for enquiries</label>
                    <input type="text" name="autosearch" id="autosearch" placeholder="Start typing to search" class="w-100 form-control mb-2">
                </form>
            </div>
        </div>
        @endif
        <div class="row quoteContainer">
            @foreach($quotes as $quote)
                @if(Auth::check() && Auth::user()->hasRole('admin'))
                    @include('partials.enquiries-admin')
                @elseif(Auth::check() && Auth::user()->hasRole('dealer'))
{{--                    @include('partials.enquiries-admin')--}}
                    @include('partials.enquiries-dealer')
                @elseif(Auth::check() && Auth::user()->hasRole('user'))
                    @include('partials.enquiries-user')
                @endif
            @endforeach
            {{ $quotes->appends($_GET)->links('partials.pagination') }}
        </div>

    </div>


@endsection


@section('footer_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script>
        $(function () {

            $("body").on('submit', '#enqSearch', function (e) {
                e.preventDefault();
            });

            $('#autosearch').on('change', function () {
                var autosearch = $('#enqSearch').serialize();
                var sortby = $('#status').serialize();

                var data = autosearch + "&" + sortby;
                $.ajax(
                    {
                        type: "get",
                        data: data,
                        url: "/my-account/enquiries/search",
                        success: function (html) {
                            $('.quoteContainer').html(html);
                        }
                    });
            });
            $('#status').on('change', function () {
                // if there is content in the type search then we can update results on the fly with the status
                if ($('#autosearch').val()) {
                    var autosearch = $('#enqSearch').serialize();
                    var sortby = $('#status').serialize();

                    var data = autosearch + "&" + sortby;
                    $.ajax(
                        {
                            type: "get",
                            data: data,
                            url: "/my-account/enquiries/search",
                            success: function (html) {
                                $('.quoteContainer').html(html);
                            }
                        });
                }

            });

            $("body").on('change', '.update-status', function (e) {

                var statusChoice = $(this).val();
                var userRowId = $(this).attr('data-value');

                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'post',
                    url: '{{route('update-status')}}',
                    data: {'id': userRowId, 'statusChoice': statusChoice}
                }).done(function (response) {
                    $('div.success').html('Status changed').delay(7000).fadeOut();
                    location.reload();
                }).fail(function () {

                });
            });


            $("body").on('submit', '.update-quote-sent-form', function (e) {
                e.preventDefault();
                var userRowId = $(this).attr('data-value');
                var statusChoice = $('#status_choice-' + userRowId).val();

                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'post',
                    url: '{{route('update-status')}}',
                    data: {'id': userRowId, 'statusChoice': statusChoice}
                }).done(function (response) {
                    $('div.success').html('Status changed').delay(7000).fadeOut();
                    location.reload();
                }).fail(function () {

                });
            });

            $("body").on('submit', '.update-sold-form', function (e) {
                e.preventDefault();
                var userRowId = $(this).attr('data-value');
                var statusChoice = $('#status_choice-' + userRowId).val();

                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'post',
                    url: '{{route('update-status')}}',
                    data: {'id': userRowId, 'statusChoice': 2}
                }).done(function (response) {
                    $('div.success').html('Status changed').delay(7000).fadeOut();
                    location.reload();
                }).fail(function () {

                });
            });

            $("body").on('submit', '.update-withdraw-form', function (e) {
                e.preventDefault();

                var userRowId = $(this).find('input[name=withdraw_id]').val();
                var withdrawChoice = $(this).find('textarea[name=update-withdraw]').val();

                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'post',
                    url: '{{route('update-withdraw')}}',
                    data: {'id': userRowId, 'withdrawChoice': withdrawChoice}
                }).done(function (response) {
                    $('div.success').html('Quote Withdrawn').delay(7000).fadeOut();
                    location.reload();

                }).fail(function () {

                });
            });
            $("body").on('submit', '.update-livechat-form', function (e) {
                e.preventDefault();

                var dealid = $(this).find('input[name=dealid]').val();

                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'post',
                    url: '{{route('update-livechat')}}',
                    data: {'id': dealid}
                }).done(function (response) {
                    $('div.success').html('Quote marked as assisted by live chat.').delay(7000).fadeOut();
                    location.reload();

                }).fail(function () {

                });
            });

            {{--$("#update-quote-sent-form").on('submit', function(){--}}

            {{--    var quotesentChoice = "1";--}}
            {{--    var userRowId = $(this).attr('data-value');--}}
            {{--    $.ajax({--}}
            {{--        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},--}}
            {{--        type: 'post',--}}
            {{--        url: '{{route('update-quote-sent')}}',--}}
            {{--        data: { 'id': userRowId, quotesentChoice: quotesentChoice }--}}
            {{--    }).done(function(response) {--}}
            {{--        $('div.success').html('Thank-you for marking this as quote sent').delay(7000).fadeOut();--}}
            {{--        location.reload();--}}

            {{--    }).fail(function() {--}}

            {{--    });--}}
            {{--});--}}
            {{--$("#update-sold-form").on('submit', function(){--}}

            {{--    var soldChoice = "2";--}}
            {{--    var userRowId = $(this).attr('data-value');--}}
            {{--    $.ajax({--}}
            {{--        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},--}}
            {{--        type: 'post',--}}
            {{--        url: '{{route('update-sold')}}',--}}
            {{--        data: { 'id': userRowId, soldChoice: soldChoice }--}}
            {{--    }).done(function(response) {--}}
            {{--        $('div.success').html('Thank-you for marking this deal as sold').delay(7000).fadeOut();--}}
            {{--        location.reload();--}}

            {{--    }).fail(function() {--}}

            {{--    });--}}
            {{--});--}}
        });
    </script>
@endsection


@section('template_linked_css')
    <style>
        .statustab {
            color: #fff;
            padding: 2px 10px;
            border: 0;
            border-radius: 10px;
            display: inline;
        }

        .r1 {
            background: #007bff !important;
        }

        .r2 {
            background: #fc5185 !important;
        }

        .r3 {
            background: #17a2b8 !important;
        }

        .r4 {
            background: #28a745 !important;
        }

        .r5 {
            background: #dc3545 !important;
        }

        .r6 {
            background: #ff8f00 !important;
        }

        .r7 {
            background: #ab17b8 !important;
        }

        .product-listing-m {
            margin: 0 auto 10px;
            overflow: hidden;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
            border-radius: 15px;
            transition-duration: 0.3s;
            -moz-transition-duration: 0.3s;
            -o-transition-duration: 0.3s;
            -webkit-transition-duration: 0.3s;
            -ms-transition-duration: 0.3s;
        }

        .button {
            margin-top: 15px;
            padding: 10px 18px;
            border: 0px;
            border-radius: 20px;
            color: #fff !important;
            font-weight: 900;
        }
    </style>

@endsection