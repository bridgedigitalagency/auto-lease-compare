@extends('layouts.app')
@section('template_title')
    Add Price
@endsection
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@section('content')
    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">Add Price</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / <a href="/my-account" style="color:#fff;">My Dashboard</a> / <a href="/my-account/my-pricing/" style="color:#fff;">My Pricing</a> / Add New Deal</div>

            </div>
        </div>
    </div>

    @if(isset($noDealer) && $noDealer == 1)
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-warning" role="alert">
                        You are not authorised to create deals.
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p>Just had a hot deal land on your desk? Add it to our database today, the deal will be live as soon as you hit the submit button. Be sure to make sure the deal is live in your automated feed to avoid it being overwritten.</p>
                    <form method="GET">
                        <div class="row">
                            <div class="col-md-12"><h2>Step 1. Find Vehicle</h2></div>
                            <div class="col-md-4">
                                <select id="maker" class="form-control mb-3" name="maker" onchange="this.form.submit()">
                                    <option value="">Select Makes</option>
                                    @foreach($sumMakes as $sum1)
                                        <option value="{{$sum1->maker}}" @if($sum1->maker == app('request')->input('maker'))selected="selected"@endif>{{$sum1->maker}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select id="model" class="form-control mb-3" name="model" onchange="this.form.submit()">
                                    <option value="">Select Models</option>
                                    @foreach($sumModel as $sumM)
                                        <option value="{{$sumM->model}}" @if($sumM->model == app('request')->input('model'))selected="selected"@endif>{{$sumM->model}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select id="derivative" class="form-control mb-3" name="derivative" onchange="this.form.submit()">
                                    <option value="">Select Derivatives</option>
                                    @foreach($sumDer as $sumD)
                                        <option value="{{$sumD->derivative}}" @if($sumD->derivative == app('request')->input('derivative'))selected="selected"@endif>{{$sumD->derivative}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-12">
                    <h2>Step 2. Fill Out Deal Details</h2>

                        <form method="post" action="/pricing" enctype="multipart/form-data">
                            <div class="col-md-4">
                                @foreach($sumCap as $sumC)
                                    <input type="text" name="cap_id" value="{{$sumC->cap_id}}">
                                @endforeach
                            </div>
                            {{ csrf_field() }}
{{--                            <input class="form-control mb-2" style="display:none" name="cap_id" id="cap_id"  data-value="" value="{{ app('request')->input('cap_id') }}--}}
{{--                                    ">--}}
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="finance_type" class="col-form-label">Finance Type</label>
                                        <select id="finance_type" class="form-control" name="finance_type" aria-required="true" aria-invalid="true" required>
                                            <option value="">Select Finance Type</option>
                                            <option value="P">Personal</option>
                                            <option value="B">Business</option>
                                        </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="deposit_value" class="col-form-label">Months Upfront</label>
                                        <select id="deposit_value" class="form-control" name="deposit_value" aria-required="true" aria-invalid="true" required>
                                            <option value="">Select Months Upfront</option>
                                            <option value="1">1</option>
                                            <option value="3">3</option>
                                            <option value="6">6</option>
                                            <option value="9">9</option>
                                            <option value="12">12</option>
                                        </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="term" class="col-form-label">Term</label>
                                        <select id="term" class="form-control" name="term" aria-required="true" aria-invalid="true" required>
                                            <option value="">Select Term</option>
                                            <option value="12">12</option>
                                            <option value="18">18</option>
                                            <option value="24">24</option>
                                            <option value="36">36</option>
                                            <option value="48">48</option>
                                        </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="annual_mileage" class="col-form-label">Annual Mileage</label>
                                        <select id="annual_mileage" class="form-control" name="annual_mileage" aria-required="true" aria-invalid="true" required>
                                            <option value="">Select Annual Mileage</option>
                                            <option value="5000">5k</option>
                                            <option value="8000">8k</option>
                                            <option value="10000">10k</option>
                                            <option value="12000">12k</option>
                                            <option value="15000">15k</option>
                                            <option value="20000">20k</option>
                                            <option value="25000">25k</option>
                                            <option value="30000">30k</option>
                                        </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Monthly Rental Price</label>
                                    <input class="form-control mb-2" name="monthly_payment" id="monthly_payment"  data-value="" value="" type="decimal">
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Admin Fee (Inc Vat If Personal)</label>
                                    <input class="form-control mb-2" name="document_fee" id="document_fee"  data-value="" value="" type="decimal">
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Stock Status</label>
                                    <select id="in_stock" class="form-control" name="in_stock" aria-required="true" aria-invalid="true" required>
                                        <option value="">Select Stock Status</option>
                                        <option value="0">Factory Order</option>
                                        <option value="1">In Stock</option>
                                    </select>
                                </div>


                                <div class="form-group col-md-3">
                                    <label>Adverse Credit Deal?</label>
                                    <select id="in_stock" class="form-control" name="in_stock" aria-required="true" aria-invalid="true" required>
                                        <option value="">Select Adverse Credit Status</option>
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                                <div style="display: none;">
                                <label>Deposit</label>
                                <input class="form-control mb-2" name="deposit_months" id="deposit_months"  data-value="" value="">
                                <label>Average Cost</label>
                                <input class="form-control mb-2" name="average_cost" id="average_cost"  data-value="" value="">
                                <label>Total Cost</label>
                                <input class="form-control mb-2" name="total_cost" id="total_cost"  data-value="" value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <button type="submit" class="button b2">Submit</button>
                                </div>
                            </div>

                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif

                        </form>

                </div>

            </div>
        </div>
    @endif

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

    <script>
        $(function() {
            /*$("#monthly_payment").keyup(function(){
                var monthly_payment = $("#monthly_payment").val();
                var deposit_months = $("#deposit_value").val();
                $("#deposit_months").val(monthly_payment*deposit_months);
                if(monthly_payment == ""){
                    $("#deposit_months").val("Enter Value");
                }
            });*/
            $('#deposit_value').change(function () {
                changeThird();
            });
            $('#monthly_payment').keyup(function () {
                changeThird();
            });
            function changeThird() {
                $('#deposit_months').val($('#deposit_value').val() * $('#monthly_payment').val());
            }


            $('#deposit_value').change(function () {
                changeTotal();
            });
            $('#monthly_payment').keyup(function () {
                changeTotal();
            });
            $('#document_fee').keyup(function () {
                changeTotal();
            });
            $('#term').change(function () {
                changeTotal();
            });
            function changeTotal() {
                $('#total_cost').val(parseFloat($('#deposit_value').val()) * parseFloat($('#monthly_payment').val()) + (parseFloat($('#monthly_payment').val()) * (parseFloat($('#term').val()) - 1)) + parseFloat($('#document_fee').val()));
            }


            $('#deposit_value').change(function () {
                changeAv();
            });
            $('#monthly_payment').keyup(function () {
                changeAv();
            });
            $('#document_fee').keyup(function () {
                changeAv();
            });
            $('#term').change(function () {
                changeAv();
            });
            function changeAv() {
                $('#average_cost').val(Math.round((parseFloat($('#deposit_value').val()) * parseFloat($('#monthly_payment').val()) + (parseFloat($('#monthly_payment').val()) * (parseFloat($('#term').val()) - 1)) + parseFloat($('#document_fee').val())) / parseFloat($('#term').val())* 100) / 100);
            }
        });
    </script>

@endsection
<style>
    .der {
        font-size: 18px;
        font-weight: 900;
    }
    .features{
        font-weight:900;
    }
    .banner{
        text-align: center;
        color: #fff;
        background: #e74c3c;
        margin-bottom: 40px;
        padding-top: 22px;
        margin-top: -1.5rem;
    }
    .alert-dismissable{
        display: none !important;
    }
</style>
