@extends('layouts.app')
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@section('template_title')
    Dealers
@endsection
@section('content')
    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">Dealers</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / <a href="/my-account" style="color:#fff;">My Dashboard</a> / Dealers</div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-3">
<a href="/users/create" class="button b2"> Add New Dealer</a>
            </div>
            <div class="col-md-9">
                @foreach($DealerInfo as $dealer)
                    <div style="background: #fff;margin-bottom: 20px;">
                        <div class="row" style="padding: 20px;">
                            <div class="col-md-3">
                                <img class="img-fluid" style="max-height: 90px;" src="/images/uploads/logos/@if(isset($dealer->logo)){{$dealer->logo}}@endif">
                            </div>
                            <div class="col-md-6">
                                @if(isset($dealer->id))
                                  ID: {{$dealer->id}} <br/>
                                @endif
                                @if(isset($dealer->name))
                                 Username: {{$dealer->name}} <br/>
                                @endif
                                @if(isset($dealer->email))
                                 Email: {{$dealer->email}} <br/>
                                @endif
                                @if(isset($dealer->phone))
                                  Phone: {{$dealer->phone}} <br/>
                                @endif

                            </div>
                            <div class="col-md-3">
                                @if(isset($dealer->TotalEnquiries))
                                    Total Enquiries: {{$dealer->TotalEnquiries}} <br/>
                                @endif
                                @if(isset($dealerCalls[$dealer->name]))
                                    Total Calls:  {{number_format($dealerCalls[$dealer->name])}}<br>
                                @endif

                                @if(isset($dealerDeals[$dealer->name]))
                                    Total Deals:  {{number_format($dealerDeals[$dealer->name][0]->counter)}}<br><br>
                                @endif

                                <a data-toggle="modal" class="button b2 pointer" data-target="#enquiries{{$dealer->id}}"><i class="fa fa-pie-chart"></i> Enquiry Status</a>

                            </div>
                            <!-- EnwQiries Modal -->
                            <div class="modal fade" id="enquiries{{$dealer->id}}" tabindex="-1" role="dialog" aria-labelledby="enquiries{{$dealer->id}}" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">{{$dealer->name}} Status</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Open: @if(isset($dealer->Open)){{$dealer->Open}}@endif <br/>
                                            Quote Sent: @if(isset($dealer->Quote_Sent)) {{$dealer->Quote_Sent}} @endif <br/>
                                            Sold: @if(isset($dealer->Sold)) {{$dealer->Sold}} @endif <br/>
                                            Pending Payment: @if(isset($dealer->PendingPayment)){{$dealer->PendingPayment}}  @endif<br/>
                                            Paid: @if(isset($dealer->Paid)){{$dealer->Paid}} @endif <br/>
                                            Closed:  @if(isset($dealer->Closed)){{$dealer->Closed}} @endif<br/>
                                            Flagged By Customer:  @if(isset($dealer->MarkedAsSold)){{$dealer->MarkedAsSold}} @endif<br/>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="button b3" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Call Modal -->
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>


@endsection
@section('template_linked_css')
<style>
    .der {
        font-size: 18px;
        font-weight: 900;
    }
    .features{
        font-weight:900;
    }
    .banner{
        text-align: center;
        color: #fff;
        margin-bottom: 40px;
        padding-top: 22px;
        margin-top: -1.5rem;
    }
</style>
    @endsection