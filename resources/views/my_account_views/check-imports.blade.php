@extends('layouts.app')
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@section('template_title')
    Admin Tools - Import script checker
@endsection

@section('content')

    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">Import script info</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / <a href="/my-account/admin-tools">Admin Tools</a> / Import script info</div>

            </div>
        </div>
    </div>

    <section class="admin">
        <div class="container">
            <div class="row">

                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Dealer</th>
                        <th scope="col">CSV file</th>
                        <th scope="col">CSV last uploaded</th>
                        <th scope="col">Database last updated</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($return as $dealer=>$value)

                    <tr>
                        <th scope="row">{{$dealer}}</th>
                        <td>
                            @foreach($value as $file=>$v)
                                {{$file}}<br>
                            @endforeach
                        </td>
                        <td>
                            @foreach($value as $file=>$v)
                                @foreach($v as $k=>$p)
                                    @if($k == 'file_updated')
                                        {{date('d-m-Y', strtotime($p))}}<br>
                                    @endif
                                @endforeach
                            @endforeach
                        </td>
                        <td>
                            @foreach($value as $file=>$v)
                                @foreach($v as $k=>$p)
                                    @if($k == 'db_updated')
                                        {{date('d-m-Y', strtotime($p))}}<br>
                                    @endif
                                @endforeach
                            @endforeach
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>



            </div>
        </div>
    </section>


@endsection


@section('footer_scripts')

@endsection


@section('template_linked_css')

@endsection
