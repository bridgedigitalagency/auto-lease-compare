@extends('layouts.app')
@section('extra_meta_tags')
<meta name="robots" content="noindex">
@endsection
@section('template_title')
Admin Tools
@endsection

@section('content')

    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">Admin Tools</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / Admin Tools</div>

            </div>
        </div>
    </div>

    <section class="admin">
        <div class="container">
            <div class="row">
                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="/my-account/check-import-scripts" style="color:#fff; text-align:center;"><div style="background: #68d155;padding: 44px; padding-left:8px;padding-right:8px;font-size: 20px;border: 1px;border-radius: 10px;"><i class="" aria-hidden="true"></i><br>Check import scripts</div></a>
                </div>

{{--                <div class="col-md-3" style="margin-bottom:20px;">--}}
{{--                    <a href="/my-account/remove-deal" style="color:#fff; text-align:center;"><div style="background: rgb(237, 85, 59);padding: 44px; padding-left:8px;padding-right:8px;font-size: 20px;border: 1px;border-radius: 10px;"><i class="" aria-hidden="true"></i><br>Remove Deal</div></a>--}}
{{--                </div>--}}

{{--                <div class="col-md-3" style="margin-bottom:20px;">--}}
{{--                    <a href="/my-account/disable-dealer" style="color:#fff; text-align:center;"><div style="background: #68d155;padding: 44px; padding-left:8px;padding-right:8px;font-size: 20px;border: 1px;border-radius: 10px;"><i class="" aria-hidden="true"></i><br>Disable Dealer</div></a>--}}
{{--                </div>--}}

                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="/heartbeat" style="color:#fff; text-align:center;"><div style="background: rgb(32, 99, 155);padding: 44px; padding-left:8px;padding-right:8px;font-size: 20px;border: 1px;border-radius: 10px;"><i class="" aria-hidden="true"></i><br>View site stats</div></a>
                </div>

                <div class="col-md-3" style=" margin-bottom:20px;">
                    <a href="/view-emails" style="color: rgb(255, 255, 255); text-align: center;"><div style="background: rgb(71, 209, 192); padding: 44px 8px; font-size: 20px; border: 1px; border-radius: 10px;"><i aria-hidden="true" class="fa fa-envelope"></i><br>View Email Templates</div></a>
                </div>


            </div>
        </div>
    </section>


@endsection


@section('footer_scripts')

@endsection


@section('template_linked_css')

@endsection