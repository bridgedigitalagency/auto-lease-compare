@extends('layouts.app')
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@section('template_title')
    My Pricing
@endsection
@section('content')
    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">My Pricing</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / <a href="/my-account" style="color:#fff;">My Dashboard</a> / My Pricing</div>

            </div>
        </div>
    </div>

    @if(isset($noDealer) && $noDealer == 1)
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-warning" role="alert">
                        You are not authorised to create deals.
                    </div>
                </div>
            </div>
        </div>
    @else
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <form method="GET">
                    <div class="row">
                        @if($database == 'LCV')
                            <a href="/my-account/my-pricing" class="text-center button b3 mb-2 r4 w-100">Edit Car Pricing</a>
                        @else
                            <a href="/my-account/my-pricing/vans" class="text-center button b3 mb-2 r4 w-100">Edit Van Pricing</a>
                        @endif
                    </div>
                    <div class="row"><div class="col-sm-12"><label for="from">Filter By Car</label>
                            <select id="maker" class="form-control mb-3" name="maker" onchange="this.form.submit()">
                                <option value="">All Makes</option>
                                @foreach($sumMakes ?? '' as $sum1)
                                    <option value="{{$sum1->maker}}" @if($sum1->maker == app('request')->input('maker'))selected="selected"@endif>{{$sum1->maker}}</option>
                                @endforeach

                            </select>

                            <select id="model" class="form-control mb-3" name="model" onchange="this.form.submit()">
                                <option value="">All Models</option>
                                @foreach($sumModel as $sumM)
                                    <option value="{{$sumM->model}}" @if($sumM->model == app('request')->input('model'))selected="selected"@endif>{{$sumM->model}}</option>
                                @endforeach
                            </select>

                            <select id="derivative" class="form-control mb-3" name="derivative" onchange="this.form.submit()">
                                <option value="">All Derivatives</option>
                                @foreach($sumDer as $sumD)
                                    <option value="{{$sumD->derivative}}" @if($sumD->derivative == app('request')->input('derivative'))selected="selected"@endif>{{$sumD->derivative}}</option>
                                @endforeach
                            </select>

                            <select id="bodystyle" class="form-control mb-3" name="bodystyle" onchange="this.form.submit()">
                                <option value="">All Body Styles</option>
                                @foreach($sumBody as $sumB)
                                    <option value="{{$sumB->bodystyle}}" @if($sumB->bodystyle == app('request')->input('bodystyle'))selected="selected"@endif>{{$sumB->bodystyle}}</option>
                                @endforeach
                            </select>

                            <select id="transmission" class="form-control mb-3" name="transmission" onchange="this.form.submit()">
                                <option value="">All Transmission</option>
                                @foreach($sumTrans as $sumT)
                                    <option value="{{$sumT->transmission}}" @if($sumT->transmission == app('request')->input('transmission'))selected="selected"@endif>{{$sumT->transmission}}</option>
                                @endforeach
                            </select>

                            <select id="fuel" class="form-control mb-3" name="fuel" onchange="this.form.submit()">
                                <option value="">All Fuel Types</option>
                                @foreach($sumFuel as $sumF)
                                    <option value="{{$sumF->fuel}}" @if($sumF->fuel == app('request')->input('fuel') && $sumF->fuel!='')selected="selected"@endif>{{$sumF->fuel}}</option>
                                @endforeach
                            </select>

                            <select id="doors" class="form-control mb-3" name="doors" onchange="this.form.submit()">
                                <option @if(!app('request')->input('doors'))selected @endif value="">All Doors</option>
                                @foreach($sumDoor as $sumD)
                                    <option value="{{$sumD->doors}}" @if($sumD->doors == app('request')->input('doors') && $sumD->doors !='')selected="selected"@endif>{{$sumD->doors}}</option>
                                @endforeach
                            </select>

                           Filter Lease Info

                            <select id="finance_type" class="form-control mb-3" name="finance_type" onchange="this.form.submit()">
                                <option value="">Select Finance Type</option>
                                    <option value="P" @if(app('request')->input('finance_type') == 'P')selected="selected"@endif>Personal</option>
                                    <option value="B" @if(app('request')->input('finance_type') == 'B')selected="selected"@endif>Business</option>
                            </select>

                            <select id="deposit_value" class="form-control mb-3" name="deposit_value" onchange="this.form.submit()">
                                <option value="">Months Upfront</option>
                                <option value="1" @if(app('request')->input('deposit_value') == '1')selected="selected"@endif>1 Months Upfront</option>
                                <option value="3" @if(app('request')->input('deposit_value') == '3')selected="selected"@endif>3 Months Upfront</option>
                                <option value="6" @if(app('request')->input('deposit_value') == '6')selected="selected"@endif>6 Months Upfront</option>
                                <option value="9" @if(app('request')->input('deposit_value') == '9')selected="selected"@endif>9 Months Upfront</option>
                                <option value="12" @if(app('request')->input('deposit_value') == '12')selected="selected"@endif>12 Months Upfront</option>

                            </select>

                            <select id="term" class="form-control mb-3" name="term" onchange="this.form.submit()">
                                <option value="">Contract Length</option>
                                <option value="12" @if(app('request')->input('term') == '12')selected="selected"@endif>12</option>
                                <option value="24" @if(app('request')->input('term') == '24')selected="selected"@endif>24</option>
                                <option value="36" @if(app('request')->input('term') == '36')selected="selected"@endif>36</option>
                                <option value="48" @if(app('request')->input('term') == '48')selected="selected"@endif>48</option>
                            </select>

                            <select id="annual_mileage" class="form-control mb-3" name="annual_mileage" onchange="this.form.submit()">
                                <option value="">Months Upfront</option>
                                <option value="5000" @if(app('request')->input('annual_mileage') == '5000')selected="selected"@endif>5000</option>
                                <option value="8000"   @if(app('request')->input('annual_mileage') == '8000')selected="selected"@endif>8000</option>
                                <option value="10000" @if(app('request')->input('annual_mileage') == '10000')selected="selected"@endif>10000</option>
                                <option value="12000" @if(app('request')->input('annual_mileage') == '12000')selected="selected"@endif>12000</option>
                                <option value="15000" @if(app('request')->input('annual_mileage') == '15000')selected="selected"@endif>15000</option>
                                <option value="20000" @if(app('request')->input('annual_mileage') == '20000')selected="selected"@endif>20000</option>
                                <option value="30000" @if(app('request')->input('annual_mileage') == '30000')selected="selected"@endif>30000</option>

                            </select>

                        </div>
                        <div class="col-sm-12 mb-3">
                            <a class="button b3" style="margin-top:30px;" href="/my-account/my-pricing" style="width:100%;"> Clear All</a>
                            <a class="button b2" style="margin-top:30px;" href="/my-account/add-price" style="width:100%;"> Add New Deal</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-9">
                @if(count($summaryCap) > 0)
                    @foreach($summaryCap as $sum)
                        <div style="background: #e8e8e8;margin-bottom: 20px;">
                            <div class="row" style="padding: 10px;">
                                <div class="col-md-6 pt-2">
                                    @if(isset($sum->maker))
                                        <h6><strong>{{$sum->maker}} {{$sum->model}}<br/>{{$sum->derivative}}</strong></h6>
                                    @endif
                                    @if(isset($sum->fuel))
                                        <h6 class="features">{{$sum->fuel}} | {{$sum->transmission}} | {{$sum->bodystyle}} |  {{$sum->doors}} Door</h6>
                                    @endif

                                    @if(isset($sum->deposit_value))
                                        <h6>@if($sum->finance_type =='B') BCH @else PCH @endif - {{$sum->deposit_value}} + {{$sum->term-1}} {{$sum->annual_mileage}}k</h6>
                                    @endif
                                </div>
                                <div class="col-md-3 pt-2">
                                    @if(isset($sum->monthly_payment))
                                        <h6>£{{$sum->monthly_payment}} PM @if($sum->finance_type =='P') Inc VAT @else Ex VAT @endif</h6>
                                    @endif
                                    @if(isset($sum->deposit_months))
                                        <h6>£{{$sum->deposit_months}} Initial Rental</h6>
                                    @endif
                                    @if(isset($sum->document_fee))
                                        <h6>£{{number_format($sum->document_fee, 2)}} Admin Fee</h6>
                                    @endif
                                </div>
                                <div class="col-md-3 pt-2">
                                    <button type="button" class="button b2 mb-2 r4 w-100" data-toggle="modal" data-target="#UpdateStatus{{$sum->id}}">
                                        Edit Price
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="UpdateStatus{{$sum->id}}" tabindex="-1" role="dialog" aria-labelledby="UpdateStatus{{$sum->id}}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">Update Price</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Price will automatically update on submission. If the deal is personal please include Inc VAT figures.</p>
                                                    <div class="form-group" data-value="{{$sum->id}}">
                                                        <form method="POST" class="update-price-form" data-value="{{$sum->id}}" action="#">
                                                            {{method_field('PUT')}}
                                                            {{ csrf_field() }}
                                                            <label>Monthly Rental Price</label>
                                                            <input type="hidden" name="cap-id" value="{{$sum->id}}">
                                                            <input class="form-control" name="update-price" id="update-price-{{$sum->id}}"  data-value="{{$sum->id}}" value="{{$sum->monthly_payment}}">
                                                            <label>Initial Rental (Deposit)</label>
                                                            <input class="form-control" name="update-deposit" id="update-deposit-{{$sum->id}}"  data-value="{{$sum->id}}" value="{{$sum->deposit_months}}">
                                                            <input type="hidden" name="database" value="{{$database}}">
                                                            <input type="submit" value="Submit" name="submit" class="submit button b2 mt-3" id="submit-{{$sum->id}}" />
                                                        </form>
                                                        <div class="success-{{$sum->id}}"></div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button b3" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else

                    <div class="alert alert-warning" role="alert">
                        You currently have no deals live.
                    </div>

                @endif
            </div>

        </div>
    </div>
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

<script>
    $(function () {
        $("body").on('submit', '.update-price-form', function (e) {
            e.preventDefault();
            var formInfo = $(this).serialize();
            var capId = $(this).find('input[name="cap-id"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'get',
                url: '{{route('update-price')}}',
                data: formInfo,
            }).done(function(response) {
                $('div.success-' + capId).html('Price Updated').delay(9000).fadeOut();
            }).fail(function() {

            });
        });
    });
</script>

@endsection
@section('template_linked_css')
<style>
    .der {
        font-size: 18px;
        font-weight: 900;
    }
    .features{
        font-weight:900;
    }
    .banner{
        text-align: center;
        color: #fff;
        background: #e74c3c;
        margin-bottom: 40px;
        padding-top: 22px;
        margin-top: -1.5rem;
    }
</style>
    @endsection