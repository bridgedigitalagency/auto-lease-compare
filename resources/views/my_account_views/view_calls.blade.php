@extends('layouts.app')
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@section('template_title')
    My Call Log
@endsection

@section('content')

    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">My Call Log</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / <a href="/my-account" style="color:#fff;">My Dashboard</a> / <a href="/my-account/enquiries" style="color:#fff;">Enquiries</a> / Call Log</div>
            </div>
        </div>
    </div>
    <div class="container">

        @if(Auth::check() && Auth::user()->hasRole(['admin']) )
            <div class="row">
                <div class="col-sm-8">
{{--                    <h2>Filter By Dealer</h2>--}}
{{--                    <p>--}}
{{--                    <select name="dealers" id="dealerSelect">--}}
{{--                        <option>--Please Select--</option>--}}
{{--                        @foreach($dealer as $deal)--}}
{{--                            <option value="{{$deal['id']}}">{{$deal['name']}}</option>--}}
{{--                        @endforeach--}}
{{--                    </select></p>--}}
                </div>
                <div class="col-sm-4 text-right">
                    <p><a href="/my-account/enquiries/call-log/export/" class="btn btn-primary">Export All Calls</a></p>
                </div>
            </div>
        @endif

        @if(Auth::check() && Auth::user()->hasRole(['dealer']) )
        <div class="row"><div class="col-sm-12"><div class="alert alert-info" role="alert">
        <div class="row">

            <div class="col-sm-4">
                Calls this month
            </div>
            <div class="col-sm-4">
                Calls last month
            </div>
            <div class="col-sm-4">
                Total Calls
            </div>

        </div>
        <div class="row">
            <div class="col-sm-4">
                {{$callsThisMonth}}
            </div>
            <div class="col-sm-4">
                {{$callsLastMonth}}
            </div>
            <div class="col-sm-4">
                {{$totalCalls}}
            </div>
        </div>
                </div></div></div>
        @endif




    @if(Auth::check() && Auth::user()->hasRole(['admin','dealer']) )
            <table class="table table-striped calls">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Phone Number</th>
                    @if(Auth::check() && Auth::user()->hasRole('admin'))
                        <th scope="col">Dealer Number</th>
                    @endif

                    @if(app('request')->input('dateorder') != null)

                        @if(app('request')->input('dateorder')=='desc')
                            <th scope="col"><a href="?s=1&dateorder=asc">Call Date / Time</a></th>
                        @else
                            <th scope="col"><a href="?s=1&dateorder=desc">Call Date / Time</a></th>
                        @endif
                    @else
                        <th scope="col"><a href="?s=1&dateorder=desc">Call Date / Time</a></th>
                    @endif

                    @if(app('request')->input('duration') != null)

                        @if(app('request')->input('duration')=='desc')
                            <th scope="col"><a href="?s=1&duration=asc">Call Duration</a></th>
                        @else
                            <th scope="col"><a href="?s=1&duration=desc">Call Duration</a></th>
                        @endif
                    @else
                        <th scope="col"><a href="?s=1&duration=desc">Call Duration</a></th>
                    @endif

                    @if(app('request')->input('status') != null)

                        @if(app('request')->input('status')=='desc')
                            <th scope="col"><a href="?s=1&status=asc">Call Status</a></th>
                        @else
                            <th scope="col"><a href="?s=1&status=desc">Call Status</a></th>
                        @endif
                    @else
                        <th scope="col"><a href="?s=1&status=desc">Call Status</a></th>
                    @endif

                </tr>
                </thead>
                <tbody>
            @foreach($calls as $call)
                @if(Auth::check() && Auth::user()->hasRole('admin'))
                    @include('partials.calls-admin')
                @elseif(Auth::check() && Auth::user()->hasRole('dealer'))
                    @include('partials.calls-dealer')
                @endif
            @endforeach
                </tbody>
            </table>
            {{ $calls->appends($_GET)->links('partials.pagination') }}

    @endif
    </div>


@endsection


@section('footer_scripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()

            $("body").on('change', '#dealerSelect', function (e) {

            });
        });
    </script>
@endsection


@section('template_linked_css')
    <style>
        .calls th a {
            color: #fff;
        }
        .product-listing-m {
            margin: 0 auto 10px;
            overflow: hidden;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
            border-radius: 15px;
            transition-duration: 0.3s;
            -moz-transition-duration: 0.3s;
            -o-transition-duration: 0.3s;
            -webkit-transition-duration: 0.3s;
            -ms-transition-duration: 0.3s;
            padding: 10px;
        }
    </style>
@endsection