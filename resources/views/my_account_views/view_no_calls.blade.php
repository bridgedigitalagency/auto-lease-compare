@extends('layouts.app')
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@section('template_title')
    My Call Log
@endsection

@section('content')

    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">My Call Log</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / <a href="/my-account" style="color:#fff;">My Dashboard</a> / <a href="/my-account/enquiries" style="color:#fff;">Enquiries</a> / Call Log</div>
            </div>
        </div>
    </div>
    <div class="container">

        <p>Sorry, there are no calls to display. Please contact a member of our team if you think this is an error.
        </p>

    </div>


@endsection


@section('footer_scripts')
@endsection


@section('template_linked_css')
    <style>
        .product-listing-m {
            margin: 0 auto 10px;
            overflow: hidden;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
            border-radius: 15px;
            transition-duration: 0.3s;
            -moz-transition-duration: 0.3s;
            -o-transition-duration: 0.3s;
            -webkit-transition-duration: 0.3s;
            -ms-transition-duration: 0.3s;
            padding: 10px;
        }
    </style>
@endsection