@extends('layouts.app')
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@section('template_title')
    My Dashboard
@endsection
@section('content')
    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">My Dashboard</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / My Dashboard</div>

            </div>
        </div>
    </div>
    <section class="dealers account myAccountLinks">
        <div class="container" style="margin-top: 20px;margin-bottom:20px;">
            <div class="row">

                @role('admin')
                @include('my_account_views/account-admin')
                @endrole

                @role('dealer')
                @include('my_account_views/account-dealer')
                @endrole

                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="/profile/{{Auth::user()->name}}" style="color:#fff; text-align:center;"><div style="background: #174f5f;"><i class="fa fa-cogs" aria-hidden="true"></i><br>My Profile</div></a>
                </div>
                <div class="col-xs-6 col-md-3" style="margin-bottom:20px;">
                    <a href="/my-account/enquiries" style="color:#fff; text-align:center;"><div style="background: #3ca3ae;"><i class="fa fa-car" aria-hidden="true"></i><br>My Enquiries</div></a>
                </div>

                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="/my-account/garage" style="color:#fff; text-align:center;"><div style="background: #ca3ad1;"><i class="fa fa-building" aria-hidden="true"></i><br>My Garage</div></a>
                </div>

                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="/logout" style="color:#fff; text-align:center;"><div style="background: #ed553b;"><i class="fa fa-sign-out" aria-hidden="true"></i><br>Logout</div></a>
                </div>
            </div>
            @role('user')<br>
            <h1>Other Services</h1>

            <h2>Vehicle Insurance</h2>
            <div class="row">
                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="https://clk.omgt1.com/?PID=35050&AID=2136424" target="_blank" style="color:#fff; text-align:center;"><div style="background: #3ca3ae;"><i class="fa fa-car"></i><br>Compare Car Insurance</div></a>
                </div>

                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="https://clk.omgt1.com/?PID=35052&AID=2136424" target="_blank" style="color:#fff; text-align:center;"><div style="background: #3ca3ae;"><i class="fa fa-car"></i><br>Compare Van Insurance</div></a>
                </div>
                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="https://www.motoreasy.com/gap-quote/step/1?utm_medium=affiliate&utm_campaign=organic&utm_source=autoleasecompare" target="_blank" style="color:#fff; text-align:center;"><div style="background: #3ca3ae;"><i class="fa fa-car"></i><br>GAP Insurance</div></a>
                </div>
                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="https://www.motoreasy.com/tyre-quote/step/1?utm_medium=affiliate&utm_campaign=organic&utm_source=autoleasecompare" target="_blank" style="color:#fff; text-align:center;"><div style="background: #3ca3ae;"><i class="fa fa-car"></i><br>Tyre Insurance</div></a>
                </div>
                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="https://www.motoreasy.com/alloy-wheel-quote/step/1?utm_medium=affiliate&utm_campaign=organic&utm_source=autoleasecompare" target="_blank" style="color:#fff; text-align:center;"><div style="background: #3ca3ae;"><i class="fa fa-car"></i><br>Alloy Wheel Insurance</div></a>
                </div>
                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="https://www.motoreasy.com/smart-repair-quote/step/1?utm_medium=affiliate&utm_campaign=organic&utm_source=autoleasecompare" target="_blank" style="color:#fff; text-align:center;"><div style="background: #3ca3ae;"><i class="fa fa-wrench"></i><br>Cosmetic Repair Insurance</div></a>
                </div>
            </div>

            <h2>Credit Score & Sell my Vehicle</h2>
            <div class="row row-eq-height">
                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="https://secure.uk.rspcdn.com/xprr/red/PID/2672" target="_blank" style="color:#fff; text-align:center;"><div style="background: #00a74e;"><i class="fa fa-tachometer-alt"></i><br>My Credit Report &amp; Score</div></a>
                </div>
                <div class="col-md-3 sellMyCar" style="margin-bottom:20px;">
                    <a href="/sell-your-car" target="_blank" style="color:#fff; text-align:center;"><div style="background: #00a74e; padding: 40px;"><i class="fa fa-car"></i><br>Sell my car</div></a>
                </div>
            </div>
            <h2>Vehicle Repairs</h2>
            <div class="row">

                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="https://autoleasecompare.motoreasy.com/car-servicing" target="_blank" style="color:#fff; text-align:center;"><div style="background: #FBBB33;"><i class="fa fa-wrench"></i><br>Car Service</div></a>
                </div>
                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="https://www.motoreasy.com/quotes/step/1?utm_medium=affiliate&utm_campaign=organic&utm_source=autoleasecompare" target="_blank" style="color:#fff; text-align:center;"><div style="background: #FBBB33;"><i class="fa fa-car"></i><br>Car Warranty</div></a>
                </div>
                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="https://www.motoreasy.com/car-warranty/electric-vehicles?utm_medium=affiliate&utm_campaign=organic&utm_source=autoleasecompare" target="_blank" style="color:#fff; text-align:center;"><div style="background: #FBBB33;"><i class="fa fa-bolt"></i><br>EV / Hybrid Warranty</div></a>
                </div>

                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="https://autoleasecompare.motoreasy.com/car/mot" target="_blank" style="color:#fff; text-align:center;"><div style="background: #FBBB33;"><i class="mot-icon"></i><br>MOT Test</div></a>
                </div>

                <div class="col-md-3" style="margin-bottom:20px;">
                    <a href="/vehicle-repair" style="color:#fff; text-align:center;"><div style="background: #FBBB33;"><i class="fa fa-wrench"></i><br>Vehicle Repair</div></a>
                </div>

            </div>
            @endrole
        </div>
    </section>
@endsection