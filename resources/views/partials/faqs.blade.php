<div id="accordion">
    @foreach($faqs as $faq)
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <span class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="false" aria-controls="collapse{{$faq->id}}">

                        {{$faq->question}}
                    </span>
                </h5>
            </div>

            <div id="collapse{{$faq->id}}" class="collapse" aria-labelledby="heading{{$faq->id}}" data-parent="#accordion">
                <div class="card-body">
                    {!!$faq->answer!!}
                </div>
            </div>
        </div>
    @endforeach

</div>
<br>
<p>If your questions are not included in the above, please send us an email to <a href="mailto:info@autoleasecompare.com">info@autoleasecompare.com</a> and one of our friendly team will be happy to help you.</p>