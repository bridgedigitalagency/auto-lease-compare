@php
    $reqArr = array();
    if(is_array(app('request')->input('fuel'))) {
        $reqArr = app('request')->input('fuel');
    }
@endphp
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a class="fuelFilterHeader mb-3 back"><strong><i class="fa fa-angle-double-left"></i> Back</strong></a>
            <strong class="mb-4">Select Fuel Type</strong>
            <select name="fuel[]" id="mobileFuelSelect" class="mobileFuel  mt-2" multiple>
                @foreach($fuels as $fuel)
                    <option @php if(in_array($fuel->fuel, $reqArr)) {echo "selected='selected'";} @endphp value="{{$fuel->fuel}}">{{$fuel->fuel}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>