@if( app('request')->input('e') == '1')

    <div class="row">
        <div class="col-md-6 col-xs-6">
            <div class="select">
                <select id="pricemin" class="w-100 form-control mb-2 pricing_select" name="pricemin">
                    <option value="">Min Price</option>
                    <option value="100">£100 p/m</option>
                    <option value="125">£125 p/m</option>
                    <option value="150">£150 p/m</option>
                    <option value="175">£175 p/m</option>
                    <option value="200">£200 p/m</option>
                    <option value="225">£225 p/m</option>
                    <option value="250">£250 p/m</option>
                    <option value="275">£275 p/m</option>
                    <option value="300">£300 p/m</option>
                    <option value="325">£325 p/m</option>
                    <option value="350">£350 p/m</option>
                    <option value="375">£375 p/m</option>
                    <option value="400">£400 p/m</option>
                    <option value="425">£425 p/m</option>
                    <option value="450">£450 p/m</option>
                    <option value="475">£475 p/m</option>
                    <option value="500">£500 p/m</option>
                    <option value="550">£550 p/m</option>
                    <option value="600">£600 p/m</option>
                    <option value="650">£650 p/m</option>
                    <option value="700">£700 p/m</option>
                    <option value="750">£750 p/m</option>
                    <option value="800">£800 p/m</option>
                    <option value="900">£900 p/m</option>
                    <option value="1000">£1000 p/m</option>
                    <option value="1100">£1100 p/m</option>
                    <option value="1200">£1200 p/m</option>
                    <option value="1300">£1300 p/m</option>
                    <option value="1400">£1400 p/m</option>
                    <option value="1500">£1500 p/m</option>
                    <option value="1750">£1750 p/m</option>
                    <option value="2000">£2000 p/m</option>
                    <option value="2250">£2250 p/m</option>
                    <option value="2500">£2500 p/m</option>
                    <option value="2750">£2750 p/m</option>
                    <option value="3000">£3000 p/m</option>
                    <option value="3500">£3500 p/m</option>
                    <option value="4000">£4000 p/m</option>
                    <option value="4500">£4500 p/m</option>
                    <option value="5000">£5000 p/m</option>
                </select>
            </div>
        </div>

        <div class="col-md-6 col-xs-6 mb-2">
            <div class="select">
                <select id="pricemax" class="w-100 form-control mb-2 pricing_select" name="pricemax">
                    <option value="">Max Price</option>
                    <option value="100">£100 p/m</option>
                    <option value="125">£125 p/m</option>
                    <option value="150">£150 p/m</option>
                    <option value="175">£175 p/m</option>
                    <option value="200">£200 p/m</option>
                    <option value="225">£225 p/m</option>
                    <option value="250">£250 p/m</option>
                    <option value="275">£275 p/m</option>
                    <option value="300">£300 p/m</option>
                    <option value="325">£325 p/m</option>
                    <option value="350">£350 p/m</option>
                    <option value="375">£375 p/m</option>
                    <option value="400">£400 p/m</option>
                    <option value="425">£425 p/m</option>
                    <option value="450">£450 p/m</option>
                    <option value="475">£475 p/m</option>
                    <option value="500">£500 p/m</option>
                    <option value="550">£550 p/m</option>
                    <option value="600">£600 p/m</option>
                    <option value="650">£650 p/m</option>
                    <option value="700">£700 p/m</option>
                    <option value="750">£750 p/m</option>
                    <option value="800">£800 p/m</option>
                    <option value="900">£900 p/m</option>
                    <option value="1000">£1000 p/m</option>
                    <option value="1100">£1100 p/m</option>
                    <option value="1200">£1200 p/m</option>
                    <option value="1300">£1300 p/m</option>
                    <option value="1400">£1400 p/m</option>
                    <option value="1500">£1500 p/m</option>
                    <option value="1750">£1750 p/m</option>
                    <option value="2000">£2000 p/m</option>
                    <option value="2250">£2250 p/m</option>
                    <option value="2500">£2500 p/m</option>
                    <option value="2750">£2750 p/m</option>
                    <option value="3000">£3000 p/m</option>
                    <option value="3500">£3500 p/m</option>
                    <option value="4000">£4000 p/m</option>
                    <option value="4500">£4500 p/m</option>
                    <option value="5000">£5000 p/m</option>
                </select>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6 col-xs-6 mb-2">
            <div class="select">
                <select id="bodystyle" class="w-100 form-control pricing_select" name="bodystyle[]">
                    @if(isset($bodystyles))
                        <option value="">Any Body Type</option>
                        @foreach($bodystyles as $bodystyle)
                            <option value="{{$bodystyle->bodystyle}}" @if(is_array(app('request')->input('bodystyle')) && in_array($bodystyle->bodystyle, app('request')->input('bodystyle'))) selected="selected" @endif>{{$bodystyle->bodystyle}}</option>
                        @endforeach
                    @else
                        <option value="" selected="">Any Body Type</option>
                        <option id='body-Cabriolet' value="cabriolet">Cabriolet</option>
                        <option id='body-Convertible' value="convertible">Convertible</option>
                        <option id='body-Coupe' value="coupe">Coupe</option>
                        <option id='body-Estate' value="estate">Estate</option>
                        <option id='body-Hardtop' value="hardtop">Hardtop</option>
                        <option id='body-Hatchback' value="hatchback">Hatchback</option>
                        <option id='body-Roadster' value="roadster">Roadster</option>
                        <option id='body-Saloon' value="saloon">Saloon</option>
                        <option id='body-Soft_Top' value="soft-top">Soft-Top</option>
                        <option id='body-Station_Wagon' value="station wagon">Station Wagon</option>
                        <option id='body-Tourer' value="tourer">Tourer</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <select name="transmission" id="transmission" class="w-100 form-control mb-2">
                @if(isset($transmission))
                    <option value="">Any Transmission</option>
                    @foreach($transmission as $trans)
                        <option value="{{$trans->transmission}}" @if(app('request')->input('transmission') == $trans->transmission) selected="selected" @endif>{{$trans->transmission}}</option>
                    @endforeach
                @else
                    <option value="">Select Transmission</option>
                    <option id="trans-manual" value="Manual">Manual</option>
                    <option id=trans-auto" value="Automatic">Automatic</option>
                @endif
            </select>
        </div>

    </div>
    <div class="row">

        <div class="col-md-6">
            <select name="fuel" id="fuel" class="w-100 form-control mb-2">
                <option value="">Select Fuel Type</option>
                @if(isset($fuel))
                    @foreach($fuel as $fuels)
                        @if(isset($fuels->fuel))
                            @if(!is_null($fuels->fuel))
                                @if($fuels->fuel != 'Petrol LPG')
                                    <option value="{{$fuels->fuel}}" @if(app('request')->input('fuel') == $fuels->fuel) selected="selected" @endif>{{$fuels->fuel}}</option>
                                @endif
                            @endif
                        @endif
                    @endforeach
                @else
                    <option value="">Select Fuel Type</option>
                    <option value="Petrol">Petrol</option>
                    <option value="Diesel">Diesel</option>
                    <option value="Petrol Electric Hybrid">Petrol Electric Hybrid</option>
                    <option value="Electric">Electric</option>
                    <option value="Petrol PlugIn Elec Hybrid">Petrol PlugIn Elec Hybrid</option>
                    <option value="Diesel Electric Hybrid">Diesel Electric Hybrid</option>
                    <option value="PlugIn Elec Hybrid">PlugIn Elec Hybrid</option>
                @endif
            </select>
        </div>
        <div class="col-md-6">
            <?php
            $engineSize = array();
            if(!is_null(app('request')->input('engineSize') ))
            {
                if(!is_array(app('request')->input('engineSize'))) {
                    $engineSize = array(app('request')->input('engineSize'));
                }
            }
            ?>
@php

    foreach($enginesizes as $v) {
        foreach($v as $eng) {
            $engines[] = $eng;
        }
    }
    if(isset($engines)) {
        $enginesizes = $engines;
    }
@endphp
            <select id="engineSize" name="engineSize" class="w-100 form-control mb-2">
                <option value="0">Any Engine Size</option>
                @if(isset($enginesizes))
                    @php

                        $display = false;
                        $check = array('0','0.1','0.2','0.3','0.4','0.5','0.6','0.7','0.8','0.9');
                        foreach($check as $c) {
                            if(in_array($c,$enginesizes)) {
                                $display = true;
                            }
                        }
                        if($display == true) {
                            echo "<option ";
                            if(in_array('0-1.0', $engineSize)) {echo "selected='selected' ";}
                            echo "value=\"0-1.0\">Less than 1.0L</option>";
                        }

                        $display = false;
                        $check = array('1.0','1.1','1.2','1.3','1.3');
                        foreach($check as $c) {
                            if(in_array($c,$enginesizes)) {
                                $display = true;
                            }
                        }
                        if($display == true) {
                            echo "<option ";
                            if(in_array('1.0-1.3', $engineSize)) {echo "selected='selected' ";}
                            echo "value='1.0-1.3'>1.0L-1.3L</option>";
                        }

                        $display = false;
                        $check = array('1.4','1.5','1.6');
                        foreach($check as $c) {
                            if(in_array($c,$enginesizes)) {
                                $display = true;
                            }
                        }

                        if($display == true) {
                            echo "<option ";
                            if(in_array('1.4-1.6', $engineSize)) {echo "selected='selected' ";}
                            echo "value='1.4-1.6'>1.4L-1.6L</option>";
                        }

                        $display = false;
                        $check = array('1.7','1.8','1.9');
                        foreach($check as $c) {
                            if(in_array($c,$enginesizes)) {
                                $display = true;
                            }
                        }
                        if($display == true) {
                            echo "<option ";
                            if(in_array('1.7-1.9', $engineSize)) {echo "selected='selected' ";}
                            echo "value='1.7-1.9'>1.7L-1.9L</option>";
                        }

                        $display = false;
                        $check = array('2.0','2.1','2.2', '2.3','2.4','2.5');
                        foreach($check as $c) {
                            if(in_array($c,$enginesizes)) {
                                $display = true;
                            }
                        }
                        if($display == true) {
                            echo "<option ";
                            if(in_array('2.0-2.5', $engineSize)) {echo "selected='selected' ";}
                            echo "value='2.0-2.5'>2.0L-2.5L</option>";
                        }

                        $display = false;
                        $check = array('2.6','2.7','2.8', '2.9');
                        foreach($check as $c) {
                            if(in_array($c,$enginesizes)) {
                                $display = true;
                            }
                        }
                        if($display == true) {
                            echo "<option ";
                            if(in_array('2.6-2.9', $engineSize)) {echo "selected='selected' ";}
                            echo "value='2.6-2.9'>2.6L-2.9L</option>";
                        }
                        $display = false;
                        $check = array('3.0','3.1','3.2', '3.3','3.4','3.5','3.6','3.7','3.8','3.9');
                        foreach($check as $c) {
                            if(in_array($c,$enginesizes)) {
                                $display = true;
                            }
                        }
                        if($display == true) {
                            echo "<option ";
                            if(in_array('3.0-3.9', $engineSize)) {echo "selected='selected' ";}
                            echo "value='3.0-3.9'>3.0L-3.9L</option>";
                        }

                        $display = false;
                        $check = array('4.0','4.1','4.2', '4.3','4.4','4.5','4.6','4.7','4.8','4.9');
                        foreach($check as $c) {
                            if(in_array($c,$enginesizes)) {
                                $display = true;
                            }
                        }
                        if($display == true) {
                            echo "<option ";
                            if(in_array('4.0-4.9', $engineSize)) {echo "selected='selected' ";}
                            echo "value='4.0-4.9'>4.0L-4.9L</option>";
                        }

                        $display = false;
                        $check = array('5.0','5.1','5.2', '5.3','5.4','5.5','5.6','5.7','5.8','5.9','6.0','6.1','6.2', '6.3','6.4','6.5','6.6','6.7','6.8','6.9');
                        foreach($check as $c) {
                            if(in_array($c,$enginesizes)) {
                                $display = true;
                            }
                        }
                        if($display == true) {
                            echo "<option ";
                            if(in_array('5.0+', $engineSize)) {echo "selected='selected' ";}
                            echo "value='5.0+'>5.0L and over</option>";
                        }
                    @endphp
                @else
                    <option value="0">Any Engine Size</option>
                    <option value="0-1.0">Less than 1.0L</option>
                    <option value="1.0-1.3">1.0L-1.3L</option>
                    <option value="1.4-1.6">1.4L-1.6L</option>
                    <option value="1.7-1.9">1.7L-1.9L</option>
                    <option value="2.0-2.5">2.0L-2.5L</option>
                    <option value="2.6-2.9">2.6L-2.9L</option>
                    <option value="3.0-3.9">3.0L-3.9L</option>
                    <option value="4.0-4.9">4.0L-4.9L</option>
                    <option value="5.0+">5.0L and over</option>
                @endif
            </select>
        </div>
    </div>

@else
    <option value="" selected="">Any Model</option>
    @foreach($models as $model)
        <option value="{{$model->range}}">{{$model->range}}</option>
    @endforeach
@endif