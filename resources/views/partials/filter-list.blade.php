<form method="GET" id="filterList">
@if(strpos(Request::url(), 'vans'))
@php $database = 'LCV'; @endphp
@if($finance_type == 'P')
        <input type="hidden" name="finance_type_show" value="P" class="finance_type_show">
@else
        <input type="hidden" name="finance_type_show" value="B" class="finance_type_show">
@endif
@else
@if(strpos(Request::url(), 'adverse') || strpos(Request::url(), 'hybrid') || strpos(Request::url(), 'performance'))
    @if($finance_type != 'B')
        <input type="hidden" name="finance_type_show" value="P" class="finance_type_show">
    @else
        <input type="hidden" name="finance_type_show" value="B" class="finance_type_show">
    @endif
    <div class="row">

        <div class="col-md-6">
            @if($finance_type != 'B')
                <a href="#" class="leaseFilterType button b3" id="personalLeaseFilterDesktop">Personal</a>
            @else
                <a href="#" class="leaseFilterType button b7" id="personalLeaseFilterDesktop">Personal</a>
            @endif

        </div>
        <div class="col-md-6">
            @if($finance_type == 'B')
                <a href="#" class="leaseFilterType button b3" id="businessLeaseFilterDesktop">Business</a>
            @else
                <a href="#" class="leaseFilterType button b7" id="businessLeaseFilterDesktop">Business</a>
            @endif
        </div>
    </div>

{{--        <div class="row">--}}
{{--            <div class="col-md-6">--}}
{{--                <input type="radio" value="P" name="finance_type_show" class="finance_type_show" @if($finance_type != 'B') checked @endif> Personal--}}
{{--            </div>--}}
{{--            <div class="col-md-6">--}}
{{--                <input type="radio" value="B" name="finance_type_show" class="finance_type_show" @if($finance_type == 'B') checked @endif> Business--}}
{{--            </div>--}}
{{--        </div>--}}
@endif
@endif

<div id="makeOptions" class="filterOptionGroup">
<a data-toggle="modal" data-target="#makeModal" class="makeFilterHeader">
    <strong>Make <span class="plussign">+</span></strong>
</a>
<div id="makeSelected">

    @if(is_array(app('request')->input('maker')))
        @foreach(app('request')->input('maker') as $maker)
            <a data-toggle="modal" data-target="#makeModal" class='makerSelectedLink' id='{{$maker}}' href='#'><img src="/images/manufacturers/{{strtolower($maker)}}.png"></a>
        @endforeach
    @endif

</div>
<div class="modal fade bd-example-modal-lg" id="makeModal" tabindex="-1" role="dialog"
     aria-labelledby="makeModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Manufacturers</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="filterMakes">
                    <div class="row">
                        @include('partials.filter-filters-make')
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b3" data-dismiss="modal">Done</button>
            </div>
        </div>
    </div>
</div>
</div>
@if(is_array(app('request')->input('maker')))
<div id="modelOptions" class="filterOptionGroup">

    <a data-toggle="modal" data-target="#modelModal" class="modelFilterHeader"><strong>Models <span class="plussign">+</span></strong>
    </a>
    <div id="modelSelected">
        @if(is_array(app('request')->input('range')))
            @foreach(app('request')->input('range') as $range)

                @php
                    $rangesS = serialize($ranges);
                @endphp

                @if(strpos($rangesS, $range))
                    <a data-toggle="modal" data-target="#modelModal" class='makerSelectedLink' id='{{$range}}' href='#'>{{$range}}<span class='rangeDelete'>X</span></a><br>
                @endif
            @endforeach
        @endif
    </div>

    <div class="modal fade bd-example-modal-lg" id="modelModal" tabindex="-1" role="dialog"
         aria-labelledby="makeModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Models</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
<div class="filterRanges">
                        <div class="row">
                            <div class="col-sm-10">
                                <ul id="rangeSelectContainer">
                                    @include('partials.filter-filters-range')
                                </ul>
                            </div>
                            <div class="col-sm-2 margin-top-2">
                                <button type="button" class="button b3" data-dismiss="modal">Done</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endif

@if(isset($database) && $database !='LCV')
    @if(is_array(app('request')->input('maker')) && is_array(app('request')->input('range')))
    <div id="trimOptions" class="filterOptionGroup" @if(isset($trims) && is_array($trims) && count($trims) > 0) style="display: block!important" @endif>

        <a data-toggle="modal" data-target="#trimModal" class="trimFilterHeader"><strong>Model Variant<span class="plussign">+</span></strong>
        </a>
        <div id="trimSelected">
            @if(is_array(app('request')->input('trim')))
                @foreach(app('request')->input('trim') as $trim)

                    @php
                        $rawTrim = $trim;
                        $explode = explode('_', $trim);
                        $range = $explode[0];
                        $trim = $explode[1];
                    @endphp
		    
<!-- <a data-toggle="modal" data-target="#trimModal" class='makerSelectedLink' id='{{$trim}}' href='#'>{{$range}} {{$trim}}<span class='rangeDelete'>X</span></a><br>-->
@if(isset($selTrim) && is_array($selTrim) && in_array($rawTrim, $selTrim))
                        <a data-toggle="modal" data-target="#trimModal" class='makerSelectedLink' id='{{$trim}}' href='#'>{{$range}} {{$trim}}<span class='rangeDelete'>X</span></a><br>
                    @endif
                @endforeach
            @endif
        </div>

        <div class="modal fade bd-example-modal-lg" id="trimModal" tabindex="-1" role="dialog"
             aria-labelledby="trimModal" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Model Variant</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
<div class="row">
                            <div class="col-sm-10">
                                <div class="filterTrims">
                                    <ul id="trimSelectContainer">
                                        @include('partials.filter-filters-trim')
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-2 margin-top-2 text-right">
                                <button type="button" class="button b3" data-dismiss="modal">Done</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endif
<div id="fuelOptions" class="filterOptionGroup">

<a data-toggle="modal" data-target="#fuelModal" class="fuelFilterHeader"><strong>Fuel Type <span class="plussign">+</span></strong>
</a>
<div id="fuelSelected">
    @if(is_array(app('request')->input('fuel')))
        @foreach(app('request')->input('fuel') as $fuel)
            <a data-toggle="modal" data-target="#fuelModal" class='fuelSelectedLink' id='{{$fuel}}' href='#'>{{$fuel}}<span class='rangeDelete'>X</span></a><br>
        @endforeach
    @endif
</div>

<div class="modal fade bd-example-modal-lg" id="fuelModal" tabindex="-1" role="dialog"
     aria-labelledby="fuelModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Select Fuel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="filterFuels">
                    <div class="row justify-content-md-center" id="fuelSelectContainer">
                        @include('partials.filter-filters-fuel')
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b3" data-dismiss="modal">Done</button>
            </div>
        </div>
    </div>
</div>
</div>

<div id="transmissionOptions" class="filterOptionGroup">
<a data-toggle="modal" data-target="#transmissionModal" class="transmissionFilterHeader">
    <strong>Transmission <span class="plussign">+</span></strong>
</a>
<div id="transmissionSelected">
    @if(is_array(app('request')->input('transmission')))
        @foreach(app('request')->input('transmission') as $transmission)
            <a data-toggle="modal" data-target="#transmissionModal" class='transmissionSelectedLink' id='{{$transmission}}' href='#'>{{$transmission}}<span class='rangeDelete'>X</span></a><br>
        @endforeach
    @endif
</div>

<div class="modal fade bd-example-modal-lg" id="transmissionModal" tabindex="-1" role="dialog"
     aria-labelledby="transmissionModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Transmission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="filterMakes">
                    <div class="row justify-content-md-center" id="transmissionSelectContainer">
                        @include('partials.filter-filters-transmission')
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b3" data-dismiss="modal">Done</button>
            </div>
        </div>
    </div>
</div>
</div>


<div id="bodystyleOptions" class="filterOptionGroup">

<a data-toggle="modal" data-target="#bodystyleModal" class="bodystyleFilterHeader">
    <strong>Body Style <span class="plussign">+</span></strong>
</a>
<div id="bodystyleSelected">
    @if(is_array(app('request')->input('bodystyle')))
        @foreach(app('request')->input('bodystyle') as $bodystyle)
            <a data-toggle="modal" data-target="#bodystyleModal" class='bodystyleSelectedLink' id='{{$bodystyle}}' href='#'>{{$bodystyle}}<span class='rangeDelete'>X</span></a><br>
        @endforeach
    @endif
</div>

<div class="modal fade bd-example-modal-lg" id="bodystyleModal" tabindex="-1" role="dialog"
     aria-labelledby="bodystyleModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Body Style</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" id="bodystyleSelectContainer">
                    @include('partials.filter-filters-bodystyle')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b3" data-dismiss="modal">Done</button>
            </div>
        </div>
    </div>
</div>
</div>

@if ($listing_type != "H")
{{--        Engine sizes not needed for hybrid --}}
<div id="enginesizeOptions" class="filterOptionGroup">

    <a data-toggle="modal" data-target="#enginesizeModal" class="enginesizeFilterHeader">
        <strong>Engine Size<span class="plussign">+</span></strong>
    </a>
    <div id="enginesizeSelected">
        @php

            $engineSize = array();
            if(!is_null(app('request')->input('engineSize') ))
            {
                $engineSize = app('request')->input('engineSize');
            }

        @endphp
        @if(is_array($engineSize))

            @foreach($engineSize as $engine)
                <a data-toggle="modal" data-target="#enginesizeModal" class='enginesizeSelectedLink' id='{{$engine}}' href='#'>{{$engine}}<span class='rangeDelete'>X</span></a><br>
            @endforeach
        @else

            @if($engineSize != "0" && !is_null($engineSize))
                <a data-toggle="modal" data-target="#enginesizeModal" class='enginesizeSelectedLink' id='{{$engineSize}}' href='#'>{{$engineSize}}<span class='rangeDelete'>X</span></a><br>
            @endif
        @endif

    </div>

    <div class="modal fade bd-example-modal-lg" id="enginesizeModal" tabindex="-1" role="dialog"
         aria-labelledby="enginesizeModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Engine Size</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row justify-content-md-center" id="enginesizeSelectContainer">
                        <div class="col-sm-9">

                            <select class="w-100 form-control" id="engineSize" name="engineSize[]" multiple>
                                @if(isset($enginesizes))

                                    @php

                                        $display = false;
                                        $check = array('0','0.1','0.2','0.3','0.4','0.5','0.6','0.7','0.8','0.9');
                                        foreach($check as $c) {
                                            if(in_array($c,$enginesizes)) {
                                                $display = true;
                                            }
                                        }
                                        if($display == true) {
                                            echo "<option ";
                                            if(in_array('0-1.0', $engineSize)) {echo "selected='selected' ";}
                                            echo "value=\"0-1.0\">Less than 1.0L</option>";
                                        }

                                        $display = false;
                                        $check = array('1.0','1.1','1.2','1.3','1.3');
                                        foreach($check as $c) {
                                            if(in_array($c,$enginesizes)) {
                                                $display = true;
                                            }
                                        }
                                        if($display == true) {
                                            echo "<option ";
                                            if(in_array('1.0-1.3', $engineSize)) {echo "selected='selected' ";}
                                            echo "value='1.0-1.3'>1.0L-1.3L</option>";
                                        }

                                        $display = false;
                                        $check = array('1.4','1.5','1.6');
                                        foreach($check as $c) {
                                            if(in_array($c,$enginesizes)) {
                                                $display = true;
                                            }
                                        }
                                        if($display == true) {
                                            echo "<option ";
                                            if(in_array('1.4-1.6', $engineSize)) {echo "selected='selected' ";}
                                            echo "value='1.4-1.6'>1.4L-1.6L</option>";
                                        }

                                        $display = false;
                                        $check = array('1.7','1.8','1.9');
                                        foreach($check as $c) {
                                            if(in_array($c,$enginesizes)) {
                                                $display = true;
                                            }
                                        }
                                        if($display == true) {
                                            echo "<option ";
                                            if(in_array('1.7-1.9', $engineSize)) {echo "selected='selected' ";}
                                            echo "value='1.7-1.9'>1.7L-1.9L</option>";
                                        }

                                        $display = false;
                                        $check = array('2.0','2.1','2.2', '2.3','2.4','2.5');
                                        foreach($check as $c) {
                                            if(in_array($c,$enginesizes)) {
                                                $display = true;
                                            }
                                        }
                                        if($display == true) {
                                            echo "<option ";
                                            if(in_array('2.0-2.5', $engineSize)) {echo "selected='selected' ";}
                                            echo "value='2.0-2.5'>2.0L-2.5L</option>";
                                        }

                                        $display = false;
                                        $check = array('2.6','2.7','2.8', '2.9');
                                        foreach($check as $c) {
                                            if(in_array($c,$enginesizes)) {
                                                $display = true;
                                            }
                                        }
                                        if($display == true) {
                                            echo "<option ";
                                            if(in_array('2.6-2.9', $engineSize)) {echo "selected='selected' ";}
                                            echo "value='2.6-2.9'>2.6L-2.9L</option>";
                                        }
                                        $display = false;
                                        $check = array('3.0','3.1','3.2', '3.3','3.4','3.5','3.6','3.7','3.8','3.9');
                                        foreach($check as $c) {
                                            if(in_array($c,$enginesizes)) {
                                                $display = true;
                                            }
                                        }
                                        if($display == true) {
                                            echo "<option ";
                                            if(in_array('3.0-3.9', $engineSize)) {echo "selected='selected' ";}
                                            echo "value='3.0-3.9'>3.0L-3.9L</option>";
                                        }

                                        $display = false;
                                        $check = array('4.0','4.1','4.2', '4.3','4.4','4.5','4.6','4.7','4.8','4.9');
                                        foreach($check as $c) {
                                            if(in_array($c,$enginesizes)) {
                                                $display = true;
                                            }
                                        }
                                        if($display == true) {
                                            echo "<option ";
                                            if(in_array('4.0-4.9', $engineSize)) {echo "selected='selected' ";}
                                            echo "value='4.0-4.9'>4.0L-4.9L</option>";
                                        }

                                        $display = false;
                                        $check = array('5.0','5.1','5.2', '5.3','5.4','5.5','5.6','5.7','5.8','5.9','6.0','6.1','6.2', '6.3','6.4','6.5','6.6','6.7','6.8','6.9');
                                        foreach($check as $c) {
                                            if(in_array($c,$enginesizes)) {
                                                $display = true;
                                            }
                                        }
                                        if($display == true) {
                                            echo "<option ";
                                            if(in_array('5.0+', $engineSize)) {echo "selected='selected' ";}
                                            echo "value='5.0+'>5.0L and over</option>";
                                        }
                                    @endphp
                                @else
                                    <option value="0-1.0">Less than 1.0L</option>
                                    <option value="1.0-1.3">1.0L-1.3L</option>
                                    <option value="1.4-1.6">1.4L-1.6L</option>
                                    <option value="1.7-1.9">1.7L-1.9L</option>
                                    <option value="2.0-2.5">2.0L-2.5L</option>
                                    <option value="2.6-2.9">2.6L-2.9L</option>
                                    <option value="3.0-3.9">3.0L-3.9L</option>
                                    <option value="4.0-4.9">4.0L-4.9L</option>
                                    <option value="5.0+">5.0L and over</option>

{{--                                        <option @if($engineSize == '5.0+') selected="selected" @endif value="5.0+">5.0L and over</option>--}}
                                @endif
                            </select>
			</div>
			<div class="col-sm-2">
                            <button type="button" class="button b3" data-dismiss="modal">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if($database !='LCV')
<div id="doorsOptions" class="filterOptionGroup">

<a data-toggle="modal" data-target="#doorsModal" class="doorsFilterHeader">
    <strong>Doors <span class="plussign">+</span></strong>
</a>
<div id="doorsSelected">
    @php
        $selectedDoors = app('request')->input('doors');
    @endphp
    @if($selectedDoors != "0" && !is_null($selectedDoors))
        @foreach($selectedDoors as $door)
            <a data-toggle="modal" data-target="#doorsModal" class='doorsSelectedLink' id='{{$door}}' href='#'>{{$door}}<span class='rangeDelete'>X</span></a><br>
        @endforeach
    @endif

</div>

<div class="modal fade bd-example-modal-lg" id="doorsModal" tabindex="-1" role="dialog"  aria-labelledby="doorsModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Select Number of Doors</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-md-center" id="doorsSelectContainer">
                    <div class="col-md-9">
                        <select class="w-100 form-control doorsDesktop" name="doors[]" id="doorsDesktop" multiple>
                            @php
                                if(isset($doors) && is_array($doors))
                                {
                                    sort($doors);
                                }
                            @endphp
                            @foreach($doors as $door)
                                @if($door->doors !=0)
                                    <option @if(isset($selectedDoors) && in_array($door->doors, $selectedDoors))
                                            selected="selected" @endif value="{{$door->doors}}">{{$door->doors}}</option>
                                @endif;
                            @endforeach
                        </select>
                    </div>
<div class="col-sm-2">
                        <button type="button" class="button b3" data-dismiss="modal">Done</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endif

<div id="budgetOptions" class="filterOptionGroup">

<a data-toggle="modal" data-target="#budgetModal" class="budgetFilterHeader">
    <strong>Budget<span class="plussign">+</span></strong>
</a>
<div id="budgetSelected">
    @php
        $priceMin = app('request')->input('pricemin');
        $priceMax = app('request')->input('pricemax');
        if($priceMin == '0' && $priceMax == '0') {}else{
            if(!is_null($priceMin) && !is_null($priceMax)) {

                if($priceMin != 0 || $priceMax != "99999") {

                    if(!is_null($priceMin) && $priceMax == "99999") {

    @endphp
    <a href="#" data-toggle='modal' data-target='#budgetModal' class='initialrentalSelectedLink'>From £{{$priceMin}}p/m<span class='rangeDelete'>X</span></a><br>
    @php
        }else{
            if(!is_null($priceMin) && $priceMin != "0") {

    @endphp
    <a href="#" data-toggle='modal' data-target='#budgetModal' class='initialrentalSelectedLink'>£{{$priceMin}} p/m - £{{$priceMax}} p/m<span class='rangeDelete'>X</span></a><br>
    @php
        }else{
    @endphp
    <a href="#" data-toggle='modal' data-target='#budgetModal' class='initialrentalSelectedLink'>Up to £{{$priceMax}} p/m<span class='rangeDelete'>X</span></a><br>
    @php
        }
    }
}
}
}
    @endphp

</div>

<div class="modal fade bd-example-modal-lg" id="budgetModal" tabindex="-1" role="dialog" aria-labelledby="budgetModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Budget</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" id="budgetSelectContainer">
                    @include('partials.filter-filters-budget')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b3" data-dismiss="modal">Done</button>
            </div>
        </div>
    </div>
</div>
</div>


<div id="initialrentalOptions" class="filterOptionGroup">

<a data-toggle="modal" data-target="#initialrentalModal" class="initialrentalFilterHeader">
    <strong>Initial Payment <span class="plussign">+</span></strong>
</a>
<div id="initialrentalSelected">
    @if(is_array(app('request')->input('deposit_value')))
        @foreach(app('request')->input('deposit_value') as $deposit_value)
            <a data-toggle="modal" data-target="#initialrentalModal" class='initialrentalSelectedLink' id='{{$deposit_value}}' href='#'>{{$deposit_value}} @if($deposit_value == 1) Month @else Months @endif <span class='rangeDelete'>X</span></a><br>
        @endforeach
    @endif
</div>

<div class="modal fade bd-example-modal-lg" id="initialrentalModal" tabindex="-1" role="dialog"
     aria-labelledby="initialrentalModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Initial Payment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-md-center" id="initialrentalContainer">
                    @include('partials.filter-filters-initialrental')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b3" data-dismiss="modal">Done</button>
            </div>
        </div>
    </div>
</div>
</div>


<div id="contractlengthOptions" class="filterOptionGroup">

<a data-toggle="modal" data-target="#contractlengthModal" class="contractlengthFilterHeader">
    <strong>Contract Length <span class="plussign">+</span></strong>
</a>
<div id="contractlengthSelected">
    @if(is_array(app('request')->input('term')))
        @foreach(app('request')->input('term') as $term)
            <a data-toggle="modal" data-target="#contractlengthModal" class='contractlengthSelectedLink' id='{{$term}}' href='#'>{{$term}} Months <span class='rangeDelete'>X</span></a>
        @endforeach
    @endif
</div>

<div class="modal fade bd-example-modal-lg" id="contractlengthModal" tabindex="-1" role="dialog"
     aria-labelledby="contractlengthModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Contract length</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-md-center" id="contractlengthContainer">
                    @include('partials.filter-filters-contractlength')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b3" data-dismiss="modal">Done</button>
            </div>
        </div>
    </div>
</div>
</div>


<div id="annualmileageOptions" class="filterOptionGroup">
<a data-toggle="modal" data-target="#annualmileageModal" class="annualmileageFilterHeader">
    <strong>Annual Mileage <span class="plussign">+</span></strong>
</a>
<div id="annualmileageSelected">
    @if(is_array(app('request')->input('annual_mileage')))
        @foreach(app('request')->input('annual_mileage') as $annual_mileage)
            <a data-toggle="modal" data-target="#annualmileageModal" class='annualmileageSelectedLink' id='{{$annual_mileage}}' href='#'>{{$annual_mileage}} <span class='rangeDelete'>X</span></a><br>
        @endforeach
    @endif
</div>

<div class="modal fade bd-example-modal-lg" id="annualmileageModal" tabindex="-1" role="dialog"
     aria-labelledby="annualmileageModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Annual Mileage</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" id="annualmileageContainer">
                    @include('partials.filter-filters-annualmileage')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Done</button>
            </div>
        </div>
    </div>
</div>
</div>
<input type="hidden" name="listing_type" value="{{$listing_type}}">

@if ($finance_type == "H")
<a href="/hybrid" class="clearAllFilters button b3" style="line-height:60px; color:#fff;">Clear all</a>
<input type="hidden" name="finance_type" value="H">
@elseif ($finance_type == "B")
@if($database == 'LCV')
    <a href="/business-vans" class="clearAllFilters button b3" style="line-height: 60px;  color:#fff;">Clear all</a>
    <input type="hidden" name="finance_type" value="B">
        <input type="hidden" name="database" value="{{$database}}">
@else
    <a href="/business" class="clearAllFilters button b3" style="line-height: 60px;  color:#fff;">Clear all</a>
    <input type="hidden" name="finance_type" value="B">
@endif
@elseif ($finance_type == "A")
<a href="/adverse-credit" class="clearAllFilters button b3" style="line-height: 60px; color:#fff;">Clear all</a>
<input type="hidden" name="finance_type" value="A">

@elseif ($finance_type == 'PERFORMANCE')
<a href="/performance" class="clearAllFilters button b3" style="line-height: 60px; color:#fff;">Clear all</a>
<input type="hidden" name="finance_type" value="PERFORMANCE">
@else

@if($database == 'LCV')
    <a href="/personal-vans" class="clearAllFilters button b3" style="line-height: 60px;  color:#fff;">Clear all</a>
    <input type="hidden" name="finance_type" value="P">
    <input type="hidden" name="database" value="{{$database}}">
@else
    <a href="/personal" class="clearAllFilters button b3" style="line-height: 60px;  color:#fff;">Clear all</a>
    <input type="hidden" name="finance_type" value="P">
@endif

@endif


</form>

<div class="sidebar_widget d-none d-lg-block">
<div class="text-center">
<p>
    <strong>Auto Lease Compare deals include:</strong>
</p>
<ul class="fa-ul" style="padding-inline-start: 10px;text-align: left;display: inline-block;font-size: 14px;font-weight: 400;padding-right: 10px;line-height: 26px;">
    <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;font-size: 20px;"></i></span>No Added Fees</li>
    @if($database == 'LCV')
        <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;font-size: 20px;"></i></span>Brand New Vans</li>
    @else
        <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;font-size: 20px;"></i></span>Brand New Cars</li>
    @endif
    <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;font-size: 20px;"></i></span>Manufacturers Warranty</li>
    <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;font-size: 20px;"></i></span>Free Mainland UK Delivery</li>
    <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;font-size: 20px;"></i></span>Breakdown Cover</li>
    <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;font-size: 20px;"></i></span>Road Tax For Contract Length</li>
    <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;font-size: 20px;"></i></span>All advertising partners are authorised and regulated by the FCA</li>
</ul>
</div>
</div>

<div class="sell mt-4 mb-4">
    @if($database == 'LCV')
        <a href="https://clk.omgt1.com/?PID=35052&AID=2136424" target="_blank">
            <img src="/images/goCompareVans.jpg" alt="Compare van insurance with Go Compare" class="img-responsive">
        </a>
    @else
        <a href="https://clk.omgt1.com/?PID=35050&AID=2136424" target="_blank">
            <img src="/images/goCompareCars.jpg" alt="Compare car insurance with Go Compare" class="img-responsive">
        </a>
    @endif
</div>

<div class="sell mt-4 d-none d-lg-block">
    <a href="/sell-your-car">
        <img src="/images/sellyourcar.jpg" alt="sell your car" class="img-responsive ">
    </a>
</div>

<div class="sell mt-4 d-none d-lg-block">
    <a href="https://www.motoreasy.com/gap-quote/step/1?utm_medium=affiliate&utm_campaign=organic&utm_source=autoleasecompare" target="_blank">
        <img src="/images/alc-motoreasy.png" alt="15% off GAP Insurance with code ALC15 at Motoreasy." class="img-responsive ">
    </a>
</div>
