<div id="accordion">

    @if($review->ten_second_review != '')
        <div class="card">
            <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false">
                <h5 class="mb-0">
                    <button class="btn btn-link" aria-controls="collapseOne">
                        Ten second review
                    </button>
                </h5>
            </div>

            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    {!! $review->ten_second_review !!}
                </div>
            </div>
        </div>
    @endif



    @if($review->background != '')

        <div class="card">
            <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        Background
                    </button>
                </h2>
            </div>

            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    {!! $review->background !!}
                </div>
            </div>
        </div>

    @endif

    @if($review->driving_experience != '')

        <div class="card">
            <div class="card-header" id="heading10" data-toggle="collapse" data-target="#collapse10" aria-expanded="false">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse10">
                        Driving Experience
                    </button>
                </h2>
            </div>

            <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordion">
                <div class="card-body">
                    {!! $review->driving_experience !!}
                </div>
            </div>
        </div>

    @endif

    @if($review->design_and_build != '')

        <div class="card">
            <div class="card-header" id="heading11" data-toggle="collapse" data-target="#collapse11" aria-expanded="false">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse11">
                        Design and build
                    </button>
                </h2>
            </div>

            <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordion">
                <div class="card-body">
                    {!! $review->design_and_build !!}
                </div>
            </div>
        </div>

    @endif

    @if($review->history != '')

        <div class="card">
            <div class="card-header" id="heading2" data-toggle="collapse" data-target="#collapse2" aria-expanded="false">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                        History
                    </button>
                </h2>
            </div>

            <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
                <div class="card-body">
                    {!! $review->history !!}
                </div>
            </div>
        </div>

    @endif

    @if($review->what_to_look_for != '')

        <div class="card">
            <div class="card-header" id="heading3" data-toggle="collapse" data-target="#collapse3" aria-expanded="false">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                        What to look for
                    </button>
                </h2>
            </div>

            <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion">
                <div class="card-body">
                    {!! $review->what_to_look_for !!}
                </div>
            </div>
        </div>

    @endif

    @if($review->what_you_get != '')

        <div class="card">
            <div class="card-header" id="heading4" data-toggle="collapse" data-target="#collaps4" aria-expanded="false">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                        What you get
                    </button>
                </h2>
            </div>

            <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
                <div class="card-body">
                    {!! $review->what_you_get !!}
                </div>
            </div>
        </div>

    @endif

    @if($review->what_you_pay != '')
        <div class="card">
            <div class="card-header" id="heading5" data-toggle="collapse" data-target="#collapse5" aria-expanded="false">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                        What you pay
                    </button>
                </h2>
            </div>

            <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
                <div class="card-body">
                    {!! $review->what_you_pay !!}
                </div>
            </div>
        </div>

    @endif

    @if($review->replacement_parts != '')
        <div class="card">
            <div class="card-header" id="heading6" data-toggle="collapse" data-target="#collapse6" aria-expanded="false">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                        Replacement parts
                    </button>
                </h2>
            </div>

            <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
                <div class="card-body">
                    {!! $review->replacement_parts !!}
                </div>
            </div>
        </div>
    @endif

    @if($review->on_the_road != '')
        <div class="card">
            <div class="card-header" id="heading7" data-toggle="collapse" data-target="#collapse7" aria-expanded="false">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
                        On the road
                    </button>
                </h2>
            </div>

            <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
                <div class="card-body">
                    {!! $review->on_the_road !!}
                </div>
            </div>
        </div>
    @endif



    @if($review->short_summary != '')
        <div class="card">
            <div class="card-header" id="heading1" data-toggle="collapse" data-target="#collapse1" aria-expanded="false">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                        Summary
                    </button>
                </h2>
            </div>

            <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#accordion">
                <div class="card-body">
                    {!! $review->short_summary !!}
                </div>
            </div>
        </div>
    @endif
</div>
