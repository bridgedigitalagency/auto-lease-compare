{{--@if ($paginator->lastPage() > 1)--}}
{{--    <ul class="pagination">--}}
{{--        <li class="page-item {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">--}}
{{--            <a class="page-link" rel="prev" data-id="{{$paginator->currentPage()-1}}" href="{{ $paginator->url($paginator->currentPage()-1) }}">Previous</a>--}}
{{--        </li>--}}


{{--        @php--}}
{{--            if($paginator->currentPage()>=4) {--}}
{{--                $page = $paginator->currentPage()-4;--}}
{{--            }else{--}}
{{--                $page = 1;--}}
{{--            }--}}
{{--        @endphp--}}
{{--        @for ($i = $page; $i <= $paginator->currentPage()+4; $i++)--}}
{{--            @if($i-1 < $paginator->lastPage())--}}
{{--            <li class="d-none d-lg-block d-xl-block page-item {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">--}}
{{--                <a class="page-link" data-id="{{$i}}" href="{{ $paginator->url($i) }}">{{ $i }}</a>--}}
{{--            </li>--}}
{{--            @endif--}}
{{--        @endfor--}}


{{--        <li class="page-item {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">--}}
{{--            <a class="page-link" rel="next" data-id="{{$paginator->currentPage()+1}}" href="{{ $paginator->url($paginator->currentPage()+1) }}">Next</a>--}}
{{--        </li>--}}
{{--    </ul>--}}
{{--@endif--}}


<ul class="pagination">
    <li class="page-item {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
        <a class="page-link" rel="prev" data-id="{{$paginator->currentPage()-1}}" href="{{ $paginator->url($paginator->currentPage()-1) }}">Previous</a>
    </li>


    <li class="page-item ">
        <a class="page-link" rel="next" data-id="{{$paginator->currentPage()+1}}" href="{{ $paginator->url($paginator->currentPage()+1) }}">Next</a>
    </li>
</ul>
