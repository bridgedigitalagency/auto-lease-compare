<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a class="specFilterHeader mb-3 back"><strong><i class="fa fa-angle-double-left"></i> Back</strong></a>
            <strong class="mb-4">Select Spec</strong>
@php
    $specs = array();
    $specs['android_apple'] = app('request')->input('android_apple');
    $specs['bluetooth'] = app('request')->input('bluetooth');
    $specs['dab'] = app('request')->input('dab');
    $specs['parking_camera'] = app('request')->input('parking_camera');
    $specs['parking_sensors'] = app('request')->input('parking_sensors');
    $specs['satnav'] = app('request')->input('satnav');
    $specs['wireless_charge'] = app('request')->input('wireless_charge');
    $specs['alloys'] = app('request')->input('alloys');
    $specs['climate_control'] = app('request')->input('climate_control');
    $specs['cruise_control'] = app('request')->input('cruise_control');
    $specs['heated_seats'] = app('request')->input('heated_seats');
    $specs['leather_seats'] = app('request')->input('leather_seats');
    $specs['panoramic_roof'] = app('request')->input('panoramic_roof');
    $specs['sunroof'] = app('request')->input('sunroof');
@endphp
            <select name="specs[]" id="specs" class="mobilespecs mt-2" multiple>
                @foreach($specs as $k=>$v)
                    <option value="{{$k}}" @if(is_array(app('request')->input('specs')) && in_array($k, app('request')->input('specs'))) selected="selected" @endif>
                        @if($k == 'android_apple')
                            Android Auto / Apple CarPlay
                        @else
                            {{ucwords(str_replace('_', ' ', $k))}}
                        @endif
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>