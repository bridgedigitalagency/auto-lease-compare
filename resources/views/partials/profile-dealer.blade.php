@if (count($errors) > 0)
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div>
@endif
@if(session()->has('message'))
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-success" role="alert">
                    <p>{{ session()->get('message') }}</p>
                </div>
            </div>
        </div>
    </div>
    </div>
@endif
<div id="user_basics_form">

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6" id="leftColumn">
                        <div class="profileBlockBackground">
                            <h3>Company Details</h3>
                            <label for="name">Company Id</label>
                            <input type="text" id="name" name="name" disabled value="{{$user->name}}">
                            <p>To change this, please contact us</p>
                        </div>
                    </div>
                    <div class="col-sm-6" id="rightColumn">
                        <div class="profileBlockBackground">
                            <div class="modalLoading">
                                <div class="lds-dual-ring"><img src='/images/loadingWheel.png'></div>
                            </div>

                            <h3>Company Logo</h3>
                            <div class="text-center mb-3"><img src="/images/uploads/logos/{{ $user->logo }}" width="200"></div>
                            <div id="imageWin" class="alert alert-success mt-3" style="display: none;">Your image has been uploaded</div>

                            <form action="{{ route('image.upload.post') }}" method="POST" enctype="multipart/form-data" id="logoUpload" autocomplete="off">
                                @csrf
                                <input autocomplete="false" name="hidden" type="text" style="display:none;">
                                <label class="file">
                                    <input type="file" id="image" name="image">
                                </label>
                                <button type="submit" class="btn btn-success">Upload</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" action="/profile/{{$user->id}}/updateUserAccount" accept-charset="UTF-8" autocomplete="off">
        {!! csrf_field() !!}
        <input name="_method" type="hidden" value="PUT">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6" id="leftColumn">
                            <div class="profileBlockBackground">
                                <h3>Personal Details</h3>
                                <label for="first_name">First Name</label>
                                <input type="text" id="first_name" name="first_name" value="{{$user->first_name}}">
                                <label for="last_name">Last Name</label>
                                <input type="text" id="last_name" name="last_name" value="{{$user->last_name}}">
                            </div>
                        </div>
                        <div class="col-sm-6" id="rightColumn">
                            <div class="profileBlockBackground">
                                <h3>Contact Details</h3>
                                <label for="email">Email Address</label>
                                <input type="email" disabled id="emailAddress" name="email" value="{{$user->email}}" data-lpignore="true">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="profileBlockBackground">
                        <h3>Address</h3>
                        <div class="row">
                            <div class="col-12">
                                <textarea name="address" class="form-control" rows="4">@if($user->address){{$user->address}}@endif</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="profileBlockBackground">
                        <h3>Delivery Policy</h3>
                        <div class="row">
                            <div class="col-12">
                                <textarea name="delivery_policy" class="form-control" rows="4">@if($user->delivery_policy){{$user->delivery_policy}}@endif</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="profileBlockBackground">
                        <h3>Opening Hours</h3>
                        <div class="row">
                            <div class="col-12">
                                <textarea name="opening_hours" class="form-control" rows="4">@if($user->opening_hours){{$user->opening_hours}}@endif</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="profileBlockBackground">
                        <h3>About Text</h3>
                        <div class="row">
                            <div class="col-12">
                                <textarea name="about" class="form-control" rows="4">@if($user->about){{$user->about}}@endif</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6" id="leftColumn">
                            <div class="profileBlockBackground">
                                <h3>Marketing Preferences</h3>
                                <div class="row">
                                    <div class="col-1">
                                        <input type="checkbox" id="mailing_list" name="mailing_list" @if($user->mailing_list)checked @endif>
                                    </div>
                                    <div class="col-10">
                                        <label for="mailing_list" data-toggle="tooltip" data-placement="top" title="Checking this will enable us to keep in touch with you via our marketing emails">Marketing Emails<i class="fa fa-info-circle infobutton"></i></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-1">
                                        <input type="checkbox" id="garage_price_notification" name="garage_price_notification" @if($user->garage_price_notification)checked @endif>
                                    </div>
                                    <div class="col-10">
                                        <label for="garage_price_notification" data-toggle="tooltip" data-placement="top" title="Checking this will enable us to notify you when cars in your garage prices drop!">Price drop notifications<i class="fa fa-info-circle infobutton"></i></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6" id="rightColumn">
                            <div class="profileBlockBackground">
                                <h3>Submit</h3>
                                <p>Make sure your information is up to date so our dealers can contact you</p>
                                <input type="submit" value="Update Details" class="btn btn-primary">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>