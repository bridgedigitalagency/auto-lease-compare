@foreach($transmissions as $transmission)
    <div class="col-md-3 text-center">
        <label>
            <input type="checkbox" class="invisible" name="transmission[]"
                @php
                    if(is_array(app('request')->input('transmission'))) {
                        $reqArr = app('request')->input('transmission');
                    }else{
                        $reqArr = array();
                    }
                    if(in_array($transmission->transmission, $reqArr)) {
                        echo "checked='checked'";
                    }
                @endphp
                   value="{{$transmission->transmission}}" id="{{$transmission->transmission}}Checkbox">
            <div class="imgdiv"><img src="/images/{{$transmission->transmission}}.png" class="filtericons">{{$transmission->transmission}}</div>
        </label>
    </div>
@endforeach