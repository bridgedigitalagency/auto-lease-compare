<div class="footer-top">
    <div class="container">
        <div class="row">
                @include('partials.footer-links')

            <div class="col-md-3 col-sm-6">
                <h6>SUBSCRIBE TO OUR HOT DEALS</h6>
                <div class="newsletter-form">
                    <!-- Begin Mailchimp Signup Form -->
                    <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
                    <style type="text/css">
                        #mc_embed_signup{clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
                    </style>
                    <style type="text/css">
                        #mc-embedded-subscribe-form input[type=checkbox]{display: inline; width: auto;margin-right: 10px;}
                        #mergeRow-gdpr {margin-top: 20px;}
                        #mergeRow-gdpr fieldset label {font-weight: normal;}
                        #mc-embedded-subscribe-form .mc_fieldset{border:none;min-height: 0px;padding-bottom:0px;}
                    </style>
                    <div id="mc_embed_signup" style="text-align: left;">
                        <form action="https://autoleasecompare.us20.list-manage.com/subscribe/post?u=d022602b54b0ed1c1366d1b53&amp;id=4884dd7421" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" style="padding-top:0px;" target="_blank" novalidate="">
                            <div id="mc_embed_signup_scroll">

                                <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" style="width: 100%; margin-bottom:10px;" required="">
                                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d022602b54b0ed1c1366d1b53_4884dd7421" tabindex="-1" value=""></div>
                                <div class=""><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button" style="font-weight:600;background-color: #fc5185; width:100%;"></div>
                            </div>
                        </form>
                    </div>

                    <!--End mc_embed_signup-->
                    <p class="subscribed-text">*We send great deals and latest auto news to our subscribed users every week.</p>
                    <p>Connect with Us:</p>
                    <div class="footer_widget">
                        <ul>
                            <li><a href="https://facebook.com/autoleasecompare" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/compare_lease" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/auto-lease-compare" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.instagram.com/auto_lease_compare/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-push-6 text-right">
                <div class="footer_widget">

                </div>
                <div class="footer_widget">

                </div>
            </div>
            <div class="col-md-12" style="text-align:center;color: #fff;">
                Auto Lease Compare Ltd is authorised and regulated by the Financial Conduct Authority: 822040<br>
                We act as a credit broker not a lender. We can introduce you to a limited number of brokers and we may receive a commission payment from the broker if you decide to enter into an agreement through them.<br>
                Auto Lease Compare Ltd - Registered in England and Wales. Company number: 11519767<br>
                Data Protection Registration number: ZA469390<br>
                VAT Number: 307521233<br>
                Registered Office Address: Auto Lease Compare Ltd, 10 Waterson Street, London, E2 8HL<br>
                Email: <a href="mailto:info@autoleasecompare.com">info@autoleasecompare.com</a>
            </div>

    </div>
</div>
</div>
@if(!isset($single_quote))
<script>
    // Creare's 'Implied Consent' EU Cookie Law Banner v:2.4

    var dropCookie = true;                      // false disables the Cookie, allowing you to style the banner
    var cookieDuration = 14;                    // Number of days before the cookie expires, and the banner reappears
    var cookieName = 'complianceCookie';        // Name of our cookie
    var cookieValue = 'on';                     // Value of cookie

    function createDiv(){
        var bodytag = document.getElementsByTagName('body')[0];
        var div = document.createElement('div');
        div.setAttribute('id','cookie-law');
        div.innerHTML = '<p>Our website uses cookies. By continuing we assume your permission to deploy cookies, as detailed in our <a href="/site/cookies" rel="nofollow" title="Cookie Policy">cookie policy</a>. <a class=" close-cookie-banner" href="javascript:void(0);" onclick="removeMe();" style="color:#3fc1c9; font-weight: 900;">Close</a></p>';
        bodytag.insertBefore(div,bodytag.lastChild); // Adds the Cookie Law Banner just after the opening <body> tag

        document.getElementsByTagName('body')[0].className+=' cookiebanner'; //Adds a class tothe <body> tag when the banner is visible

        createCookie(window.cookieName,window.cookieValue, window.cookieDuration); // Create the cookie
    }


    function createCookie(name,value,days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        if(window.dropCookie) {
            document.cookie = name+"="+value+expires+"; path=/";
        }
    }

    function checkCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    function eraseCookie(name) {
        createCookie(name,"",-1);
    }

    window.onload = function(){
        if(checkCookie(window.cookieName) != window.cookieValue){
            createDiv();
        }
    }

    function removeMe(){
        var element = document.getElementById('cookie-law');
        element.parentNode.removeChild(element);
    }
</script>
    @endif