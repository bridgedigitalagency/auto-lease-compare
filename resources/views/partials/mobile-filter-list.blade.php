<form method="GET" id="mobileFilterList">
    @if(strpos(Request::url(), 'vans'))
        @php $database = 'LCV'; @endphp
    @endif


    <div class="hiddenOptions" id="mobileMakers">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <a class="makeFilterHeader mb-3 back"><strong><i class="fa fa-angle-double-left"></i> Back</strong></a>
                    <strong class="mb-4">Select Make</strong>
                    <div class="row" style="padding-bottom: 70px;">

                        @php
                            if(is_array(app('request')->input('maker'))) {
                                $reqArr = app('request')->input('maker');
                            }else{
                                $reqArr = array();
                            }
                        @endphp
                        @foreach($makes ?? '' as $make)
                        <div class="col-md-2 col-sm-2 col-4 text-center">
                            <input type="checkbox"
                                   @if(in_array($make->maker, $reqArr)) checked @endif
                                   value="{{$make->maker}}" name="maker[]" id="mobcb{{$loop->iteration}}">
                            <label for="mobcb{{$loop->iteration}}"><img
                                        src="/images/manufacturers/@php echo(strtolower($make->maker)) @endphp.png" style="max-width: 100%;"
                                        id="@php echo(strtolower($make->maker)) @endphpimg"mob>
                                <br/>{{$make->maker}}
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
<div class="applydiv">
                    <a class="back button b1 mobileFiltersBottomBack" style="color: #fff!important;">Apply</a>
</div>
        </div>
    </div>

    <div class="hiddenOptions" id="mobileModels">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <a class="modelFilterHeader mb-3 back"><strong><i class="fa fa-angle-double-left"></i> Back</strong></a>
{{--                    <strong class="mb-4">Select Model</strong>--}}

                        @if(is_array(app('request')->input('maker')))
                            @php

                                $makerArr = app('request')->input('maker');

                                    if(is_array(app('request')->input('range'))) {

                                        $reqArr = app('request')->input('range');
                                }else{
                                $reqArr = array();
                                }
                                    $rangesCopy = $ranges;
                            @endphp
                            <ul id="rangeSelectContainer">
                            @if(isset($ranges) && is_array($ranges))
                                @foreach($ranges as $maker=>$ranges)
                                    @if(in_array($maker, $makerArr))
                                        <li>
                                            <label>Select {{$maker}} Model</label>

                                            <select name="range[]" id="mobile-range-{{str_replace(' ', '', $maker)}}" class="w-100 form-control mobileRange mt-2" multiple="multiple">
                                                @foreach($ranges as $range)
                                                    <option value="{{$range}}" @if(in_array($range, $reqArr)) selected="selected" @endif>{{$range}}</option>
                                                @endforeach
                                            </select>
                                        </li>
                                    @endif
                                @endforeach
                            @endif
                            </ul>
                        @endif
                </div>
            </div>
            <div class="applydiv">
                <a class="back button b1 mobileFiltersBottomBack" style="color: #fff!important;">Apply</a>
            </div>
        </div>
    </div>

    <div class="hiddenOptions" id="mobileTrim">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <a class="trimFilterHeader mb-3 back"><strong><i class="fa fa-angle-double-left"></i> Back</strong></a>
{{--                    <strong class="mb-4">Select Trim</strong>--}}
{{--                    <select id="trim" class="w-100 form-control pricing_select mobileTrim  mt-2" name="trim[]" multiple>--}}
{{--                        <option value="">Please select a model</option>--}}
{{--                        @if(isset($trims))--}}
{{--                            @php--}}

{{--                                if(is_array(app('request')->input('trim'))) {--}}

{{--                                    $trimReqArr = app('request')->input('trim');--}}
{{--                            }else{--}}
{{--                            $trimReqArr = array();--}}
{{--                            }--}}
{{--                            @endphp--}}
{{--                            @foreach($trims as $range=>$trims)--}}
{{--                                <optgroup label="{{$range}}">--}}
{{--                                    @foreach($trims as $trim)--}}
{{--                                        <option value="{{$trim}}" data-range="{{$trim}}" @if(in_array($trim, $trimReqArr) && in_array($range, $reqArr)) selected="selected" @endif >{{$trim}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </optgroup>--}}
{{--                            @endforeach--}}
{{--                        @endif--}}
{{--                    </select>--}}
                    @include('partials.mobile-trims')
                </div>
            </div>
            <div class="applydiv">
                <a class="back button b1 mobileFiltersBottomBack" style="color: #fff!important;">Apply</a>
            </div>
        </div>
    </div>

    <div class="hiddenOptions" id="mobileFuel">
        @include('partials.mobile-filters-fuel')
        <div class="applydiv">
            <a class="back button b1 mobileFiltersBottomBack" style="color: #fff!important;">Apply</a>
        </div>      </div>

    <div class="hiddenOptions" id="mobileTransmissionContainer">
        @include('partials.mobile-filters-transmission')
        <div class="applydiv">
            <a class="back button b1 mobileFiltersBottomBack" style="color: #fff!important;">Apply</a>
        </div>
    </div>

    <div class="hiddenOptions" id="mobileBodystyle">
        @include('partials.mobile-filters-bodystyle')
        <div class="applydiv">
            <a class="back button b1 mobileFiltersBottomBack" style="color: #fff!important;">Apply</a>
        </div>
    </div>
@if ($listing_type != "H")
    @php
        $engineSize = array();
        if(!is_null(app('request')->input('engineSize') ))
        {
            $engineSize = app('request')->input('engineSize');
        }
    @endphp
    <div class="hiddenOptions" id="mobileEngine">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <a class="enginesizeFilterHeader mb-3 back"><strong><i class="fa fa-angle-double-left"></i> Back</strong></a>
                    <strong class="mb-4">Filter Engine Size</strong>
                    <select class="w-100 form-control mobileEngineSize mt-2" id="mobileEngineSize" name="engineSize[]" multiple>

                        @if(isset($enginesizes))

                            @php

                                $display = false;
                                $check = array('0','0.1','0.2','0.3','0.4','0.5','0.6','0.7','0.8','0.9');
                                foreach($check as $c) {
                                    if(in_array($c,$enginesizes)) {
                                        $display = true;
                                    }
                                }
                                if($display == true) {
                                    echo "<option ";
                                    if(in_array('0-1.0', $engineSize)) {echo "selected='selected' ";}
                                    echo "value=\"0-1.0\">Less than 1.0L</option>";
                                }

                                $display = false;
                                $check = array('1.0','1.1','1.2','1.3','1.3');
                                foreach($check as $c) {
                                    if(in_array($c,$enginesizes)) {
                                        $display = true;
                                    }
                                }
                                if($display == true) {
                                    echo "<option ";
                                    if(in_array('1.0-1.3', $engineSize)) {echo "selected='selected' ";}
                                    echo "value='1.0-1.3'>1.0L-1.3L</option>";
                                }

                                $display = false;
                                $check = array('1.4','1.5','1.6');
                                foreach($check as $c) {
                                    if(in_array($c,$enginesizes)) {
                                        $display = true;
                                    }
                                }
                                if($display == true) {
                                    echo "<option ";
                                    if(in_array('1.4-1.6', $engineSize)) {echo "selected='selected' ";}
                                    echo "value='1.4-1.6'>1.4L-1.6L</option>";
                                }

                                $display = false;
                                $check = array('1.7','1.8','1.9');
                                foreach($check as $c) {
                                    if(in_array($c,$enginesizes)) {
                                        $display = true;
                                    }
                                }
                                if($display == true) {
                                    echo "<option ";
                                    if(in_array('1.7-1.9', $engineSize)) {echo "selected='selected' ";}
                                    echo "value='1.7-1.9'>1.7L-1.9L</option>";
                                }

                                $display = false;
                                $check = array('2.0','2.1','2.2', '2.3','2.4','2.5');
                                foreach($check as $c) {
                                    if(in_array($c,$enginesizes)) {
                                        $display = true;
                                    }
                                }
                                if($display == true) {
                                    echo "<option ";
                                    if(in_array('2.0-2.5', $engineSize)) {echo "selected='selected' ";}
                                    echo "value='2.0-2.5'>2.0L-2.5L</option>";
                                }

                                $display = false;
                                $check = array('2.6','2.7','2.8', '2.9');
                                foreach($check as $c) {
                                    if(in_array($c,$enginesizes)) {
                                        $display = true;
                                    }
                                }
                                if($display == true) {
                                    echo "<option ";
                                    if(in_array('2.6-2.9', $engineSize)) {echo "selected='selected' ";}
                                    echo "value='2.6-2.9'>2.6L-2.9L</option>";
                                }
                                $display = false;
                                $check = array('3.0','3.1','3.2', '3.3','3.4','3.5','3.6','3.7','3.8','3.9');
                                foreach($check as $c) {
                                    if(in_array($c,$enginesizes)) {
                                        $display = true;
                                    }
                                }
                                if($display == true) {
                                    echo "<option ";
                                    if(in_array('3.0-3.9', $engineSize)) {echo "selected='selected' ";}
                                    echo "value='3.0-3.9'>3.0L-3.9L</option>";
                                }

                                $display = false;
                                $check = array('4.0','4.1','4.2', '4.3','4.4','4.5','4.6','4.7','4.8','4.9');
                                foreach($check as $c) {
                                    if(in_array($c,$enginesizes)) {
                                        $display = true;
                                    }
                                }
                                if($display == true) {
                                    echo "<option ";
                                    if(in_array('4.0-4.9', $engineSize)) {echo "selected='selected' ";}
                                    echo "value='4.0-4.9'>4.0L-4.9L</option>";
                                }

                                $display = false;
                                $check = array('5.0','5.1','5.2', '5.3','5.4','5.5','5.6','5.7','5.8','5.9','6.0','6.1','6.2', '6.3','6.4','6.5','6.6','6.7','6.8','6.9');
                                foreach($check as $c) {
                                    if(in_array($c,$enginesizes)) {
                                        $display = true;
                                    }
                                }
                                if($display == true) {
                                    echo "<option ";
                                    if(in_array('5.0+', $engineSize)) {echo "selected='selected' ";}
                                    echo "value='5.0+'>5.0L and over</option>";
                                }
                            @endphp
                        @else
                            <option value="0-1.0">Less than 1.0L</option>
                            <option value="1.0-1.3">1.0L-1.3L</option>
                            <option value="1.4-1.6">1.4L-1.6L</option>
                            <option value="1.7-1.9">1.7L-1.9L</option>
                            <option value="2.0-2.5">2.0L-2.5L</option>
                            <option value="2.6-2.9">2.6L-2.9L</option>
                            <option value="3.0-3.9">3.0L-3.9L</option>
                            <option value="4.0-4.9">4.0L-4.9L</option>
                            <option value="5.0+">5.0L and over</option>

                            {{--                                        <option @if($engineSize == '5.0+') selected="selected" @endif value="5.0+">5.0L and over</option>--}}
                        @endif
                    </select>
                </div>
            </div>

            <div class="applydiv">
                <a class="back button b1 mobileFiltersBottomBack" style="color: #fff!important;">Apply</a>
            </div>
        </div>
    </div>
@endif
@if(isset($database) && $database !='LCV')
    <div class="hiddenOptions" id="mobileDoors">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <a class="doorsFilterHeader mb-3 back"><strong><i class="fa fa-angle-double-left"></i> Back</strong></a>
                    <strong class="mb-4">Select Doors</strong>
                    <select class="w-100 form-control mobileDoorSelect mt-2" name="doors[]" id="doors" multiple>
                        @php
                            $selectedDoors = app('request')->input('doors');
                            if(isset($doors) && is_array($doors))
                            {
                                sort($doors);
                            }
                        @endphp
                        @foreach($doors as $door)
                            @if($door->doors !=0)
                                <option @if(isset($selectedDoors) && in_array($door->doors, $selectedDoors)) selected="selected" @endif value="{{$door->doors}}">{{$door->doors}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="applydiv">
                <a class="back button b1 mobileFiltersBottomBack" style="color: #fff!important;">Apply</a>
            </div>
        </div>
    </div>
@endif
    <div class="hiddenOptions" id="mobileOptions">
        @include('partials.mobile-filters-specs')
        <div class="applydiv">
            <a class="back button b1 mobileFiltersBottomBack" style="color: #fff!important;">Apply</a>
        </div>
    </div>

    <div class="hiddenOptions" id="mobileBudget">
        @include('partials.mobile-filters-budget')
        <div class="applydiv">
            <a class="back button b1 mobileFiltersBottomBack" style="color: #fff!important;">Apply</a>
        </div>
    </div>

    <div class="hiddenOptions" id="mobileInitial">
        @include('partials.mobile-filters-initialrental')
        <div class="applydiv">
            <a class="back button b1 mobileFiltersBottomBack" style="color: #fff!important;">Apply</a>
        </div>
    </div>

    <div class="hiddenOptions"  id="mobileContract">
        @include('partials.mobile-filters-contractlength')

        <div class="applydiv">
            <a class="back button b1 mobileFiltersBottomBack" style="color: #fff!important;">Apply</a>
        </div>
    </div>

    <div class="hiddenOptions" id="mobileMileage">
        @include('partials.mobile-filters-annualmileage')
        <div class="applydiv">
            <a class="back button b1 mobileFiltersBottomBack" style="color: #fff!important;">Apply</a>
        </div>
    </div>

    <div style="padding-bottom: 80px;" id="mobileFiltersListing">
    @if(strpos(Request::url(), 'adverse') || strpos(Request::url(), 'hybrid') || strpos(Request::url(), 'performance'))
            @if($finance_type != 'B')
                <input type="hidden" name="finance_type_show" value="P" class="finance_type_show">
            @else
                <input type="hidden" name="finance_type_show" value="B" class="finance_type_show">
            @endif
        <div class="container">
            <div class="row">

                <div class="col-xs-6 text-center">
                    @if($finance_type != 'B')
                        <a href="#" class="leaseFilterType button b3" id="personalLeaseFilter">Personal</a>
                    @else
                        <a href="#" class="leaseFilterType button b7" id="personalLeaseFilter">Personal</a>
                    @endif

                </div>
                <div class="col-xs-6 text-center">
                    @if($finance_type == 'B')
                        <a href="#" class="leaseFilterType button b3" id="businessLeaseFilter">Business</a>
                    @else
                        <a href="#" class="leaseFilterType button b7" id="businessLeaseFilter">Business</a>
                    @endif
                </div>
            </div>
        </div>
    @endif


        <div id="makeOptions" class="filterOptionGroup">
            <a class="makeFilterHeader">
                <strong>Make <span class="plussign"></span></strong>
            </a>
            <div id="makeSelected" class="mobileMakerSelected">
                @if(is_array(app('request')->input('maker')))
                    @foreach(app('request')->input('maker') as $maker)
                        <a class='makerSelectedLink' id='{{$maker}}' href='#' style="display: inline-block">
                            <img src="/images/manufacturers/{{strtolower($maker)}}.png">
                        </a>
                    @endforeach
                @endif
            </div>
        </div>

        @if(is_array(app('request')->input('maker')))
    <div id="modelOptions" class="filterOptionGroup">
        <a class="modelFilterHeader"><strong>Models <span class="plussign"></span></strong></a>
        <div id="modelSelected">
            @if(is_array(app('request')->input('range')))
                @foreach(app('request')->input('range') as $range)

                    @php
                        $rangesS = serialize($rangesCopy);
                    @endphp

                    @if(strpos($rangesS, $range))
                        <a class='rangeSelectedLink' id='{{$range}}' href='#'>{{$range}}<span class='rangeDelete'>X</span></a>
                    @endif
                @endforeach
            @endif

        </div>
    </div>
        @endif

        @if(is_array(app('request')->input('maker')) && is_array(app('request')->input('range')))
     <div id="trimOptions" class="filterOptionGroup" @if(isset($trims) && is_array($trims) && count($trims) > 0) style="display: block!important" @endif>
            <a class="trimFilterHeader"><strong>Select Model Variant <span class="plussign"></span></strong></a>
         <div id="trimSelected">
             @if(is_array(app('request')->input('trim')))
                 @foreach(app('request')->input('trim') as $trim)
                     @php
                        $rawTrim = $trim;
                        $explode = explode('_', $trim);
                        $range = $explode[0];
                        $trim = $explode[1];
                     @endphp
			     @if(isset($selTrim) && is_array($selTrim) && in_array($rawTrim, $selTrim))
                     <a class='trimSelectedLink' data-range='{{$range}}' id='{{$rawTrim}}' href='#'>{{$range}} {{$trim}}<span class='rangeDelete'>X</span></a>
                     @endif
                 @endforeach
             @endif
         </div>
     </div>
        @endif

    <div id="fuelOptions" class="filterOptionGroup">
        <a class="fuelFilterHeader"><strong>Fuel Type <span class="plussign"></span></strong>
        </a>
        <div id="fuelSelected">
            @if(is_array(app('request')->input('fuel')))
                @foreach(app('request')->input('fuel') as $fuel)
                    <a class='fuelSelectedLink' id='{{$fuel}}' href='#'>{{$fuel}}<span class='rangeDelete'>X</span></a>

                @endforeach
            @endif


        </div>
    </div>

    <div id="transmissionOptions" class="filterOptionGroup">
        <a class="transmissionFilterHeader">
            <strong>Transmission <span class="plussign"></span></strong>
        </a>

        <div id="transmissionSelected">
            @if(is_array(app('request')->input('transmission')))
                @foreach(app('request')->input('transmission') as $transmission)
                    <a class='transmissionSelectedLink' id='{{$transmission}}' href='#'>{{$transmission}}<span class='rangeDelete'>X</span></a>
                @endforeach
            @endif


        </div>
    </div>

    <div id="bodystyleOptions" class="filterOptionGroup">
        <a class="bodystyleFilterHeader">
            <strong>Body Style <span class="plussign"></span></strong>
        </a>
        <div id="bodystyleSelected">
            @if(is_array(app('request')->input('bodystyle')))
                @foreach(app('request')->input('bodystyle') as $bodystyle)
                    <a class='bodystyleSelectedLink' id='{{$bodystyle}}' href='#'>{{$bodystyle}}<span class='rangeDelete'>X</span></a>
                @endforeach
            @endif
        </div>
    </div>
@if ($listing_type != "H")
    <div id="enginesizeOptions" class="filterOptionGroup">
        <a class="enginesizeFilterHeader">
            <strong>Engine Size<span class="plussign"></span></strong>
        </a>
        <div id="enginesizeSelected">
            @php

                $engineSize = array();
                if(!is_null(app('request')->input('engineSize') ))
                {
                    $engineSize = app('request')->input('engineSize');
                }

            @endphp
            @if(is_array($engineSize))

                @foreach($engineSize as $engine)
                    <a class='enginesizeSelectedLink' id='{{$engine}}' href='#'>{{$engine}}<span class='rangeDelete'>X</span></a>
                @endforeach
            @else

                @if($engineSize != "0" && !is_null($engineSize))
                    <a class='enginesizeSelectedLink' id='{{$engineSize}}' href='#'>{{$engineSize}}<span class='rangeDelete'>X</span></a>
                @endif
            @endif

        </div>
    </div>
@endif
    <div id="doorsOptions" class="filterOptionGroup">
        <a class="doorsFilterHeader">
            <strong>Doors <span class="plussign"></span></strong>
        </a>
        <div id="doorsSelected">
            @php
                $selectedDoors = app('request')->input('doors');
            @endphp
            @if($selectedDoors != "0" && !is_null($selectedDoors))
                @foreach($selectedDoors as $door)
                    <a class='doorsSelectedLink' id='{{$door}}' href='#'>{{$door}}<span class='rangeDelete'>X</span></a>
                @endforeach
            @endif

        </div>
    </div>

    <div id="budgetOptions" class="filterOptionGroup">
        <a class="budgetFilterHeader">
            <strong>Budget<span class="plussign"></span></strong>
        </a>
        <div id="budgetSelected">
            @php
                $priceMin = app('request')->input('pricemin');
                $priceMax = app('request')->input('pricemax');
                if($priceMin == '0' && $priceMax == '0') {}else{
                    if(!is_null($priceMin) && !is_null($priceMax)) {

                        if($priceMin != 0 || $priceMax != "99999") {

                            if(!is_null($priceMin) && $priceMax == "99999") {

            @endphp
            <a href="#"class='budgetSelectedLink'>From £{{$priceMin}}p/m<span class='rangeDelete'>X</span></a>
            @php
                }else{
                    if(!is_null($priceMin) && $priceMin != "0") {

            @endphp
            <a href="#" class='budgetSelectedLink'>£{{$priceMin}} p/m - £{{$priceMax}} p/m<span class='rangeDelete'>X</span></a>
            @php
                }else{
            @endphp
            <a href="#" class='budgetSelectedLink'>Up to £{{$priceMax}} p/m<span class='rangeDelete'>X</span></a>
            @php
                }
            }
        }
    }
}
            @endphp

        </div>
    </div>


    <div id="initialrentalOptions" class="filterOptionGroup">
        <a class="initialrentalFilterHeader">
            <strong>Initial Payment <span class="plussign"></span></strong>
        </a>
        <div id="initialrentalSelected">
            @if(is_array(app('request')->input('deposit_value')))
                @foreach(app('request')->input('deposit_value') as $deposit_value)
                    <a class='initialrentalSelectedLink' id='{{$deposit_value}}' href='#'>{{$deposit_value}} @if($deposit_value == 1) Month @else Months @endif <span class='rangeDelete'>X</span></a>
                @endforeach
            @endif
        </div>
    </div>

    <div id="contractlengthOptions" class="filterOptionGroup">
        <a class="contractlengthFilterHeader">
            <strong>Contract Length <span class="plussign"></span></strong>
        </a>
        <div id="contractlengthSelected">
            @if(is_array(app('request')->input('term')))
                @foreach(app('request')->input('term') as $term)
                    <a class='contractlengthSelectedLink' id='{{$term}}' href='#'>{{$term}} Months <span class='rangeDelete'>X</span></a>
                @endforeach
            @endif
        </div>
    </div>


    <div id="annualmileageOptions" class="filterOptionGroup">
        <a class="annualmileageFilterHeader">
            <strong>Annual Mileage <span class="plussign"></span></strong>
        </a>
        <div id="annualmileageSelected">
            @if(is_array(app('request')->input('annual_mileage')))
                @foreach(app('request')->input('annual_mileage') as $annual_mileage)
                    <a class='annualmileageSelectedLink' id='{{$annual_mileage}}' href='#'>{{$annual_mileage}} <span class='rangeDelete'>X</span></a>
                @endforeach
            @endif
        </div>
    </div>
</div>

    <input type="hidden" name="listing_type" value="{{$listing_type}}">

           <div style="position: fixed; padding-bottom: 15px; bottom:0px;width: 80%; border-top: 2px solid #fc5185; border-right: 2px solid #fc5185; background: #fff;" id="formCtas">
           @if ($finance_type == "H")
                <a href="/hybrid" class="button b3" style="float: left; width: 40%;line-height:60px; color:#fff;">Clear all</a>
                <input type="hidden" name="finance_type" value="H">
            @elseif ($finance_type == "B")
                   @if($database == 'LCV')
                       <a href="/business-vans" class="button b3" style="float: left; width: 40%;line-height: 60px;  color:#fff;">Clear all</a>
                       <input type="hidden" name="finance_type" value="B">
                       <input type="hidden" name="database" value="{{$database}}">
                   @else
                       <a href="/business" class="button b3" style="float: left; width: 40%;line-height: 60px;  color:#fff;">Clear all</a>
                       <input type="hidden" name="finance_type" value="B">
                   @endif
            @elseif ($finance_type == "A")
                <a href="/adverse-credit" class="button b3" style="float: left; width: 40%;line-height: 60px; color:#fff;">Clear all</a>
                <input type="hidden" name="finance_type" value="A">

            @elseif ($finance_type == 'PERFORMANCE')
                <a href="/performance" class="button b3" style="float: left; width: 40%;line-height: 60px; color:#fff;">Clear all</a>
                <input type="hidden" name="finance_type" value="PERFORMANCE">
            @else
                   @if($database == 'LCV')
                       <input type="hidden" name="finance_type" value="P">
                       <a href="/personal-vans" class="button b3" style="float: left; width: 40%;line-height: 60px;  color:#fff; ">Clear all</a>
                       <input type="hidden" name="database" value="{{$database}}">
                   @else
                       <a href="/personal" class="button b3" style="float: left; width: 40%;line-height: 60px;  color:#fff;">Clear all</a>
                       <input type="hidden" name="finance_type" value="P">
                   @endif
            @endif


               <a href="#" class="button b1" onclick="closeNav();" style="float: right; width: 40%;line-height:60px; color:#fff;margin-right:15px;">Filter Results</a>
           </div>
</form>
