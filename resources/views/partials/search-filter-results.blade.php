@forelse($summaryCap as $sum)
    <div style="background: #fff;margin-bottom: 20px;" class="result-row1">
        <div class="row result-row">
            <div class="col-md-3">
                @if($database == 'LCV')
                    <a href="/quote/enquiry/{{$sum->id}}/lcv" class="rowcars">
                @else
                    <a href="/quote/enquiry/{{$sum->id}}/cars" class="rowcars">
                @endif
                    @if($sum->in_stock =='1') <div class="stocktag"><p>  In Stock </p></div> @endif
                    @if($database == 'LCV')
                        <img src="/images/capimages/LCV/{{ $sum->imageid }}.jpg" alt="{{$sum->maker}} {{$sum->range}} {{$sum->derivative}}" class="img-fluid">
                    @else
                        <img src="/images/capimages/{{ $sum->imageid }}.jpg" alt="{{$sum->maker}} {{$sum->range}} {{$sum->derivative}}" class="img-fluid">
                    @endif

                </a>
            </div>
            <div class="col-md-6 pt-2" >
                @if($database == 'LCV')
                    <a href="/quote/enquiry/{{$sum->id}}/lcv" class="rowcars">
                @else
                    <a href="/quote/enquiry/{{$sum->id}}/cars" class="rowcars">
                @endif
                    @if(isset($sum->maker))
                        <h3>{{$sum->maker}} {{$sum->range}}<br/><span class="der">{{$sum->derivative}}</span></h3>
                    @endif
                    @if(isset($sum->fuel))
                        <h6 class="features">{{$sum->fuel}} | {{$sum->transmission}} | {{$sum->bodystyle}} </h6>
                    @endif

                    @if(isset($sum->deposit_value))
                        <small><i class="fa fa-road"></i> {{$sum->deposit_value}} Months Upfront   <i class="fa fa-calendar"></i> {{$sum->term}} Month Contract  <i class="fa fa-tachometer"></i> {{$sum->annual_mileage}} Miles P/A</small>
                    @endif
                    <br/><small>Deal Provider: <strong style="color: #fc5185;">{{$sum->deal_id}}</strong></small>
                </a>
            </div>

            <div class="col-md-3 pt-2">
                @if($database == 'LCV')
                    <a href="/quote/enquiry/{{$sum->id}}/lcv" class="rowcars">
                @else
                    <a href="/quote/enquiry/{{$sum->id}}/cars" class="rowcars">
                @endif
                    @if(isset($sum->monthly_payment))
                        <h6><span class="resultprice">£{{$sum->monthly_payment}}</span> P/M</h6>
                    @endif
                    @if(isset($sum->deposit_months))
                        <h6>£{{$sum->deposit_months + $sum->document_fee}} <small>Total Upfront</small></h6>
                    @endif
                    @if(isset($sum->average_cost))
                        <h6 style="margin-bottom: 0px;">£{{$sum->average_cost}} <small>Avg Monthly Cost</small></h6>
                    @endif
                    <small> All Prices @if($sum->finance_type =='P') Inc @else Ex @endif VAT</small><br/>
                </a>
                <a href="#" data-id="{{$sum->id}}" class="addToGarageListing">
                    <i class="fa fa-car"></i> Save to garage</a>
                @role('admin')
                @if($sum->featured =='1')
                    <form method="POST" class="update-featured-form" data-value="{{$sum->id}}" action="#">
                        {{method_field('PUT')}}
                        {{ csrf_field() }}
                        <input type="hidden" name="cap-id" value="{{$sum->id}}">
                        <input class="form-control" style="display:none;" name="update-featured" id="update-featured-{{$sum->id}}"  data-value="{{$sum->id}}" value="0">
                        <input type="submit" value="- Remove Featured" name="submit" class="submit button b3 mt-3" id="submit-{{$sum->id}}" />
                    </form>
                    <div class="success-{{$sum->id}}"></div>
                @else
                    <form method="POST" class="update-featured-form" data-value="{{$sum->id}}" action="#">
                        {{method_field('PUT')}}
                        {{ csrf_field() }}
                        <input type="hidden" name="cap-id" value="{{$sum->id}}">
                        <input class="form-control" style="display:none;" name="update-featured" id="update-featured-{{$sum->id}}"  data-value="{{$sum->id}}" value="1">
                        <input type="submit" value="+ Make Featured" name="submit" class="submit button b2 mt-3" id="submit-{{$sum->id}}" />
                    </form>
                    <div class="success-{{$sum->id}}"></div>
                @endif
                @endrole
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
    </div>


@empty
    <h1>Sorry, no deals available at this point in time.</h1>
    <p>Please alter you search criteria or use the filters to find your desired vehicle</p>
@endforelse

@include('partials/garage-modals')

{{$summaryCap->appends(request()->input())->links('partials.pagination')}}