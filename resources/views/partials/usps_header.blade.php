    <div class="uspsHeader d-none d-md-block d-lg-block d-xl-block">
        <div class="row">
            <div class="col-md-2"><div class="quotebannerbottom"><i class="fa fa-check-circle" style="color: rgb(252, 81, 133); font-size: 20px;"></i> No Added Fees</div></div>
            <div class="col-md-2"><div class="quotebannerbottom"><i class="fa fa-check-circle" style="color: rgb(252, 81, 133); font-size: 20px;"></i>
                    @if($database == 'LCV')
                        Brand New Vans
                    @else
                        Brand New Cars
                    @endif
                </div></div>
            <div class="col-md-2"><div class="quotebannerbottom"><i class="fa fa-check-circle" style="color: rgb(252, 81, 133); font-size: 20px;"></i> Road Tax Included</div></div>
            <div class="col-md-2"><div class="quotebannerbottom"><i class="fa fa-check-circle" style="color: rgb(252, 81, 133); font-size: 20px;"></i> Manufacturers Warranty</div></div>
            <div class="col-md-2"><div class="quotebannerbottom"><i class="fa fa-check-circle" style="color: rgb(252, 81, 133); font-size: 20px;"></i> Free Delivery UK</div></div>
            <div class="col-md-2"><div class="quotebannerbottom"><i class="fa fa-check-circle" style="color: rgb(252, 81, 133); font-size: 20px;"></i> Breakdown Cover</div></div>
        </div>
    </div>

    <div class="uspsHeaderMobile d-xs-block d-sm-block d-md-none d-lg-none d-xl-none">
        <div class="quotebannerbottom"><i class="fa fa-check-circle" style="color: rgb(252, 81, 133); font-size: 20px;"></i> No Added Fees</div>
        <div class="quotebannerbottom"><i class="fa fa-check-circle" style="color: rgb(252, 81, 133); font-size: 20px;"></i>
            @if($database == 'LCV')
                Brand New Vans
            @else
                Brand New Cars
            @endif
        </div>
        <div class="quotebannerbottom"><i class="fa fa-check-circle" style="color: rgb(252, 81, 133); font-size: 20px;"></i> Road Tax Included</div>
        <div class="quotebannerbottom"><i class="fa fa-check-circle" style="color: rgb(252, 81, 133); font-size: 20px;"></i> Manufacturers Warranty</div>
        <div class="quotebannerbottom"><i class="fa fa-check-circle" style="color: rgb(252, 81, 133); font-size: 20px;"></i> Free Delivery UK</div>
        <div class="quotebannerbottom"><i class="fa fa-check-circle" style="color: rgb(252, 81, 133); font-size: 20px;"></i> Breakdown Cover</div>

    </div>