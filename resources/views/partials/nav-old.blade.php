<nav class="navbar navbar-expand-lg navbar-light navbar-laravel">
    <div class="container">
        @if(isset($database) && $database == 'LCV')
            <a class="navbar-brand" href="{{ url('/vans') }}">
        @else
            <a class="navbar-brand" href="{{ url('/') }}">
        @endif
            <img src="/images/alc.svg" alt="Auto Lease Compare" class="img-fluid logo logohead" style="width: 330px;">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            <span class="sr-only">{!! trans('titles.toggleNav') !!}</span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            {{-- Left Side Of Navbar --}}
            @role('admin')
            <ul class="navbar-nav mr-auto">

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {!! trans('titles.adminDropdownNav') !!}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item {{ (Request::is('roles') || Request::is('permissions')) ? 'active' : null }}" href="
{{--{{ route('laravelroles::roles.index'index) }}--}}
                                    ">
                                {!! trans('titles.laravelroles') !!}
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item {{ Request::is('users', 'users/' . Auth::user()->id, 'users/' . Auth::user()->id . '/edit') ? 'active' : null }}" href="{{ url('/users') }}">
                                {!! trans('titles.adminUserList') !!}
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item {{ Request::is('users/create') ? 'active' : null }}" href="{{ url('/users/create') }}">
                                {!! trans('titles.adminNewUser') !!}
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item {{ Request::is('themes','themes/create') ? 'active' : null }}" href="{{ url('/themes') }}">
                                {!! trans('titles.adminThemesList') !!}
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item {{ Request::is('logs') ? 'active' : null }}" href="{{ url('/logs') }}">
                                {!! trans('titles.adminLogs') !!}
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item {{ Request::is('activity') ? 'active' : null }}" href="{{ url('/activity') }}">
                                {!! trans('titles.adminActivity') !!}
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item {{ Request::is('phpinfo') ? 'active' : null }}" href="{{ url('/phpinfo') }}">
                                {!! trans('titles.adminPHP') !!}
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item {{ Request::is('routes') ? 'active' : null }}" href="{{ url('/routes') }}">
                                {!! trans('titles.adminRoutes') !!}
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item {{ Request::is('active-users') ? 'active' : null }}" href="{{ url('/active-users') }}">
                                {!! trans('titles.activeUsers') !!}
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item {{ Request::is('blocker') ? 'active' : null }}" href="{{ route('laravelblocker::blocker.index') }}">
                                {!! trans('titles.laravelBlocker') !!}
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item {{ Request::is('blog-admin') ? 'active' : null }}" href="/blog-admin">
                                Blog Administration
                            </a>
                        </div>
                    </li>
            </ul>
            @endrole
            {{-- Right Side Of Navbar --}}
            <div class="dropdown">
                <a class="dropdown-item  d-md-block d-lg-none" href="/personal">Personal Car Leasing</a>
                <a class="dropdown-item d-md-block d-lg-none" href="/business">Business Car Leasing</a>
                <a class="dropdown-item  d-md-block d-lg-none" href="/performance">Performance Car Leasing</a>
                <a class="dropdown-item  d-md-block d-lg-none" href="/hybrid">Hybrid & Electric Car Leasing</a>
                <a class="dropdown-item d-md-block d-lg-none" href="/adverse-credit">Adverse Credit Leasing</a>
                <a class="dropdown-item d-md-block d-lg-none" href="/leasing-information">Leasing FAQs</a>
                <a class="dropdown-item d-md-block d-lg-none" href="/blog">Blog</a>
                <a class="mr-4 pointer d-none d-md-none d-lg-block" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-search"></i> Car Leasing
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <a class="dropdown-item" href="/personal">Personal Car Leasing</a>
                    <a class="dropdown-item" href="/business">Business Car Leasing</a>
                    <a class="dropdown-item" href="/performance">Performance Car Leasing</a>
                    <a class="dropdown-item" href="/hybrid">Hybrid & Electric Car Leasing</a>
                    <a class="dropdown-item" href="/adverse-credit">Adverse Credit Leasing</a>
                </div>
            </div>
            <div class="dropdown">
                <a class="mr-4 pointer d-none d-md-none d-lg-block" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-search"></i>Van Leasing
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <a class="dropdown-item" href="/personal-vans">Personal Van Leasing</a>
                    <a class="dropdown-item" href="/business-vans">Business Van Leasing</a>
                </div>
            </div>
            @guest
                <a class="menu-item mr-4 pointer" data-toggle="modal" data-target="#login"><i class="fa fa-tachometer d-none d-md-none d-lg-inline"></i> My Garage</a>
            @else
                <a class="menu-item mr-4 pointer" href="/my-account/garage"><i class="fa fa-tachometer d-none d-md-none d-lg-inline"></i> My Garage</a>
            @endguest

            <div class="dropdown d-none d-md-none d-lg-block">
                <a class="mr-3 pointer" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-rocket"></i> Leasing Help
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenu3">
                    <a class="dropdown-item" href="/leasing-information">Leasing FAQs</a>
                    <a class="dropdown-item" href="/blog">Blog</a>
                </div>
            </div>
            @guest
                <button type="button" class="r5 button b3 navbutton" data-toggle="modal" data-target="#login" id="navigationLoginButton">
                    <i class="fa fa-sign-in"></i> Login
                </button>

                <button type="button" class="r5 button b2 navbutton" data-toggle="modal" data-target="#register" id="registerModalPopup">
                    <i class="fa fa-car"></i> Register
                </button>
            @endguest
            {{-- Right Side Of Navbar --}}
            <ul class="navbar-nav ml-2">
                {{-- Authentication Links --}}
                @guest

                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle d-none d-md-none d-lg-block" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>


                        <a href="/my-account/" class="dropdown-item d-md-block d-lg-none">
                            My Dashboard
                        </a>
                        <a class="dropdown-item d-md-block d-lg-none {{ Request::is('profile/'.Auth::user()->name, 'profile/'.Auth::user()->name . '/edit') ? 'active' : null }}" href="{{ url('/profile/'.Auth::user()->name) }}">
                            {!! trans('My Profile') !!}
                        </a>
                        <a href="/my-account/enquiries/" class="dropdown-item  d-md-block d-lg-none">
                            My Enquiries
                        </a>

                        @role('dealer|admin')
                        <a href="/my-account/export-quotes/" class="dropdown-item  d-md-block d-lg-none">
                            Export Enquiries
                        </a>
                        <a href="/my-account/my-pricing/" class="dropdown-item  d-md-block d-lg-none">
                            My Pricing
                        </a>
                        @endrole
                        @role('admin')
                        <a href="/my-account/dealers/" class="dropdown-item  d-md-block d-lg-none">
                            Dealers
                        </a>
                        <a href="/activity-log/" class="dropdown-item  d-md-block d-lg-none">
                            Activity Log
                        </a>
                        @endrole
                        <a class="dropdown-item d-md-block d-lg-none" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>


                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a href="/my-account/" class="dropdown-item d-none d-md-none d-lg-block">
                                My Dashboard
                            </a>
                            <a class="dropdown-item {{ Request::is('profile/'.Auth::user()->name, 'profile/'.Auth::user()->name . '/edit') ? 'active' : null }}" href="{{ url('/profile/'.Auth::user()->name) }}">
                                {!! trans('My Profile') !!}
                            </a>
                            <a href="/my-account/enquiries/" class="dropdown-item d-none d-md-none d-lg-block">
                                My Enquiries
                            </a>
                            <a href="/my-account/garage/" class="dropdown-item d-none d-md-none d-lg-block">
                                My Garage
                            </a>

                            @role('dealer|admin')
                            <a href="/my-account/export-quotes/" class="dropdown-item d-none d-md-none d-lg-block">
                                Export Enquiries
                            </a>
                            <a href="/my-account/my-pricing/" class="dropdown-item d-none d-md-none d-lg-block">
                                My Pricing
                            </a>
                            @endrole
                            @role('admin')
                            <a href="/my-account/dealers/" class="dropdown-item d-none d-md-none d-lg-block">
                                Dealers
                            </a>
                            <a href="/activity-log/"  class="dropdown-item d-none d-md-none d-lg-block">
                                Activity Log
                            </a>
                            @endrole

                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item d-none d-md-none d-lg-block" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
