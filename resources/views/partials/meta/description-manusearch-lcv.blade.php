@if($make=='Citroen')
    <meta name="description" content="Browse the best Citroen van lease deals from trusted UK suppliers with Auto Lease Compare. Including the popular Berlingo and Dispatch models. Search now.">
@elseif($make=='Fiat')
    <meta name="description" content="From the Doblo to the Ducato, browse the best Fiat van lease deals quickly and easily with Auto Lease Compare, securing you the best deals. Search now.">
@elseif($make=='Ford')
    <meta name="description" content="From Ranger trucks to Transits, secure your dream Ford at the best price delivered to your door with van leasing deals from Auto Lease Compare. Search now.">
@elseif($make=='Isuzu')
    <meta name="description" content="Browse the best Isuzu van lease deals from trusted UK providers all in one place with Auto Lease Compare, including the popular Isuzu D-Max. Search now.">
@elseif($make=='Iveco')
    <meta name="description" content="Browse the best Iveco van lease deals quickly and easily with Auto Lease Compare, securing you the best deals. Includes the Iveco Daily Search now.">
@elseif($make=='Land Rover')
    <meta name="description" content="Search the best Land Rover van lease deals including the Discovery with Auto Lease Compare. Fast and easy, saving you time and money. Search now.">
@elseif($make=='Ldv')
    <meta name="description" content="Search the LDV Vans lease deals and secure the best price today with Auto Lease Compare. Prices updated daily, saving you time and money. Search now.">
@elseif($make=='Man')
    <meta name="description" content="Search MAN vans lease deals and secure the best price today with Auto Lease Compare. Prices updated daily, saving you time and money. Search now.">
@elseif($make=='Mercedes-Benz')
    <meta name="description" content="Browse the best Mercedes-Benz van lease deals from trusted UK providers with Auto Lease Compare, includes X-Class, Sprinter & Vito models. Search now.">
@elseif($make=='Mitsubishi')
    <meta name="description" content="Secure a brand new Mitsubishi truck at the best price, delivered to your door with van leasing deals from Auto Lease Compare. Fast and easy. Search now.">
@elseif($make=='Nissan')
    <meta name="description" content="Search Nissan van lease deals and secure the best price today with Auto Lease Compare. Prices updated daily, saving you money. Search now.">
@elseif($make=='Peugeot')
    <meta name="description" content="Browse a range of Peugeot van lease deals & secure the best price today with Auto Lease Compare. Prices updated daily, saving you time & money. Search now.">
@elseif($make=='Renault')
    <meta name="description" content="Browse the best Renault van lease deals all in one place with Auto Lease Compare, including the popular Master, Trafic and Kangoo models. Search now.">
@elseif($make=='Ssangyong')
    <meta name="description" content="Secure a brand new Ssangyong at an incredible price with van leasing deals from Auto Lease Compare. Fast & simple, saving you time & money. Search now.">
@elseif($make=='Toyota')
    <meta name="description" content="Browse the best Toyota van lease deals from trusted UK providers all in one place with Auto Lease Compare, including the popular PROACE models. Search now.">
@elseif($make=='Vauxhall')
    <meta name="description" content="From the smaller Cargo to the Vivaro, browse a wide range of Vauxhall vans with incredible van leasing deals from Auto Lease Compare. Search now.">
@elseif($make=='Volkswagen')
    <meta name="description" content="From Caddys to Crafters and more, secure your dream VW van at the best price with van leasing from Auto Lease Compare. Delivered to your door. Search now.">
@else
    <meta name="description" content="Browse the best @if(isset($make)){{ $make  }}@endif van lease deals from trusted UK suppliers with Auto Lease Compare. Including the popular Berlingo and Dispatch models. Search now.">
@endif