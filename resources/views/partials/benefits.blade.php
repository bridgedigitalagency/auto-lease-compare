<div class="benefits">
    <div class="card mb-1">
        <div class="card-body">
            <h3>Cheap and hassle-free</h3>

            <p>
                <span class="tick"><i class="fas fa-check"></i></span>
                You can drive a brand new car every few years which you never thought you could afford. Once the contract lease period finishes on your lease agreement you can get another brand new car - simple  and hassle-free. There's also no hefty financial loan to buy the car.</p>
        </div>
    </div>
    <div class="card mb-1">
        <div class="card-body">
            <h3>Fixed monthly payment and flexible initial payment</h3>
            <p><span class="tick"><i class="fas fa-check"></i></span>Fixed monthly payments, which are cheaper than buying a car on finance (PCP). You can also choose how much you want to pay upfront (normally 1, 3, 6, 9 or 12 months initial payment).</p>
        </div>
    </div>
    <div class="card mb-1">
        <div class="card-body">
            <h3>Contract length of your choice</h3>
            <p><span class="tick"><i class="fas fa-check"></i></span>You can choose how long you want the car for – contracts lengths are normally 2, 3 or 4 years.</p>
        </div>
    </div>
    <div class="card mb-1">
        <div class="card-body">
            <h3>Online applications</h3>
            <p><span class="tick"><i class="fas fa-check"></i></span>You can enquire or call to get a quote in minutes, then the leasing provider will be in touch to go through a quick credit check.</p>
        </div>
    </div>
    <div class="card mb-1">
        <div class="card-body">
            <h3>Warranty protection</h3>
            <p><span class="tick"><i class="fas fa-check"></i></span>Manufacturer’s warranty is included with your lease deals free of charge. Most leases let you drop your car off at the garage for no extra cost if something goes wrong.</p>
        </div>
    </div>
    <div class="card mb-1">
        <div class="card-body">
            <h3>Lower maintenance costs</h3>
            <p><span class="tick"><i class="fas fa-check"></i></span>Much lower maintenance costs - breakdown cover often comes as part of the package (covered under manufactured warranty), servicing sometimes too if you included a maintenance package as part of the agreement.</p>
        </div>
    </div>
    <div class="card mb-1">
        <div class="card-body">
            <h3>No MOT required</h3>
            <p><span class="tick"><i class="fas fa-check"></i></span>New cars do not need a MOT in the first three years of their life.</p>
        </div>
    </div>
    <div class="card mb-1">
        <div class="card-body">
            <h3>Road tax included</h3>
            <p><span class="tick"><i class="fas fa-check"></i></span>Road Tax included as part of your lease agreement free of charge.</p>
        </div>
    </div>
    <div class="card mb-1">
        <div class="card-body">
            <h3>Avoid depreciation costs</h3>
            <p><span class="tick"><i class="fas fa-check"></i></span>Avoid deprecation costs if you were to buy the vehicle. This lowers the risk of owning a depreciating asset.</p>
        </div>
    </div>
    <div class="card mb-1">
        <div class="card-body">
            <h3>Free Delivery</h3>
            <p><span class="tick"><i class="fas fa-check"></i></span>Vehicles can be delivered straight to your home free of charge (mainland UK).</p>
        </div>
    </div>
    <div class="card mb-1">
        <div class="card-body">
            <h3>On the other hand, <small>leasing might not be the best option if you:</small></h3>
            <p>
            <ul>
                <li>Want to own the car at the end of the contract.</li>
                <li>Are considering a used vehicle, instead of new.</li>
                <li>Can arrange maintenance & servicing yourself.</li>
                <li>Aren’t confident you can make payments for the full term.</li>
            </ul>
        </div>
    </div>
</div>