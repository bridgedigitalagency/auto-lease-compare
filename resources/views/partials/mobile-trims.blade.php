@if(isset($trims))
    @php

        if(is_array(app('request')->input('trim'))) {

            $reqArr = app('request')->input('trim');
    }else{
    $reqArr = array();
    }
    @endphp
    @foreach($trims as $range=>$trims)
        <li class="trimLi trim-{{$range}}"      @if(is_array(app('request')->input('range')))
        @foreach(app('request')->input('range') as $showRange)
        @if($range == $showRange) style="display: block!important" @endif
                @endforeach
                @endif>
            <label>{{$range}}</label>

            <select name="trim[]" id="mobile-trim-{{$range}}" class="w-100 form-control mobileTrim  mt-2" multiple="multiple" >
                @foreach($trims as $trim)
                    <option value="{{$range}}_{{$trim}}" data-range="{{$range}}_{{$trim}}"
                            @if(in_array($range . '_' . $trim, $reqArr)) selected="selected" @endif
                    >{{$trim}}</option>
                @endforeach
            </select>
        </li>
    @endforeach
@endif