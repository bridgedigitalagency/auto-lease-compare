<div class="jumbotron" style="padding: 2rem 2rem 1rem">
    @if($make == 'Citroen')
        <h2  class="headerText">Citroen Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Citroen have been providing reliable commercial vehicles for almost 75 years. Founded as long ago as 1919, Citroen has grown to become one of the world’s biggest and most trusted vehicle manufacturers world wide.</p>
        <p>Today Citroen offers a range of vans that help to keep businesses of all shapes and sizes moving, so whether it’s deliveries or something more industrial, you’re certain to find the perfect van for you.</p>
        <p>With Auto Lease Compare, you can browse hundreds of Citroen van lease deals from some of the UK’s most-trusted lease providers. From the Citroen Berlingo to the Dispatch, we offer lease deals with amazing value across the manufacturer's most popular models.</p>
        <div class="hiddenContent">
            <p>Better still, our handy comparison tool gives you control over everything, from your initial deposit to how long you’d like to lease your vehicle for and much, much more, allowing you to browse hundreds of deals quickly and easily to help you find the right Citroen van lease for you.</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <h2>Trusted Citroen Van Leasing with no hidden fees</h2>

            <p>We hate hidden fees as much as you do and we’re committed to providing a transparent service. Our leasing comparison tool is completely free to use, with all the prices you see on our website supplied directly from the lease provider.</p>
            <p>Equally, we only work with companies who are as passionate about delivering a great customer experience as we are. Every leasing partner featured on our site is a member of the British Vehicle Rental and Leasing Association (BVRLA), fully-regulated by the Financial Conduct Authority (FCA), and has been carefully hand-picked by our team.</p>
            <h2>Benefits of a Citroen Van Lease</h2>
            <p>So how can Citroen van leasing benefit you? You’ll receive a brand new van delivered to your home or workplace, with no MOT due for three years, no road tax to pay and manufacturer’s warranty included.</p>
            <p>All you have to do is pay your monthly rental for the duration of your agreement term, then return your van in good condition at the end of your lease period.</p>
            <p>Browse our very latest Citroen van leases below, or get in touch if you need any additional information.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Fiat')
        <h2 class="headerText">Fiat Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Browse the very latest Fiat van lease deals below and drive away at the best price, today.</p>
        <p>Renowned for versatility, Fiat has developed a strong reputation for producing robust and reputable vans to meet all kinds of commercial requirements.</p>
        <div class="hiddenContent">
            <p>From smaller vans like the Fiat Doblo and Fiorino, to the larger Ducato with dropside and tipper, and the Fullback pickup truck for easy loading, there’s a Fiat lease deal for everyone at Auto Lease Compare.</p>
            <p>Learn more <a href="https://www.autoleasecompare.com/leasing-information">about leasing’s benefits.</a></p>
            <h2>Find your perfect Fiat Van Lease</h2>
            <p>Van leasing is a great way to drive a brand new Fiat at an affordable cost.</p>
            <p>Thanks to our advanced comparison tool, it’s easy to find your perfect Fiat van lease. Simply use our free comparison tool to set your preferences, from engine size to initial deposit, lease term and much more, then simply browse the best deals for you, returned in just a few seconds.</p>
            <p>Our prices are updated daily and our tool is completely free to use, so you can be sure that you’re always getting the best price with no hidden fees.</p>
            <p>Once you’ve chosen your Fiat lease, we’ll put you in touch with the lease provider who will finalise things and arrange delivery of your brand new vehicle.Then just keep up with your monthly payments and return the vehicle in a good condition at the end of the agreed lease period.</p>
            <p>For us, trust is a must, which is why, like all of the leasing providers featured on our website, we’re fully-regulated by the Financial Conduct Authority (FCA). Our leasing partners have all been exclusively hand-picked and are members of the <a href="https://www.bvrla.co.uk/">BVRLA.</a></p>
            <p>Browse our current Fiat Van leases below, or get in touch for more information.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Ford')
        <h2 class="headerText">Ford Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Ford have been manufacturing some of the world’s most popular vans since the 1950s and have since become one of the UK’s most popular van suppliers, with an extensive range to suit businesses of all shapes and sizes.</p>
        <p>Whatever you’re looking for, with Auto Lease Compare, you can secure your dream Ford van lease in just a few simple steps, with prices updated daily so that you’re always in the driver’s seat when it comes to getting the best price.</p>
        <p>Our innovative comparison tool allows you to browse and filter hundreds of Ford van lease deals in just a couple of clicks by giving you control over everything from fuel and transmission type to budget, mileage and much more, saving you time, money and energy.</p>
        <div class="hiddenContent">
            <p>From the trusty Ford Ranger pickup to the van edition of the Fiesta, we have a wide selection of Ford lease vans available from the UK’s most trusted leasing companies.</p>
            <p>If you’re looking for something larger, we offer plenty of Ford Transit leases too, including Connect, Courier and Custom lease deals.</p>
            <p>Learn more <a href="https://www.autoleasecompare.com/leasing-information">about leasing’s benefits.</a></p>
            <p>Ford van leasing allows you to enjoy the experience of driving a brand new Ford at a fraction of the cost, with delivery to your door, no MOT due for three years and no road tax to pay, plus manufacturer’s warranty cover.</p>
            <p>Our service is also completely free to use. Every price you see on our site is supplied by one of our trusted, hand-picked leasing partners - all we do is connect you with them.</p>
            <p>Once your lease is confirmed, all you need to do is continue to make your monthly payment on the agreed date, then return your car at the end of the lease period. It’s that easy!</p>
            <p>And for extra peace of mind, you can rest easy in the knowledge that all of our leasing partners are members of the BVRLA and conducted by the FCA.</p>
            <p>Sound good? Browse the latest Ford van lease deals below, or get in touch for more information.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Peugeot')
        <h2 class="headerText">Peugeot Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Enjoy a smoother leasing experience than you ever thought possible with a Peugeot van lease from Auto Lease Compare.</p>
        <p>One of the world’s most-renowned producers of both vans and cars, Peugeot have been at the forefront of the automotive industry for many years. Efficiency has played a large part in this thanks to Peugeot’s continued ability to combine a smooth driving experience with low running costs.</p>
        <p>Now, with our powerful online tool, you can browse thousands of Peugeot van lease deals from trusted leasing providers in a matter of seconds.</p>
        <div class="hiddenContent">
            <p>Better still,from engine size to transmission, body style to budget and much more, you can completely refine your search to quickly - and easily - return deals that match your personal requirements, saving you time and money.</p>
            <h2>Find your perfect Model</h2>
            <p>If you’re looking for a larger Peugeot van then a Peugeot Boxer lease is your best bet. The Boxer is available in eight different body styles. Lightweight and durable, the Boxer is as versatile as they come and has previously been used for all sorts of applications, from horseboxes to ambulances.</p>
            <p>If you need something smaller, a Peugeot Partner lease might be for you. Having been around for over 20  years, the Partner has developed a reputation for reliability, while you’ll also benefit from Peugeot’s latest engine technology for an even smoother driving experience.</p>
            <p>Available with an electric or diesel motor, the competitively-priced  Peugeot Expert aims to deliver a customisable experience and a wealth of space, whatever you need it for. With various high-tech features, including modern safety equipment, hands-free automatic sliding doors and a head-up display</p>
            <p>Learn more <a href="https://www.autoleasecompare.com/leasing-information">about leasing’s benefits.</a></p>
            <h2>Benefits of a Peugeot Van Lease</h2>
            <p>Peugeot van leasing allows you to drive a brand new van at a fraction of the price, without having to worry about vehicle depreciation and road tax, plus MOT for the first three years. We’ll even arrange delivery straight to your door!</p>
            <p>Our prices are updated regularly too, so you can be sure that you’re always getting the best deal, and we’ll never charge you a penny. We simply put you in touch with the leasing company, who finalise the legal bits and schedule delivery of your van. It’s that easy!</p>
            <p>Trust and transparency are huge to us. We only work with trusted, hand-picked leasing partners with a proven track record for delivering a glowing customer experience. Crucially, every one of our partners is a member of the BVRLA and conducted by the FCA.</p>
            <p>So what are you waiting for? Start browsing the latest Peugeot van lease deals below, or get in touch if you need any help with anything.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Land Rover')
        <h1 class="headerText">Land Rover Van Leasing</h1>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Land Rover probably aren’t the first to come to mind when you think ‘van’, but through the commercial edition of its popular Discovery model, they’re slowly starting to change that.</p>
        <p>Subtle and incredibly stylish, the Land Rover Discovery Commercial combines comfort with practicality, serving as a van-based edition of the luxury SUV range.</p>
        <p>So what’s the difference?. Second and third-row seating give way, replaced instead by a wire mesh bulkhead and privacy glass. In doing so, land rover van leasing reduces costs for commercial users by providing the same VAT benefits whilst lowering vehicle tax.</p>
        <div class="hiddenContent">
            <p>Available with SD4 and SD4 engines with a choice of trims, leasing the Discovery Commercial continues to provide off-road excellence in a completely new way.</p>
            <p>Learn more <a href="https://www.autoleasecompare.com/leasing-information">about leasing’s benefits.</a></p>
            <h2>Browse Discovery Commercial Lease Deals Quickly & Easily</h2>
            <p>It’s now easier than ever before to find your perfect Land Rover van lease. Thanks to our innovative online tool, you can search through hundreds of Discovery Commercial lease deals in just a couple of clicks, while our enhanced filtering systems allow you to refine your search like never before.</p>
            <p>We’ll then do the rest, rapidly scanning thousands of lease deals from reliable, UK-based providers and identifying the ones that match your criteria.</p>
            <p>All of this serves to save you time, energy and money, so you can find the deals that match your personal requirements in just a few seconds.</p>
            <h2>Benefits of Contract Hire</h2>
            <p>Free delivery straight to your home or workplace. Road tax and manufacturer's warranty included. No MOT due for three years.</p>
            <p>These are just some of the benefits of Discovery Commercial contract hire. But if you need further convincing, you’ll also get to drive a brand new Discovery Commercial at a fraction of the cost for the duration of your lease, plus you’ll avoid any depreciation in the value of the vehicle.</p>
            <p>Our deals are also updated daily to ensure you’re always getting the best price, while we only work with carefully-selected leasing partners with a passion for delivering an amazing service to their customers. In fact, all of our partners are members of the BVRLA and regulated by the Financial Conduct Authority for extra peace of mind.</p>
            <p>Ready to explore the world? Browse our Land Rover Discovery Commercial lease deals below, or get in touch if you need any additional support from our friendly team.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Mercedes-Benz')
        <h2 class="headerText">Mercedes Benz Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Mercedes van leasing opens up a whole new world of possibilities though an impressive selection of reliable vans and pickup trucks.</p>
        <p>Undeniably one of the leaders of the luxury automotive market, Mercedes have now tranferrered that same prestige and power to create a fleet of vans for both personal and commercial use, with numerous sizes available to tailor for all requirements.</p>
        <div class="hiddenContent">
            <p>With our innovative comparison tool, you can save time, money and energy by searching through thousands of lease deals from trusted providers quickly and efficiently.</p>
            <p>Our extensive filtering options put you in the driver’s seat, allowing you to find the best Mercedes van lease deals that match your personal requirements in just a couple of clicks.</p>
            <h2>Citan, Vito & Sprinter Van Leasing</h2>
            <p>Slick and stylish, the Mercedes Vito and Citan models are the physical proof that sometimes less is more, and are the standout choices for those looking for a smaller van without compromising on comfort.</p>
            <p>If you’re looking for something larger than a Sprinter van lease should be your first port of call. Available in multiple styles, the Sprinter has been helping to power businesses for over 30 years thanks to a combination of  impressive storage and revolutionary technology.</p>
            <p>The X Class is labelled by Mercedes as the first ever premium pickup. Tough and durable, it’s well-suited to all types of terrain without sacrificing style.</p>
            <p>Learn more <a href="https://www.autoleasecompare.com/leasing-information">about leasing’s benefits.</a></p>
            <p>By taking out a Mercedes Benz van lease with us, you’ll be getting more than just a vehicle. You’ll be getting a stunning, brand new Mercedes delivered straight to your home or workplace, with manufacturer’s warranty and tax included. You’ll be getting the best price, because our listings are updated daily to reflect the latest offers from our carefully-selected partners, all of who are members of the BVRLA and FCA regulated. And you'll be benefitting from the latest technology, with no MOT due for the first three years.</p>
            <p>So how does it work? Our comparison tool is completely free to use. All you need to do is find your perfect deal, then we’ll put you in touch with the provider of your selected Mercedes van lease who will finalise things and arrange delivery of your vehicle. Then all you have to do is keep up with your monthly payments and return your vehicle in good condition at the end of the leasing period. It’s that easy!</p>
            <p>Choose from the options below to start browsing our latest Mercedes Benz van lease deals. If you need some additional help, don’t hesitate to contact us.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Mitsubishi')
        <h2 class="headerText">Mitsubishi Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Highly regarded for its robust and reliable vehicles, Mitsubishi vans have built up a strong reputation in the commercial sector.</p>
        <p>From the comfort of the popular Shogun Sport and the spacious Outlander to the latest L200 pickup truck, our powerful comparison tool allows you to search the very best Mitsubishi van lease deals on the market faster than ever before, saving you energy, time and money.</p>
        <div class="hiddenContent">
            <h2>Benefits of Mitsubishi Van Leasing</h2>
            <p>A Mitsubishi van lease comes with plenty of benefits. You’ll receive a brand new Mitsubishi vehicle straight to your chosen address with free delivery, road tax included and manufacturer’s warranty too! Plus, because our deals are updated daily, you can be sure that you’re always getting the best price available.</p>
            <p>Learn more <a href="https://www.autoleasecompare.com/leasing-information">about leasing’s benefits.</a></p>
            <p>We’ll never charge you a penny for our services either. All of our deals are supplied directly by our carefully-selected  leasing partners, all of who are FCA regulated and members of the BVRLA for extra peace of mind. We simply put you in touch with them to finalise things, then you’re good to go.</p>
            <p>And thanks to our near-endless filtering options, you can quickly refine your search and strip out any deals that don’t perfectly match your requirements.</p>
            <p>So, if it sounds like Mitsubishi van leasing could be for you then you can use the options below to start browsing, but don’t hesitate to contact us if you need any extra help or information.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make=='Nissan')
        <h2 class="headerText">Nissan Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Nissan offers one of the widest selections of lease vans on the market with a reliable answer to the needs of every business.</p>
        <p>With over 85 years’ experience providing top of the range commercial vehicles, Nissan has cemented its reputation as a leading supplier of vans and pickups worldwide with a range of useful features and attractive designs.</p>
        <div class="hiddenContent">
            <p>With Auto Lease Compare, you can browse Nissan van lease deals from hundreds of market-leading providers in just a few seconds, using our detailed filtering options to identify the most suitable lease offers for you quickly and easily.</p>
            <p>Our lease comparison tool is completely free to use, while all of our leasing partners have been carefully selected based on their track record for providing an exceptional service with the customer at the core.</p>
            <p>Learn more about leasing’s benefits.</p>
            <p>Learn more <a href="https://www.autoleasecompare.com/leasing-information">about leasing’s benefits.</a></p>
            <p>Nissan’s selection of vans is perhaps the most versatile, meeting the needs of businesses of all shapes and sizes, so it’s easy to find a Nissan van lease that will work for you.</p>
            <p>Need a pickup truck? The award-winning <strong>Nissan Navara</strong> utilises the latest technology to create a robust, sturdy vehicle that provides unbeatable value.</p>
            <p>If you’re looking for a smaller van, the all electric <strong>e-NV200</strong> is perfect for use in urban environments with responsible engineering to deliver a cost-effective, smooth and quiet driving experience, while the <strong>NV250</strong> is another great option with an attractive exterior and impressive cargo space.</p>
            <p>The <strong>NV400</strong> is Nissan’s largest van available for lease, offering an abundance of storage. It’s also available in various styles and specifications, with plenty of high-tech features for good measure.</p>
            <h2>The Benefits (H2)</h2>
            <p>Leasing offers you the chance to drive a brand new Nissan van at an affordable price, with no MOT due for three years and Manufacturer’s warranty included, as is road tax.</p>
            <p>You’ll also benefit from free delivery straight to your home, office, or any other place of work, making your life simpler.And because our deals are updated daily, you’ll always be getting the best available price</p>
            <p>Choose from the below options to start browsing our latest Nissan van contract hire offers, or get in touch if you need any additional help.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make=='Renault')
        <h2 class="headerText">Renault Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Find and secure your perfect Renault van lease in just a few simple steps with Auto Lease Compare.</p>
        <p>Having manufactured commercial vans for over 60 years, Renault offers a wide range of versatile options that cater for businesses of all types.</p>
        <p>If you need a mid-sized van, the Renault Trafic is the ideal choice for all types of trade thanks to it’s long list of customisable specifications.</p>
        <div class="hiddenContent">
            <p>The smallest of Renault’s fleet is the Renault Kangoo, offering economical brilliance and impressive storage, alongside air conditioning and bluetooth on selected business models.</p>
            <p>If you need a large van, you should consider a Renault Master, with impressively low-running costs combined with a smart, stylish interior that you might not expect from a bigger van.</p>
            <p>Learn more <a href="https://www.autoleasecompare.com/leasing-information">about leasing’s benefits.</a></p>
            <h2>Why Auto Lease Compare?</h2>
            <p>Our innovative tool saves you time, money and energy by allowing you to browse thousands of Renault van lease deals in just a few seconds, all brought to you by the UK’s most trusted leasing providers. You can even narrow deals down to the ones that match your personal requirements thanks to our extensive filtering options, so whether it’s the Master, Kangoo, or Trafic, we’ll help you find the right lease deal for you.</p>
            <p>All of our hand-picked leasing partners share a passion for delivering a brilliant customer experience and are members of the BVRLA and we’r regulated by the Financial Conduct Authority (FCA) for extra peace of mind..</p>
            <h2>What are the benefits of Renault van leasing?</h2>
            <p>When you lease a Renault through our site, you’ll receive plenty of benefits, including:
            <ul>
                <li>Enjoy a brand new vehicle for an agreed monthly payment</li>
                <li>Listings updated daily, so you’ll always get the best price</li>
                <li>Free and flexible delivery to your home or workplace</li>
                <li>Manufacturer’s warranty</li>
                <li>Road tax included</li>
                <li>No MOT due for the first three years</li>
            </ul>
            <p>Choose from the vehicle options below to start browsing the latest Renault van contract hire opportunities, or get in touch for more information.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

    @elseif($make=='Ssangyong')
        <h2 class="headerText">Ssangyong Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Browse hundreds of deals and find your ideal Ssangyong pickup lease with Auto Lease Compare.</p>
        <p>Tough, durable and practical, a Ssangyong Musso lease provides the perfect pickup, whatever you need it for. The Musso may not be the most famous name in the market, but it’s sturdiness is rivalled by few, and the vehicle’s seven-year warranty is testament to that.</p>
        <div class="hiddenContent">
            <p>When you choose a lease deal through us, you’ll receive a brand new Ssangyong delivered straight to your door with road tax and manufacturer’s warranty included, plus no road tax due for three years. You’ll also benefit from the latest vehicle technology and safety features.</p>
            <p>Better still, our super smart comparison tool allows you to browse and filter hundreds of Ssangyong musso lease deals from our trusted UK partners in just a couple of clicks, with listings updated daily to ensure your always getting the best price</p>
            <p>And every partner featured on our website has been carefully vetted to ensure exceptional service. All of the leasing companies we work with are members of the BVRLA and regulated by the Financial Conduct Authority.</p>
            <p>Learn more <a href="https://www.autoleasecompare.com/leasing-information">about leasing’s benefits.</a></p>
            <h2>How Does It Work?</h2>
            <p>Leasing a Ssangyong with Auto Lease Compare is quick and easy. Simply browse and filter deals until you find your perfect lease, then use the contact details provided to contact the lease provider who will finalise things and cover all the legal bits, as well as scheduling delivery. Then all you need to do is enjoy your brand new pickup and continue to make the agreed monthly payments until the end of the lease period, at which point you’ll return your vehicle.</p>
            <p>Click below to start browsing our latest Ssangyong musso contract hire deals or contact us to learn more. For cars and additional models, be sure to check out our other <a href="https://www.autoleasecompare.com/car-leasing/ssangyong">Ssangyong lease deals</a>.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

    @elseif($make=='Toyota')
        <h2 class="headerText">Toyota Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Founded in Japan in 1937, Toyota  is one of the most trusted names in the automotive industry worldwide. Renowned for reliability, they offer a wide range of products including cars, hybrids, vans and other commercial vehicles.</p>
        <p>When it comes to vans, they have a range of different sizes and models to suit any business or use. Whether you are looking for a small van for personal use, a larger van for business or anything in between, you are sure to find the perfect match with Toyota.</p>
        <div class="hiddenContent">
            <p>With Auto Lease Compare you can browse the best Toyota van lease deals from the most trusted lease providers. Our comparison tool is quick and easy to use, allowing you to set your budget, contract length, initial deposit amount and more so you can be sure you’re getting the perfect deal for you.</p>
            <p>Once you have found your ideal Toyota van lease deal, we offer free flexible delivery straight to your door or business premises.</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How van and car leasing works.</a></p>
            <h2>Finding the Right Model for You</h2>
            <p>Finding the right Toyota van lease deal depends on your requirements. For those who need a larger van the Toyota Proace is a great option. We offer Toyota Proace leases for a range of body styles and length combinations. It also boasts an advanced diesel engine to promote fuel efficiency, making it a popular choice for businesses.</p>
            <p>Looking for something slightly smaller? Check out the Toyota Proace City. Designed to easily navigate city streets, our Toyota Proace City lease deals are ideal for independent business owners and those who do not need to carry large items around.</p>
            <p>Alternatively, why not try the Toyota Hilux? With the ability to navigate off-road terrain with a large flatbed, our Toyota Hilux lease deals are suitable for both business and pleasure. Carry around tools and work equipment easily during the week, and swap them for a mountain bike or surfboard at the weekend for some off-road activities!</p>
            <h2>The Benefits of a Toyota Van Lease</h2>
            <p>There are a wide range of benefits to leasing a Toyota van, not only do you get to drive a brand new vehicle, but you can do it at an affordable price. We update our Toyota van lease deals daily so you can always be sure that you’re getting the very best price.</p>
            <p>Our flexible leasing options also means you have complete control. You can set your desired contract length, deposit amount, specify van features and more so you can get exactly what you’re looking for.</p>
            <p>We will deliver your van direct to your door or business for free and you can rest easy knowing that all our vans come with a manufacturer warranty, road tax included and no MOT needed for the first 3 years.</p>
            <p><a href="https://www.autoleasecompare.com/leasing-information">Find out more about the benefits of van leasing</a></p>
            <p>Click below to browse our Toyota van lease deals. Not quite what you’re looking for? Check out our <a href="https://www.autoleasecompare.com/car-leasing/toyota">Toyota car lease deals.</a></p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

@elseif($make=='Vauxhall')
        <h2 class="headerText">Vauxhall Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Vauxhall is one of the UK’s largest and most well known vehicle manufacturers. Founded in 1857 by Alexander Wilson in London, Vauxhall vehicles are now only available in the UK, with the brand known as Opel internationally.</p>
        <p>Vauxhall are known for producing high quality, reliable vehicles in a wide range of styles. The Vauxhall range encompasses cars and vans, with options ideal for both business and personal use.</p>
        <div class="hiddenContent">
            <p>With Auto Lease Compare you can compare the very best Vauxhall van leasing deals using our quick and easy online tool. With options to suit any budget, you could be driving around a brand new Vauxhall van at an affordable price in no time!</p>
            <p>We also offer free delivery to your home address or business location, as well as manufacturer warranties, and road tax included.</p>
            <p>Learn more <a href="https://www.autoleasecompare.com/leasing-information">about leasing’s benefits.</a></p>
            <h2>Vauxhall Van Leasing</h2>
            <p>We update our deals daily so you can be sure you’re always getting the best price! We ensure that all of our leasing partners have been thoroughly vetted and we are regulated by the Financial Conduct Authority (FCA) for extra peace of mind.</p>
            <p>Our range of Vauxhall van leasing deals includes the Vauxhall Combo Cargo, Vauxhall Movano and the Vauxhall Vivaro.</p>
            <p>Our Vauxhall Vivaro lease deals are extremely popular, with multiple options and features to choose from. The Vivaro is practical and cost efficient, with leading fuel economy; perfect for business use.</p>
            <p>The smallest in the range is the Combo Cargo. Offering a payload of up to c1.0T, a low loading height, sliding side doors and rear door access, our Vauxhall Combo Cargo lease deals are ideal for those who need to carry heavy loads but don’t want a huge van.</p>
            <p>For those who need a large van, the Movano is the ideal choice. The largest of the range, designed with driver comfort in mind, a capacity of up to 22m³ and a whole host of safety features.</p>
            <h2>How Does it  Work?</h2>
            <p>Leasing a Vauxhall van from Auto Lease Compare couldn’t be easier. Simply select your desired model, inputting your minimum and maximum prices if you have a budget in mind. You can also select whether the van will be for business or personal use, set an initial deposit amount and dictate van features such as engine size.</p>
            <p>Once you have found a van lease deal you are happy with, use the contact details provided to get in touch with the lease provider. They will finalise the details of your contract and specify any legal requirements. Once agreed you will be required to pay the agreed monthly payments until the end of your contract, after which you will return the vehicle.</p>
            <p>Click below to browse our Vauxhall van leasing deals, or check out our <a href="https://www.autoleasecompare.com/car-leasing/vauxhall">Vauxhall car leasing deals</a> instead.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
@elseif($make=='Volkswagen')
        <h2 class="headerText">Volkswagen Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Volkswagen is a German automanufacter, founded in 1937. They are one of the largest automakers worldwide, and certainly one of the most well known. Responsible for iconic cars such as the VW Beetle, their range also includes vans, other commercial vehicles and even electric cars.
        <p>Volkswagen vans are known to be reliable and well-made; a popular choice for both business and personal use. So whether you’re looking for a pick up truck to transport your surfboard or bike, or a van for moving large or heavy items, you are sure to find something to suit!
        <p>With our online comparison tool it couldn’t be easier. You can select your desired model and features, as well as setting a maximum budget, an initial deposit amount and ideal contract length. In seconds you will be met with hundreds of Volkswagen van leasing deals to choose from.
        <div class="hiddenContent">
            <p>We only work with trusted leasing providers so you can rest easy. We also update our deals daily to ensure we always offer the very best price!</p>
            <h2>Find the Right Model for You</h2>
            <p>Finding the right Volkswagen van lease will depend on your requirements. We offer a wide range of VW deals to choose from so you can find the perfect fit for your needs.</p>
            <h3>VW Transporter Leases</h3>
            <p>The VW Transporter is available in both Diesel and Electric models. With ample space in both and multiple design options, its versatility makes it the ideal choice for businesses. It has been designed with both comfort and safety in mind with the latest driver assistance technology.</p>
            <h3>VW Amarok Leases</h3>
            <p>The Volkswagen Amarok is a premium pick up truck that can navigate off-road terrain just as easily as on. With high ground clearance and 4MOTION all-wheel drive, the Amarok has traction in almost any situation, even with a heavy payload. This results in driver comfort, no matter what the cargo is.</p>
            <h3>VW Caddy Leases</h3>
            <p>The Volkswagen Caddy makes an ideal van for work, with it’s versatile design options, sliding door side and rear door access. The smaller size lends itself to busy city streets, but doesn’t compromise on space, with ample room in the back for tools, equipment or anything else you need to transport.</p>
            <h3>VW Crafter Leases</h3>
            <p>Award-winning and practical, the Volkswagen Crafter blends safety, usability and comfort all in one. With options such as a high roof and auto and manual gearboxes, the Crafter can fit into almost any business.</p>
            <h2>The Benefits of a Volkswagen Van Lease</h2>
            <p>Not only can you drive around a brand new Volkswagen van but you can do it at an affordable price! We also offer manufacturer warranties, with road tax included in the price, and there is no need to MOT your vehicle for the first 3 years of your contract.</p>
            <p>Our daily price updates also means you can be sure you’re getting the best deal.</p>
            <p><a href="https://www.autoleasecompare.com/leasing-information">Learn more about the benefits of van leasing</a></p>
            <p>Browse our full range of Volkswagen van leasing deals by clicking below. Not quite what you’re looking for? Try our <a href="https://www.autoleasecompare.com/car-leasing/volkswagen">Volkswagen car leasing deals</a>.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

    @elseif($make=='Isuzu')
        <h2 class="headerText">Isuzu Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Founded in Tokyo in 1916, Isuzu is a Japanese automanufactuer that specialises in pick-up trucks. They have previously worked with both Toyota and Honda in order to further research and innovation in the automotive industry.</p>
        <p>Designed with practicality and durability in mind, Isuzu pick-ups are reliable and sturdy, perfect for both on-road and off-road use. With a range of features, you can tailor your Isuzu pick up to suit your needs.</p>
        <div class="hiddenContent">
            <p>Simply select your budget, deposit amount and contract length. You can then filter through vehicle features until you find the ideal Isuzu lease deal for you.</p>
            <p><a href="https://www.autoleasecompare.com/leasing-information">Learn more about the benefits of leasing</a></p>
            <h2>Isuzu Pickup Lease Deals</h2>
            <p>We offer hundreds of leasing options and deals to suit any need and budget. Our Isuzu lease deals are affordable and easy to understand with our online comparison tool. We also offer flexible delivery, delivering straight to your door or business premises.</p>
            <h3>Isuzu D-Max Lease</h3>
            <p>The Isuzu D-Max was awarded the 2021 Pick-up of the Year by 4x4 Magazine, and is one of the strongest, safest and most reliable pick-up trucks on the market. With the ability to carry heavy loads, with a large rear flatbed, the D-Max is versatile and practical.</p>
            <h2>How Does it Work?</h2>
            <p>To lease an Isuzu through Auto Lease Compare simply select them as a manufacturer, specify your model, budget and desired features. You can then explore lease deals before settling on the right one for you. Once you have made your decision, use the contact details supplied to get in touch with the lease provider. They will take you through the leasing process, ensuring all legal requirements are met.</p>
            <p>Once your lease contract has been finalised, your brand new vehicle will be delivered to you. You will be required to pay agreed monthly payments, as per the terms of your contract and will have to return the vehicle at the end of the agreed period.</p>
            <p>Click below to check out our Isuzu lease deals. Looking for something else? Check out our other <a href="https://www.autoleasecompare.com/">car leasing</a> and <a href="https://www.autoleasecompare.com/vans">van leasing deals</a>.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make=='Iveco')
        <h2 class="headerText">Iveco Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>A well known manufacturer of commercial vehicles, Iveco is the go-to name for businesses. Offering lorries, trucks and vans, Iveco products range from heavy goods vehicles to those designed for lighter loads.</p>
        <p>Designed with businesses in mind, Iveco vans are reliable and sturdy, taking into account everything you might need to help your business run smoothly. From capacity and payload to driving experience and safety, Iveco has practicality at the heart of it’s brand.</p>
        <div class="hiddenContent">
            <p>Get a price to lease an Iveco van in seconds with Auto Lease Compare. All you have to do is select your desired model, input your budget and adjust any filters to your requirements and you will be met with hundreds of lease deals.</p>
            <h2>Iveco Daily Lease Deals</h2>
            <p>The Iveco Daily is one of the most versatile vans on the market. With capacity options from 7.3 to 19.6 m³ and high performance front suspension, the Iveco Daily is ideal for those who need to move heavy payloads.</p>
            <p>Not only is the Daily large but it has additional storage in the cabin, and has both rear and side access doors for easy loading. It also has a high efficiency feature that can help you save up to 10% in fuel costs.</p>
            <p>We have a range of Iveco Daily lease deals to choose from, all from trusted providers so you can be confident in your choice. We also update our deals every day to ensure we only offer the very best prices available.</p>
            <h2>Why Should You Lease an Iveco Van?</h2>
            <p>We should be asking ‘why wouldn’t you?’</p>
            <p>By leasing an Iveco van through Auto Lease Compare you get a brand new van without the high price! We offer a range of leasing options to suit almost any budget, giving you control over filters so you can get the best deal for you.</p>
            <p>All of our vans also come with road tax included, manufacturer warranties and no need for an MOT within the first 3 years. If that’s not reason enough, we also offer free, flexible delivery straight to your home or business premises.</p>
            <p><a href="https://www.autoleasecompare.com/leasing-information">Learn More: The Benefits of Leasing</a></p>
            <p>Click below to browse our Iveco lease deals or check out some of our other <a href="https://www.autoleasecompare.com/vans">van leasing deals</a>.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make=='LDV')
        <h2 class="headerText">LDV Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>LDV vehicles are manufactured by SAIC, the largest automotive manufacturer in China. Trust is built into their brand, by consistently delivering high quality, safe vehicles with top specifications. They specialise in providing commercial vehicles to customers with a range of needs and business requirements.</p>
        <p>Technological innovation is at the forefront of the LDV organisation, with state of the art technology across all models. They promote the use of alternative fuels such as EV, hybrid and hydrogen in order to help businesses move into the future. The LDV range includes vans, minibuses and chassis cabs.</p>
        <div class="hiddenContent">
            <p>We offer LDV van lease deals to help you secure a high quality van at an affordable price for your business. You can filter the deals by inputting information such as your budget, initial deposit amount and contract length, and within seconds you will be shown the LDV van lease deals that match your requirements. It couldn’t be easier!</p>
            <h2>Why Lease an LDV Van?</h2>
            <p>There are many benefits to leasing an LDV van; not only do you get to drive a brand new, top specification van, built with state of the art technology, but you get to do it at an affordable monthly price. We work with trusted lease providers and update prices daily so you can be sure that with Auto Lease Compare, you will always get the best price!</p>
            <p>All of our vehicles also come with road tax included, manufacturer warranties and no need for an MOT for the first 3 years. We also offer free, flexible shipping so when your LDV van lease is confirmed, we will deliver your vehicle directly to your home or business address.</p>
            <p><a href="https://www.autoleasecompare.com/leasing-information">Learn More about The Benefits of Leasing</a></p>
            <h2>How Does it Work?</h2>
            <p>Leasing an LDV van with Auto Lease Compare is super quick and easy. All you need to do is select the model, input any necessary requirements such as engine type or budget, and we will show the deals that are best suited to you.</p>
            <p>Once you have selected your chosen deal, you will be able to see the contact information for the lease provider. Simply get in touch via the information shown and they will go through the terms of the contract with you and ensure any legal requirements are met. Once the contract is confirmed your vehicle will then be delivered.</p>
            <p>You will be required to pay the agreed price on a monthly basis until the end of your contract and will have to return the vehicle when your contract expires.</p>
            <p>Browse our LDV van lease deals below. Can’t find what you’re looking for? Take a look at our other <a href="https://www.autoleasecompare.com/vans">van lease deals</a>.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make=='Man')
        <h2 class="headerText">MAN Van Leasing</h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Founded in Germany in 1758, Man first began producing heavy machinery such as steam engines and locomotives. Over their history they have become known for producing buses and trucks however, in 2016 a van was introduced to their product offering in order to complete their range of commercial vehicles.</p>
        <p>Man are focused on providing innovative transport solutions that are made to the highest quality for reliability and comfort. Man vans include state of the art technology that can be tailored to the needs and requirements of a business.</p>
        <div class="hiddenContent">
            <p>We offer a range of Man van leasing options to help you get the best deal! We work with trusted lease providers and are regulated by the Financial Conduct Authority for added peace of mind.</p>
            <p>You can have complete control over your lease deal using our easy online tool. Input your budget, deposit amount, contract length to be shown Man van lease deals that are perfectly suited to you.</p>
            <h2>Man Van Leasing</h2>
            <p>We offer a wide range of Man leasing options, so whether you are looking for a van for business or personal use, you are sure to find an option to suit with our trusted leasing partners.</p>
            <h3>Man TGE Van Lease</h3>
            <p>The Man TGE is a highly practical vehicle, perfect for everyday work requirements. With a large load capacity, comfortable driver’s cab and innovative storage solutions, the TGE is ideal for a range of industries. We have Man TGE van lease options to suit almost any budget, allowing you to choose your van specification to match your needs.</p>
            <h2>The Benefits of Van Leasing</h2>
            <p>Choosing to lease a Man van can have many benefits. The monthly payments make van leasing a more affordable option, without having to compromise on quality. You can drive around a brand new, top-specification van for a low upfront cost.</p>
            <p>Not only is it great for you as an individual but it is great for business too, having state of the art technology and equipment on hand to help you do your job!</p>
            <p>In addition to this, by leasing a Man van with Auto Lease Compare you can be confident you’re getting the best price. We update all of our prices daily to ensure they are up to date and correct. Road tax is also included with all of our vehicles, along with a manufacturer warranty and no need for an MOT for the first 3 years.</p>
            <p>Once you have chosen the perfect deal for you, we will deliver your new Man van to your home or business premises for no extra cost!</p>
            <p><a href="https://www.autoleasecompare.com/leasing-information">Learn More about The Benefits of Leasing</a></p>
            <p>Browse our Man van leasing deals below or check out our other <a href="https://www.autoleasecompare.com/vans">van lease deals</a> for something a little different!</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

    @endif


</div>
