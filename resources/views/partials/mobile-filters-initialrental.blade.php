<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a class="initialrentalFilterHeader mb-3 back"><strong><i class="fa fa-angle-double-left"></i> Back</strong></a>
            <strong class="mb-4">Select Initial Rental</strong>
            @php
                if(is_array(app('request')->input('deposit_value'))) {
                    $reqArr = app('request')->input('deposit_value');
                }else{
                    $reqArr = array();
                }
            @endphp
        <select multiple name="deposit_value[]" id="deposit_value" class="mobileInitialRental mt-2">
            @if(isset($deposit_values))
                @foreach($deposit_values as $deposit)
                    <option @if(in_array($deposit->deposit_value, $reqArr)) selected="selected" @endif value="{{$deposit->deposit_value}}">{{$deposit->deposit_value}} Months</option>
                @endforeach
            @else
                <option @if(in_array(1, $reqArr)) selected="selected" @endif value="1">1 months</option>
                <option @if(in_array(3, $reqArr)) selected="selected" @endif value="3">3 months</option>
                <option @if(in_array(6, $reqArr)) selected="selected" @endif value="6">6 months</option>
                <option @if(in_array(9, $reqArr)) selected="selected" @endif value="9">9 months</option>
                <option @if(in_array(12, $reqArr)) selected="selected" @endif value="12">12 months</option>
            @endif
        </select>

        </div>
    </div>
</div>