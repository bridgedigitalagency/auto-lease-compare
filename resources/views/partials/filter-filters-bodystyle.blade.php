@php
    if(is_array(app('request')->input('bodystyle'))) {
        $reqArr = app('request')->input('bodystyle');
    }else{
        $reqArr = array();
    }
@endphp
@foreach($bodystyles as $bodystyle)
    <div class="col-md-3 text-center">
        <label><input type="checkbox" class="invisible" name="bodystyle[]"
                      @if(in_array($bodystyle->bodystyle, $reqArr)) checked @endif
                      value="{{$bodystyle->bodystyle}}" id="{{$bodystyle->bodystyle}}Checkbox">
            <div class="imgdiv"><img src="/images/bodystyles/{{str_replace(' ', '-', $bodystyle->bodystyle)}}.png" class="filtericons">{{$bodystyle->bodystyle}}</div></label>
    </div>
@endforeach