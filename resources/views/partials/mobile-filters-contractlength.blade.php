@php
    $showasterix = false;
@endphp
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a class="contractlengthFilterHeader mb-3 back"><strong><i class="fa fa-angle-double-left"></i> Back</strong></a>
            <strong class="mb-4">Select Contract Length</strong>
            @php
                if(is_array(app('request')->input('term'))) {
                   $reqArr = app('request')->input('term');
                }else{
                   $reqArr = array();
                }
            @endphp
            <select name="term[]" id="term" id="term" class="mobileTerm mt-2" multiple>
                @if(isset($terms))
                    @foreach($terms as $term)
                        @php
                            if($term->term == '12') {
                                $showasterix = true;
                            }
                        @endphp
                        <option @if(in_array($term->term, $reqArr)) selected="selected" @endif value="{{$term->term}}">{{$term->term}} Months @if($term->term == "12") * @endif</option>
                    @endforeach
                @else
                    <option @if(in_array('12', $reqArr)) selected="selected" @endif value="12">12 Months *</option>
                    <option @if(in_array('24', $reqArr)) selected="selected" @endif value="24">24 Months</option>
                    <option @if(in_array('36', $reqArr)) selected="selected" @endif value="36">36 Months</option>
                    <option @if(in_array('48', $reqArr)) selected="selected" @endif value="48">48 Months</option>
                @endif
            </select>
            @if($showasterix)
                <p class="text-center"><small>* There may be a limited number of deals available for this option.</small></p>
            @endif
        </div>
    </div>
</div>