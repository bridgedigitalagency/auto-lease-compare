@php
    if(is_array(app('request')->input('maker'))) {
        $reqArr = app('request')->input('maker');
    }else{
        $reqArr = array();
    }
    @endphp
@foreach($makes ?? '' as $make)
    <div class="col-md-2 col-sm-2 col-4 text-center">
        <input type="checkbox"
        @if(in_array($make->maker, $reqArr)) checked @endif
        value="{{$make->maker}}" name="maker[]" id="cb{{$loop->iteration}}">
            <label for="cb{{$loop->iteration}}"><img
            src="/images/manufacturers/@php echo(strtolower($make->maker)) @endphp.png"
            id="@php echo(strtolower($make->maker)) @endphpimg"><br/>{{$make->maker}}</label>
    </div>
@endforeach