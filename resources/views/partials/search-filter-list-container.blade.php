@if(strpos(Request::url(), 'vans'))
    @php $database = 'LCV'; @endphp
@endif
<div class="row">
    <div class="col-sm-3" id="filterListContainer">
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <div id="mobileFilters">
                @include('partials.mobile-filter-list')
            </div>
        </div>
        <div id="mySidenav2" class="sidenav2">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav2()">&times;</a>
            <div class="col-md-12">
                <label>Sort By</label>
                <form id="mobilesortByForm">
                    <select name="sort" class="form-control" id="mobilefilterSortBy">
                        <option value="monthly_payment">Cheapest Monthly Price</option>
<option value="average_cost">Cheapest Average Monthly Cost</option>
                        <option value="deposit_months">Cheapest Total Upfront</option>
                        <option value="total_cost">Cheapest Total Cost</option>
                        <option value="highest_price">Highest Monthly Price</option>
                    </select>
                </form>
            </div>
            <div class="col-sm-12">
                <form id="mobilestockForm">
                    <div class="custom-control custom-switch stockForm">
                        <input type="checkbox" class="custom-control-input" id="mobilein_stock" name="in_stock">
                        <label class="custom-control-label" for="in_stock">In Stock</label>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-6 filterbuttons d-block d-sm-block d-md-none"  onclick="openNav()"><i class="fa fa-filter" aria-hidden="true"></i> Filter</div>
            <div class="col-6 filterbuttons d-block d-sm-block d-md-none"  onclick="openNav2()"><i class="fa fa-sort" aria-hidden="true"></i> Sort By</div>
        </div>
        <div class="d-none d-md-block desktopFilters">
            @include('partials.filter-list')
        </div>
        <div class="sidebar_widget">
            <div class="text-center">
                <p>
                    <strong>Auto Lease Compare deals include:</strong>
                </p>
                <ul class="fa-ul" style="padding-inline-start: 0px;text-align: left;display: inline-block;">
                    <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;"></i></span>No Added Fees</li>
                    @if($database == 'LCV')
                        <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;font-size: 20px;"></i></span>Brand New Vans</li>
                    @else
                        <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;font-size: 20px;"></i></span>Brand New Cars</li>
                    @endif
                    <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;"></i></span>Manufacturers Warranty</li>
                    <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;"></i></span>Free Mainland UK Delivery</li>
                    <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;"></i></span>Breakdown Cover</li>
                    <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;"></i></span>Road Tax For Contract Length</li>
                    <li style="list-style: none;"><span class="fa-li"><i class="fa fa-check-circle" style="color:#fc5185;"></i></span>All advertising partners are authorised and regulated by the FCA</li>
                </ul>
            </div>
        </div>
        <div class="sell mt-4">
            <a href="/sell-your-car">
                <img src="/images/sellyourcar.jpg" alt="sell your car" class="img-responsive ">
            </a>
        </div>
        <div class="sell mt-4">
            <a href="https://www.motoreasy.com/gap-quote/step/1?utm_medium=affiliate&utm_campaign=organic&utm_source=autoleasecompare" target="_blank">
                <img src="/images/alc-motoreasy.png" alt="15% off GAP Insurance with code ALC15 at Motoreasy." class="img-responsive ">
            </a>
        </div>
    </div>
    <div class="col-sm-9">
        <div class="row filterListSort">
            <div class="col-12 col-lg-7 sortByFormContainer">
                <label style="width:19%;">
                    <a href="#" data-toggle="modal" data-target="#sortByInfoButton">
                        Sort By
                        <i class="fa fa-info-circle infobutton"></i>
                    </a>
                </label>
                <form id="sortByForm" style="width:80%; display:inline-block;">
                    <select name="sort" class="form-control" id="filterSortBy">
                        <option value="monthly_payment">Cheapest Monthly Price</option>
                        <option value="deposit_months">Cheapest Total Upfront</option>
                        <option value="total_cost">Cheapest Total Cost</option>
                        <option value="average_cost">Cheapest Average Monthly Cost</option>
                        <option value="highest_price">Highest Monthly Price</option>
                    </select>
                </form>
            </div>
            <div class="d-none d-lg-block col-3 col-lg-2 p-0 stockFormContainer">
                <form id="stockForm">
                    <input type="hidden" name="search" value="{{$searchedString}}">
                    <div class="custom-control custom-switch stockForm">
                        <input type="checkbox" class="custom-control-input" id="in_stock" name="in_stock">
                        <span class="stocklabel">In Stock</span><label class="custom-control-label switcher" for="in_stock" style="float: right;"></label>
                    </div>
                </form>
            </div>
            <div class="d-none d-lg-block col-lg-3">
                <button type="button" data-toggle="modal" data-target="#typesearch" class="button b2" style="float:right;"><i class="fa fa-search"></i> Type Search</button>
            </div>
        </div>


        <div class="modal fade bd-example-modal-lg" id="sortByInfoButton" tabindex="-1" role="dialog"
             aria-labelledby="sortByInfoButton" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Sort By Information</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="sortByInfoModalContent">
                                    <p>
                                        <strong style="color: #fc5185;">Cheapest Monthly Price</strong>
                                        Sort deals by the lowest monthly price (the price you will pay each month)
                                    </p>
                                    <p>
                                        <strong style="color: #fc5185;">Cheapest Total Upfront</strong>
                                        Sort deals by the lowest total upfront payment (Inc. Initial Payment + Documentation fee)
                                    </p>
                                    <p>
                                        <strong style="color: #fc5185;">Cheapest Total Cost</strong>
                                        Sort deals by the lowest total cost across the entire term of the contract (Inc. Total Monthly Payments + Initial Payment + Documentation fee)
                                    </p>
                                    <p>
                                        <strong style="color: #fc5185;">Cheapest Average Monthly Cost</strong>
                                        Sort deals by the lowest average monthly cost (Total Cost divided by the length of the contract)
                                    </p>
                                    <p>
                                        <strong style="color: #fc5185;">Highest Monthly Price</strong>
                                        Sort deals by the highest monthly price to enable you to see what cars you can get for your max budget
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="button b3" data-dismiss="modal">Done</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="typesearch" tabindex="-1" role="dialog" aria-labelledby="typesearch" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Type To Search</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/search" method="get">
                            @if ($finance_type == "H")
                                <input type="hidden" name="finance_type" value="H">
                            @elseif ($finance_type == "B")
                                <input type="hidden" name="finance_type" value="B">
                            @elseif ($finance_type == "A")
                                <input type="hidden" name="finance_type" value="A">
                            @elseif ($finance_type == 'PERFORMANCE')
                                <input type="hidden" name="finance_type" value="PERFORMANCE">
                            @else
                                <input type="hidden" name="finance_type" value="P">
                            @endif

                            <input type="hidden" name="database" value="{{$database}}">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="search" class="form-control" placeholder="Search Anything" value="{{$searchedString}}">
                                    <br/>
                                    <input type="submit" class="button b2">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="button b3" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div id="filterContentContainer">
                    @include('partials.search-filter-results')
                </div>
            </div>
        </div>
    </div>
</div>


@section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="/css/listing.css">
@endsection
