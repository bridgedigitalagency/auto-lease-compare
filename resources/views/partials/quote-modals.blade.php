<?php
$cap_id = $single_quote->cap_id;
$url = "http://api.autoleasecompare.com/cap/get-by-id/$cap_id";
$response = file_get_contents($url);
$jsond = json_decode($response);
$displayData = array();
$techData = $jsond->technical;
foreach ($techData->data as $k => $v) {
    foreach ($v as $d) {
        if ($d->description == "CC") {
            $displayData['cc'] = $d->value;
        }
        if ($d->description == "Engine Power - BHP") {
            $displayData['power'] = $d->value;
        }
        if ($d->description == "Engine Torque - RPM") {
            $displayData['torque'] = $d->value;
        }
        if ($d->description == "Top Speed") {
            $displayData['topspeed'] = $d->value;
        }
        if ($d->description == "0 to 62 mph (secs)") {
            $displayData['0to60'] = $d->value;
        }
        if ($d->description == "EC Urban (mpg)") {
            $displayData['urbanmpg'] = $d->value;
        }
        if ($d->description == "EC Extra Urban (mpg)") {
            $displayData['extraurbanmpg'] = $d->value;
        }
        if ($d->description == "EC Combined (mpg)") {
            $displayData['combinedmpg'] = $d->value;
        }
        if ($d->description == "Length") {
            $displayData['length'] = $d->value;
        }
        if ($d->description == "Width") {
            $displayData['width'] = $d->value;
        }
        if ($d->description == "Height") {
            $displayData['height'] = $d->value;
        }
        if ($d->description == "Wheelbase") {
            $displayData['wheelbase'] = $d->value;
        }
        if ($d->description == "Fuel Tank Capacity (Litres):") {
            $displayData['fuelTank'] = $d->value;
        }
        if ($d->description == "Insurance Group 1 - 50 Effective January 07") {
            $displayData['insurance'] = $d->value;
        }
        if ($d->description == "Standard manufacturers warranty - Years") {
            $displayData['warranty'] = $d->value;
        }
        if ($d->description == "Gears") {
            $displayData['gears'] = $d->value;
        }
        if ($d->description == "Fuel Tank Capacity (Litres)") {
            $displayData['fuelCapacity'] = $d->value;
        }
        if ($d->description == "Alloys?") {
            $displayData['alloys'] = $d->value;
        }
        if ($d->description == "Tyre Size Front") {
            $displayData['frontTyreSize'] = $d->value;
        }
        if ($d->description == "Tyre Size Rear") {
            $displayData['rearTyreSize'] = $d->value;
        }
    }
}
?>
<!-- Quote Modal -->
<div class="modal fade" id="quotemodel" tabindex="-1" role="dialog" aria-labelledby="quotemodel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Send Your Enquiry To {{$single_quote->deal_id}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #d6d6d6;">
                        <h5 class="text-center"><strong>Deal Breakdown</strong></h5>
                        <table style="width:100%;">
                            <tbody>
                            <tr>
                                <td class="t1"><small class="red">Initial Payment</small></td>
                                <td class="t2"><small>&pound;<span id="originalInitialRentalqf"><?php echo $single_quote->deposit_months;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT (<span id="originalMonths3"><?php echo $single_quote->deposit_value;?></span> Months Upfront)</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Contract Length</small></td>
                                <td class="t2"><small><span id="originalTerm3"><?php echo $single_quote->term;?></span> Months</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Annual Mileage</small></td>
                                <td class="t2"><small><span id="originalMileageqf"><?php echo $single_quote->annual_mileage;?></span> Miles Per Annum</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Monthly Rental</small></td>
                                <td class="t2"><small><span id="originalTerm4"><?php echo $single_quote->term - 1;?></span> monthly payments of £<span id="originalMonthlyRental3" data-bind="orignal_monthly_rental"><?php echo $single_quote->monthly_payment;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Average Monthly Cost</small></td>
                                <td class="t2"><small>£<span id="originalAv3"><?php echo $single_quote->average_cost;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</small></td>
                            </tr>

                            <tr>
                                <td class="t1"><small class="red">Document Fee</small></td>
                                <td class="t2"><small>£<span id="originalDocumentFee"><?php echo $single_quote->document_fee;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Stock Staus</small></td>
                                <td class="t2"><small>@if($single_quote->in_stock =='1') Stock Vehicle @else Factory Order @endif</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Vehicle Type</small></td>
                                <td class="t2"><small>Brand New</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Road Tax</small></td>
                                <td class="t2"><small>Included</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Manufactures Warranty</small></td>
                                <td class="t2"><small>Included</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Delivery</small></td>
                                <td class="t2"><small>{{$dealerInfo->delivery_policy}}</small></td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h5 class="text-center d-none d-md-block"><strong>Deal Provider</strong></h5>
                        <div class="text-center">
                            <strong>This {{$single_quote->maker}} {{$single_quote->model}} {{$single_quote->derivative}} is brought to you by {{$single_quote->deal_id}}</strong></div>
                        <div class="row justify-content-md-center mt-3 d-none d-md-flex">
                            @if(isset($dealerInfoJSON->logo))
                                <div class="col-sm-5 col-md-12 text-center">
                                    <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-3" alt="{{$dealerInfoJSON->name}}" style="max-width: 40%;">
                                </div>
                            @endif
                            @if(isset($dealerInfoJSON->trustpilot))
                                @if($trustpilotRatings->businessUnit->trustScore > 0)
                                    <div class="col-sm-5 col-md-12 text-center">
                                        <img src="/images/trustpilot/trustpilot-{{$trustpilotRatings->businessUnit->stars}}.png" alt="{{$trustpilotRatings->businessUnit->stars}} on Trustpilot" class="img-fluid" style="max-width: 80%;"><br/>
                                        <img src="/images/trustpilot/trustpilot-logo.svg" alt="Trustpilot" class="img-fluid"><br/>
                                        <span class="tp-text">Based on {{$trustpilotRatings->businessUnit->numberOfReviews->total}} Reviews</span>
                                    </div>
                                @endif
                            @endif
                        </div>

                    </div>
                    <div class="col-md-6 enquiretoday">
                        <h5 class="text-center mb-1" style="font-size: 30px;color: #fc5185;"><strong>Enquire Today</strong></h5>

                        @if( app('request')->input('enqErr') == '1')
                            <div class="alert alert-danger" role="alert">
                                <p>There was an error submitting your enquiry, please try again.</p>
                                <p>If the problem persists please <a href="mailto:info@autoleasecompare.com">contact us</a></p>
                            </div>
                        @endif

                        <form method="post" action="/quote" enctype="multipart/form-data" id="enquiryForm">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label for="name" class="col-sm-12 col-form-label">Name</label>
                                <div class="col-sm-12">
                                    <input name="name" type="text" class="form-control data-hj-allow" id="name" placeholder="Name" value="@if (Auth::check()) @if(isset(Auth::user()->first_name)){{Auth::user()->first_name }} {{Auth::user()->last_name }}@endif @endif" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-12 col-form-label">Email</label>
                                <div class="col-sm-12">
                                    <input name="email" type="email" class="form-control data-hj-allow" id="email" placeholder="Email" value="@if (Auth::check()){{{ isset(Auth::user()->email) ? Auth::user()->email : Auth::user()->email }}}@endif" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-sm-12 col-form-label">Phone</label>
                                <div class="col-sm-12">
                                    <input name="phone_number" type="tel" class="form-control data-hj-allow" id="phone_number"
                                           placeholder="Phone" value="@if (Auth::check()){{{ isset(Auth::user()->phone) ? Auth::user()->phone : Auth::user()->phone }}}@endif" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="contact_preference" class="col-sm-12 col-form-label">Contact Preference</label>
                                <div class="col-sm-12">
                                    <select id="contact_preference" class="form-control" name="contact_preference" aria-required="true" required aria-invalid="true" required>
                                        <option value="5">No Contact Preference</option>
                                        <option value="1">Call me back</option>
                                        <option value="2">Call me back (Morning)</option>
                                        <option value="3">Call me back (Afternoon)</option>
                                        <option value="4">Email me back</option>
                                    </select>
                                </div>
                            </div>
                            <div style="display: none;">
                                <textarea name="comment" id="comment"></textarea>
                                <input type="text" name="subject" id="subject">
                            </div>
<div class="form-group row">

                                <label for="message" class="col-sm-12 col-form-label">Maintenance Package</label>
                                <div class="col-sm-12">
                                    <div class="maintenance2">
                                        <input type="checkbox" id="maintenance" name="maintenance" rel="Quote me for maintenance package: Yes" style="margin-right: 9px;"><strong style="font-size: 13px; vertical-align: text-bottom;">Quote me for maintenance package?</strong><br/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="message" class="col-sm-12 col-form-label">Your Message</label>
                                <div class="col-sm-12">
                                    <textarea class="form-control data-hj-allow" id="message" name="message" value="Your Message" type="text" style="height: 110px; margin-bottom:15px;">Hello, I'm interested in this deal, please send me a personalised quote.</textarea>
                                    <input type="checkbox" name="hotdeals" value="1" style="margin-right: 9px;"><strong style="font-size: 13px; vertical-align: text-bottom;">Subscribe to our hot deals</strong><br/>
                                </div>
                            </div>
                            <input name="model_id" type="hidden" class="form-control" id="model_id"
                                   value="{{$single_quote->id}}">
                            <input name="database" type="hidden" class="form-control" id="database"
                                   value="{{$single_quote->database}}">
                            <input name="make" type="hidden" class="form-control" id="make"
                                   value="{{$single_quote->maker}}">
                            <input name="model" type="hidden" class="form-control" id="model"
                                   value="{{$single_quote->model}} {{$single_quote->derivative}}">
                            <input name="deal_id" type="hidden" class="form-control" id="deal_id"
                                   value="{{$single_quote->deal_id}}">
                            <input name="transmission" type="hidden" class="form-control" id="transmission"
                                   value="{{$single_quote->transmission}}">
                            <input name="fuel_type" type="hidden" class="form-control" id="fuel_type"
                                   value="{{$single_quote->fuel}}">
                            <input name="prices_id" type="hidden" class="form-control" id="prices_id"
                                   value="{{$single_quote->id}}">
                            <input name="price" type="hidden" class="form-control" id="price"
                                   value="{{$single_quote->monthly_payment}}">
                            <input name="profile" type="hidden" class="form-control" id="profile"
                                   value="£{{$single_quote->deposit_months}} Inc VAT ({{$single_quote->deposit_value}} Months Upfront) | {{$single_quote->term}} Month Contract | {{$single_quote->annual_mileage}}k Miles P/A">
                            <input name="finance_type" type="hidden" class="form-control" id="finance_type"
                                   value="@if($single_quote->finance_type =='P')P @else B @endif">
                            <input name="document_fee" type="hidden" class="form-control" id="document_fee"
                                   value="{{$single_quote->document_fee}}">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <button type="submit" class="mt-2 mb-3 button b3" onclick="ga('send', 'event', 'MessageEnquiry', 'click', 'enquiry');">Send Enquiry Now ></button>
                                </div>
                                <div class="col-md-12">
                                    <p><small><strong>Please note:</strong> Don’t worry - by clicking this button, you are not committing to this lease deal. We’ll simply pass on your enquiry to the advertiser so they can help get you behind the wheel of your new car!</small></p>
                                    <p><small>You are free to make additional enquiries to other advertisers within Auto Lease Compare.</small></p>
                                    <p><small>By submitting this enquiry you are confirming that you have read and understood the terms of our <a href="/site/privacy" target="_blank">privacy policy.</a></small></p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row text-center mt-3 d-sm-block d-md-none">
                        @if(isset($dealerInfoJSON->logo))
                            <div class="col-12">
                                <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-3" alt="{{$dealerInfoJSON->name}}" style="max-width: 80%; text-align: center;">
                            </div>
                        @endif
                        @if(isset($dealerInfoJSON->trustpilot))
                            @if($trustpilotRatings->businessUnit->trustScore > 0)
                                <div class="col-12">
                                    <img src="/images/trustpilot/trustpilot-{{$trustpilotRatings->businessUnit->stars}}.png" alt="{{$trustpilotRatings->businessUnit->stars}} on Trustpilot" class="img-fluid"><br/>
                                    <img src="/images/trustpilot/trustpilot-logo.svg" alt="Trustpilot" class="img-fluid"><br/>
                                    <span class="tp-text">Based on {{$trustpilotRatings->businessUnit->numberOfReviews->total}} Reviews</span>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b2" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Quote Modal -->
<!-- Deal info Modal -->
<div class="modal fade" id="dealInformation" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dealInformationLabel">
                    Deal Information
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Please note that Auto Lease Compare do not charge you or add anything on top of the price you see!</p>

                            <table style="width: 100%;" class="table table-striped">
                                <tbody>
                                <tr>
                                    <td class="t1 red">Initial Payment</td>
                                    <td class="t2">
                                            <strong><span id="customised_pricedi">£<?php echo $single_quote->deposit_months;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT (<span id="originalDepositValuedi"><?php echo $single_quote->deposit_value ?></span> Months Upfront)</strong>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="t1 red">Monthly Rental</td>
                                    <td class="t2">

                                            <strong><span id="originalTermdi3"><?php echo($single_quote->term - 1);?></span> monthly payments of £<span class="originalMonthlyRentaldi" data-bind="orignal_monthly_rental"><?php echo $single_quote->monthly_payment;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</strong>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="t1 red">Contract Length</td>
                                    <td class="t2">

                                            <strong><span id="originalTermdi"><?php echo $single_quote->term;?></span> Months</strong>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="t1 red">Profile:</td>
                                    <td class="t2">
                                        <strong><span id="originaldpdi"><?php echo $single_quote->deposit_value;?></span> + <span id="originalTermdi2"><?php echo $single_quote->term - 1;?></span> (1 payment of £<span id="customised_pricedi2"><?php echo $single_quote->deposit_months;?></span> + <span id="originalTermdi4"><?php echo $single_quote->term - 1;?></span> monthly payments of £<span class="originalMonthlyRentaldi"
                                        data-bind="orignal_monthly_rental"><?php echo $single_quote->monthly_payment;?></span> @if($single_quote->finance_type =='P')
                                                Inc @else Ex @endif VAT)</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="t1 red">Annual Mileage:</td>
                                    <td class="t2">
                                            <strong><span id="originalMileagedi"><?php echo $single_quote->annual_mileage;?></span> Miles Per Annum</strong>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="t1 red">Document Fee</td>
                                    <td class="t2">

                                            <strong>£<span id="originalDocumentFeedi"><?php echo $single_quote->document_fee;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</strong>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="t1 red">Total Cost</td>
                                    <td class="t2">
                                        <strong>£<span id="originalTotaldi"><?php echo $single_quote->total_cost;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT (inc. initial payment and document fee)</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="t1 red">Av.&nbsp;cost per month:</td>
                                    <td class="t2">
                                        <strong>£<span id="originalAvdi"><?php echo $single_quote->average_cost;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT (inc. initial payment and document fee)</strong>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="t1 red">Stock</td>
                                    <td class="t2"><strong>@if($single_quote->in_stock == 1) In Stock @else Factory Order @endif</strong></td>
                                </tr>
                                <tr>
                                    <td class="t1 red">Vehicle Type</td>
                                    <td class="t2"><strong>{{$single_quote->bodystyle}}</strong></td>
                                </tr>
                                <tr>
                                    <td class="t1 red">Road Tax</td>
                                    <td class="t2"><strong>Included</strong></td>
                                </tr>
                                <?php if(isset($displayData['warranty']) && $displayData['warranty'] != '') {?>
                                <tr>
                                    <td class="t1 red">Warranty</td>
                                    <td class="t2"><strong><?php echo $displayData['warranty'];?> Years</strong></td>
                                </tr>
                                <?php } ?>

                                <tr>
                                    <td class="t1 red">Delivery</td>
                                    <td class="t2"><strong>{{$dealerInfo->delivery_policy}}</strong></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-sm-12">

                        <h3>Definitions</h3>
                        <div id="accordion">

                            <div class="card">
                                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" aria-controls="collapseOne">
                                            What is a profile?
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>A <strong>‘profile’</strong> is a term used in the leasing industry to describe the structure of the payment e.g. 9 + 47, this indicates an initial upfront payment of 9 months followed by 47 monthly payments, totalling into a 4 year contract (48 months).&nbsp;</p>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" aria-controls="collapseTwo">
                                            What is the document fee?
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>The <strong>document fee</strong> is the administration fee which is charged by the leasing provider.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" >
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" aria-controls="collapseFour">
                                            What is the total cost?
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>The <strong>Total Cost</strong> across the entire term of the contract (Inc. Total Monthly Payments + Initial Payment + Documentation fee).</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" aria-controls="collapseThree">
                                            What is the average monthly cost?
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>The <strong>Average Monthly Cost</strong> is the Total Cost (defined above) divided by the length of the contract.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" aria-controls="collapseFive">
                                            What is the monthly rental?
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>The <strong>Monthly Rental</strong> is the set price you will pay each month.</p>
                                    </div>
                                </div>
                            </div>

                        </div>

                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Deal info Modal -->
<!-- Call Modal -->
<div class="modal fade" id="quotecall" tabindex="-1" role="dialog" aria-labelledby="quotecall" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Call <?php echo $single_quote->deal_id;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #d6d6d6;">
                        <h5 class="text-center"><strong>Deal Breakdown</strong></h5>
                        <table style="width:100%;">
                            <tbody>
                            <tr>
                                <td class="t1"><small class="red">Initial Payment</small></td>
                                <td class="t2"><small>&pound;<span id="customised_priceci"><?php echo $single_quote->deposit_months;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT (<span id="originalDepositValueci"><?php echo $single_quote->deposit_value;?></span> Months Upfront)</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Contract Length</small></td>
                                <td class="t2"><small><span id="originalTermci"><?php echo $single_quote->term;?></span> Months</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Annual Mileage</small></td>
                                <td class="t2"><small><span id="originalMileageci"><?php echo $single_quote->annual_mileage;?></span> Miles Per Annum</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Monthly Rental</small></td>
                                <td class="t2"><small><span id="originalTermci2"><?php echo $single_quote->term - 1;?></span> monthly payments of £<span id="originalMonthlyRentalci"><?php echo $single_quote->monthly_payment;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Average Monthly Cost</small></td>
                                <td class="t2"><small>£<span id="originalAvci"><?php echo $single_quote->average_cost;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Document Fee</small></td>
                                <td class="t2"><small>£<span id="originalDocumentFeeCi"><?php echo $single_quote->document_fee;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Stock Staus</small></td>
                                <td class="t2"><small>@if($single_quote->in_stock =='1') Stock Vehicle @else Factory Order @endif</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Vehicle Type</small></td>
                                <td class="t2"><small>Brand New</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Road Tax</small></td>
                                <td class="t2"><small>Included</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Manufactures Warranty</small></td>
                                <td class="t2"><small>Included</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Delivery</small></td>
                                <td class="t2"><small>{{$dealerInfo->delivery_policy}}</small></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 text-center mt-4">
                                @if(isset($dealerInfoJSON->phone))
                                    <h5><strong>Call Today</strong></h5>
                                    <a class="calllink button b3" style="margin-bottom: 20px;" href="tel:{{$dealerInfoJSON->phone}}">{{$dealerInfoJSON->phone}}</a>
                                @endif
                                @if(isset($dealerInfoJSON->opening_hours))
                                    <h5 class="mt-4"><strong>Opening Hours</strong></h5>
                                    {{$dealerInfoJSON->opening_hours}}<br/>
                                @endif
                                @if(isset($dealerInfoJSON->phone))
                                    Are they closed? <a style="color:#fc5185; cursor: pointer" data-dismiss="modal" id="modalclosecall" data-toggle="modal" data-target="#quotemodel" onclick="ga('send', {hitType: 'event',eventCategory: 'EnquireNow',eventAction: 'LoadForm',eventLabel: 'Forms'});">Send them an enquiry instead.</a>
                                @endif
                                <br/>
                            </div>
                        </div>
                        <br/>
                        <div class="text-center">
                            <strong>This {{$single_quote->maker}} {{$single_quote->model}} {{$single_quote->derivative}} is brought to you by {{$single_quote->deal_id}}</strong></div>
                        <div class="row justify-content-md-center mt-3">
                            @if(isset($dealerInfoJSON->logo))
                                <div class="col-sm-6 col-md-12 text-center">
                                    <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-3" alt="{{$dealerInfoJSON->name}}" style="max-width: 80%;">
                                </div>
                            @endif
                            @if(isset($dealerInfoJSON->trustpilot))
                                @if($trustpilotRatings->businessUnit->trustScore > 0)
                                        <div class="col-sm-6 col-md-12 text-center">
                                        <img src="/images/trustpilot/trustpilot-{{$trustpilotRatings->businessUnit->stars}}.png" alt="{{$trustpilotRatings->businessUnit->stars}} on Trustpilot" class="img-fluid" style="max-width: 80%;"><br/>
                                        <img src="/images/trustpilot/trustpilot-logo.svg" alt="Trustpilot" class="img-fluid"><br/>
                                        <span class="tp-text">Based on {{$trustpilotRatings->businessUnit->numberOfReviews->total}} Reviews</span>
                                    </div>
                                @endif
                            @endif
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b2" data-dismiss="modal" id="modalclosecall">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Call Modal -->

<!-- Dealer Modal -->
<div class="modal fade" id="dealermodal" tabindex="-1" role="dialog" aria-labelledby="dealermodal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3 class="mb-3">This Deal Is Brought To You By @if(isset($dealerInfoJSON->name)) {{$dealerInfoJSON->name}} @endif</h3>
                            <div class="row justify-content-md-center">
                                @if(isset($dealerInfoJSON->logo))
                                    <div class="col-sm-3 col-md-12  text-center">
                                        <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-3" alt="{{$dealerInfoJSON->name}}" style="max-width: 80%;">
                                    </div>
                                @endif
                                @if(isset($dealerInfoJSON->trustpilot))
                                    @if($trustpilotRatings->businessUnit->trustScore > 0)
                                        <div class="col-md-3">
                                            <img src="/images/trustpilot/trustpilot-{{$trustpilotRatings->businessUnit->stars}}.png" alt="{{$trustpilotRatings->businessUnit->stars}} on Trustpilot" class="img-fluid"><br/>
                                            <img src="/images/trustpilot/trustpilot-logo.svg" alt="Trustpilot" class="img-fluid"><br/>
                                            <span class="tp-text">Based on {{$trustpilotRatings->businessUnit->numberOfReviews->total}} Reviews</span>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6 mt-3 dealerinfo">
                            @if(isset($dealerInfoJSON->address))
                                <h5>Address</h5>
                                {{$dealerInfoJSON->address}}<br/>
                                <small>Regardless of their location, the leasing company will almost always arrange to deliver your brand new car straight to your front door and all paperwork is done via email, this alleviates the need for a leasing company to be near you.</small>
                                <br/>
                            @endif
                            @if(isset($dealerInfoJSON->opening_hours))
                                <h5>Opening Hours</h5>
                                {{$dealerInfoJSON->opening_hours}}<br/>
                                <small>Please note response times for your quote will typically be between the above opening hours.</small>
                            @endif
                        </div>
                        <div class="col-md-6 mt-3 dealerinfo">
                            @if(isset($dealerInfoJSON->about))
                                <h5>About Us</h5>
                                {{$dealerInfoJSON->about}}<br/>
                            @endif
                            @if(isset($dealerInfoJSON->delivery_policy))
                                <h5>Delivery Policy</h5>
                                {{$dealerInfoJSON->delivery_policy}}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b3" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Dealer Modal -->

<!-- Social Share Modal -->
<div class="modal fade" id="socialModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle" style="color: #000;">Share This Lease Deal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="share-buttons">
                <?php $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                ?>
                <!-- Social Button HTML -->
                    <!-- Twitter -->
                    <a href="https://twitter.com/share?url=<?php echo $actual_link; ?>&text=Check Out This Lease Deal&via=compare_lease" target="_blank" class="share-btn twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <!-- Facebook -->
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link; ?>" target="_blank" class="share-btn facebook">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <!-- LinkedIn -->
                    <a href="https://www.linkedin.com/shareArticle?url=<?php echo $actual_link; ?>&title=<TITLE>&summary=<SUMMARY>&source=<SOURCE_URL>" target="_blank" class="share-btn linkedin">
                        <i class="fa fa-linkedin"></i>
                    </a>
                    <!-- Email -->
                    <a href="mailto:?subject={{$single_quote->maker}}  {{$single_quote->range}}  {{$single_quote->derivative}} Lease Deal&body=Check out this {{$single_quote->maker}}  {{$single_quote->range}}  {{$single_quote->derivative}} lease deal on Auto Lease Compare <?php echo $actual_link; ?>" target="_blank" class="share-btn email">
                        <i class="fa fa-envelope"></i>
                    </a>
                    <!-- Whatsapp -->
                    <a href="whatsapp://send?text=Check Out This Lease Deal! <?php echo $actual_link; ?>" data-action="share/whatsapp/share" class="share-btn whatsapp">
                        <i class="fa fa-whatsapp"></i>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Social Share Modal -->

<!-- Colours Modal -->
<div class="modal fade" id="coloursmodel" tabindex="-1" role="dialog" aria-labelledby="coloursmodal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="model-standard">
                    Select Your Preferred Colour</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 style="color:#fc5185;">Select your preferred colours to add to your enquiry</h5>
                            <i>*Some of these colours will come at an additional cost, these can be discussed with the deal provider</i>
                            <div id="ChooseColours">
                                @foreach($jsonColour as $metalics)
                                    @foreach($metalics as $metalics1)
                                        @if (strpos(strtolower($metalics1->description), 'metallic -') !== false)
                                            @if($loop->first)
                                                <h5 style="color:#fc5185;" class="mt-3 mb-1">Metallic</h5>
                                            @endif
                                            <input type="checkbox" rel="{{ $metalics1->description }} @if($metalics1->price =='0') - (Free) @else - (Additional Cost) @endif" value="@if($single_quote->finance_type =='P') {{ number_format($metalics1->price*1.2, 2) }}@else {{ number_format($metalics1->price, 2) }}@endif"/>
                                            {{ $metalics1->description }} @if($metalics1->price =='0') Free @else - <i style="color:#fc5185;">Additional Cost</i> @endif</br>
                                        @endif
                                    @endforeach
                                @endforeach
                                @foreach($jsonColour as $solids)
                                    @foreach($solids as $solids1)
                                        @if (strpos(strtolower($solids1->description), 'solid -') !== false)
                                            @if($loop->first)
                                                <h5 style="color:#fc5185;" class="mt-3 mb-1">Solid</h5>
                                            @endif
                                            <input type="checkbox" rel="{{ str_replace("'", "", $solids1->description) }} @if($solids1->price =='0') - (Free) @else - (Additional Cost) @endif" value="@if($single_quote->finance_type =='P') {{ number_format($solids1->price*1.2, 2) }}@else {{ number_format($solids1->price, 2) }}@endif"/>
                                            {{ $solids1->description }} @if($solids1->price =='0') - <i style="color:#fc5185;">Free</i> @else - <i style="color:#fc5185;">Additional Cost</i> @endif</br>
                                        @endif
                                    @endforeach
                                @endforeach
                                @foreach($jsonColour as $specials)
                                    @foreach($specials as $specials1)
                                        @if (strpos(strtolower($specials1->description), 'paint -') !== false)
                                            @if($loop->first)
                                                <h5 style="color:#fc5185;" class="mt-3 mb-1">Special Paint</h5>
                                            @endif
                                            <input type="checkbox" rel="{{ $specials1->description }} @if($specials1->price =='0') - (Free) @else - (Additional Cost) @endif" value="@if($single_quote->finance_type =='P') {{ number_format($specials1->price*1.2, 2) }}@else {{ number_format($specials1->price, 2) }}@endif"/>
                                            {{ $specials1->description }} @if($specials1->price =='0') - <i style="color:#fc5185;">Free</i> @else - <i style="color:#fc5185;">Additional Cost</i>@endif</br>
                                        @endif
                                    @endforeach
                                @endforeach
                                @foreach($jsonColour as $pearls)
                                    @foreach($pearls as $pearls1)
                                        @if (strpos(strtolower($pearls1->description), 'pearl -') !== false)
                                            @if($loop->first)
                                                <h5 style="color:#fc5185;" class="mt-3 mb-1">Pearl</h5>
                                            @endif
                                            <input type="checkbox" rel="{{ $pearls1->description }} @if($pearls1->price =='0') - (Free) @else - (Additional Cost) @endif" value="@if($single_quote->finance_type =='P') {{ number_format($pearls1->price*1.2, 2) }}@else {{ number_format($pearls1->price, 2) }}@endif"/>
                                            {{ $pearls1->description }} @if($pearls1->price =='0') - <i style="color:#fc5185;">Free</i> @else - <i style="color:#fc5185;">Additional Cost</i>@endif</br>
                                        @endif
                                    @endforeach
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b3" data-dismiss="modal">Apply</button>
            </div>
        </div>
    </div>
</div>
<!-- End Colours Modal -->


<!-- Technical & Spec Modal -->
<?php
foreach($jsond as $j) {

// get loop around the data to get the details for the buttons
foreach($j as $k=>$v) {
if ($k == 'desc_refs' || $k == 'data') {
} else {
?>
<div class="modal fade" id="model<?= $k; ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="model-<?=$k;?>">
                    <?php
                    switch ($k) {
                        case "standard":
                            echo "Standard Equipment";
                            break;
                        case "options":
                            echo "Optional Extras";
                    }
                    ?>
                </h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body tech">
                <?php
                switch ($k) {
                    case "standard":
                        break;
                    case "options":
                        echo "<h5 style='color:#fc5185;'>Select the optional extras to add to your enquiry</h5>
                <i>*Some of these optional extras will come at an additional cost, these can be discussed with the deal provider</i>
                ";
                }
                ?>
                <?php
                foreach ($v as $vk => $vv) {
                ?>
                <h5 style="color:#fc5185;"><?php echo($vk); ?></h5>
                <ul style="
    list-style-type: none;
    padding-inline-start: 10px;
">
                    <?php
                    foreach ($vv as $item) {
                        if (isset($item->price)) {

                            if ($item->price == '0') {
                                $item->descriptionField = $item->description . " - (Free)";
                            } else {
                                $item->descriptionField = $item->description . " - (Additional Cost)";
                            }

                            echo "<li><input type='checkbox' rel='" . $item->descriptionField . "'/> " . $item->description;

                            if ($item->price == '0') {
                                echo ' - <i style="color:#fc5185;">Free</i>';
                            } else {
                                echo ' - <i style="color:#fc5185;">Additional Cost</i>';
                            }
                            echo "</li>";
                        } elseif (isset($item->value)) {
                            echo "<li>" . $item->description . " (" . $item->value . ")</li>";
                        } else {
                            echo "<li>" . $item->description . "</li>";
                        }
                    }
                    ?>
                </ul>
                <?php
                }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php
                    switch ($k) {
                        case "standard":
                            echo "Close";
                            break;
                        case "options":
                            echo "Apply";
                    }
                    ?></button>
            </div>
        </div>
    </div>
</div>

<?php
}
}
}
?>

<!--tech-->
<div class="modal fade bd-example-modal-lg" id="modeldata" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="model-data">Technical Data</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body tech row">
                <div class="col-md-4">
                    <h5 style="color:#fc5185;">General</h5>
                    <div class="row">
                        @if(isset($single_quote->fuel))
                            <div class="col-6">Fuel Type:</div>
                            <div class="col-6">{{$single_quote->fuel}}</div>
                        @endif
                        @if(isset($single_quote->transmission))
                            <div class="col-6">Transmission:</div>
                            <div class="col-6"><?php if ($single_quote->transmission == 'Manual') {
                                    echo 'Manual';
                                } else {
                                    echo 'Automatic';
                                }?></div>
                        @endif
                        @if(isset($single_quote->co2))
                            <div class="col-6">Co2:</div>
                            <div class="col-6">{{$single_quote->co2}} g/KM</div>
                        @endif
                        <?php if(isset($displayData['cc'])) {?>
                        <div class="col-6">CC:</div>
                        <div class="col-6"><?php echo(isset($displayData['cc']) ? $displayData['cc'] : '');?> CC</div>
                        <?php } ?>
                        @if(isset($single_quote->doors))
                            <div class="col-6">Doors:</div>
                            <div class="col-6">{{$single_quote->doors}}</div>
                        @endif
                        <?php if(isset($displayData['gears'])) {?>
                        <div class="col-6">Gears:</div>
                        <div class="col-6"> <?php echo $displayData['gears']; ?></div>
                        <?php } ?>
                        <?php if(isset($displayData['insurance'])) {?>
                        <div class="col-6">Insurance:</div>
                        <div class="col-6"> <?php echo $displayData['insurance']; ?></div>
                        <?php } ?>
                        <?php if(isset($displayData['warranty']) && $displayData['warranty'] != '') {?>
                        <div class="col-6">Warranty:</div>
                        <div class="col-6"><?php echo $displayData['warranty'];?> Years</div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <h5 style="color:#fc5185;">PERFORMANCE</h5>
                    <div class="row">
                        <?php if(isset($displayData['power'])) {?>
                        <div class="col-6">Power:</div>
                        <div class="col-6"><?php echo $displayData['power']; ?> BHP</div>
                        <?php } ?>
                        <?php if(isset($displayData['torque'])) {?>
                        <div class="col-6">Torque:</div>
                        <div class="col-6"><?php if (isset($displayData['torque']) && !is_null($displayData['torque'])) {
                                echo $displayData['torque'] . " RPM";
                            } else {
                                echo "N/A";
                            }?></div>
                        <?php } ?>
                        <?php if(isset($displayData['topspeed'])) {?>
                        <div class="col-6">Top Speed:</div>
                        <div class="col-6"><?php echo $displayData['topspeed']; ?> MPH</div>
                        <?php } ?>
                        <?php if (isset($displayData['0to60'])) { ?>
                                        <?php if ($displayData['0to60'] !== false) {
                            echo "<div class='col-6 col-6'>0 to 62 mph:</div><div class='col-6 col-6'> " . $displayData['0to60'] . " SECS</div>";
                        } ?>
                                        <?php } ?>
                    </div>
                    <h5 style="color:#fc5185;">FUEL CONSUMPTION</h5>
                    <div class="row">
                        <?php if(isset($displayData['urbanmpg'])) {?>
                        <div class="col-6">Urban:</div>
                        <div class="col-6"> <?=$displayData['urbanmpg'];?> MPG</div>
                        <?php } ?>
                        <?php if(isset($displayData['extraurbanmpg'])) {?>
                        <div class="col-6">Extra urban:</div>
                        <div class="col-6"><?=$displayData['extraurbanmpg'];?> MPG</div>
                        <?php } ?>
                        <?php if(isset($displayData['combinedmpg'])) {?>
                        <div class="col-6">Combined:</div>
                        <div class="col-6"><?=$displayData['combinedmpg'];?> MPG</div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <h5 style="color:#fc5185;">METRICS</h5>
                    <div class="row">
                        <?php if(isset($displayData['length'])) {?>
                        <div class="col-6">Length:</div>
                        <div class="col-6"> <?php echo $displayData['length']; ?> mm</div>
                        <?php } ?>
                        <?php if(isset($displayData['width'])) {?>
                        <div class="col-6">Width:</div>
                        <div class="col-6"><?php if (isset($displayData['width']) && !is_null($displayData['width'])) {
                                echo $displayData['width'] . ' mm';
                            } else {
                                echo 'N/A';
                            } ?></div>
                        <?php } ?>
                        <?php if(isset($displayData['height'])) {?>
                        <div class="col-6">Height:</div>
                        <div class="col-6"><?php echo $displayData['height']; ?> mm</div>
                        <?php } ?>
                        <?php if(isset($displayData['wheelbase'])) {?>
                        <div class="col-6">Wheelbase:</div>
                        <div class="col-6"><?php echo $displayData['wheelbase']; ?> mm</div>
                        <?php } ?>
                        <?php if(isset($displayData['fuelCapacity'])) {?>
                        <div class="col-6">Fuel Tank Capacity:</div>
                        <div class="col-6"><?php echo $displayData['fuelCapacity'];?> Litres</div>
                        <?php } ?>
                        <?php if(isset($displayData['alloys'])) {?>
                        <div class="col-6">Alloys:</div>
                        <div class="col-6"><?php if ($displayData['alloys']) {
                                echo "Yes";
                            } else {
                                echo "No";
                            };?></div>
                        <?php } ?>
                        <?php if(isset($displayData['frontTyreSize'])) {?>
                        <div class="col-6">Tyres (Front):</div>
                        <div class="col-6"><?php echo $displayData['frontTyreSize']; ?></div>
                        <?php } ?>
                        <?php if(isset($displayData['rearTyreSize'])) {?>
                        <div class="col-6">Tyres (Rear):</div>
                        <div class="col-6"><?php echo $displayData['rearTyreSize']; ?></div>
                        <?php } ?>
                    </div>
                </div>
                <!--endtech-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Tech & Spec Modal -->
