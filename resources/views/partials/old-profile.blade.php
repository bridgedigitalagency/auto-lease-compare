<div class="container">
    <div class="row">
        <div class="col-12 mb-5">
            <div class="card border-0">
                <div class="card-body p-0">

                    @if (Auth::user()->id == $user->id)
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-sm-4 col-md-3 profile-sidebar text-white rounded-left-sm-up">
                                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

                                        @role('dealer|admin')
                                        <a class="nav-link active" data-toggle="pill" href=".edit-profile-tab" role="tab" aria-controls="edit-profile-tab" aria-selected="true">
                                            {{ trans('Upload Logo') }}
                                        </a>
                                        <a class="nav-link" data-toggle="pill" href=".edit-settings-tab" role="tab" aria-controls="edit-settings-tab" aria-selected="false">
                                            {{ trans('User Information') }}
                                        </a>
                                        @else
                                            <a class="nav-link active" data-toggle="pill" href=".edit-settings-tab" role="tab" aria-controls="edit-settings-tab" aria-selected="true">
                                                {{ trans('User Information') }}
                                            </a>
                                            @endrole

                                            <a class="nav-link" data-toggle="pill" href=".edit-account-tab" role="tab" aria-controls="edit-settings-tab" aria-selected="false">
                                                {{ trans('Password Reset') }}
                                            </a>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-8 col-md-9">
                                    <div class="tab-content" id="v-pills-tabContent">

                                        @role('dealer|admin')
                                        <div class="tab-pane fade show active edit-profile-tab" role="tabpanel" aria-labelledby="edit-profile-tab">
                                            <div class="row mb-1">
                                                <div class="col-sm-12 pt-4 pb-4" style="text-align: center;">
                                                    @if ($message = Session::get('success'))
                                                        <div class="alert alert-success alert-block">
                                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                                            <strong>{{ $message }}</strong>
                                                        </div>
                                                        <img src="/images/uploads/logos/{{ $user->logo }}" width="200">
                                                    @endif

                                                    @if (count($errors) > 0)
                                                        <div class="alert alert-danger">
                                                            <strong>Whoops!</strong> There were some problems with your input.
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif
                                                    <h2>Logo Upload</h2>
                                                    @if(isset($user->logo))
                                                        <img src="/images/uploads/logos/{{ $user->logo }}" width="200" class="mb-4">
                                                    @endif

                                                    <form action="{{ route('image.upload.post') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="row justify-content-md-center">
                                                            <div class="col-md-4">
                                                                <input type="file" name="image" class="form-control">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button type="submit" class="btn btn-success">Upload</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @endrole

                                        @role('user')
                                        <div class="tab-pane fade  show active edit-settings-tab" role="tabpanel" aria-labelledby="edit-settings-tab">
                                            @else
                                                <div class="tab-pane fade edit-settings-tab" role="tabpanel" aria-labelledby="edit-settings-tab">
                                                    @endrole

                                                    {!! Form::model($user, array('action' => array('ProfilesController@updateUserAccount', $user->id), 'method' => 'PUT', 'id' => 'user_basics_form')) !!}

                                                    {!! csrf_field() !!}

                                                    <div class="pt-4 pr-3 pl-2 form-group has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                                                        {!! Form::label('name', trans('forms.create_user_label_username'), array('class' => 'col-md-3 control-label')); !!}
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                {!! Form::text('name', $user->name, array('id' => 'name', 'class' => 'form-control', 'readonly', 'placeholder' => trans('forms.create_user_ph_username'))) !!}
                                                                <div class="input-group-append">
                                                                    <label class="input-group-text" for="name">
                                                                        <i class="fa fa-fw {{ trans('forms.create_user_icon_username') }}" aria-hidden="true"></i>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            @if($errors->has('name'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('name') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="pr-3 pl-2 form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                                                        {!! Form::label('email', trans('forms.create_user_label_email'), array('class' => 'col-md-3 control-label')); !!}
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                @if(Auth::check() && Auth::user()->hasRole('dealer'))
                                                                    {!! Form::text('email', $user->email, array('id' => 'email', 'class' => 'form-control', 'readonly', 'placeholder' => trans('forms.create_user_ph_email'))) !!}
                                                                @else
                                                                    {!! Form::text('email', $user->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_email'))) !!}
                                                                @endif
                                                                <div class="input-group-append">
                                                                    <label for="email" class="input-group-text">
                                                                        <i class="fa fa-fw {{ trans('forms.create_user_icon_email') }}" aria-hidden="true"></i>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            @if ($errors->has('email'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('email') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="pr-3 pl-2 form-group has-feedback row {{ $errors->has('phone') ? ' has-error ' : '' }}">
                                                        {!! Form::label('phone', trans('Phone Number'), array('class' => 'col-md-3 control-label')); !!}
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                {!! Form::text('phone', $user->phone, array('id' => 'phone', 'class' => 'form-control', 'readonly', 'placeholder' => "Enter Phone Number")) !!}
                                                                <div class="input-group-append">
                                                                    <label for="phone" class="input-group-text">
                                                                        <i class="fa fa-fw fa-phone" aria-hidden="true"></i>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            @if ($errors->has('phone'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    @if(Auth::check() && Auth::user()->hasRole('user'))

                                                    @else
                                                        <div class="pr-3 pl-2 form-group has-feedback row {{ $errors->has('address') ? ' has-error ' : '' }}">
                                                            {!! Form::label('address', trans('Address'), array('class' => 'col-md-3 control-label')); !!}
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    {!! Form::text('address', $user->address, array('id' => 'address', 'class' => 'form-control', 'placeholder' => 'Address')) !!}
                                                                    <div class="input-group-append">
                                                                        <label for="address" class="input-group-text">
                                                                            <i class="fa fa-fw fa-location-arrow" aria-hidden="true"></i>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                @if ($errors->has('address'))
                                                                    <span class="help-block">
                                                                            <strong>{{ $errors->first('address') }}</strong>
                                                                        </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="pr-3 pl-2 form-group has-feedback row {{ $errors->has('opening_hours') ? ' has-error ' : '' }}">
                                                            {!! Form::label('opening_hours', trans('Opening Hours'), array('class' => 'col-md-3 control-label')); !!}
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    {!! Form::text('opening_hours', $user->opening_hours, array('id' => 'opening_hours', 'class' => 'form-control', 'placeholder' => trans('Enter Opening Hours'))) !!}
                                                                    <div class="input-group-append">
                                                                        <label for="opening_hours" class="input-group-text">
                                                                            <i class="fa fa-fw fa-clock-o" aria-hidden="true"></i>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                @if ($errors->has('opening_hours'))
                                                                    <span class="help-block">
                                                                                <strong>{{ $errors->first('opening_hours') }}</strong>
                                                                            </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="pr-3 pl-2 form-group has-feedback row {{ $errors->has('about') ? ' has-error ' : '' }}">
                                                            {!! Form::label('about', trans('About You'), array('class' => 'col-md-3 control-label')); !!}
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    {!! Form::textarea('about', $user->about, array('id' => 'about', 'class' => 'form-control', 'placeholder' => trans('About Your Company'))) !!}
                                                                    <div class="input-group-append">
                                                                        <label for="about" class="input-group-text">
                                                                            <i class="fa fa-fw fa-info" aria-hidden="true"></i>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                @if ($errors->has('about'))
                                                                    <span class="help-block">
                                                                                    <strong>{{ $errors->first('about') }}</strong>
                                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="pr-3 pl-2 form-group has-feedback row {{ $errors->has('delivery_policy') ? ' has-error ' : '' }}">
                                                            {!! Form::label('delivery_policy', trans('Delivery Policy'), array('class' => 'col-md-3 control-label')); !!}
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    {!! Form::text('delivery_policy', $user->delivery_policy, array('id' => 'delivery_policy', 'class' => 'form-control', 'placeholder' => trans('Enter Delivery Information'))) !!}
                                                                    <div class="input-group-append">
                                                                        <label for="delivery_policy" class="input-group-text">
                                                                            <i class="fa fa-fw fa-road" aria-hidden="true"></i>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                @if ($errors->has('delivery_policy'))
                                                                    <span class="help-block">
                                                                                    <strong>{{ $errors->first('delivery_policy') }}</strong>
                                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    @endif


                                                    <div class="pr-3 pl-2 form-group has-feedback row {{ $errors->has('first_name') ? ' has-error ' : '' }}">
                                                        {!! Form::label('first_name', trans('forms.create_user_label_firstname'), array('class' => 'col-md-3 control-label')); !!}
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                {!! Form::text('first_name', $user->first_name, array('id' => 'first_name', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_firstname'))) !!}
                                                                <div class="input-group-append">
                                                                    <label class="input-group-text" for="first_name">
                                                                        <i class="fa fa-fw {{ trans('forms.create_user_icon_firstname') }}" aria-hidden="true"></i>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            @if($errors->has('first_name'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="pr-3 pl-2 form-group has-feedback row {{ $errors->has('last_name') ? ' has-error ' : '' }}">
                                                        {!! Form::label('last_name', trans('forms.create_user_label_lastname'), array('class' => 'col-md-3 control-label')); !!}
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                {!! Form::text('last_name', $user->last_name, array('id' => 'last_name', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_lastname'))) !!}
                                                                <div class="input-group-append">
                                                                    <label class="input-group-text" for="last_name">
                                                                        <i class="fa fa-fw {{ trans('forms.create_user_icon_lastname') }}" aria-hidden="true"></i>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            @if($errors->has('last_name'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <p>Checking this will enable us to notify you when cars in your <a href="/my-account/garage">garage</a> prices drop!</p>
                                                    <div class="pr-3 pl-2 form-group has-feedback row ">
                                                        <label for="delivery_policy" class="col-md-3 control-label">Enable Price Drop Notifications</label>
                                                        <div class="col-md-9">
                                                            <div class="input-group">
                                                                <input id="garage_price_notification" name="garage_price_notification" type="checkbox" class="form-control" @if($user->garage_price_notification == 1) checked @endif>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-9 offset-md-3">
                                                            {!! Form::button(
                                                                '<i class="fa fa-fw fa-save" aria-hidden="true"></i> ' . trans('profile.submitProfileButton'),
                                                                 array(
                                                                    'class'             => 'btn btn-success disabled',
                                                                    'id'                => 'account_save_trigger',
                                                                    'disabled'          => true,
                                                                    'type'              => 'button',
                                                                    'data-submit'       => trans('profile.submitProfileButton'),
                                                                    'data-target'       => '#confirmForm',
                                                                    'data-modalClass'   => 'modal-success',
                                                                    'data-toggle'       => 'modal',
                                                                    'data-title'        => trans('modals.edit_user__modal_text_confirm_title'),
                                                                    'data-message'      => trans('modals.edit_user__modal_text_confirm_message')
                                                            )) !!}
                                                        </div>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>

                                                <div class="tab-pane fade edit-account-tab" role="tabpanel" aria-labelledby="edit-account-tab">
                                                    <ul class="account-admin-subnav nav nav-pills nav-justified margin-bottom-3 margin-top-1">
                                                        <li class="nav-item bg-info">
                                                            <a data-toggle="pill" href="#changepw" class="nav-link warning-pill-trigger text-white active" aria-selected="true">
                                                                {{ trans('profile.changePwPill') }}
                                                            </a>
                                                        </li>
                                                        <li class="nav-item bg-info">
                                                            <a data-toggle="pill" href="#deleteAccount" class="nav-link danger-pill-trigger text-white">
                                                                {{ trans('profile.deleteAccountPill') }}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content">

                                                        <div id="changepw" class="tab-pane fade show active">

                                                            <h3 class="margin-bottom-1 text-center text-warning">
                                                                {{ trans('profile.changePwTitle') }}
                                                            </h3>

                                                            {!! Form::model($user, array('action' => array('ProfilesController@updateUserPassword', $user->id), 'method' => 'PUT', 'autocomplete' => 'new-password')) !!}

                                                            <div class="pw-change-container margin-bottom-2">

                                                                <div class="form-group has-feedback row {{ $errors->has('password') ? ' has-error ' : '' }}">
                                                                    {!! Form::label('password', trans('forms.create_user_label_password'), array('class' => 'col-md-3 control-label')); !!}
                                                                    <div class="col-md-9">
                                                                        {!! Form::password('password', array('id' => 'password', 'class' => 'form-control ', 'placeholder' => trans('forms.create_user_ph_password'), 'autocomplete' => 'new-password')) !!}
                                                                        @if ($errors->has('password'))
                                                                            <span class="help-block">
                                                                                <strong>{{ $errors->first('password') }}</strong>
                                                                            </span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="form-group has-feedback row {{ $errors->has('password_confirmation') ? ' has-error ' : '' }}">
                                                                    {!! Form::label('password_confirmation', trans('forms.create_user_label_pw_confirmation'), array('class' => 'col-md-3 control-label')); !!}
                                                                    <div class="col-md-9">
                                                                        {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_pw_confirmation'))) !!}
                                                                        <span id="pw_status"></span>
                                                                        @if ($errors->has('password_confirmation'))
                                                                            <span class="help-block">
                                                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                                            </span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-md-9 offset-md-3">
                                                                    {!! Form::button(
                                                                        '<i class="fa fa-fw fa-save" aria-hidden="true"></i> ' . trans('profile.submitPWButton'),
                                                                         array(
                                                                            'class'             => 'btn btn-warning',
                                                                            'id'                => 'pw_save_trigger',
                                                                            'disabled'          => true,
                                                                            'type'              => 'button',
                                                                            'data-submit'       => trans('profile.submitButton'),
                                                                            'data-target'       => '#confirmForm',
                                                                            'data-modalClass'   => 'modal-warning',
                                                                            'data-toggle'       => 'modal',
                                                                            'data-title'        => trans('modals.edit_user__modal_text_confirm_title'),
                                                                            'data-message'      => trans('modals.edit_user__modal_text_confirm_message')
                                                                    )) !!}
                                                                </div>
                                                            </div>
                                                            {!! Form::close() !!}

                                                        </div>


                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else
                                <p>{{ trans('profile.notYourProfile') }}</p>
                            @endif


                        </div>
                </div>
            </div>
        </div>
    </div>

@include('modals.modal-form')