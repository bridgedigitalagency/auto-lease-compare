@php
    $apple = app('request')->input('android_apple');
    $bluetooth = app('request')->input('bluetooth');
    $dab = app('request')->input('dab');
    $parking_camera = app('request')->input('parking_camera');
    $parking_sensors = app('request')->input('parking_sensors');
    $satnav = app('request')->input('sat_nav');
    $wireless_charge = app('request')->input('wireless_charge');
    $alloys = app('request')->input('alloys');
    $climate_control = app('request')->input('climate_control');
    $cruise_control = app('request')->input('cruise_control');
    $heated_seats = app('request')->input('heated_seats');
    $leather_seats = app('request')->input('leather_seats');
    $panoramic_roof = app('request')->input('panoramic_roof');
    $sunroof = app('request')->input('sunroof');
@endphp
<div class="row">
    <div class="col col-sm-12">
    <p><strong>Convenience</strong></p>
    </div>
</div>
<div class="row">
    <div class="col col-sm-3 text-center">
        <input type="checkbox" style="display:none;" @php if($apple=='android_apple') {@endphp checked="checked" @php } @endphp name="android_apple" id="apple" value="android_apple">
        <label for="apple"><div class="imgdiv"><img src="/images/carplay.png" class="filtericons">Android Auto / Apple Play</div></label>
    </div>
    <div class="col col-sm-3 text-center">
        <input type="checkbox" style="display:none;" @php if($bluetooth=='bluetooth') {@endphp checked="checked" @php } @endphp name="bluetooth" id="bt" value="bluetooth">
        <label for="bt"><div class="imgdiv"><img src="/images/bluetooth.png" class="filtericons">Bluetooth</div></label>
    </div>
    <div class="col col-sm-3 text-center">
        <input type="checkbox" style="display:none;" @php if($dab=='dab') {@endphp checked="checked" @php } @endphp name="dab" id="dabradio" value="dab">
        <label for="dabradio"><div class="imgdiv"><img src="/images/dab.png" class="filtericons">DAB</div></label>
    </div>
    <div class="col col-sm-3 text-center">
        <input type="checkbox" style="display:none;" @php if($parking_camera=='parking_camera') {@endphp checked="checked" @php } @endphp name="parking_camera" id="parkingcamera" value="parking_camera">
        <label for="parkingcamera"><div class="imgdiv"><img src="/images/parkassist.png" class="filtericons">Parking Assist Camera</div></label>
    </div>
    <div class="col col-sm-3 text-center">
        <input type="checkbox" style="display:none;" @php if($parking_sensors=='parking_sensors') {@endphp checked="checked" @php } @endphp name="parking_sensors" id="parkingsensors" value="parking_sensors">
        <label for="parkingsensors"><div class="imgdiv"><img src="/images/parkingsensors.png" class="filtericons">Parking Sensors</div></label>
    </div>
    <div class="col col-sm-3 text-center">
<input type="checkbox" style="display:none;" @php if($satnav=='sat_nav') {@endphp checked="checked" @php } @endphp name="sat_nav" id="satnavigation" value="sat_nav">
        <label for="satnavigation"><div class="imgdiv"><img src="/images/nav.png" class="filtericons">Sat Nav</div></label>
    </div>
    <div class="col col-sm-3 text-center">
        <input type="checkbox" style="display:none;" @php if($wireless_charge=='wireless_charge') {@endphp checked="checked" @php } @endphp name="wireless_charge" id="wirelesscharge" value="wireless_charge">
        <label for="wirelesscharge"><div class="imgdiv"><img src="/images/wirelesscharge.png" class="filtericons">Wireless Mobile Charging</div></label>
    </div>
</div>

<div class="row">
    <div class="col col-sm-12">
    <p><strong>Style</strong></p>
    </div>
</div>
<div class="row">
    <div class="col col-sm-3 text-center">
        <input type="checkbox" style="display:none;" @php if($alloys=='alloys') {@endphp checked="checked" @php } @endphp name="alloys" id="alloy" value="alloys">
        <label for="alloy"><div class="imgdiv"><img src="/images/alloys.png" class="filtericons">Alloys Wheels</div></label>
    </div>
</div>
<div class="row">
    <div class="col col-sm-12">
    <p><strong>Comfort & Safety</strong></p>
    </div>
</div>
<div class="row">
    <div class="col col-sm-3 text-center">
        <input type="checkbox" style="display:none;" @php if($climate_control=='climate_control') {@endphp checked="checked" @php } @endphp name="climate_control" id="climatecontrol"  value="climate_control">
        <label for="climatecontrol"><div class="imgdiv"><img src="/images/climate.png" class="filtericons">Climate Control</div></label>
    </div>
    <div class="col col-sm-3 text-center">
        <input type="checkbox" style="display:none;" @php if($cruise_control=='cruise_control') {@endphp checked="checked" @php } @endphp name="cruise_control" id="cruisecontrol" value="cruise_control">
        <label for="cruisecontrol"><div class="imgdiv"><img src="/images/cruise.png" class="filtericons">Cruise Control</div></label>
    </div>
    <div class="col col-sm-3 text-center">
        <input type="checkbox" style="display:none;" @php if($heated_seats=='heated_seats') {@endphp checked="checked" @php } @endphp name="heated_seats" id="heatedseats" value="heated_seats">
        <label for="heatedseats"><div class="imgdiv"><img src="/images/heatedseats.png" class="filtericons">Heated Seats</div></label>
    </div>
    <div class="col col-sm-3 text-center">
        <input type="checkbox" style="display:none;" @php if($leather_seats=='leather_seats') {@endphp checked="checked" @php } @endphp name="leather_seats" id="leatherseats" value="leather_seats">
        <label for="leatherseats"><div class="imgdiv"><img src="/images/leather.png" class="filtericons">Leather Seats</div></label>
    </div>
    <div class="col col-sm-3 text-center">
        <input type="checkbox" style="display:none;" @php if($panoramic_roof=='panoramic_roof') {@endphp checked="checked" @php } @endphp name="panoramic_roof" id="panoramicroof" value="panoramic_roof">
        <label for="panoramicroof"><div class="imgdiv"><img src="/images/panroof.png" class="filtericons">Panoramic Roof</div></label>
    </div>
    <div class="col col-sm-3 text-center">
        <input type="checkbox" style="display:none;" @php if($sunroof=='sunroof') {@endphp checked="checked" @php } @endphp name="sunroof" id="sunroofs" value="sunroof">
        <label for="sunroofs"><div class="imgdiv"><img src="/images/sunroof.png" class="filtericons">Sunroof</div></label>
    </div>
</div>
