<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a class="annualmileageFilterHeader mb-3 back"><strong><i class="fa fa-angle-double-left"></i> Back</strong></a>
            <strong class="mb-4">Select Annual Mileage</strong>
            @php if(is_array(app('request')->input('annual_mileage'))) { $reqArr = app('request')->input('annual_mileage'); }else{ $reqArr = array(); } @endphp
            <select name="annual_mileage[]" id="annual_mileage" class="mobileannual_mileage mt-2" multiple>
                @if(isset($annual_mileages))
                    @foreach($annual_mileages as $mileage)
                        <option @if(in_array( $mileage->annual_mileage, $reqArr)) selected="selected" @endif value="{{$mileage->annual_mileage}}">{{$mileage->annual_mileage}}</option>
                    @endforeach
                @else
                    <option @if(in_array( '5000', $reqArr)) selected="selected" @endif value="5000">5000</option>
                    <option @if(in_array( '8000', $reqArr)) selected="selected" @endif value="8000">8000</option>
                    <option @if(in_array( '10000', $reqArr)) selected="selected" @endif value="10000">10000</option>
                    <option @if(in_array( '12000', $reqArr)) selected="selected" @endif value="12000">12000</option>
                    <option @if(in_array( '15000', $reqArr)) selected="selected" @endif value="15000">15000</option>
                    <option @if(in_array( '20000', $reqArr)) selected="selected" @endif value="20000">20000</option>
                    <option @if(in_array( '30000', $reqArr)) selected="selected" @endif value="30000">30000</option>
                @endif
            </select>
        </div>
    </div>
</div>