@php
    $makerArr = app('request')->input('maker');

        if(is_array(app('request')->input('range'))) {

            $reqArr = app('request')->input('range');
    }else{
    $reqArr = array();
    }
@endphp
@if(isset($ranges) && is_array($ranges))
    @foreach($ranges as $maker=>$ranges)
        @if(in_array($maker, $makerArr))
            <li>
                <label>Select {{$maker}} Model</label>

                <select name="range[]" id="range-{{str_replace(' ', '', $maker)}}" class="rangeSelect" multiple="multiple">
                    @foreach($ranges as $range)
                        <option value="{{$range}}" @if(in_array($range, $reqArr)) selected="selected" @endif>{{$range}}</option>
                    @endforeach
                </select>
            </li>
        @endif
    @endforeach
@endif