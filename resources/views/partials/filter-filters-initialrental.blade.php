@php
    if(is_array(app('request')->input('deposit_value'))) {
        $reqArr = app('request')->input('deposit_value');
    }else{
        $reqArr = array();
    }
@endphp

@if(isset($deposit_values))
    @foreach($deposit_values as $deposit)

        <div class="col-md-4 col-sm-6 text-center"><label><input type="checkbox" name="deposit_value[]"
                                                                 @if(in_array($deposit->deposit_value, $reqArr)) checked @endif
                                                                 value="{{$deposit->deposit_value}}" class="d-none" id="{{$deposit->deposit_value}}Checkbox">
            <div class="textlabel">{{$deposit->deposit_value}} Months</div>
            </label>
        </div>
    @endforeach
@else
<div class="col-md-4 col-sm-6 text-center"><label><input type="checkbox" name="deposit_value[]"
                  @if(in_array(1, $reqArr)) checked @endif
                  value="1" class="d-none" id="1Checkbox">
        <div class="textlabel">1 Month</div>
    </label>
</div>

<div class="col-md-4 col-sm-6 text-center"><label><input type="checkbox" name="deposit_value[]"
                  @if(in_array(3, $reqArr)) checked @endif
                  value="3" class="d-none" id="3Checkbox">
        <div class="textlabel">3 Months</div>
    </label>
</div>
<div class="col-md-4 col-sm-6 text-center"><label><input type="checkbox" name="deposit_value[]"
                                                         @if(in_array(6, $reqArr)) checked @endif
                                                         value="6" class="d-none" id="6Checkbox">
        <div class="textlabel">6 Months</div>
    </label>
</div>
<div class="col-md-4 col-sm-6 text-center"><label><input type="checkbox" name="deposit_value[]"
                                                         @if(in_array(9, $reqArr)) checked @endif
                                                         value="9" class="d-none" id="9Checkbox">
        <div class="textlabel">9 Months</div>
    </label>
</div>
<div class="col-md-4 col-sm-6 text-center"><label><input type="checkbox" name="deposit_value[]"
                  @if(in_array(12, $reqArr)) checked @endif
                  value="12" class="d-none" id="12Checkbox">
        <div class="textlabel">12 Months</div>
    </label>
</div>
@endif