<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 pt-4">
            <div class="form-group row">
                <h5 class="modal-title" id="exampleModalLongTitle">Login To Auto Lease Compare</h5>
            </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="">{{ __('E-Mail Address') }}</label>
                            <div class="col-md-12 pl-0">
                                <input id="email" type="email" class="form-control @if(isset($errors)){{ $errors->has('email') ? ' is-invalid' : '' }} @endif " name="email" value="{{ old('email') }}" required autofocus>

                                @if (isset($errors) && $errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong> @if(isset($errors)) {{ $errors->first('email') }} @endif </strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="">{{ __('Password') }}</label>

                            <div class="col-md-12 pl-0">
                                <input id="password" type="password" class="form-control @if(isset($errors)){{ $errors->has('password') ? ' is-invalid' : '' }}@endif " name="password" required>

                                @if (isset($errors) && $errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 pl-0">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="submit" class="button b3">
                                  <i class="fa fa-sign-in"></i>  {{ __('Login') }}
                                </button>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <a style="border: 0;padding: 0px;" class="btn-link relative-price" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        </div>
                        <div class="row mb-2">
                            New to Auto Lease Compare? <a type="button" data-toggle="modal" data-target="#register" class="r5 ml-2 relative-price" style="border: 0;padding: 0px;"> Register Today</a>
                        </div>
                        <div class="row mb-2">
                             <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                        {{--<p class="text-center mb-3">
                                                    Or Login with
                                                </p>

                                                @include('partials.socials-icons')--}}
                    </form>
        </div>
        <div class="col-md-4 loginbg d-none d-md-block">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>