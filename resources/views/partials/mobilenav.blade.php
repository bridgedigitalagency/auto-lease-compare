
@guest
    <div class="container loginActions">
        <div class="row">
            <div class="col-5 offset-1"><a data-target="#login" data-toggle="modal" class="text-center button b3 w-100">Login</a></div>
            <div class="col-5"><a data-target="#register" data-toggle="modal" class="text-center button b2 w-100">Register</a></div>
        </div>
    </div>
@else
    <div class="container loginActions">
        <div class="row">

            <div class="col-5 offset-1"><a class="text-center button b1 w-100" style="background: rgb(23, 79, 95);" href="/my-account">My Dashboard</a></div>
            <div class="col-5"><a class="text-center button b3 w-100" href="/logout">Logout</a></div>
        </div>
    </div>
@endguest

<nav id="mainNavigation">
    <ul>
        <li><a href="#" id="carTop">Car Leasing</a>
            <ul id="carLeasingSub">
                <li><a href="/">Car Leasing Homepage</a></li>
                <li><a href="/personal">Personal Car Leasing</a></li>
                <li><a href="/business">Business Car Leasing</a></li>
{{--                <li><a href="/performance">Performance Car Leasing</a></li>--}}
                <li><a href="/hybrid">Hybrid & Electric Car Leasing</a></li>
            </ul>
        </li>
        <li><a href="#" id="vanTop">Van Leasing</a>
            <ul id="vanLeasingSub">
                <li><a href="/vans">Van Leasing Homepage</a></li>
                <li><a href="/personal-vans">Personal Van Leasing</a></li>
                <li><a href="/business-vans">Business Van Leasing</a></li>
            </ul>
        </li>

        @guest
            <li><a data-toggle="modal" data-target="#login" id="mobileMenuGarageLink">My Garage</a></li>
        @else
            <li><a href="/my-account/garage">My Garage</a></li>
            <li><a href="/my-account/enquiries">My Enquiries</a></li>
        @endguest
        <li><a href="#" id="leasingTop">Leasing Help</a>
            <ul id="leasingSub">
                <li><a href="/#about" id="aboutLink">About Us</a></li>
                <li><a href="/leasing-information">Leasing FAQs</a></li>
                <li><a href="/blog">News & Leasing Guides</a></li>
                <hr>
                <li><span class="otherServices">Other Services:</span></li>

                <li><a href="https://clk.omgt1.com/?PID=35050&AID=2136424" target="_blank">Compare Car Insurance</a></li>
                <li><a href="https://clk.omgt1.com/?PID=35052&AID=2136424" target="_blank">Compare Van Insurance</a></li>
                <li><a href="https://secure.uk.rspcdn.com/xprr/red/PID/3060" target="_blank">Check your credit score</a></li>
                <li><a href="https://www.motoreasy.com/gap-quote/step/1?utm_medium=affiliate&utm_campaign=organic&utm_source=autoleasecompare" target="_blank">GAP Insurance</a></li>
                <li><a href="/vehicle-repair">Repair your vehicle</a></li>
                <li><a href="/sell-your-car">Sell your vehicle</a></li>
            </ul>
        </li>
    </ul>
    @role('dealer|admin')
    <div class="menuHeader">
        <a href="#" id="dealerMenu">Dealer Navigation</a>
    </div>
    @endrole
    @role('admin')
    <div class="menuHeader">
        <a href="#" id="adminMenu">Admin Navigation</a>
    </div>
    @endrole
</nav>
@role('dealer|admin')
<nav id="dealerNavigation" style="display: none">
    <ul>
        <li><a href="/profile/{{Auth::user()->name}}">My profile</a></li>
        <li><a href="/my-account/enquiries">My Enquiries</a></li>
        <li><a href="/my-account/enquiries/call-log">Call Log</a></li>
        <li><a href="/my-account/export-quotes">Export Quotes</a></li>
        <li><a href="/my-account/my-pricing">My pricing</a></li>
        <li><a href="/my-account/add-price">Add New Deal</a></li>
        <li><a href="/my-account/garage">My Garage</a></li>
        <li><a href="#" id="mainMenu">Back to main menu</a></li>
    </ul>
</nav>
@endrole
@role('admin')
    <nav id="adminNavigation" style="display: none">
        <ul>
            <li><a href="/my-account/dealers/">Dealers</a></li>
            <li><a href="{{ url('/users') }}">User Admin</a></li>
            <li><a href="{{ url('/users/create') }}">New User</a></li>
            <li><a href="{{ url('/logs') }}">{!! trans('titles.adminLogs') !!}</a></li>
            <li><a href="{{ url('/activity') }}">{!! trans('titles.adminActivity') !!}</a></li>
            <li><a href="{{ url('/phpinfo') }}">Server Info</a></li>
            <li><a href="{{ url('/active-users') }}">{!! trans('titles.activeUsers') !!}</a></li>
            <li><a href="{{ route('laravelblocker::blocker.index') }}">{!! trans('titles.laravelBlocker') !!}</a></li>
            <li><a href="/blog-admin">Blog Admin</a></li>
            <li><a href="#" id="mainMenu">Back to main menu</a></li>
        </ul>
    </nav>
@endrole