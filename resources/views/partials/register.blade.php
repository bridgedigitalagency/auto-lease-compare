<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 pt-4">
            <div class="form-group row">
                <h5 class="modal-title" id="exampleModalLongTitle">Register With Auto Lease Compare</h5><br/>

                <p><i class="fa fa-check"></i> Save and compare your favourite vehicles in our comparison garage<br/>
                <i class="fa fa-check"></i> Track your enquiries & price history<br/>
{{--                <i class="fa fa-check"></i> Receive Email notifications when your saved vehicles drop in price</p>--}}
            </div>
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="form-group row">
                    <div class="col-md-6  pl-0">
                        <label for="first_name">{{ __('First Name') }}</label>
                            <input id="first_name" type="text" class="form-control  @if(isset($errors)) {{ $errors->has('first_name') ? ' is-invalid' : '' }} @endif " name="first_name" value="{{ old('first_name') }}" required autofocus>

                        @if(isset($errors) && $errors->has('first_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif

                    </div>
                    <div class="col-md-6  pl-0">
                    <label for="last_name">{{ __('Last Name') }}</label>
                        <input id="last_name" type="text" class="form-control @if(isset($errors)) {{ $errors->has('last_name') ? ' is-invalid' : '' }} @endif " name="last_name" value="{{ old('last_name') }}" required autofocus>
                        @if(isset($errors) && $errors->has('last_name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>

                <div class="form-group row">
                    <label for="email">{{ __('E-Mail Address') }}</label>
                    <div class="col-md-12 pl-0">
                        <input id="email" type="email" class="form-control @if(isset($errors)) {{ $errors->has('email') ? ' is-invalid' : '' }} @endif " name="email" value="{{ old('email') }}" required>

                        @if(isset($errors) && $errors->has('email'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password">{{ __('Password') }}</label>
                    <div class="col-md-12 pl-0">
                        <input id="password" type="password" class="form-control @if(isset($errors)) {{ $errors->has('password') ? ' is-invalid' : '' }} @endif " name="password" required>
                        @if(isset($errors) && $errors->has('password'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                             </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm" >{{ __('Confirm Password') }}</label>
                    <div class="col-md-12  pl-0">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>




                {{--@if(config('settings.reCaptchStatus'))
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-4">
                            <div class="g-recaptcha" data-sitekey="{{ config('settings.reCaptchSite') }}"></div>
                        </div>
                    </div>
                @endif--}}

                <div class="form-group row mb-4">
                    <div class="col-md-8 pl-0 mt-1">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="terms" style="display: inline-block" required> I Agree with the <a href="/site/terms" style="padding:0px; border:0px;">Terms and Conditions</a>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="newsletter" id="newsletter" value="1" style="display: inline-block"> {{ __('Subscribe to our hot deals') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <button type="submit" class="button b3"  onclick="ga('send', {hitType: 'event',eventCategory: 'Register',eventAction: 'Submission',eventLabel: 'SignUp'});" id="registrationFormSubmit">
                            <i class="fa fa-sign-in"></i>  {{ __('Register') }}
                        </button>
                    </div>
                </div>
                <div class="row mb-2">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                {{--<div class="row">
                    <div class="col-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
                        <p class="text-center mb-4">
                            Or Use Social Logins to Register
                        </p>
                        @include('partials.socials')
                    </div>
                </div>--}}

            </form>
        </div>
        <div class="col-md-4 registerbg d-none d-md-block">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>



