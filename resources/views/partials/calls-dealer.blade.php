<tr data-id="{{$call->id}}">
    <th scope="row">
        @if($call->i_number)
            {{$call->i_number}}
        @else
            Unknown
        @endif
    </th>
    <td>{{$call->c_date}} - {{$call->c_time}}</td>
    <td>@php
            if($call->c_duration > 60) {
                $seconds = $call->c_duration;

                $start_seconds = round($seconds);
                if($start_seconds <60)
                {
                    $minutes ="";
                    $seconds = $start_seconds;
                }
                else
                {

                    $minutes = floor($start_seconds/60);
                    $seconds = $start_seconds - $minutes*60;

                    $minutes = "$minutes"." Minute(s)";
                    $seconds = $seconds." Second(s)";
                }
                echo "$minutes $seconds";
            }else{
                echo $call->c_duration . " Seconds";
            }
        @endphp</td>

    <td>
        @if($call->status == 1)
            <span class="answeredCall">Answered</span>
        @elseif($call->status == 2)
            <span class="missedCall">Missed Call</span>
        @endif
    </td>
</tr>
