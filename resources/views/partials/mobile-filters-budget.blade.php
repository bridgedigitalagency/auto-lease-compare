<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a class="budgetFilterHeader mb-3 back"><strong><i class="fa fa-angle-double-left"></i> Back</strong></a>
            <strong class="mb-4">Filter Budget</strong><br/>
            @php
    $pricemax = app('request')->input('pricemax');
    if(isset($pricemax)) {
        $maxReqArr = app('request')->input('pricemax');
    }else{
        $maxReqArr = array();
    }
    $pricemin = app('request')->input('pricemin');
    if(isset($pricemin)) {
        $minReqArr = app('request')->input('pricemin');
    }else{
        $minReqArr = array();
    }
@endphp
    <label for="pricemin">Min Per Month</label>
        <select id="pricemin" class="mobilePriceMin" name="pricemin" multiple>
        <option value="">£---</option>
        <option @if($minReqArr == 100) selected="selected" @endif value="100">£100 p/m</option>
        <option @if($minReqArr == 125) selected="selected" @endif value="125">£125 p/m</option>
        <option @if($minReqArr == 150) selected="selected" @endif value="150">£150 p/m</option>
        <option @if($minReqArr == 175) selected="selected" @endif value="175">£175 p/m</option>
        <option @if($minReqArr == 200) selected="selected" @endif value="200">£200 p/m</option>
        <option @if($minReqArr == 225) selected="selected" @endif value="225">£225 p/m</option>
        <option @if($minReqArr == 250) selected="selected" @endif value="250">£250 p/m</option>
        <option @if($minReqArr == 275) selected="selected" @endif value="275">£275 p/m</option>
        <option @if($minReqArr == 300) selected="selected" @endif value="300">£300 p/m</option>
        <option @if($minReqArr == 325) selected="selected" @endif value="325">£325 p/m</option>
        <option @if($minReqArr == 350) selected="selected" @endif value="350">£350 p/m</option>
        <option @if($minReqArr == 375) selected="selected" @endif value="375">£375 p/m</option>
        <option @if($minReqArr == 400) selected="selected" @endif value="400">£400 p/m</option>
        <option @if($minReqArr == 425) selected="selected" @endif value="425">£425 p/m</option>
        <option @if($minReqArr == 450) selected="selected" @endif value="450">£450 p/m</option>
        <option @if($minReqArr == 475) selected="selected" @endif value="475">£475 p/m</option>
        <option @if($minReqArr == 500) selected="selected" @endif value="500">£500 p/m</option>
        <option @if($minReqArr == 550) selected="selected" @endif value="550">£550 p/m</option>
        <option @if($minReqArr == 600) selected="selected" @endif value="600">£600 p/m</option>
        <option @if($minReqArr == 650) selected="selected" @endif value="650">£650 p/m</option>
        <option @if($minReqArr == 700) selected="selected" @endif value="700">£700 p/m</option>
        <option @if($minReqArr == 750) selected="selected" @endif value="750">£750 p/m</option>
        <option @if($minReqArr == 800) selected="selected" @endif value="800">£800 p/m</option>
        <option @if($minReqArr == 900) selected="selected" @endif value="900">£900 p/m</option>
        <option @if($minReqArr == 1000) selected="selected" @endif value="1000">£1000 p/m</option>
    </select>

    <label for="pricemax">Max Per Month</label>
    <select id="pricemax" class="mobilePriceMax" name="pricemax" multiple>
        <option value="">£---</option>
        <option @if($maxReqArr == 100) selected="selected" @endif value="100">£100 p/m</option>
        <option @if($maxReqArr == 125) selected="selected" @endif value="125">£125 p/m</option>
        <option @if($maxReqArr == 150) selected="selected" @endif value="150">£150 p/m</option>
        <option @if($maxReqArr == 175) selected="selected" @endif value="175">£175 p/m</option>
        <option @if($maxReqArr == 200) selected="selected" @endif value="200">£200 p/m</option>
        <option @if($maxReqArr == 225) selected="selected" @endif value="225">£225 p/m</option>
        <option @if($maxReqArr == 250) selected="selected" @endif value="250">£250 p/m</option>
        <option @if($maxReqArr == 275) selected="selected" @endif value="275">£275 p/m</option>
        <option @if($maxReqArr == 300) selected="selected" @endif value="300">£300 p/m</option>
        <option @if($maxReqArr == 325) selected="selected" @endif value="325">£325 p/m</option>
        <option @if($maxReqArr == 350) selected="selected" @endif value="350">£350 p/m</option>
        <option @if($maxReqArr == 375) selected="selected" @endif value="375">£375 p/m</option>
        <option @if($maxReqArr == 400) selected="selected" @endif value="400">£400 p/m</option>
        <option @if($maxReqArr == 425) selected="selected" @endif value="425">£425 p/m</option>
        <option @if($maxReqArr == 450) selected="selected" @endif value="450">£450 p/m</option>
        <option @if($maxReqArr == 475) selected="selected" @endif value="475">£475 p/m</option>
        <option @if($maxReqArr == 500) selected="selected" @endif value="500">£500 p/m</option>
        <option @if($maxReqArr == 550) selected="selected" @endif value="550">£550 p/m</option>
        <option @if($maxReqArr == 600) selected="selected" @endif value="600">£600 p/m</option>
        <option @if($maxReqArr == 650) selected="selected" @endif value="650">£650 p/m</option>
        <option @if($maxReqArr == 700) selected="selected" @endif value="700">£700 p/m</option>
        <option @if($maxReqArr == 750) selected="selected" @endif value="750">£750 p/m</option>
        <option @if($maxReqArr == 800) selected="selected" @endif value="800">£800 p/m</option>
        <option @if($maxReqArr == 900) selected="selected" @endif value="900">£900 p/m</option>
        <option @if($maxReqArr == 1000) selected="selected" @endif value="1000">£1000 p/m</option>
    </select>

        </div>
    </div>
</div>