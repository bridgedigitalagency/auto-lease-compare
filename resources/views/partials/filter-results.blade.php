<div class="row">
    <div class="col-md-12">

{{--        <div class="showingNumber">Showing {{number_format(($summaryCap->currentpage()-1)*$summaryCap->perpage()+1)}} to {{number_format($summaryCap->currentpage()*$summaryCap->perpage())}}--}}
{{--            of  {{number_format($summaryCap->total())}} deals.--}}
{{--        </div>--}}

	<div class="showingNumber"></div>

    </div>
</div>
@forelse($summaryCap as $sum)
    <div style="background: #fff;margin-bottom: 15px; padding-bottom:3px;" class="result-row1">
        <div class="row result-row">
            <div class="col-md-4">

                @if($sum->database == 'LCV')
                    <a href="/quote/enquiry/{{$sum->id}}/lcv" class="rowcars">
                @else
                    <a href="/quote/enquiry/{{$sum->id}}/cars" class="rowcars">
                @endif

                    @if(!is_null($sum->insurance) && $sum->insurance > 0)
                        <span class="mobileInsuranceTag d-inline d-sm-none"><p>Free Insurance</p></span>
                    @endif
                    @if($sum->deal_id == 'Vanarama')
                        <div class="vanaramaBlackFriday" style="position: absolute; text-align: left;">
                            <img src="/images/vanaramaListing.png" style="max-width: 135px;">
                        </div>
                    @else
                        @if($sum->in_stock =='1')<div class="stocktag instock"><p>In Stock</p></div> @else <div class="stocktag"><p>Factory Order</p></div> @endif
                    @endif

	                <img src="{{$sum->imageid}}" alt="{{$sum->maker}} {{$sum->range}} {{$sum->derivative}}" class="img-fluid listingcar">
                    <small style="margin-top: -22px; display: block; text-align: center">Images for Illustration only</small>
                </a>
            </div>
            <div class="col-md-5 pt-2 pl-0 dealInfoBlock">
                @if($sum->database == 'LCV')
                    <a href="/quote/enquiry/{{$sum->id}}/lcv" class="rowcars">
                @else
                    <a href="/quote/enquiry/{{$sum->id}}/cars" class="rowcars">
                @endif
                    @if(isset($sum->maker))
                        <h3>{{$sum->maker}} {{$sum->range}}<br><span class="der"> {{$sum->derivative}}</span></h3>
                    @endif
                    @if(isset($sum->fuel))
                        <h6 class="features">{{$sum->fuel}} | {{$sum->transmission}} | {{$sum->bodystyle}} </h6>
                    @endif

                    @if(isset($sum->deposit_value))
                        <small>{{$sum->deposit_value}} Months Upfront  <span class="d-inline d-sm-none d-md-inline">|</span> {{$sum->term}} Months Contract <span class="d-inline d-sm-none d-md-inline">|</span> {{number_format($sum->annual_mileage)}} Miles P/A</small>
                    @endif
                        <br class="d-none d-sm-block"><small class="d-none d-sm-block">Deal Provider: <strong style="color: #fc5185;">{{$sum->deal_id}}</strong>
                            @if(!is_null($sum->insurance) && $sum->insurance > 0)<span class="insurance">Includes 1 Year’s Free Insurance</span>@endif
                            @if($sum->deal_id == 'Car Lease UK')
                                @if($sum->cap_id == '96677' || $sum->cap_id == '96678')
                                    <span class="insurance" style="font-size: 0.8rem;">Inc. £500 charge point contribution</span>
                                @endif
                            @endif
                        </small>

                </a>
            </div>

            <div class="col-md-3 pt-2">
{{--                <span class="mobileInsuranceTag d-none d-sm-inline"><p>FREE insurance</p></span>--}}
                @if($sum->database == 'LCV')
                    <a href="/quote/enquiry/{{$sum->id}}/lcv" class="rowcars" style="display: block">
                @else
                    <a href="/quote/enquiry/{{$sum->id}}/cars" class="rowcars" style="display: block">
                @endif
                    @if(isset($sum->monthly_payment))
                        <h6><span class="resultprice" style="color: rgb(252, 81, 133);">£{{number_format($sum->monthly_payment, 2)}}</span> P/M</h6>
                    @endif

                    <div class="row">
                    @if(isset($sum->deposit_months))
                        <div class="col-6 col-sm-12">
                            <h6>£{{number_format($sum->deposit_months + $sum->document_fee, 2)}} <br class="d-block d-sm-none"><small>Total Upfront</small></h6>
                        </div>
                    @endif

                    @if( app('request')->input('sort')  === 'total_cost')
                        <div class="col-6 col-sm-12">
                            <h6 style="margin-bottom: 0px;">£{{number_format($sum->total_cost, 2)}} <br class="d-block d-sm-none"><small>Total Cost</small></h6>
                        </div>

                    @else(isset($sum->average_cost))
                        <div class="col-6 col-sm-12">
                            <h6 style="margin-bottom: 0px;">£{{number_format($sum->average_cost, 2)}} <br class="d-block d-sm-none"><small>Av. Monthly Cost</small></h6>
                        </div>
                    @endif



                    </div>
                        <hr class="d-block d-sm-none mt-1 mb-1">

                        <small class="d-inline d-sm-none">Deal Provider: <strong style="color: #fc5185;">{{$sum->deal_id}}</strong><br></small>
                        <small> All Prices @if($sum->finance_type =='P') Inc @else Ex @endif VAT
                            @if(!is_null($sum->insurance) && $sum->insurance > 0)<span class="mobileInsurance d-inline d-sm-none">*Includes 1 year FREE insurance</span>@endif</small>
                </a>
                <a href="#" data-id="{{$sum->id}}" data-database="{{$sum->database}}" class="addToGarageListing d-none d-sm-block">
                    <i class="fa fa-car"></i> Save to garage
                </a>
                @role('admin')
                @if($sum->featured =='1')
                    <form method="POST" class="update-featured-form" data-value="{{$sum->id}}" action="#">
                        {{method_field('PUT')}}
                        {{ csrf_field() }}
                        <input type="hidden" name="cap-id" value="{{$sum->id}}">
                        <input class="form-control" style="display:none;" name="update-featured" id="update-featured-{{$sum->id}}"  data-value="{{$sum->id}}" value="0">
                        <input type="submit" value="- Remove Featured" name="submit" class="submit button b3 mt-3" id="submit-{{$sum->id}}" />
                    </form>
                    <div class="success-{{$sum->id}}"></div>
                @else
                    <form method="POST" class="update-featured-form" data-value="{{$sum->id}}" action="#">
                        {{method_field('PUT')}}
                        {{ csrf_field() }}
                        <input type="hidden" name="cap-id" value="{{$sum->id}}">
                        <input class="form-control" style="display:none;" name="update-featured" id="update-featured-{{$sum->id}}"  data-value="{{$sum->id}}" value="1">
                        <input type="submit" value="+ Make Featured" name="submit" class="submit button b2 mt-3" id="submit-{{$sum->id}}" />
                    </form>
                    <div class="success-{{$sum->id}}"></div>
                @endif
                @endrole
            </div>
        </div>
    </div>


@empty

    <h1>Sorry, no deals available at this point in time.</h1>
    <p>Please alter your search criteria</p>
@endforelse

@include('partials/garage-modals')

{{$summaryCap->links('partials.pagination')}}
