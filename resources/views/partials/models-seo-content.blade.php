<?php

$headerWordConcat = " Car Leasing";

?>
<div class="jumbotron" style="padding: 2rem 2rem 1rem">
@if($make == 'BMW')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
            <p>BMW is well known for producing stunning, luxurious, high-performance vehicles. This doesn’t come cheap and since buying a car outright may not be the best option for you; this is where we come in!</p>
            <p>Car leasing, also known as contract hire, makes it easy and affordable to drive a brand new BMW. But with so many car leasing companies around, it's hard to know if you're getting the best deal. </p>

            <div class="hiddenContent">
                <p>Here at Auto Lease Compare, we help you search and compare BMW lease deals. Our partners are well-established car leasing companies, authorised and regulated by the Financial Conduct Authority (FCA).</p>
                <p>It’s free to compare deals on this website! We don’t charge you at all. The price you see on our website is direct from the lease provider.</p>

                <p>We simply put you in touch with a leasing company that has the best deal for you. So you can be sure that you're getting the best deal available. If you’re new to leasing, <a href="https://www.autoleasecompare.com/leasing-information">learn more about how car leasing works here.</a></p>
                <p>There are 26 different models for you to choose from. So whether you are looking for a sleek saloon for your business, or a roomy SUV to fit the whole family; there’s a BMW for you!</p>
                <p>Browse the latest available BMW car lease deals below.</p>
            </div>
            <a href="#" class="jumbotronReadMore">Read More</a>
@elseif($make == 'Mercedes-Benz')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
            <p>Mercedes-Benz produces some of the best in class luxury cars in the world. Buying a Mercedes may not be an option but you save money through lasing. Auto Lease Compare allows you to search and compare Mercedes-Benz lease deals with ease.</p>
            <p>With so many car leasing companies out there, it’s hard to know if you’re getting the best car lease deal. We compile Mercedes car lease deals from the UK’s top leasing companies all in one place. So it's easy to compare and find the best deal for you.</p>
            <div class="hiddenContent">
                <p>All the car leasing companies you see on our website are authorised and regulated by the Financial Conduct Authority (FCA).</p>
                <p>Car leasing, also known as contract hire, puts you in control!</p>
                <p>Choose between personal and business deals for 20 Mercedes-Benz models. Select your initial rental amount, agreement term, and annual mileage. Then, enjoy your Mercedes for 2, 3 or 4 years and hand it back when the agreement term is over.</p>
                <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
                <p>Our service is completely free and impartial. We don’t charge you at all. We put you in touch with the leasing company that has your chosen deal at no extra cost to you. The deal you see on our website is direct from the car lease provider.</p>
                <p>Browse the latest available Mercedes car lease deals below.</p>
            </div>
            <a href="#" class="jumbotronReadMore">Read More</a>
@elseif($make == 'Audi')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Leasing an Audi has never been easier! With Auto Lease Compare, luxury, prestige, and comfort are only a click away.</p>
        <p>Audi boasts some of the best performance and luxury car models in the world. So it’s not cheap to buy an Audi car outright. Buying may not be the best option for you anyway because of the cost you incurred as the car depreciates in value. This is where car leasing comes in!</p>
        <div class="hiddenContent">
            <p>Car leasing, also known as contract hire, makes it affordable to drive a brand new Audi. There is no large upfront cost and you don’t have to worry about owning a depreciating asset. You get to drive your new Audi for a set term of 2, 3, or 4 years and then return it and take a new lease or walk away hassle-free.</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Here at Auto Lease Compare, we make it easy to compare lease deals. To save you time and money, we compile car lease deals from the best leasing companies in the UK. So you can search and compare them all in one place. All the companies we list on our website are authorised and regulated by the Financial Conduct Authority (FCA).</p>
            <p>We also don’t charge you at all. The price you see on our website is direct from the car lease provider. We only put you in touch with the car leasing company that has the Audi lease deal of your choice at no extra cost to you.</p>
            <p>Our range of deals covers 26 Audi models. That includes the sporty Audi TT and R8 models, the roomy SUVs in the Q-range, and the Sportbacks in the A-range.</p>
            <p>Browse the latest available Audi car lease deals below</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
@elseif($make == 'Ford')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Quality, reliable, trusted! This is the reputation that you buy into when you drive a Ford vehicle. With car leasing, it’s never been easier and more affordable to enjoy the thrill of a Ford on the road.</p>
        <p>Car leasing, also known as contract hire,  puts you in control!</p>
        <div class="hiddenContent">
            <p>You choose your initial rental amount, agreement term, and annual mileage. Then simply pay your monthly rental until you hand the car back at the end of the agreement term. It’s that easy! </p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>There are hundreds of car leasing companies operating in the UK. So it's hard to know if you're getting the best deal. You also have to be aware of illegitimate companies out there. It's a lot! Which is why we want to simplify the process of finding a car lease deal.</p>
            <p>We do this by compiling millions of lease deals from trusted car leasing companies.  We handpick each car lease partner and they are all authorised and regulated by the Financial Conduct Authority (FCA).</p>
            <p>Search and compare Ford car lease deals for free on our website! We don’t charge you at all. The price you see on our website is direct from the lease provider. We connect you with a leasing company that has your chosen Ford car lease deal on offer.</p>
            <p>So for whatever class of car, you’re looking for, we can help you find a great deal. From the popular Fiesta and Focus models which are perfect for inner-city living to the classic Mustang that's revered by car lovers all around the world.</p>
            <p>Browse the latest available Ford car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
@elseif($make == 'Volkswagen')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Volkswagen is well known for producing practical, high-quality cars. But buying a VW outright is pricey and may not be the best option for you anyway. This is where car leasing comes in!</p>
        <p>Personal and Business car leasing, also known as contract hire, makes it affordable to drive a VW. That's because you don't have to absorb the cost of depreciation as the car loses value over time. </p>
        <div class="hiddenContent">
            <p>Car leasing puts you in control!</p>
            <p>You choose your initial rental amount, agreement term, and annual mileage. Then enjoy your VW car as you pay your monthly rental amount for 2, 3, or 4 years until the end of the agreement term. At which point you return the car to the lease provider. It’s that easy! </p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Here at Auto Lease Compare, we help you search and compare millions of car lease deals. We partner with verified, well-established car leasing companies authorised and regulated by the Financial Conduct Authority.</p>
            <p>It’s also free to search for VW lease deals on our website. We don’t charge you at all. The price you see on your deal is direct from the lease provider. We simply put you in touch with them at no extra cost to you.</p>
            <p>So whatever class of car you’re looking for, we can help you find a great deal.</p>
            <p>Browse the latest available VW car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
@elseif($make == 'Volvo')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Finding the best Volvo car lease deal has never been easier! With Auto Lease Compare, your new Volvo is only a few clicks away.</p>
        <p>Volvo is well-known for producing cars with forward thinking technology. All Volvo models boast five star EuroNCAP safety ratings. So it’s not surprising that Volvo is usually the top choice for family cars.</p>
        <div class="hiddenContent">
            <p>However, buying a Volvo outright can be expensive and may not be the best option for you anyway because of the cost you incurred as the car depreciates in value. This is where car leasing comes in!</p>
            <p>Car leasing, also known as contract hire, makes it affordable to drive a brand new Volvo. There is no large upfront cost and you don’t have to worry about owning a depreciating asset. You get to drive your new Volvo for a set term of 2, 3, or 4 years and then return it and take a new lease or walk away hassle-free.</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Here at Auto Lease Compare, we make it easy to compare Volvo lease deals. To save you time and money, we compile car lease deals from the best leasing companies in the UK. So you can search and compare them all in one place. All the companies we list on our website are authorised and regulated by the Financial Conduct Authority (FCA).</p>
            <p>We also don’t charge you at all. The price you see on our website is direct from the car lease provider. We only put you in touch with the car leasing company that has the Volvo lease deal of your choice at no extra cost to you.</p>
            <p>Browse the latest available Volvo car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
@elseif($make == 'Kia')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Finding a great Kia car lease deal has never been easier! With Auto Lease Compare, your new Kia is only a few clicks away.</p>
        <p>Kia offers economical, practical, high-quality cars. It’s even cheaper to drive a Kia because of the low monthly rental payments when you take out a car lease, also known as contract hire.</p>
        <div class="hiddenContent">
            <p>With car leasing you don't have to absorb the cost of depreciation as the car loses value over time. </p>
            <p>You choose your initial rental amount, agreement term, and annual mileage. Then enjoy your Kia car as you pay your low monthly rental amount for 2, 3, or 4 years until the end of the agreement term. At which point you return the car to the lease provider. It’s that easy! </p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Here at Auto Lease Compare, we help you search and compare millions of car lease deals. We partner with legitimate, trusted car leasing companies authorised and regulated by the Financial Conduct Authority.</p>
            <p>It’s also free to search for Kia lease deals on our website. We don’t charge you at all! The price you see on your deal is direct from the lease provider. We simply put you in touch with them at no extra cost to you.</p>
            <p>So whatever class of car you’re looking for, we can help you find a great deal.</p>
            <p>Browse the latest available Kia car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
@elseif($make == 'Mini')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Iconic and instantly recognisable, MINI is a proudly British brand of cars that has grown to consist of 5 models with the addition of MINI Electric in 2020.</p>
        <p>Driving a MINI is now more affordable than ever before thanks to car leasing, also known as contract hire.</p>
        <div class="hiddenContent">
            <p>It's simple! Choose your initial rental amount, agreement term, and annual mileage. Then simply pay your monthly rental until you hand the car back at the end of the agreement term. That’s it! </p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>There are hundreds of car leasing companies operating in the UK. So it's hard to know if you're getting the best deal. You also have to be aware of illegitimate scam companies out there. It's a lot to keep up with! Which is why we want to simplify the process of finding a car lease deal.</p>
            <p>We do this by compiling millions of lease deals from trusted car leasing companies.  We handpick each car lease partner and they are all authorised and regulated by the Financial Conduct Authority (FCA).</p>
            <p>Search and compare MINI car lease deals for free on our website! We don’t charge you at all. The price you see on our website is direct from the lease provider. We connect you with a leasing company that has your chosen MINI lease deal on offer.</p>
            <p>Save time and money - find your car lease deal with Auto Lease Compare.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Tesla')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Drive a Tesla for a fraction of the cost with a car lease deal. Here at Auto Lease Compare, we can help you search and compare lease deals on Tesla car models including the Model S, Model X, & Model 3.</p>
        <p>We compile millions of lease deals from trusted UK car leasing companies and make it easy for you to search and find the best car lease deal for you. Our car lease partners are handpicked for your protection as they are authorised and regulated by the Financial Conduct Authority (FCA).</p>
        <div class="hiddenContent">
            <p>Car leasing, also known as contract hire, puts you in control! You choose your initial rental amount, agreement term, and annual mileage. Then simply pay your monthly rental until you hand the car back at the end of the agreement term. It’s that easy!</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>It’s free to compare deals with us! We don’t charge you at all. The price you see on our website is direct from the lease provider. We simply put you in touch with a leasing company that has the best deal for you. So you can be sure that you're getting the best deal available.</p>
            <p>As Tesla continues to grow into the most desirable and respected car manufacturer in the world, now is an exciting time to secure your Tesla car lease deal. Tesla cars regularly receive software updates that add new features and functionality as Tesla continues to innovate. So one day soon, your Tesla could even be turned into an autonomous self-driving vehicle.</p>
            <p>Browse and compare the latest available Tesla car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Nissan')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Find a Nissan car lease deal from our selection of UK car leasing companies. We gather deals from the best leasing companies and help you compare them in one place, making it easy to find the best deal for you.</p>
        <p>Car leasing, also known as contract hire, puts you in control! You choose your initial rental amount, agreement term, and annual mileage. Then simply pay your monthly rental until you hand the car back at the end of the agreement term. It’s that easy!</p>
        <div class="hiddenContent">
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
        <p>Nissan is a truly global brand and for the past 20 years has pioneered green technology in the automotive industry. Their selections of hybrid and electric cars include the popular Nissan LEAF electric model, as well as the Nissan Note and Qashqai hybrid vehicles. So, if you’re after a eco-friendly vehicle, you’ll find one in the Nissan range and we can help you get the best deal.</p>
        <p>We don’t charge you at all. The price you see on our website is direct from the lease provider. We connect you with a leasing company that has your chosen Nissan car lease deal on offer. Browse the latest available deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
@elseif($make == 'Toyota')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Toyota is known all over the world for producing high quality, strong build vehicles that can deliver great performance for years. Here in Britain, it’s a household name with models like the Toyota Aygo, Yaris, and CH-R among the most popular cars on British roads.</p>
        <p>A car lease is the most affordable way to drive a Toyota with prices less than £150 per month from the smallest and most economical urban hatch, the Toyota Aygo. With car leasing, also known as contract hire, you are in control!</p>
        <div class="hiddenContent">
            <p>You choose your initial rental amount, agreement term, and annual mileage. Then simply pay your monthly rental until you return the car to the lease provider at the end of the agreement term. It’s that easy! Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Our car leasing partners are handpicked and strictly vetted. You’ll receive excellent customer service from them, and you are protected from fraud and mis-selling because all the car leasing companies we refer you to are authorised and regulated by the Financial Conduct Authority (FCA).</p>
            <p>We also don’t charge you at all. This is a free car leasing comparison service. The price you see on our website is direct from the lease provider and there are no added or hidden fees from us.</p>
            <p>Browse the latest available Toyota car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

    @elseif($make == 'Hyundai')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Discover the quiet brilliance of Hyundai - one of the biggest car manufacturers in the world.</p>
        <p>Over the last decade, Hyundai has completely revamped its entire line of vehicles with innovative, smartly designed vehicles that are packed with features even at the lowest spec.</p>
        <div class="hiddenContent">
            <p>Taking out a car lease is the most affordable way to drive a Hyundai. It also eliminates the liability of owning a depreciating asset. Car leasing, also known as contract hire, is a method of long term car rental.</p>
            <p>You choose your initial rental amount, agreement term, and annual mileage. Then simply pay your monthly rental amount until you hand the car back at the end of the agreement term. It’s that easy! Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>We select our car leasing partners based on the quality of their service. So you can be assured that you’ll receive great customer service along with your chosen deal. You’re also protected from fraud and unfair treatment as all our partners are regulated by the Financial Conduct Authority and so must abide by strict rules and regulations.</p>
            <p>So whether you are looking for a nippy top-of-the-range city car like the i10, or a sturdy family car like the Tucson or Santa Fe, you’ll find a vehicle for you in the Hyundai range.</p>
            <p>Browse the latest available Hyundai car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Skoda')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Find a Skoda car lease deal from our selection of UK car leasing companies. We make it easy for you to search and compare lease deals from multiple car leasing companies. All our leasing partners offer great deals and brilliant customer service. They are regulated by the Financial Conduct Authority for your protection and assurance.</p>
        <p>Skoda is part of the VW Group and like it’s sister company, Skoda has a reputation for producing reliable, high quality, distinctive cars. So if you want a hatchback, you can take a pick from models which include the popular Skoda Fabia, Octavia, Scala, and Superb - or if you’re after a larger SUV, then check out the Karoq and Kodiaq models.</p>
        <div class="hiddenContent">
            <p>Those new to car leasing can learn more about how car leasing works here. In short, car leasing, also known as contract hire, makes it affordable to drive a brand new car without the risk or worry of owning a depreciating asset. You simply rent and maintain the car until you hand it back at the end of the agreement term which is usually 4 years. It’s that easy!</p>
            <p>Our car leasing comparison service is totally free and impartial. We don’t charge you at all. The deal you choose is directly from the lease provider with no added fees from us. We simply connect you with a leasing company that has your chosen Skoda lease deal on offer.</p>
            <p>Browse our catalogue of Skoda car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Smart')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Smart cars are compact, efficient and cheap to run. The perfect companion for navigating narrow city streets and small parking spaces. Developed by the engineers at Mercedes-Benz, the Smart range has recently been revamped and upgraded to offer all-electric options across the range making the already economical tiny cars even more so.</p>
        <p>You can drive a brand new Smart car by taking advantage of a car lease deal from one of our leasing partners. Car leasing, also known as contract hire, let’s you choose your initial rental amount, agreement term, and annual mileage. Then you simply pay your monthly rental whilst driving and maintaining the car until you hand the car back at the end of the agreement term which is typically 4 years. It’s that easy!</p>
        <div class="hiddenContent">
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>It’s free to search and compare car lease deals on this website! The price you see on our website is direct from the lease provider with no added fees from us. We simply help you search and compare Smart lease deals.</p>
            <p>Our partners are well-established car leasing companies, authorised and regulated by the Financial Conduct Authority (FCA). We hand pick our car lease partners to ensure that you receive the best customer service.</p>
            <p>Browse the latest available Smart car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Seat')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Search and compare Seat car lease deals from trusted UK car leasing companies - all in one place. We gather deals from the best leasing companies making it easy to find the best deal for you.</p>
        <p>Car leasing, also known as contract hire, is a great way to drive a brand new Seat car at an affordable rate. Choose your initial rental amount, agreement term, and annual mileage. Then simply pay your monthly rental and maintain the car until you hand the car back at the end of the agreement term. It’s that easy!</p>
        <div class="hiddenContent">
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>We don’t charge you at all. The price you see on our website is direct from the lease provider. We connect you with a leasing company that has your chosen Seat car lease offer at not extra cost to you.</p>
            <p>Our partners are hand picked, well-established, and trusted car leasing companies operating in the UK. They are all authorised and regulated by the Financial Conduct Authority (FCA). We carefully vet each company to ensure that you’ll receive the best customer service.</p>
            <p>Browse the latest available Seat car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Vauxhall')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Finding a great Vauxhall car lease deal has never been easier! We gather deals from a select group of well established, trusted UK car leasing companies. All you have to do is search, compare, and choose the best deal for you. So you save time and money with Auto Lease Compare!</p>
        <p>With car leasing you don't have to absorb the cost of depreciation as the car loses value over time. You simply enjoy and maintain your Vauxhall car as you pay a low monthly rental amount for 2, 3, or 4 years until the end of the agreement term. At which point you return the car to the lease provider. It’s that easy!</p>
        <div class="hiddenContent">
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>It’s free to search for Vauxhall lease deals on our website. We don’t charge you at all! The price you see on your chosen deal is direct from the lease provider. We simply put you in touch with them at no extra cost to you.</p>
            <p>So whatever class of car you’re looking for, we can help you find a great deal.</p>
            <p>Browse the latest available Vauxhall car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Jaguar')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>The easiest and cheapest way to drive a Jaguar is with a car lease deal, also known as Contract Hire.</p>
        <p>Jaguar manufactures powerful, luxurious, high-end cars that are both stylish and robust. So it’s no wonder that buying a brand new Jaguar could set you back at least £26,000 even before taking upkeep costs into consideration.</p>
        <div class="hiddenContent">
            <p>You could drive any vehicle from the Jaguar range for a fraction of the cost of buying it outright. From the sporty F-TYPE, to the award winning all-electric I-PACE.</p>
            <p>So how does it work? Car leasing, puts you in control! Simply choose your initial rental amount, agreement term, annual mileage, and any additional perks such as maintenance and servicing. Your monthly rental price is based on the specs and additional services you choose.</p>
            <p>Your car will be delivered to you and all you have to do is pay your monthly rental and maintain your Jaguar in good condition until you hand it back at the end of the agreement term. It’s that easy!</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>We compile millions of lease deals from trusted UK car leasing companies. This makes it easy for you to search and find the best car lease deal for you. Our car lease partners are handpicked reputable companies authorised and regulated by the Financial Conduct Authority (FCA).</p>
            <p>It’s free to compare lease deals and contact car lease providers via our website. The price you see here is direct from the lease provider. We simply put you in touch with them at no extra cost to you. So you can be sure that you're getting the best deal available.</p>
            <p>Browse and compare the latest available Jaguar car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Land Rover')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Search and compare Land Rover car lease deals for free. We gather the best Business and Personal Contract Hire deals from reputable UK-based car leasing companies - all in one place. So it’s easy to find the best deal for you.</p>
        <p>Car leasing, also known as Contract Hire, puts you in control! You choose your initial rental amount, agreement term, and annual mileage. Then simply pay your monthly rental until you hand the car back at the end of the agreement term. It’s that easy!</p>
        <div class="hiddenContent">
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Every Land Rover is a perfect balance of luxury, quality, and strength. Be it the sleek Range Rover lineup which includes the Velar, Evoque, and Range Rover Sport, or the more adventurous Land Rover Discovery and Discovery Sport.</p>
            <p>2020 marks the return of the Land Rover Defender! Back with an updated modern look and stunning silhouette. The Defender is the ultimate all-terrain vehicle and a perfect companion for the adventurous spirit.</p>
            <p>Remember, we don’t charge you at all. The price you see on our website is direct from the lease provider. We simply connect you with a leasing company that has your chosen car lease deal on offer.</p>
            <p>Browse the latest available Land Rover lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Lexus')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Lexus is known all over the world for its premium vehicles which are always packed with technology and luxurious features. So leasing a Lexus is a great way to spread the cost and drive in style while maintaining your cash flow.</p>
        <p>We help you search and compare lease deals from a range of reputable car leasing companies all in one place.</p>
        <div class="hiddenContent">
            <p>You choose your initial rental amount, agreement term, annual mileage, and any additional perks such as maintenance and servicing. Your monthly rental price is based on the specs and additional services you choose. Your lease provider will deliver your brand new Lexus to you or you can choose to collect it. Then simply pay your monthly rental until you return the car to the lease provider at the end of the agreement term. It’s that easy!</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Our car leasing partners are handpicked and strictly vetted. They offer the best customer service and are authorised and regulated by the Financial Conduct Authority (FCA). So you can be sure that your consumer rights are protected.</p>
            <p>Our comparison service is free. We don’t charge you at all. The price you see on our website is direct from the lease provider and there are no added or hidden fees from us.</p>
            <p>Browse the latest available Lexus car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Honda')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Easily search and compare Honda lease deals from a range of reputable car leasing companies. You can find the full Honda range here, from the popular Civic and CR-V models, to the reborn, sporty Honda NSX.</p>
        <p>Leasing, also known as Contract Hire, offers you a cheap and affordable method of driving a Honda. It also eliminates the liability of owning a depreciating asset.</p>
        <div class="hiddenContent">
            <p>You simply choose your initial rental amount, agreement term, and annual mileage - as well as any other perks and services available from the lease provider. Then simply pay your monthly rental amount until you hand the car back at the end of the agreement term. It’s that easy!</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>We compile deals from proven and trustworthy UK-based car leasing companies. So you can be assured that you’ll receive great customer service along with your chosen deal. They are also authorised and regulated by the Financial Conduct Authority (FCA). So you can rest assured knowing that you’re protected from any mis-selling, fraud, unfair treatment.</p>
            <p>Browse the latest available Honda car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Mazda')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Find a Mazda car lease deal that’s perfect for you. We make it easy to search and compare deals by compiling all available deals from reputable UK-based car leasing companies all in one place.</p>
        <p>Mazda is a great choice. They offer some of the best-in-class vehicles boasting solid builds, economical running costs, and feature-packed interiors. The MX-5 is famously regarded as the best sports car of the last 25 years and the current fourth-generation has won a What Car? Award every year since its release.</p>
        <div class="hiddenContent">
            <p>Leasing, also known as Contract Hire, is a great way to drive any car for a fraction of the cost compared to buying in cash or through a PCP deal. You simply rent and maintain the car in good condition until you hand it back at the end of the agreement term which is usually 4 years. It’s that easy!</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>We do the hard work for you so all you have to do is pick the best car lease deal and we’ll put you in touch with the car lease provider. Each car lease provider is handpicked and thoroughly vetted. We ensure that they offer great deals and brilliant customer service, and we check that they are regulated by the Financial Conduct Authority for your protection and assurance.</p>
            <p>Our car leasing comparison service is totally free and impartial. We don’t charge you at all. The deal you choose is directly from the lease provider with no added fees from us.</p>
            <p>Browse our catalogue of Mazda car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Porsche')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Buying a brand new Porsche will set you back at least £46,000 for the cheapest model, the 718 Cayman. But with leasing, you could pay less per month to drive a more expensive model.</p>
        <p>A Porsche is a perfect fusion of performance and luxury. As a leading manufacturer of high end vehicles, Porsche knows how to deliver incredibly powerful performance in a range of vehicle bodies, from compact sports cars to spacious SUVs.</p>
        <div class="hiddenContent">
            <p>Leasing, also known as Contract Hire, is the most affordable way to drive a Porsche and we make it easy to find the best car lease deal for you.</p>
            <p>We compile lease deals from reputable UK-based car leasing companies all-in-one place. Our partners are well-established lease providers, authorised and regulated by the Financial Conduct Authority (FCA). So all you have to do is search, compare and choose the best deal hassle-free.</p>
            <p>Car leasing lets you choose your initial rental amount, agreement term, and annual mileage along with any additional perks and services you want from the lease provider. Then you simply pay your monthly rental whilst driving and maintaining the Porsche until you hand the car back at the end of the agreement term which is typically 4 years. It’s that easy!</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>It’s free to search and compare car lease deals here! All quotes are direct from the lease provider with no added fees from us.</p>
            <p>Browse the latest available Porsche car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Peugeot')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Save time and money by comparing Peugeot car lease deals using our comparison tool. We’ve partnered with the best UK car leasing companies to bring you all their deals in one place. Finding a personal or business Peugeot car lease deal has never been easier.</p>
        <p>We don’t charge you at all. The price you see on our website is direct from the lease provider. We simply connect you with the leasing company that has your chosen lease offer at no extra cost to you.</p>
        <div class="hiddenContent">
            <p>Car leasing, also known as Contract Hire, is a great way to drive a brand new car at an affordable rate. You choose your initial rental amount, agreement term, and annual mileage. Then simply pay your monthly rental and maintain the car until you hand the car back at the end of the agreement term. It’s that easy!</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Our partners are hand picked, well-established, and trusted car leasing companies operating in the UK. They are all authorised and regulated by the Financial Conduct Authority (FCA). We carefully vet each company to ensure that you’ll receive the best customer service.</p>
            <p>Browse all the latest available Peugeot car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Citroen')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Finding a great Citroen car lease deal has never been easier! With Auto Lease Compare, all you have to do is search, compare, and choose the best deal for you.</p>
        <p>Car leasing is a cost effective way to drive a beautiful new car at a fraction of the cost. You simply choose your initial rental, agreement term, and annual mileage as well as any other add-ons you want. This determines your monthly rental amount. Then all you have to do is drive and maintain your car in good condition whilst paying your monthly rental amount until the end of the agreement term; at which point you return the car to the lease provider. It’s that easy!</p>
        <div class="hiddenContent">
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>It’s free to search for Citroen lease deals on our website. We don’t charge you at all! The price you see on your chosen deal is direct from the lease provider. We simply put you in touch with them at no extra cost to you.</p>
            <p>All our car leasing partners are strictly vetted to ensure that you receive the best customer service and leasing experience. Every one of our lease providers is authorised and regulated by the Financial Conduct Authority (FCA) so you can be sure that your consumer rights are protected.</p>
            <p>Whatever class of car you’re looking for, Citroen has a great range of vehicles for you to choose from. Browse the latest available Citroen car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Jeep')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Jeep has been producing durable, all-terrain vehicles since the 1940s, but nowadays these stylish 4x4s provide maximum comfort, delivering a luxury travel experience whilst continuing to place adventure at your fingertips.</p>
        <p>It’s never been easier to find a great deal on your dream Jeep! Auto Lease Compare brings you the best prices across all models of Jeep, including the best Renegade, Wrangler, Compass and Grand Cherokee lease deals. Simply search, compare, and choose the best deal for you.</p>
        <div class="hiddenContent">
            <p>Whether you’re looking for personal or business, car leasing allows you to enjoy a stunning, new Jeep at a much more affordable price. We put you in the driver’s seat by giving you control over your initial rental, agreement term, annual mileage and any add-ons, which together determine your monthly rental amount.</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Then you’ll be on your way; all you need to do is continue to pay your monthly rental amount until the end of the agreement term, whilst ensuring the car remains in good condition. At the end of the payment term, you just return the car to the lease provider. It’s that easy!</p>
            <p>It’s free to search for Jeep lease deals on our website. We don’t charge you at all! The price you see on your chosen deal is direct from the lease provider. We simply put you in touch with them at no extra cost to you.</p>
            <p>Your peace of mind is integral to our own. That’s why we go the extra mile to protect your consumer rights. All our car leasing partners are strictly vetted to ensure they share our commitment to delivering an amazing customer experience throughout your journey with us. Crucially, every one of our lease providers is authorised and regulated by the Financial Conduct Authority (FCA).</p>
            <p>There’s a Jeep for everyone at Auto Lease Compare. Browse the latest available Jeep car lease deals below to find yours.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

    @elseif($make == 'Renault')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Renault has been at the forefront of vehicle manufacturing since 1899 and their commitment to easy handling slick aesthetics has made them a worldwide favourite when it comes to delivering a smooth and reliable travel experience.</p>
        <p>With Auto Lease Compare, you can drive your dream Renault at a more affordable price than you ever thought possible. From the classic Clio and Megane to the fully-electric Zoe, Auto Lease Compare brings you the best lease prices across Renault’s most popular ranges. If you’re looking for a stunning SUV, make sure you browse our Renault Captur, Kadjar and Koleos lease deals.</p>
        <div class="hiddenContent">
            <p>Whether you’re looking for personal or business, car leasing allows you to enjoy a stunning, new Renault at a price controlled by you. We put you in the driver’s seat by giving you control over your initial rental, agreement term, annual mileage and any add-ons, which together determine your monthly rental amount.</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Then all you need to do is ensure the car remains in good condition whilst continuing to make your monthly payments until the end of the agreement term (typically 2-4 years), at which point you’ll return your Renault to the lease provider. Simple, right?</p>
            <p>It’s free to search for Renault lease deals on our website. We’re an impartial service for browsing the best leasing prices, so we don’t charge you at all! The price you see on your chosen deal is direct from the lease provider; we simply put you in touch with them at no extra cost to you.</p>
            <p>We also pride ourselves on going the extra mile to protect your consumer rights. All our car leasing partners have been strictly vetted and hand-picked to ensure they share our commitment to delivering an amazing customer experience. And for extra peace of mind,  every one of our lease providers is authorised and regulated by the Financial Conduct Authority (FCA).</p>
            <p>Browse the latest available Renault car lease deals below and start your next journey today.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

    @elseif($make == 'Fiat')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Looking to lease a Fiat? Look no further. Undoubtedly one of Italy’s famous manufacturers, Fiat have been manufacturing stylish vehicles for over 120 years. From the perfect family car in the Panda to the classy Tipo, we have Fiat lease deals for everyone at Auto Lease Compare.</p>
        <p>In recent times the Fiat 500 has taken the world by storm to become one of the Fiat’s most popular ever ranges. From the standard edition to the 500L and 500X, our Fiat 500 lease deals cover all models.</p>
        <div class="hiddenContent">
            <p>At Auto Lease Compare, our simple, free-to-use comparison tool puts the best deals from the UK’s most-trusted leasing companies at your fingertips in a matter of seconds, saving you time and money.</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Simply select your initial rental amount, preferred term and annual mileage and away you go; all that’s left is to pay your monthly rental and keep the car in a condition as good as the feeling you get driving it, then hand the car back to the leasing company at the end of your agreement term.</p>
            <p>Better still, our service is completely free. All the prices you see on our website are supplied directly from the lease provider. We simply connect you with the leasing company offering your selected price at no extra cost to you.</p>
            <p>It’s hugely important to us that our partners share our drive when it comes to delivering a great customer experience. All of our leasing partners are carefully-selected, established, and trusted companies based in the UK. They are all authorised and regulated by the Financial Conduct Authority (FCA).</p>
            <p>Browse all the latest available Fiat car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>


    @elseif($make == 'Dacia')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Widely-perceived as one of the better car manufacturers when it comes to value for money, Dacia have been combining affordable with reliable for over 50 years. And with a wide-range of lease deals available across popular models there’s never been a better time to secure your dream Dacia.</p>
        <p>At Auto Lease Compare, our free comparison tool collates the very best Dacia lease deals offered by the UK’s most reputable leasing companies and puts them all right at your fingertips, saving you time, money and hassle.</p>
        <div class="hiddenContent">
            <p>From the Logan to the Sandero and everything in-between, our contract hire offers cover all of your favourite models, including the latest Dacia Duster lease deals.</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Auto Lease Compare puts you in complete control; you enter your initial rental amount, preferred term and annual mileage, plus any extras, and we’ll do the rest at no extra cost.</p>
            <p>Our service is completely free to use, with all the prices you see on our website supplied directly from the lease provider; we simply help you find your dream car at your dream price, then connect you with the leasing company. All you have to do is pay your monthly rental for the duration of your agreement term and keep the car in good condition, then return it at the end of your lease period.</p>
            <p>Delivering an exceptional customer experience is our main priority and it’s immensely important to us that our partners feel the same. Every leasing company featured on our website has been hand-picked and carefully-vetted., as well as being UK-based, reputable and authorised and regulated by the Financial Conduct Authority (FCA).</p>
            <p>Browse all the latest available Dacia car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

    @elseif($make == 'Alfa Romeo')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Browse the best Alfa Romeo lease deals, brought to you by Auto Lease Compare.</p>
        <p>Since 1910, Alfa Romeo’s respected reputation has continued to grow as a result of expert engineering combined with savvy use of the latest technology.</p>
        <p>From Alfa Romeo Giulia lease deals to the powerful Stelvio and the stunning Giulietta hatchback, there’s an Alfa Romeo for everyone with Auto Lease Compare.</p>
        <div class="hiddenContent">
            <p>Better still, whether business or personal, your Alfa Romeo car lease is always on your terms with our free comparison tool. From your initial rental deposit to your preferred term and annual mileage, you control everything. Simply let us know what you’re looking for, and we’ll collate the best deals and prices from the UK’s most reputable leasing companies. Then all you have to do is choose the deal that’s right for you!</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Our service is completely free to use, with all the prices you see on our website supplied directly from the lease provider; we simply connect you with your perfect agreement.</p>
            <p>All you have to do is pay your monthly rental for the duration of your agreement term and keep the car in good condition, then return it at the end of your lease period.</p>
            <p>Delivering an exceptional customer experience is the very reason that we exist. All of our leasing partners are both authorised and regulated by the Financial Conduct Authority (FCA), but we also go the extra mile to make sure that they’re as committed as we are to delivering unbeatable customer service. In fact, every leasing company featured on Auto lease Compare has been carefully vetted and hand-selected.</p>
            <p>Browse all the latest available Alfa Romeo lease opportunities below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>


    @elseif($make == 'Mitsubishi')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Founded in 1870, Mitsubishi’s tough and durable models have become favourites around the world, enabling the adventurous to explore new horizons in unrivalled comfort.</p>
        <p>With incredible Mitsubishi Outlander lease deals available and other popular models including the Eclipse, Mirage and Shogun all available there’s never been a better time to secure your dream Mitsubishi motor.</p>
        <div class="hiddenContent">
            <p>Through our carefully-vetted, hand-picked leasing partners - each of which is authorised and regulated by the Financial Conduct Authority (FCA) - Auto Lease Compare brings you the best Mitsubishi lease deals from the UK’s most reputable leasing companies all in one place; saving you time, effort and money.</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>All you have to do is select your initial rental amount, annual mileage and contract term and we’ll do the rest. Then just pay the agreed amount each month, keep your Mitsubishi in good condition and simply return the vehicle at the end of your lease term. Easy!</p>
            <p>With both personal and business Mitsubishi contract hire available from just £113.50, car leasing is an affordable way to drive a new vehicle without having to worry about depreciation. Best of all, we won’t charge you a single penny; the only payments you’ll make are those agreed with the leasing provider.</p>
            <p>Ready to set the wheels in motion? You can browse all the latest Mitsubishi car lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

    @elseif($make == 'Abarth')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Since 1949, Abarth has proudly headed up the sports division of Fiat, taking already popular designs such as the Fiat 500 and notching up the level of power and performance with a slightly sportier appearance to match.</p>
        <p>At Auto Lease Compare you’ll find the best Abarth lease deals across the manufacturer’s most popular models, including the best contract hire rates for both the 695 and 595.</p>
        <div class="hiddenContent">
            <p>Auto Lease Compare is a completely free comparison tool that gives you complete control, from the amount you’ll pay up front to your annual mileage and the length of agreement term, plus any extras.</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>We’ll then provide you with the very best rates from the UK’s most reputable leasing companies, each of which has been carefully-vetted and hand-picked by our team to ensure you’ll receive exceptional customer service. All you need to do is select the lease term that’s right for you, then make your monthly payments as agreed until the end of your Abarth lease term, at which point you’ll simply return the vehicle to the leasing company in its original condition.</p>
            <p>Transparency is key to us; the prices you see are supplied directly by our leasing partners, who are authorised and regulated by the Financial Conduct Authority (FCA). There’s no referral fee or cost to pay to Auto Leasing Compare, we’re just here to improve your leasing experience by helping you source the perfect deal.</p>
            <p>Ready to start your journey? You’ll find all our latest Abarth lease offers below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>



    @elseif($make == 'Suzuki')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Suzuki introduced its first car to the world in 1955 and has since grown into one of japan’s largest manufacturers with a proven track record for delivering excellence and efficiency.</p>
        <p>From SUVs to stylish hatchbacks, you’ll find all your favourite models on Auto lease Compare, including unbeatable Suzuki Swift, Jimny and Vitara lease deals. If you’re after a comfortable hatchback you’ll struggle to beat the Swift, but if you desire something with a bit more room then a Suzuki Vitara lease could be the one for you.</p>
        <div class="hiddenContent">
            <p>The good news? It’s free to search Suzuki lease deals on our website. We don’t charge you a single penny! The price you see on your chosen deal is direct from the lease provider. We simply put you in touch with them at no extra cost to you.</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <p>Whether you require business contract hire or a personal lease, Suzuki car leasing gives you the chance to enjoy a new Suzuki at a fraction of the cost.</p>
            <p>We put you in the driver’s seat; simply tell us what you’re looking for, how long for and your annual mileage, and we’ll scan the UK’s most reputable leasing companies to bring you the best Suzuki contract hire offers in a matter of seconds.</p>
            <p>Once you’ve chosen a car lease deal that’s right for you, all you need to do is make your monthly payments, maintain the car and return it to the leasing company at the end of your agreement term. It’s that simple!</p>
            <p>All the Suzuki car lease deals that you’ll find on our site are supplied by UK-based leasing companies that have been carefully selected and are authorised and regulated by the Financial Conduct Authority (FCA), so you can rest assured knowing that you’ll receive exceptional service.</p>
            <p>You’ll find our latest suzuki lease hire opportunities below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Cupra')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>If you’re searching for a new flashy high-performance car then why not look at leasing a brand new Cupra?</p>
        <p>Cupra is a sports car brand that was born from the Seat brand in 2018. As well as bringing a breath of fresh air to the market, they have inherited a pedigree of manufacturing high-performance cars that deliver excitement from their parent manufacturer.</p>
        <div class="hiddenContent">
            <h2>Cupra Leasing</h2>
            <p>Leasing a great and affordable way to get your hands on all the latest and greatest models like Cupra without having to pay a huge upfront cost or commit to a longer costly finance plan. Leasing a Cupra is a great way to immerse yourself in the culture of a new and innovative manufacturer</p>
            <p>You can compare lease deals on Cupra through us to find the best deal that works for you. Whether you’re looking to compare personal car leasing or business car leasing, with Auto Lease Compare we’ll bring you a whole host of Cupra leasing deals from a range of providers.</p>
            <p>Choose from the Cupra Ateca SUV built with practicality and performance in mind, the luxurious Cupra Formentor e-Hybrid, the high-octane Cupra Leon which is based on its Ceat cousin, or the Cupra Leon Estate for a mixture of practicality and performance.</p>
            <p>Browse our range of brand Cupra lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif(strtoupper($make) == 'MG MOTOR UK')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Leasing your dream MG car just got a lot easier! Auto Lease Compare - the #1 car leasing comparison site in the UK, make sure that your ideal car is only moments away.</p>
        <p>MG specialise in petrol, electric and hybrid cars/SUVs, with style and luxury taking the driving seat. Buying outright might not be the most appropriate option for your finances because these cars don’t come cheap. This is where car leasing is here to help.</p>
        <div class="hiddenContent">
            <p>Car leasing, also known as contract hire, makes it easy and affordable to drive a brand new MG car. With so many car leasing companies around, it's hard to know if you're getting the best MG lease deal. Here at Auto Lease Compare, we will assist you by searching and comparing all deals relevant to MG car leasing. The partners we collaborate with are all well-established car leasing companies, authorised and regulated by the Financial Conduct Authority (FCA)</p>
            <p>When comparing car leasing deals, you don’t have to worry about costs. It’s completely free to compare deals on Auto Lease Compare. The price you see on our website is directly quoted from the lease provider.</p>
            <p>If you’re looking to get clued up on car leasing before you take the next steps, check out our guide <a href="https://www.autoleasecompare.com/leasing-information">here.</a></p>
            <p>There are 5 models to choose from included in our current range, including the MG HS and the MG MG3. Whether you’re wanting a petrol car or an electric car, we’ve got you covered!</p>
            <p>Browse our latest available MG lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>
    @elseif($make == 'Polestar')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>
        <p>Browse the best Polestar lease deals from trusted suppliers quickly and easily with Auto Lease Compare.</p>
        <p>Founded by Polestar racing - a high-performance branch of the Volvo Car Group - a Polestar car lease will allow you to experience supreme comfort, stunning aesthetics and innovative racing technology adapted for the road for the first time.</p>
        <p>And despite its slick appearance and powerful performance, the Polestar has been built with sustainability at its core to create a vehicle that’s electric, efficient and environmentally responsible.</p>
        <div class="hiddenContent">
            <h2>Polestar 2 Leasing</h2>
            <p>Modern, stylish and bursting with modern technology, the Polestar 2 has become one of the most sought after vehicles. This all-electric five-door vehicle features precision engineering and a twin-electric motor capable of delivering a 0-62mph time of just 4.7 seconds.</p>
            <p>Building on the precedent set by the Polestar 1, Polestar 2 leasing offers excellent performance and a robust, quality build, plus a next level entertainment and navigation system powered by Android, including Google Assistant, Maps and Play.</p>
            <p>Better still, our fast and simple comparison tool makes it easy for you to browse Polestar 2 lease deals in just a few seconds, while allowing you to completely customise your search to return the deals that meet your personal requirements.</p>

            <p>And because we work with trusted leasing companies across the UK, we’re able to offer a vast range of both Polestar 2 business leases and personal leases.</p>
            <p>Learn more: <a href="https://www.autoleasecompare.com/leasing-information">How car leasing works.</a></p>
            <h2>Benefits of Polestar Leasing</h2>
            <p>Polestar leasing allows you to drive Volvo’s stunning, high-performance range for a fraction of the cost, with road tax included and no MOT due for the first two years. Everything's better when it’s stress-free; you’ll be covered by the manufacturer’s warranty and we’ll arrange free delivery of your vehicle to your chosen address, whether that’s your home or your workplace.</p>
            <h2>How it Works</h2>
            <p>Once you’ve found the perfect lease, simply click through to the lease provider’s or contact them using the details provided. The dealer will then finalise the agreement and arrange delivery of your vehicle. Then all you need to do is make the agreed monthly payments for the duration of your Polestar car lease, then return your vehicle in good condition at the end of the lease period.</p>
            <h2>Why Auto Lease Compare?</h2>
            <p>We’re committed to making car leasing a simple, fully-transparent process and delivering exceptional customer experience (we were even named the <a href="https://www.autoleasecompare.com/blog/Winners---Best-Emerging-Car-Leasing-Start-up-UK">UK’s best emerging car lease startup</a>). We only work with carefully selected suppliers who share these goals and have built trusted reputations based on their remarkable customer service level. All of our partners are members of the BVRLA and, like us, are regulated by the Financial Conduct Authority (FCA).</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

    @elseif($make == 'Maserati')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Looking to lease a Maserati car? Leasing your perfect Maserati is only a few clicks away through Auto Lease Compare - the UK’s #1 car leasing comparison site.</p>
        <p>Maserati oozes Italian luxury and elegance, specialising in petrol cars, hybrid cars, SUVs and sports cars. Buying outright might not be the best option for your finances as these cars come with a hefty price tag. This is where car leasing steps in.</p>
        <div class="hiddenContent">
            <p>Car leasing, also known as contract hire, makes it much more affordable to drive a brand new Maserati. With the internet being saturated with car leasing companies, it's hard to know if you're getting the best Maserati lease deal. Here at Auto Lease Compare, we’re here to help by searching and comparing all deals relevant to Maserati car leasing for you. All of our listed partners are all well-established car leasing companies, authorised and regulated by the Financial Conduct Authority (FCA).</p>
            <p>Comparing car leasing deals doesn't cost a penny when searching on our site, so you can compare as many deals as you wish without incurring any costs. The prices listed on our website are direct prices from the lease provider themselves.</p>
            <p>Need more info on car leasing? Check out our handy guide to all things car leasing here.</p>
            <p>Browse our latest Maserati lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

    @elseif($make == 'Ssangyong')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Interested in a SsangYong car but can’t afford to buy it outright? Auto Lease Compare are on hand with a number of leasing options to make sure that your ideal SsangYong car isn’t just a distant dream.</p>
        <p>SsangYong specialises in 4x4s, SUVs and People Carriers for those that are looking for that little bit of extra space or seating in their car. If buying a car isn’t for you because of the often expensive price tag, car leasing could be for you.</p>
        <div class="hiddenContent">
            <p>Car leasing, also known as contract hire, makes it much more affordable to drive a brand new SsangYong car. With the internet being saturated with car leasing companies, it's hard</p>
            <p>to know if you're getting the best SsangYong lease deal. Here at Auto Lease Compare, we’re here to help by searching and comparing all deals relevant to Maserati car leasing for you. All of our listed partners are all well-established car leasing companies, authorised and regulated by the Financial Conduct Authority (FCA).</p>
            <p>Comparing SsangYong leasing deals is a quick and free service on our site, so you don’t have to pay a penny when comparing deals. The prices listed on our website are direct prices from the lease provider themselves.</p>
            <p>Unsure about car leasing and need more information? Check out our help page on all things car leasing <a href="/leasing-information">here.</a></p>
            <p>Browse our latest SsangYong lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

    @elseif($make == 'Subaru')
        <h2 class="headerText"> {{ $make . $headerWordConcat }} </h2>
        <div class="float-right d-flex flex-column align-items-center">
            <a href="/car-leasing/{{str_replace(' ', '-', strtolower($make))}}" title="{{ucwords(strtolower($make))}}">
                <img src="/images/manufacturers/{{strtolower($make)}}.png" class="img-responsive" alt="{{strtolower($make)}} logo" width="100"  height="100">
            </a>
        </div>

        <p>Interested in a Subaru lease? Leasing your perfect Subaru is a quick and simple process through Auto Lease Compare - the UK’s #1 car leasing comparison site.</p>
        <p>Subaru specialises in luxury SUVs and sports cars with both petrol and hybrid options. If buying a Subaru outright seems off putting to you because of its expensive price tag, car leasing may be a suitable option instead.</p>
        <div class="hiddenContent">
            <p>Car leasing, also known as contract hire, makes it much more affordable to drive a brand new Subaru. With the internet full of car leasing companies, it's hard to know if you're getting the best Subaru lease deal. Here at Auto Lease Compare, we’re here to help by searching and comparing all deals relevant to Subaru car leasing for you. All of our listed partners are all well-established car leasing companies, authorised and regulated by the Financial Conduct Authority (FCA).</p>
            <p>Searching our site for Subaru leasing deals is completely free to do, so you can compare as many deals as you wish without it costing a penny. The prices listed on our website are direct prices from the lease provider themselves.</p>
            <p>We currently have a range of Subaru models currently available to lease such as the Subaru Forester, the Subaru Levorg, the Subaru XV and the Subaru Impreza.</p>
            <p>Unsure about car leasing? Check out our helpful car leasing information page <a href="/leasing-information">here.</a></p>
            <p>Browse our latest Subaru lease deals below.</p>
        </div>
        <a href="#" class="jumbotronReadMore">Read More</a>

    @endif


</div>
