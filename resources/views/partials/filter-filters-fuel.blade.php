@php $reqArr = array(); @endphp
@foreach($fuels as $fuel)
    @if(!is_null($fuel->fuel))
        @if($fuel->fuel != 'Petrol LPG')
    <div class="col-md-3 col-sm-3 text-center">
        <label><input type="checkbox" class="invisible" name="fuel[]" id="{{str_replace(' ', '-', $fuel->fuel)}}Checkbox" value="{{$fuel->fuel}}"
                    @php
                        if(is_array(app('request')->input('fuel'))) {
                            $reqArr = app('request')->input('fuel');
                        }
                        if(in_array($fuel->fuel, $reqArr)) {
                            echo "checked='checked'";
                        }
                    @endphp
            >
            <div class="imgdiv"><img src="/images/{{$fuel->fuel}}.png" class="filtericons">{{$fuel->fuel}}</div></label>
    </div>
        @endif
    @endif
@endforeach