<div class="container-fluid bg mb-5">
    <div class="row">
        <div class="col-md-12 pt-5 pb-5" style="padding-top: 1rem!important;padding-bottom: 0rem!important">
            @if($database == 'LCV')
                <h1 style="color:#fff; text-align: center">Compare @if(isset($make)) {{$make}} @endif Van Lease Deals</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/vans" style="color:#fff;">Van Lease Comparison (Home)</a>@if(isset($make)) / {{ $make }} @endif Lease Deals</div>
            @else
                <h1 style="color:#fff; text-align: center">Compare @if(isset($make)) {{$make}} @endif Lease Deals</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Car Lease Comparison (Home)</a>@if(isset($make)) / {{ $make }} @endif Lease Deals</div>
            @endif

        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col">
            @if($database == 'LCV')
                <p>Oh no! It looks like we don't currently have any deals available for this manufacturer! Our deals are updated daily, so be sure to keep checking back. In the meantime, return to our <a href="/vans">homepage</a> or check out our <a href="/personal-vans">personal</a> and <a href="/business-vans">business</a> pages for the best van lease deals on the web!</p>
            @else
                <p>Oh no! It looks like we don't currently have any deals available for this manufacturer! Our deals are updated daily, so be sure to keep checking back. In the meantime, return to our <a href="/">homepage</a> or check out our <a href="/personal">personal</a> and <a href="/business">business</a> pages for the best car lease deals on the web!</p>
            @endif

        </div>
    </div>
</div>