@if (count($errors) > 0)
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div>
@endif

<div id="user_basics_form">
    <form method="POST" action="/profile/{{$user->id}}/updateUserAccount" accept-charset="UTF-8" autocomplete="off">
        {!! csrf_field() !!}
        <input name="_method" type="hidden" value="PUT">
        <input autocomplete="false" name="hidden" type="text" style="display:none;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6" id="leftColumn">
                            <div class="profileBlockBackground">
                                <h3>Personal Details</h3>
                                <label for="first_name">First Name</label>
                                <input type="text" id="first_name" name="first_name" value="{{$user->first_name}}">
                                <label for="last_name">Last Name</label>
                                <input type="text" id="last_name" name="last_name" value="{{$user->last_name}}">
                            </div>
                        </div>
                        <div class="col-sm-6" id="rightColumn">
                            <div class="profileBlockBackground">
                                <h3>Contact Details</h3>
                                <label for="email">Email Address</label>
                                <input type="email" id="emailAddress" name="email" value="{{$user->email}}">
                                <label for="phoneNumber">Phone Number</label>
                                <input type="text" id="phoneNumber" name="phone" value="{{$user->phone}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6" id="leftColumn">
                            <div class="profileBlockBackground">
                                <h3>Marketing Preferences</h3>
                                <div class="row">
                                    <div class="col-1">
                                        <input type="checkbox" id="mailing_list" name="mailing_list" @if($user->mailing_list)checked @endif>
                                    </div>
                                    <div class="col-10">
                                        <label for="mailing_list" data-toggle="tooltip" data-placement="top" title="Checking this will enable us to keep in touch with you via our marketing emails">Marketing Emails<i class="fa fa-info-circle infobutton"></i></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-1">
                                        <input type="checkbox" id="garage_price_notification" name="garage_price_notification" @if($user->garage_price_notification)checked @endif>
                                    </div>
                                    <div class="col-10">
                                        <label for="garage_price_notification" data-toggle="tooltip" data-placement="top" title="Checking this will enable us to notify you when cars in your garage prices drop!">Price drop notifications<i class="fa fa-info-circle infobutton"></i></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6" id="rightColumn">
                            <div class="profileBlockBackground">
                                <h3>Submit</h3>
                                <p>Make sure your information is up to date so our dealers can contact you</p>
                                <input type="submit" value="Update Details" class="btn btn-primary">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</form>
<div class="container">
    <div class="row">
        <div class="col-sm-12"><br>
            <h2>Delete account</h2>
            <p>If you'd like to delete your account, you can below. We will delete all of your user info but still keep a record of any leasing deals you have completed.</p>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div id="deleteAccount" class="profileBlockBackground">
                <div id="deleteContent">
                <p class="margin-bottom-2 text-center">
                    <i class="fa fa-exclamation-triangle fa-fw" aria-hidden="true"></i>
                    <strong>Deleting</strong> your account is <u><strong>permanent</strong></u> and <u><strong>cannot</strong></u> be undone.
                    <i class="fa fa-exclamation-triangle fa-fw" aria-hidden="true"></i>
                </p>

                <div class="row">
                    <div class="col-sm-6 offset-sm-3 text-center">

                        {!! Form::model($user, array('action' => array('ProfilesController@deleteUserAccount', $user->id), 'method' => 'DELETE')) !!}

                        <div class="btn-group btn-group-vertical margin-bottom-2 custom-checkbox-fa" data-toggle="buttons">
                            <label class="btn no-shadow" for="checkConfirmDelete">
                                <input type="checkbox" name='checkConfirmDelete' id="checkConfirmDelete">
                                <i class="fa fa-square-o fa-fw fa-2x"></i>
                                <i class="fa fa-check-square-o fa-fw fa-2x"></i>
                                <span class="margin-left-2"> Confirm Account Deletion</span>
                            </label>
                        </div>
                        <input type="submit" id="delete_account_trigger" type="button" class="btn btn-block btn-danger" disabled="disabled" value="Delete My Account">

                        {!! Form::close() !!}

                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>