<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a class="transmissionFilterHeader mb-3 back"><strong><i class="fa fa-angle-double-left"></i> Back</strong></a>
            <strong class="mb-4">Select Transmission</strong>
            @php
                if(is_array(app('request')->input('transmission'))) {
                    $reqArr = app('request')->input('transmission');
                }else{
                    $reqArr = array();
                }
            @endphp

            <select name="transmission[]" id="mobileTransmissionSelect" class="mobileTransmissionFilter mt-2" multiple>
                @foreach($transmissions as $transmission)
                    <option value="{{$transmission->transmission}}" @if(in_array($transmission->transmission, $reqArr)) selected="selected" @endif>{{$transmission->transmission}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>