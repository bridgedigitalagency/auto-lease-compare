@php if(is_array(app('request')->input('annual_mileage'))) { $reqArr = app('request')->input('annual_mileage'); }else{ $reqArr = array(); } @endphp
{{--<div class="col-md-3 col-xs-4 text-center">--}}
{{--    <label>--}}
{{--        <input type="checkbox" name="annual_mileage[]" @if(in_array( '5000', $reqArr)) checked @endif value="5000" class="d-none" id="5000Checkbox">--}}
{{--        <div class="textlabel">5000</div>--}}
{{--    </label>--}}
{{--</div>--}}
{{--<div class="col-md-3 col-xs-4 text-center">--}}
{{--    <label>--}}
{{--        <input type="checkbox" name="annual_mileage[]" @if(in_array( '8000', $reqArr)) checked @endif value="8000" class="d-none" id="8000Checkbox">--}}
{{--        <div class="textlabel">8000</div>--}}
{{--    </label>--}}
{{--</div>--}}
{{--<div class="col-md-3 col-xs-4 text-center">--}}
{{--    <label>--}}
{{--        <input type="checkbox" name="annual_mileage[]" @if(in_array( '10000', $reqArr)) checked @endif value="10000" class="d-none" id="10000Checkbox">--}}
{{--        <div class="textlabel">10000</div>--}}
{{--    </label>--}}
{{--</div>--}}
{{--<div class="col-md-3 col-xs-4 text-center">--}}
{{--    <label>--}}
{{--        <input type="checkbox" name="annual_mileage[]" @if(in_array( '12000', $reqArr)) checked @endif value="12000" class="d-none" id="12000Checkbox">--}}
{{--        <div class="textlabel">12000</div>--}}
{{--    </label>--}}
{{--</div>--}}
{{--<div class="col-md-3 col-xs-4 text-center">--}}
{{--    <label>--}}
{{--        <input type="checkbox" name="annual_mileage[]" @if(in_array( '15000', $reqArr)) checked @endif value="15000" class="d-none" id="15000Checkbox">--}}
{{--        <div class="textlabel">15000</div>--}}
{{--    </label>--}}
{{--</div>--}}

{{--<div class="col-md-3 col-xs-4 text-center">--}}
{{--    <label>--}}
{{--        <input type="checkbox" name="annual_mileage[]" @if(in_array( '20000', $reqArr)) checked @endif value="20000" class="d-none" id="20000Checkbox">--}}
{{--        <div class="textlabel">20000</div>--}}
{{--    </label>--}}
{{--</div>--}}

{{--<div class="col-md-3 col-xs-4 text-center">--}}
{{--    <label>--}}
{{--        <input type="checkbox" name="annual_mileage[]" @if(in_array( '30000', $reqArr)) checked @endif value="30000" class="d-none" id="30000Checkbox">--}}
{{--        <div class="textlabel">30000</div>--}}
{{--    </label>--}}
{{--</div>--}}
@if(isset($annual_mileages))
    @foreach($annual_mileages as $mileage)
        <div class="col-md-3 col-xs-4 text-center">
            <label>
                <input type="checkbox" name="annual_mileage[]" @if(in_array( $mileage->annual_mileage, $reqArr)) checked @endif value="{{$mileage->annual_mileage}}" class="d-none" id="{{$mileage->annual_mileage}}Checkbox">
                <div class="textlabel">{{$mileage->annual_mileage}}</div>
            </label>
        </div>
    @endforeach
@endif