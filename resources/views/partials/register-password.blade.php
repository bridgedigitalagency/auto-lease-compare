<div class="col-sm-8 offset-md-2 passbox">
    <h2 class="text-center" style="font-weight: 900;">Why not create an account?</h2>
    <p class="text-center">This will make it easier to;<br />
        <i class="fa fa-check"></i> Manage and track your enquiries<br />
        <i class="fa fa-check"></i> Save cars to your garage<br />
        <i class="fa fa-check"></i> Track price history of cars<br />
        <i class="fa fa-check"></i> Receive notifications via email when your saved car drops in price<br />

{{--            <strong class="text-center b3 p-1 text-white">Password needs to have at least 6 characters</strong>--}}

    </p>
            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="form-group row d-none">
                    <div class="col-md-6  pl-0">
                        <label for="first_name">{{ __('First Name') }}</label>
                        <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{substr($confirm_quote->name, 0, strpos($confirm_quote->name, ' '))}} {{ old('first_name') }}" required autofocus>

                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                        @endif

                    </div>
                    <div class="col-md-6 pl-0 d-none">
                        <label for="last_name">{{ __('Last Name') }}</label>
                        <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{substr($confirm_quote->name, strpos($confirm_quote->name, ' '), strlen($confirm_quote->name))}} {{ old('last_name') }}" required autofocus>
                        @if ($errors->has('last_name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>

                <div class="form-group row d-none">
                    <label for="email">{{ __('E-Mail Address') }}</label>
                    <div class="col-md-12 pl-0">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$confirm_quote->email}}" required>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                 <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password">{{ __('Password') }}</label>
                    <div class="col-md-12 pl-0">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                             </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm" >{{ __('Confirm Password') }}</label>
                    <div class="col-md-12  pl-0">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>
                {{--@if(config('settings.reCaptchStatus'))
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-4">
                            <div class="g-recaptcha" data-sitekey="{{ config('settings.reCaptchSite') }}"></div>
                        </div>
                    </div>
                @endif--}}
                <div class="form-group row">
                    <div class="col-md-8 pl-0 mt-1">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="mailing_list" name="mailing_list" style="display: inline-block" value="1"> {{ __('Subscribe to our hot deals') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <button type="submit" class="button b3">
                            <i class="fa fa-sign-in"></i>  {{ __('Register') }}
                        </button>
                    </div>
                </div>
                {{--<div class="row">
                    <div class="col-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
                        <p class="text-center mb-4">
                            Or Use Social Logins to Register
                        </p>
                        @include('partials.socials')
                    </div>
                </div>--}}
            </form>

</div>




