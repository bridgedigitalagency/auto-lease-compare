<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a class="bodystyleFilterHeader mb-3 back"><strong><i class="fa fa-angle-double-left"></i> Back</strong></a>
            <strong class="mb-4">Select Body Style</strong>
            @php
                if(is_array(app('request')->input('bodystyle'))) {
                    $reqArr = app('request')->input('bodystyle');
                }else{
                    $reqArr = array();
                }
            @endphp
            <select name="bodystyle[]" id="mobileBodystyle" class="mobileBodystyle mt-2" multiple>
                @foreach($bodystyles as $bodystyle)
                    <option value="{{$bodystyle->bodystyle}}" @if(in_array($bodystyle->bodystyle, $reqArr)) selected="selected" @endif>{{$bodystyle->bodystyle}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>