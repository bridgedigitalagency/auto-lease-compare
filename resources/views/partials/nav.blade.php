<div class="whiteBg">
<div class="container">
    <nav class="navbar navbar-expand-lg">
        <div class="logo">
            <a class="navbar-brand" href="
            @if(isset($database) && $database == 'LCV')
            {{ url('/vans') }}
            @else
            {{ url('/') }}
            @endif
                    ">
                <img src="/images/alc.svg" alt="Auto Lease Compare" class="img-fluid logo logohead" style="width: 330px;">
            </a>
        </div>
        <div class=" mr-auto"></div>
        {{--    MOBILE MENU    --}}
        <div class="visible-xs visible-sm hidden-md hidden-lg-block navbar-light text-right" id="openMobileMenu">
            <button type="button" class="navbar-toggler" id="mobileMenuToggle"><span class="navbar-toggler-icon"></span> <span class="sr-only">Toggle Navigation</span></button>
        </div>

        <button type="button" class="close" aria-label="Close" style="display: none;" id="closeMobileMenu">
            <span aria-hidden="true">&times;</span>
        </button>

        {{--    DESKTOP MENU    --}}
        <div class="hidden-xs hidden-sm visible-md visible-lg-block">

            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                {{-- Right Side Of Navbar --}}
                <div class="dropdown">
                    <a class="mr-4 pointer d-none d-md-none d-lg-block" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-search"></i> Car Leasing
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <a class="dropdown-item" href="/">Car Leasing Homepage</a>
                        <a class="dropdown-item" href="/personal">Personal Car Leasing</a>
                        <a class="dropdown-item" href="/business">Business Car Leasing</a>
{{--                        <a class="dropdown-item" href="/performance">Performance Car Leasing</a>--}}
                        <a class="dropdown-item" href="/hybrid">Hybrid & Electric Car Leasing</a>
                    </div>
                </div>
                <div class="dropdown">
                    <a class="mr-4 pointer d-none d-md-none d-lg-block" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-search"></i>Van Leasing
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu3">
                        <a class="dropdown-item" href="/vans">Van Leasing Homepage</a>
                        <a class="dropdown-item" href="/personal-vans">Personal Van Leasing</a>
                        <a class="dropdown-item" href="/business-vans">Business Van Leasing</a>
                    </div>
                </div>
                @guest
                    <a class="menu-item mr-4 pointer" data-toggle="modal" data-target="#login"><i class="fa fa-tachometer d-none d-md-none d-lg-inline"></i> My Garage</a>
                @else
                    <a class="menu-item mr-4 pointer" href="/my-account/garage"><i class="fa fa-tachometer d-none d-md-none d-lg-inline"></i> My Garage</a>
                @endguest

                <div class="dropdown d-none d-md-none d-lg-block">
                    <a class="mr-3 pointer" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-rocket"></i> Leasing Help
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu3">
                        <a class="dropdown-item" href="#about">About Us</a>
                        <a class="dropdown-item" href="/leasing-information">Leasing FAQs</a>
                        <a class="dropdown-item" href="/blog">News & Leasing Guides</a>

                        <p style="font-weight: bold; border-top: 1px solid #666; padding: 10px 1.5rem 0; margin-top: 10px"><strong>Other Services:</strong></p>

                        <a href="https://clk.omgt1.com/?PID=35050&AID=2136424" target="_blank" class="dropdown-item">Compare Car Insurance</a>
                        <a href="https://clk.omgt1.com/?PID=35052&AID=2136424" target="_blank" class="dropdown-item">Compare Van Insurance</a>
                        <a href="https://secure.uk.rspcdn.com/xprr/red/PID/3060" target="_blank" class="dropdown-item">Check your credit score</a>
                        <a href="https://www.motoreasy.com/gap-quote/step/1?utm_medium=affiliate&utm_campaign=organic&utm_source=autoleasecompare" target="_blank" class="dropdown-item">GAP Insurance</a>
                        <a href="/vehicle-repair" class="dropdown-item">Repair your vehicle</a>
                        <a href="/sell-your-car" class="dropdown-item">Sell your vehicle</a>
                    </div>
                </div>
                @guest
                    <button type="button" class="r5 button b2 navbutton" data-toggle="modal" data-target="#login" id="navigationLoginButton">
                        <i class="fa fa-sign-in"></i> Login / Register
                    </button>
                @endguest
                {{-- Right Side Of Navbar --}}
                <ul class="navbar-nav ml-2">
                    {{-- Authentication Links --}}
                    @guest

                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle d-none d-md-none d-lg-block" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                                My Dashboard <span class="caret"></span>
                            </a>


                            <a href="/my-account/" class="dropdown-item d-md-block d-lg-none">
                                My Dashboard
                            </a>
                            <a class="dropdown-item d-md-block d-lg-none {{ Request::is('profile/'.Auth::user()->name, 'profile/'.Auth::user()->name . '/edit') ? 'active' : null }}" href="{{ url('/profile/'.Auth::user()->name) }}">
                                {!! trans('My Profile') !!}
                            </a>
                            <a href="/my-account/enquiries/" class="dropdown-item  d-md-block d-lg-none">
                                My Enquiries
                            </a>

                            @role('dealer|admin')
                            <a href="/my-account/export-quotes/" class="dropdown-item  d-md-block d-lg-none">
                                Export Enquiries
                            </a>
                            <a href="/my-account/my-pricing/" class="dropdown-item  d-md-block d-lg-none">
                                My Pricing
                            </a>
                            @endrole
                            @role('admin')
                            <a href="/my-account/dealers/" class="dropdown-item  d-md-block d-lg-none">
                                Dealers
                            </a>
                            <a href="/activity-log/" class="dropdown-item  d-md-block d-lg-none">
                                Activity Log
                            </a>
                            @endrole
                            <a class="dropdown-item d-md-block d-lg-none" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>


                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a href="/my-account/" class="dropdown-item d-none d-md-none d-lg-block">
                                    My Dashboard
                                </a>
                                <a class="dropdown-item {{ Request::is('profile/'.Auth::user()->name, 'profile/'.Auth::user()->name . '/edit') ? 'active' : null }}" href="{{ url('/profile/'.Auth::user()->name) }}">
                                    {!! trans('My Profile') !!}
                                </a>
                                <a href="/my-account/enquiries/" class="dropdown-item d-none d-md-none d-lg-block">
                                    My Enquiries
                                </a>
                                <a href="/my-account/garage/" class="dropdown-item d-none d-md-none d-lg-block">
                                    My Garage
                                </a>

                                @role('dealer|admin')
                                <a href="/my-account/export-quotes/" class="dropdown-item d-none d-md-none d-lg-block">
                                    Export Enquiries
                                </a>
                                <a href="/my-account/my-pricing/" class="dropdown-item d-none d-md-none d-lg-block">
                                    My Pricing
                                </a>
                                @endrole
                                @role('admin')
                                <a href="/my-account/dealers/" class="dropdown-item d-none d-md-none d-lg-block">
                                    Dealers
                                </a>
                                <a href="/activity-log/" class="dropdown-item d-none d-md-none d-lg-block">
                                    Activity Log
                                </a>
                                @endrole

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item d-none d-md-none d-lg-block" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
                    @role('admin')

                    <div class="dropdown d-none d-md-none d-lg-block">
                        <a class="mr-3 pointer" id="dropdownMenu4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-cog"></i> Admin
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu4">
                            <a class="dropdown-item {{ Request::is('users', 'users/' . Auth::user()->id, 'users/' . Auth::user()->id . '/edit') ? 'active' : null }}" href="{{ url('/users') }}">
                                        User Admin
                                    </a>
                                <a class="dropdown-item {{ Request::is('users/create') ? 'active' : null }}" href="{{ url('/users/create') }}">
                                        New User
                                    </a>

                                    <a class="dropdown-item {{ Request::is('logs') ? 'active' : null }}" href="{{ url('/logs') }}">
                                        {!! trans('titles.adminLogs') !!}
                                    </a>

                                    <a class="dropdown-item {{ Request::is('activity') ? 'active' : null }}" href="{{ url('/activity') }}">
                                        {!! trans('titles.adminActivity') !!}
                                    </a>

                                    <a class="dropdown-item {{ Request::is('phpinfo') ? 'active' : null }}" href="{{ url('/phpinfo') }}">
                                        Server Info
                                    </a>

                                    <a class="dropdown-item {{ Request::is('active-users') ? 'active' : null }}" href="{{ url('/active-users') }}">
                                        {!! trans('titles.activeUsers') !!}
                                    </a>

                                    <a class="dropdown-item {{ Request::is('blocker') ? 'active' : null }}" href="{{ route('laravelblocker::blocker.index') }}">
                                        {!! trans('titles.laravelBlocker') !!}
                                    </a>

                                    <a class="dropdown-item {{ Request::is('blog-admin') ? 'active' : null }}" href="/blog-admin">
                                        Blog Admin
                                    </a>
                        </div>
                    </div>
                    @endrole

            </div>
        </div>

    </nav>
</div>

<div id="mobileMenu" style="display: none;">
    @include('partials.mobilenav')
</div>

</div>