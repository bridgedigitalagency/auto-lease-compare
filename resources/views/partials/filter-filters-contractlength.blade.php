@php
    if(is_array(app('request')->input('term'))) {
        $reqArr = app('request')->input('term');
    }else{
        $reqArr = array();
    }
    $showasterix = false;
@endphp
@if(isset($terms))
    @foreach($terms as $term)
        @php
            if($term->term == '12') {
                $showasterix = true;
            }
        @endphp
        <div class="col-md-3 text-center p-1">
            <label>
                <input class="d-none" type="checkbox" name="term[]" id="{{$term->term}}Checkbox"
                       @if(in_array($term->term, $reqArr)) checked @endif value="{{$term->term}}">
                <div class="textlabel">{{$term->term}} Months @if($term->term == "12") * @endif</div>
            </label>
        </div>
    @endforeach

    @if($showasterix)
        <p><small>* There may be a limited number of deals available for this option.</small></p>
    @endif
@else
    <div class="col-md-3 text-center p-1">
        <label>
            <input class="d-none" type="checkbox" name="term[]" id="12Checkbox"
                   @if(in_array('12', $reqArr)) checked @endif value="12">
            <div class="textlabel">12 Months*</div>
        </label>
    </div>
    <div class="col-md-3 text-center p-1">
        <label>
            <input class="d-none" type="checkbox" name="term[]" id="24Checkbox"
                   @if(in_array('24', $reqArr)) checked @endif value="24">
            <div class="textlabel">24 Months</div>
        </label>
    </div>
    <div class="col-md-3 text-center p-1">
        <label>
            <input class="d-none" type="checkbox" name="term[]" id="36Checkbox"
                   @if(in_array('36', $reqArr)) checked @endif value="36">
            <div class="textlabel">36 Months</div>
        </label>
    </div>
    <div class="col-md-3 text-center p-1">
        <label>
            <input class="d-none" type="checkbox" name="term[]" id="48Checkbox"
                   @if(in_array('48', $reqArr)) checked @endif value="48">
            <div class="textlabel">48 Months</div>
        </label>
    </div>
    <p>* There may be a limited number of deals available for this option.</p>
@endif