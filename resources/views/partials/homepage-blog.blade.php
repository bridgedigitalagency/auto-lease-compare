@if($blogposts)
    @foreach($blogposts as $blog)

        @php
            $blog['title']['rendered'] = str_replace('&#8211;', '-', $blog['title']['rendered']);
            $blog['title']['rendered'] = str_replace('&#8217;', '\'', $blog['title']['rendered']);


        @endphp

        <div class="col-md-12" style="margin-bottom:20px;">
            <a href="{{$blog['link']}}" title="{{$blog['title']['rendered']}}">
                <div class="blog-info-box">
                    @php
                        $doc = new DOMDocument();
                        @$doc->loadHTML($blog['content']['rendered']);

                        $tags = $doc->getElementsByTagName('img');

                        foreach ($tags as $tag) {
                            $src = $tag->getAttribute('src');
                               continue;
                        }
                    @endphp

                    <div class="blogimg">
                        @if($src!='')
                            <img src="{{$src}}" class="img-responsive" alt="{{$blog['title']['rendered']}}">
                        @endif
                    </div>
                    <div class="blogtitle">
                        <p>
                            <strong>{{ str_replace('', '', $blog['title']['rendered'])}}</strong><br>
                        </p>
                    </div>
                </div>
            </a>
        </div>
    @endforeach
@endif