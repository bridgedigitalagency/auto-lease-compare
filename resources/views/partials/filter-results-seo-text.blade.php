@if($database == 'LCV')

    @if($finance_type == "B")
        <p>Business van leasing is perfect for limited companies, sole traders and partnerships, as well as smaller businesses and startups. With Auto Lease Compare, you can browse thousands of business van lease deals from the UK’s most-trusted providers in just a couple of clicks. And with a wide range of commercial van leasing options available from leading names like Ford, Citroen and Vauxhall - and with prices updated daily -  it’s never been easier to secure the best deal!</p>
    @else
        <p>Browse thousands of personal van lease deals from trusted UK providers with Auto Lease Compare and drive away in a brand new van at a fraction of the cost. With hundreds of personal lease vans available from respected manufacturers like Ford, Vauxhall and Renault, it’s never been easier to secure your dream deal.</p>
    @endif
@else

    @if ($finance_type == "H")
        <p>Hybrid and electric vehicles are the big trend in the market right now. Lower carbon emissions, lower taxes, low fuel costs and supreme efficiency are some of the factors that make hybrid and electric car leasing so popular.</p>
        <p>Many drivers are choosing to lease and not purchase outright, thus not having the burden of owning a vastly depreciating asset requiring expensive battery replacements, with the future value of the vehicle undetermined. We have a wide range of hybrid and electric car leasing deals from the UK’s top providers. <strong>Browse our collection of electric and hybrid car deals here.</strong></p>
    @elseif ($finance_type == "B")

        <h2>What is business leasing?</h2>
        <p>Our <strong>business car leasing deals</strong> are perfect for limited companies, sole traders and partnerships. With our <strong>business leasing deals</strong>, you and your employees can drive the latest cars for affordable prices, claim back as much as 50% in VAT and also avoid depreciation in the value of your cars. Businesses also like the benefit of the vehicle not appearing on the 'Balance Sheet' as it is owned by the leasing company - this means that the liability of the finance is not shown on the company accounts. Other benefits of choosing a <strong>business car lease</strong> include tax reclaims in some cases, while your staff will also benefit from the latest safety features and free delivery straight to their home or workplace. <strong>Search and compare business lease deals from the UK’s top leasing companies here.</strong></p>
        <div class="expandedContent">
            <h2> Why Choose Auto Lease Compare For Business Car Leasing Comparisons?</h2>
            <p>Through our innovative, free-to-use tool, we’ve completely revolutionised the <strong>business car leasing comparison</strong> process by partnering with hundreds of providers across the UK, so that you can browse thousands of <strong>business car lease deals</strong> all in one place in just a few seconds.</p>
            <h2>Fully-Flexible Business Car Leasing</h2>
            <p>Leasing isn’t a one size fits all process, so we’ve gone beyond other <strong>business car lease comparison</strong> sites and given you complete control over everything from manufacturer, model and mileage to engine size, body style, budget, and much more, so that you can build and browse the <strong>business car lease deals</strong> that work for you.</p>
            <h2>Trusted Business Car Leasing</h2>
            <p>Protecting your business is important to us and  we’re committed to providing reliable, trusted <strong>car leasing for businesses</strong>. Just like all of the lease partners featured on our site - we’re completely regulated by the Financial Conduct Authority (FCA.) Each of our partners is a member of the British Vehicle Rental and Leasing Association (BVRLA) and has been carefully hand-picked based on exceptional customer service levels.</p>
            <h3> What Manufacturers Are Available For Business Car Leasing?</h3>
            <p>More or less any that you can think of! From popular options like Audi, Mercedes and BMW to Peugeot, Renault and Volkswagen, with Auto Lease Compare you can browse hundreds of business leasing cars and models faster than you ever thought possible. Whatever your budget and requirements, you’ll find the perfect fit with our easy-to-use business car lease comparison tool.</p>
        </div>
        <a id="readMoreListingContent">Read More</a>



    @elseif ($finance_type == "A")
        <p>We at Auto Lease Compare understand how stressful getting a car is while having a low credit score. We also believe that a low credit score should not restrict you from driving the car you love. That is why compare adverse credit leasing deals from the UK’s top leasing companies – all in one place. We have a specially selected range of perfect cars you can drive despite having a poor credit score. Our suppliers for bad credit leasing also do bespoke deals, so if you don’t find a car or a budget that suits your needs – send them an enquiry today with details of what it is you are looking for and they will be able to create a bespoke deal just for you. <strong>Search and compare adverse credit lease deals to find your dream car today.</strong></p>
    @elseif ($finance_type == 'PERFORMANCE')
        <p>Luxurious, powerful performance cars at best prices on the market. At Auto Lease Compare, we have some of the most impressive performance lease deals from the UK’s top leasing providers. <strong>Browse and compare our performance car deals today.</strong></p>
    @else
        <h2>What is personal leasing?</h2>
        <p>Personal car leasing, also commonly known as Personal Contract Hire (PCH) or Private Car Leasing,  is a long term rental agreement, so you won’t be purchasing the car, you simply return it at the end of the lease term. It is the most affordable, convenient and hassle-free way to drive a brand new car. Choose which car you want, how long you want it for (2, 3 or 4 years), what payment you can afford upfront (normally 1, 3, 6, 9 or 12 months initial payment) and how many miles you think you will drive. Then find fixed monthly payments that suit your budget. At Auto Lease Compare, we bring you the best deals on the market from the UK’s top leasing providers.</p>
        <p><strong>Search and compare personal lease deals to find your dream car today.</strong></p>
        <div class="expandedContent">
            <h2> Benefits of Personal Car Leasing</h2>

            <p><strong>Personal car leasing</strong> offers a vast number of benefits in comparison to buying a vehicle outright.</p>

            <ul>
                <li>Enjoy a brand new car at a fraction of the cost</li>
                <li>Free delivery straight to your door; long-gone are the days of hunting round showrooms and vehicle transportation fees!</li>
                <li>No MOT due for the first three years</li>
                <li>Manufacturer’s warranty included; benefit from significantly reduced repair costs</li>
            </ul>

            <h2> Why Choose Auto Lease Compare For Personal Car Leasing Comparison?</h2>

            <p>At Auto Lease Compare, we understand that your time is precious and committed to saving our customers time, money and energy by completely simplifying the <strong>personal car lease comparison</strong> process. Our service is completely free to use too, so it’s a win win!</p>

            <h2> Browse Thousands of Personal Lease Deals</h2>

            <p>Our handy lease comparison tool is super simple to use and allows you to browse the best <strong>personal car lease deals</strong> available anywhere on the market in just a few seconds. Prices are regularly updated to make sure that you’re always getting the best deal available.</p>

            <p>We give you complete control over everything so that you can perform a tailored <strong>personal lease comparison</strong> like never before. From mileage and budget to body style, engine size and even the number of doors, our innovative tool allows you to completely customise your <strong>personal lease</strong> then shows you the best offers available.</p>

            <h3> Trusted Personal Leasing That’s Beyond Compare!</h3>

            <p>Money doesn’t grow on trees, so it's really important to us that you're protected throughout the leasing process. Every partner featured on our site has been carefully hand-picked and is a member of the British Vehicle Rental and Leasing Association (BVRLA). Just like all of our partners, we’re also regulated by the Financial Conduct Authority (FCA), so you can rest assured in the knowledge that you’ll only find trusted <strong>personal car lease deals</strong> on our site.</p>
        </div>
        <a id="readMoreListingContent">Read More</a>
    @endif

@endif
