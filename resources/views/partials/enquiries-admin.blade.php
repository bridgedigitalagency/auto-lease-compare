<div class="col-md-12 product-listing-m mb-3" data-id="{{$quote->id}}">
    <div class="row">
        <div class="col-md-12 mt-2">
            @if($quote->status==1 )
                <span class="statustab r1">Quote Sent</span>
            @elseif($quote->status==2)
                <span class="statustab r2">Mark Sold</span>
            @elseif($quote->status==3)
                <span class="statustab r3">Pending Payment</span>
            @elseif($quote->status==4)
                <span class="statustab r4"> Paid</span>
            @elseif($quote->status==5)
                <span class="statustab r5"> Closed</span>
            @elseif($quote->status==6)
                <span class="statustab r6"> Open</span>
            @elseif($quote->status==7)
                <span class="statustab r5"> Widthdrawn</span>
            @endif
            @if($quote->markassold==1 )
                <span class="statustab r7" >Customer Flagged</span>
            @endif
            @if(isset($quote->make))
                <h5 style="display: inline;">{{$quote->make}} {{$quote->model}}</h5>
            @endif
        </div>
        <div class="col-md-5">
            @if(isset($quote->name))
                <strong>Name: </strong> {{$quote->name}}
            @endif
            <br/>
            @if(isset($quote->email))
                <strong>Email: </strong>{{$quote->email}}
            @endif
            <br/>
            @if(isset($quote->phone_number))
                <strong>Phone: </strong>{{$quote->phone_number}}
            @endif
            <br/>
            @if(isset($quote->contact_preference))
                <strong>Contact Preference: </strong>
                    @php
                        switch ($quote->contact_preference) {
                            case 1:
                                echo "Call me back";
                                break;
                            case 2:
                                echo "Call me back (Morning)";
                                break;
                            case 3:
                                echo "Call me back (Afternoon)";
                                break;
                            case 4:
                                echo "Email me back";
                                break;
                            case 5:
                                echo "No Preference";
                                break;
                        }
                    @endphp
            @endif
            <br/>
            @if(isset($quote->model_id))
                <strong>Deal Id: </strong>{{$quote->model_id}}
            @endif
            <br/>
            @if(isset($quote->id))
                <strong>Enquiry Id: </strong>ALC{{$quote->id}}
            @endif
            <br/>
            @if(isset($quote->deal_id))
                <strong>Dealer: </strong>{{$quote->deal_id}}
            @endif
            <br/>
            @if(isset($quote->finance_type))
                <strong>Lease Type: </strong>@if($quote->finance_type == 'P') Personal @elseif($quote->finance_type == 'B') Business @endif
            @endif
            @if(isset($quote->finance_type) && $quote->finance_type == 'B')
                @if(isset($quote->business_name))
                    <br>
                    <strong>Business Name: </strong>{{$quote->business_name}}
                @endif
            @endif
        </div>
        <div class="col-md-4">
            @if(isset($quote->price))
                <strong>Price: </strong>£{{$quote->price}}
            @endif
            <br/>
            @if(isset($quote->document_fee))
                <strong>Document Fee: </strong>£{{$quote->document_fee}}
            @endif
            <br/>
            @if(isset($quote->profile))
                <strong>Profile: </strong>{{$quote->profile}}
            @endif
            <br/>
            @if(isset($quote->transmission))
                <strong>Transmission: </strong>{{$quote->transmission}}
            @endif
            <br/>
            @if(isset($quote->fuel_type))
                <strong>Fuel Type: </strong>{{$quote->fuel_type}}
            @endif
            <br/>
            @if(isset($quote->created_at))
                <strong>Created: </strong> <?php echo date('d-m-Y H:i:s', strtotime($quote->created_at));?>
                    <br/>
            @endif
            @if($quote->status == 7)
                    <strong>Widthdrawn Reason: </strong> {{$quote->withdraw_enquiry}}
                <br>
            @endif
            @if(isset($quote->finance_type) && $quote->finance_type == 'B')
                @if(isset($quote->business_type))
                    <strong>Business Type: </strong>{{$quote->business_type}}
                    <br>
                @endif
            @endif

        </div>
        <div class="col-md-3">
        @if($quote->status != 7)
            <!-- Button Enquiry modal -->
            <button type="button" class="btn btn-primary mb-2 r1 w-100" data-toggle="modal" data-target="#ModalMessage{{$quote->id}}">
                View Sent Message
            </button>
            <!-- Modal -->
            <div class="modal fade" id="ModalMessage{{$quote->id}}" tabindex="-1" role="dialog" aria-labelledby="ModalMessage{{$quote->id}}" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Customer Message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            @if(isset($quote->enquiry))
                                {{$quote->enquiry}}
                            @endif
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
                <button type="button" class="btn btn-primary mb-2 r4 w-100" data-toggle="modal" data-target="#UpdateStatus{{$quote->id}}">
                    Update Status
                </button>
                <!-- Modal -->
                <div class="modal fade" id="UpdateStatus{{$quote->id}}" tabindex="-1" role="dialog" aria-labelledby="UpdateStatus{{$quote->id}}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Update Status</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Quote status will automatically change when you update </p>
                                <div class="form-group" data-value="{{$quote->id}}">
                                    <select class="form-control update-status" name="status_choice" id="status_choice-{{$quote->id}}" data-value="{{$quote->id}}">
                                        <option value="6" @if($quote->status =='6') selected="selected" @endif>Open</option>
                                        <option value="1" @if($quote->status=='1') selected="selected" @endif>Quote Sent</option>
                                        <option value="2" @if($quote->status=='2') selected="selected" @endif>Mark Sold</option>
                                        <option value="3" @if($quote->status=='3') selected="selected" @endif>Pending Payment</option>
                                        <option value="4" @if($quote->status=='4') selected="selected" @endif>Paid</option>
                                        <option value="5" @if($quote->status=='5') selected="selected" @endif>Closed</option>
                                    </select>
                                    <div class="success"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            <button type="button" class="btn btn-primary mb-2 r5 w-100" data-toggle="modal" data-target="#WithdrawEnquiry{{$quote->id}}">
                Withdraw Enquiry
            </button>
            <!-- Modal -->
            <div class="modal fade" id="WithdrawEnquiry{{$quote->id}}" tabindex="-1" role="dialog" aria-labelledby="WithdrawEnquiry{{$quote->id}}" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Are you sure you would like to withdraw your enquiry?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" class="update-withdraw-form" data-value="{{$quote->id}}">
                                <p>Please give a reason for withdrawal.</p>
                                <input type="hidden" name="withdraw_id" value="{{$quote->id}}">
                                <textarea class="form-control update_withdraw" name="update-withdraw" id="update-withdraw-{{$quote->id}}"  data-value="{{$quote->id}}">@if(isset($quote->withdraw_enquiry)){{$quote->withdraw_enquiry}}@endif</textarea>
                                <input type="submit" value="Submit" name="submit" class="submit button r2" />
                            </form>
                            <div class="success"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
                <button type="button" class="btn btn-primary mb-2 r6 w-100" data-toggle="modal" data-target="#liveChatAssitance{{$quote->id}}">
                    Mark as live chat assisted
                </button>
                <div class="modal fade" id="liveChatAssitance{{$quote->id}}" tabindex="-1" role="dialog" aria-labelledby="liveChatAssitance{{$quote->id}}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Live chat assisted</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" class="update-livechat-form" data-value="{{$quote->id}}">
                                    <p>Mark this deal as completed or assisted by the live chat service.</p>
                                    <input type="hidden" name="dealid" value="{{$quote->id}}">
                                    <input type="submit" value="Submit" name="submit" class="submit button r2" />
                                </form>
                                <div class="success"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Dealer information -->
                <button type="button" class="btn btn-dark mb-2 w-100" data-toggle="modal" data-target="#ModalMessage{{str_replace(' ', '', $quote->dealer_name)}}">
                    Dealer Information
                </button>
                <!-- Modal -->
                <div class="modal fade" id="ModalMessage{{str_replace(' ', '', $quote->dealer_name)}}" tabindex="-1" role="dialog" aria-labelledby="ModalMessage{{str_replace(' ', '', $quote->dealer_name)}}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Dealer Information</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col">
                                                Name:
                                            </div>
                                            <div class="col">
                                                {{$quote->dealer_name}}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                Phone:
                                            </div>
                                            <div class="col">
                                                {{$quote->dealer_phone}}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                Opening Hours:
                                            </div>
                                            <div class="col">
                                                {{$quote->dealer_opening_hours}}
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

            @endif
        </div>
    </div>
</div>