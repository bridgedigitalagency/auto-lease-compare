<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>

@if($database == 'LCV')
    @if (\Request::is('business-vans'))
        <h2>Business Van Leasing with Auto Lease Compare, bringing you thousands of the best deals daily</h2>
    @else
        <h2>Browse through thousands of Personal Van Lease deals in just a few clicks</h2>
    @endif

@else

    @if (\Request::is('hybrid'))<h2>Search and compare reliable Hybrid & Electric car leasing deals from the UK's leading providers</h2>
    @elseif (\Request::is('business'))<h2>The UK’s #1 Car Leasing Comparison Site</h2>
    @elseif (\Request::is('performance'))<h2>Browse the UK's hottest performance lease deals from trusted leasing providers</h2>
    @else <h2>The UK’s #1 Car Leasing Comparison Site</h2>@endif

@endif
<div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b6a8b0d04a076446a9ad" data-businessunit-id="5c0d3879e18cab000171b6fe" data-style-height="24px" data-style-width="100%" data-theme="dark">
    <a href="https://uk.trustpilot.com/review/www.autoleasecompare.com" target="_blank" rel="noopener">Trustpilot</a>
</div>