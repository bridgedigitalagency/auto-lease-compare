@if(isset($vehicleRating->performance))
    <div class="col-md-6">
        <h5>Performance</h5>
        <div class="pull-left" style="width: 10%; line-height:1;">
            <div style="height:9px; margin:5px 0;">{{ $vehicleRating->performance }} <span class="glyphicon glyphicon-star"></span></div>
        </div>
        <div class="pull-left" style="width: 75%;">
            <div class="progress" style="height:9px; margin:8px 0;">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $vehicleRating->performance }}" aria-valuemin="0" aria-valuemax="10" style="width: {{ $vehicleRating->performance }}0%">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
        <div class="pull-right" style="margin-left:10px;width: 10%;">10</div>
    </div>
@endif
@if(isset($vehicleRating->handling))
    <div class="col-md-6">
        <h5>Handling</h5>
        <div class="pull-left" style="width: 10%; line-height:1;">
            <div style="height:9px; margin:5px 0;">{{ $vehicleRating->handling }} <span class="glyphicon glyphicon-star"></span></div>
        </div>
        <div class="pull-left" style="width: 75%;">
            <div class="progress" style="height:9px; margin:8px 0;">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $vehicleRating->handling }}" aria-valuemin="0" aria-valuemax="10" style="width: {{ $vehicleRating->handling }}0%">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
        <div class="pull-right" style="margin-left:10px;width: 10%;">10</div>
    </div>
@endif
@if(isset($vehicleRating->comfort))
    <div class="col-md-6">
        <h5>Comfort</h5>
        <div class="pull-left" style="width: 10%; line-height:1;">
            <div style="height:9px; margin:5px 0;">{{ $vehicleRating->comfort }} <span class="glyphicon glyphicon-star"></span></div>
        </div>
        <div class="pull-left" style="width: 75%;">
            <div class="progress" style="height:9px; margin:8px 0;">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $vehicleRating->comfort }}" aria-valuemin="0" aria-valuemax="10" style="width: {{ $vehicleRating->comfort }}0%">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
        <div class="pull-right" style="margin-left:10px;width: 10%;">10</div>
    </div>
@endif
@if(isset($vehicleRating->space))
    <div class="col-md-6">
        <h5>Space</h5>
        <div class="pull-left" style="width: 10%; line-height:1;">
            <div style="height:9px; margin:5px 0;">{{ $vehicleRating->space }} <span class="glyphicon glyphicon-star"></span></div>
        </div>
        <div class="pull-left" style="width: 75%;">
            <div class="progress" style="height:9px; margin:8px 0;">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $vehicleRating->space }}" aria-valuemin="0" aria-valuemax="10" style="width: {{ $vehicleRating->space }}0%">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
        <div class="pull-right" style="margin-left:10px;width: 10%;">10</div>
    </div>
@endif
@if(isset($vehicleRating->styling))
    <div class="col-md-6">
        <h5>Style</h5>
        <div class="pull-left" style="width: 10%; line-height:1;">
            <div style="height:9px; margin:5px 0;">{{ $vehicleRating->styling }} <span class="glyphicon glyphicon-star"></span></div>
        </div>
        <div class="pull-left" style="width: 75%;">
            <div class="progress" style="height:9px; margin:8px 0;">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $vehicleRating->styling }}" aria-valuemin="0" aria-valuemax="10" style="width: {{ $vehicleRating->styling }}0%">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
        <div class="pull-right" style="margin-left:10px;width: 10%;">10</div>
    </div>
@endif
@if(isset($vehicleRating->build))
    <div class="col-md-6">
        <h5>Build</h5>
        <div class="pull-left" style="width: 10%; line-height:1;">
            <div style="height:9px; margin:5px 0;">{{ $vehicleRating->build }} <span class="glyphicon glyphicon-star"></span></div>
        </div>
        <div class="pull-left" style="width: 75%;">
            <div class="progress" style="height:9px; margin:8px 0;">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $vehicleRating->build }}" aria-valuemin="0" aria-valuemax="10" style="width: {{ $vehicleRating->build }}0%">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
        <div class="pull-right" style="margin-left:10px;width: 10%;">10</div>
    </div>
@endif
@if(isset($vehicleRating->value))
    <div class="col-md-6">
        <h5>Value</h5>
        <div class="pull-left" style="width: 10%; line-height:1;">
            <div style="height:9px; margin:5px 0;">{{ $vehicleRating->value }} <span class="glyphicon glyphicon-star"></span></div>
        </div>
        <div class="pull-left" style="width: 75%;">
            <div class="progress" style="height:9px; margin:8px 0;">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $vehicleRating->value }}" aria-valuemin="0" aria-valuemax="10" style="width: {{ $vehicleRating->value }}0%">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
        <div class="pull-right" style="margin-left:10px;width: 10%;">10</div>
    </div>
@endif
@if(isset($vehicleRating->equipment))
    <div class="col-md-6">
        <h5>Equipment</h5>
        <div class="pull-left" style="width: 10%; line-height:1;">
            <div style="height:9px; margin:5px 0;">{{ $vehicleRating->equipment }} <span class="glyphicon glyphicon-star"></span></div>
        </div>
        <div class="pull-left" style="width: 75%;">
            <div class="progress" style="height:9px; margin:8px 0;">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $vehicleRating->equipment }}" aria-valuemin="0" aria-valuemax="10" style="width: {{ $vehicleRating->equipment }}0%">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
        <div class="pull-right" style="margin-left:10px;width: 10%;">10</div>
    </div>
@endif
@if(isset($vehicleRating->economy))
    <div class="col-md-6">
        <h5>Economy</h5>
        <div class="pull-left" style="width: 10%; line-height:1;">
            <div style="height:9px; margin:5px 0;">{{ $vehicleRating->economy }} <span class="glyphicon glyphicon-star"></span></div>
        </div>
        <div class="pull-left" style="width: 75%;">
            <div class="progress" style="height:9px; margin:8px 0;">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $vehicleRating->economy }}" aria-valuemin="0" aria-valuemax="10" style="width: {{ $vehicleRating->economy }}0%">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
        <div class="pull-right" style="margin-left:10px;width: 10%;">10</div>
    </div>
@endif
@if(isset($vehicleRating->depreciation))
    <div class="col-md-6">
        <h5>Depreciation</h5>
        <div class="pull-left" style="width: 10%; line-height:1;">
            <div style="height:9px; margin:5px 0;">{{ $vehicleRating->depreciation }} <span class="glyphicon glyphicon-star"></span></div>
        </div>
        <div class="pull-left" style="width: 75%;">
            <div class="progress" style="height:9px; margin:8px 0;">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $vehicleRating->depreciation }}" aria-valuemin="0" aria-valuemax="10" style="width: {{ $vehicleRating->depreciation }}0%">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
        <div class="pull-right" style="margin-left:10px;width: 10%;">10</div>
    </div>
@endif
@if(isset($vehicleRating->insurance))
    <div class="col-md-6">
        <h5>Insurance</h5>
        <div class="pull-left" style="width: 10%; line-height:1;">
            <div style="height:9px; margin:5px 0;">{{ $vehicleRating->insurance }} <span class="glyphicon glyphicon-star"></span></div>
        </div>
        <div class="pull-left" style="width: 75%;">
            <div class="progress" style="height:9px; margin:8px 0;">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $vehicleRating->insurance }}" aria-valuemin="0" aria-valuemax="10" style="width: {{ $vehicleRating->insurance }}0%">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
        <div class="pull-right" style="margin-left:10px;width: 10%;">10</div>
    </div>
@endif
@if(isset($vehicleRating->total))
    <div class="col-md-6">
        <h5>Total</h5>
        <div class="pull-left" style="width: 10%; line-height:1;">
            <div style="height:9px; margin:5px 0;">{{ $vehicleRating->total }} <span class="glyphicon glyphicon-star"></span></div>
        </div>
        <div class="pull-left" style="width: 75%;">
            <div class="progress" style="height:9px; margin:8px 0;">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $vehicleRating->total }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $vehicleRating->total }}%">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
        <div class="pull-right" style="margin-left:10px;width: 10%;">100</div>
    </div>
@endif