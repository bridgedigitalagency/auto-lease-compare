@extends('layouts.app')
@section('content')
    <div class="container-fluid banner">
      <div class="row">
        <div class="col-md-12">
          <h1>Personal Car Leasing</h1>
        </div>
      </div>
    </div>

    <div class="container">
        <div class="row">

                <div class="col-sm-3">
                    <form method="GET" id="filterList">
                        <label for="from">Lease Criteria</label>

                            <select id="maker" class="form-control" name="maker">
                                <option value="">Select Make</option>
                                @foreach($summaryMake ?? '' as $make)
                                    <option value="{{$make->maker}}">{{$make->maker}}</option>
                                @endforeach
                            </select><br/>
                            @if isset($summaryMake)
                             <select id="range" class="form-control" name="range">
                                <option value="">Select Model</option>
                                @foreach($summaryMake ?? '' as $range)
                                    <option value="{{$range->range}}">{{$range->range}}</option>
                                @endforeach
                            </select><br/>
                             <select id="trim" class="form-control" name="trim">
                                <option value="">Select Trim</option>
                                @foreach($summaryMake ?? '' as $trim)
                                    <option value="{{$trim->trim}}">{{$trim->trim}}</option>
                                @endforeach
                            </select><br/>
                            <select id="fuel" class="form-control" name="fuel">
                                <option value="">Select Fuel</option>
                                @foreach($summaryMake ?? '' as $fuel)
                                    <option value="{{$make->fuel}}">{{$make->fuel}}</option>
                                @endforeach
                            </select><br/>
                             <select id="transmission" class="form-control" name="transmission">
                                <option value="">Select Transmission</option>
                                @foreach($summaryMake ?? '' as $transmission)
                                    <option value="{{$range->transmission}}">{{$range->transmission}}</option>
                                @endforeach
                            </select><br/>
                             <select id="bodystyle" class="form-control" name="bodystyle">
                                <option value="">Select Bodytype</option>
                                @foreach($summaryMake ?? '' as $bodytype)
                                    <option value="{{$bodytype->bodystyle}}">{{$bodytype->bodystyle}}</option>
                                @endforeach
                            </select><br/>

                            <select id="deposit_value" class="form-control" name="deposit_value">
                                <option value="">Months Upfront</option>
                                @foreach($summaryDeposit as $sumD)
                                    <option value="{{$sumD}}">{{$sumD}}</option>
                                @endforeach
                            </select>

                              <br/>
                            <select id="term" class="form-control" name="term">
                                <option value="">Contract Length</option>
                                @foreach($summaryTerm as $sumT)
                                    <option value="{{$sumT}}">{{$sumT}}</option>
                                @endforeach
                            </select>
                            <br/>
                            <select id="annual_mileage" class="form-control" name="annual_mileage">
                                <option value="">Mileage</option>
                                @foreach($summaryMileage as $sumM)
                                    <option value="{{$sumM}}">{{$sumM}}</option>
                                @endforeach
                            </select>
                            <br/>
                            <select id="in_stock" class="form-control" name="in_stock">
                                <option value="">Stock</option>
                                @foreach($summaryStock as $sumS)
                                    <option value="{{$sumS}}">{{$sumS}}</option>
                                @endforeach
                            </select>
                             <br/>
                        <button class="btn btn-success" style="margin-top:30px;" type="submit" style="width:100%;"> Apply Filters</button>
                    </form>
                </div>
                <div class="col-md-9">
                    @foreach($summaryCap as $sum)
                        <div style="background: #e8e8e8;margin-bottom: 20px;">

                            <div class="row" style="padding: 20px;">
                            <div class="col-md-3">

                            </div>
                              <div class="col-md-6">
                                @if(isset($sum->maker))
                                    <h3>{{$sum->maker}} {{$sum->range}}<br/><span class="der">{{$sum->derivative}}</span></h3>
                                @endif
                                @if(isset($sum->fuel))
                                    <h6 class="features">{{$sum->fuel}} | {{$sum->transmission}} | {{$sum->bodystyle}} </h6>
                                @endif

                                @if(isset($sum->deposit_value))
                                    <h6>Profile: {{$sum->deposit_value}} + {{$sum->term-1}}</h6>
                                @endif
                                @if(isset($sum->annual_mileage))
                                    <h6>Annual Mileage: {{$sum->annual_mileage}}</h6>
                                @endif
                              </div>
                            <div class="col-md-3">
                                @if(isset($sum->monthly_payment))
                                    <h6>£{{$sum->monthly_payment}} Per Month, Inc VAT</h6>
                                @endif
                                 @if(isset($sum->deposit_months))
                                    <h6>£{{$sum->deposit_months}} Initial Rental</h6>
                                @endif
                                @if(isset($sum->document_fee))
                                    <h6>£{{$sum->document_fee}} Admin Fee</h6>
                                @endif
                            </div>
                           </div>
                          </div>
                    @endforeach
                </div>

        </div>
    </div>


@endsection
<style>
.der {
    font-size: 18px;
    font-weight: 900;
}
.features{
    font-weight:900;
}
.banner{
    text-align: center;
    color: #fff;
    background: #e74c3c;
    margin-bottom: 40px;
    padding-top: 22px;
    margin-top: -1.5rem;
}
</style>