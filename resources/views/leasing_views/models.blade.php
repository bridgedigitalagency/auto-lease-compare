@extends('layouts.app')
<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>

@if($database == 'LCV')
    @section('template_title')@if(isset($make)){{ $make  }}@endif Van Lease Deals @endsection
    @section('template_description')
        @include('partials.meta.description-manusearch-lcv')
    @endsection
@else
    @section('template_title')@if(isset($make)){{ $make  }}@endif Car Lease Deals @endsection
    @section('template_description')
        <meta name="description" content="Discover and compare @if(isset($make)){{ $make  }}@endif car lease deals from the UK's leading car leasing companies all in one place. We've made it easy to find and lease a @if(isset($make)){{ $make  }}@endif car.">
    @endsection
@endif
@section('content')
    @if(isset($counter) && $counter == 0)
        @include('partials.models-404')
    @else
    <div class="container-fluid banner">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5 " style="padding-top: 1rem!important;padding-bottom: 0rem!important">
                @if($database == 'LCV')
                    <h1 style="color:#fff; text-align: center">Compare @if(isset($make)) {{$make}} @endif Van Lease Deals</h1>
                    <h2 style="color:#fff; text-align: center">The UK’s #1 Car Leasing Comparison Site</h2>
                    <div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b6a8b0d04a076446a9ad" data-businessunit-id="5c0d3879e18cab000171b6fe" data-style-height="24px" data-style-width="100%" data-theme="dark">
                        <a href="https://uk.trustpilot.com/review/www.autoleasecompare.com" target="_blank" rel="noopener">Trustpilot</a>
                    </div>
                    <br/>
                    <div style="text-align:center; color:#fff;" class="breadc"><a href="/vans" style="color:#fff;">Van Lease Comparison (Home)</a>@if(isset($make)) / {{ $make }} @endif Lease Deals</div>
                @else
                    <h1 style="color:#fff; text-align: center">Compare @if(isset($make)) {{$make}} @endif Lease Deals</h1>
                    <h2 style="color:#fff; text-align: center">The UK’s #1 Car Leasing Comparison Site</h2>
                    <div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b6a8b0d04a076446a9ad" data-businessunit-id="5c0d3879e18cab000171b6fe" data-style-height="24px" data-style-width="100%" data-theme="dark">
                        <a href="https://uk.trustpilot.com/review/www.autoleasecompare.com" target="_blank" rel="noopener">Trustpilot</a>
                    </div>
                    <br/>


                    <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Car Lease Comparison (Home)</a>@if(isset($make)) / {{ $make }} @endif Lease Deals</div>
                @endif
            </div>
        </div>
    </div>

    <div class="container-fluid whitebg uspsBannerHeader">
        <div class="row">
            <div class="col-12">
                @include('partials.usps_header')
            </div>
        </div>
    </div>
    <div class="container">
        @if($database == 'LCV')
                @include('partials.models-seo-content-lcv')
        @else
            @if($make == 'BMW' || $make == 'Mercedes-Benz' || $make == 'Audi' || $make == 'Ford' || $make == 'Volkswagen' || $make == 'Volvo' || $make == 'Kia' || $make == 'Mini' || $make == 'Tesla' || $make == 'Nissan' || $make == 'Toyota' || $make == 'Hyundai' || $make == 'Skoda' || $make == 'Smart' || $make == 'Seat' || $make == 'Vauxhall' || $make == 'Jaguar' || $make == 'Land Rover' || $make == 'Lexus' || $make == 'Honda' || $make == 'Mazda' || $make == 'Porsche' || $make == 'Peugeot' || $make == 'Citroen' || $make == 'Jeep' || $make == 'Renault' || $make == 'Fiat' || $make == 'Dacia' || $make == 'Alfa Romeo' || $make == 'Mitsubishi' || $make == 'Abarth' || $make == 'Suzuki' || $make == 'Polestar' || $make == 'Subaru' || $make == 'Maserati' || $make == 'Cupra' || strtoupper($make) == 'MG MOTOR UK' || $make == 'Ssangyong')
                @include('partials.models-seo-content')
            @endif
        @endif

        <div class="row">
            @if(isset($results) && is_array($results))

                @foreach($results as $result)
                    @if(isset($result['personal']) || isset($result['personal']))

                        <div class="col-md-3 mb-4 relative-link">

                            <div class="image">
                                <img src="{{ $result['imageid'] }}" class="img-fluid" alt="Image of {{ $result['maker'] }} {{ $result['range'] }}">

                            </div>


                            <div class="relative">
                                <div class="relative-title"><p>{{ $result['maker'] }} {{ $result['range'] }}</p></div>


                                <div class="row">
                                    @if(is_array($result['personal']) && count($result['personal'])>0)
                                        <div class="col-6 text-center">
                                            <div class="relative-price mb-4 text-center">From £{{ $result['personal']['monthly_payment'] }}<small>Inc VAT.</small></div>
                                        </div>
                                        @if(is_array($result['business']) && count($result['business'])>0)
                                            <div class="col-6 text-center">
                                                <div class="relative-price business-price mb-4 text-center">From £{{ $result['business']['monthly_payment'] }}<small>Ex VAT.</small></div>
                                            </div>
                                        @endif
                                    @else
                                        <div class="col-12 text-center">
                                            <div class="relative-price business-price mb-4 text-center">From £{{ $result['business']['monthly_payment'] }}<small>Ex VAT.</small></div>
                                        </div>
                                    @endif
                                </div>

                                <div class="row">
                                    @if(is_array($result['personal']) && count($result['personal'])>0)
                                        <div class="col-6 text-center">
                                            @if($database == 'LCV')
                                                <a href="/personal-vans?maker[]={{ $result['maker'] }}&range[]={{ $result['range'] }}" class="text-white button b3">Personal</a>
                                            @else
                                                <a href="/personal?maker[]={{ $result['maker'] }}&range[]={{ $result['range'] }}" class="text-white button b3">Personal</a>
                                            @endif
                                        </div>
                                        @if(is_array($result['business']) && count($result['business'])>0)
                                            <div class="col-6 text-center">
                                                @if($database == 'LCV')
                                                    <a href="/business-vans?maker[]={{ $result['maker'] }}&range[]={{ $result['range'] }}" class="text-white button b2">Business</a>
                                                @else
                                                    <a href="/business?maker[]={{ $result['maker'] }}&range[]={{ $result['range'] }}" class="text-white button b2">Business</a>
                                                @endif
                                            </div>
                                        @endif
                                    @else
                                        <div class="col-12 text-center">
                                            @if($database == 'LCV')
                                                <a href="/business-vans?maker[]={{ $result['maker'] }}&range[]={{ $result['range'] }}" class="text-white button b2">Business</a>
                                            @else
                                                <a href="/business?maker[]={{ $result['maker'] }}&range[]={{ $result['range'] }}" class="text-white button b2">Business</a>
                                            @endif

                                        </div>
                                    @endif
                                </div>
                                <div class="mb-3"></div>
                            </div>

                        </div>

                    @endif
                @endforeach
            @endif

        </div>


    </div>
    @endif
@endsection
@section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="/css/banner.css">
    <link rel="stylesheet" type="text/css" href="/css/models.css">
    <style>
        .relative-price {

        }
        .business-price {
            color: #3fc1c9!important;
        }
        .relative-price small {
            clear: both;
            color: #000;
            display: block;
        }
        .jumbotronReadMore {
            text-align: center;
            display: block;
            width: 100%;
            clear: both;
            font-size: 1.2rem;
            text-decoration: underline;
        }
        .jumbotron .headerText {
            font-size: 1.8rem;
        }
        .jumbotron h1,.jumbotron h2,.jumbotron h3 {
            font-size: 1.5rem;
        }
        .hiddenContent {
            display: none;
        }

    </style>
@endsection
@push('scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <script>
        $(function () {
            $('.jumbotronReadMore').click(function() {
                $('.hiddenContent').toggle();
                var text = $('.jumbotronReadMore').text();
                $('.jumbotronReadMore').text(
                    text == "Read Less" ? "Read More" : "Read Less");
            });
            $('.uspsHeaderMobile').slick({
                autoplay        : true,
                arrows          : false,
                dots            : false,
                draggable       : true,
                slidesToShow    : 1,
                slidesToScroll  : 1,
                touchMove       : true,
                swipe           : true,
                swipeToSlide    : true,
                infinite        : true,
                prevArrow       : '',
                nextArrow       : '',
                mobileFirst     : true
            });
        });
    </script>


@endpush