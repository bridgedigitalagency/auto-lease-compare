@extends('layouts.app')
@section('content')

    <div class="container-fluid banner">
        <div class="row">
            <div class="col-md-12">
                <h1>{{$pageName}}</h1>

                @if($database == 'LCV')Van @else Car @endif Images are for illustration purposes only, you can request different colours and specifications when you message the leasing company
            </div>
        </div>
    </div>

    <div class="container listingContainer">
        @if($typeSearch)
            @include('partials.search-filter-list-container')
        @else
            <div class="row">
                <div class="col-md-9">
                    <h1>Sorry, no deals available at this point in time.</h1>
                    <p>Please alter you search criteria or use the filters to find your desired vehicle</p>
                </div>
                <div class="d-none d-lg-block col-md-3">
                    <button type="button" data-toggle="modal" data-target="#typesearch" class="button b2" style="float:right;"><i class="fa fa-search"></i> Type Search</button>
                </div>
            </div>
    </div>

    <div class="modal fade" id="typesearch" tabindex="-1" role="dialog" aria-labelledby="typesearch" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Type To Search</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/search" method="get">
                        @if ($finance_type == "H")
                            <input type="hidden" name="finance_type" value="H">
                        @elseif ($finance_type == "B")
                            <input type="hidden" name="finance_type" value="B">
                        @elseif ($finance_type == "A")
                            <input type="hidden" name="finance_type" value="A">
                        @elseif ($finance_type == 'PERFORMANCE')
                            <input type="hidden" name="finance_type" value="PERFORMANCE">
                        @else
                            <input type="hidden" name="finance_type" value="P">
                        @endif

                        <input type="hidden" name="database" value="{{$database}}">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" name="search" class="form-control" placeholder="Search Anything" value="{{$searchedString}}">
                                <br/>
                                <input type="submit" class="button b2">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button b3" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    @endif
    </div>

    <div class="modalLoading">
        <div class="lds-dual-ring"><img src='/images/loadingWheel.png'></div>
    </div>
@endsection
@section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="/css/listing.css">
@endsection


@push('scripts')

    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script>
        $(function () {

            // trigger select2
            $('.rangeSelectMulti').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                maximumSelectionLength: 3,
                placeholder: "Select Model",

            });
            $('#engineSize').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                maximumSelectionLength: 3,
                placeholder: "Select engine size",

            });

            @if(isset(app('request')->input('maker')[0]))
            checkMaker();
            @endif;

            $('#filterListContainer').on(' hidden.bs.modal', function () {
                updateFilterContent();
            });

            // Handler for updating the sort by ordering
            $("body").on('change', '#filterSortBy', function (e) {
                updateFilterContent();
            });

            $('body').on('change', '.finance_type_show', function (e) {
                updateFilterContent();
            });

            // Handler for updating the sort by ordering
            $("body").on('change', '#in_stock', function (e) {
                updateFilterContent();
            });

            // Pagination handler
            $("body").on('click', '.page-link', function (e) {
                e.preventDefault();
                var page = $(this).attr('data-id');

                // Ajax update the page to the page number
                updateFilterContent(page);
            });

            // Garage
            $("body").on('click', '.addToGarageListing', function (e) {
                e.preventDefault();
                // get the car we're adding to the garage (dealid)
                var carId = $(this).attr('data-id');
                var database = $(this).attr('data-database');
                $.ajax(
                    {
                        url: "/my-account/garage/add/" + carId + '/' + database,
                        success: function (result) {
                            // saved
                            if (result == "S") {
                                $("#modelgarage").modal("show");
                                // Need to login
                            } else if (result == "L") {
                                $("#modelGarageLog").modal("show");
                                // Garage is full
                            } else if (result == "C") {
                                window.location.href = "/my-account/garage?c=1";
                            }else if (result == "C1") {
                                window.location.href = "/my-account/garage?c=2";
                            }
                        }
                    }
                );
            });

            if($(window).width() < 800) {
                $("body").on('click', '.makeFilterHeader', function (e) {
                    $('#mobileMakers').toggle();
                });
                $("body").on('click', '.modelFilterHeader', function (e) {
                    $('#mobileModels').toggle();
                });
                $("body").on('click', '.trimFilterHeader', function (e) {
                    $('#mobileTrim').toggle();
                });

                $("body").on('click', '.annualmileageFilterHeader', function (e) {
                    $('#mobileMileage').toggle();
                });
                $("body").on('click', '.contractlengthFilterHeader', function (e) {
                    $('#mobileContract').toggle();
                });
                $("body").on('click', '.initialrentalFilterHeader', function (e) {
                    $('#mobileInitial').toggle();
                });
                $("body").on('click', '.budgetFilterHeader', function (e) {
                    $('#mobileBudget').toggle();
                });
                $("body").on('click', '.doorsFilterHeader', function (e) {
                    $('#mobileDoors').toggle();
                });

                $("body").on('click', '.enginesizeFilterHeader', function (e) {
                    $('#mobileEngine').toggle();
                });

                $("body").on('click', '.bodystyleFilterHeader', function (e) {
                    $('#mobileBodystyle').toggle();
                });
                $("body").on('click', '.transmissionFilterHeader', function (e) {
                    $('#mobileTransmission').toggle();
                });
                $("body").on('click', '.fuelFilterHeader', function (e) {
                    $('#mobileFuel').toggle();
                });



                $('.mobileMaker').select2({
                    allowClear: true,
                    theme: 'bootstrap4',
                    width: "100%",
                    placeholder: "Select Manufacturer",
                });

                $('.mobileRange').select2({
                    allowClear: true,
                    theme: 'bootstrap4',
                    width: "100%",
                    placeholder: "Select Model",
                });
                $('.mobileTrim').select2({
                    allowClear: true,
                    theme: 'bootstrap4',
                    width: "100%",
                    placeholder: "Select Trim",
                });

                $('body').on('change', '.mobileMaker', function () {
                    var data = $('#mobileFilterList').serialize();
                    $.ajax(
                        {
                            type: "get",
                            data: data,
                            url: "/home/manusearch",
                            success: function (html) {

                                $('.mobileRange').html(html);


                            }
                        }
                    )
                });

                $('body').on('change', '.mobileRange', function () {
                    var data = $('#mobileFilterList').serialize();
                    $.ajax(
                        {
                            type: "get",
                            data: data,
                            url: "/personal/trimsearch",
                            success: function (html) {

                                $('.mobileTrim').html(html);


                            }
                        }
                    )
                });

            }
        });

        // Update the results
        function updateFilterContent(page) {

            updateAllSidebarBlocks();

            $('.modalLoading').show();
            var filters = $('#filterList').serialize();
            var stock = $('#stockForm').serialize();
            var typeSearch = $('#typeSearchForm').serialize();
            var sort = $('#sortByForm').serialize();

            var data = filters + "&" + stock + "&" + sort + "&" + typeSearch;
            if (typeof page !== 'undefined') {
                var data = data + "&page=" + page;
            }

            @if($database == 'LCV')

            @if(app('request')->input('finance_type') == 'B')
            var url = "/business-vans/results";
            @else
            var url = "/personal-vans/results";
            @endif

            @else
            var url = "/personal/results";

            @if (\Request::is('hybrid'))
            var url = "/hybrid/results";
            @elseif (\Request::is('business'))
            var url = "/business/results";
            @elseif (\Request::is('performance'))
            var url = "/performance/results";
            @elseif (\Request::is('adverse-credit'))
            var url = "/adverse-credit/results";
            @endif
            @endif
            $.ajax(
                {
                    type: "get",
                    data: data,
                    url: url,
                    success: function (html) {
                        $('#filterContentContainer').html(html)
                    }
                }
            )
        }

        function checkMaker() {
            $('#makeSelected').html("");

            $('#makeModal input:checked').each(function () {

                // Add maker icons to sidebar
                var value = $(this).attr('value');
                var image = value.toLowerCase();
                var img = "<img src='/images/manufacturers/" + image + ".png'>";
                $('#makeSelected').append(img);

                // show each of the range select boxes for the selected makers
                var manu = $(this).attr('value');
                var manuval = manu.replace(' ', '_');
                $('#' + manuval + '-select').show();

            });

            // reset all the content in range modal
            $('#rangeSelectContainer').html('');
            var data = $('#filterList').serialize();
            var filters = $('#filterList').serialize();
            var stock = $('#stockForm').serialize();
            var typeSearch = $('#typeSearchForm').serialize();

            var data = filters + "&" + stock + "&" + sort + "&" + typeSearch
            if (typeof page !== 'undefined') {
                var data = data + "&page=" + page;
            }



            @if($database == 'LCV')

            @if(app('request')->input('finance_type') == 'B')
            var url = "/business-vans/results";
            @else
            var url = "/personal-vans/results";
            @endif

            @else
            var url = "/personal/results";

            @if (\Request::is('hybrid'))
            var url = "/hybrid/filters/range";
            @elseif (\Request::is('business'))
            var url = "/business/filters/range";
            @elseif (\Request::is('performance'))
            var url = "/performance/filters/range";

            @endif
            @endif

            $.ajax(
                {
                    type: "get",
                    data: data,
                    url: url,
                    success: function (html) {

                        $('#rangeSelectContainer').html(html);

                        $('.rangeSelect').select2({
                            allowClear: true,
                            theme: 'bootstrap4',
                            width: "100%",
                            placeholder: "Select Range",
                        });

                        $('.trimSelect').select2({
                            allowClear: true,
                            theme: 'bootstrap4',
                            width: "100%",
                            placeholder: "Select Trim",
                        });
                        if ($('#engineSize').length) {
                            $('#engineSize').select2({
                                allowClear: true,
                                theme: 'bootstrap4',
                                width: "100%",
                                maximumSelectionLength: 3,
                                placeholder: "Select Engine Size",

                            });
                        }

                    }
                }
            )

            // Show trim filters
            $('#modelOptions').show();
        }

        function updateAllSidebarBlocks() {
            $('.modalLoading').show();

            var filters = $('#filterList').serialize();
            var stock = $('#stockForm').serialize();
            var sort = $('#sortByForm').serialize();
            var typeSearch = $('#typeSearchForm').serialize();

            var data = filters + "&" + stock + "&" + sort + "&" + typeSearch;
            if (typeof page !== 'undefined') {
                var data = data + "&page=" + page;
            }

            @if($database == 'LCV')

            @if(app('request')->input('finance_type') == 'B')
            var url = "/business-vans/filters";
            @else
            var url = "/personal-vans/filters";
            @endif

            @else
            var url = "/personal/filters";

            @if (\Request::is('hybrid'))
            var url = "/hybrid/filters";
            @elseif (\Request::is('business'))
            var url = "/business/filters";
            @elseif (\Request::is('performance'))
            var url = "/performance/filters";
            @elseif (\Request::is('adverse-credit'))
            var url = "/adverse-credit/filters";
            @endif
            @endif


            $.ajax(
                {
                    type: "get",
                    data: data,
                    url: url,
                    success: function (html) {
                        if(window.innerWidth <= 800) {


                            $('#mobileFilters').html('');
                            // update trims modal with new content
                            $('#mobileFilters').html(html);

                        } else {

                            $('#filterListContainer').html('');
                            // update trims modal with new content
                            $('#filterListContainer').html(html);
                        }





                        if ($('.rangeSelect').length) {
                            $('.rangeSelect').select2({
                                allowClear: true,
                                theme: 'bootstrap4',
                                width: "100%",
                                maximumSelectionLength: 3,
                                placeholder: "Select Model",

                            });
                        }

                        if ($('.trimSelect').length) {
                            $('.trimSelect').select2({
                                allowClear: true,
                                theme: 'bootstrap4',
                                width: "100%",
                                maximumSelectionLength: 3,
                                placeholder: "Select Model",

                            });
                        }

                        if ($('#doors').length) {
                            $('#doors').select2({
                                allowClear: true,
                                theme: 'bootstrap4',
                                width: "100%",
                                maximumSelectionLength: 3,
                                placeholder: "Select number of doors",

                            });
                        }

                        if ($('#engineSize').length) {
                            $('#engineSize').select2({
                                allowClear: true,
                                theme: 'bootstrap4',
                                width: "100%",
                                maximumSelectionLength: 3,
                                placeholder: "Select engine size",

                            });
                        }

                        $('.modalLoading').hide();

                    }
                }
            )

        }

    </script>
    @role('admin')
    <script>
        $(function () {
            $("body").on('submit', '.update-featured-form', function (e) {
                e.preventDefault();
                var formInfo = $(this).serialize();
                var capId = $(this).find('input[name="cap-id"]').val();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'get',
                    url: '{{route('update-featured')}}',
                    data: formInfo,
                }).done(function(response) {
                    $('div.success-' + capId).html('Featured Updated').delay(9000).fadeOut();
                }).fail(function() {
                });
            });
        });
    </script>
    @endrole
@endpush