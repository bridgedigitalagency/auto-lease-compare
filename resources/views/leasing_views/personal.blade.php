@extends('layouts.app')
@section('content')
    <div class="container-fluid banner">
        <div class="row">
            <div class="col-md-12">
                <h1>Personal Car Leasing</h1>
            </div>
        </div>
    </div>
    <section class="smallnav">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="bread">
                        <a href="/">Home</a> / <a href="/personal">Personal Car Leasing</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('partials.filter-list')
            </div>
            <div class="col-sm-9">
                @include('partials.filter-results')
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<style>
    .der {
        font-size: 18px;
        font-weight: 900;
    }
    .features{
        font-weight:900;
    }
    .banner{
        text-align: center;
        color: #fff;
        background: #fff;
        padding-top: 22px;
        margin-top: -1.5rem;
    }
</style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
@endpush