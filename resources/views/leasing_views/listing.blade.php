@extends('layouts.app')
@section('template_title')
    @if($database == 'LCV')
        @if (\Request::is('business-vans'))
            Business Van Lease Deals
        @else
            Personal Van Lease Deals
        @endif
    @else
        @if (\Request::is('hybrid'))Hybrid Electric Car Lease Deals @elseif (\Request::is('business'))Business Car Leasing Comparison Deals @elseif (\Request::is('performance'))Performance Car Leasing Deals @elseif (\Request::is('adverse-credit'))Compare Adverse Credit Car Lease @else Personal Car Lease Comparison @endif
    @endif
@endsection

@section('template_description')
    @if($database == 'LCV')
        @if (\Request::is('business'))
            <meta name="description" content="Browse the best business van lease deals from trusted UK providers in just a few seconds. Includes leading names like Ford, Citroen & Vauxhall. Search now.">
        @else
            <meta name="description" content="Browse thousands of personal van lease deals with Auto Lease Compare, including Ford, Vauxhall, Renault & more. Search now & secure your dream deal today!">
        @endif
    @else
        @if (\Request::is('hybrid'))
            <meta name="description" content="Discover our latest car lease deals on electric and hybrid cars. Compare thousands of eco-friendly cars available at unbeatable, cheap prices.">
        @elseif (\Request::is('business'))
            <meta name="description" content="Browse to compare our best business car deals for the cheapest price. Choose your dream car today from our collection. View more here.">
        @elseif (\Request::is('performance'))
            <meta name="description" content="Explore our range of brand new performance cars to find your perfect deal. Fee transparency, manufacturer's warranty and free UK delivery. Book now.">
        @elseif (\Request::is('adverse-credit'))
            <meta name="description" content="Don't let a bad credit score hold you back. We offer a range of adverse credit lease car deals, so you don't have to worry about poor score. View our deals here.">
        @else
            <meta name="description" content="Get the best personal car lease deals from Auto Lease Comparison at unbeatable and affordable prices. View our deals online and book now.">
        @endif
    @endif
@endsection

@section('content')

    <div class="container-fluid banner">
        <div class="row justify-content-md-center">
            <div class="col-lg-6 col-md-12">
                <h1>{{$pageName}}</h1>
                @include('partials.listing-header-text')
            </div>
        </div>
        @if($finance_type !='PERFORMANCE')
            <div class="car d-none d-lg-block">
                <div class="carbody"></div>
                <div class="weel weel1"></div>
                <div class="weel weel2"></div>
            </div>
        @endif

    </div>
    <div class="container-fluid whiteBg uspsBannerHeader">
    @include('partials.usps_header')
    </div>
    <div class="container">
        <div class="row mobfilters">
            <div class="col-6 filterbuttons d-block d-md-block d-lg-none" onclick="openNav()" style="padding-top: 17px;">
                <i class="fa fa-filter" aria-hidden="true" style="color: rgb(252, 81, 133);"></i> Filter
            </div>
            <div class="col-6 d-block d-md-block d-lg-none stockFormContainer1">

            </div>


            {{--            <div class="col-6 filterbuttons d-block d-md-block d-lg-none" onclick="openNav2()"><i class="fa fa-sort" aria-hidden="true"></i> Sort By</div>--}}
        </div>
    </div>

    <div class="container listingContainer">
        @include('partials.filter-list-container')
    </div>
    <div class="modalLoading">
        <div class="lds-dual-ring"><img src='/images/loadingWheel.png'></div>
    </div>
    <div class="blackout" id="blackout1"></div>
@endsection
@section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="/css/listing.css">
    @if (\Request::is('hybrid'))
        <style>
            .banner{
                background:#3fc1c9!important;
            }
            .car {
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
                margin: 0 auto;
                position: absolute;
                width: 319px;
                height: 172px;
                overflow: hidden;
                right: 0px;
                top: 72px;
            }

            .carbody {
                animation: carmove 3.1s infinite linear;
                background: url('/images/bmwi8.png') 0 0;
                background-size: cover;
                height: 145px;
                position: relative;
                width: 346px;
                z-index: 125;
            }

            .weel {
                animation: wheel 0.7s infinite linear;
                background: url('/images/wheelbmw.png') 0 0;
                height: 71px;
                position: absolute;
                top: 39%;
                width: 71px;
                z-index: 200;
                background-size: cover;
            }

            .weel1 {
                left: 45px;
            }

            .weel2 {
                left: 441px;
            }
        </style>
    @elseif (\Request::is('business'))
        <style>
            .banner{
                background:#3fc1c9!important;
            }
            .car {
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
                margin: 0 auto;
                position: absolute;
                width: 319px;
                height: 172px;
                overflow: hidden;
                right: 0px;
                top: 72px;
            }

            .carbody {
                animation: carmove 3.1s infinite linear;
                background: url('/images/mercc.jpg') 0 0;
                background-size: cover;
                height: 145px;
                position: relative;
                width: 346px;
                z-index: 125;
            }

            .weel {
                animation: wheel 0.7s infinite linear;
                background: url('/images/mercwheel.png') 0 0;
                height: 64px;
                position: absolute;
                top: 43%;
                width: 64px;
                z-index: 200;
                background-size: cover;
            }

            .weel1 {
                left: 45px;
            }

            .weel2 {
                left: 300px;
            }
        </style>
    @elseif (\Request::is('performance'))
        <style>
            .banner{
                background:#3fc1c9!important;
            }
            .car {
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
                margin: 0 auto;
                position: absolute;
                width: 319px;
                height: 172px;
                overflow: hidden;
                right: 0px;
                top: 72px;
            }

            .carbody {
                animation: carmove 3.1s infinite linear;
                background: url('/images/mclaren.png') 0 0;
                background-size: cover;
                height: 145px;
                position: relative;
                width: 346px;
                z-index: 125;
            }

            .weel {
                animation: wheel 0.7s infinite linear;
                background: url('/images/wheelmclaren.png') 0 0;
                height: 72px;
                position: absolute;
                top: 41%;
                width: 72px;
                z-index: 200;
                background-size: cover;
            }

            .weel1 {
                left: 56px;
            }

            .weel2 {
                left: 441px;
            }
        </style>
    @else
        <style>
            .car {
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
                margin: 0 auto;
                position: absolute;
                width: 319px;
                height: 172px;
                overflow: hidden;
                right: 0px;
                top: 72px;
            }
            .banner{
                background: #3fc1c9 !important
            }
            .carbody {
                animation: carmove 3.1s infinite linear;
                @if($database == 'LCV')
background: url('/images/van-homepage.png') 0 0;
                @else
background: url('/images/golf.png') 0 0;
                @endif
background-size: cover;
                height: 145px;
                position: relative;
                width: 346px;
                z-index: 125;
            }

            @if($database == 'LCV')
            .weel {
                animation: wheel 0.7s infinite linear;
                background: url(/images/wheel_van.png) 0 0;
                height: 55px;
                position: absolute;
                top: 54%;
                width: 55px;
                z-index: 200;
                background-size: cover;
            }

            .weel1 {
                left: 70px;
            }

            .weel2 {
                left: 295px;
            }
            @else
            .weel {
                animation: wheel 0.7s infinite linear;
                background: url(/images/wheel.png) 0 0;
                height: 65px;
                position: absolute;
                top: 46%;
                width: 65px;
                z-index: 200;
                background-size: cover;
            }

            .weel1 {
                left: 46px;
            }

            .weel2 {
                left: 295px;
            }
            @endif
        </style>
    @endif
    <style>
        .insurance {
            color: #3fc1c9;
            padding-left: 20px;
            font-weight: bold;
            font-size: 0.85rem;
        }
        .mobileInsurance {
            color: #3fc1c9;
            padding: 0;
        }
        /*animations*/
        @keyframes carmove {
            0% {
                transform: translateY(0px);
            }
            25% {
                transform: translateY(1px)
            }
            29% {
                transform: translateY(2px)
            }
            33% {
                transform: translateY(3px)
            }
            47% {
                transform: translateY(0px)
            }
            58% {
                transform: translateY(1px)
            }
            62% {
                transform: translateY(2px)
            }
            66% {
                transform: translateY(3px)
            }
            75% {
                transform: translateY(1px)
            }
            100% {
                transform: translateY(0px)
            }
        }

        @keyframes wheel {
            0% {
                transform: rotate(0deg)
            }
            100% {
                transform: rotate(-359deg)
            }
        }
    </style>
@endsection
@push('scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "WebPage",
    @if (\Request::is('hybrid'))
            "name": "Personal Car Lease Comparison",
            "name": "Personal Car Lease Comparison",
            "description": "Personal car leasing also commonly known as PCH (Personal Contract Hire) is a long term rental agreement, so you won’t be purchasing the car, you simply return it at the end of the lease term. It is the most affordable, convenient and hassle-free way to drive a brand new car. Choose which car you want, how long you want it for (2, 3 or 4 years), what payment you can afford upfront (normally 1, 3, 6, 9 or 12 months initial payment) and how many miles you think you will drive. Then find fixed monthly payments that suit your budget. At Auto Lease Compare, we bring you the best deals on the market from the UK’s top leasing providers. Search and compare personal lease deals to find your dream car today.",
            "mainEntity":{
                "@type": "Service",
                "serviceType": "Personal Car Leasing",
                "url": "https://www.autoleasecompare.com/car-leasing"
                }
            }
@elseif (\Request::is('business'))
            "name": "business Car Lease Comparison",
            "description": "Personal car leasing also commonly known as PCH (Personal Contract Hire) is a long term rental agreement, so you won’t be purchasing the car, you simply return it at the end of the lease term. It is the most affordable, convenient and hassle-free way to drive a brand new car. Choose which car you want, how long you want it for (2, 3 or 4 years), what payment you can afford upfront (normally 1, 3, 6, 9 or 12 months initial payment) and how many miles you think you will drive. Then find fixed monthly payments that suit your budget. At Auto Lease Compare, we bring you the best deals on the market from the UK’s top leasing providers. Search and compare personal lease deals to find your dream car today.",
            "mainEntity":{
                "@type": "Service",
                "serviceType": "Business Car Leasing",
                "url": "https://www.autoleasecompare.com/business"
                }
            }
@elseif (\Request::is('performance'))
            "name": "Performance Car Lease Comparison",
            "description": "Personal car leasing also commonly known as PCH (Personal Contract Hire) is a long term rental agreement, so you won’t be purchasing the car, you simply return it at the end of the lease term. It is the most affordable, convenient and hassle-free way to drive a brand new car. Choose which car you want, how long you want it for (2, 3 or 4 years), what payment you can afford upfront (normally 1, 3, 6, 9 or 12 months initial payment) and how many miles you think you will drive. Then find fixed monthly payments that suit your budget. At Auto Lease Compare, we bring you the best deals on the market from the UK’s top leasing providers. Search and compare personal lease deals to find your dream car today.",
            "mainEntity":{
                "@type": "Service",
                "serviceType": "Performance Car Leasing",
                "url": "https://www.autoleasecompare.com/performance"
                }
            }
@elseif (\Request::is('adverse-credit'))
            "name": "Adverse Credit Car Lease Comparison",
            "description": "Personal car leasing also commonly known as PCH (Personal Contract Hire) is a long term rental agreement, so you won’t be purchasing the car, you simply return it at the end of the lease term. It is the most affordable, convenient and hassle-free way to drive a brand new car. Choose which car you want, how long you want it for (2, 3 or 4 years), what payment you can afford upfront (normally 1, 3, 6, 9 or 12 months initial payment) and how many miles you think you will drive. Then find fixed monthly payments that suit your budget. At Auto Lease Compare, we bring you the best deals on the market from the UK’s top leasing providers. Search and compare personal lease deals to find your dream car today.",
            "mainEntity":{
                "@type": "Service",
                "serviceType": "Adverse Credit Car Leasing",
                "url": "https://www.autoleasecompare.com/adverse-credit"
                }
            }
@else
            "name": "Personal Car Lease Comparison",
            "description": "Personal car leasing also commonly known as PCH (Personal Contract Hire) is a long term rental agreement, so you won’t be purchasing the car, you simply return it at the end of the lease term. It is the most affordable, convenient and hassle-free way to drive a brand new car. Choose which car you want, how long you want it for (2, 3 or 4 years), what payment you can afford upfront (normally 1, 3, 6, 9 or 12 months initial payment) and how many miles you think you will drive. Then find fixed monthly payments that suit your budget. At Auto Lease Compare, we bring you the best deals on the market from the UK’s top leasing providers. Search and compare personal lease deals to find your dream car today.",
            "mainEntity":{
                "@type": "Service",
                "serviceType": "Personal Car Leasing",
                "url": "https://www.autoleasecompare.com/personal"
                }
            }
@endif
    </script>


    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    <script>
        $(function () {
		// updateFilteredCounter();

            // Setup thumbnail slider
            $('.uspsHeaderMobile').slick({
                autoplay        : true,
                arrows          : false,
                dots            : false,
                draggable       : true,
                slidesToShow    : 1,
                slidesToScroll  : 1,
                touchMove       : true,
                swipe           : true,
                swipeToSlide    : true,
                infinite        : true,
                prevArrow       : '',
                nextArrow       : '',
                mobileFirst     : true
            });

            $("body").on('click', '.leaseFilterType', function (e) {
                e.preventDefault();

                var id = $(this).attr('id');

                if(id == 'personalLeaseFilterDesktop' || id == 'personalLeaseFilter') {
                    $('.finance_type_show').val('P');

                    $(this).addClass('b3');
                    $(this).removeClass('b7');
                    $('#businessLeaseFilterDesktop').addClass('b7');
                    $('#businessLeaseFilter').addClass('b7');
                }else {
                    $('.finance_type_show').val('B');
                    $(this).addClass('b3');
                    $(this).removeClass('b7');
                    $('#personalLeaseFilterDesktop').addClass('b7');
                    $('#personalLeaseFilter').addClass('b7');
                }
                setTimeout(function(){
                    updateFilterContent();
                }, 100);

            });

            // Show loading spinner when clicking on clear all link
            $("body").on('click', '.clearAllFilters', function (e) {
                $('.modalLoading').show();
            });

            // trigger select2
            $('.rangeSelect').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                maximumSelectionLength: 3,
                placeholder: "Select Model",

            });

            // trigger select2
            $('.rangeSelectMulti').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                maximumSelectionLength: 3,
                placeholder: "Select Model",

            });

            $('.trimSelect').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                maximumSelectionLength: 3,
                placeholder: "Select Model Variant",

            });

            $('.doorsDesktop').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                maximumSelectionLength: 3,
                placeholder: "Select Doors",

            });

            $('#engineSize').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                maximumSelectionLength: 3,
                placeholder: "Select engine size",

            });
            $('.mobileMaker').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                placeholder: "Select Manufacturer",
            });

            $('.mobileRange').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                maximumSelectionLength: 3,
                placeholder: "Select Model",
            });
            $('.mobileTrim').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                maximumSelectionLength: 3,
                placeholder: "Select Trim",
            });
            $('#mobileTransmissionSelect').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                placeholder: "Select Transmission",
            });
            $('.mobileBodystyle').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                placeholder: "Select Body Style",
            });
            $('.mobileFuel').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                placeholder: "Select Fuel",
            });

            $('.mobileDoorSelect').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                placeholder: "Select number of doors",
            });
            $('.mobileannual_mileage').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                placeholder: "Select estimated annual mileage",
            });
            $('.mobileTerm').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                placeholder: "Select contract length",
            });
            $('.mobileInitialRental').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                placeholder: "Select initial rental",
            });
            $('.mobilespecs').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                placeholder: "Select optional extras",
            });
            $('.mobileEngineSize').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                placeholder: "Select engine size",
            });

            $('.mobilePriceMin').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                maximumSelectionLength:1,
                placeholder: "Select minimum monthly price",
            });

            $('.mobilePriceMax').select2({
                allowClear: true,
                theme: 'bootstrap4',
                width: "100%",
                maximumSelectionLength:1,
                placeholder: "Select maximum monthly price",
            });

            $('#filterListContainer').on(' hidden.bs.modal', function () {
                updateFilterContent();
            });

            // Handler for updating the sort by ordering
            $("body").on('change', '#filterSortBy', function (e) {
                updateFilterContent();
            });

            $('body').on('change', '.finance_type_show', function (e) {
                updateFilterContent();
            });

            // Handler for updating the sort by ordering
            $("body").on('change', '#in_stock', function (e) {
                updateFilterContent();
            });

            // Pagination handler
            $("body").on('click', '.page-link', function (e) {
                e.preventDefault();
                var page = $(this).attr('data-id');

                // Ajax update the page to the page number
                updateFilterContent(page);
                window.scrollTo(0, 0);
            });

            // Garage
            $("body").on('click', '.addToGarageListing', function (e) {
                e.preventDefault();
                // get the car we're adding to the garage (dealid)
                var carId = $(this).attr('data-id');
                var database = $(this).attr('data-database');
                $.ajax(
                    {
                        url: "/my-account/garage/add/" + carId + '/' + database,
                        success: function (result) {

                            // // saved
                            // if (result == "S") {
                            //     $("#modelgarage").modal("show");
                            // // Need to login
                            // } else if (result == "L") {
                            //     $("#modelGarageLog").modal("show");
                            // // Garage is full
                            // } else if (result == "C") {
                            //     window.location.href = "/my-account/garage?c=1";
                            // }

                            if (result == "S") {
                                $("#modelgarage").modal("show");
                                $('#garageModalContent').html('<p>Your selected car has been added into your <a href="/my-account/garage">garage</a>. You can view your garage from your <a href="/my-account/">dashboard</a>.</p>');
                                // Need to login
                            } else if (result == "L") {
                                $("#modelgarage").modal("show");
                                $('#garageModalContent').html('<p>Sorry, you need to be logged in to your account to use the garage feature. Please <a href="#login" data-toggle="modal" data-dismiss="modal">login or register</a> now.</p><p style="text-align: center"><strong>Within to your Auto Lease Compare garage! </strong>Here you can:</p>' +
                                    '                    <p style="text-align: center">' +
                                    '                        <i class="fa fa-check"></i> Save and compare your favourite lease deals all in one place.<br>' +
                                    '                        <i class="fa fa-check"></i> Compare technical specifications and historical price changes.<br>' +
                                    '                        <i class="fa fa-check"></i> We will send you a notification when your saved deal drops in price!<br>' +
                                    '                    </p>' +
                                    '                    <p style="text-align: center">Hot lease deals are often on a limited stock so can sell out super quickly. <strong>Be quick to enquire, as they can be gone by tomorrow!</strong></p>');
                                // Garage is full
                            } else if (result == "C") {
                                window.location.href = "/my-account/garage?c=1";
                            } else if (result == "C1") {
                                window.location.href = "/my-account/garage?c=2";
                            }
                        }
                    }
                );
            });


            @if( app('request')->input('homepageSearch') == '1' || app('request')->input('h') == '1')
            if($(window).width() > 800) {
                if ($('.rangeSelect').length) {
                    $('.rangeSelect').select2({
                        allowClear: true,
                        theme: 'bootstrap4',
                        width: "100%",
                        maximumSelectionLength: 3,
                        placeholder: "Select Model",

                    });
                }

                if ($('.trimSelect').length) {
                    $('.trimSelect').select2({
                        allowClear: true,
                        theme: 'bootstrap4',
                        width: "100%",
                        maximumSelectionLength: 3,
                        placeholder: "Select Model",

                    });
                }

                if ($('#doorsDesktop').length) {
                    $('#doorsDesktop').select2({
                        allowClear: true,
                        theme: 'bootstrap4',
                        width: "100%",
                        placeholder: "Select number of doors",
                    });
                }

                if ($('#engineSize').length) {
                    $('#engineSize').select2({
                        allowClear: true,
                        theme: 'bootstrap4',
                        width: "100%",
                        maximumSelectionLength: 3,
                        placeholder: "Select engine size",

                    });
                }
            }
            @endif

            if($(window).width() < 800) {
                $("body").on('click', '.makeFilterHeader', function (e) {
                    $('#formCtas').hide();
                    $('#mobileFiltersListing').hide();
                    $('#mobileMakers').toggle();
                    $('#mobileMakers').toggleClass('closed');
                });
                $("body").on('click', '.modelFilterHeader', function (e) {
                    $('#formCtas').hide();
                    $('#mobileFiltersListing').hide();
                    $('#mobileModels').toggle();
                    $('#mobileModels').toggleClass('closed');
                });
                $("body").on('click', '.trimFilterHeader', function (e) {
                    $('#formCtas').hide();
                    $('#mobileFiltersListing').hide();
                    $('#mobileTrim').toggle();
                    $('#mobileTrim').toggleClass('closed');
                });
                $("body").on('click', '.annualmileageFilterHeader', function (e) {
                    $('#formCtas').hide();
                    $('#mobileFiltersListing').hide();
                    $('#mobileMileage').toggle();
                    $('#mobileMileage').toggleClass('closed');
                });
                $("body").on('click', '.contractlengthFilterHeader', function (e) {
                    $('#formCtas').hide();
                    $('#mobileFiltersListing').hide();
                    $('#mobileContract').toggle();
                    $('#mobileContract').toggleClass('closed');
                });
                $("body").on('click', '.initialrentalFilterHeader', function (e) {
                    $('#formCtas').hide();
                    $('#mobileFiltersListing').hide();
                    $('#mobileInitial').toggle();
                    $('#mobileInitial').toggleClass('closed');
                });
                $("body").on('click', '.budgetFilterHeader', function (e) {
                    $('#formCtas').hide();
                    $('#mobileFiltersListing').hide();
                    $('#mobileBudget').toggle();
                    $('#mobileBudget').toggleClass('closed');
                });
                $("body").on('click', '.doorsFilterHeader', function (e) {
                    $('#formCtas').hide();
                    $('#mobileFiltersListing').hide();
                    $('#mobileDoors').toggle();
                    $('#mobileDoors').toggleClass('closed');
                });
                $("body").on('click', '.enginesizeFilterHeader', function (e) {
                    $('#formCtas').hide();
                    $('#mobileFiltersListing').hide();
                    $('#mobileEngine').toggle();
                    $('#mobileEngine').toggleClass('closed');
                });
                $("body").on('click', '.bodystyleFilterHeader', function (e) {
                    $('#formCtas').hide();
                    $('#mobileFiltersListing').hide();
                    $('#mobileBodystyle').toggle();
                    $('#mobileBodystyle').toggleClass('closed');
                });
                $("body").on('click', '.transmissionFilterHeader', function (e) {
                    $('#formCtas').hide();
                    $('#mobileFiltersListing').hide();
                    $('#mobileTransmissionContainer').toggle();
                    $('#mobileTransmissionContainer').toggleClass('closed');
                });
                $("body").on('click', '.fuelFilterHeader', function (e) {
                    $('#formCtas').hide();
                    $('#mobileFiltersListing').hide();
                    $('#mobileFuel').toggle();
                    $('#mobileFuel').toggleClass('closed');
                });
                $("body").on('click', '.specFilterHeader', function (e) {
                    $('#formCtas').hide();
                    $('#mobileFiltersListing').hide();
                    $('#mobileOptions').toggle();
                    $('#mobileOptions').toggleClass('closed');
                });

                $("body").on('select2:opening select2:closing', 'select', function (e) {
                    var searchfield = $( '#'+e.target.id ).parent().find('.select2-search__field');
                    searchfield.prop('disabled', true);
                });


                //
                $('body').on('click', '.back', function () {
                    $('#formCtas').show();
                    $('#mobileFiltersListing').show();
                    updateFilterContent();
                });

                $('body').on('click', '.fuelSelectedLink', function () {

                    var idToRemove = $(this).attr('id');
                    var $select = $('#mobileFuelSelect');

                    var values = $select.val();
                    if (values) {
                        var i = values.indexOf(idToRemove);
                        if (i >= 0) {
                            values.splice(i, 1);
                            $select.val(values).change();

                            $(this).remove();
                            updateFilterContent();

                        }
                    }
                });

                $('body').on('click', '.makerSelectedLink', function () {

                    var idToRemove = $(this).attr('id');
                    var $select = $('.mobileMaker');

                    var values = $select.val();
                    if (values) {
                        var i = values.indexOf(idToRemove);
                        if (i >= 0) {
                            values.splice(i, 1);
                            $select.val(values).change();

                            $(this).remove();
                            updateFilterContent();

                        }
                    }
                });

                $('body').on('click', '.rangeSelectedLink', function () {

                    var idToRemove = $(this).attr('id');
                    var $select = $('.mobileRange');

                    var values = $select.val();
                    if (values) {
                        var i = values.indexOf(idToRemove);
                        if (i >= 0) {
                            values.splice(i, 1);
                            $select.val(values).change();

                            $(this).remove();
                            updateFilterContent();

                        }
                    }
                });

                $('body').on('click', '.trimSelectedLink', function () {

                    var idToRemove = $(this).attr('id');
                    var range = $(this).attr('data-range');
                    var $select = $('#mobile-trim-' + range);

                    var values = $select.val();
                    if (values) {
                        var i = values.indexOf(idToRemove);
                        if (i >= 0) {
                            values.splice(i, 1);
                            $select.val(values).change();

                            $(this).remove();
                            updateFilterContent();

                        }
                    }
                });

                $('body').on('click', '.transmissionSelectedLink', function () {

                    var idToRemove = $(this).attr('id');
                    var $select = $('.mobileTransmissionFilter');

                    var values = $select.val();
                    if (values) {
                        var i = values.indexOf(idToRemove);
                        if (i >= 0) {
                            values.splice(i, 1);
                            $select.val(values).change();

                            $(this).remove();
                            updateFilterContent();

                        }
                    }
                });

                $('body').on('click', '.bodystyleSelectedLink', function () {

                    var idToRemove = $(this).attr('id');
                    var $select = $('.mobileBodystyle');

                    var values = $select.val();
                    if (values) {
                        var i = values.indexOf(idToRemove);
                        if (i >= 0) {
                            values.splice(i, 1);
                            $select.val(values).change();

                            $(this).remove();
                            updateFilterContent();

                        }
                    }
                });

                $('body').on('click', '.enginesizeSelectedLink', function () {

                    var idToRemove = $(this).attr('id');
                    var $select = $('.mobileDoorSelect');

                    var values = $select.val();
                    if (values) {
                        var i = values.indexOf(idToRemove);
                        if (i >= 0) {
                            values.splice(i, 1);
                            $select.val(values).change();

                            $(this).remove();
                            updateFilterContent();

                        }
                    }
                });

                $('body').on('click', '.doorsSelectedLink', function () {

                    var idToRemove = $(this).attr('id');
                    var $select = $('.mobileDoorSelect');

                    var values = $select.val();
                    if (values) {
                        var i = values.indexOf(idToRemove);
                        if (i >= 0) {
                            values.splice(i, 1);
                            $select.val(values).change();

                            $(this).remove();
                            updateFilterContent();

                        }
                    }
                });

                $('body').on('click', '.optionsSelectedLink', function () {

                    var idToRemove = $(this).attr('id');
                    var $select = $('.mobilespecs');

                    var values = $select.val();
                    if (values) {
                        var i = values.indexOf(idToRemove);
                        if (i >= 0) {
                            values.splice(i, 1);
                            $select.val(values).change();

                            $(this).remove();
                            updateFilterContent();

                        }
                    }
                });

                $('body').on('click', '.initialrentalSelectedLink', function () {

                    var idToRemove = $(this).attr('id');
                    var $select = $('.mobileInitialRental');

                    var values = $select.val();
                    if (values) {
                        var i = values.indexOf(idToRemove);
                        if (i >= 0) {
                            values.splice(i, 1);
                            $select.val(values).change();

                            $(this).remove();
                            updateFilterContent();

                        }
                    }
                });

                $('body').on('click', '.contractlengthSelectedLink', function () {

                    var idToRemove = $(this).attr('id');
                    var $select = $('.mobileTerm');

                    var values = $select.val();
                    if (values) {
                        var i = values.indexOf(idToRemove);
                        if (i >= 0) {
                            values.splice(i, 1);
                            $select.val(values).change();

                            $(this).remove();
                            updateFilterContent();

                        }
                    }
                });

                $('body').on('click', '.annualmileageSelectedLink', function () {

                    var idToRemove = $(this).attr('id');
                    var $select = $('.mobileannual_mileage');

                    var values = $select.val();
                    if (values) {
                        var i = values.indexOf(idToRemove);
                        if (i >= 0) {
                            values.splice(i, 1);
                            $select.val(values).change();

                            $(this).remove();
                            updateFilterContent();

                        }
                    }
                });

                $('body').on('click', '.budgetSelectedLink', function () {

                    $('.mobilePriceMin').val([]);
                    $('.mobilePriceMax').val([]);
                    $(this).remove();
                    updateFilterContent();
                });

            }
            $('.modalLoading').hide();
        });

        // Update the results
        function updateFilterContent(page) {


            $('.modalLoading').show();

            var url = "/personal/results";
            var redirectUrl = "/personal/";

                    @if (\Request::is('hybrid'))
            var url = "/hybrid/results";
            var redirectUrl = "/hybrid/";
                    @elseif (\Request::is('business'))
            var url = "/business/results";
            var redirectUrl = "/business/";
                    @elseif (\Request::is('performance'))
            var url = "/performance/results";
            var redirectUrl = "/performance/";
                    @elseif (\Request::is('adverse-credit'))
            var url = "/adverse-credit/results";
            var redirectUrl = "/adverse-credit/";
                    @elseif (\Request::is('personal-vans'))
            var url = "/personal-vans/results";
            var redirectUrl = "/personal-vans/";
                    @elseif (\Request::is('business-vans'))
            var url = "/business-vans/results";
            var redirectUrl = "/business-vans/";
                    @endif

            var filters = $('#filterList').serialize();

            var stock = $('#stockForm').serialize();
            var sort = $('#sortByForm').serialize();
            var data = filters + "&" + stock + "&" + sort;
            if (typeof page !== 'undefined') {
                var data = data + "&page=" + page;
            }
                    @if (\Request::is('personal-vans'))
            var data = data + "&database=LCV";
                    @elseif (\Request::is('business-vans'))
            var data = data + "&database=LCV";
            @endif
            if(window.innerWidth > 800) {
                window.history.pushState({}, "Auto Lease Compare listing", redirectUrl + '?' + decodeURIComponent(data) + '&h=1');
            }
            updateAllSidebarBlocks();

            $.ajax(
                {
                    type: "get",
                    data: data,
                    url: url,
                    success: function (html) {
                        $('#filterContentContainer').html(html)
                    }
                }
	    )
		    // updateFilteredCounter(page);
        }

	function updateFilteredCounterOld() {

            var url = "/personal/counter";
                    @if (\Request::is('hybrid'))
            var url = "/hybrid/counter";
                    @elseif (\Request::is('business'))
            var url = "/business/counter";
                    @elseif (\Request::is('performance'))
            var url = "/performance/counter";
                    @elseif (\Request::is('adverse-credit'))
            var url = "/adverse-credit/counter";
                    @elseif (\Request::is('personal-vans'))
            var url = "/personal-vans/counter";
                    @elseif (\Request::is('business-vans'))
            var url = "/business-vans/counter";
            @endif

            var filters = $('#filterList').serialize();

            var stock = $('#stockForm').serialize();
            var sort = $('#sortByForm').serialize();
            var data = filters + "&" + stock + "&" + sort;
            if (typeof page !== 'undefined') {
                var data = data + "&page=" + page;
            }
                    @if (\Request::is('personal-vans'))
            var data = data + "&database=LCV";
                    @elseif (\Request::is('business-vans'))
            var data = data + "&database=LCV";
            @endif
            $.ajax(
                {
                    type: "get",
                    data: data,
                    url: url,
                    success: function (html) {
                        $('.showingNumber').html('');
                        // update trims modal with new content
                        $('.showingNumber').html(html);
                    }
                });
        }

        function updateAllSidebarBlocks() {
            $('.modalLoading').show();

            if(window.innerWidth <= 800) {

                var filters = $('#mobileFilterList').serialize();
                var stock = $('#stockForm').serialize();
                var sort = $('#sortByForm').serialize();
                var data = filters + "&" + stock + "&" + sort;
                if (typeof page !== 'undefined') {
                    var data = data + "&page=" + page;
                }

                var url = "/personal/filters/mobile";
                        @if (\Request::is('hybrid'))
                var url = "/hybrid/filters/mobile";
                        @elseif (\Request::is('business'))
                var url = "/business/filters/mobile";
                        @elseif (\Request::is('performance'))
                var url = "/performance/filters/mobile";
                        @elseif (\Request::is('adverse-credit'))
                var url = "/adverse-credit/filters/mobile";
                        @elseif (\Request::is('personal-vans'))
                var url = "/personal-vans/filters/mobile";
                        @elseif (\Request::is('business-vans'))
                var url = "/business-vans/filters/mobile";
                @endif
                $.ajax(
                    {
                        type: "get",
                        data: data,
                        url: url,
                        success: function (html) {

                            $('#mobileFilters').html('');
                            // update trims modal with new content
                            $('#mobileFilters').html(html);


                            if ($('.mobileMaker').length) {
                                $('.mobileMaker').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    placeholder: "Select Manufacturer",
                                });
                            }
                            if ($('.mobileRange').length) {
                                $('.mobileRange').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    maximumSelectionLength: 3,
                                    placeholder: "Select Model",
                                });
                            }
                            if ($('.mobileTrim').length) {
                                $('.mobileTrim').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    maximumSelectionLength: 3,
                                    placeholder: "Select Trim",
                                });
                            }
                            if ($('#mobileTransmissionSelect').length) {
                                $('#mobileTransmissionSelect').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    placeholder: "Select Transmission",
                                });
                            }
                            if ($('.mobileBodystyle').length) {
                                $('.mobileBodystyle').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    placeholder: "Select Body Style",
                                });
                            }
                            if ($('.mobileFuel').length) {
                                $('.mobileFuel').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    placeholder: "Select Fuel",
                                });
                            }
                            if ($('.mobileDoorSelect').length) {
                                $('.mobileDoorSelect').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    placeholder: "Select number of doors",
                                });
                            }
                            if ($('.mobileannual_mileage').length) {
                                $('.mobileannual_mileage').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    placeholder: "Select estimated annual mileage",
                                });
                            }
                            if ($('.mobileTerm').length) {
                                $('.mobileTerm').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    placeholder: "Select contract length",
                                });
                            }
                            if ($('.mobileInitialRental').length) {
                                $('.mobileInitialRental').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    placeholder: "Select initial rental",
                                });
                            }
                            if ($('.mobilespecs').length) {
                                $('.mobilespecs').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    placeholder: "Select optional extras",
                                });
                            }
                            if ($('.mobilePriceMin').length) {

                                $('.mobilePriceMin').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    placeholder: "Select minimum monthly price",
                                    maximumSelectionLength:1,
                                });
                            }
                            if ($('.mobilePriceMax').length) {
                                $('.mobilePriceMax').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    placeholder: "Select maximum monthly price",
                                    maximumSelectionLength:1,
                                });
                            }
                            if ($('.mobileEngineSize').length) {
                                $('.mobileEngineSize').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    placeholder: "Select engine size",
                                });
                            }


                            $('.modalLoading').hide();
                        }
                    }
                )


            }else {
                var filters = $('#filterList').serialize();
                var stock = $('#stockForm').serialize();
                var sort = $('#sortByForm').serialize();
                var data = filters + "&" + stock + "&" + sort;
                if (typeof page !== 'undefined') {
                    var data = data + "&page=" + page;
                }

                var url = "/personal/filters";
                        @if (\Request::is('hybrid'))
                var url = "/hybrid/filters";
                        @elseif (\Request::is('business'))
                var url = "/business/filters";
                        @elseif (\Request::is('performance'))
                var url = "/performance/filters";
                        @elseif (\Request::is('adverse-credit'))
                var url = "/adverse-credit/filters";
                        @elseif (\Request::is('personal-vans'))
                var url = "/personal-vans/filters";
                var data = data + "&database=LCV";
                        @elseif (\Request::is('business-vans'))
                var url = "/business-vans/filters";
                var data = data + "&database=LCV";
                @endif
                $.ajax(
                    {
                        type: "get",
                        data: data,
                        url: url,
                        success: function (html) {
                            if(window.innerWidth <= 800) {

                                $('#mobileFilters').html('');
                                // update trims modal with new content
                                $('#mobileFilters').html(html);

                            } else {

                                $('#filterListContainer').html('');
                                // update trims modal with new content
                                $('#filterListContainer').html(html);
                            }

                            if ($('.rangeSelect').length) {
                                $('.rangeSelect').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    maximumSelectionLength: 3,
                                    placeholder: "Select Model",

                                });
                            }

                            if ($('.trimSelect').length) {
                                $('.trimSelect').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    maximumSelectionLength: 3,
                                    placeholder: "Select Model",

                                });
                            }

                            if ($('#doorsDesktop').length) {
                                $('#doorsDesktop').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    placeholder: "Select number of doors",
                                });
                            }

                            if ($('#engineSize').length) {
                                $('#engineSize').select2({
                                    allowClear: true,
                                    theme: 'bootstrap4',
                                    width: "100%",
                                    maximumSelectionLength: 3,
                                    placeholder: "Select engine size",

                                });
                            }

                            $('.modalLoading').hide();
                        }
                    }
                )
            }
        }

        $(function () {
            $("body").on('submit', '.update-featured-form', function (e) {
                e.preventDefault();
                var formInfo = $(this).serialize();
                var capId = $(this).find('input[name="cap-id"]').val();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'get',
                    url: '{{route('update-featured')}}',
                    data: formInfo,
                }).done(function(response) {
                    $('div.success-' + capId).html('Featured Updated').delay(9000).fadeOut();
                }).fail(function() {

                });
            });

        });

        function moveDiv() {
            if ($(window).width() < 992) {
                $("#stockForm").appendTo(".stockFormContainer1");
            } else {
                $("#stockForm").appendTo(".stockFormContainer");

            }
        }
        moveDiv();
        $(window).resize(moveDiv);
        window.onpageshow = function(event) {
            if (event.persisted) {
                window.location.reload();
            }
        };
    </script>
@endpush
