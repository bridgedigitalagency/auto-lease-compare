@extends('layouts.app')
@section('template_title') 404 Forbidden @endsection
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@push('scripts')
@endpush
@section('content')
    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">403 Forbidden</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>There seems to be a problem</h2>
                <p>It looks like you dont have access to view this page.</p>
                <p><a href="/">Back home</a></p>
            </div>
        </div>
    </div>

@endsection