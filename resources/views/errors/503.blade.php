<!DOCTYPE html>
<html>
<head>
    <title>Site is down for maintenance</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="/css/maintenance.css">
</head>
<body>
<article>
    <img src="/images/alc.svg">
</article>
<article>
    <h1>Site is temporarily unavailable.</h1>
    <p>Scheduled maintenance is currently in progress. Please check back soon.</p>
    <p>We apologise for any inconvenience.</p>
</article>
</body>
</html>
