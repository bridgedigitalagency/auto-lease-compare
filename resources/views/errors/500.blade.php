@extends('layouts.app')
@section('template_title') 500 Internal Server Error @endsection
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@push('scripts')
@endpush
@section('content')
    @php
    $message = "User has triggered a 500 error on: " . Request::url();
        Log::error($message);
    @endphp
    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">Internal Server Error 500</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>There seems to be a problem</h2>
                <p>It looks like the page you're trying to view cannot be displayed because of an error. Please try again.</p>
                <p>This error has been logged and we will look into it as soon as possible.</p>
                <p><a href="/">Back home</a></p>
            </div>
        </div>
    </div>

@endsection