@extends('layouts.app')
@section('content')

    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">Sell Your Vehicle</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Car Lease Comparison (Home)</a></div>
            </div>
        </div>
    </div>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Compare multiple offers for your vehicle today with Auto Lease Compare in partnership with Wizzle.</h2>
                    <p>We help you find a vehicle buyer online by advertising your vehicle free to our network of thousands of potential buyers in the UK.
                        We’ll also find you instant offers from the major online vehicle buying companies, so you don’t have to.</p>

                    <div id="wizzleAppLoader" class="mb-5"></div>

                    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
                    <script>
                        function startWizzleApp(json){
                            var mtx = jQuery("<script/>").text(json.data);
                            var css = jQuery("<style/>").text(json.css);
                            var fnt = jQuery("<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Varela+Round&display=swap:300,400,600&amp;subset=latin&amp;lang=en' type='text/css'/>");
                            var cde = jQuery("<script/>").attr('src', json.script).attr('crossorigin', true);
                            jQuery('#wizzleAppLoader').html(json.html);
                            jQuery('head').append(mtx);
                            jQuery('head').prepend(css);
                            jQuery('head').append(fnt);
                            jQuery('body').append(cde);
                        }
                        jQuery(document).ready(function($){
                            jQuery.ajax({
                                async: true,
                                url: 'https://www.wizzle.co.uk/webapp/wizzle/load',
                                headers: {'x-wizzle-auth' : 'jZ5aNaWrZbjSHHmmryPJ7Jqty32RmMEH'},
                                dataType: 'jsonp'
                            });
                        });
                    </script>



                </div>
            </div>
        </div>
    </section>
@endsection
<style>
    .wizzletheme body {
        font-family: "Varela Round",Helvetica,Arial,sans-serif !important;
    }
    .navbar-laravel {
        margin-bottom: 0px;
    }
    html.wizzletheme {
        font-size: unset;
    }
</style>