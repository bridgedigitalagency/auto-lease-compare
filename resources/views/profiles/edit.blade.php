@extends('layouts.app')

@section('template_title')

    {{ trans('profile.templateTitle') }}
@endsection

@section('content')
    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">Edit Profile</h1>
                <div style="text-align:center; color:#fff;"  class="breadc"><a href="/" style="color:#fff;">Home</a> / <a href="/my-account" style="color:#fff;">My Dashboard</a></div>
            </div>
        </div>
    </div>


    @if(Auth::check() && Auth::user()->hasRole('admin'))

        @include('partials.profile-admin')

    @elseif(Auth::check() && Auth::user()->hasRole('dealer'))
        @include('partials.profile-dealer')
    @elseif(Auth::check() && Auth::user()->hasRole('user'))
        @include('partials.profile-user')
    @endif

@endsection
@section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="/css/profile.css">
@endsection
@section('footer_scripts')
    <script src="/js/profile.js" type="text/javascript"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()

            $('#checkConfirmDelete').change(function() {
                var submitDelete = $('#delete_account_trigger');
                var self = $(this);

                if (self.is(':checked')) {
                    submitDelete.attr('disabled', false);
                }
                else {
                    submitDelete.attr('disabled', true);
                }
            });
            $('#userProfileUpdate').submit(function(e) {

                var newName = $('#name').val();
                $('#accountName').val(newName);

                return true;
            });

            $('#userSelect').submit(function(e) {
                e.preventDefault();
                var user = $('#userAccount option:selected').val();
                window.location.href = '/profile/' + user;
            });

        })

    </script>
@endsection
