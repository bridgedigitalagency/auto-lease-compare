@extends('layouts.app')
@if($database == 'LCV')
    @section('template_title')Van Leasing | Compare Van Lease Deals  @endsection
    @section('template_description')
        <meta name="description" content="Drive a brand new van at a fraction of the cost with Auto Lease Compare; bringing you the best van lease deals available all in one place. Search now.">
    @endsection
@else
    @section('template_title')UK's Best Car Leasing Comparison Site @endsection
    @section('template_description')
        <meta name="description" content="Compare the cheapest lease deals on the market from UK's top leasing companies. Compare millions of car lease offers. No sign-up required. Call or Enquire now"> @endsection
@endif
@section('content')
    <section class="bg">
        <div class="container">
            <div class="banner_text text-center div_zindex white-text mb-4">
                <h1 class="main-title">
                    @if($database == 'LCV')
                        Compare Van Leasing Deals
                    @else
                        Compare Car Leasing Deals
                    @endif
                </h1>
                <h2>
                    @if($database == 'LCV')
                        The Van Lease Comparison Site
                    @else
                        The UK’s #1 Car Leasing Comparison Site
                    @endif
                        <h3 class="trustpilot">
                            <div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b6a8b0d04a076446a9ad" data-businessunit-id="5c0d3879e18cab000171b6fe" data-style-height="24px" data-style-width="100%" data-theme="dark">
                                <a href="https://uk.trustpilot.com/review/www.autoleasecompare.com" target="_blank" rel="noopener">Trustpilot</a>
                            </div>
                        </h3>
                </h2>
                {{--<img src="/images/trustpilot-inline-white.png" width="250">--}}
            </div>
            <div class="row">
                <div class="col-md-5 homesearch">
                    <form action="#" method="get" class="" id="homepageCarSearch">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 mb-2">
                                <h3>Find Your Perfect Lease Today</h3>
                                <div class="finance_type">
                                    @if(isset($database) && $database == 'LCV')
                                        <label><input type="radio" name="finance-type" value="b" checked=""> Business</label>
                                        <label><input type="radio" name="finance-type" value="p"> Personal</label>
                                    @else
                                        <label><input type="radio" name="finance-type" value="p" checked=""> Personal</label>
                                        <label><input type="radio" name="finance-type" value="b"> Business</label>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-md-push-3">
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 mb-2">
                                        <div class="select">
                                            <select id="maker" class="w-100 form-control pricing_select" name="maker">
                                                <option value="" selected="">Any Make</option>
                                                @foreach($makers as $maker)
                                                    <option value="{{$maker->maker}}">{{$maker->maker}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-6 mb-2">
                                        <div class="select">
                                            <select id="range" class="w-100 form-control pricing_select" name="range[]">
                                                <option value="" selected="">Any Model</option>
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="extras">
                            <div class="row viewMore mb-3">
                                <div class="col-md-12">
                                    <a href="#" id="showMoreFilters">Not sure what you want? See more filters &darr;</a>
                                </div>
                            </div>
                            <div class="extraFields" style="display: none">
                                <div class="row">
                                    <div class="col-md-6 col-xs-6">
                                        <div class="select">
                                            <select id="pricemin" class="w-100 form-control mb-2 pricing_select" name="pricemin">
                                                <option value="">Min Price</option>
                                                <option value="100">£100 p/m</option>
                                                <option value="125">£125 p/m</option>
                                                <option value="150">£150 p/m</option>
                                                <option value="175">£175 p/m</option>
                                                <option value="200">£200 p/m</option>
                                                <option value="225">£225 p/m</option>
                                                <option value="250">£250 p/m</option>
                                                <option value="275">£275 p/m</option>
                                                <option value="300">£300 p/m</option>
                                                <option value="325">£325 p/m</option>
                                                <option value="350">£350 p/m</option>
                                                <option value="375">£375 p/m</option>
                                                <option value="400">£400 p/m</option>
                                                <option value="425">£425 p/m</option>
                                                <option value="450">£450 p/m</option>
                                                <option value="475">£475 p/m</option>
                                                <option value="500">£500 p/m</option>
                                                <option value="550">£550 p/m</option>
                                                <option value="600">£600 p/m</option>
                                                <option value="650">£650 p/m</option>
                                                <option value="700">£700 p/m</option>
                                                <option value="750">£750 p/m</option>
                                                <option value="800">£800 p/m</option>
                                                <option value="900">£900 p/m</option>
                                                <option value="1000">£1000 p/m</option>
                                                <option value="1100">£1100 p/m</option>
                                                <option value="1200">£1200 p/m</option>
                                                <option value="1300">£1300 p/m</option>
                                                <option value="1400">£1400 p/m</option>
                                                <option value="1500">£1500 p/m</option>
                                                <option value="1750">£1750 p/m</option>
                                                <option value="2000">£2000 p/m</option>
                                                <option value="2250">£2250 p/m</option>
                                                <option value="2500">£2500 p/m</option>
                                                <option value="2750">£2750 p/m</option>
                                                <option value="3000">£3000 p/m</option>
                                                <option value="3500">£3500 p/m</option>
                                                <option value="4000">£4000 p/m</option>
                                                <option value="4500">£4500 p/m</option>
                                                <option value="5000">£5000 p/m</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-xs-6 mb-2">
                                        <div class="select">
                                            <select id="pricemax" class="w-100 form-control mb-2 pricing_select" name="pricemax">
                                                <option value="">Max Price</option>
                                                <option value="100">£100 p/m</option>
                                                <option value="125">£125 p/m</option>
                                                <option value="150">£150 p/m</option>
                                                <option value="175">£175 p/m</option>
                                                <option value="200">£200 p/m</option>
                                                <option value="225">£225 p/m</option>
                                                <option value="250">£250 p/m</option>
                                                <option value="275">£275 p/m</option>
                                                <option value="300">£300 p/m</option>
                                                <option value="325">£325 p/m</option>
                                                <option value="350">£350 p/m</option>
                                                <option value="375">£375 p/m</option>
                                                <option value="400">£400 p/m</option>
                                                <option value="425">£425 p/m</option>
                                                <option value="450">£450 p/m</option>
                                                <option value="475">£475 p/m</option>
                                                <option value="500">£500 p/m</option>
                                                <option value="550">£550 p/m</option>
                                                <option value="600">£600 p/m</option>
                                                <option value="650">£650 p/m</option>
                                                <option value="700">£700 p/m</option>
                                                <option value="750">£750 p/m</option>
                                                <option value="800">£800 p/m</option>
                                                <option value="900">£900 p/m</option>
                                                <option value="1000">£1000 p/m</option>
                                                <option value="1100">£1100 p/m</option>
                                                <option value="1200">£1200 p/m</option>
                                                <option value="1300">£1300 p/m</option>
                                                <option value="1400">£1400 p/m</option>
                                                <option value="1500">£1500 p/m</option>
                                                <option value="1750">£1750 p/m</option>
                                                <option value="2000">£2000 p/m</option>
                                                <option value="2250">£2250 p/m</option>
                                                <option value="2500">£2500 p/m</option>
                                                <option value="2750">£2750 p/m</option>
                                                <option value="3000">£3000 p/m</option>
                                                <option value="3500">£3500 p/m</option>
                                                <option value="4000">£4000 p/m</option>
                                                <option value="4500">£4500 p/m</option>
                                                <option value="5000">£5000 p/m</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-6 mb-2">
                                    <div class="select">
                                        <select id="bodystyle" class="w-100 form-control pricing_select" name="bodystyle[]">
                                            @if($database == 'LCV')
                                                <option value="" selected="">Any Body Type</option>
                                                @foreach($bodystyles as $bodystyle)
                                                    <option id='body-{{str_replace(' ', '-', $bodystyle->bodystyle)}}' value="{{$bodystyle->bodystyle}}">{{$bodystyle->bodystyle}}</option>
                                                @endforeach
                                            @else
                                                <option value="" selected="">Any Body Type</option>
                                                @foreach($bodystyles as $bodystyle)
                                                    <option id='body-{{str_replace(' ', '-', $bodystyle->bodystyle)}}' value="{{$bodystyle->bodystyle}}">{{$bodystyle->bodystyle}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <select name="transmission" id="transmission" class="w-100 form-control mb-2">
                                        <option value="">Select Transmission</option>
                                        <option id=trans-auto" value="Automatic">Automatic</option>
                                        <option id="trans-manual" value="Manual">Manual</option>
                                    </select>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <select name="fuel" id="fuel" class="w-100 form-control mb-2">
                                        <option value="">Select Fuel Type</option>
                                        <option value="Diesel">Diesel</option>
                                        <option value="Diesel Electric Hybrid">Diesel Electric Hybrid</option>
                                        <option value="Electric">Electric</option>
                                        <option value="Petrol">Petrol</option>
                                        <option value="Petrol Electric Hybrid">Petrol Electric Hybrid</option>
                                        <option value="PlugIn Elec Hybrid">PlugIn Elec Hybrid</option>
                                        <option value="Petrol PlugIn Elec Hybrid">Petrol PlugIn Elec Hybrid</option>

                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <select id="engineSize" name="engineSize" class="w-100 form-control mb-2">
                                        <option value="0">Any Engine Size</option>
                                        <option value="0-1.0">Less than 1.0L</option>
                                        <option value="1.0-1.3">1.0L-1.3L</option>
                                        <option value="1.4-1.6">1.4L-1.6L</option>
                                        <option value="1.7-1.9">1.7L-1.9L</option>
                                        <option value="2.0-2.5">2.0L-2.5L</option>
                                        <option value="2.6-2.9">2.6L-2.9L</option>
                                        <option value="3.0-3.9">3.0L-3.9L</option>
                                        <option value="4.0-4.9">4.0L-4.9L</option>
                                        <option value="5.0+">5.0L and over</option>
                                    </select>
                                </div>
                            </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-12">
                                <a href="#" id="showLessFilters">View less filters &uarr;</a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12 col-xs-12">
                                    <button type="submit" class="button b3"><i class="fa fa-search"></i> Find deals</button>
                                    @if($database == 'LCV')
                                        <input type="hidden" name="database" value="LCV">
                                        <p><a style="margin-top: 10px; display: block; color: #000;" href="/">Looking for a car?</a></p>
                                    @else
                                        <input type="hidden" name="database" value="CARS">
                                        <p><a style="margin-top: 10px; display: block; color: #000;" href="/vans">Looking for a van?</a></p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-1 d-none-1200"></div>
                <div class="col-md-7 col-lg-6 alc-process">
                    <ol>
                        <li><img src="/images/search1.png" alt="search car leasing deals"><span>
                                @if($database == 'LCV')
                                    1. Search for your perfect van
                                @else
                                    1. Search for your perfect car
                                @endif
                            </span></li>
                        <li><img src="/images/compare.png" alt="compare car leasing deals"><span>2. Compare deals from trusted lease providers</span></li>
                        <li><img src="/images/choose.png" alt="choose a lease deal"><span>3. Choose the best deals to suit your needs</span></li>
                        <li><img src="/images/enquire.png" alt="Enquire Today"><span>4. Send enquiries to the lease providers or call direct</span></li>
                    </ol>
                    <div class="car">
                        <div class="carbody"></div>
                        <div class="weel weel1"></div>
                        <div class="weel weel2"></div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-2"><div class="bannerbottom"><i class="fa fa-check"></i> No Added Fees</div></div>
                <div class="col-md-2"><div class="bannerbottom"><i class="fa fa-check"></i>
                    @if($database == 'LCV')
                        Brand New Vans
                    @else
                        Brand New Cars
                    @endif
                </div></div>
                    <div class="col-md-2"><div class="bannerbottom"><i class="fa fa-check"></i> Road Tax Included</div></div>
                    <div class="col-md-2"><div class="bannerbottom"><i class="fa fa-check"></i> Manufacturers Warranty</div></div>
                    <div class="col-md-2"><div class="bannerbottom"><i class="fa fa-check"></i> Free Delivery UK</div></div>
                    <div class="col-md-2"><div class="bannerbottom"><i class="fa fa-check"></i> Breakdown Cover</div></div>
            </div>
        </div>
    </section>

    <section class="featured pb-4 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center pb-4 pt-md-5 pt-3">
                    <h2>
                    @if($database == 'LCV')
                        Hot Van Leasing Deals
                    @else
                        Hot Car Leasing Deals
                    @endif
                    </h2>
                </div>
            </div>
            <div class="row row-eq-height">
                <div class="owl-carousel owl-theme">
                @foreach($getFeatured as $result)
                    <div class="col-12 mb-3 mt-3 relative-link">
                        <div class="result-row1">
                            @if(!is_null($result->insurance) && $result->insurance > 0)
                                <span class="mobileInsuranceTag d-inline"><p>Free Insurance</p></span>
                            @endif
                            @if($result->in_stock =='1')<div class="stocktag instock"><p>In Stock</p></div> @else <div class="stocktag"><p>Factory Order</p></div> @endif
                            @if(strtoupper($database) == 'LCV')
                                <a href="/quote/enquiry/{{ $result->id }}/lcv">
                            @else
                                <a href="/quote/enquiry/{{ $result->id }}/cars">
                            @endif

                                <img src="{{ $result->imageid }}" alt="{{ $result->maker }} {{ $result->model }} {{ $result->derivative }}">

                                <div class="relative text-center">
                                    <div class="relative-title"><p>{{ $result->maker }} {{ $result->model }} {{ $result->derivative }}</p></div>
                                    <span class="relative-price mb-3" style="font-size: 19px;">From £ {{ $result->monthly_payment }} P/M</span><br/>
                                    <div class="mb-2"></div>
                                </div>
                            </a>
                            @role('admin')
                                <form method="POST" class="update-featured-form" data-value="{{$result->id}}" action="#">
                                    {{method_field('PUT')}}
                                    {{ csrf_field() }}
                                    <input type="hidden" name="cap-id" value="{{$result->id}}">
                                    <input class="form-control" style="display:none;" name="update-featured" id="update-featured-{{$result->id}}"  data-value="{{$result->id}}" value="0">
                                    <input type="submit" value="- Remove Featured" name="submit" class="submit button b3 mt-3" id="submit-{{$result->id}}" />
                                </form>
                                <div class="success-{{$result->id}}"></div>
                            @endrole
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </section>
    <section class="browseman pt-md-5 pt-3 pb-md-4 pb-3">
        <div class="container">
            <div class="text-center pb-3">
                <h2>Browse By Manufacturer</h2>
            </div>
            <div id="manufacturer_list">
                @foreach($makers as $maker)
                    @if ($loop->first)
                        <div class="row row-eq-height justify-content-center">
                            @endif
                            @if(isset($database) && $database == 'LCV')
                                <div class="col col-md-1 col-2 homepage{{$maker->maker}}" style="text-align: center; padding-bottom: 20px;">
                                    <a href="/van-leasing/{{str_replace(' ', '-', strtolower($maker->maker))}}" title="{{ucwords(strtolower($maker->maker))}}">
                                        <img src="/images/manufacturers/{{strtolower($maker->maker)}}.png" class="img-responsive" alt="{{strtolower($maker->maker)}} logo" width="65"  height="65">
                                    </a>
                                </div>
                            @else
                                <div class="col col-md-1 col-2 homepage{{$maker->maker}}" style="text-align: center; padding-bottom: 20px;">
                                    <a href="/car-leasing/{{str_replace(' ', '-', strtolower($maker->maker))}}" title="{{ucwords(strtolower($maker->maker))}}">
                                        <img src="/images/manufacturers/{{strtolower($maker->maker)}}.png" class="img-responsive" alt="{{strtolower($maker->maker)}} logo" width="65"  height="65">
                                    </a>
                                </div>
                            @endif
                            @if($loop->iteration % 12 == 0)
                        </div>
                        <div class="row row-eq-height justify-content-center">
                            @endif
                            @if ($loop->last)
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>

    <section class="browseLeaseType manufacturerSearch pt-md-5 pt-1 pb-mb-5 pb-3">
        <div class="container">
            <div class="text-center pb-3 ">
                <h2 class="w900">Browse By Lease Type</h2>
            </div>
            <div class="row text-white about text-center">
                <div class="col-md-6 mb-4">
                    @if($database == 'LCV')
                        <a href="/personal-vans/" class="text-white">
                    @else
                        <a href="/personal/" class="text-white">
                    @endif

                            @if($database == 'LCV')
                                <div class="boxes" style="padding:150px 0px;background-image: url(/images/personal-van-leasing.jpg); background-position: right center;background-size: cover;">
                                    <h2>
                                Personal Van Leasing
                            @else
                                <div class="boxes" style="padding:100px 0px;background-image: url(/images/personal-car-leasing.jpg); background-position: right center;background-size: cover;">
                                    <h2>
                                Personal Car Leasing
                            @endif
                            </h2>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 mb-4">
                    @if($database == 'LCV')
                        <a href="/business-vans/" class="text-white">
                    @else
                        <a href="/business/" class="text-white">
                    @endif

                                @if($database  == 'LCV')
                                <div class="boxes" style=" padding:150px 0px;background-image: url(/images/business-van-leasing.jpg); background-position: right center;background-size: cover;">
                                    <h2>
                                    Business Van Leasing
                                @else
                                    <div class="boxes" style="padding:100px 0px;background-image: url(/images/business-car-leasing.jpg); background-position: right center;background-size: cover;">
                                        <h2>
                                    Business Car Leasing
                                @endif
                            </h2>
                        </div>
                    </a>
                </div>
                @if($database != 'LCV')
                    <div class="col-md-6 mb-4">
                        <a href="/personal/?fuel[]=Diesel%20PlugIn%20Elec%20Hybrid&fuel[]=Hydrogen&fuel[]=Petrol%20Electric%20Hybrid&fuel[]=Petrol%20PlugIn%20Elec%20Hybrid" class="text-white">
                            <div class="boxes" style="padding:100px 0px;background-image: url(/images/performance-car-leasing.jpg); background-position: right center;background-size: cover;">
                                <h2>Hybrid Car Leasing</h2>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="/personal/?fuel[]=Electric" class="text-white">
                            <div class="boxes" style="padding:100px 0px;background-image: url(/images/hybrid-car-leasing.jpg); background-position: right center;background-size: cover;">
                                <h2>Electric Car Leasing</h2>
                            </div>
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </section>

    <section class="pt-md-5 pb-md-5 pb-4 bg garage-home">
        <div class="container text-white text-center">
            <h2 class="mb-3">Save Your Favourite Lease Deals</h2>
            @if($database == 'LCV')
                <p>Compare van leasing deals in our state of the art comparison garage. Compare specifications, costs and price changes throughout the leasing market. </p>
                <h3>We will even send you an email when your saved vans drop in price!</h3>
            @else
                <p>Compare car leasing deals in our state of the art comparison garage. Compare specifications, costs and price changes throughout the leasing market. </p>
                <h3>We will even send you an email when your saved cars drop in price!</h3>
            @endif
            <img src="/images/comparelease.jpg" width="700" alt="compare cars in our garage" class="img-responsive mx-auto mb-3">
            @guest
                <a data-toggle="modal" data-target="#login" class="button b3"  style="cursor: pointer;">My Garage</a>
            @else
                <a href="/my-account/garage" class="button b3">My Garage</a>
            @endguest

        </div>
    </section>
    <section class="pb-md-5 pb-3 pt-md-5 pt-3 b3 text-white about">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-4 text-center">
                    <div class="hideonmobile">
                    @if($database == 'LCV')
                        <h2 style="font-size: 2.2rem;">The UK’s No.1 Van Leasing Comparison Site</h2>
                    @else
                        <h2 style="font-size: 2.2rem;">The UK’s No.1 Car Leasing Comparison Site</h2>
                    @endif
                    </div>
<a name="about"></a>

                    <h2>Compare Cheap Lease Deals From Trusted UK Providers</h2>
                    <!-- TrustBox widget - Micro Review Count -->
                    <div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b6a8b0d04a076446a9ad" data-businessunit-id="5c0d3879e18cab000171b6fe" data-style-height="24px" data-style-width="100%" data-theme="dark">
                        <a href="https://uk.trustpilot.com/review/www.autoleasecompare.com" target="_blank" rel="noopener">Trustpilot</a>
                    </div>
                    <!-- End TrustBox widget -->
                    <a href="https://uk.trustpilot.com/review/www.autoleasecompare.com" target="_blank" style="color: #fff;">Rated Excellent on Trustpilot</a>
                </div>
                <div class="col-md-6">
                    <h2 class="mb-3 text-center">About Auto Lease Compare</h2>
                    <p>Buying is outdated, leasing is the future… Auto Lease Compare is changing the way you think about vehicle ownership. </p>
                    <p>In early 2019, we (the Co-founders) were trying to find a personal car lease online. We spent hours manually searching through the websites of various <strong>car leasing companies</strong> to compare prices - this is when we realised something was missing from this space recognising that the vehicle leasing process was fragmented. </p>
                    <p>The obvious lack of effective research and comparison tools made leasing a new vehicle challenging and time consuming, whilst giving no confidence to the consumer that they have found the best deal. </p>
                    <p>At Auto Lease Compare, we are determined to redesign and fix the vehicle leasing process, allowing the consumer to explore and compare what the market really has to offer all in one place - in a customer-focused, time and cost effective manner. We are the UK’s most trusted lease comparison site. </p>
                    <h4>Useful Links</h4>
                    <ul>
                        <li>
                            <a class="text-white" href="/leasing-information">About Leasing & FAQs</a>
                        </li>
                        @guest
                            <li>
                                <a class="text-white" data-target="#login" data-toggle="modal" style="cursor: pointer;">Comparison Garage</a>
                            </li>
                        @else
                            <li>
                                <a class="text-white"  href="/my-account/garage">Comparison Garage</a>
                            </li>
                        @endguest

                        <li>
                            <a class="text-white"  href="/blog">Blog</a>
                        </li>
                        <li>
                            <a class="text-white" data-target="#login" data-toggle="modal" style="cursor: pointer;">Login</a>
                        </li>
                        <li>
                            <a class="text-white" data-target="#register" data-toggle="modal" style="cursor: pointer;">Create An Account</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <h2 class="mb-3 text-center">Why Use Auto Lease Compare</h2>
                    <ul class="fa-ul">
                        <li>
                            <span class="fa-li"><i class="fa fa-car"></i></span>
                            <h3>Compare Deals</h3>
                            @if($database == 'LCV')
			                    <p>We compare van lease deals from the UK’s best leasing providers so that you can browse through millions of deals - all in one place. </p>
                            @else
                                <p>We compare car lease deals from the UK’s best leasing providers so that you can browse through millions of deals - all in one place. </p>
                            @endif
                        </li>
                        <li>
                            <span class="fa-li"><i class="fa fa-thumbs-o-up"></i></span>
                            <h3>Save Money</h3>
                            <p>We compile the best deals we can find so you can compare them, giving confidence you are getting a great price.</p>
                        </li>
                        <li>
                            <span class="fa-li"><i class="fa fa-history"></i></span>
                            <h4>Save Time & Energy</h4>
                            @if($database == 'LCV')
                                <p>You no longer need to waste time trawling the internet looking for the best deal, find your perfect van on Auto Lease Compare. </p>
                            @else
                                <p>You no longer need to waste time trawling the internet looking for the best deal, find your perfect car on Auto Lease Compare. </p>
                            @endif

                        </li>
                        <li>
                            <span class="fa-li"><i class="fa fa-handshake-o"></i></span>
                            <h3>Deal Direct; No Extra Fees</h3>
<p>It’s completely free to <strong>compare lease prices</strong> with us - we put you in direct contact with the leasing company and never add anything onto the price you see. </p>
                        </li>
                        <li>
                            <span class="fa-li"><i class="fa fa-clock-o"></i></span>
                            <h3>Regular Pricing Updates</h3>
<p>We believe in full transparency and our partners are always vigilant on prices, updating them regularly to compete for your business.</p>
                        </li>
                        <li>
                            <span class="fa-li"><i class="fa fa-users"></i></span>
                            <h3>Top Leasing Companies</h3>
                            @if($database == 'LCV')
                                <p>We hand pick who we allow to advertise on our website, ensuring you receive the best customer service from the UKs top leasing companies. </p> <p>Other <strong><a class="text-white" href="/leasing-information">benefits of van leasing</a></strong> include a brand new vehicle delivered to your door (free of charge) with road tax and manufacturer’s warranty included – giving you extra peace of mind.  Oh, and no MOT due for the first three years!</p>
                            @else
                                <p>We hand pick who we allow to advertise on our website, ensuring you receive the best customer service from the UKs top leasing companies. </p> <p>Other <strong><a class="text-white" href="/leasing-information">benefits of car leasing</a></strong> include a brand new vehicle delivered to your door (free of charge) with road tax and manufacturer’s warranty included – giving you extra peace of mind.  Oh, and no MOT due for the first three years!</p>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    @if($blogposts)
    <section class="browseman blogposts pt-md-5 pt-3 pb-md-5 pb-3">
        <div class="container">
            <div class="row">
                <div class="col col-sm-12">
                    <div class="text-center">
                        <h2 class="pb-4">News & Leasing Guides</h2>
                    </div>
                </div>
            </div>

            <div class="row row-eq-height">
                <div class="owl-carousel owl-theme">
                    @include('partials.homepage-blog')
                </div>
                <div class="col-sm-12 text-right">
                    <a href="/blog" class="button b1">View all</a>
                </div>
            </div>

        </div>
    </section>
    @endif

@endsection
@push('scripts')

    <script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Auto Lease Compare",
  "url": "https://www.autoleasecompare.com/",
  "logo": "https://www.autoleasecompare.com/images/alc.svg",
  "sameAs": [
    "https://www.facebook.com/autoleasecompare",
    "https://twitter.com/compare_lease",
    "https://www.instagram.com/auto_lease_compare/",
    "https://www.linkedin.com/company/auto-lease-compare"
  ]
}
</script>

    <script src="/owlcarousel/owl.carousel.min.js"></script>

    <!-- TrustBox script -->
    <script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
    <!-- End TrustBox script -->

    <script type="text/javascript">
        function getUrlVars() {
            var vars = {};
            var parts = decodeURI(window.location.hash).replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }
        $(function () {

            @guest
                @if( app('request')->input('login') == '1')
                    $('#navigationLoginButton').click();
                @endif
			@if( app('request')->input('register') == '1')
                    $('#registerModalPopup').click();
                @endif
            @else
                @if( app('request')->input('login') == '1')
                    window.location.href="/my-account";
                @endif
            @endguest

            @if ($errors->has('email'))
                $('#navigationLoginButton').click();
            @endif

            @if ($errors->has('password'))
                $('#registerModalPopup').click();
            @endif

            $('body').on('click', '#showMoreFilters', function (e) {
                e.preventDefault()
                $('.extraFields').toggle();
                $('.viewMore').toggle();
                $('#showLessFilters').toggle();
            });

            $('body').on('click', '#showLessFilters', function (e) {
                e.preventDefault()
                $('.extraFields').toggle();
                $('.viewMore').toggle();
                $('#showLessFilters').toggle();
            });

            $('body').on('change', '#priceType', function () {

                var pricerange = $("#pricerange");
                if($('#priceType option:selected').val() == 'flexible' || $('#priceType option:selected').val() == '') {

                    pricerange.attr("disabled","disabled");
                }else{
                    pricerange.removeAttr("disabled");
                }

            });

            $('.featured .owl-carousel').owlCarousel({
                loop:true,
                responsiveClass:true,
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false
                    },
                    600:{
                        items:2,
                        nav:false
                    },
                    992:{
                        items:2,
                        nav:false,
                    },
                    1200:{
                        items:4,
                        nav:false,
                    }
                }
            })

            $('.blogposts .owl-carousel').owlCarousel({
                loop:true,
                responsiveClass:true,
                autoplay:false,
                responsive:{
                    0:{
                        items:1,
                        nav:false
                    },
                    600:{
                        items:2,
                        nav:false
                    },
                    992:{
                        items:2,
                        nav:false,
                    },
                    1200:{
                        items:3,
                        nav:false,
                    }
                }
            })

            if(window.location.hash) {
                var maker = getUrlVars()["maker"];
                var range = getUrlVars()["range[]"];

                $('#maker')
                    .val(maker)
                    .trigger('change');
                var data = $('#homepageCarSearch').serialize();
                $.ajax(
                    {
                        type: "get",
                        data: data,
                        url: "/home/manusearch",
                        success: function (html) {
                            $('#range').html(html);
                            $('#range')
                                .val(range)
                                .trigger('change');
                        }
                    }
                )
            }

            $('body').on('change', '#range', function () {
                var data = $('#homepageCarSearch').serialize() + "&e=1";
                window.location.hash = data;

                $.ajax(
                    {
                        type: "get",
                        data: data,
                        url: "/home/manusearch",
                        success: function (html) {
                            $('.extraFields').html(html);
                        }
                    }
                )

            });

            $('body').on('change', '#bodystyle', function () {
                var data = $('#homepageCarSearch').serialize() + "&e=1";
                window.location.hash = data;

                $.ajax(
                    {
                        type: "get",
                        data: data,
                        url: "/home/manusearch",
                        success: function (html) {
                            $('.extraFields').html(html);
                        }
                    }
                )

            });

            $('body').on('change', '#transmission', function () {
                var data = $('#homepageCarSearch').serialize() + "&e=1";
                window.location.hash = data;

                $.ajax(
                    {
                        type: "get",
                        data: data,
                        url: "/home/manusearch",
                        success: function (html) {
                            $('.extraFields').html(html);
                        }
                    }
                )
            });
            $('body').on('change', '#fuel', function () {
                var data = $('#homepageCarSearch').serialize() + "&e=1";
                window.location.hash = data;

                $.ajax(
                    {
                        type: "get",
                        data: data,
                        url: "/home/manusearch",
                        success: function (html) {
                            $('.extraFields').html(html);
                        }
                    }
                )

            });
            $('body').on('change', '#engineSize', function () {
                var data = $('#homepageCarSearch').serialize() + "&e=1";
                window.location.hash = data;

                $.ajax(
                    {
                        type: "get",
                        data: data,
                        url: "/home/manusearch",
                        success: function (html) {
                            $('.extraFields').html(html);
                        }
                    }
                )

            });

            $('body').on('change', '#maker', function () {
                var data = $('#homepageCarSearch').serialize();
                // window.location.hash = $('#maker').val();
                window.location.hash = data;
                $.ajax(
                    {
                        type: "get",
                        data: data,
                        url: "/home/manusearch",
                        success: function (html) {
                            $('#range').html(html);
                        }
                    }
                )
            });

            // catch the form on submit to manipulate data to work with listing pages
            $('#homepageCarSearch').submit(function (e) {
                e.preventDefault();
                // check where to post the form to
                var financeType = $('input[name=finance-type]:checked').val();
                @if($database == 'LCV')
                    if (financeType == 'p') {
                        var location = '/personal-vans';
                    } else if (financeType == 'b') {
                        var location = '/business-vans';
                    }
                @else
                    if (financeType == 'p') {
                        var location = '/personal';
                    } else if (financeType == 'b') {
                        var location = '/business';
                    }
                @endif

                // Build form
                var form = '';
                var maker = $('#maker option:selected').val();
                if (maker) {
                    var form = form + "<input type='hidden' name='maker[]' value='" + maker + "'>";
                }
                // get selected range if one
                var range = $('#range option:selected').val();
                if (range) {
                    var form = form + "<input type='hidden' name='range[]' value='" + range + "'>";
                }
                // selected bodystyle if one
                var bodystyle = $('#bodystyle option:selected').val();
                if (bodystyle) {
                    var form = form + "<input type='hidden' name='bodystyle[]' value='" + bodystyle + "'>";
                }
                // selected transmission if one
                var transmission = $('#transmission option:selected').val();
                if (transmission) {
                    var form = form + "<input type='hidden' name='transmission[]' value='" + transmission + "'>";
                }

                // selected transmission if one
                var engineSize = $('#engineSize option:selected').val();
                if (engineSize) {
                    if(engineSize == '0') {} else {
                        var form = form + "<input type='hidden' name='engineSize[]' value='" + engineSize + "'>";
                    }
                }

                // selected bodystyle if one
                var fuel = $('#fuel option:selected').val();
                if (fuel) {
                    var form = form + "<input type='hidden' name='fuel[]' value='" + fuel + "'>";
                }
                // // prices
                var priceMin = $('#pricemin option:selected').val();
                var priceMax = $('#pricemax option:selected').val();

                if(priceMin == '') {
                    priceMin = 0;
                }

if(priceMin == '' && priceMax == '') {

                }else {
                    var form = form + "<input type='hidden' name='pricemin' value='" + priceMin + "'>";
                    var form = form + "<input type='hidden' name='pricemax' value='" + priceMax + "'>";
                }
                // trigger homepage form submit
                //var form = form + "<input type='hidden' name='homepageSearch' value='1'>";

                // Submit form
                $('<form action="' + location + '" method="get">' + form + '</form>').appendTo('body').submit();

            });
        });

        @role('admin')
        $(function () {
            $("body").on('submit', '.update-featured-form', function (e) {
                e.preventDefault();
                var formInfo = $(this).serialize();
                var capId = $(this).find('input[name="cap-id"]').val();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'get',
                    url: '{{route('update-featured')}}',
                    data: formInfo,
                }).done(function(response) {
                    $('div.success-' + capId).html('Featured Updated').delay(9000).fadeOut();
                    location.reload();
                }).fail(function() {

                });
            });
        });
        @endrole
    </script>

@endpush
@section('template_linked_css')
<style>
   .cc-banner-embed iframe {
        width: 100%;
        height: 84px;
    }
   .cc-banner-embed {
       width: 100%;
       height: 84px;
   }
   .blogimg {
       max-height: 200px;
       overflow: hidden
   }
   .blogtitle {
       background: #222b38;
       padding: 8px;
       text-align: center;
       color: #fff;
       min-height: 79px;
   }
    .car {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        margin: 0 auto;
        position: relative;
        width: 600px;
        height: 271px;
        overflow: hidden;
        margin-top: 114px
    }

    .carbody {
        animation: carmove 3.1s infinite linear;
        @if($database == 'LCV')
            background: url('/images/van-homepage.png');
        @else
            background: url('/images/golf.png');
        @endif
        background-size: cover;
        height: 214px;
        position: relative;
        width: 600px;
        z-index: 125;
    }

   @if($database == 'LCV')
        .weel {
           animation: wheel 0.7s infinite linear;
           background: url('/images/wheel_van.png');
           height: 90px;
           position: absolute;
           top: 50%;
           width: 89px;;
           z-index: 200;
        }
        .weel1 {
            left: 100px;
        }

        .weel2 {
            left: 434px;
        }
   @else
       .weel {
           animation: wheel 0.7s infinite linear;
           background: url('/images/wheel.png');
           height: 100px;
           position: absolute;
           top: 42%;
           width: 99px;;
           z-index: 200;
       }
        .weel1 {
           left: 68px;
        }

        .weel2 {
            left: 441px;
        }
   @endif


    /*animations*/
    @keyframes carmove {
        0% {
            transform: translateY(0px);
        }
        25% {
            transform: translateY(1px)
        }
        29% {
            transform: translateY(2px)
        }
        33% {
            transform: translateY(3px)
        }
        47% {
            transform: translateY(0px)
        }
        58% {
            transform: translateY(1px)
        }
        62% {
            transform: translateY(2px)
        }
        66% {
            transform: translateY(3px)
        }
        75% {
            transform: translateY(1px)
        }
        100% {
            transform: translateY(0px)
        }
    }

    @keyframes wheel {
        0% {
            transform: rotate(0deg)
        }
        100% {
            transform: rotate(-359deg)
        }
    }

    body {
        -webkit-animation: color-fade 10s infinite;
        -moz-animation: color-fade 10s infinite;
        animation: color-fade 10s infinite;
    }

    .featured {
        background: #f8f9fa;
    }

    .alc-process li {
        width: 100px;
        height: 30px;
        /*
                padding: 60px 0 0 0;
        */
        margin: 0;
        text-align: center;
        position: absolute;
        left: 0;
        top: 110px;
    }

    .alc-process li img {
        width: 50px;
        height: auto;
        display: block;
        margin: auto;
    }

    .alc-process li + li {
        left: 120px;
        top: 26px;
    }

    .alc-process li + li img {
        width: 58px;
        height: auto;
        display: block;
        margin: auto;
    }

    .alc-process li + li + li {
        left: 285px;
        top: 6px;
    }

    .alc-process li + li + li img {
        width: 58px;
        height: auto;
        display: block;
        margin: auto;
    }

    .alc-process li + li + li + li {
        left: 442px;
        top: 0px;
        width: 140px;
    }

    .alc-process li + li + li + li img {
        width: 58px;
        height: auto;
        display: block;
        margin: auto;
    }

    .alc-process ol {
        padding: 0;
        margin: 0;
    }

    .alc-process ol li span, .alc-process ol li p {
        color: #fff;
        font-weight: 900;
    }

    .alc-process ol li {
        color: #fff;
        font-weight: 900;
        list-style-type: none;
        width: 119px;
    }
   @media screen and (max-width:1028px) and (min-width:768px) {

       .cc-banner-embed iframe {

           height: 110px;
       }

       .cc-banner-embed {

           height: 110px;
       }
   }
   @media screen and (max-width:768px) {
       .cc-banner-embed iframe {

           height: 135px;
       }
       .cc-banner-embed {

           height: 135px;
       }
   }

    @media screen and (max-width:768px) and (min-width:500px) {
        @if($database == 'LCV')
            .weel {
                top: 46%!important;
                width: 55px!important;
                height: 55px!important;
            }
            .weel1 {
                left: 80px;
            }
            .weel2 {
                left: 327px;
            }
        @else
            .weel1 {
                left: 51px;
            }
            .weel2 {
                left: 327px;
            }
        @endif


        .weel {
            height: 73px;
            position: absolute;
            top: 39%;
            width: 73px;
            z-index: 200;
            background-size: cover;
        }
        .car{
            height: 218px;
            width: 444px;
        }
    }
    @media only screen and (max-width: 500px) {


        @if(isset($database) && $database == 'LCV')

            .weel1 {
                left: 55px;
            }
            .weel2 {
                left: 234px;
            }
            .alc-process ol {
                display:none;
            }
            .weel {
                height: 43px;
                position: absolute;
                top: 52%;
                width: 43px;
                background-size: cover;
            }
        @else
            .weel1 {
                left: 38px;
            }
            .weel2 {
                left: 237px;
            }
            .alc-prcess ol {
                display: none;
            }
            .weel {
                height: 50px;
                position: absolute;
                top: 46%;
                width: 50px;
                background-size: cover;
            }
        @endif
        .car{
            margin-top:24px;
            height:140px !important;
            width: 330px !important;
        }

    }

</style>
@endsection
