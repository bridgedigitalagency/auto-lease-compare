@extends('layouts.app')
@section('template_title') {{$single_quote->maker}} {{$single_quote->range}} {{$single_quote->derivative}} Lease Deal By {{$single_quote->deal_id}} @endsection
@section('template_description')
    <meta name="description" content="{{$single_quote->maker}} {{$single_quote->range}} {{$single_quote->derivative}} only £{{$single_quote->monthly_payment}}P/M, All Brand New Cars, Road Tax &amp; Manufacturers Warranty Included">
@endsection
@section('extra_meta_tags')
    <meta name="robots" content="noindex">
@endsection
@push('scripts')

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

    <script src="/owlcarousel/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script type="text/javascript" src="/js/lightbox.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/css/lightbox.css">
    <script>

        (function($){

            window.deals = [];

            @foreach($customiseDeal as $otherDeals)
            deals.push(
                {
                    dealid: {{$otherDeals->id}},
                    capid: {{$otherDeals->cap_id}},
                    database: "{{$single_quote->database}}",
                    deposit_value: {{$otherDeals->deposit_value}},
                    deposit_months: {{$otherDeals->deposit_months}},
                    term: {{$otherDeals->term}},
                    mileage: {{$otherDeals->annual_mileage}},
                    document_fee: {{$otherDeals->document_fee}},
                    monthly_payment: {{$otherDeals->monthly_payment}},
                    finance_type: "{{$otherDeals->finance_type}}",
                    total_initial_cost: "{{$otherDeals->total_initial_cost}}",
                    average_cost: "{{$otherDeals->average_cost}}",
                }
            );
            @endforeach

            $('#enquiryForm').on('submit', function(e) {
                var name = $('#name').val();
                var n = name.split(' ');
                var fname = n[0];
                if (n[1] == null) {
                    var lname = '';
                }else{
                    var lname = n[1];
                }
                var email = $('#email').val();
                var phone = $('#phone_number').val();

                var url = "https://secure.uk.rspcdn.com/xprr/red/PID/2671/SID/?fname=" + fname + "&lname=" + lname + "&email=" + email + "&phone=" + phone;

                if($('#creditcheck').is(":checked")) {
                    window.open(url);
                }

            });

            @if( app('request')->input('enqErr') == '1')
            $('#enquireTodayButton').click();
            @endif


            if(window.innerWidth <= 800) {

                // On modal display action for enquiry form
                $('#quotemodel').on('shown.bs.modal', function() {
                    $('.mobhide').toggle();
                    setTimeout(function() {
                        $('body').addClass('modal-open');
                    }, 150);
                });

                // On modal hide action for enquiry form
                $('#quotemodel').on('hidden.bs.modal', function() {
                    $('.mobhide').toggle();
                });

                // On modal display for call provider
                $('#quotecall').on('shown.bs.modal', function() {
                    $('.mobhide').toggle();
                });
                // On modal hide for call provider
                $('#quotecall').on('hidden.bs.modal', function() {
                    $('.mobhide').toggle();
                });

                // Setup thumbnail slider
                $('.quoteUspsHeaderMobile').slick({
                    autoplay        : true,
                    arrows          : false,
                    dots            : false,
                    draggable       : true,
                    slidesToShow    : 1,
                    slidesToScroll  : 1,
                    touchMove       : true,
                    swipe           : true,
                    swipeToSlide    : true,
                    infinite        : true,
                    prevArrow       : '',
                    nextArrow       : '',
                    mobileFirst     : true
                });

            }else{


                // On modal display action for enquiry form
                $('#quotemodel').on('shown.bs.modal', function() {
                    setTimeout(function() {
                        $('body').addClass('modal-open');
                    }, 150);
                });
            }




            $('#priceHistoryContainer').append('<div class="modalLoading"><div class="lds-dual-ring"><img src=\'/images/loadingWheel.png\'></div></div>');
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'get',
                url: 'https://pricehistory.autoleasecompare.com/pr/<?php echo $single_quote->cap_id;?>/<?php echo $single_quote->annual_mileage;?>/<?php echo $single_quote->term;?>/<?php echo $single_quote->deposit_value;?>/<?php echo $single_quote->finance_type;?>/<?php echo($single_quote->database); ?>'
            }).done(function(html) {
                $('#priceHistory').html(html);
                $('.modalLoading').hide();
            }).fail(function() {

            });

            // Setup thumbnail slider
            $('.productImages').slick({
                autoplay        : false,
                arrows          : true,
                dots            : false,
                draggable       : true,
                slidesToShow    : 1,
                slidesToScroll  : 1,
                touchMove       : true,
                swipe           : true,
                swipeToSlide    : true,
                infinite        : true,
                prevArrow       : '.slick-prev',
                nextArrow       : '.slick-next',
                mobileFirst     : true,
                asNavFor: '.productImagesThumbs',
            });
            $('.productImagesThumbs').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                variableWidth: true,
                asNavFor: '.productImages',
                dots: false,
                arrows: false,
                focusOnSelect: true

            });
            $('.productImages').slickLightbox({
                src: 'href',
                itemSelector: 'div a'
            });

            function updateTextArea() {
                var theMessage = $("textarea#message").val();
                var allVals = [];
                var allVals2 = [];
                var allVals3 = [];

                $('#ChooseColours :checked').each(function(i) {
                    allVals.push((i!=0?"\r\n":"\r\n\r\nMy Preferred Colours Are:\r\n") + $(this).attr('rel'));
                });
                $('.tech :checked').each(function(i) {
                    allVals2.push((i!=0?"\r\n":"\r\n\r\nMy Preferred Options Are:\r\n") + $(this).attr('rel'));
                });
                // $('.maintenance2 :checked').each(function(i) {
                //     allVals3.push((i!=0?"\r\n":"\r\n\r\n")+ $(this).attr('rel'));
                // });

                if(allVals.length == 0 && allVals2.length == 0 && allVals3.length == 0) {
                    $('.callExtrasTitle').hide();
                }else{
                    $('.callExtrasTitle').show();
                    $('#callExtras').html("<strong>Selected extras:</strong><ul id='allval1'></ul><ul id='allval2'></ul><ul id='allval3'></ul>");

                    $listSelector = $("#allval1");
                    $.each(allVals, function(i, obj) {
                        $listSelector.append("<li>" + obj +"</li>");
                    });

                    $listSelector2 = $("#allval2");
                    $.each(allVals2, function(i, obj) {
                        $listSelector2.append("<li>" + obj +"</li>");
                    });

                    $listSelector3 = $("#allval3");
                    $.each(allVals3, function(i, obj) {
                        $listSelector3.append("<li>" + obj +"</li>");
                    });


                }

                $('#message').val("Hello, I'm interested in this deal, please send me a personalised quote." + allVals + allVals2 + allVals3).attr('rows',allVals.length) ;

            }
            $(function() {
                $('#ChooseColours input').click(updateTextArea);
                $('.tech input').click(updateTextArea);
                // $('.maintenance2 input').click(updateTextArea);

                updateTextArea();
            });

        })(jQuery);

        (function($){
            $(".owl-carousel").owlCarousel({
                margin:10,
                loop:true,
                items:4,
                dots: true,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false
                    },
                    600:{
                        items:2,
                        nav:false
                    },
                    992:{
                        items:2,
                        nav:false,

                    },
                    1200:{
                        items:4,
                        nav:false,

                    }
                }

            });
        })(jQuery);
    </script>
    <script src="/js/quote.js"></script>
@endpush

@section('content')
    @include('quote_views/components/quote-modals')
    <section class="smallnav">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="bread">
                        @if($single_quote->finance_type =='P')
                            @if(isset($single_quote->breadcrumb))
                                @if($single_quote->breadcrumb == 'H')
                                    <a href="/">Car Lease Comparison (Home)</a> / <a href="/hybrid">Electric & Hybrid Car Leasing</a> / <a href="/hybrid?maker[]={{$single_quote->maker}}">{{$single_quote->maker}}</a> / <a href="/hybrid?maker[]={{$single_quote->maker}}&range[]={{$single_quote->range}}">{{$single_quote->range}}</a> / {{$single_quote->derivative}}
                                @elseif($single_quote->breadcrumb == 'PERFORMANCE')
                                    <a href="/">Car Lease Comparison (Home)</a> / <a href="/performance">Performance Car Leasing</a> / <a href="/performance?maker[]={{$single_quote->maker}}">{{$single_quote->maker}}</a> / <a href="/performance?maker[]={{$single_quote->maker}}&range[]={{$single_quote->range}}">{{$single_quote->range}}</a> / {{$single_quote->derivative}}
                                @elseif($single_quote->breadcrumb == 'PV')
                                    <a href="/vans">Van Lease Comparison (Home)</a> / <a href="/personal-vans">Personal Van Leasing</a> / <a href="/personal-vans?maker[]={{$single_quote->maker}}">{{$single_quote->maker}}</a> / <a href="/personal-vans?maker[]={{$single_quote->maker}}&range[]={{$single_quote->range}}">{{$single_quote->range}}</a> / {{$single_quote->derivative}}
                                @endif
                            @else
                                @if($database == 'LCV')
                                    <a href="/vans">Van Lease Comparison (Home)</a> / <a href="/personal-vans">Personal Van Leasing</a> / <a href="/personal-vans?maker[]={{$single_quote->maker}}">{{$single_quote->maker}}</a> / <a href="/personal-vans?maker[]={{$single_quote->maker}}&range[]={{$single_quote->range}}">{{$single_quote->range}}</a> / {{$single_quote->derivative}}
                                @else
                                    <a href="/">Car Lease Comparison (Home)</a> / <a href="/personal">Personal Car Leasing</a> / <a href="/personal?maker[]={{$single_quote->maker}}">{{$single_quote->maker}}</a> / <a href="/personal?maker[]={{$single_quote->maker}}&range[]={{$single_quote->range}}">{{$single_quote->range}}</a> / {{$single_quote->derivative}}
                                @endif
                            @endif
                        @else
                            @if($database == 'LCV')
                                <a href="/vans">Van Lease Comparison (Home)</a> / <a href="/business-vans">Business Van Leasing</a> / <a href="/business-vans?maker[]={{$single_quote->maker}}">{{$single_quote->maker}}</a> / <a href="/business-vans?maker[]={{$single_quote->maker}}&range[]={{$single_quote->range}}">{{$single_quote->range}}</a> / {{$single_quote->derivative}}
                            @else
                                <a href="/">Car Lease Comparison (Home)</a> / <a href="/business">Business Car Leasing</a> / <a href="/business?maker[]={{$single_quote->maker}}">{{$single_quote->maker}}</a> / <a href="/business?maker[]={{$single_quote->maker}}&range[]={{$single_quote->range}}">{{$single_quote->range}}</a> / {{$single_quote->derivative}}
                            @endif

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('quote_views.components.uspsbanner')

    @role('admin')

    @if( app('request')->input('hot') == '1')
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success" role="alert">
                        This deal has been updated.
                    </div>
                </div>
            </div>
        </div>
    @endif
    <section id="adminMenu">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="http://dev.autoleasecompare.com/quote/enquiry/{{$uid}}/{{$database}}">View deal on dev site</a> <a href="/quote/enquiry/{{$single_quote->id}}/{{$database}}/delete">Delete deals for this dealer</a> | @if($single_quote->featured==1)<a href="/quote/enquiry/{{$single_quote->id}}/{{$database}}/feature">Remove this deal from featured</a> @else <a href="/quote/enquiry/{{$single_quote->id}}/{{$database}}/feature">Make this deal featured</a> @endif
                </div>
            </div>
        </div>
    </section>
    @endrole
{{--https://cdn.autoleasecompare.com/vehicleimages/LCV/std/0007d7fc-79d2-40f7-b830-a9357eb6b013--}}
    <section id="topSection">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    @if($single_quote->in_stock =='1')<div class="stocktag instock"><p>In Stock</p></div> @else <div class="stocktag"><p>Factory Order</p></div> @endif
                    <div id="photoContainer">

                        @if(count($capImages) > 1)
                            <div id="carousel" class="productImages">
                                @foreach($capImages as $image)
                                    <div>
                                        <a href="{{str_replace('big', 'large', $image)}}">
                                            <img src="{{$image}}" alt="{{$single_quote->maker}} {{$single_quote->range}} {{$single_quote->derivative}}">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                            <p class="illustag" style="color: #000">Images for illustration only, various colours available</p>
                            <p class="slick-arrow left slick-prev"><i class="fa fa-chevron-left"></i></p>
                            <p class="slick-arrow right slick-next"><i class="fa fa-chevron-right"></i></p>
                            <div id="carouselThumbs" class="productImagesThumbs">
                                @foreach($capImages as $image)
                                    <div>
                                        <img src="{{str_replace('big', 'std', $image)}}" width="100" alt="{{$single_quote->maker}} {{$single_quote->range}} {{$single_quote->derivative}}">
                                    </div>
                                @endforeach

                            </div>
                        @else
                            <div>
                                @foreach($capImages as $image)
                                    <img src="{{$image}}" alt="{{$single_quote->maker}} {{$single_quote->range}} {{$single_quote->derivative}}">
                                @endforeach
                            </div>
                        @endif

                    </div>

                    <div class="row">
                        <div class="col-6 text-left">

                            <a href="#" data-id="{{$single_quote->id}}" data-database="{{$single_quote->database}}" data-toggle="modal" data-target="#modelgarage" class="dealTopLinks" id="addToGarabeButton">
                                <i class="fa fa-car"></i> Save to garage</a>

                        </div>
                        <div class="col-6 text-right">
                            <a data-toggle="modal" data-target="#socialModalCenter" class="dealTopLinks"><i class="fa fa-share"></i> Share this deal</a>
                        </div>
                    </div>

                </div>

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12 dealbox mb-3 vehicleMainInfo">
                            <h1>{{$single_quote->maker}} {{$single_quote->range}}<br/><span class="smallfont">{{$single_quote->derivative}}</span></h1>
                            @if(isset($single_quote->bodystyle) && !is_null($single_quote->bodystyle)){{$single_quote->bodystyle}} | @endif @if(isset($single_quote->doors) && !is_null($single_quote->doors)){{$single_quote->doors}} Doors | @endif {{$single_quote->fuel}} | {{$single_quote->transmission}}
                        </div>

                        @if(!is_null($single_quote->insurance) && $single_quote->insurance > 0)
                            <div class="col-6 dealbox mainPrice mb-2 mt-2 ">
                        @else
                            <div class="col-12 dealbox mainPrice mb-2 mt-2 ">
                        @endif
                            <h3>
                                <strong style="color: #fc5185;">
                                    <span class="monthly_payment_text" data-bind="orignal_monthly_rental">£<?php echo $single_quote->monthly_payment;?></span>
                                </strong>
                                <small style="font-size: 12px;">Per Month
                                    @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</small>
                            </h3>
                        </div>
                        @if(!is_null($single_quote->insurance) && $single_quote->insurance > 0)
                        <div class="col-6 dealbox insuranceText mb-2 mt-2 ">
                            <p class="smallfont">Includes 1 Year’s Free Insurance</p>
                        </div>
                        @endif
                    </div>
                    <div class="row">

                        @include('quote_views.components.priceinfo')

                    </div>
                    <div class="infoButtons">
                        @include('quote_views.components.infobuttons')
                    </div>

                    <div class="row" style="display: none;">
                        <div class="col-md-12">
                            <hr>
                            <div class="row justify-content-center">
                                @if(isset($dealerInfoJSON->trustpilot))
                                    @if($trustpilotRatings->businessUnit->trustScore > 0)
                                        <div class="col-3">
                                            @if(isset($dealerInfoJSON->logo))
                                                <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-2" alt="{{$dealerInfoJSON->name}}">
                                            @endif
                                        </div>
                                        <div class="col-3">
                                            <img src="/images/trustpilot/trustpilot-{{$trustpilotRatings->businessUnit->stars}}.png" alt="{{$trustpilotRatings->businessUnit->stars}} on Trustpilot" width="100"  class="img-fluid"><br/>
                                            <img src="/images/trustpilot/trustpilot-logo.svg" alt="Trustpilot" width="100"  class="img-fluid"><br/>
                                        </div>
                                        <div class="col-md-6 text-center">
                                            <span class="tp-text">This Lease Company has a trust score of {{$trustpilotRatings->businessUnit->trustScore}} based on {{$trustpilotRatings->businessUnit->numberOfReviews->total}} Reviews</span>
                                        </div>
                                    @else
                                        @if(isset($dealerInfoJSON->logo))
                                            <div class="col-12 text-center d-sm-block d-xs-block d-md-none d-lg-none d-xl-none">
                                                <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-2" alt="{{$dealerInfoJSON->name}}" style="max-width: 200px;">
                                            </div>
                                            <div class="col-12 text-right d-none d-md-block">
                                                <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-2" alt="{{$dealerInfoJSON->name}}" style="max-width: 200px;">
                                            </div>
                                        @endif
                                    @endif
                                @else
                                    @if(isset($dealerInfoJSON->logo))
                                        <div class="col-12 text-center d-sm-block d-xs-block d-md-none d-lg-none d-xl-none">
                                            <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-2" alt="{{$dealerInfoJSON->name}}" style="max-width: 200px;">
                                        </div>
                                        <div class="col-12 text-right d-none d-md-block">
                                            <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-2" alt="{{$dealerInfoJSON->name}}" style="max-width: 200px;">
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="calltoactions">
        <div class="container">
            <div id="customiseDeal" class="row">
                <div class="col-md-4 customisationOptions">
                    @include('quote_views.components.customise-lease')
                </div>
                <div class="col-md-4 optionalExtras">
                    <h5 class="pinkHeading"><span data-toggle="tooltip" data-placement="top" data-html="true" title="Some of these optional extras & colours will come at an additional cost, these can be discussed with the Deal Provider and added to your quote.<br><br>Note: some of these options will not be available for in stock deals. <br><br> <b>Select your preferred optional extras & colours to add to your enquiry.</b>"> <i class="fa fa-info-circle"></i> Optional Extras & Colours</span></h5>

                    <div class="d-none d-lg-flex row buttonpad optionalButtons">
                        <div class="col-md-12"><button data-toggle="modal" class="button b2 pointer specbuttons " data-target="#modeloptions"><i class="fa fa-info-circle"></i> Select Optional Extras</button></div>
                        <div class="col-md-12"><button data-toggle="modal" class="button b2 pointer specbuttons" data-target="#coloursmodel"><i class="fa fa-info-circle"></i> Select Colour</button></div>
                    </div>
                    <div class="d-lg-none">
                        <a style="display: inline-block; width: 100%; margin-bottom: 20px;" data-toggle="modal" class="button b2 pointer specbuttons text-center" data-target="#modeloptions"><i class="fa fa-info-circle"></i> Select Optional Extras</a>
                        <a style="display: inline-block; width: 100%; margin-bottom: 20px;" data-toggle="modal" class="button b2 pointer specbuttons text-center" data-target="#coloursmodel"><i class="fa fa-info-circle"></i> Select Colour</a>
                    </div>

                </div>
                <div class="col-md-4">

                    <div class="hideOnMobile">
                        <div class="col-md-12 text-right">
                            <h5 class="pinkHeading"> Deal Information</h5>
                        </div>
                        <div class="row buttonpad text-right">


                            <div class="col-md-12">
                                <button type="button" data-toggle="modal" data-target="#dealInformation" class="button b2"><i class="fa fa-info-circle"></i> Deal Information</button>
                            </div>
                            <div class="col-md-12">
                                <button type="button" class="button b2" data-toggle="modal" data-target="#dealermodal">
                                    <i class="fa fa-info-circle"></i>  About Provider
                                </button>
                            </div>

                        </div>
                    </div>

                </div>


            </div>


        </div>
    </section>

    <section id="vehicleInfo">
        <div class="container pt-1">
            <div class="row">
                <div class="col-md-12 techSpecs">
                    <h2 class="pinkHeading mb-4 mt-4">Technical Specs, Equipment & Vehicle Review</h2>
                    <div class="d-none d-md-flex row buttonpad">
                        <div class="col-md-4"><button data-toggle="modal" class="button b2 pointer specbuttons" data-target="#modeldata"><i class="fa fa-wrench"></i> Technical Data</button></div>
                        <div class="col-md-4"><button data-toggle="modal" class="button b2 pointer specbuttons" data-target="#modelstandard"><i class="fa fa-car"></i> Standard Equipment</button></div>
                        <div class="col-md-4"><button data-toggle="modal" class="button b2 pointer specbuttons" data-target="#modelreview"><i class="fa fa-edit"></i> Vehicle Review & Video</button></div>
                    </div>
                    <div class="d-md-none">
                        <a style="display: inline-block; width: 100%; margin-bottom: 20px;" data-toggle="modal" class="button b2 pointer specbuttons text-center" data-target="#modeldata"><i class="fa fa-wrench"></i> Technical Data</a>
                        <a style="display: inline-block; width: 100%; margin-bottom: 20px;" data-toggle="modal" class="button b2 pointer specbuttons text-center" data-target="#modelstandard"><i class="fa fa-car"></i> Standard Equipment</a>
                        <a style="display: inline-block; width: 100%; margin-bottom: 20px;" data-toggle="modal" class="button b2 pointer specbuttons text-center" data-target="#modelreview"><i class="fa fa-edit"></i> Vehicle Review & Video</a>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id="reviews" style="background:#fff;" >
        <div class="container pb-5">
            <div class="row">
                <div class="col-md-6 priceHistoryRatings">
                    <h5 class="pinkHeading mb-4 mt-5"><span data-toggle="tooltip" data-placement="top" data-html="true" title="Prices can change on a deal for many reasons, including the profile (annual mileage initial deposit & contract length) of the deal you have chosen. We track the prices of these so you can see if your getting a good deal. Act fast as good value deals are normally on limited stock and sell out quickly!"> <i class="fa fa-info-circle"></i> Price History</span></h5>
                    <div id="priceHistoryContainer">
                        <div id="priceHistory"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h2 class="pinkHeading mt-5 mb-4">Rating Breakdown</h2>
                    <div class="row" id="reviews">
                        @if($vehicleRating === false)
                            <div class="col-12">
                                <p>We don't currently have a rating breakdown for this vehicle, check back soon!</p>
                            </div>
                        @else
                            @include('partials/quote-scores')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>


    @if($RelatedModelQuery)
        @include('quote_views.components.related-models')
    @endif



    <div class="container mobhide mobileFooterCTA">
        <div class="stocktag"><p> @if($single_quote->in_stock =='1') In Stock @else Factory order @endif</p></div>

        <div class="row">
            <div class="col">
                <p>
                    <span class="topBlock">
                        <span class="monthly_price"><span id="originalTerm2" class="term_text_minus_initial"><?php echo $single_quote->term - 1 ?></span> Monthly Payments of
                            <strong style="color: #fc5185;">
                                <span class="monthly_payment_text" data-update="orignal_monthly_rental">£<?php echo number_format((float)$single_quote->monthly_payment, 2, '.','');?></span>
                            </strong>
                        </span><br>
                        <span class="average_cost_text">£<?php echo number_format((float)$single_quote->average_cost, 2, '.','');?></span> Av. Monthly Cost (Inc Total Upfront)
                    </span>

                    @if(isset($dealerInfoJSON->logo))
                        <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-2" alt="{{$dealerInfoJSON->name}}" align="right">
                    @endif
                    <span style="font-weight:bold;">Initial Payment:</span> <span id="depoval" class="initial_payment_price">£<?php echo number_format((float)$single_quote->deposit_months, 2, '.','');?></span><br>
                    <span style="font-weight:bold;">Document Fee:</span> £<?php echo number_format((float)$single_quote->document_fee, 2, '.','');?><br>
                    <span style="font-weight:bold;">Total Upfront:</span> <span class="total_initial_cost_text">£<?php echo number_format((float)($single_quote->document_fee + $single_quote->deposit_months), 2, '.','');?></span><br>
                </p>

            </div>
        </div>

        <div class="row ctaButtons">
            <div class="col-6 text-center pb-2">
                <a class="button b3" data-toggle="modal" data-target="#quotemodel"><i class="fa fa-envelope"></i> Enquire Today</a>
            </div>
            <div class="col-6 text-center pb-2">
                @if(isset($openingTimes) && is_array($openingTimes))
                    @if($openingTimes['status']==1)
                        <a class="button b6 mobCallProv" onclick="ga('send', {hitType: 'event',eventCategory: 'CallNow',eventAction: 'ButtonClick',eventLabel: 'Call'}" data-toggle="modal" data-target="#quotecall"><i class="fa fa-phone"></i> Call For Quote</a>
                    @endif

                    @if($openingTimes['status']==2)
                        <a class="button b6 mobCallProv"><i class="fa fa-phone"></i> Calls reopen {{date('g:ia', strtotime($openingTimes['time']))}} today</a>
                    @endif

                    @if($openingTimes['status']==3)
                        <a class="button b6 mobCallProv"><i class="fa fa-phone"></i> Calls reopen on {{date('g:ia', strtotime($openingTimes['time']))}} {{$openingTimes['day']}}</a>
                    @endif
                @else
                    <a class="button b6 mobCallProv" onclick="ga('send', {hitType: 'event',eventCategory: 'CallNow',eventAction: 'ButtonClick',eventLabel: 'Call'}" data-toggle="modal" data-target="#quotecall"><i class="fa fa-phone"></i> Call For Quote</a>
                @endif
            </div>
        </div>
    </div>
    @include('partials/garage-modals')
@endsection
@section('template_linked_css')
    @php
        $rand = rand(0, 50000);
    @endphp
    <link rel="stylesheet" type="text/css" href="/css/quote.css?s={{$rand}}">
    <style>
        .quotebannerbottom {
            text-align: center;
            padding: 0.8rem 0;
        }

        #enquiryForm .button {
            padding: .5rem 4rem;
        }
        .techSpecs {
            order: 2;
        }
        .priceHistoryRatings {
            order: 1;
        }
        div#lhnHocButton.lhnround {
            z-index: 2;
            bottom: 80px;
        }
        @media screen and (max-width: 500px) {
            .modal-backdrop {
                z-index: 10!important;
            }
        }
        @media screen and (max-width: 800px) {
            .techSpecs {
                order: 1;
            }
            .priceHistoryRatings {
                order: 2;
            }
        }

        #priceHistory {
            width: 100%;
            display: block;

        }
        .modalLoading {
            position: absolute;
            top: 106px;
            left: 0;
            z-index: 1050;
            display: block;
            width: 100%;
            background: rgba(0, 0, 0, 0.5);
            height: 380px;
            outline: 0;
        }
        .plussign{
            margin-left:10px;
            color: #fc5185;
        }
        .lds-dual-ring {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -50px;
            margin-left: -50px;
            width: 106px;
            height: 106px;
            background: url(/images/loadingWheel.png) no-repeat;
            transform: none;
            -webkit-transform: none;
            -webkit-animation:spin 1s linear infinite;
            -moz-animation:spin 1s linear infinite;
            animation:spin 1s linear infinite;
        }
        @-moz-keyframes spin { 100% { -moz-transform: rotate(360deg); } }
        @-webkit-keyframes spin { 100% { -webkit-transform: rotate(360deg); } }
        @keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); } }


    </style>
@endsection
