<section id="related">
    <div class="container mt-5 pb-5">
        <div class="row">
            <div class="col-md-12">
                <h2>Similar Lease Deals</h2>
            </div>
        </div>
        @if(count($RelatedModelQuery) > 4)
            <div class="owl-carousel row owl-theme row-eq-height">
                @foreach($RelatedModelQuery as $related)
                    <div class="col">
                        @if($database == 'LCV')
                            <a href="/quote/enquiry/{{ $related->id }}/lcv" class="relative-link">
                                @else
                                    <a href="/quote/enquiry/{{ $related->id }}/cars" class="relative-link">
                                        @endif
                                        @php
                                            $external_link = "https://www.autoleasecompare.com/images/capimages/".$related->imageid."_3.JPG";
                                        @endphp

                                        @if (@getimagesize($external_link))
                                            <img src="{{$external_link}}" class="img-fluid" alt="Related Car" onerror="this.src='https://www.autoleasecompare.com/images/comingsooncar.jpg'">
                                        @else
                                            @if($database == 'LCV')
                                                <img src="/images/capimages/LCV/{{$related->imageid}}.jpg" class="img-fluid" alt="Related Car" onerror="this.src='https://www.autoleasecompare.com/images/comingsooncar.jpg'">
                                            @else
                                                <img src="/images/comingsooncar.jpg" class="img-fluid" alt="Related Car">
                                            @endif
                                        @endif

                                        <div class="relative">
                                            <span class="relative-title" style="min-height:70px;">{{ $related->maker }} {{ $related->range }} {{ $related->derivative }}</span>
                                            <span class="relative-price">From £{{ $related->monthly_payment }}</span>
                                        </div>
                                    </a>
                    </div>
                @endforeach
            </div>
        @else
            <div class="row row-eq-height">
                @foreach($RelatedModelQuery as $related)
                    <div class="col-6 col-sm-6 col-md-3 text-center">

                        @if($database == 'LCV')
                            <a href="/quote/enquiry/{{ $related->id }}/lcv" class="relative-link">
                                @else
                                    <a href="/quote/enquiry/{{ $related->id }}/cars" class="relative-link">
                                        @endif
                                        @php
                                            $external_link = "https://www.autoleasecompare.com/images/capimages/".$related->imageid."_3.JPG";
                                        @endphp

                                        @if (@getimagesize($external_link))
                                            <img src="{{$external_link}}" class="img-fluid" alt="Related Car" onerror="this.src='https://www.autoleasecompare.com/images/comingsooncar.jpg'">
                                        @else
                                            @if($database == 'LCV')
                                                <img src="/images/capimages/LCV/{{$related->imageid}}.jpg" class="img-fluid" alt="Related Car" onerror="this.src='https://www.autoleasecompare.com/images/comingsooncar.jpg'">
                                            @else
                                                <img src="/images/comingsooncar.jpg" class="img-fluid" alt="Related Car">
                                            @endif
                                        @endif

                                        <div class="relative">
                                            <span class="relative-title" style="min-height:70px;">{{ $related->maker }} {{ $related->range }} {{ $related->derivative }}</span>
                                            <span class="relative-price">From £{{ $related->monthly_payment }}</span>
                                        </div>
                                    </a>
                    </div>
                @endforeach
            </div>
        @endif

    </div>
</section>