<div class="col-12">
    <table class="table priceTable">
        <tr>
            <td class="priceTitle">
                <span data-toggle="tooltip" data-placement="top" title="This is the length of your lease agreement."> <i class="fa fa-info-circle"></i> Contract Length:</span>
            </td>
            <td class="pricePrice"><span id="originalTerm" class="term_text"><?php echo $single_quote->term ?></span> Months</td>

            <td class="priceTitle">
                <span data-toggle="tooltip" data-placement="top" title="This is your annual mileage allowance."> <i class="fa fa-info-circle"></i> Annual Mileage:</span>
            </td>

            <td class="pricePrice"><span class="annual_mileage_text"><?php echo $single_quote->annual_mileage ?></span></td>


        </tr>
        <tr>
            <td class="priceTitle infotooltip">
                <span data-toggle="tooltip" data-placement="top" title="The document fee is the administration fee which is charged by the leasing provider."> <i class="fa fa-info-circle"></i> Document Fee:</span>
            </td>
            <td class="pricePrice">
                £<span id="originalDocumentFee"><?php echo number_format((float)$single_quote->document_fee, 2, '.','');?></span>
            </td>

            <td class="priceTitle">
                <span data-toggle="tooltip" data-placement="top" title="The Total Upfront cost is the Initial Paymet + the Document Fee."> <i class="fa fa-info-circle"></i> Total Upfront:</span>
            </td>
            <td class="pricePrice"><span class="total_initial_cost_text">£<?php echo number_format((float)$single_quote->total_initial_cost, 2, '.','');?></span></td>


        </tr>
        <tr>
            <td class="priceTitle">
                <span data-toggle="tooltip" data-placement="top" title="<?php echo $single_quote->deposit_value ?> Months Upfront." class="initialPaymentTooltip"><i class="fa fa-info-circle"></i> Initial Payment:</span>
            </td>
            <td class="pricePrice">
                <span id="customised_price" class="initial_payment_price">£<?php echo $single_quote->deposit_months;?></span>
            </td>

            <td class="priceTitle">
                <span data-toggle="tooltip" data-placement="top" title="The average monthly cost is the Total Cost (inc. Initial Payment & Document Fee) divided by the length of the contract."><i class="fa fa-info-circle"></i> Average Monthly Cost:</span>
            </td>
            <td class="pricePrice"><span class="average_cost_text">£<?php echo number_format((float)$single_quote->average_cost, 2, '.','');?></span></td>
        </tr>
    </table>


    <div class="mobilePriceTable d-none">
        <div class="row">
            <div class="col-6 priceTitle">
                <span data-toggle="tooltip" data-placement="top" title="This is the length of your lease agreement."> <i class="fa fa-info-circle"></i> Contract Length:</span>
            </div>
            <div class="col-6 pricePrice">
                <span id="originalTerm" class="term_text"><?php echo $single_quote->term ?></span> Months
            </div>
        </div>
        <div class="row">
            <div class="col-6 priceTitle">
                <span data-toggle="tooltip" data-placement="top" title="This is your annual mileage allowance"> <i class="fa fa-info-circle"></i> Annual Mileage:</span>
            </div>
            <div class="col-6 pricePrice">
                <span class="annual_mileage_text"><?php echo $single_quote->annual_mileage ?></span>
            </div>
        </div>
        <div class="row">
            <div class="col-6 priceTitle">
                <span data-toggle="tooltip" data-placement="top" title="<?php echo $single_quote->deposit_value ?> Months Upfront" class="initialPaymentTooltip"><i class="fa fa-info-circle"></i> Initial Payment:</span>
            </div>
            <div class="col-6 pricePrice">
                <span id="customised_price" class="initial_payment_price">£<?php echo $single_quote->deposit_months;?></span>
            </div>
        </div>

        <div class="row">
            <div class="col-6 priceTitle">
                <span data-toggle="tooltip" data-placement="top" title="The document fee is the administration fee which is charged by the leasing provider."> <i class="fa fa-info-circle"></i> Document Fee:</span>
            </div>
            <div class="col-6 pricePrice">
                £<span id="originalDocumentFee"><?php echo number_format((float)$single_quote->document_fee, 2, '.','');?></span>
            </div>
        </div>

        <div class="row">
            <div class="col-6 priceTitle">
                <span data-toggle="tooltip" data-placement="top" title="The Total Upfront cost is the Initial Payment + the Document Fee"> <i class="fa fa-info-circle"></i> Total Upfront:</span>
            </div>
            <div class="col-6 pricePrice">
                <span class="total_initial_cost_text">£<?php echo number_format((float)$single_quote->total_initial_cost, 2, '.','');?></span>
            </div>
        </div>

        <div class="row">
            <div class="col-6 priceTitle">
                <span data-toggle="tooltip" data-placement="top" title="The Average Monthly Cost is the Total Cost divided by the length of the contract."><i class="fa fa-info-circle"></i> Average Monthly Cost:</span>
            </div>
            <div class="col-6 pricePrice">
                <span class="average_cost_text">£<?php echo number_format((float)$single_quote->average_cost, 2, '.','');?></span>
            </div>
        </div>

    </div>
</div>
