<div class="card">
    <div class="card-header" id="heading{{str_replace([' ','/'], ['-','-'], $k)}}" data-toggle="collapse" data-target="#collapse{{str_replace([' ','/'], ['-','-'], $k)}}" aria-expanded="false">
        <h5 class="mb-0">
            <button class="btn btn-link" aria-controls="collapse{{str_replace([' ','/'], ['-','-'], $k)}}">
                {{$k}}
            </button>
        </h5>
    </div>

    <div id="collapse{{str_replace([' ','/'], ['-','-'], $k)}}" class="collapse" aria-labelledby="heading{{str_replace([' ','/'], ['-','-'], $k)}}" data-parent="#accordion">
        <div class="card-body">
            <ul style="list-style-type: none; padding-inline-start: 10px;">
                @foreach($v as $equipment)
                    <li>
                        {{$equipment}}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
