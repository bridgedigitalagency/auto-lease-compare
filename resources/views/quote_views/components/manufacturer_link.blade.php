@if($manufacturerLinks)
<div class="row">
    <div class="col-12 col text-center">
        <p>
            Visit <a href="{{$manufacturerLinks}}" target="_blank">{{$manufacturerLinks}}</a> for more information.
        </p>
    </div>
</div>
@endif