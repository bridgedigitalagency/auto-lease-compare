
<h5 class="pinkHeading">Customise Your Lease</h5>

<div class="row">
    <div class="col-md-12">
        <label class="control-label" for="deposit_value">
            <span data-toggle="tooltip" data-placement="top"  data-html="true" title="Your initial payment is essentially your first monthly payment, equivalent to a set number of months. It’s not a deposit – it is part of your total lease cost.  The more months you put upfront, the lower your monthly payments will be. <br><br><b>Select what payment you can afford upfront.</b> "><i class="fa fa-info-circle"></i> Initial Payment</span>
        </label>
        <div id="deposit_val">
            @foreach($depositVal as $k=>$v)
                @if($v == true)

                    @if($k == $single_quote->deposit_value)
                        <button value="{{$k}}" class="button b2" data-selected="true">{{$k}}m</button>
                    @else
                        <button value="{{$k}}" class="button b7">{{$k}}m</button>
                    @endif
                @endif
            @endforeach
        </div>


    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <label class="control-label" for="customise_term">
            <span data-toggle="tooltip" data-placement="top" data-html="true" title="This is simply the duration of your lease.<br><br><b>Select how long you want the vehicle for.</b> "><i class="fa fa-info-circle"></i> Contract Length</span>
        </label>
        <div id="term_val">
            @foreach($termVal as $k=>$v)
                @if($v == true)

                    @if($k == $single_quote->term)
                        <button value="{{$k}}" class="button b2" data-selected="true">{{$k}}m</button>
                    @else
                        <button value="{{$k}}" class="button b7">{{$k}}m</button>
                    @endif
                @endif
            @endforeach
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <label class="control-label" for="annual_mileage">
            <span data-toggle="tooltip" data-placement="top" data-html="true" title="This is your annual mileage allowance.<br><br><b>Select how many miles you think you will drive each year.</b>"><i class="fa fa-info-circle"></i> Annual Mileage</span>
        </label>
        <div class="form-group">
            <select id="annual_mileage" class="form-control">
                @foreach($mileageVal as $k=>$v)
                    @if($v == true)

                        @if($k == $single_quote->annual_mileage)
                            <option value="{{$k}}" selected>{{$k}} Miles P/A</option>
                        @else
                            <option value="{{$k}}">{{$k}} Miles P/A</option>
                        @endif
                    @endif
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="alert alert-warning fade d-none" role="alert" id="priceUpdatedNotification">
    Price details have been updated!
</div>