<div class="row providerDetails">
    <div class="col-5 col-md-6 approvedPartner text-center">
        <span><img src="/images/approvedpartner.png"><br> Approved Partner</span>
    </div>
    <div class="col-7 col-md-6 text-right">
        <div class="hideonmobile">
        <h5 class="pinkHeading"><span data-toggle="tooltip" data-placement="top" data-html="true" title="Fill in the <b>'Enquire Today'</b> form or click the <b>'Call Now'</b> button to get your personalised quote. The Deal Provider will confirm final offer details."> <i class="fa fa-info-circle"></i> Request a Quote</span></h5>
        <button id="enquireTodayButton" type="button" class="button b3" data-toggle="modal" data-target="#quotemodel"onclick="ga('send', {hitType: 'event',eventCategory: 'EnquireNow',eventAction: 'LoadForm',eventLabel: 'Forms'});">
            <i class="fa fa-envelope"></i>  Enquire Today
        </button>
        </div>
        <div class="showonmobile">
            <button type="button" data-toggle="modal" data-target="#dealInformation" class="button b2"><i class="fa fa-info-circle"></i> Deal Information</button>
        </div>
    </div>
</div>
<div class="row providerDetails">
    <div class="col-5 col-md-6 text-center">
        @if(isset($dealerInfoJSON->logo))
            <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-2 dealerLogo" alt="{{$dealerInfoJSON->name}}">
        @endif
    </div>
    <div class="col-7 col-md-6 text-right">
        <div class="hideonmobile">
        <button type="button" class="button b6 callButtonDesktop" data-toggle="modal" data-target="#quotecall" onclick="ga('send', {hitType: 'event',eventCategory: 'CallNow',eventAction: 'ButtonClick',eventLabel: 'Call'});">
            <i class="fa fa-phone"></i>  Call For Quote
        </button>
        </div>
        <div class="showonmobile">
            <button type="button" class="button b2" data-toggle="modal" data-target="#dealermodal"><i class="fa fa-info-circle"></i>  About Provider</button>
        </div>
    </div>
</div>
