<?php

//    foreach ($v as $d) {
//        if ($d->description == "CC") {
//            $displayData['cc'] = $d->value;
//        }
//        if ($d->description == "Engine Power - BHP") {
//            $displayData['power'] = $d->value;
//        }
//        if ($d->description == "Engine Torque - RPM") {
//            $displayData['torque'] = $d->value;
//        }
//        if ($d->description == "Top Speed") {
//            $displayData['topspeed'] = $d->value;
//        }
//        if ($d->description == "0 to 62 mph (secs)") {
//            $displayData['0to60'] = $d->value;
//        }
//        if ($d->description == "EC Urban (mpg)") {
//            $displayData['urbanmpg'] = $d->value;
//        }
//        if ($d->description == "EC Extra Urban (mpg)") {
//            $displayData['extraurbanmpg'] = $d->value;
//        }
//        if ($d->description == "EC Combined (mpg)") {
//            $displayData['combinedmpg'] = $d->value;
//        }
//        if ($d->description == "Length") {
//            $displayData['length'] = $d->value;
//        }
//        if ($d->description == "Width") {
//            $displayData['width'] = $d->value;
//        }
//        if ($d->description == "Height") {
//            $displayData['height'] = $d->value;
//        }
//        if ($d->description == "Wheelbase") {
//            $displayData['wheelbase'] = $d->value;
//        }
//        if ($d->description == "Fuel Tank Capacity (Litres):") {
//            $displayData['fuelTank'] = $d->value;
//        }
//        if ($d->description == "Insurance Group 1 - 50 Effective January 07") {
//            $displayData['insurance'] = $d->value;
//        }
//        if ($d->description == "Standard manufacturers warranty - Years") {
//            $displayData['warranty'] = $d->value;
//        }
//        if ($d->description == "Gears") {
//            $displayData['gears'] = $d->value;
//        }
//        if ($d->description == "Fuel Tank Capacity (Litres)") {
//            $displayData['fuelCapacity'] = $d->value;
//        }
//        if ($d->description == "Alloys?") {
//            $displayData['alloys'] = $d->value;
//        }
//        if ($d->description == "Tyre Size Front") {
//            $displayData['frontTyreSize'] = $d->value;
//        }
//        if ($d->description == "Tyre Size Rear") {
//            $displayData['rearTyreSize'] = $d->value;
//        }
//    }
//}
//?>
<!-- Quote Modal -->
<div class="modal fade" id="quotemodel" tabindex="-1" role="dialog" aria-labelledby="quotemodel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Send Your Enquiry To {{$single_quote->deal_id}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 dealInfoBreakdownA" style="border-right: 1px solid #d6d6d6;">
                        <h5 class="text-center"><strong>Deal Breakdown</strong></h5>
                        <table style="width:100%;">
                            <tbody>
                            <tr>
                                <td class="t1"><small class="red">Initial Payment</small></td>
                                <td class="t2"><small><span id="originalInitialRentalqf" class="initial_payment_price">£<?php echo $single_quote->deposit_months;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT (<span class="initial_payment_months"><?php echo $single_quote->deposit_value;?></span> Months Upfront)</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Contract Length</small></td>
                                <td class="t2"><small><span id="originalTerm3" class="term_text"><?php echo $single_quote->term;?></span> Months</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Annual Mileage</small></td>
                                <td class="t2"><small><span class="annual_mileage_text"><?php echo $single_quote->annual_mileage;?></span> Miles Per Annum</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Monthly Rental</small></td>
                                <td class="t2"><small><span id="originalTerm4" class="term_text_minus_initial"><?php echo $single_quote->term - 1;?></span>
                                        monthly payments of <span class="monthly_payment_text" data-bind="orignal_monthly_rental">£<?php echo $single_quote->monthly_payment;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Average Monthly Cost</small></td>
                                <td class="t2"><small><span class="average_cost_text">£<?php echo $single_quote->average_cost;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Document Fee</small></td>
                                <td class="t2"><small>£<span id="originalDocumentFee"><?php echo $single_quote->document_fee;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Stock Staus</small></td>
                                <td class="t2"><small>@if($single_quote->in_stock =='1') Stock Vehicle @else Factory Order @endif</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Vehicle Type</small></td>
                                <td class="t2"><small>Brand New</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Road Tax</small></td>
                                <td class="t2"><small>Included</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Manufactures Warranty</small></td>
                                <td class="t2"><small>Included</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Delivery</small></td>
                                <td class="t2"><small>{{$dealerInfo->delivery_policy}}</small></td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h5 class="text-center d-none d-md-block"><strong>Deal Provider</strong></h5>
                        <div class="text-center">
                            <strong>This {{$single_quote->maker}} {{$single_quote->model}} {{$single_quote->derivative}} is brought to you by {{$single_quote->deal_id}}</strong></div>
                        <div class="row justify-content-md-center mt-3 d-none d-md-flex">
                            @if(isset($dealerInfoJSON->logo))
                                <div class="col-sm-5 col-md-12 text-center">
                                    <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-3" alt="{{$dealerInfoJSON->name}}" style="max-width: 40%;">
                                </div>
                            @endif
                            @if(isset($dealerInfoJSON->trustpilot))
                                @if($trustpilotRatings->businessUnit->trustScore > 0)
                                    <div class="col-sm-5 col-md-12 text-center">
                                        <img src="/images/trustpilot/trustpilot-{{$trustpilotRatings->businessUnit->stars}}.png" alt="{{$trustpilotRatings->businessUnit->stars}} on Trustpilot" class="img-fluid" style="max-width: 80%;"><br/>
                                        <img src="/images/trustpilot/trustpilot-logo.svg" alt="Trustpilot" class="img-fluid"><br/>
                                        <span class="tp-text">Based on {{$trustpilotRatings->businessUnit->numberOfReviews->total}} Reviews</span>
                                    </div>
                                @endif
                            @endif
                        </div>

                    </div>
                    <div class="col-md-6 enquiretoday dealInfoBreakdownB">
                        <h5 class="text-center mb-1" style="font-size: 30px;color: #fc5185;"><strong>Enquire Today</strong></h5>

                        @if( app('request')->input('enqErr') == '1')
                            <div class="alert alert-danger" role="alert">
                                <p>There was an error submitting your enquiry, please try again.</p>
                                <p>If the problem persists please <a href="mailto:info@autoleasecompare.com">contact us</a></p>
                            </div>
                        @endif

                        <form method="post" action="/quote" enctype="multipart/form-data" id="enquiryForm">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label for="name" class="col-sm-12 col-form-label">Name</label>
                                <div class="col-sm-12">
                                    <input name="name" type="text" class="form-control data-hj-allow" id="name" placeholder="Name" value="@if(Auth::check())@if(isset(Auth::user()->first_name)){{Auth::user()->first_name }} {{Auth::user()->last_name }}@endif @elseif(Cookie::get('name') != null && !Auth::check()){{Cookie::get('name') }}@endif" required pattern=".*\S+.*">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-12 col-form-label">Email</label>
                                <div class="col-sm-12">
                                    <input name="email" type="email" class="form-control data-hj-allow" id="email" placeholder="Email" value="@if(Cookie::get('email') != null && !Auth::check()){{Cookie::get('email') }}@endif @if (Auth::check()){{{ isset(Auth::user()->email) ? Auth::user()->email : Auth::user()->email }}} @endif " required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-sm-12 col-form-label">Phone</label>
                                <div class="col-sm-12">
                                    <input name="phone_number" type="tel" class="form-control data-hj-allow" id="phone_number"
                                           placeholder="Phone" value="{{$phone_number}}" required pattern="[0-9.+() ]+">
                                </div>
                            </div>
                            @if($single_quote->finance_type == 'B')
                                <div class="form-group row">
                                    <label for="businessName" class="col-sm-12 col-form-label">Business Name</label>
                                    <div class="col-sm-12">
                                        <input name="businessName" type="text" class="form-control data-hj-allow" id="businessName"
                                               placeholder="Business Name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="businessType" class="col-sm-12 col-form-label">Business Type</label>
                                    <div class="col-sm-12">
                                        <select id="businessType" class="form-control" name="businessType" aria-invalid="true" >
                                            <option value="">Please select</option>
                                            <option value="Limited Company">Limited Company</option>
                                            <option value="Private Individual">Private Individual</option>
                                            <option value="Sole Trader">Sole Trader</option>
                                            <option value="LLP">LLP</option>
                                            <option value="Registered Charity">Registered Charity</option>
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label for="contact_preference" class="col-sm-12 col-form-label">Contact Preference</label>
                                <div class="col-sm-12">
                                    <select id="contact_preference" class="form-control" name="contact_preference" aria-required="true" required aria-invalid="true">
                                        <option selected="selected" value="5">No Contact Preference</option>
                                        <option value="1">Call me back</option>
                                        <option value="2">Call me back (Morning)</option>
                                        <option value="3">Call me back (Afternoon)</option>
                                        <option value="4">Email me back</option>
                                    </select>

                                </div>
                            </div>

                            <div style="display: none;">
                                <textarea name="comment" id="comment"></textarea>
                                <input type="text" name="subject" id="subject">
                            </div>
                            <div class="form-group row">

                                <label for="message" class="col-sm-12 col-form-label">Maintenance Package</label>
                                <div class="col-sm-12">
                                    <div class="maintenance2">
                                        <input type="checkbox" id="maintenance" name="maintenance" rel="Quote me for maintenance package: Yes" style="margin-right: 9px;"><strong style="font-size: 13px; vertical-align: text-bottom;">Quote me for maintenance package?</strong><br/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="message" class="col-sm-12 col-form-label">Your Message</label>
                                <div class="col-sm-12">
                                    <textarea class="form-control data-hj-allow" id="message" name="message" value="Your Message" type="text" style="height: 110px; margin-bottom:15px;">Hello, I'm interested in this deal, please send me a personalised quote.</textarea>
                                    <input type="checkbox" name="hotdeals" value="1" style="margin-right: 9px;"><strong style="font-size: 13px; vertical-align: text-bottom;">Subscribe to our hot deals</strong><br/>
                                    <input type="checkbox" name="creditcheck" id="creditcheck" value="1" style="margin-right: 9px;"><strong style="font-size: 13px; vertical-align: text-bottom;">Free 14 day trial! Check your credit report!</strong><br/>
                                </div>
                            </div>
                            <input name="model_id" type="hidden" class="form-control" id="model_id"
                                   value="{{$single_quote->id}}">
                            <input name="database" type="hidden" class="form-control" id="database"
                                   value="{{$single_quote->database}}">
                            <input name="make" type="hidden" class="form-control" id="make"
                                   value="{{$single_quote->maker}}">
                            <input name="model" type="hidden" class="form-control" id="model"
                                   value="{{$single_quote->model}} {{$single_quote->derivative}}">
                            <input name="deal_id" type="hidden" class="form-control" id="deal_id"
                                   value="{{$single_quote->deal_id}}">
                            <input name="transmission" type="hidden" class="form-control" id="transmission"
                                   value="{{$single_quote->transmission}}">
                            <input name="fuel_type" type="hidden" class="form-control" id="fuel_type"
                                   value="{{$single_quote->fuel}}">
                            <input name="prices_id" type="hidden" class="form-control" id="prices_id"
                                   value="{{$single_quote->id}}">
                            <input name="cap_id" type="hidden" class="form-control" id="cap_id"
                                   value="{{$single_quote->cap_id}}">
                            <input name="price" type="hidden" class="form-control" id="price"
                                   value="{{$single_quote->monthly_payment}}">
                            <input name="profile" type="hidden" class="form-control" id="profile"
                                   value="£{{$single_quote->deposit_months}} @if($single_quote->finance_type =='P')Inc VAT @else Ex VAT @endif ({{$single_quote->deposit_value}} Months Upfront) | {{$single_quote->term}} Month Contract | {{$single_quote->annual_mileage}}k Miles P/A">
                            <input name="finance_type" type="hidden" class="form-control" id="finance_type"
                                   value="@if($single_quote->finance_type =='P')P @else B @endif">
                            <input name="document_fee" type="hidden" class="form-control" id="document_fee"
                                   value="{{$single_quote->document_fee}}">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <button type="submit" class="mt-2 mb-3 button b3" onclick="ga('send', 'event', 'MessageEnquiry', 'click', 'enquiry');">Send Enquiry Now ></button>
                                </div>
                                <div class="col-md-12">
                                    <p><small><strong>Please note:</strong> Don’t worry - by clicking this button, you are not committing to this lease deal. We’ll simply pass on your enquiry to the advertiser so they can help get you behind the wheel of your new car!</small></p>
                                    <p><small>You are free to make additional enquiries to other advertisers within Auto Lease Compare.</small></p>
                                    <p><small>By submitting this enquiry you are confirming that you have read and understood the terms of our <a href="/site/privacy" target="_blank">privacy policy.</a></small></p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row text-center mt-3 d-sm-block d-md-none">
                        @if(isset($dealerInfoJSON->logo))
                            <div class="col-12">
                                <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-3" alt="{{$dealerInfoJSON->name}}" style="max-width: 40%; text-align: center;">
                            </div>
                        @endif
                        @if(isset($dealerInfoJSON->trustpilot))
                            @if($trustpilotRatings->businessUnit->trustScore > 0)
                                <div class="col-12">
                                    <img src="/images/trustpilot/trustpilot-{{$trustpilotRatings->businessUnit->stars}}.png" alt="{{$trustpilotRatings->businessUnit->stars}} on Trustpilot" class="img-fluid"><br/>
                                    <img src="/images/trustpilot/trustpilot-logo.svg" alt="Trustpilot" class="img-fluid"><br/>
                                    <span class="tp-text">Based on {{$trustpilotRatings->businessUnit->numberOfReviews->total}} Reviews</span>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b2" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Quote Modal -->
<!-- Deal info Modal -->
<div class="modal fade" id="dealInformation" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dealInformationLabel">
                    Deal Information
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p>Please note that Auto Lease Compare do not charge you or add anything on top of the price you see!</p>

                        <table style="width: 100%;" class="table table-striped">
                            <tbody>
                            <tr>
                                <td class="t1 red">Initial Payment</td>
                                <td class="t2">
                                    <strong><span class="initial_payment_price">£<?php echo $single_quote->deposit_months;?></span>
                                        @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT (
                                        <span class="initial_payment_months"><?php echo $single_quote->deposit_value ?></span> Months Upfront)</strong>

                                </td>
                            </tr>
                            <tr>
                                <td class="t1 red">Monthly Rental</td>
                                <td class="t2">

                                    <strong><span id="originalTermdi3" class="term_text_minus_initial"><?php echo($single_quote->term - 1);?></span> monthly payments of
                                        <span class="monthly_payment_text" data-bind="orignal_monthly_rental">£<?php echo $single_quote->monthly_payment;?></span>
                                        @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</strong>

                                </td>
                            </tr>
                            <tr>
                                <td class="t1 red">Contract Length</td>
                                <td class="t2">

                                    <strong><span id="originalTermdi" class="term_text"><?php echo $single_quote->term;?></span> Months</strong>

                                </td>
                            </tr>
                            <tr>
                                <td class="t1 red">Profile:</td>
                                <td class="t2">
                                    <strong>
                                        <span id="originaldpdi" class="initial_payment_months"><?php echo $single_quote->deposit_value;?></span>
                                        + <span id="originalTermdi2" class="term_text_minus_initial"><?php echo $single_quote->term - 1;?></span>
                                        (1 payment of <span class="initial_payment_price">£<?php echo $single_quote->deposit_months;?></span> +
                                        <span class="term_text_minus_initial"><?php echo $single_quote->term - 1;?></span>
                                        monthly payments of <span class="monthly_payment_text" data-bind="orignal_monthly_rental">£<?php echo $single_quote->monthly_payment;?></span> @if($single_quote->finance_type =='P')
                                            Inc @else Ex @endif VAT)</strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="t1 red">Annual Mileage:</td>
                                <td class="t2">
                                    <strong><span class="annual_mileage_text"><?php echo $single_quote->annual_mileage;?></span> Miles Per Annum</strong>
                                </td>

                            </tr>
                            <tr>
                                <td class="t1 red">Document Fee</td>
                                <td class="t2">

                                    <strong>£<span id="originalDocumentFeedi"><?php echo $single_quote->document_fee;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</strong>

                                </td>
                            </tr>
                            <tr>
                                <td class="t1 red">Total Cost</td>
                                <td class="t2">
                                    <strong><span id="originalTotaldi" class="total_cost_text">£<?php echo $single_quote->total_cost;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT (inc. initial payment and document fee)</strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="t1 red">Av.&nbsp;cost per month:</td>
                                <td class="t2">
                                    <strong><span class="average_cost_text">£<?php echo $single_quote->average_cost;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT (inc. initial payment and document fee)</strong>
                                </td>

                            </tr>
                            <tr>
                                <td class="t1 red">Stock</td>
                                <td class="t2"><strong>@if($single_quote->in_stock == 1) In Stock @else Factory Order @endif</strong></td>
                            </tr>
                            <tr>
                                <td class="t1 red">Vehicle Type</td>
                                <td class="t2"><strong>{{$single_quote->bodystyle}}</strong></td>
                            </tr>
                            <tr>
                                <td class="t1 red">Road Tax</td>
                                <td class="t2"><strong>Included</strong></td>
                            </tr>
                            @if(isset($displayData['warranty']) && $displayData['warranty'] != '')
                                <tr>
                                    <td class="t1 red">Warranty</td>
                                    <td class="t2"><strong><?php echo $displayData['warranty'];?> Years</strong></td>
                                    @else
                                        <td class="t1 red">Manufactures Warranty</td>
                                        <td class="t2"><strong>Included</strong></td>
                                </tr>
                            @endif

                            <tr>
                                <td class="t1 red">Delivery</td>
                                <td class="t2"><strong>{{$dealerInfo->delivery_policy}}</strong></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">

                        <h3>Definitions</h3>
                        <div id="accordion">

                            <div class="card">
                                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" aria-controls="collapseOne">
                                            What is a profile?
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>A <strong>‘profile’</strong> is a term used in the leasing industry to describe the structure of the payment e.g. 9 + 47, this indicates an initial upfront payment of 9 months followed by 47 monthly payments, totalling into a 4 year contract (48 months).&nbsp;</p>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" aria-controls="collapseTwo">
                                            What is the document fee?
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>The <strong>document fee</strong> is the administration fee which is charged by the leasing provider.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" >
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" aria-controls="collapseFour">
                                            What is the total cost?
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>The <strong>Total Cost</strong> across the entire term of the contract (Inc. Total Monthly Payments + Initial Payment + Documentation fee).</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" aria-controls="collapseThree">
                                            What is the average monthly cost?
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>The <strong>Average Monthly Cost</strong> is the Total Cost (defined above) divided by the length of the contract.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" aria-controls="collapseFive">
                                            What is the monthly rental?
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>The <strong>Monthly Rental</strong> is the set price you will pay each month.</p>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Deal info Modal -->
<!-- Call Modal -->
<div class="modal fade" id="quotecall" tabindex="-1" role="dialog" aria-labelledby="quotecall" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Call <?php echo $single_quote->deal_id;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #d6d6d6;">
                        <h5 class="text-center"><strong>Deal Breakdown</strong></h5>
                        <table style="width:100%;">
                            <tbody>
                            <tr>
                                <td class="t1"><small class="red">Initial Payment</small></td>
                                <td class="t2"><small><span id="customised_priceci" class="initial_payment_price">£<?php echo $single_quote->deposit_months;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT (<span class="initial_payment_months"><?php echo $single_quote->deposit_value;?></span> Months Upfront)</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Contract Length</small></td>
                                <td class="t2"><small><span class="term_text"><?php echo $single_quote->term;?></span> Months</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Annual Mileage</small></td>
                                <td class="t2"><small><span class="annual_mileage_text"><?php echo $single_quote->annual_mileage;?></span> Miles Per Annum</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Monthly Rental</small></td>
                                <td class="t2"><small><span id="originalTermci2" class="term_text_minus_initial"><?php echo $single_quote->term - 1;?></span> monthly payments of
                                        <span class="monthly_payment_text">£<?php echo $single_quote->monthly_payment;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Average Monthly Cost</small></td>
                                <td class="t2"><small><span class="average_cost_text">£<?php echo $single_quote->average_cost;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Document Fee</small></td>
                                <td class="t2"><small>£<span id="originalDocumentFeeCi"><?php echo $single_quote->document_fee;?></span> @if($single_quote->finance_type =='P') Inc @else Ex @endif VAT</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Stock Staus</small></td>
                                <td class="t2"><small>@if($single_quote->in_stock =='1') Stock Vehicle @else Factory Order @endif</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Vehicle Type</small></td>
                                <td class="t2"><small>Brand New</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Road Tax</small></td>
                                <td class="t2"><small>Included</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Manufactures Warranty</small></td>
                                <td class="t2"><small>Included</small></td>
                            </tr>
                            <tr>
                                <td class="t1"><small class="red">Delivery</small></td>
                                <td class="t2"><small>{{$dealerInfo->delivery_policy}}</small></td>
                            </tr>
                            </tbody>
                        </table>

                        <h5 class="text-center callExtrasTitle"><strong>Extras</strong></h5>
                        <div id="callExtras"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 text-center mt-4">
                                @if($openingTimes)

                                    @if($openingTimes['status']==1)
                                        <h5><strong>Call today for a quote</strong></h5>
                                        <a class="calllink button b3" style="margin-bottom: 20px;" href="tel:{{$dealerInfoJSON->phone}}">{{$dealerInfoJSON->phone}}</a>
                                    @endif

                                    @if($openingTimes['status']==2)
                                        <h5><strong>Call later today for a quote</strong></h5>
                                        <a class="calllink button b3" style="margin-bottom: 20px;" href="#"><small style="font-size: 14px; vertical-align: middle;">
                                                Calls reopen {{date('g:ia', strtotime($openingTimes['time']))}} today
                                            </small></a>
                                    @endif

                                    @if($openingTimes['status']==3)
                                        <h5><strong>Sorry, this dealer is closed.</strong></h5>
                                        <p>Calls reopen on {{date('g:ia', strtotime($openingTimes['time']))}} {{$openingTimes['day']}}</p>
                                        <a style="color:#fc5185; cursor: pointer" data-dismiss="modal" id="modalclosecall" data-toggle="modal" data-target="#quotemodel" onclick="ga('send', {hitType: 'event',eventCategory: 'EnquireNow',eventAction: 'LoadForm',eventLabel: 'Forms'});">Send them an enquiry instead.</a>
                                    @endif


                                    <h5><strong>Opening Hours</strong></h5>
                                    {{$dealerInfoJSON->opening_hours}}<br/>

                                @else

                                    @if(isset($dealerInfoJSON->phone))
                                        <h5><strong>Call Today</strong></h5>
                                        <a class="calllink button b3" style="margin-bottom: 20px;" href="tel:{{$dealerInfoJSON->phone}}">{{$dealerInfoJSON->phone}}</a>
                                    @endif

                                    @if(isset($dealerInfoJSON->opening_hours))
                                        <h5 class="mt-4"><strong>Opening Hours</strong></h5>
                                        {{$dealerInfoJSON->opening_hours}}<br/>
                                    @endif
                                    @if(isset($dealerInfoJSON->phone))
                                        Are they closed? <a style="color:#fc5185; cursor: pointer" data-dismiss="modal" id="modalclosecall" data-toggle="modal" data-target="#quotemodel" onclick="ga('send', {hitType: 'event',eventCategory: 'EnquireNow',eventAction: 'LoadForm',eventLabel: 'Forms'});">Send them an enquiry instead.</a>
                                    @endif
                                @endif

                                <p><small>Calls will be charged at your standard network rate (no additional charges).</small></p>
                            </div>
                        </div>
                        <br/>
                        <div class="text-center">
                            <strong>This {{$single_quote->maker}} {{$single_quote->model}} {{$single_quote->derivative}} is brought to you by {{$single_quote->deal_id}}</strong></div>
                        <div class="row justify-content-md-center mt-3">
                            @if(isset($dealerInfoJSON->logo))
                                <div class="col-sm-6 col-md-12 text-center">
                                    <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-3" alt="{{$dealerInfoJSON->name}}" style="max-width: 40%;">
                                </div>
                            @endif
                            @if(isset($dealerInfoJSON->trustpilot))
                                @if($trustpilotRatings->businessUnit->trustScore > 0)
                                    <div class="col-sm-6 col-md-12 text-center">
                                        <img src="/images/trustpilot/trustpilot-{{$trustpilotRatings->businessUnit->stars}}.png" alt="{{$trustpilotRatings->businessUnit->stars}} on Trustpilot" class="img-fluid" style="max-width: 80%;"><br/>
                                        <img src="/images/trustpilot/trustpilot-logo.svg" alt="Trustpilot" class="img-fluid"><br/>
                                        <span class="tp-text">Based on {{$trustpilotRatings->businessUnit->numberOfReviews->total}} Reviews</span>
                                    </div>
                                @endif
                            @endif
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b2" data-dismiss="modal" id="modalclosecall">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Call Modal -->

<!-- Dealer Modal -->
<div class="modal fade" id="dealermodal" tabindex="-1" role="dialog" aria-labelledby="dealermodal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3 class="mb-3">This Deal Is Brought To You By @if(isset($dealerInfoJSON->name)) {{$dealerInfoJSON->name}} @endif</h3>
                            <div class="row justify-content-md-center">
                                @if(isset($dealerInfoJSON->logo))
                                    <div class="col-sm-3 col-md-12  text-center">
                                        <img src="/images/uploads/logos/{{$dealerInfoJSON->logo}}" class="img-fluid mb-3" alt="{{$dealerInfoJSON->name}}" style="max-width: 40%;">
                                    </div>
                                @endif
                                @if(isset($dealerInfoJSON->trustpilot))
                                    @if($trustpilotRatings->businessUnit->trustScore > 0)
                                        <div class="col-md-3">
                                            <img src="/images/trustpilot/trustpilot-{{$trustpilotRatings->businessUnit->stars}}.png" alt="{{$trustpilotRatings->businessUnit->stars}} on Trustpilot" class="img-fluid"><br/>
                                            <img src="/images/trustpilot/trustpilot-logo.svg" alt="Trustpilot" class="img-fluid"><br/>
                                            <span class="tp-text">Based on {{$trustpilotRatings->businessUnit->numberOfReviews->total}} Reviews</span>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6 mt-3 dealerinfo">
                            @if(isset($dealerInfoJSON->address))
                                <h5>Address</h5>
                                {{$dealerInfoJSON->address}}<br/>
                                <small>Regardless of their location, the leasing company will almost always arrange to deliver your brand new car straight to your front door and all paperwork is done via email, this alleviates the need for a leasing company to be near you.</small>
                                <br/>
                            @endif
                            @if(isset($dealerInfoJSON->opening_hours))
                                <h5>Opening Hours</h5>
                                {{$dealerInfoJSON->opening_hours}}<br/>
                                <small>Please note response times for your quote will typically be between the above opening hours.</small>
                            @endif
                        </div>
                        <div class="col-md-6 mt-3 dealerinfo">
                            @if(isset($dealerInfoJSON->about))
                                <h5>About Us</h5>
                                {{$dealerInfoJSON->about}}<br/>
                            @endif
                            @if(isset($dealerInfoJSON->delivery_policy))
                                <h5>Delivery Policy</h5>
                                {{$dealerInfoJSON->delivery_policy}}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b3" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Dealer Modal -->

<!-- Social Share Modal -->
<div class="modal fade" id="socialModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle" style="color: #000;">Share This Lease Deal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="share-buttons">
                <?php $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                ?>
                <!-- Social Button HTML -->
                    <!-- Twitter -->
                    <a href="https://twitter.com/share?url=<?php echo $actual_link; ?>&text=Check Out This Lease Deal&via=compare_lease" target="_blank" class="share-btn twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <!-- Facebook -->
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link; ?>" target="_blank" class="share-btn facebook">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <!-- LinkedIn -->
                    <a href="https://www.linkedin.com/shareArticle?url=<?php echo $actual_link; ?>&title=<TITLE>&summary=<SUMMARY>&source=<SOURCE_URL>" target="_blank" class="share-btn linkedin">
                        <i class="fa fa-linkedin"></i>
                    </a>
                    <!-- Email -->
                    <a href="mailto:?subject={{$single_quote->maker}}  {{$single_quote->range}}  {{$single_quote->derivative}} Lease Deal&body=Check out this {{$single_quote->maker}}  {{$single_quote->range}}  {{$single_quote->derivative}} lease deal on Auto Lease Compare <?php echo $actual_link; ?>" target="_blank" class="share-btn email">
                        <i class="fa fa-envelope"></i>
                    </a>
                    <!-- Whatsapp -->
                    <a href="whatsapp://send?text=Check Out This Lease Deal! <?php echo $actual_link; ?>" data-action="share/whatsapp/share" class="share-btn whatsapp">
                        <i class="fa fa-whatsapp"></i>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Social Share Modal -->

<!-- Colours Modal -->
<div class="modal fade" id="coloursmodel" tabindex="-1" role="dialog" aria-labelledby="coloursmodal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="model-standard">
                    Select Your Preferred Colour</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 style="color:#fc5185;">Select your preferred colours to add to your enquiry</h5>
                            <i>*Some of these colours will come at an additional cost, these can be discussed with the deal provider</i>
{{--                            <p>We're currently doing some updates to the optional extras and vehicle colours. In the meantime, the deal provider can provide you with more information.</p>--}}
                            <div id="ChooseColours">
                                @foreach($optionalExtras as $k=>$v)
                                    @if($k == 'Paintwork')
                                        <h5 style="color: rgb(252, 81, 133);">{{$k}}</h5>
                                        <ul style="list-style-type: none; padding-inline-start: 10px;">
                                            @foreach($v as $val)
                                                @php
                                                    $price = 'Free';
                                                    if($val['price'] > 0) {
                                                        $price = 'Additional Cost';
                                                    }
                                                @endphp
                                                <li>
                                                    <input type="checkbox" rel="{{$val['description']}} - {{$price}}"> {{$val['description']}} -
                                                    <i style="color: rgb(252, 81, 133);">{{$price}}</i>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                @endforeach
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-12 col text-center">
                            <p>
                                <small>Vehicle data provided by CAP Motor Research Ltd. Actual vehicle specification may vary.</small>
                            </p>
                        </div>
                    </div>
                    @include('quote_views.components.manufacturer_link')
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b3" data-dismiss="modal" data-toggle="modal" data-target="#quotemodel">Add To Enquiry</button>
            </div>
        </div>
    </div>
</div>
<!-- End Colours Modal -->


<!-- standard equipment -->
<div class="modal fade" id="modelstandard" tabindex="-1" role="dialog" aria-labelledby="modelstandard" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="model-standard">Standard Equipment</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body tech">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            @foreach($standardEquip as $k=>$v)
                                <div id="accordion">
                                    @include("quote_views.components.standard-equip")
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col text-center">
                            <p>
                                <small>Vehicle data provided by CAP Motor Research Ltd. Actual vehicle specification may vary.</small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button b3" data-dismiss="modal">close</button>
            </div>
        </div>
    </div>
</div>

<!-- Technical & Spec Modal -->

<div class="modal fade" id="modeloptions" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="model-options">Optional Extras</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body tech">
                <h5 style='color:#fc5185;'>Select the optional extras to add to your enquiry</h5>
                <i>*Some of these optional extras will come at an additional cost, these can be discussed with the deal provider</i>

{{--                <p>We're currently doing some updates to the optional extras and vehicle colours. In the meantime, the deal provider can provide you with more information.</p>--}}

                @foreach($optionalExtras as $k=>$v)
                    @if($k != 'Paintwork')
                        <h5 style="color: rgb(252, 81, 133);">{{$k}}</h5>
                        <ul style="list-style-type: none; padding-inline-start: 10px;">
                            @foreach($v as $val)
                                @php
                                    $price = 'Free';
                                    if($val['price'] > 0) {
                                        $price = 'Additional Cost';
                                    }
                                @endphp
                                <li>
                                    <input type="checkbox" rel="{{$val['description']}} - {{$price}}"> {{$val['description']}} -
                                    <i style="color: rgb(252, 81, 133);">{{$price}}</i>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                @endforeach



                <p style="text-align: center;">
                    <small>Vehicle data provided by CAP Motor Research Ltd. Actual vehicle specification may vary.</small>
                </p>

                @include('quote_views.components.manufacturer_link')


            </div>
            <div class="modal-footer">
                <button type="button" class="button b3" data-dismiss="modal" data-toggle="modal" data-target="#quotemodel">Add To Enquiry</button>
            </div>
        </div>
    </div>
</div>

<!--tech-->
<div class="modal fade bd-example-modal-lg" id="modeldata" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="model-data">Technical Data</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body tech">
                <div class="row">
                    <div class="col-md-4">
                        <h5 style="color:#fc5185;">General</h5>
                        <div class="row">
                            @if(isset($single_quote->fuel))
                                @if($single_quote->fuel != 'Electric')
                                    <div class="col-6">Fuel Type:</div>
                                    <div class="col-6">{{$single_quote->fuel}}</div>
                                @endif
                            @endif
                            @if(isset($single_quote->transmission))
                                <div class="col-6">Transmission:</div>
                                <div class="col-6"><?php if ($single_quote->transmission == 'Manual') {
                                        echo 'Manual';
                                    } else {
                                        echo 'Automatic';
                                    }?></div>
                            @endif
                            @if(isset($single_quote->co2))
                                <div class="col-6">Co2:</div>
                                <div class="col-6">{{$single_quote->co2}} g/KM</div>
                            @endif
                            <?php if(isset($displayData['cc'])) {?>
                                @if($single_quote->fuel != 'Electric')
                                    <div class="col-6">CC:</div>
                                    <div class="col-6"><?php echo(isset($displayData['cc']) ? $displayData['cc'] : '');?> CC</div>
                                @endif
                            <?php } ?>
                                @if(isset($single_quote->doors))
                                    <div class="col-6">Doors:</div>
                                    <div class="col-6">{{$single_quote->doors}}</div>
                                @endif
                                @if(isset($single_quote->seats))
                                    <div class="col-6">Seats:</div>
                                    <div class="col-6">{{$single_quote->seats}}</div>
                                @endif

                            <?php if(isset($techData->gears)) {?>
                            <div class="col-6">Gears:</div>
                            <div class="col-6"> <?php echo $techData->gears; ?></div>
                            <?php } ?>
                            <?php if(isset($techData->insurancegroup1)) {?>
                                <div class="col-6"><div style="white-space: nowrap;">Insurance Group:</div></div>
                            <div class="col-6"> <?php echo $techData->insurancegroup1; ?></div>
                            <?php } ?>
                            <?php if(isset($techData->standardmanwarranty_years) && $techData->standardmanwarranty_years != '') {?>
                            <div class="col-6">Warranty:</div>
                            <div class="col-6"><?php echo $techData->standardmanwarranty_years;?> Years</div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h5 style="color:#fc5185;">PERFORMANCE</h5>
                        <div class="row">
                            <?php if(isset($techData->enginepower_bhp)) {?>
                            <div class="col-6">Power:</div>
                            <div class="col-6"><?php echo $techData->enginepower_bhp; ?> BHP</div>
                            <?php } ?>
                            <?php if(isset($techData->topspeed)) {?>
                            <div class="col-6">Top Speed:</div>
                            <div class="col-6"><?php echo $techData->topspeed; ?> MPH</div>
                            <?php } ?>
                            <?php if (isset($techData->{'0to62'})) { ?>
                                        <?php if ($techData->{'0to62'} !== false) {
                                echo "<div class='col-6 col-6'>0 to 62 mph:</div><div class='col-6 col-6'> " . $techData->{'0to62'} . " SECS</div>";
                            } ?>
                                        <?php } ?>
                        </div>
                        @if($single_quote->fuel != 'Electric' && (isset($single_quote->mpgextraurban) && $single_quote->mpgextraurban !=0)
                        &&(isset($single_quote->mpgcombined) && $single_quote->mpgcombined !=0) && (isset($techData->urbanmpg) && $techData->urbanmpg !=0))
                        <h5 style="color:#fc5185;">FUEL CONSUMPTION</h5>
                        <div class="row">
                            <?php if(isset($techData->urbanmpg)) {?>
                            <div class="col-6">Urban:</div>
                            <div class="col-6"> <?=$techData->urbanmpg;?> MPG</div>
                            <?php } ?>
                            <?php if(isset($single_quote->extraurbanmpg)) {?>
                            <div class="col-6">Extra urban:</div>
                            <div class="col-6"><?=$single_quote->extraurbanmpg;?> MPG</div>
                            <?php } ?>
                            <?php if(isset($single_quote->combinedmpg)) {?>
                            <div class="col-6">Combined:</div>
                            <div class="col-6"><?=$single_quote->combinedmpg;?> MPG</div>
                            <?php } ?>
                        </div>
                            @endif
                    </div>
                    <div class="col-md-4">
                        <h5 style="color:#fc5185;">METRICS</h5>
                        <div class="row">
                            <?php if(isset($techData->length)) {?>
                            <div class="col-6">Length:</div>
                            <div class="col-6"> <?php echo $techData->length; ?> mm</div>
                            <?php } ?>
                            <?php if(isset($techData->width)) {?>
                            <div class="col-6">Width:</div>
                            <div class="col-6"><?php if (isset($techData->width) && !is_null($techData->width)) {
                                    echo $techData->width . ' mm';
                                } else {
                                    echo 'N/A';
                                } ?></div>
                            <?php } ?>
                            <?php if(isset($techData->height)) {?>
                            <div class="col-6">Height:</div>
                            <div class="col-6"><?php echo $techData->height; ?> mm</div>
                            <?php } ?>
                            <?php if(isset($techData->wheelbase)) {?>
                            <div class="col-6">Wheelbase:</div>
                            <div class="col-6"><?php echo $techData->wheelbase; ?> mm</div>
                            <?php } ?>
                            @if($single_quote->fuel != 'Electric')
                                <?php if(isset($techData->fueltankcapacity)) {?>
                                <div class="col-6">Fuel Tank Capacity:</div>
                                <div class="col-6"><?php echo $techData->fueltankcapacity;?> Litres</div>
                                <?php } ?>
                            @endif
                            <?php if(isset($techData->alloys)) {?>
                            <div class="col-6">Alloys:</div>
                            <div class="col-6"><?php if ($techData->alloys) {
                                    echo "Yes";
                                } else {
                                    echo "No";
                                };?></div>
                            <?php } ?>
                            <?php if(isset($techData->frontTyreSize)) {?>
                            <div class="col-6">Tyres (Front):</div>
                            <div class="col-6"><?php echo $techData->frontTyreSize; ?></div>
                            <?php } ?>
                            <?php if(isset($techData->rearTyreSize)) {?>
                            <div class="col-6">Tyres (Rear):</div>
                            <div class="col-6"><?php echo $techData->rearTyreSize; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                @include('quote_views.components.manufacturer_link')
                <div class="row">
                    <div class="col-12 col text-center">
                        <p>
                            <small>Vehicle data provided by CAP Motor Research Ltd. Actual vehicle specification may vary.</small>
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Tech & Spec Modal -->

<!--Reviews-->
<div class="modal fade bd-example-modal-lg" id="modelreview" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="model-data">Vehicle Review</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row">
                <div class="col-md-12">
                    @if(isset($json_output1->videolist) && count($json_output1->videolist) > 0)


                        <div class="video_responsive text-center d-none d-md-none d-lg-block">
                            <iframe src="https://ssl.caranddriving.com/cdwebsite/player.aspx?id={{$json_output1->reviewid}}&autostart=false&cid=autoleasecompare&w=600" width="600" height="400" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" align="middle" allowfullscreen id="cdvideoDesktop" ></iframe>
                        </div>
                        <div class="video_responsive text-center d-sm-block d-xs-block d-md-block d-lg-none d-xl-none">
                            <iframe src="https://ssl.caranddriving.com/cdwebsite/player.aspx?id={{$json_output1->reviewid}}&amp;autostart=false&amp;cid=autoleasecompare&amp;responsive=true" frameborder="0" marginwidth="0" marginheight="0" scrolling="yes" align="middle" allowfullscreen id="cdvideo" ></iframe>
                        </div>


                    @endif
                    @if(isset($reviews) && count($reviews) > 0)
                        @foreach($reviews as $review)
                            @include('partials/quote-reviews')
                        @endforeach
                    @else
                        <p style="text-align: center; margin-top: 10px;">Sorry, we don't have any written reviews available for this vehicle currently. Please check back later.</p>
                    @endif
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>