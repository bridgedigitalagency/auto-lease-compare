<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script>

    var dayslabels = [@foreach($cars as $model) @if ($loop->first) @foreach(($model['priceHistory']) as $hist) '<?= date("d/m/Y", strtotime($hist->updated_at)) ?>',@endforeach @endif @endforeach];
    var daysdata = [@foreach($cars as $model){
        type: 'line',
        backgroundColor: '@if($loop->first)rgba(63, 193, 201, 1)@elseif($loop->last)rgba(252, 79, 131, 1)@else rgba(252, 154, 79, 1)@endif',
        borderColor: '@if($loop->first)rgba(63, 193, 201, 1)@elseif($loop->last)rgba(252, 79, 131, 1)@else rgba(252, 154, 79, 1)@endif',
        label: 'Daily Prices For: {{$model['maker']}} {{$model['range']}}',
        yAxisID: 'y-axis-0',
        fill: false,
        data: [@foreach(($model['priceHistory']) as $hist){{$hist->monthly_payment}}@if($loop->last)@else,@endif @endforeach],
        spanGaps: true,
    }@if($loop->last)@else,@endif @endforeach];

</script>


<div id="canvasContainer">
    <canvas id="forecast" width="400" height="250"></canvas>
</div>
<script>
    var ctx = document.getElementById('forecast').getContext('2d');
    var config = {
        type: 'line',
        data: {
            labels: dayslabels,
            datasets: daysdata,
        },

        options: {
            scales: {
                scaleSteps : 100,
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        steps: 10,
                        stepSize: 100,
                        fontColor: 'rgba(34, 43, 56, 1)',
                        fontSize: '14',
                        fontFamily: 'Varela Round',
                        // Include a dollar sign in the ticks

                        userCallback: function (label, index, labels) {
                            // when the floored value is the same as the value we have a whole number
                            if (Math.floor(label) === label) {
                                return '£' + label;
                            }
                        },
                    }
                }],
                xAxes: [{
                    /* For changing color of x-axis coordinates */
                    ticks: {
                        fontColor: 'rgba(34, 43, 56, 1)',
                        fontSize: '14',
                        fontStyle: "bold",
                        fontFamily: 'Varela Round'
                    }
                }]
            },
            legend: {
                labels: {
                    // This more specific font property overrides the global property
                    fontFamily: 'Varela Round'
                }
            },
            tooltips: {
                cornerRadius: 4,
                caretSize: 4,
                xPadding: 16,
                yPadding: 10,
                fontFamily: 'Varela Round',
                backgroundColor: 'rgba(63, 193, 201, 1)',
                titleFontStyle: 'normal',
                titleMarginBottom: 15,
                titleFontSize: 18,
                bodyFontSize: 18,
                bodyFontFamily: 'Varela Round',
                bodyFontColor: 'rgba(252, 81, 133, 1)',
                bodyFontStyle: 'bold',
                displayColors: false,
                titleFontFamily: 'Varela Round',
                callbacks: {
                    label: function (tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label + ': £' + tooltipItems.yLabel + ' P/M';
                    }
                }
            }
        }
    };
    var forecast_chart = new Chart(ctx, config);
</script>
{{--<button id="0" type="button" class="chartbtn buttonslim b2">Daily</button>--}}
{{--<button id="1" type="button" class="chartbtn buttonslim b2">Weekly</button>--}}
{{--<button id="2" type="button" class="chartbtn buttonslim b2">Monthly</button>--}}