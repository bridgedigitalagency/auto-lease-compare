@extends('layouts.app')
@section('template_title') Page not found @endsection
@section('content')
    <div class="container-fluid banner">
        <div class="row justify-content-md-center">
            <div class="col-lg-6 col-md-12">
                <h1>Deal Expired</h1>
                The page you were looking for cannot be found
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                @role('admin')

                    @if( app('request')->input('del') == '1')
                    <div class="alert alert-info" role="alert">
                        This deal has been removed
                    </div>
                    @endif
                @endrole
                <div class="message mb-4">
                    <h2 class="mb-4">The deal you have requested has expired.</h2>

                    <a class="button b6" href="/">Return to homepage </a>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="/css/listing.css">
    <style>
        .message {
            margin: 0 0 20px;
        }
        .banner{
            background: #364f6b !important;
            padding: 44px 20px;
        }
        .car {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            margin: 0 auto;
            position: absolute;
            width: 319px;
            height: 172px;
            overflow: hidden;
            right: 0px;
            top: 130px;
        }

        .carbody {
            animation: carmove 3.1s infinite linear;
            background: url('/images/bmw3.jpg') 0 0;
            background-size: cover;
            height: 145px;
            position: relative;
            width: 346px;
            z-index: 125;
        }

        .weel {
            animation: wheel 0.7s infinite linear;
            background: url('/images/wheelbmw3.png') 0 0;
            height: 68px;
            position: absolute;
            top: 45%;
            width: 68px;
            z-index: 200;
            background-size: cover;
        }

        .weel1 {
            left: 36px;
        }

        .weel2 {
            left: 441px;
        }

        /*animations*/
        @keyframes carmove {
            0% {
                transform: translateY(0px);
            }
            25% {
                transform: translateY(1px)
            }
            29% {
                transform: translateY(2px)
            }
            33% {
                transform: translateY(3px)
            }
            47% {
                transform: translateY(0px)
            }
            58% {
                transform: translateY(1px)
            }
            62% {
                transform: translateY(2px)
            }
            66% {
                transform: translateY(3px)
            }
            75% {
                transform: translateY(1px)
            }
            100% {
                transform: translateY(0px)
            }
        }

        @keyframes wheel {
            0% {
                transform: rotate(0deg)
            }
            100% {
                transform: rotate(-359deg)
            }
        }
    </style>
@endsection
