@if($hide == 0)
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script>

        var dayslabels = [@foreach($cars as $model) @if ($loop->first)@foreach(($model['priceHistory']) as $hist) '<?= date("d/m", strtotime($hist->updated_at)) ?>',@endforeach @endif @endforeach];
        var daysdata = [@foreach($cars as $model){
            type: 'line',
            backgroundColor: '@if($loop->first)rgba(63, 193, 201, 1)@elseif($loop->last)rgba(252, 79, 131, 1)@else rgba(252, 154, 79, 1)@endif',
            borderColor: '@if($loop->first)rgba(63, 193, 201, 1)@elseif($loop->last)rgba(252, 79, 131, 1)@else rgba(252, 154, 79, 1)@endif',
            label: 'Daily Prices For: {{$model['maker']}} {{$model['range']}}',
            yAxisID: 'y-axis-0',
            fill: false,
            data: [@foreach(($model['priceHistory']) as $hist) {{$hist->monthly_payment}} @if($loop->last)@else,@endif @endforeach],
            spanGaps: true,
        }@if($loop->last)@else,@endif @endforeach];

        $(document).ready(function() {

            $('#0').click(function() {
                $('#forecast').remove(); // this is my <canvas> element
                $('#canvasContainer').append('<canvas id="forecast" width="400" height="250"></canvas>');

                var dayslabels = [@foreach($cars as $model)@if ($loop->first) @foreach(($model['priceHistory']) as $hist) '<?= date("d/m", strtotime($hist->updated_at)) ?>',@endforeach @endif @endforeach];
                var daysdata = [@foreach($cars as $model){
                    type: 'line',
                    backgroundColor: '@if($loop->first)rgba(63, 193, 201, 1)@elseif($loop->last)rgba(252, 79, 131, 1)@else rgba(252, 154, 79, 1)@endif',
                    borderColor: '@if($loop->first)rgba(63, 193, 201, 1)@elseif($loop->last)rgba(252, 79, 131, 1)@else rgba(252, 154, 79, 1)@endif',
                    label: 'Daily Prices For: {{$model['maker']}} {{$model['range']}}',
                    yAxisID: 'y-axis-0',
                    fill: false,
                    data: [@foreach(($model['priceHistory']) as $hist) {{$hist->monthly_payment}} @if($loop->last)@else,@endif @endforeach],
                    spanGaps: true,
                    maxTicksLimit: 20,
                }@if($loop->last)@else,@endif @endforeach];
                var ctx = document.getElementById('forecast').getContext('2d');
                var forecast_chart = new Chart(ctx, config);

            });

            $('#1').click(function() {
                $('#forecast').remove(); // this is my <canvas> element
                $('#canvasContainer').append('<canvas id="forecast" width="400" height="250"></canvas>');
                var dayslabelsweekly = [@foreach($cars as $model)@if ($loop->first) @foreach(($model['priceHistoryMonthly']) as $hist) @if(date('N', strtotime($hist->updated_at)) == 1) '<?= date("d/m", strtotime($hist->updated_at)) ?>', @endif @endforeach @endif @endforeach];
                var daysdataweekly = [@foreach($cars as $model){
                    type: 'line',
                    backgroundColor: '@if($loop->first)rgba(63, 193, 201, 1)@elseif($loop->last)rgba(252, 79, 131, 1)@else rgba(252, 154, 79, 1)@endif',
                    borderColor: '@if($loop->first)rgba(63, 193, 201, 1)@elseif($loop->last)rgba(252, 79, 131, 1)@else rgba(252, 154, 79, 1)@endif',
                    label: 'Weekly Prices For: {{$model['maker']}} {{$model['range']}}',
                    yAxisID: 'y-axis-0',
                    fill: false,
                    data: [@foreach(($model['priceHistoryMonthly']) as $hist)@if(date('N', strtotime($hist->updated_at)) == 1){{$hist->monthly_payment}}  @if($loop->last)@else,@endif @endif @endforeach],
                    spanGaps: true,
                    maxTicksLimit: 20,
                }@if($loop->last)@else,@endif @endforeach];
                var configweekly = {
                    type: 'line',
                    scaleOverride : true,
                    scaleSteps : 10,
                    scaleStepWidth : 50,
                    scaleStartValue : 0,
                    data: {
                        labels: dayslabelsweekly,
                        datasets: daysdataweekly,
                    },


                    options: {
                        scales: {
                            scaleSteps : 100,
                            yAxes: [{
                                ticks: {
                                    beginAtZero: false,
                                    // stepSize: 5,
                                    steps: 2,
                                    stepSize: @foreach($cars as $model) {{round((($model['priceHigh'] + 40) - ($model['priceLow'] - 40)) / 5)}}@endforeach,
                                    suggestedMin: @foreach($cars as $model) @if($model['finance_type'] == 'B') {{$model['priceLow'] - 40}} @else {{$model['priceLow'] - 30}} @endif @endforeach,
                                    suggestedMax: @foreach($cars as $model) {{$model['priceHigh'] + 40}} @endforeach,
                                    fontColor: 'rgba(252, 81, 133, 1)',
                                    fontSize: '16',
                                    fontStyle: "bold",
                                    fontFamily: 'Varela Round',
                                    padding: 20,
                                    // Include a dollar sign in the ticks

                                    userCallback: function (label, index, labels) {
                                        // when the floored value is the same as the value we have a whole number
                                        if (Math.floor(label) === label) {
                                            return '£' + label;
                                        }
                                    },
                                }
                            }],
                            xAxes: [{
                                /* For changing color of x-axis coordinates */
                                ticks: {
                                    fontColor: 'rgba(34, 43, 56, 1)',
                                    fontSize: '14',
                                    fontStyle: "bold",
                                    fontFamily: 'Varela Round'
                                }
                            }]
                        },
                        legend: {
                            display: false
                        },
                        tooltips: {
                            cornerRadius: 4,
                            caretSize: 4,
                            xPadding: 16,
                            yPadding: 10,
                            fontFamily: 'Varela Round',
                            backgroundColor: 'rgba(63, 193, 201, 1)',
                            titleFontStyle: 'normal',
                            titleMarginBottom: 15,
                            titleFontSize: 18,
                            bodyFontSize: 18,
                            bodyFontFamily: 'Varela Round',
                            bodyFontColor: 'rgba(252, 81, 133, 1)',
                            bodyFontStyle: 'bold',
                            displayColors: false,
                            titleFontFamily: 'Varela Round',
                            callbacks: {
                                label: function (tooltipItems, data) {
                                    return '£' + tooltipItems.yLabel + ' P/M';
                                }
                            }
                        }
                    }
                };
                var ctxweekly = document.getElementById('forecast').getContext('2d');
                var forecast_chart = new Chart(ctxweekly, configweekly);

            });
            $('#2').click(function() {

                $('#forecast').remove(); // this is my <canvas> element
                $('#canvasContainer').append('<canvas id="forecast" width="400" height="250"></canvas>');

                var dayslabelsmonthly = [@foreach($cars as $model) @if ($loop->first) @foreach(($model['priceHistoryMonthly']) as $hist) @if(date('d', strtotime($hist->updated_at)) == date('d')) '{{date("d/m/Y", strtotime($hist->updated_at)) }}', @endif @endforeach @endif @endforeach];
                var daysdatamonthly = [@foreach($cars as $model){
                    type: 'line',
                    backgroundColor: '@if($loop->first)rgba(63, 193, 201, 1)@elseif($loop->last)rgba(252, 79, 131, 1)@else rgba(252, 154, 79, 1)@endif',
                    borderColor: '@if($loop->first)rgba(63, 193, 201, 1)@elseif($loop->last)rgba(252, 79, 131, 1)@else rgba(252, 154, 79, 1)@endif',
                    label: 'Monthly Prices For: {{$model['maker']}} {{$model['range']}}',
                    yAxisID: 'y-axis-0',
                    fill: false,
                    data: [@foreach(($model['priceHistoryMonthly']) as $hist) @if(date('d', strtotime($hist->updated_at)) == date('d'))  {{$hist->monthly_payment}} @if($loop->last)@else,@endif @endif @endforeach],
                    spanGaps: true,
                }@if($loop->last)@else,@endif @endforeach];
                var configmonthly = {
                    type: 'line',
                    scaleOverride : true,
                    scaleSteps : 10,
                    scaleStepWidth : 50,
                    scaleStartValue : 0,
                    data: {
                        labels: dayslabelsmonthly,
                        datasets: daysdatamonthly,
                    },

                    options: {
                        scales: {
                            scaleSteps : 100,

                            yAxes: [{
                                ticks: {
                                    beginAtZero: false,
                                    steps: 10,
                                    stepSize: @foreach($cars as $model) {{round((($model['priceHigh'] + 20) - ($model['priceLow'] - 20)) / 10)}} @endforeach,
                                    fontColor: 'rgba(252, 81, 133, 1)',
                                    fontSize: '16',
                                    fontStyle: "bold",
                                    fontFamily: 'Varela Round',
                                    // Include a dollar sign in the ticks

                                    userCallback: function (label, index, labels) {
                                        // when the floored value is the same as the value we have a whole number
                                        if (Math.floor(label) === label) {
                                            return '£' + label;
                                        }
                                    },
                                }
                            }],
                            xAxes: [{
                                /* For changing color of x-axis coordinates */
                                ticks: {
                                    fontColor: 'rgba(34, 43, 56, 1)',
                                    fontSize: '14',
                                    fontStyle: "bold",
                                    fontFamily: 'Varela Round'
                                }
                            }]
                        },
                        legend: {
                            display: false
                        },
                        tooltips: {
                            cornerRadius: 4,
                            caretSize: 4,
                            xPadding: 16,
                            yPadding: 10,
                            fontFamily: 'Varela Round',
                            backgroundColor: 'rgba(63, 193, 201, 1)',
                            titleFontStyle: 'normal',
                            titleMarginBottom: 15,
                            titleFontSize: 18,
                            bodyFontSize: 18,
                            bodyFontFamily: 'Varela Round',
                            bodyFontColor: 'rgba(252, 81, 133, 1)',
                            bodyFontStyle: 'bold',
                            displayColors: false,
                            titleFontFamily: 'Varela Round',
                            callbacks: {
                                label: function (tooltipItems, data) {
                                    return '£' + tooltipItems.yLabel + ' P/M';
                                }
                            }
                        }
                    }
                };
                var ctxmonthly = document.getElementById('forecast').getContext('2d');
                var forecast_chart = new Chart(ctxmonthly, configmonthly);
            });
        });
    </script>


    <div id="canvasContainer">
        <canvas id="forecast" width="400" height="250"></canvas>
    </div>
    <script>
        var ctx = document.getElementById('forecast').getContext('2d');
        var config = {

            type: 'linear',
            data: {
                labels: dayslabels,
                datasets: daysdata,
            },

            options: {
                scales: {
                    scaleSteps : 10,

                    yAxes: [{
                        ticks: {
                            beginAtZero: false,
                            stepSize: @foreach($cars as $model) {{round((($model['priceHigh'] + 20) - ($model['priceLow'] - 20)) / 10)}} @endforeach,
                            steps: 10,
                            suggestedMin: @foreach($cars as $model) {{$model['priceLow'] - 20}} @endforeach,
                            suggestedMax: @foreach($cars as $model) {{$model['priceHigh'] + 20}} @endforeach,
                            autoskip: false,
                            fontColor: 'rgba(252, 81, 133, 1)',
                            fontSize: '16',
                            fontStyle: "bold",
                            fontFamily: 'Varela Round',
                            padding: 0,

                            // Include a dollar sign in the ticks

                            userCallback: function (label, index, labels) {
                                // when the floored value is the same as the value we have a whole number
                                if (Math.floor(label) === label) {
                                    return '£' + label;
                                }
                            },
                        }
                    }],
                    xAxes: [{
                        /* For changing color of x-axis coordinates */
                        ticks: {
                            fontColor: 'rgba(34, 43, 56, 1)',
                            fontSize: '14',
                            fontStyle: "bold",
                            fontFamily: 'Varela Round'
                        }
                    }]
                },
                legend: {

                    display: false
                },
                tooltips: {
                    cornerRadius: 4,
                    caretSize: 4,
                    xPadding: 16,
                    yPadding: 10,
                    fontFamily: 'Varela Round',
                    backgroundColor: 'rgba(63, 193, 201, 1)',
                    titleFontStyle: 'normal',
                    titleMarginBottom: 15,
                    titleFontSize: 18,
                    bodyFontSize: 18,
                    bodyFontFamily: 'Varela Round',
                    bodyFontColor: 'rgba(252, 81, 133, 1)',
                    bodyFontStyle: 'bold',
                    displayColors: false,
                    titleFontFamily: 'Varela Round',
                    callbacks: {
                        label: function (tooltipItems, data) {
                            return '£' + tooltipItems.yLabel + ' P/M';
                        }
                    }
                }
            }
        };
        var forecast_chart = new Chart(ctx, config);
    </script>

    <button id="0" type="button" class="chartbtn buttonslim b2">Daily</button>
    @if($showWeekly == 1)<button id="1" type="button" class="chartbtn buttonslim b2">Weekly</button>@endif
    @if($showMonthly == 100)<button id="2" type="button" class="chartbtn buttonslim b2">Monthly</button>@endif

@else
    <p>We're building a price history for this vehicle. Check back soon!</p>
@endif