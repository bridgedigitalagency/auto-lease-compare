@extends('layouts.app')

@section('content')

    @if($confirm_quote->deal_id == 'Just Drive')
        <script type='text/javascript'src="https://track.omguk.com/2103151/e/ss/?APPID={{$confirm_quote->id}}&AID=2136424&MID=2103151&PID=39626&Status=&Action=Lead&ex1={{$confirm_quote->make}}&ex2={{$confirm_quote->model}}&Channel=optimise"></script><noscript><img src="https://track.omguk.com/e/si/?APPID={{$confirm_quote->id}}&AID=2136424&MID=2103151&PID=39626&Status=&Action=Lead&ex1={{$confirm_quote->make}}&ex2={{$confirm_quote->model}}&Channel=optimise" border="0" height="1" width="1"></noscript>
    @endif
    <div class="container-fluid b3 mb-5">
        <div class="row">
            <div class="col-md-12 pt-5 pb-5">
                <h1 style="color:#fff; text-align: center">Thank You For Using Auto Lease Compare</h1>
                <div style="text-align:center; color:#fff;" class="breadc"><a href="/" style="color:#fff;">Home</a> / Thanks</div>
            </div>
        </div>
    </div>

    <section>
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-sm-12 text-center">
                    <div class="jumbotron thankyouJumbo">
                        <p class="lead">Your message has been sent to <span class="dealer">{{$confirm_quote->deal_id}}</span>, they will contact you as soon as possible!</p>
                        <p>Your Enquiry ID is ALC{{$confirm_quote->id}}</p>
                        <hr class="my-4">


                        @guest
                            @if($userAvail == 1)
                                <p>
                                    We have found an account registered with us with the email address you used.
                                </p>
                                <p>
                                    Your quote will automatically be logged against your account for when you login again.
                                </p>
                                <p class="lead">
                                    <a data-toggle="modal" data-target="#login" class="button b3" href="#" role="button">Login Now</a>
                                </p>
                            @else
                                <div class="col-sm-12 text-center">@include('partials.register-password')</div>
                            @endif
                        @else
                            <p>
                                You can view the details of your quote within your dashboard.
                            </p>
                            <p class="lead">
                                <a href="/my-account/enquiries" class="button b3">View quotes</a>
                            </p>
                        @endguest
                    </div>
                </div>
            </div>

            <div class="row justify-content-md-center">

                <div class="col-md-6 mt-4 mb-4 goCompare">
                    @if($confirm_quote->database == 'LCV')
                        <h2>Need van insurance?</h2>
                        <p>Whether it's for business or personal use, you can compare van insurance with GoCompare in one, simple search. When it matters, GoCompare.</p>
                        <a href="https://clk.omgt1.com/?PID=35052&AID=2136424" target="_blank">
                            <img src="/images/goCompare.jpg" alt="Compare Van insurance with Go Compare" class="img-responsive">
                        </a>
                    @else
                        <h2>Need car insurance?</h2>
                        <p>Don’t pay more than you should for your car insurance. Compare with GoCompare and find car insurance quotes from insurers that'll cover every journey. When it matters, GoCompare.</p>
                        <a href="https://clk.omgt1.com/?PID=35050&AID=2136424" target="_blank">
                            <img src="/images/goCompare.jpg" alt="Compare car insurance with Go Compare" class="img-responsive">
                        </a>
                    @endif
                </div>

                <div class="col-md-6 mt-4 mb-4">
                    <h2>Need to sell your old car?</h2>
                    <p>Wizzle uses cutting edge technology to help you sell your car. Their industry leading ‘car appraisal tool’ means you can show off your car to thousands of dealers around the country and compare the best prices.</p>
                    <a href="/sell-your-car">
                        <img src="/images/sellyourcarhorizontal.jpg" alt="sell your car" class="img-responsive">
                    </a>
                </div>
            </div>



            <div class="row justify-content-md-center row-eq-height">

                @php

                    if(strpos($confirm_quote->name, ' ')) {
                        $name = explode(' ', $confirm_quote->name);
                        $fname = $name[0];
                        $sname = $name[1];
                    }else{
                        $fname = $confirm_quote->name;
                        $sname = '';
                    }


                    $email = $confirm_quote->email;
                    $phone = $confirm_quote->phone_number;

                $url = 'https://secure.uk.rspcdn.com/xprr/red/PID/2673/SID/n/SID2/n/SID3/typ?SID2=n&title=&fname='.$fname.'&lname='.$sname.'&email='.$email.'&straddr=&city=&zip=&dbd=&dbm=&dby=&phone='.$phone.'&AffiliateReferenceID=&submit=';
                @endphp

                <div class="col-md-6 mt-4 mb-4">
                    <div style="background: #fff; padding: 1rem;">
                        <h2>Check your Credit Report?</h2>
                        <p style="text-align: center; padding-top: 10px;"><img src="/images/capImage.jpg" style="max-width: 50%;"></p>
                        <p>
                            It’s important to check your credit report, as 1 in 3 contain errors which can negatively impact your score and stop you being accepted for your new vehicle. A healthy credit report and score will help you get accepted for finance. This is not a mandatory step, but we recommend getting a FREE* copy from our partner UK Credit Ratings, they have credit agents on hand to answer any questions via freephone and live chat, plus you can correct any errors on their live portal.
                        </p>
                        <div class="creditScoreLink" style="text-align: center">
                            <p>
                                <a href="{{$url}}" target="_blank" class="button b3">Get Your FREE* Credit Report</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-4 mb-4">
                    <div style="background: #fff; padding: 1rem; height: 100%;">
                        <h2>Need to repair your existing vehicle?</h2>
                        <p style="text-align: center; margin: 30px 0;"><img src="images/dwvlogo.svg" style="max-width: 75%; text-align: center;"></p>
                        <p>DWV is one of the largest specialist mobile SMART, dent removal and alloy wheel repairers in the UK. This means that if you need minor paint scratches, scuffs, stone chips, dents or alloy wheels repairing - they can fix your vehicle directly at your home or office with great customer service and competitive pricing.</p>
                        <p style="text-align: center;">
                            <a href="/vehicle-repair" target="_blank" class="button b3">Get a quote today</a>
                        </p>
                    </div>
                </div>
            </div>
    </section>

@endsection




