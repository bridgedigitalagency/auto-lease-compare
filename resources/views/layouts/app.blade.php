<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>
        @yield('template_description')

        <meta name="author" content="Auto Lease Compare">
        <meta name="format-detection" content="telephone=no">

        <link rel="shortcut icon" href="/favicon.ico">
        <link rel="canonical" href="https://www.autoleasecompare.com{{$_SERVER['REQUEST_URI']}}" />

        <!-- TrustBox script -->
        <script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
        <!-- End TrustBox script -->

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-NGGHTKP');</script>
        <!-- End Google Tag Manager -->

        @yield('extra_meta_tags')
        {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        {{-- Fonts --}}
        @yield('template_linked_fonts')
        @php
            $rand = rand(0,9999);
            $rand2 = rand(0,9999);
        @endphp

        {{-- Styles --}}
        <link href="{{ mix('/css/app.css') }}?s={{$rand}}" rel="stylesheet">
        <link rel="stylesheet" href="/css/site.css?s={{$rand2}}">

        <link rel="stylesheet" href="/css/components.css?s={{$rand}}">
        <link rel="stylesheet" href="/owlcarousel/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="/owlcarousel/assets/owl.theme.default.min.css">

        @yield('template_linked_css')

        <style type="text/css">
            @yield('template_fastload_css')

            @if (Auth::User() && (Auth::User()->profile) && (Auth::User()->profile->avatar_status == 0))
                .user-avatar-nav {
                    background: url({{ Gravatar::get(Auth::user()->email) }}) 50% 50% no-repeat;
                    background-size: auto 100%;
                }
            @endif

        </style>

        {{-- Scripts --}}
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>

        @if (Auth::User() && (Auth::User()->profile) && $theme->link != null && $theme->link != 'null')
            <link rel="stylesheet" type="text/css" href="{{ $theme->link }}">
        @endif

        @yield('head')
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '2217410978319688');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2217410978319688&ev=PageView&noscript=1"/></noscript>

        <script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"17263507"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>

        <!-- live chat -->
        <script type="text/javascript">
            window.lhnJsSdkInit = function () {
                lhnJsSdk.setup = {
                    application_id: "25cb2181-be71-4c5a-81d9-bdc274e3befd",
                    application_secret: "8loorouea+9aajgb5x/hxk0kjsjxmqbe58odpztpn6fkdzleom"
                };
                lhnJsSdk.controls = [{
                    type: "hoc",
                    id: "d5c52811-ac40-443c-9531-bffd10943385"
                }];
            };

            (function (d, s) {
                var newjs, lhnjs = d.getElementsByTagName(s)[0];
                newjs = d.createElement(s);
                newjs.src = "https://developer.livehelpnow.net/js/sdk/lhn-jssdk-current.min.js";
                lhnjs.parentNode.insertBefore(newjs, lhnjs);
            }(document, "script"));
        </script>
        <!-- end live chat -->

        <!-- Hotjar Tracking Code for https://www.autoleasecompare.com/ -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:1916369,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
        <meta name="OMG-Verify-V1" content="294684ff-8d54-4c91-888f-a9d936c55574" />
        <meta name="facebook-domain-verification" content="0zwccc99dxt7doz37bonwnong2oqc8" />
    </head>
    <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NGGHTKP"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
        <div id="app">

            @include('partials.nav')

            <main class="">
{{--                <a href="#" style="display: block; clear: both; width: 100%;">--}}
{{--        <span class="alert alert-danger" role="alert" data-toggle="modal" data-target="#covidBanner" style="display: block; width: 100%; margin: 0; text-align: center;">--}}
{{--        COVID-19 – What does it mean for my new lease car?--}}
{{--        </span>--}}
{{--                </a>--}}



{{--                <div class="modal fade" id="covidBanner" tabindex="-1" role="dialog" aria-labelledby="covidBannerLabel" aria-hidden="true">--}}
{{--                    <div class="modal-dialog" role="document">--}}
{{--                        <div class="modal-content">--}}
{{--                            <div class="modal-header">--}}
{{--                                <h5 class="modal-title" id="exampleModalLabel">COVID-19 – What does it mean for my new lease car?</h5>--}}
{{--                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                    <span aria-hidden="true">&times;</span>--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                            <div class="modal-body">--}}
{{--                                <p><strong>First and foremost, we wish all of our customers & partners the best of health in these uncertain times and we urge you to stay at home, don’t travel unnecessarily and to follow the latest advice on the government website.</strong></p>--}}
{{--                                <p>A handful of leasing providers have decided to close their doors due to the current situation, however we will only display the deals of providers who are still open or trading.</p>--}}
{{--                                <p>The British Vehicle Rental & Leasing Association (BVRLA) published a press released which confirmed that “The Department for Transport has made it clear in a letter to the logistics sector, that logistics, including the collection and delivery of vehicles, should carry on during the lockdown, provided that it can be done in accordance with Coronavirus safety guidelines.”</p>--}}
{{--                                <p>Please remember that we are a comparison site so please confirm lead times when making an enquiry with our leasing providers.</p>--}}
{{--                                <p>Best wishes,<br>--}}
{{--                                    Auto Lease Compare Team</p>--}}
{{--                            </div>--}}
{{--                            <div class="modal-footer">--}}
{{--                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
                @yield('content')

            </main>

        </div>

        @include('partials.footer')
        {{-- Scripts --}}
        <script src="{{ mix('/js/app.js') }}"></script>

        @if(config('settings.googleMapsAPIStatus'))
            {!! HTML::script('//maps.googleapis.com/maps/api/js?key='.config("settings.googleMapsAPIKey").'&libraries=places&dummy=.js', array('type' => 'text/javascript')) !!}
        @endif

        @yield('footer_scripts')
        @stack('scripts')

        <!-- Modal -->
        <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="pl-3">
                        @include('partials.login')
                    </div>
                </div>
            </div>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="register" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="pl-3">
                        @include('partials.register')
                    </div>
                </div>
            </div>
        </div>
    <script src="/js/site.js"></script>
    <script type="text/javascript">
        var __raconfig = __raconfig || {};
        __raconfig.uid = '607070bd95f2c';
        __raconfig.action = 'track';
        (function () {
            var ra = document.createElement('script');
            ra.type = 'text/javascript';
            ra.src = 'https://ruler.nyltx.com/lib/1.0/ra-bootstrap.min.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ra, s);
        }());
    </script>
    </body>
</html>
