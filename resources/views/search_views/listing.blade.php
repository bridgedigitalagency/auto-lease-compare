@extends('layouts.app')
@section('content')

    <div class="container-fluid banner">
        <div class="row">
            <div class="col-md-12">
                <h1>{{$pageName}}</h1>
            </div>
        </div>
    </div>

    <div class="container listingContainer">

    </div>
@endsection
@section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="/css/listing.css">
@endsection
@push('scripts')

@endpush