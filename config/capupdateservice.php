<?php

return [

    'clients' => [
        'cap_hpi' => [
            'base_wsdl' => [
                'vehicles' => [
                    'url' => 'https://soap.cap.co.uk/Vehicles/CapVehicles.asmx',
                    'method' => 'POST',
                ],
                'vrm' => [
                    'url' => 'https://soap.cap.co.uk/vrm/capvrm.asmx',
                    'method' => 'POST',
                ],
                'dvla' => [
                    'url' => 'https://soap.cap.co.uk/dvla/capdvla.asmx',
                    'method' => 'POST',
                ],
                'nvd' => [
                    'url' => 'https://soap.cap.co.uk/nvd/capnvd.asmx',
                    'method' => 'POST',
                ],
                'p11d' => [
                    'url' => 'https://soap.cap.co.uk/p11d/capp11d.asmx',
                    'method' => 'POST',
                ],
                'legislation' => [
                    'url' => 'https://soap.cap.co.uk/legislation/caplegislation.asmx',
                    'method' => 'POST',
                ],
                'used_values' => [
                    'url' => 'https://soap.cap.co.uk/usedvalues/capusedvalues.asmx',
                    'method' => 'POST',
                ],
                'used_values_live' => [
                    'url' => 'https://soap.cap.co.uk/usedvalueslive/capusedvalueslive.asmx',
                    'method' => 'POST',
                ],
                'future_values' => [
                    'url' => 'https://soap.cap.co.uk/futurevalues/capfuturevalues.asmx',
                    'method' => 'POST',
                ],
                'smr' => [
                    'url' => 'https://soap.cap.co.uk/Smr/capsmr.asmx',
                    'method' => 'POST',
                ],
                'tco' => [
                    'url' => 'https://soap.cap.co.uk/tco/captco_webservice.asmx',
                    'method' => 'POST',
                ],
                'images' => [
                    'url' => 'https://soap.cap.co.uk/images/VehicleImage.aspx',
                    'method' => 'GET',
                ],
                'params' => [
                    'api_key' => env('CAP_SUB_ID'),
                    'api_password' => env('CAP_SUB_PASSWORD'),
                ]
            ],
        ],
        'CAD'=> [
            'images' => [
                'url' => 'https://www.caranddriving.com/ws/A041/ws.asmx',
                'method' => 'POST ',
            ],
            'params' => [
                'api_key' => env('CAD_USER_ID'),
            ]
        ],
    ],
];
