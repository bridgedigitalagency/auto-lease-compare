<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
| Middleware options can be located in `app/Http/Kernel.php`
|
*/

// Homepage Route
Route::group(['middleware' => ['web', 'checkblocked']], function () {
    Route::get('/', 'WelcomeController@welcome')->name('welcome');
    Route::get('/vans', 'WelcomeController@welcome_vans');

    Route::get('/heartbeat', 'WelcomeController@viewHeartbeat');

    // Car leasing routes
    Route::get('/personal', 'LeasingController@viewPersonal');
    Route::get('/business', 'LeasingController@viewBusiness');
    Route::get('/performance', 'LeasingController@viewPerformance');
    Route::get('/hybrid', 'LeasingController@viewHybrid');

    Route::get('/quote/404', 'QuoteController@viewDeal404');
    Route::get('/quote/enquiry/404', 'QuoteController@viewDeal404');
    Route::get('/quote/enquiry/{id}/', 'QuoteController@view_quote')->name('Quote');
    Route::get('/quote/enquiry/{id}/{database}', 'QuoteController@view_quote')->name('Quote');

    Route::get('/leasing-information/', 'WelcomeController@info')->name('Leasing Information');
    Route::get('/sell-your-car/', 'WelcomeController@sell')->name('Sell Your Car');
    Route::get('/thankyou', 'QuoteController@thankyou')->name('Thank You');

    Route::post('quote', 'QuoteController@store');

    Route::get('/personal/results', 'LeasingController@viewResults');
    Route::get('/personal/filters', 'LeasingController@viewFilters');
    Route::get('/personal/counter', 'LeasingController@viewCounter');
    Route::get('/personal/filters/mobile', 'LeasingController@viewMobileFilters');


    Route::get('/home/manusearch', 'WelcomeController@viewHomeSearch');
    Route::get('/personal/trimsearch', 'LeasingController@getMobileTrims');

    Route::get('/business/results', 'LeasingController@viewResults');
    Route::get('/business/filters', 'LeasingController@viewFilters');
    Route::get('/business/counter', 'LeasingController@viewCounter');
    Route::get('/business/filters/mobile', 'LeasingController@viewMobileFilters');

    Route::get('/performance/results', 'LeasingController@viewResults');
    Route::get('/performance/filters', 'LeasingController@viewFilters');
    Route::get('/performance/counter', 'LeasingController@viewCounter');
    Route::get('/performance/filters/mobile', 'LeasingController@viewMobileFilters');

    Route::get('/hybrid/results', 'LeasingController@viewResults');
    Route::get('/hybrid/filters', 'LeasingController@viewFilters');
    Route::get('/hybrid/counter', 'LeasingController@viewCounter');
    Route::get('/hybrid/filters/mobile', 'LeasingController@viewMobileFilters');

    // Redirect adverse credit
    Route::redirect('/adverse-credit', '/adverse', 302);
    Route::redirect('/adverse-credit/results', '/adverse', 302);
    Route::redirect('/adverse-credit/filters', '/adverse', 302);
    Route::redirect('/adverse-credit/filters/mobile', '/adverse', 302);
    Route::get('/adverse', 'LeasingController@viewAdverseTemp');

//    Route::get('/adverse-credit', 'LeasingController@viewAdverse');
//    Route::get('/adverse-credit/results', 'LeasingController@viewResults');
//    Route::get('/adverse-credit/filters', 'LeasingController@viewFilters');
//    Route::get('/adverse-credit/filters/mobile', 'LeasingController@viewMobileFilters');


    // Van routes
    Route::get('/personal-vans', 'LeasingController@viewPersonalVans');
    Route::get('/personal-vans/results', 'LeasingController@viewResults');
    Route::get('/personal-vans/filters', 'LeasingController@viewFilters');
    Route::get('/personal-vans/filters/mobile', 'LeasingController@viewMobileFilters');

    Route::get('/business-vans', 'LeasingController@viewBusinessVans');
    Route::get('/business-vans/results', 'LeasingController@viewResults');
    Route::get('/business-vans/filters', 'LeasingController@viewFilters');
    Route::get('/business-vans/filters/mobile', 'LeasingController@viewMobileFilters');


    // Account Routes
    Route::get('/my-account/garage/add/{id}/{database}', 'GarageController@add_garage');
    Route::get('/my-account/garage/delete/{id}/{database}', 'GarageController@delete_garage');
    Route::get('/my-account/garage', 'GarageController@view_garage');
    Route::get('/my-account/garage/switchPriceDropNotifications', 'GarageController@switchPriceDropNotifications');

    Route::get('/car-leasing/{maker}', 'LeasingController@carLeasing');
    Route::get('/van-leasing/{maker}', 'LeasingController@vanLeasing');

    Route::get('/search/', 'SearchController@viewSearch');

    Route::get('/site/terms', 'WelcomeController@terms');
    Route::get('/site/cookies', 'WelcomeController@cookies');
    Route::get('/site/privacy', 'WelcomeController@privacy');
    Route::get('/site/partners', 'WelcomeController@partners');

    Route::post('/site/partners-submit', 'WelcomeController@logPartner');

    Route::get('/quote/pricehistory/{priceid}/{mileage}/{term}/{depositvalue}/{finance_type}', 'QuoteController@viewPriceHistory');
    Route::get('/quote/pricehistory/garage', 'QuoteController@viewGaragePriceHistory');

    Route::post('/api/ttnc/payload.php', 'CallsController@processPayload');

    Route::get('/updateVanCap', 'CapUpdateController@update_van_leasing');

});

// Authentication Routes
Auth::routes();

// Public Routes
Route::group(['middleware' => ['web', 'activity', 'checkblocked']], function () {
   
    // Activation Routesactivate
    Route::get('/activate', ['as' => 'activate', 'uses' => 'Auth\ActivateController@initial']);

    Route::get('/activate/{token}', ['as' => 'authenticated.activate', 'uses' => 'Auth\ActivateController@activate']);
    Route::get('/activation', ['as' => 'authenticated.activation-resend', 'uses' => 'Auth\ActivateController@resend']);
    Route::get('/exceeded', ['as' => 'exceeded', 'uses' => 'Auth\ActivateController@exceeded']);

    // Route to for user to reactivate their user deleted account.
    Route::get('/re-activate/{token}', ['as' => 'user.reactivate', 'uses' => 'RestoreUserController@userReActivate']);
});

// Registered and Activated User Routes
Route::group(['middleware' => ['auth', 'activated', 'activity', 'checkblocked']], function () {
    Route::get('/my-account', 'MyAccountController@hello')->name('My Account');
    Route::get('/my-account/dealers', 'DealerPricingController@view_dealers')->name('View Dealers');
    Route::get('/my-account/my-pricing', 'DealerPricingController@view_dealer_pricing')->name('View My Pricing');
    Route::get('/my-account/my-pricing/vans', 'DealerPricingController@view_dealer_van_pricing')->name('View My Pricing');
    Route::get('update-price', 'DealerPricingController@updatePrice')->name('update-price');
    Route::get('update-featured', 'LeasingController@updateFeatured')->name('update-featured');
    Route::get('/my-account/add-price', 'AddPriceController@addPrice')->name('Add Price');
    Route::get('/my-account/enquiries', 'EnquiriesController@viewEnquiries')->name('View Enquiries');
    Route::get('/my-account/enquiries/call-log', 'EnquiriesController@viewCalls')->name('View Calls');
    Route::get('/my-account/enquiries/search', 'EnquiriesController@searchEnquiries')->name('View Enquiries');
    Route::post('update-status', 'EnquiriesController@updateStatus')->name('update-status');
    Route::post('update-withdraw', 'EnquiriesController@updateWithdraw')->name('update-withdraw');
    Route::post('update-livechat', 'EnquiriesController@updateLiveChat')->name('update-livechat');
    Route::post('update-quote-sent', 'EnquiriesController@updateQuoteSent')->name('update-quote-sent');
    Route::post('update-sold', 'EnquiriesController@updateSold')->name('update-sold');
    Route::get('/my-account/export-quotes/', 'EnquiriesController@export');
    Route::get('/my-account/export-dwv/', 'EnquiriesController@exportdwv');
    Route::get('/my-account/export-users/', 'UserController@export');
    Route::get('/my-account/enquiries/call-log/export/', 'EnquiriesController@exportcalls');
    Route::get('/my-account/garage/', 'GarageController@view_garage')->name('garage');;

    Route::post('pricing', 'AddPriceController@store');

    // Activation Routes
    Route::get('/activation-required', ['uses' => 'Auth\ActivateController@activationRequired'])->name('activation-required');
    Route::get('/logout', ['uses' => 'Auth\LoginController@logout'])->name('logout');
});

// Registered and Activated User Routes
Route::group(['middleware' => ['auth', 'activated', 'activity', 'twostep', 'checkblocked']], function () {

    //  Homepage Route - Redirect based on user role is in controller.
//    Route::get('/home', ['as' => 'public.home',   'uses' => 'UserController@index']);
    Route::redirect('/home', '/my-account', 301);
    // Show users profile - viewable by other users.
    Route::get('profile/{username}', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@show',
    ]);
});

// Registered, activated, and is current user routes.
Route::group(['middleware' => ['auth', 'activated', 'currentUser', 'activity', 'twostep', 'checkblocked']], function () {

    // User Profile and Account Routes
    Route::resource(
        'profile',
        'ProfilesController', [
            'only' => [
                'show',
                'edit',
                'update',
                'create',
            ],
        ]
    );
    Route::put('profile/{username}/updateUserAccount', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@updateUserAccount',
    ]);
    Route::put('profile/{username}/updateUserPassword', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@updateUserPassword',
    ]);
    Route::delete('profile/{username}/deleteUserAccount', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@deleteUserAccount',
    ]);

    // Route to show user avatar
    Route::get('images/profile/{id}/avatar/{image}', [
        'uses' => 'ProfilesController@userProfileAvatar',
    ]);

    // Route to upload user avatar.

});

// Registered, activated, and is admin routes.
Route::group(['middleware' => ['auth', 'activated', 'role:admin', 'activity', 'twostep', 'checkblocked']], function () {

    Route::get('/my-account/admin-tools', 'MyAccountController@admin_tools');
    Route::get('/my-account/check-import-scripts', 'MyAccountController@check_imports');
    Route::resource('/users/deleted', 'SoftDeletesController', [
        'only' => [
            'index', 'show', 'update', 'destroy',
        ],
    ]);

    Route::resource('users', 'UsersManagementController', [
        'names' => [
            'index'   => 'users',
            'destroy' => 'user.destroy',
        ],
        'except' => [
            'deleted',
        ],
    ]);
    Route::post('search-users', 'UsersManagementController@search')->name('search-users');

    Route::resource('themes', 'ThemesManagementController', [
        'names' => [
            'index'   => 'themes',
            'destroy' => 'themes.destroy',
        ],
    ]);

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('routes', 'AdminDetailsController@listRoutes');
    Route::get('active-users', 'AdminDetailsController@activeUsers');
    Route::get('activity-log', 'QuoteController@viewActivityLog');

    Route::get('view-emails', 'EmailController@viewEmails');
    Route::get('view-emails/view/{template}', 'EmailController@viewEmail');
    Route::get('/quote/enquiry/{id}/{database}/delete', 'QuoteController@delete_quote')->name('Quote');
    Route::get('/quote/enquiry/{id}/{database}/feature', 'QuoteController@feature_quote')->name('Quote');


});

Route::redirect('/php', '/phpinfo', 301);
Route::get('/vehicle-repair/', 'PagesController@vehicleRepair');
Route::post('/post-vehicle-repair', 'PagesController@vehicleRepairPost');
//Route::get('car-leasing', 'PagesController@carleasing');
Route::post('image-upload', 'ProfilesController@imageUploadPost')->name('image.upload.post');
Route::get('/tracking/quote/{priceId}/{database}/{type}', 'QuoteController@tracking');
// Method to find missing C&D images. Local only
//Route::get('candd', 'QuoteController@candd');