<?php
/**
 * The base configuration for WordPress
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');


/** Load the Studio 24 WordPress Multi-Environment Config. */
require_once(ABSPATH . 'wp-config.load.php');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'alc_wp_';

define( 'WP_HOME', 'https://www.autoleasecompare.com' );
define( 'WP_SITEURL', 'https://www.autoleasecompare.com' );

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');