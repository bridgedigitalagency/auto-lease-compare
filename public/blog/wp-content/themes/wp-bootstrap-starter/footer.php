<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->



    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <h6>Popular Car Lease Searches</h6>
                    <ul>
                        <li><a href="/personal?maker%5B%5D=MERCEDES-BENZ">Mercedes-Benz Personal Leasing</a></li>
                        <li><a href="/personal?maker%5B%5D=AUDI">Audi Personal Leasing</a></li>
                        <li><a href="/personal?maker%5B%5D=FORD">Ford Personal Leasing</a></li>
                        <li><a href="/personal?maker%5B%5D=LAND%20ROVER">Land Rover Personal Leasing</a></li>
                        <li><a href="/personal?maker%5B%5D=VOLKSWAGEN">Volkswagen Personal Leasing</a></li>
                        <li><a href="/personal?maker%5B%5D=BMW">BMW Personal Leasing</a></li>
                        <li><a href="/personal?maker%5B%5D=VOLVO">Volvo Personal Leasing</a></li>
                        <li><a href="/personal?maker%5B%5D=HYUNDAI">Hyundai Personal Leasing</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6">
                    <h6>Popular Van Lease Searches</h6>
                    <ul>
                        <li><a href="/business-vans?maker[]=FORD&range[]=RANGER">Ford Ranger Lease</a></li>
                        <li><a href="/business-vans?maker[]=FORD&range[]=TRANSIT%20CUSTOM">Ford Transit Custom Lease</a></li>
                        <li><a href="/business-vans?maker[]=VOLKSWAGEN&range[]=CADDY">VW Caddy Lease</a></li>
                        <li><a href="/business-vans?maker[]=MERCEDES-BENZ&range[]=SPRINTER">Mercedes Sprinter Lease</a></li>
                        <li><a href="/business-vans?maker[]=PEUGEOT&range[]=EXPERT">Peugeot Expert Lease</a></li>
                        <li><a href="/business-vans?maker[]=CITROEN&range[]=DISPATCH">Citroen Dispatch Lease</a></li>
                        <li><a href="/business-vans?maker[]=FORD&range[]=TRANSIT">Ford Transit Lease</a></li>
                        <li><a href="/business-vans?maker[]=RENAULT">Renault Lease</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6">
                    <h6>Useful Links</h6>
                    <ul>
                        <li><a href="/business">Business Lease Deals</a></li>
                        <li><a href="/personal">Personal Lease Deals</a></li>
                        <li><a href="/vans">Van Leasing</a></li>
                        <li><a href="/site/partners">Want to Advertise with us?</a></li>
                        <li><a href="/sell-your-car">Need to sell your car or van?</a></li>
                        <li><a href="/site/privacy">Privacy Policy</a></li>
                        <li><a href="/site/terms">Terms &amp; Conditions</a></li>
                        <li><a href="/site/cookies">Cookie Policy</a></li>
                    </ul>
                </div>

                <div class="col-md-3 col-sm-6">
                    <h6>SUBSCRIBE TO OUR HOT DEALS</h6>
                    <div class="newsletter-form">
                        <!-- Begin Mailchimp Signup Form -->
                        <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
                        <style type="text/css">
                            #mc_embed_signup{clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
                        </style>
                        <style type="text/css">
                            #mc-embedded-subscribe-form input[type=checkbox]{display: inline; width: auto;margin-right: 10px;}
                            #mergeRow-gdpr {margin-top: 20px;}
                            #mergeRow-gdpr fieldset label {font-weight: normal;}
                            #mc-embedded-subscribe-form .mc_fieldset{border:none;min-height: 0px;padding-bottom:0px;}
                        </style>
                        <div id="mc_embed_signup" style="text-align: left;">
                            <form action="https://autoleasecompare.us20.list-manage.com/subscribe/post?u=d022602b54b0ed1c1366d1b53&amp;id=4884dd7421" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" style="padding-top:0px;" target="_blank" novalidate="">
                                <div id="mc_embed_signup_scroll">

                                    <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" style="width: 100%; margin-bottom:10px;" required="">
                                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d022602b54b0ed1c1366d1b53_4884dd7421" tabindex="-1" value=""></div>
                                    <div class=""><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button" style="font-weight:600;background-color: #fc5185; width:100%;"></div>
                                </div>
                            </form>
                        </div>

                        <!--End mc_embed_signup-->
                        <p class="subscribed-text">*We send great deals and latest auto news to our subscribed users every week.</p>
                        <p>Connect with Us:</p>
                        <div class="footer_widget">
                            <ul>
                                <li><a href="https://facebook.com/autoleasecompare" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                                <li><a href="https://twitter.com/compare_lease" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/auto-lease-compare" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                                <li><a href="https://www.instagram.com/auto_lease_compare/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-6 text-right">
                    <div class="footer_widget">

                    </div>
                    <div class="footer_widget">

                    </div>
                </div>
                <div class="col-md-12" style="text-align:center;color: #fff;">
                    Auto Lease Compare Ltd is authorised and regulated by the Financial Conduct Authority: 822040<br>
                    We act as a credit broker not a lender. We can introduce you to a limited number of brokers and we may receive a commission payment from the broker if you decide to enter into an agreement through them.<br>
                    Auto Lease Compare Ltd - Registered in England and Wales. Company number: 11519767<br>
                    Data Protection Registration number: ZA469390<br>
                    VAT Number: 307521233<br>
                    Registered Office Address: Auto Lease Compare Ltd, 10 Waterson Street, London, E2 8HL<br>
                    Email: <a href="mailto:info@autoleasecompare.com">info@autoleasecompare.com</a>
                </div>

            </div>
        </div>
    </div>


<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>