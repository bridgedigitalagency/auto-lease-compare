<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>

    <?php if ( is_front_page() ) : ?>
        <link rel="stylesheet" type="text/css" href="/blog/wp-content/themes/wp-bootstrap-starter/homepage.css">
    <?php endif; ?>
    <link rel="stylesheet" type="text/css" href="/blog/wp-content/themes/wp-bootstrap-starter/blog.css">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <script src="/blog/wp-content/themes/wp-bootstrap-starter/blog.js"></script>
    <link rel="stylesheet" href="/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="/owlcarousel/assets/owl.theme.default.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="/owlcarousel/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function() {
            if ($('.owl-carousel').length) {
                $('.owl-carousel').owlCarousel({
                    loop:true,
                    responsiveClass:true,
                    autoplay:false,
                    autoplayTimeout:5000,
                    autoplayHoverPause:true,
                    responsive:{
                        0:{
                            items:1,
                            nav:false
                        },
                        600:{
                            items:2,
                            nav:false
                        },
                        992:{
                            items:2,
                            nav:false,
                        },
                        1200:{
                            items:3,
                            nav:false,
                        }
                    }
                })
            }
        });
    </script>
</head>

<body <?php body_class(); ?>>

<?php

// WordPress 5.2 wp_body_open implementation
if ( function_exists( 'wp_body_open' ) ) {
    wp_body_open();
} else {
    do_action( 'wp_body_open' );
}

?>

<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wp-bootstrap-starter' ); ?></a>
    <?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
    <header id="masthead" class="site-header navbar-static-top <?php echo wp_bootstrap_starter_bg_class(); ?>" role="banner">


        <div class="container">
            <nav class="navbar navbar-expand-xl p-0">
                <div class="navbar-brand">

                    <a href="/">
                        <img src="/images/alc.svg" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" class="img-fluid logo logohead" style="width: 330px;">
                    </a>


                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <?php
                wp_nav_menu(array(
                    'theme_location'    => 'primary',
                    'container'       => 'div',
                    'container_id'    => 'main-nav',
                    'container_class' => 'collapse navbar-collapse justify-content-end',
                    'menu_id'         => false,
                    'menu_class'      => 'navbar-nav',
                    'depth'           => 3,
                    'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                    'walker'          => new wp_bootstrap_navwalker()
                ));
                ?>

                <a href="/" class="button b1" style="margin-left: 20px;">Browse car leasing deals</a>
            </nav>

        </div>
    </header><!-- #masthead -->
    <?php if ( is_front_page() ) : ?>
        <div class="container-fluid b3">
            <div class="row">
                <div class="col-md-12 pt-5 pb-5">
                    <h1 style="color: rgb(255, 255, 255); text-align: center;">News & Leasing Guides</h1>
                    <div class="breadc" style="text-align: center; color: rgb(255, 255, 255);"><a href="/" style="color: rgb(255, 255, 255);">Home</a> / News & Leasing Guides</div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="container-fluid b3 mb-5">
            <div class="row">
                <div class="col-md-12 pt-5 pb-5">
                    <h1 style="color: rgb(255, 255, 255); text-align: center;"><?php the_title(); ?></h1>
                    <div class="breadc" style="text-align: center; color: rgb(255, 255, 255);">
                        <a href="/">Home</a> / <a href="/blog">News & Leasing Guides </a> / <?php the_title(); ?></div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div id="content" class="site-content">
        <div class="container">
            <div class="row">
<?php endif; ?>