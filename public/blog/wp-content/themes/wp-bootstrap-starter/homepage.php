<?php
/**
 * Template Name: Homepage
 *
 */
get_header(); ?>


<?php
// the query
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>

<div class="container">
    <div class="row row-eq-height">
        <h2>Featured Products</h2>
        <div class="owl-carousel owl-theme">

            <div class="col-12">
                <div class="item-container">
                    <div class="item-image image-service"></div>
                    <div class="item-text-block">
                        <div class="item-title">Service</div>
                        <p class="item-text">Car servicing at 40% cheaper than main dealers. Full, interim and manufacturer car services.</p>
                        <p class="item-link"><a href="https://autoleasecompare.motoreasy.com/car-servicing" target="_blank">Read More</a></p>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="item-container">
                    <div class="item-image image-warranty"></div>
                    <div class="item-text-block">
                        <div class="item-title">Warranty</div>
                        <p class="item-text">Comprehensive cover including market leading wear and tear provision</p>
                        <p class="item-link"><a href="https://autoleasecompare.motoreasy.com/car-warranty" target="_blank">Read More</a></p>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="item-container">
                    <div class="item-image image-ev"></div>
                    <div class="item-text-block">
                        <div class="item-title">EV / Hybrid Warranty</div>
                        <p class="item-text">The UK’s first extended electric warranty to provide optional drive battery protection.</p>
                        <p class="item-link"><a href="https://autoleasecompare.motoreasy.com/car-warranty/electric-vehicles" target="_blank">Read More</a></p>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="item-container">
                    <div class="item-image image-gap"></div>
                    <div class="item-text-block">
                        <div class="item-title">GAP Insurance</div>
                        <p class="item-text">5-star Defaqto cover up to 75% cheaper than dealers</p>
                        <p class="item-link"><a href="https://autoleasecompare.motoreasy.com/gap-insurance" target="_blank">Read More</a></p>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="item-container">
                    <div class="item-image image-mot"></div>
                    <div class="item-text-block">
                        <div class="item-title">MOT Test</div>
                        <p class="item-text">MOT tests from £30, up to 45% cheaper than the DVSA</p>
                        <p class="item-link"><a href="https://autoleasecompare.motoreasy.com/car/mot" target="_blank">Read More</a></p>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="item-container">
                    <div class="item-image image-tyre"></div>
                    <div class="item-text-block">
                        <div class="item-title">Tyre Insurance</div>
                        <p class="item-text">Punctures and sidewall damage for standard, performance and run-flats</p>
                        <p class="item-link"><a href="https://autoleasecompare.motoreasy.com/insurance/tyre" target="_blank">Read More</a></p>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="item-container">
                    <div class="item-image image-alloy"></div>
                    <div class="item-text-block">
                        <div class="item-title">Alloy Wheel Insurance</div>
                        <p class="item-text">Protection from scuffs, dents from under a fiver a month</p>
                        <p class="item-link"><a href="https://autoleasecompare.motoreasy.com/insurance/alloy" target="_blank">Read More</a></p>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="item-container">
                    <div class="item-image image-cosmetic"></div>
                    <div class="item-text-block">
                        <div class="item-title">Cosmetic Repair Insurance</div>
                        <p class="item-text">Cover for scratches, dents and SMART repairs from £7/month</p>
                        <p class="item-link"><a href="https://autoleasecompare.motoreasy.com/insurance/cosmetic-repair" target="_blank">Read More</a></p>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<?php if ( $wpb_all_query->have_posts() ) : ?>

        <div class="container" id="homepagePosts">
            <div class="row">
                <h2>News & leasing guides</h2>
            </div>
            <div class="row row-eq-height">

        <!-- the loop -->
        <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
            <div class="col-md-4 mb-3">
                <div class="blog-info-box">
                    <div class="blogimg">
                        <a href="<?php the_permalink(); ?>" class="thumbnail-wrapper">
                            <img src="<?php echo(catch_that_image()); ?>" alt="<?php the_title(); ?>">
                        </a>
                    </div>
                    <div class="blogtitle">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <!-- end of the loop -->
            </div>
    </div>

    <?php wp_reset_postdata(); ?>

<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php
get_footer();

