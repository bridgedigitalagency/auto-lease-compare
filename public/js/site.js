const $dropdown = $(".dropdown");
const $dropdownToggle = $(".dropdown-toggle");
const $dropdownMenu = $(".dropdown-menu");
const showClass = "show";

(function($){

    $dropdown.hover(
        function() {
            const $this = $(this);
            $this.addClass(showClass);
            $this.find($dropdownToggle).attr("aria-expanded", "true");
            $this.find($dropdownMenu).addClass(showClass);
        },
        function() {
            const $this = $(this);
            $this.removeClass(showClass);
            $this.find($dropdownToggle).attr("aria-expanded", "false");
            $this.find($dropdownMenu).removeClass(showClass);
        }
    );

    // Show loading spinner when clicking on clear all link
    $("body").on('click', '#dropdownMenu2, #dropdownMenu3 .rowcars', function (e) {
        displayLoadSpinner();
    });

    $('body').on('click', '#mobileMenuToggle', function(e) {
        $('body').toggleClass('menu-modal-open');
        $('#mobileMenu').toggle();
        $('#lhnHocButton').toggle();
    });
    $('body').on('click', '#mobileMenuGarageLink', function(e) {
        $('body').toggleClass('menu-modal-open');
        $('#mobileMenu').toggle();
        $('#lhnHocButton').toggle();
    });

    $('body').on('click', '.loginActions a', function(e) {
        $('body').toggleClass('menu-modal-open');
        $('#mobileMenu').toggle();
        $('#lhnHocButton').toggle();
    });

    $('body').on('click', '#vanTop', function(e) {
        $('#vanLeasingSub').slideToggle();

        if($("#carLeasingSub").is(":visible")){
            $('#carLeasingSub').slideToggle();
        }
        if($("#leasingSub").is(":visible")){
            $('#leasingSub').slideToggle();
        }
    });

    $('body').on('click', '#aboutLink', function(e) {
        $('body').removeClass('modal-open');
        $('#mobileMenu').hide();
        $('#lhnHocButton').show();
    });

    $('body').on('click', '#readMoreListingContent', function(e) {
        var text = $('#readMoreListingContent').text();
        $('.expandedContent').slideToggle();
        $('#readMoreListingContent').text(text == "Read Less" ? "Read More" : "Read Less");
    });

    $('body').on('click', '#carTop', function(e) {
        $('#carLeasingSub').slideToggle();

        if($("#vanLeasingSub").is(":visible")){
            $('#vanLeasingSub').slideToggle();
        }
        if($("#leasingSub").is(":visible")){
            $('#leasingSub').slideToggle();
        }

    });
    $('body').on('click', '#leasingTop', function(e) {
        $('#leasingSub').slideToggle();

        if($("#vanLeasingSub").is(":visible")){
            $('#vanLeasingSub').slideToggle();
        }
        if($("#carLeasingSub").is(":visible")){
            $('#carLeasingSub').slideToggle();
        }

    });
    $('body').on('click', '#mainMenu', function(e) {
        $('#mainNavigation').slideToggle();
        $('#adminNavigation').slideUp();
        $('#dealerNavigation').slideUp();
    });

    $('body').on('click', '#adminMenu', function(e) {
        $('#mainNavigation').slideToggle();
        $('#adminNavigation').slideToggle();
    });

    $('body').on('click', '#dealerMenu', function(e) {
        $('#mainNavigation').slideToggle();
        $('#dealerNavigation').slideToggle();
    });
    $('body').on('click', '#closeMobileMenu', function(e) {
        $('body').toggleClass('menu-modal-open');
        $('#mobileMenu').toggle();
        $('#lhnHocButton').toggle();
    });
})(jQuery);



function displayLoadSpinner() {
    if ( $( ".modalLoading" ).length ) {
        $('.modalLoading').show();
    }else{
        $( "body" ).append("<div class='modalLoading'><div class='lds-dual-ring'><img src='/images/loadingWheel.png'></div></div><div class='blackout' id='blackout1'></div>");
        $('.modalLoading').show();
    }

    setTimeout(function(){
        $('.modalLoading').hide();
    }, 2000);
}

