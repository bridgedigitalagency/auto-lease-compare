$(function () {

    let allowedUploadTypes = ['jpg', 'JPG', 'jpeg', 'JPEG', 'PNG', 'png'];

    //binds to onchange event of your input field
    $('#vehicleRepair').bind('change', function() {

        const fi = document.getElementById('damage');
        // Check if any file is selected.
        if (fi.files.length > 0) {
            for (i = 0; i <= fi.files.length - 1; i++) {
                let name = fi.files[i].name;
                let fileExt = name.split('.').pop();
                if(!allowedUploadTypes.includes(fileExt)) {
                    $('#damageFileError').show();

                    $('#damage').val('');
                }else{
                    $('#damageFileError').hide();
                    // const fsize = fi.files.item(i).size;
                    // const file = Math.round((fsize / 1024));
                    // // The size of the file.
                    // if (file >= 8192) {
                    //     $('#damage').val('');
                    //     $('#damageError').show();
                    // } else {
                    //     $('#damageError').hide();
                    // }
                }
            }
        }

        const fil = document.getElementById('reference');
        // Check if any file is selected.
        if (fil.files.length > 0) {
            for (i = 0; i <= fil.files.length - 1; i++) {
                let name = fil.files[i].name;
                let fileExt = name.split('.').pop();
                if(!allowedUploadTypes.includes(fileExt)) {
                    $('#referenceFileError').show();

                    $('#reference').val('');
                }else{
                    $('#referenceFileError').hide();
                    //
                    // const fsize = fil.files.item(i).size;
                    // const file = Math.round((fsize / 1024));
                    // // The size of the file.
                    // if (file >= 8192) {
                    //     $('#reference').val('');
                    //     $('#referenceError').show();
                    // } else {
                    //     $('#referenceError').hide();
                    // }
                }
            }
        }


    });
});

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}