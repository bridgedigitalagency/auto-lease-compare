var timeCheck;

$(document).on('click','#addToGarabeButton', function () {
    var carId = $('#model_id').val();
    var database = $('#database').val();

    $.ajax(
        {
            url: "/my-account/garage/add/" + carId + "/" + database,
            success: function (result) {
                // saved
                if (result == "S") {
                    $('#garageModalContent').html('<p>Your selected car has been added into your <a href="/my-account/garage">garage</a>. You can view your garage from your <a href="/my-account/">dashboard</a>.</p>');
                    // Need to login
                } else if (result == "S1") {
                    $('#garageModalContent').html('<p>Your selected car has been added into your <a href="/my-account/garage">garage</a>.<br> We had to remove one or more expired deals that you had previously saved. You can view your garage from your <a href="/my-account/">dashboard</a>.</p>');
                } else if (result == "L") {
                    $('#garageModalContent').html('<p>Sorry, you need to be logged in to your account to use the garage feature. Please <a href="#login" data-toggle="modal" data-dismiss="modal">login or register</a> now.</p><p style="text-align: center"><strong>Within to your Auto Lease Compare garage! </strong>Here you can:</p>' +
                        '                    <p style="text-align: center">' +
                        '                        <i class="fa fa-check"></i> Save and compare your favourite lease deals all in one place.<br>' +
                        '                        <i class="fa fa-check"></i> Compare technical specifications and historical price changes.<br>' +
                        '                        <i class="fa fa-check"></i> We will send you a notification when your saved deal drops in price!<br>' +
                        '                    </p>' +
                        '                    <p style="text-align: center">Hot lease deals are often on a limited stock so can sell out super quickly. <strong>Be quick to enquire, as they can be gone by tomorrow!</strong></p>');
                    // Garage is full
                } else if (result == "C") {
                    window.location.href = "/my-account/garage?c=1";
                }else if (result == "C1") {
                    window.location.href = "/my-account/garage?c=2";
                }
            }
        }
    );
});

(function($){

    if($(".mobhide").is(':visible')) {
        $('.footer-bottom').css('padding-bottom', 40 + $(".mobhide").height());
    }

    // Start bootstrap tooltip
    $('[data-toggle="tooltip"]').tooltip()

    // Disabling quote submit button to avoid duplicated enquiries
    $("body").on('submit', '#enquiryForm', function (e) {
        $("#enquiryForm button").attr("disabled", true);
        $("#enquiryForm button").text("Sending...");
        $("#enquiryForm button").toggleClass('b3');
        $("#enquiryForm button").toggleClass('b7');
        setTimeout(function() {
            $("#enquiryForm button").toggleClass('b3');
            $("#enquiryForm button").toggleClass('b7');
            $("#enquiryForm button").attr('disabled', false);
            $("#enquiryForm button").text("Send Enquiry Now >");
        }, 5000);
    });


    // Logic to deal with changing deposit value
    $("body").on('click', '#deposit_val button', function (e) {

        $('#deposit_val button').removeAttr('data-selected');

        $('#deposit_val .b2').addClass('b7');
        $('#deposit_val .b2').removeClass('b2');

        $(this).attr('data-selected', true);
        $(this).addClass('b2');
        $(this).removeClass('b7');

        updateDeal();
    });

    // logic to deal with changing term
    $("body").on('click', '#term_val button', function (e) {

        $('#term_val button').removeAttr('data-selected');

        $('#term_val .b2').addClass('b7');
        $('#term_val .b2').removeClass('b2');

        $(this).attr('data-selected', true);
        $(this).addClass('b2');
        $(this).removeClass('b7');

        updateDeal();
    });

    // update deal with updated mileage option
    $("body").on('change', '#annual_mileage', function (e) {
        updateDeal();
    });

    // Stop video when modal is closed
    $('#modelreview').on('hidden.bs.modal', function () {
        $("#modelreview iframe").attr("src", $("#modelreview iframe").attr("src"));
    });

    var pricesId = $('#model_id').val();
    var database = $('#database').val();

    $("body").on('click', '.callButtonDesktop', function (e) {
        $.ajax(
            {
                url: "/tracking/quote/" + pricesId + "/" + database + "/call",
                success: function (result) {console.log('Call Button Triggered')}
            }
        );
    });

    $("body").on('click', '#enquireTodayButton', function (e) {
        $.ajax(
            {
                url: "/tracking/quote/" + pricesId + "/" + database + "/enquiry",
                success: function (result) {console.log('Enquiry Button Triggered')}
            }
        );
    });


})(jQuery);

/**
 * Update
 */
function updateDeal() {

    clearTimeout(timeCheck);

    if($('#priceUpdatedNotification').hasClass('show')) {
        $('#priceUpdatedNotification').addClass('d-none');
        $('#priceUpdatedNotification').removeClass('show');
    }

    let deals = window.deals;
    let deposit_value = $('#deposit_val [data-selected="true"]').val();
    let term = $('#term_val [data-selected="true"]').val();
    let mileage = $('#annual_mileage').find(":selected").val();

    let deal = window.deals.filter( function(e) {
        return e.term == term && e.deposit_value == deposit_value && e.mileage == mileage;
    });

    updateEnquiryForm(deal);
    updateDepositValues(deal);
    updateTerm(deal);
    updateMonthlyPayments(deal);
    updateTotalUpfront(deal);
    updateAverageMonthlyCost(deal);
    updateAnnualMileage(deal);
    updatePriceHistoryChart(deal);
    updateTotalCost(deal);

    $('#priceUpdatedNotification').removeClass('d-none');
    $('#priceUpdatedNotification').addClass('show');
    timeCheck = setTimeout(function(){
        if($('#priceUpdatedNotification').hasClass('show')) {
            // Hide after 3 seconds if it is visible
            $('#priceUpdatedNotification').addClass('d-none');
            $('#priceUpdatedNotification').removeClass('show');
        }
    }, 3000);

}

function updatePriceHistoryChart(deal)
{
    let selectedDeal = deal[0];

    $('#priceHistoryContainer').append('<div class="modalLoading"><div class="lds-dual-ring"><img src=\'/images/loadingWheel.png\'></div></div>');
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        type: 'get',
        url: 'https://pricehistory.autoleasecompare.com/pr/' + selectedDeal.capid + '/' + selectedDeal.mileage + '/' + selectedDeal.term + '/' + selectedDeal.deposit_value + '/' + selectedDeal.finance_type + '/'  + selectedDeal.database
    }).done(function(html) {
        $('#priceHistory').html(html);
        $('.modalLoading').hide();
    }).fail(function() {

    });
}

/**
 * Method to update the hidden fields on the enquiry form to populate DB / emails correctly
 */
function updateEnquiryForm(deal) {

    let selectedDeal = deal[0];
    let vat = " Inc VAT";

    if(selectedDeal.finance_type === 'B') {
        let vat = " Ex VAT";
    }
    // profile text is included on the enquiry form
    let profile = '£' + selectedDeal.deposit_months + vat + " (" + selectedDeal.deposit_value + " Months Upfront) | " + selectedDeal.term + " Month Contract | " + selectedDeal.mileage + " Miles P/A";

    $('#enquiryForm #model_id').val(selectedDeal.dealid);
    $('#enquiryForm #prices_id').val(selectedDeal.dealid);
    $('#enquiryForm #profile').val(profile);
    $('#enquiryForm #price').val(selectedDeal.monthly_payment);
    $('#enquiryForm #document_fee').val(selectedDeal.document_fee);
    $('#enquiryForm #finance_type').val(selectedDeal.finance_type);

}

// Update the deposit values (initial payment) everywhere on the page
function updateDepositValues(deal) {
    let selectedDeal = deal[0];
    let deposit_value = selectedDeal.deposit_value;
    let deposit_months = selectedDeal.deposit_months;

    $('.initialPaymentTooltip').attr('data-original-title', deposit_value + ' months upfront');
    $('.initialPaymentTooltip').attr('title', deposit_value + ' months upfront');
    $('.initial_payment_price').text('£' + deposit_months);
    $('.initial_payment_months').text(deposit_value);

}
// Update the term value everywhere on the page
function updateTerm(deal) {
    let selectedDeal = deal[0];
    let term = selectedDeal.term;

    $('.term_text').text(term);
    $('.term_text_minus_initial').text(term -1);
}

function updateMonthlyPayments(deal) {
    let selectedDeal = deal[0];
    let monthly_payment = selectedDeal.monthly_payment;

    $('.monthly_payment_text').text("£" + monthly_payment);
}

function updateTotalUpfront(deal) {
    let selectedDeal = deal[0];
    let total_initial_cost = selectedDeal.total_initial_cost;

    $('.total_initial_cost_text').text("£" + total_initial_cost);
}

function updateTotalCost(deal)
{
    let selectedDeal = deal[0];

    let totalcost = selectedDeal.monthly_payment * (selectedDeal.term - 1) + selectedDeal.deposit_months + selectedDeal.document_fee;

    $('.total_cost_text').text("£" + totalcost.toFixed(2));

}

function updateAverageMonthlyCost(deal) {
    let selectedDeal = deal[0];
    let average_cost = selectedDeal.average_cost;

    $('.average_cost_text').text("£" + average_cost);
}
function updateAnnualMileage(deal) {
    let selectedDeal = deal[0];
    let annual_mileage = selectedDeal.mileage;

    $('.annual_mileage_text').text(annual_mileage);
}
