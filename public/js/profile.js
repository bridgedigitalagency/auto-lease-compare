$(function () {

    $('#logoUpload').on('submit', function (e) {
        e.preventDefault();
        $('.modalLoading').show();
        let uploadUrl = "/image-upload";
        let token = $('meta[name="csrf-token"]').attr('content');
        let formData = new FormData(this);
        $.ajax(
            {
                type: "POST",
                url: uploadUrl + '?_token=' + token,
                data: formData,
                contentType: false,
                processData: false,
                success: function (result) {
                    $('.profileBlockBackground img').attr('src',"/images/uploads/logos/" + result);
                    $('#imageWin').show();
                }
            }
        );
        $('.modalLoading').hide();
    });
});