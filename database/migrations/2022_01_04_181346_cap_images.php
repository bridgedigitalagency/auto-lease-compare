<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CapImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cap_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cap_id');
            $table->string('image_name');
            $table->integer('is_default')->default(0);
            $table->string('database')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cap_images');
    }
}
