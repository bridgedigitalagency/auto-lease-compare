<?php

namespace App\Exports;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;



class UsersExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function headings(): array
    {
        if (Auth::user()->hasRole(['admin'])) {

            return [
                'ID',
                'Name',
                'First Name',
                'Last Name',
                'Email',
                'Phone',
                'Mailing List',
                'Created At',
            ];
        } else{

        }
    }


    public function query()
    {
        if (Auth::user()->hasRole(['admin'])) {

            return User::query()->select(array(
                'id',
                'name',
                'first_name',
                'last_name',
                'email',
                'phone',
                'mailing_list',
                'created_at',
            ))
                ->whereNotNull('id' );

        } else {

        }
    }
}
