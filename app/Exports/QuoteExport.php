<?php

namespace App\Exports;

use App\Models\Quote;
use App\Models\User;
use Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

ini_set('memory_limit', '4G');



class QuoteExport implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;

    public function headings(): array
    {
        if (Auth::user()->hasRole(['admin'])) {

            return [
                'ID',
                'Model ID',
                'Make',
                'Model',
                'Dealer',
                'Transmission',
                'Fuel Type',
                'Price',
                'Profile',
                'Finance Type',
                'Doc Fee',
                'Name',
                'Email',
                'Phone Number',
                'Enquiry',
                'Contact Preference',
                'Status',
                'Customer Flagged',
                'Withdraw Enquiry',
                'Created By',
                'Created At',
                'Hot Deals',
                'Live chat assisted',
                'database'

            ];
        } elseif (Auth::user()->hasRole(['dealer']))  {

            return [
                'ID',
                'Model ID',
                'Make',
                'Model',
                'Dealer',
                'Price',
                'Status',
                'Profile',
                'Finance Type',
                'Doc Fee',
                'Created At',
            ];

        }
    }


    public function query()
    {
        if (Auth::user()->hasRole(['admin'])) {

            $requestData = Request::all();

            $quote = DB::table('quote')
                ->when(isset($requestData['deal_id']), function ($query) use ($requestData) {
                    return $query->where('deal_id', '=', $requestData['deal_id']);
                })
                ->when(isset($requestData['date_from']), function ($query) use ($requestData) {
                    return $query->where('created_at', '>', $requestData['date_from']);
                })
                ->orderBy('id');

            return $quote;

        } elseif (Auth::user()->hasRole(['dealer']))  {

            $getQuoteAuth = Auth::user()->name;
            $getQuoteAuth = trim(str_replace('OFFLINE', '', $getQuoteAuth));

            return Quote::query()->select(array(
                'id',
                'model_id',
                'make',
                'model',
                'deal_id',
                'price',
                'status',
                'profile',
                'finance_type',
                'document_fee',
                'created_at',
                'database'
            ))
                ->where('deal_id' , '=', $getQuoteAuth);

        }
    }

    /**
     * @param mixed $invoice
     * @return array
     */
    public function map($quote): array
    {

        if (Auth::user()->hasRole(['admin'])) {

            switch ($quote->status) {
                case '1':
                    $status = 'Quote Sent';
                    break;
                case '2':
                    $status = 'Marked Sold';
                    break;
                case '3':
                    $status = 'Pending Payment';
                    break;
                case '4':
                    $status = 'Paid';
                    break;
                case '5':
                    $status = 'Closed';
                    break;
                case '6':
                    $status = 'Open';
                    break;
                case '7':
                    $status = 'Withdrawn';
                    break;
            }


            switch ($quote->contact_preference) {

                case '1':
                    $contactPreference = 'Call';
                    break;
                case '2':
                    $contactPreference = 'Call morning';
                    break;
                case '3':
                    $contactPreference = 'Call afternoon';
                    break;
                case '4':
                    $contactPreference = 'Email';
                    break;
                case '5':
                    $contactPreference = 'No Preference';
                    break;
            }

            switch ($quote->finance_type) {
                case 'P':
                    $finance_type = 'Personal';
                    break;
                case 'B':
                    $finance_type = 'Business';
                    break;
                default:
                    $finance_type = 'Personal';
                    break;
            }
            $username = '';
            $user = User::find($quote->created_by);
            if($user) {
                $username = $user->name;
            }
            return [
                $quote->id,
                $quote->model_id,
                $quote->make,
                $quote->model,
                $quote->deal_id,
                $quote->transmission,
                $quote->fuel_type,
                $quote->price,
                $quote->profile,
                $finance_type,
                $quote->document_fee,
                $quote->name,
                $quote->email,
                $quote->phone_number,
                $quote->enquiry,
                $contactPreference,
                $status,
                $quote->markassold,
                $quote->withdraw_enquiry,
                $username,
                $quote->created_at,
                $quote->hotdeals,
                $quote->chat,
                $quote->database

            ];

        } elseif (Auth::user()->hasRole(['dealer']))  {

            switch ($quote->status) {
                case '1':
                    $status = 'Quote Sent';
                    break;
                case '2':
                    $status = 'Marked Sold';
                    break;
                case '3':
                    $status = 'Pending Payment';
                    break;
                case '4':
                    $status = 'Paid';
                    break;
                case '5':
                    $status = 'Closed';
                    break;
                case '6':
                    $status = 'Open';
                    break;
                case '7':
                    $status = 'Withdrawn';
                    break;

            }

            if(is_null($quote->finance_type) || !isset($quote->finance_type)) {
                $finance_type = "Personal";
            }

            switch ($quote->finance_type) {
                case 'P':
                    $finance_type = 'Personal';
                break;
                case 'B':
                    $finance_type = 'Business';
                break;
            }


            return [
                $quote->id,
                $quote->model_id,
                $quote->make,
                $quote->model,
                $quote->deal_id,
                $quote->price,
                $status,
                $quote->profile,
                $finance_type,
                $quote->document_fee,
                $quote->created_at,
            ];

        }

    }
}
