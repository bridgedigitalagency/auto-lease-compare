<?php

namespace App\Exports;

use App\Models\Quote;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

ini_set('memory_limit', '4G');



class DwvExport implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;

    public function headings(): array
    {
        if (Auth::user()->hasRole(['admin'])) {

            return [
                'id',
                'name',
                'email',
                'address',
                'phone',
                'make',
                'model',
                'registration',
                'additional_details',
                'created_at',
                'alloy',
                'paint',
                'dent',


            ];
        }
    }


    public function query()
    {
        return DB::table('dwv_submissions')->whereNotNull('id')->orderBy('id');
    }

    /**
     * @param mixed $invoice
     * @return array
     */
    public function map($quote): array
    {

        if (Auth::user()->hasRole(['admin'])) {

            return [
                $quote->id,
                $quote->name,
                $quote->email,
                $quote->address,
                $quote->phone,
                $quote->make,
                $quote->model,
                $quote->registration,
                $quote->additional_details,
                $quote->created_at,
                $quote->alloy,
                $quote->dent,
                $quote->paint
            ];


        }

    }
}
