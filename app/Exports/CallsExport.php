<?php

namespace App\Exports;

use App\Models\Calls;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;



class CallsExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function headings(): array
    {
        if (Auth::user()->hasRole(['admin'])) {

            return [
                'id',
                'c_ref',
                'i_number',
                'o_number',
                'c_date',
                'c_time',
                'c_duration',
                'status',
            ];
        } else{

        }
    }


    public function query()
    {
        if (Auth::user()->hasRole(['admin'])) {

            return Calls::query()->select(array(
                'id',
                'c_ref',
                'i_number',
                'o_number',
                'c_date',
                'c_time',
                'c_duration',
                'status',
            ))
                ->whereNotNull('id' );

        } else {

        }
    }
}
