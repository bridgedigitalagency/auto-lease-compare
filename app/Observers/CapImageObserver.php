<?php

namespace App\Observers;

use App\Models\CapImage;
use App\Jobs\DeleteCDNCapImage;

class CapImageObserver
{
    /**
     * Handle the cap image "created" event.
     *
     * @param  \App\Models\CapImage  $capImage
     * @return void
     */
    public function created(CapImage $capImage)
    {
        //
    }

    /**
     * Handle the cap image "updated" event.
     *
     * @param  \App\Models\CapImage  $capImage
     * @return void
     */
    public function updated(CapImage $capImage)
    {
        //
    }

    /**
     * Handle the cap image "deleted" event.
     *
     * @param  \App\Models\CapImage  $capImage
     * @return void
     */
    public function deleted(CapImage $capImage)
    {
        DeleteCDNCapImage::dispatch($capImage->image_name, $capImage->database)->onConnection('database')->onQueue('alc');
    }

    /**
     * Handle the cap image "restored" event.
     *
     * @param  \App\Models\CapImage  $capImage
     * @return void
     */
    public function restored(CapImage $capImage)
    {
        //
    }

    /**
     * Handle the cap image "force deleted" event.
     *
     * @param  \App\Models\CapImage  $capImage
     * @return void
     */
    public function forceDeleted(CapImage $capImage)
    {
        //
    }
}
