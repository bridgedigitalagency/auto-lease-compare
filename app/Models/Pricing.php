<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    protected $table = 'pricestable';

    protected $guarded = [];
    public $timestamps = false;
}