<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VanPricing extends Model
{
    protected $table = 'van_pricestable';

    protected $guarded = [];
    public $timestamps = false;
}