<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VanPriceHistory extends Model
{
    protected $table = 'priceshistory_vans';

    protected $guarded = [];
    public $timestamps = false;
}
