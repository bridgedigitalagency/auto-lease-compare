<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CapReview extends Model
{
    protected $table = 'cap_reviews';

    protected $guarded = [];
    public $timestamps = false;
}
