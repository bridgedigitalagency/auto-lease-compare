<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cap extends Model
{
    protected $table = 'capreference';

    protected $guarded = [];
    public $timestamps = false;

    public function images(){
        return $this->hasMany(CapImage::class,'cap_id','cap_id');
    }
}
