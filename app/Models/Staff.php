<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff_members';

    protected $guarded = [];
    public $timestamps = false;
}
