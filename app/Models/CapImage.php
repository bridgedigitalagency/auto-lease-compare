<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CapImage extends Model
{
    protected $fillable = ['cap_id' , 'image_name', 'is_default','cap','database'];

}
