<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calls extends Model
{
    protected $table = 'ttnc';

    protected $guarded = [];
    public $timestamps = false;
}
