<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceHistory extends Model
{
    protected $table = 'priceshistory_cars';

    protected $guarded = [];
    public $timestamps = false;
}
