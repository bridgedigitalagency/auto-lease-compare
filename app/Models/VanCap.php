<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VanCap extends Model
{
    protected $table = 'van_capreference';

    protected $guarded = [];
    public $timestamps = false;
}
