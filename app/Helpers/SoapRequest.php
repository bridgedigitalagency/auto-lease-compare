<?php

namespace App\Helpers;

use SoapClient;

class SoapRequest
{
    private $wsdl = '';
    private $function = '';
    private $params = [];

    public function send()
    {
        $client = new SoapClient($this->buildWSDL(), $this->buildOptions());
        try {
            $response = $client->{$this->function}($this->params);
            $resKey = $this->function.'Result';

        } catch (\SoapFault $e) {
            echo "Error: {$e}";
            return false;
        }

        return $response->{$resKey};
    }

    /**
     * Set Base URL
     *
     * @param string $url
     * @return this
     */
    public function setWSDL($url)
    {
        $this->wsdl = $url;

        return $this;
    }

    /**
     * Generate URL based on HTTP method
     *
     * @return string
     */
    public function buildWSDL()
    {
        return $this->wsdl.'?wsdl';
    }

    /**
     * @return array
     */
    public function buildOptions()
    {
        return array(
            'soap_version' => SOAP_1_1,
            'cache_wsdl' => WSDL_CACHE_BOTH,
            'trace' => TRUE,
            'connection_timeout' => 300,
            'exceptions' => TRUE,
            'keep_alive' => TRUE
        );
    }

    /**
     * Set function
     *
     * @param string $url
     * @return this
     */
    public function setFunction($functionName)
    {
        $this->function = $functionName;

        return $this;
    }

    /**
     * Set params
     *
     * @param array $params
     * @return this
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Add param
     *
     * @param string $key
     * @param mixed $value
     * @return this
     */
    public function addParam($key, $value)
    {
        $this->params[$key] = $value;

        return $this;
    }
}