<?php

namespace App\Helpers;

use GuzzleHttp\Client;


class HttpRequest
{
    private $params = [];

    private $method = 'GET';

    private $baseURL = '';

    private $request;

    public function __construct()
    {
        $this->request = new Client();
    }

    public function send()
    {
        $response = $this->request->request($this->method,$this->buildURL(), $this->getParams());
        return $response->getBody();
    }

    /**
     * Set Base URL
     *
     * @param string $url
     * @return this
     */
    public function setURL($url)
    {
        $this->baseURL = $url;

        return $this;
    }

    /**
     * Generate URL based on HTTP method
     *
     * @return string
     */
    public function buildURL()
    {
        if ($this->method === 'GET') {
            $params = http_build_query($this->params);
            return "{$this->baseURL}?{$params}";
        }

        return $this->baseURL;
    }

    /**
     * Return params based on HTTP method
     *
     * @return array
     */
    private function getParams()
    {
        return $this->method === 'GET' ? [] : $this->params;
    }

    /**
     * Set params
     *
     * @param array $params
     * @return this
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Add param
     *
     * @param string $key
     * @param mixed $value
     * @return this
     */
    public function addParam($key, $value)
    {
        $this->params[$key] = $value;

        return $this;
    }

    /**
     * Set HTTP Method
     *
     * @param string $method
     * @return this
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }
}
