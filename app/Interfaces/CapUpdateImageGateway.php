<?php

namespace App\Interfaces;

interface CapUpdateImageGateway
{
    public function getCapImage($url, $function, $params);
}