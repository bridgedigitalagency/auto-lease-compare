<?php

namespace App\Interfaces;

interface CDNInterface
{
    public function store($fileNames, $database);
    public function delete($fileName,$database);
    public function purge($fileName,$database);
}