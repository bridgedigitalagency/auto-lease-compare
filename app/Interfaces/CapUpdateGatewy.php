<?php

namespace App\Interfaces;

interface CapUpdateGatewy
{
    public function getCapData($url, $function, $params);
}