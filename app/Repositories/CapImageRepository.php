<?php

namespace App\Repositories;

use App\Models\CapImage;

class CapImageRepository
{
    public function store($cap_id, $image_name, $is_default,$database)
    {
        return CapImage::create(['cap_id' => $cap_id , 'image_name'=> $image_name, 'is_default'=> $is_default, 'database'=>$database]);

    }
}