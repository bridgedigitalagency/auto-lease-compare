<?php

namespace App\Providers;

use App\Interfaces\CDNInterface;
use App\Models\CapImage;
use App\Observers\CapImageObserver;
use App\Services\CDN\DOService;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Paginator::useBootstrapThree();
        Schema::defaultStringLength(191);
        $this->app->bind(CDNInterface::class, DOService::class);
        CapImage::observe(CapImageObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if(in_array(Request::ip(), [ env('DEV_IP_ADDRESS') ] )) {
            config(['app.debug' => true]);
        }
    }
}
