<?php

namespace App\Providers;

use App\Interfaces\CapUpdateGatewy;
use App\Interfaces\CapUpdateImageGateway;
use App\Services\CapUpdateImageServices\CAD;
use App\Services\CapUpdateImageServices\HPI;
use App\Services\CapUpdateServices\CapHpi;
use CodeDredd\Soap\Facades\Soap;
use Illuminate\Support\ServiceProvider;

class CapUpdateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CapUpdateGatewy::class, function ($app) {
            $gateway = env('CAP_UPDATE_SERVICE_GATEWAY');
            switch ($gateway) {
                case 'CAPHPI':
                    return new CapHpi();
                default:
                    throw new Exception('Unsupported Cap update Gateway');
            }
        });

        $this->app->bind(CapUpdateImageGateway::class, function ($app) {
            $gateway = env('CAP_UPDATE_IMAGE_SERVICE_GATEWAY');
            switch ($gateway) {
                case 'CAPHPI':
                    return new HPI();
                case 'CAD':
                    return new CAD();
                default:
                    throw new Exception('Unsupported Cap update image Gateway');
            }
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
