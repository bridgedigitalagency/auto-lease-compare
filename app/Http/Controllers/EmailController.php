<?php

namespace App\Http\Controllers;

use App\Models\Cap;
use App\Models\Pricing;
use App\Models\Quote;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use jeremykenedy\LaravelRoles\Models\Role;

class EmailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewEmails() {

        return view('my_account_views.view-emails');
    }

    public function viewEmail($template) {

        $model = Quote::select()
            ->where('id', '=', 1189)
            ->first();
        $quote = $model;
        $pricing = Pricing::where('id', $model['model_id']) -> first();
        $dealer = User::where('name',$model->deal_id) -> first();
        $cap = Cap::where('cap_id', $pricing->cap_id) -> first();
        $contact_preference = $this->getContactPreferences($model->contact_preference);
        $imageId = $cap->image_id;
        if(isset($database) && $database == 'LCV') {
            $file = 'images/capimages/LCV/' . $cap->imageid . '.jpg';
        }else {
            $file = 'images/capimages/' . $cap->imageid . '_3.JPG';
        }

        if(file_exists($file)) {
            $image = $file;
        }else{
            $image = 'images/comingsooncar.jpg';
        }

        if($template == 'garage_price_update') {
            $text = $model->make . ' ' . $model->model;
        }else{
            $text = array(
                'name' => 'Scott Mokler',
                'job' => 'Director',
                'company' => 'Bridge Digital Agency',
                'companyweb' => 'www.bridgedigitalagency.net',
                'email'=>'scott@bridgedigitalagency.net',
                'companyno'=>12544020,
                'fcano'=>1111111,
                'bvrla'=>111111,
                'subject'=> ' Test email'
            );
        }

        $call = DB::table('ttnc')
            ->where('id', '=', 1)
            ->first();

        $callInfo['anumber'] = $call->i_number;
        $callInfo['call_date'] = $call->c_date;
        $callInfo['call_time'] = $call->c_time;
        $callInfo['duration'] = $call->c_duration;

        $vars = compact(
            'model',
            'pricing',
            'dealer',
            'cap',
            'contact_preference',
            'image',
            'text',
            'callInfo',
            'quote'
        );
        return view('emails.' . $template, $vars);
    }

    private function getContactPreferences($contactPref)
    {

        $contactPreferences = array(
            1 => 'Call me back',
            2 => 'Call me back (Morning)',
            3 => 'Call me back (Afternoon)',
            4 => 'Email me back',
            5 => 'No Preference',
        );

        return $contactPreferences[$contactPref];

    }

}