<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Cap;
use App\Models\Pricing;
use App\Models\PriceHistory;
use Illuminate\Support\Facades\Redirect;
use Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use jeremykenedy\LaravelRoles\Models\Role;

class GarageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function view_garage() {
        $user = Auth::user();
        $userId = $user->id;
        $cap = '';

        $garageContent = DB::table('garage')
            ->where('user_id', '=', $userId)
            ->get();

        foreach($garageContent as $garage) {

            $database = $garage->database;

            if($database == 'LCV') {
                $pricestable = 'van_pricestable';
            }else{
                $pricestable = 'pricestable';
            }

            $cdnUrl = env('DO_IMAGE_PATH') . $database . '/';

            // Add cap data
            $cap = DB::table('capreference')
                ->where('cap_id', '=', $garage->cap_id)
                ->where('database','=',$database)
                ->first();

            if($cap->imageid >= 1) {
                $vehicleImage = $this->fetchVehicleDefaultImage($cap->cap_id, $database);
                $imageUrl = $cdnUrl . 'big/' . $vehicleImage->image_name;
                if (@getimagesize($imageUrl)) {
                    $cap->imageid = $imageUrl;
                }else{
                    $cap->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
                }
            }else{
                $cap->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
            }

            // Get technical data
            $techData = DB::table('cap_tech_data')
                ->where('database', '=', $database)
                ->where('cap_id', '=', $garage->cap_id)
                ->get();

            // status 2 = expired
            if($garage->status != 2) {

                // Check if deal exists
                $prices =  DB::table($pricestable)
                    ->join('users', $pricestable .'.deal_id', '=', 'users.name')
                    ->select('*', $pricestable . '.id as id')
                    ->where($pricestable . '.id', '=', $garage->prices_id)
                    ->first();

                // Status needs updating to expired
                if(is_null($prices)) {
                    DB::table('garage')->where('id', $garage->id)->update(['status' => 2]);
                    $garage->status = 2;
                }

            }

            $garage->cap = $cap;
            $garage->techData = $techData;

        }


        return View('garage_views.view-garage', compact('garageContent'));

    }


    public function delete_garage($carId,$database = "CARS")
    {
        if($this->delete_car($carId, $database)) {
            return Redirect::to('/my-account/garage?m=2');
        }
    }

    /**
     * @param $carId
     * @param string $database
     * @return string
     */
    public function add_garage($carId, $database = 'CARS')
    {

        $garage = array();
        $user = Auth::user();

        if (is_null($user)) {
            return "L";
        } else {


            $userId = $user->id;

            // Fetch car details

            if($database == 'LCV') {
                $prices = DB::table('van_pricestable')
                    ->where('id', $carId)
                    ->first();
            }else {
                $prices = DB::table('pricestable')
                    ->where('id', $carId)
                    ->first();
            }

            $cap = DB::table('capreference')
                ->where('cap_id', '=', $prices->cap_id)
                ->where('database','=', $database)
                ->first();

            $garageCheck = DB::table('garage')
                ->where('user_id', '=', $userId)
                ->where('prices_id', '=', $carId)
                ->where('database','=', $database)
                ->first();

            $garageCount = DB::table('garage')
                ->where('user_id', '=', $userId)
                ->get();

            $garageUserCount = count($garageCount);

            if($garageCheck) {
                return "C1";
            }

            if($garageUserCount >= 3) {
                return "C";
            }

            if (!is_null($prices)) {
                DB::table('garage')->insert(
                    array(
                        'user_id'=>$userId,
                        'deal_id'=>$prices->deal_id,
                        'prices_id'=>$prices->id,
                        'cap_id'=>$prices->cap_id,
                        'monthly_payment'=> $prices->monthly_payment,
                        'total_initial_cost'=>$prices->total_initial_cost,
                        'annual_mileage'=>$prices->annual_mileage,
                        'finance_type'=>$prices->finance_type,
                        'in_stock'=>$prices->in_stock,
                        'term'=>$prices->term,
                        'deposit_value'=>$prices->deposit_value,
                        'status'=>1,
                        'database'=>$cap->database
                    )
                );

                return "S";
            }
        }
    }

    /**
     * @param $carId
     */
    private function delete_car ($carId, $database) {
        $user = Auth::user();
        $userId = $user->id;

        $garage = DB::table('garage')
            ->where('user_id', '=', $userId)
            ->where('database','=',$database)
            ->where('prices_id', '=', $carId)
            ->delete();

        if($garage) {
            return true;
        }
    }

    public function switchPriceDropNotifications () {
        $data = Request::all();
        $notification = $data['checkbox'];

        $user = Auth::user();
        $userId = $user->id;

        $user = User::find($userId);
        $user->garage_price_notification = $notification;
        $user->save();
    }

    /**
     * @param null $cap_id
     * @param $database
     * @return false|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    private function fetchVehicleDefaultImage($cap_id = null, $database) {

        if(isset($cap_id) && !is_null($cap_id)) {
            $image = DB::table('cap_images')
                ->where('cap_id', '=', $cap_id)
                ->where('is_default', '=', 1)
                ->where('database', '=', $database)
                ->first();

            return $image;
        }else{
            return false;
        }

    }
}
