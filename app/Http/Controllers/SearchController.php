<?php
namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Cap;
use App\Models\Pricing;
use Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use jeremykenedy\LaravelRoles\Models\Role;

class SearchController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewSearch()
    {

        $requestData = $data = Request::all();

        if (!isset($data['finance_type'])) {
            $finance_type = 'P';
        } else {
            $finance_type = $data['finance_type'];
        }

        if(isset($data['listing_type'])) {
            $listing_type = $data['listing_type'];
        }else{
            if(isset($data['finance_type'])) {
                $listing_type = $data['finance_type'];
                $data['listing_type'] = $listing_type;
            }else{
                $data['finance_type'] = $data['listing_type'] = $listing_type = 'P';
            }
        }

        if(isset($data['database'])) {
            $database = $data['database'];
        }else{
            $database = 'CARS';
            $data['database'] = 'CARS';
        }

        if($database == 'LCV') {
            $pricestable = 'van_pricestable';
        }else{
            $pricestable = 'pricestable';
        }

        if ($finance_type == 'A') {
            $adverse = 1;
        } else {
            $adverse = 0;
        }

        $fuel = array();
        if ($finance_type == 'H') {
            $fuel = array(
                'Petrol Electric Hybrid',
                'Petrol PlugIn Elec Hybrid',
                'Diesel Electric Hybrid',
                'PlugIn Elec Hybrid',
            );
        }
        $trim = array();
        // SET TRIMS FOR PERFORMANCE
        if ($finance_type == 'PERFORMANCE') {
            $trim = array('%gti%', '%AMG%');
        }

        if(isset($requestData['search'])) {
            $searchedString = $requestData['search'];
            $searchString = explode(' ', $requestData['search']);
        }else{
            $searchedString = null;
            $searchString = null;
        }


        $summaryCap1 = array();

        $summaryCap1 = DB::table($pricestable)

            ->join('capreference', $pricestable.'.cap_id', '=', 'capreference.cap_id')
//            ->select('pricestable.*', 'capreference.*')
//            ->select(DB::raw("pricestable.*, capreference.* , CONCAT('capreference.maker','capreference.model','capreference.derivative') AS searchterm"))
            ->when(isset($finance_type), function ($query) use ($finance_type) {
                if ($finance_type == 'H') {
                    $finance_type = "P";
                }
                if ($finance_type == 'PERFORMANCE') {
                    $finance_type = "P";
                }
                if ($finance_type == 'A') {
                    $finance_type = "P";
                }
                return $query->where('finance_type', '=', $finance_type);
            })
            ->whereNotNull($pricestable . '.monthly_payment')
            ->when($adverse == 1, function ($summaryCap1) use ($adverse) {
                return $summaryCap1->where('adverse_credit', '=', 1);
            })
            ->when(count($fuel) > 0, function ($summaryCap1) use ($fuel) {
                return $summaryCap1->wherein('fuel', $fuel);
            })
            ->where(function ($summaryCap1) use ($trim) {

                for ($i = 0; $i < count($trim); $i++) {
                    $summaryCap1
                        ->orWhere('capreference.trim', 'LIKE', $trim[$i]);
                }
                return $summaryCap1;
            })

            ->where(function ($summaryCap1) use ($searchString) {
                if($searchString!='') {
                    $searchStringImploded = implode(' ', $searchString);
                }else{
                    $searchStringImploded = '';
                    $searchString = [];
                }

                // manipulate input data
                foreach ($searchString as $k=>$v) {

                    if($v == 'class') {
                        $mercs = 1;
                    }
                }

                if(isset($mercs) && $mercs == 1) {
                    $summaryCap1->whereRaw("`range` = '" . $searchStringImploded . "'");
                }else {
//                    foreach ($searchString as $string) {
//                        $summaryCap1->whereRaw("MATCH (maker,model,derivative) AGAINST ('\"$string\"' IN BOOLEAN MODE)");
//                    }
                    $summaryCap1->whereRaw("MATCH (maker,model,derivative) AGAINST ('\"$searchStringImploded\"' IN BOOLEAN MODE)");
                }

                return $summaryCap1;
            })

            ->limit(15)
            ->orderBy('monthly_payment')
            ->paginate(15);

        $summaryCap = ($summaryCap1);

        if($summaryCap->total() > 0) {

            $leasing = new LeasingController();

            $makes = $leasing->generateFilterSql($data, 'maker');
//        $makes = $leasing->getMakes($listing_type);
            $rangeData = $leasing->generateFilterSql($data, 'range');
            foreach ($rangeData as $range) {
                $ranges[$range->maker][] = $range->range;
            }
            $trimData = $leasing->generateFilterSql($data, 'trim');
            foreach ($trimData as $trim) {
                $trims[$trim->range][] = $trim->trim;
            }
            $fuels = $leasing->generateFilterSql($data, 'fuel');
            $transmissions = $leasing->generateFilterSql($data, 'transmission');
            $bodystyles = $leasing->generateFilterSql($data, 'bodystyle');
            $doors = $leasing->generateFilterSql($data, 'doors');
            $enginesizes = $leasing->generateFilterSql($data, 'enginesize');
        }else{
            $makes = [];
            $ranges = [];
            $trimData = [];
            $fuels = [];
            $transmissions = [];
            $bodystyles = [];
            $doors = [];
            $enginesizes = [];
        }

        $pageName = "Search Results";
        // If someone has browsed to /search without typing anything we need to display a better
        // page than just "no deals available" so set this flag and change the text in the template
        if(is_null($searchedString)) {
            $typeSearch = false;
        }else{
            $typeSearch = true;
        }

        $vars = compact('summaryCap',
            'makes',
            'ranges',
            'fuels',
            'transmissions',
            'bodystyles',
            'doors',
            'enginesizes',
            'pageName',
            'finance_type',
            'searchedString',
            'listing_type',
            'database',
            'typeSearch');
        return View('leasing_views/search-results', $vars);
    }
}
