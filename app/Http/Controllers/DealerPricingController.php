<?php

namespace App\Http\Controllers;

use App\Libraries\TTNCApi;
use App\Models\User;
use App\Models\Cap;
use App\Models\Pricing;
use App\Models\Quote;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use jeremykenedy\LaravelRoles\Models\Role;

class DealerPricingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view_dealer_van_pricing(Request $request) {
        return $this->view_dealer_pricing($request, 'LCV');
    }

    public function view_dealer_pricing(Request $request, $database = 'CARS')
    {
        $data = $request->all();
        $summaryCap = [];
        $sumMakes = [];
        $sumModel = [];
        $sumDer = [];
        $sumBody = [];
        $sumTrans = [];
        $sumFuel = [];
        $sumDoor = [];

        if($database == 'LCV') {
            $pricestable = 'van_pricestable';
        }else{
            $pricestable = 'pricestable';
        }

        if (Auth::user()->hasRole(['dealer'])) {
            $getDealerID = Auth::user()->name;
            $summaryCap1 = DB::table($pricestable)
                ->join('capreference', $pricestable . '.cap_id', '=', 'capreference.cap_id')
                ->select($pricestable . '.*','capreference.*')
                ->whereNotNull($pricestable . '.monthly_payment')
                ->where('deal_id', '=', $getDealerID)
                ->where('capreference.database', '=', $database)
                ->when(isset($data['maker']), function ($query) use ($data) {
                    return $query->where('maker', '=', $data['maker']);
                })
                ->when(isset($data['range']), function ($query) use ($data) {
                    return $query->where('range', '=', $data['range']);
                })
                ->when(isset($data['model']), function ($query) use ($data) {
                    return $query->where('model', '=', $data['model']);
                })
                ->when(isset($data['trim']), function ($query) use ($data) {
                    return $query->where('trim', '=', $data['trim']);
                })
                ->when(isset($data['derivative']), function ($query) use ($data) {
                    return $query->where('derivative', '=', $data['derivative']);
                })
                ->when(isset($data['bodystyle']), function ($query) use ($data) {
                    return $query->where('bodystyle', '=', $data['bodystyle']);
                })
                ->when(isset($data['transmission']), function ($query) use ($data) {
                    return $query->where('transmission', '=', $data['transmission']);
                })
                ->when(isset($data['fuel']), function ($query) use ($data) {
                    return $query->where('fuel', '=', $data['fuel']);
                })
                ->when(isset($data['doors']), function ($query) use ($data) {
                    return $query->where('doors', '=', $data['doors']);
                })
                ->when(isset($data['finance_type']), function ($query) use ($data) {
                    return $query->where('finance_type', '=', $data['finance_type']);
                })
                ->when(isset($data['term']), function ($query) use ($data) {
                    return $query->where('term', '=', $data['term']);
                })
                ->when(isset($data['deposit_value']), function ($query) use ($data) {
                    return $query->where('deposit_value', '=', $data['deposit_value']);
                })
                ->when(isset($data['annual_mileage']), function ($query) use ($data) {
                    return $query->where('annual_mileage', '=', $data['annual_mileage']);
                })
                ->when(isset($data['in_stock']), function ($query) use ($data) {
                    return $query->where('in_stock', '=', $data['in_stock']);
                })
                ->limit(100)
                ->orderBy('monthly_payment', 'ASC')
                ->paginate(20);

            $summaryCap = ($summaryCap1);


            $summaryMakes = DB::table('capreference')
                ->when(isset($data['model']), function ($query) use ($data) {
                    return $query->where('model', '=', $data['model']);
                })
                ->when(isset($data['derivative']), function ($query) use ($data) {
                    return $query->where('derivative', '=', $data['derivative']);
                })
                ->when(isset($data['bodystyle']), function ($query) use ($data) {
                    return $query->where('bodystyle', '=', $data['bodystyle']);
                })
                ->when(isset($data['transmission']), function ($query) use ($data) {
                    return $query->where('transmission', '=', $data['transmission']);
                })
                ->when(isset($data['fuel']), function ($query) use ($data) {
                    return $query->where('fuel', '=', $data['fuel']);
                })
                ->when(isset($data['doors']), function ($query) use ($data) {
                    return $query->where('doors', '=', $data['doors']);
                })
                ->distinct()->get('maker');

            $sumMakes = ($summaryMakes);
            //dd($sumMakes);

            $summaryModels = DB::table('capreference')->distinct()
                ->when(isset($data['maker']), function ($query) use ($data) {
                    return $query->where('maker', '=', $data['maker']);
                })
                ->when(isset($data['derivative']), function ($query) use ($data) {
                    return $query->where('derivative', '=', $data['derivative']);
                })
                ->when(isset($data['bodystyle']), function ($query) use ($data) {
                    return $query->where('bodystyle', '=', $data['bodystyle']);
                })
                ->when(isset($data['transmission']), function ($query) use ($data) {
                    return $query->where('transmission', '=', $data['transmission']);
                })
                ->when(isset($data['fuel']), function ($query) use ($data) {
                    return $query->where('fuel', '=', $data['fuel']);
                })
                ->when(isset($data['doors']), function ($query) use ($data) {
                    return $query->where('doors', '=', $data['doors']);
                })
             ->get('model');
            $sumModel = ($summaryModels);

            $summaryDerivative = DB::table('capreference')->distinct()
                ->when(isset($data['maker']), function ($query) use ($data) {
                    return $query->where('maker', '=', $data['maker']);
                })
                ->when(isset($data['model']), function ($query) use ($data) {
                    return $query->where('model', '=', $data['model']);
                })
                ->when(isset($data['bodystyle']), function ($query) use ($data) {
                    return $query->where('bodystyle', '=', $data['bodystyle']);
                })
                ->when(isset($data['transmission']), function ($query) use ($data) {
                    return $query->where('transmission', '=', $data['transmission']);
                })
                ->when(isset($data['fuel']), function ($query) use ($data) {
                    return $query->where('fuel', '=', $data['fuel']);
                })
                ->when(isset($data['doors']), function ($query) use ($data) {
                    return $query->where('doors', '=', $data['doors']);
                })
                ->get('derivative');
            $sumDer = ($summaryDerivative);
            //dd($sumModel);

            $summaryBodyType = DB::table('capreference')->distinct()
                ->when(isset($data['maker']), function ($query) use ($data) {
                    return $query->where('maker', '=', $data['maker']);
                })
                ->when(isset($data['model']), function ($query) use ($data) {
                    return $query->where('model', '=', $data['model']);
                })
                ->when(isset($data['derivative']), function ($query) use ($data) {
                    return $query->where('derivative', '=', $data['derivative']);
                })
                ->when(isset($data['transmission']), function ($query) use ($data) {
                    return $query->where('transmission', '=', $data['transmission']);
                })
                ->when(isset($data['fuel']), function ($query) use ($data) {
                    return $query->where('fuel', '=', $data['fuel']);
                })
                ->when(isset($data['doors']), function ($query) use ($data) {
                    return $query->where('doors', '=', $data['doors']);
                })
                ->get('bodystyle');

            $sumBody = ($summaryBodyType);

            $summaryTransmission = DB::table('capreference')->distinct()
                ->when(isset($data['maker']), function ($query) use ($data) {
                    return $query->where('maker', '=', $data['maker']);
                })
                ->when(isset($data['model']), function ($query) use ($data) {
                    return $query->where('model', '=', $data['model']);
                })
                ->when(isset($data['derivative']), function ($query) use ($data) {
                    return $query->where('derivative', '=', $data['derivative']);
                })
                ->when(isset($data['bodystyle']), function ($query) use ($data) {
                    return $query->where('bodystyle', '=', $data['bodystyle']);
                })
                ->when(isset($data['fuel']), function ($query) use ($data) {
                    return $query->where('fuel', '=', $data['fuel']);
                })
                ->when(isset($data['doors']), function ($query) use ($data) {
                    return $query->where('doors', '=', $data['doors']);
                })
                ->get('transmission');

            $sumTrans = ($summaryTransmission);

            $summaryFuelType = DB::table('capreference')->distinct()
                ->when(isset($data['maker']), function ($query) use ($data) {
                    return $query->where('maker', '=', $data['maker']);
                })
                ->when(isset($data['model']), function ($query) use ($data) {
                    return $query->where('model', '=', $data['model']);
                })
                ->when(isset($data['derivative']), function ($query) use ($data) {
                    return $query->where('derivative', '=', $data['derivative']);
                })
                ->when(isset($data['bodystyle']), function ($query) use ($data) {
                    return $query->where('bodystyle', '=', $data['bodystyle']);
                })
                ->when(isset($data['transmission']), function ($query) use ($data) {
                    return $query->where('transmission', '=', $data['transmission']);
                })
                ->when(isset($data['doors']), function ($query) use ($data) {
                    return $query->where('doors', '=', $data['doors']);
                })
                ->get('fuel');

            $sumFuel = ($summaryFuelType);

            $summaryDoorType = DB::table('capreference')->distinct()
                ->when(isset($data['maker']), function ($query) use ($data) {
                    return $query->where('maker', '=', $data['maker']);
                })
                ->when(isset($data['model']), function ($query) use ($data) {
                    return $query->where('model', '=', $data['model']);
                })
                ->when(isset($data['derivative']), function ($query) use ($data) {
                    return $query->where('derivative', '=', $data['derivative']);
                })
                ->when(isset($data['bodystyle']), function ($query) use ($data) {
                    return $query->where('bodystyle', '=', $data['bodystyle']);
                })
                ->when(isset($data['transmission']), function ($query) use ($data) {
                    return $query->where('transmission', '=', $data['transmission']);
                })
                ->when(isset($data['fuel']), function ($query) use ($data) {
                    return $query->where('fuel', '=', $data['fuel']);
                })
                ->get('doors');

            $sumDoor = ($summaryDoorType);

            return View('my_account_views.view_dealer_pricing', compact('summaryCap', 'sumMakes', 'sumModel', 'sumDer', 'sumBody', 'sumTrans', 'sumFuel', 'sumDoor', 'database'));
        }else{
            $noDealer = 1;
            return View('my_account_views.view_dealer_pricing', compact('noDealer'));
        }
    }

    public function updatePrice(Request $request)
    {
        $pricesId  = $request['cap-id'];
        $updatePrice  = $request['update-price'];
        $updateDeposit  = $request['update-deposit'];
        $database = $request['database'];

        if($database == 'LCV') {
            $pricestable = 'van_pricestable';
        }else{
            $pricestable = 'pricestable';
        }

        $price = DB::table($pricestable)->where('id', '=', $pricesId)->first();

        $update =  DB::table($pricestable)->where('id', $pricesId)->update([
                        'monthly_payment'=> $updatePrice,
                'deposit_months'=>$updateDeposit
            ]);

        // Update average cost / total cost / total initial cost after the price change
        $averageCost = DB::raw("UPDATE pricestable SET average_cost = (monthly_payment*(term-1)+deposit_months+document_fee)/(term) WHERE id=" . $pricesId);
        $totalCost = DB::raw("UPDATE pricestable SET total_cost = (monthly_payment*(term-1)+deposit_months+document_fee) WHERE id=" . $pricesId);
        $totalInitialCost = DB::raw("UPDATE pricestable SET total_initial_cost = (deposit_months+document_fee) WHERE id=" . $pricesId);

        // Log price change in db
        DB::table('price_changes')
            ->insert([
                'deal_id' => $price->deal_id,
                'price_id' => $pricesId,
                'old_price' => $price->monthly_payment,
                'new_price' => $updatePrice,
                'old_deposit' => $price->deposit_months,
                'new_deposit' => $updateDeposit,
                'user_id' => Auth::user()->id,
                'updated_at' => now(),
                'database'=>$database
            ]);

        if($update) {
            return response()->json([
                'Status Updated',
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'Update failed',
            ], Response::HTTP_OK);
        }

    }



    public function view_dealers(Request $request)
    {
        $data = $request->all();

        if (Auth::user()->hasRole(['admin'])) {

            /*$DealerInfo = User::whereHas('roles', function($q){$q->whereIn('name', ['Dealer']);})
                //->join('quote', 'quote.deal_id', '=', 'users.name')->count();
                ->paginate(20);*/

            $dealerCalls = array();

            $DealerInfo = DB::select( DB::raw("select
                  t.name,
                  t.id, 
                  t.email,
                  t.phone,
                  t.logo,
                  count(distinct q.id) as TotalEnquiries,
                  SUM(CASE WHEN markassold=1 THEN 1 ELSE 0 END) as MarkedAsSold,
                  SUM(CASE WHEN q.status=6 THEN 1 ELSE 0 END) as Open,
                  SUM(CASE WHEN q.status=1 THEN 1 ELSE 0 END) as Quote_Sent,
                  SUM(CASE WHEN q.status=2 THEN 1 ELSE 0 END) as Sold,
                  SUM(CASE WHEN q.status=3 THEN 1 ELSE 0 END) as PendingPayment,
                  SUM(CASE WHEN q.status=4 THEN 1 ELSE 0 END) as Paid,
                  SUM(CASE WHEN q.status=5 THEN 1 ELSE 0 END) as Closed
                from
                  users t
                  left join role_user r ON r.user_id = t.id
                  left join quote q ON q.deal_id = t.name 
                WHERE r.role_id = '4'
                group by
                  t.name"));

            //dd($DealerInfo);
            foreach($DealerInfo as $dealer)
            {
                $dealerDeals[$dealer->name] = DB::table('pricestable')
                    ->select(DB::raw('count(*) as counter'))
                    ->where('deal_id', '=', $dealer->name)
                    ->get();
            }

            foreach($DealerInfo as $dealer) {

                $phone = str_replace(' ', '', $dealer->phone);
                
                $dealerCalls[$dealer->name] = DB::table('ttnc')
                    ->where('o_number', '=', $phone)
                    ->count();
            }


        }

        $vars = compact('DealerInfo', 'dealerDeals', 'dealerCalls');

        return View('my_account_views.view_dealers', $vars);
    }

}
