<?php

namespace App\Http\Controllers;

use App\Models\PriceHistory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use jeremykenedy\LaravelRoles\Models\Role;

class MyAccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function hello()
    {
        return view('my_account_views.my-account');
    }

    public function admin_tools() {

        return view('my_account_views.admin-tools');

    }

    public function check_imports() {

        if($_SERVER['HTTP_HOST']=='alc.local') {
            $dir = '/Users/scottmokler/sites/dealers/';
        }else{
            $dir = '/home/autoleasecompare/dealers/';
        }

	$dealers = array(
            'Blue Chilli Cars' => array(
                'dir' => 'bluechillicars'
            ),
            'Car Leasing-Online' => array(
                'dir'=>'carleaseonline'
            ),
            'Car Lease UK' => array(
                'dir'=>'carleaseuk'
            ),
            'GB Vehicle Leasing' => array(
                'dir'=>'gbleasing'
            ),
            'Genus Leasing' => array(
                'dir'=>'genus'
            ),
            'Leap Vehicle Leasing' => array(
                'dir'=>'leapleasing'
            ),
            'LeaseShop' => array(
                'dir'=>'leaseshop'
            ),
            'Lets Talk Leasing' => array(
                'dir'=>'letstalkleasing'
            ),
            'Rivervale' => array(
                'dir'=>'rivervale'
            ),
            'Vanarama' => array(
                'dir'=>'autorama'
            ),
            'Just Drive' =>array(
                'dir'=>'justdrive',
            ),
            'Total Vehicle Leasing' =>array(
                'dir'=>'totalvehicleleasing'
            ),
            'The Vehicle Leasing Expert' =>array(
                'dir'=>'vehicleleasingexpert'
            ),
            'Carparison' => array(
                'dir'=>'carparison'
            ),
            'Car Leasing Made Simple' => array(
                'dir'=>'clms'
            ),
            'Champion' => array(
                'dir'=>'dvs'
            ),

// CALAS
            'J&R Leasing'=>array(
                'dir'=>'00095',
                'calas'=>true
            ),
            'Lease4Less'=>array(
                'dir'=>'00590',
                'calas'=>true
            ),
            'FleetPrices'=>array(
                'dir'=>'00020',
                'calas'=>true
            ),
            'Ask 4 Leasing' => array(
                'dir'=>'00332',
                'calas'=>true
            ),
            'Dreamlease' => array(
                'dir'=>'dvs',
            ),
            'iCarlease' => array(
                'dir'=>'00569',
                'calas'=>true
            ),
            'Milease' => array(
                'dir'=>'01019',
                'calas'=>true
            ),



        );

        $dealerSql = DB::table('pricestable')
            ->SELECT('deal_id')
            ->distinct()
            ->get();

        $return = array();
        foreach($dealerSql as $dealer) {

            if(array_key_exists($dealer->deal_id, $dealers)) {

                // Calas deals
                if(isset($dealers[$dealer->deal_id]['calas']) && $dealers[$dealer->deal_id]['calas'] == true) {

                    $calasFiles = preg_grep('/^([^.])/', scandir($dir . '/calas/cars/'));

                    foreach($calasFiles as $file) {
                        if($file!='vehicle-lookup') {
                            $filename = $dir . '/calas/cars/' . $file;

                            if(strpos($filename, $dealers[$dealer->deal_id]['dir'])) {
                                $return[$dealer->deal_id][$file]['file_updated'] = date("Y-m-d", filemtime($filename));
                                $dealerUpdated = DB::table('pricestable')
                                    ->SELECT('updated_at')
                                    ->distinct()
                                    ->where('deal_id', '=', $dealer->deal_id)
                                    ->first();
                                $return[$dealer->deal_id][$file]['db_updated'] = $dealerUpdated->updated_at;
                            }
                        }
                    }


                // Independent brokers
                }else {

                    $files = preg_grep('/^([^.])/', scandir($dir . $dealers[$dealer->deal_id]['dir'] . '/cars/'));
                    foreach ($files as $file) {
                        $filename = $dir . $dealers[$dealer->deal_id]['dir'] . '/cars/' . $file;
                        $return[$dealer->deal_id][$file]['file_updated'] = date("Y-m-d", filemtime($filename));
                        $dealerUpdated = DB::table('pricestable')
                            ->SELECT('updated_at')
                            ->distinct()
                            ->where('deal_id', '=', $dealer->deal_id)
                            ->first();
                        $return[$dealer->deal_id][$file]['db_updated'] = $dealerUpdated->updated_at;
                    }
                }

            }

        }
        $vars = compact('return');

        return view('my_account_views.check-imports', $vars);
    }

}
