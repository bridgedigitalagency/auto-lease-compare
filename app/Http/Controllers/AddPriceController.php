<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Cap;
use App\Models\Pricing;
use App\Models\Quote;
use App\Models\VanPricing;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use jeremykenedy\LaravelRoles\Models\Role;

class AddPriceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addPrice(Request $request)
    {
        $data = $request->all();

        $summaryCap = [];
        $sumMakes = [];
        $sumModel = [];
        $sumDer = [];
        $sumCap = [];

        if (Auth::user()->hasRole(['dealer'])) {
            $getDealerID = Auth::user()->name;

            $summaryMakes = DB::table('capreference')
                ->when(isset($data['model']), function ($query) use ($data) {
                    return $query->where('model', '=', $data['model']);
                })
                ->when(isset($data['derivative']), function ($query) use ($data) {
                    return $query->where('derivative', '=', $data['derivative']);
                })
                ->distinct()->get('maker');

            $sumMakes = ($summaryMakes);
            //dd($sumMakes);

            $summaryModels = DB::table('capreference')->distinct()
                ->when(isset($data['maker']), function ($query) use ($data) {
                    return $query->where('maker', '=', $data['maker']);
                })
                ->when(isset($data['derivative']), function ($query) use ($data) {
                    return $query->where('derivative', '=', $data['derivative']);
                })
                ->get('model');
            $sumModel = ($summaryModels);

            $summaryDerivative = DB::table('capreference')->distinct()
                ->when(isset($data['maker']), function ($query) use ($data) {
                    return $query->where('maker', '=', $data['maker']);
                })
                ->when(isset($data['model']), function ($query) use ($data) {
                    return $query->where('model', '=', $data['model']);
                })
                ->get('derivative');
            $sumDer = ($summaryDerivative);

            $summaryCapID = DB::table('capreference')->distinct()
                ->when(isset($data['maker']), function ($query) use ($data) {
                    return $query->where('maker', '=', $data['maker']);
                })
                ->when(isset($data['model']), function ($query) use ($data) {
                    return $query->where('model', '=', $data['model']);
                })
                ->when(isset($data['derivative']), function ($query) use ($data) {
                    return $query->where('derivative', '=', $data['derivative']);
                })
                ->get('cap_id');
            $sumCap = ($summaryCapID);


            return View('my_account_views.add_dealer_price', compact('summaryCap', 'sumMakes', 'sumModel', 'sumDer', 'sumCap'));


        } else  {
            $noDealer = 1;
            return View('my_account_views.add_dealer_price', compact('noDealer'));
        }


    }

    /* *
     * Form Submission For New Price
     * */

    public function store(Request $request)
    {

        if (Auth::user()->hasRole(['dealer'])) {

            // Cap check
            $capId = $request['cap_id'];

            $check = DB::table('capreference')
                ->where('cap_id', '=', $capId)
                ->first();

            if ($check->database == 'CARS') {

                $pricing = new Pricing;
                $pricing->deal_id = \Auth::user()->name;
                $pricing->cap_id = $request['cap_id'];
                $pricing->finance_type = $request['finance_type'];
                $pricing->deposit_value = $request['deposit_value'];
                $pricing->term = $request['term'];
                $pricing->monthly_payment = $request['monthly_payment'];
                $pricing->annual_mileage = $request['annual_mileage'];
                $pricing->document_fee = $request['document_fee'];
                $pricing->in_stock = $request['in_stock'];
                $pricing->expires_at = '0000-00-00';
                $pricing->updated_at = now();
                $pricing->featured = '0';
                $pricing->deposit_months = $request['deposit_months'];
                $pricing->adverse_credit = $request['adverse_credit'];
                $pricing->average_cost = $request['average_cost'];
                $pricing->total_cost = $request['total_cost'];
                $pricing->save();

                return redirect()->back()->with('message', 'Thanks for adding a new price. The new deal is now live');
            }else{

                $pricing = new VanPricing;
                $pricing->deal_id = \Auth::user()->name;
                $pricing->cap_id = $request['cap_id'];
                $pricing->finance_type = $request['finance_type'];
                $pricing->deposit_value = $request['deposit_value'];
                $pricing->term = $request['term'];
                $pricing->monthly_payment = $request['monthly_payment'];
                $pricing->annual_mileage = $request['annual_mileage'];
                $pricing->document_fee = $request['document_fee'];
                $pricing->in_stock = $request['in_stock'];
                $pricing->expires_at = '0000-00-00';
                $pricing->updated_at = now();
                $pricing->featured = '0';
                $pricing->deposit_months = $request['deposit_months'];
                $pricing->adverse_credit = $request['adverse_credit'];
                $pricing->average_cost = $request['average_cost'];
                $pricing->total_cost = $request['total_cost'];
                $pricing->save();

                return redirect()->back()->with('message', 'Thanks for adding a new price. The new deal is now live');

            }

        }else{
            return redirect("/");
        }
    }

}
