<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Request;
use Mail;
use Carbon\Carbon;
use App\Models\Cap;

class WelcomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        $database = 'CARS';
        $cdnUrl = env('DO_IMAGE_PATH') . $database . '/';

            $makers = DB::table('manufacturers_search')
                ->where('database', '=', 'CARS')
                ->where('status', '=', 1)
                ->get();

            $bodystyles = DB::table('capreference')
                ->where('database', '=', 'CARS')
                ->groupBy('bodystyle')
                ->get();

            $getFeatured = DB::table('pricestable')
                ->join('capreference', 'pricestable.cap_id', '=', 'capreference.cap_id')
                ->join('users', 'pricestable.deal_id', '=', 'users.name')
                ->select('*', 'pricestable.id as id')
                ->where('featured', '=', '1')
                ->where('database', '=', 'CARS')

                ->inRandomOrder()
                ->limit(15)
                ->get();

            $count = 0;
            foreach($getFeatured as $featured) {
                if($featured->imageid > 0) {
                    $vehicleImage = $this->fetchVehicleDefaultImage($featured->cap_id, $featured->database);
                    $imageUrl = $cdnUrl . 'large/' . $vehicleImage->image_name;
                    if (@getimagesize($imageUrl)) {
                        $getFeatured[$count]->imageid = $imageUrl;
                    }else{
                        $getFeatured[$count]->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
                    }
                }else{
                    $getFeatured[$count]->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
                }
                $count = $count + 1;
            }



        // Check if WP is enabled
        if( env('WP_ENABLED') ) {

                $domain = Request()->server->get('SERVER_NAME');
                if ($domain == 'alc-master.local') {
                    $blogUrl = 'http://' . $domain . '/blog/wp-json/wp/v2/posts';
                } elseif ($domain == 'dev.autoleasecompare.com') {
                    $blogUrl = 'http://' . $domain . '/blog/wp-json/wp/v2/posts';
                } else {
                    $blogUrl = 'https://' . $domain . '/blog/wp-json/wp/v2/posts';
                }

                if (!isset($blogUrl)) {
                    $blogPosts = false;
                } else {
                    $headers = get_headers($blogUrl);
                }

                if (substr($headers[0], 9, 3) != "200") {
                    $blogposts = false;
                } else {
                    $result_from_json = file_get_contents($blogUrl);
                    $blogposts = json_decode($result_from_json, true);
                }

            }
            // Set blogposts as an empty array otherwise
            else{
                $blogposts = [];
            }

            return view('welcome', compact('makers', 'getFeatured', 'database', 'blogposts', 'bodystyles'));


    }

    public function welcome_vans()
    {

        $database = 'LCV';
        $cdnUrl = env('DO_IMAGE_PATH') . $database . '/';

        // get all manufacturers
        $makers = DB::table('capreference')
            ->select(DB::raw('DISTINCT(`maker`)'))
            ->where('database', '=', 'LCV')
            ->orderBy('maker')
            ->get();
        foreach ($makers as $maker) {
            $ranges = DB::table('capreference')
                ->select(DB::raw('DISTINCT(`range`)'))
                ->where('database', '=', 'LCV')
                ->where('maker', '=', $maker->maker)
                ->get();
            foreach ($ranges as $range) {
                $options[$maker->maker][] = $range->range;
            }
        }

        $bodystyles = DB::table('capreference')
            ->where('database', '=', 'LCV')
            ->groupBy('bodystyle')
            ->get();

        $getFeatured = DB::table('van_pricestable')
            ->join('capreference', 'van_pricestable.cap_id', '=', 'capreference.cap_id')
            ->join('users', 'van_pricestable.deal_id', '=', 'users.name')
            ->select('*', 'van_pricestable.id as id')
            ->where('featured', '=', '1')
            ->where('database','=','LCV')

            ->inRandomOrder()
            ->limit(15)
            ->get();

        $count = 0;
        foreach($getFeatured as $featured) {
            if($featured->imageid != 0) {
                $vehicleImage = $this->fetchVehicleDefaultImage($featured->cap_id, $featured->database);
                if(is_null($vehicleImage)) {
                    $getFeatured[$count]->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
                }

                if (isset($vehicleImage->image_name) && !is_null($vehicleImage->image_name)) {
                    $imageUrl = $cdnUrl . 'large/' . $vehicleImage->image_name;
                    if (@getimagesize($imageUrl)) {
                        $getFeatured[$count]->imageid = $imageUrl;
                    } else {
                        $getFeatured[$count]->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
                    }
                }
            }else{
                $getFeatured[$count]->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
            }
            $count = $count + 1;
        }

        //dd($getFeatured);
        $domain = Request()->server->get('SERVER_NAME');
        if($domain == 'alc-master.local') {
            $blogUrl = 'http://' . $domain . '/blog/wp-json/wp/v2/posts';
        }elseif($domain == 'dev.autoleasecompare.com') {
            $blogUrl = 'http://' . $domain . '/blog/wp-json/wp/v2/posts';
        }else{
            $blogUrl = 'https://' . $domain . '/blog/wp-json/wp/v2/posts';
        }

        $headers = get_headers($blogUrl);
        if(substr($headers[0], 9, 3) != "200"){
            $blogposts = false;
        }else{
            $result_from_json = file_get_contents($blogUrl);
            $blogposts = json_decode($result_from_json,true );
        }

        return view('welcome', compact('options', 'makers', 'getFeatured', 'database', 'blogposts','bodystyles'));

    }





    public function info()
    {
        $faqs = DB::table('faqs')
            ->get();
        return view('pages.leasing-information', compact('faqs'));
    }

    public function sell()
    {
        return view('sell-your-car');
    }

    public function updateFeatured(\Illuminate\Http\Request $request)
    {
        $capId  = $request['cap-id'];
        $updateFeatured  = $request['update-featured'];

        DB::table('pricestable')->where('id', $capId)->update(['featured' => $updateFeatured]);

        return response()->json([
            'Status Updated',
        ], Response::HTTP_OK);
    }

    public function terms()
    {
        return view('site_views.terms');
    }

    public function cookies()
    {
        return view('site_views.cookies');
    }

    public function privacy()
    {
        return view('site_views.privacy');
    }

    public function partners()
    {
        return view('site_views.partners');
    }

    public function logPartner(Request $request)
    {


        $emailtext = Request::all();
	  if(!isset($emailtext['cars'])) {
            return redirect("/site/partners?e=1");
        }
        if($emailtext['cars']!='3') {
            return redirect("/site/partners?e=1");
        }

        // email the form to alc
//        $email = 'scott@bridgedigitalagency.co.uk';
        $email = 'info@autoleasecompare.com';
        $contact_company = 'Auto Lease Compare';
        $message = '';
        $subject = 'Partners Enquiry - Auto Lease Compare';

        $redirectTo = "/site/partners?s=1";

        Mail::send(array('html' => 'emails.partner-enquiry'), array('text' => $emailtext), function($message) use ($email, $subject, $contact_company)
        {
            $message->to($email, $contact_company)->subject($subject);
        });

        return redirect($redirectTo);
    }

    public function viewHomeSearch() {
        $debug = env('APP_DEBUG');

        if (!request()->ajax()) {
            if(!$debug) {
                return redirect('/');
            }
        }

        $requestData = Request::all();
        $database = $requestData['database'];

        // Logic for the extra fields on homepage form
        if(app('request')->input('e')==1) {

            $bodystyles  = DB::table('capreference')
                ->select(DB::raw('DISTINCT(`bodystyle`)'))

                ->when(isset($requestData['maker']) && !is_null($requestData['maker']), function ($query) use ($requestData) {
                    if (is_array($requestData['maker'])) {
                        return $query->wherein('maker', $requestData['maker']);
                    } else {
                        return $query->where('maker', '=', $requestData['maker']);
                    }
                })

                ->when(isset($requestData['range']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {

                    if (is_array($requestData['range'])) {
                        return $query->wherein('range', $requestData['range']);
                    } else {
                        return $query->where('range', '=', $requestData['range']);
                    }
                })
                ->when(isset($requestData['transmission']) && isset($requestData['range']) && is_array($requestData['range']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['transmission'])) {
                        if($requestData['transmission'][0]=='') {} else {
                            return $query->wherein('transmission', $requestData['transmission']);
                        }
                    } else {
                        return $query->where('transmission', '=', $requestData['transmission']);
                    }
                })
                ->where('database', '=', $database)
                ->orderBy('bodystyle')
                ->get();

            $transmission  = DB::table('capreference')
                ->select(DB::raw('DISTINCT(`transmission`)'))

                ->when(isset($requestData['maker']) && isset($requestData['range']) && is_array($requestData['range']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['maker'])) {
                        return $query->wherein('maker', $requestData['maker']);
                    } else {
                        return $query->where('maker', '=', $requestData['maker']);
                    }
                })

                ->when(isset($requestData['range']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['range'])) {
                        return $query->wherein('range', $requestData['range']);
                    } else {
                        return $query->where('range', '=', $requestData['range']);
                    }
                })
                ->when(isset($requestData['bodystyle']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['bodystyle'])) {
                        if($requestData['bodystyle'][0]=='') {} else {
                            return $query->wherein('bodystyle', $requestData['bodystyle']);
                        }
                    } else {
                        return $query->where('bodystyle', '=', $requestData['bodystyle']);
                    }
                })
                ->when(isset($requestData['fuel']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['fuel'])) {
                        if($requestData['fuel'][0]=='') {} else {
                            return $query->wherein('fuel', $requestData['fuel']);
                        }
                    } else {
                        return $query->where('fuel', '=', $requestData['fuel']);
                    }
                })
                ->where('database', '=', $database)
                ->orderBy('transmission')
                ->get();


            $fuel  = DB::table('capreference')
                ->select(DB::raw('DISTINCT(`fuel`)'))

                ->when(isset($requestData['maker']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['maker'])) {
                        return $query->wherein('maker', $requestData['maker']);
                    } else {
                        return $query->where('maker', '=', $requestData['maker']);
                    }
                })

                ->when(isset($requestData['range']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['range'])) {
                        return $query->wherein('range', $requestData['range']);
                    } else {
                        return $query->where('range', '=', $requestData['range']);
                    }
                })
                ->when(isset($requestData['bodystyle']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['bodystyle'])) {
                        if($requestData['bodystyle'][0]=='') {} else {

                            return $query->wherein('bodystyle', $requestData['bodystyle']);
                        }
                    } else {
                        return $query->where('bodystyle', '=', $requestData['bodystyle']);
                    }
                })
                ->when(isset($requestData['transmission']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['transmission'])) {
                        if($requestData['transmission'][0]=='') {} else {
                            return $query->wherein('transmission', $requestData['transmission']);
                        }
                    } else {
                        return $query->where('transmission', '=', $requestData['transmission']);
                    }
                })
                ->where('database', '=', $database)
                ->orderBy('fuel')
                ->get();

            $enginesizes  = DB::table('capreference')
                ->select(DB::raw('DISTINCT(`enginesize`)'))

                ->when(isset($requestData['maker']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['maker'])) {
                        return $query->wherein('maker', $requestData['maker']);
                    } else {
                        return $query->where('maker', '=', $requestData['maker']);
                    }
                })

                ->when(isset($requestData['range']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['range'])) {
                        return $query->wherein('range', $requestData['range']);
                    } else {
                        return $query->where('range', '=', $requestData['range']);
                    }
                })
                ->when(isset($requestData['bodystyle']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['bodystyle'])) {
                        if($requestData['bodystyle'][0]=='') {} else {

                            return $query->wherein('bodystyle', $requestData['bodystyle']);
                        }
                    } else {
                        return $query->where('bodystyle', '=', $requestData['bodystyle']);
                    }
                })
                ->when(isset($requestData['transmission']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['transmission'])) {
                        if($requestData['transmission'][0]=='') {} else {
                            return $query->wherein('transmission', $requestData['transmission']);
                        }
                    } else {
                        return $query->where('transmission', '=', $requestData['transmission']);
                    }
                })
                ->when(isset($requestData['fuel']) && !is_null($requestData['range'][0]), function ($query) use ($requestData) {
                    if (is_array($requestData['fuel'])) {
                        if($requestData['fuel'][0]=='') {} else {
                            return $query->wherein('fuel', $requestData['fuel']);
                        }
                    } else {
                        return $query->where('fuel', '=', $requestData['fuel']);
                    }
                })
                ->where('database', '=', $database)
                ->get();
                $enginesizes = json_decode(json_encode($enginesizes), true);

            $vars = compact(
                'fuel',
                'bodystyles',
                'transmission',
                'enginesizes'
            );
            return view('partials.homepage-models', $vars);

        // Logic for populating model select box on homepage form
        }else {
            $models = DB::table('capreference')
                ->select(DB::raw('DISTINCT(`range`)'))
                ->when(isset($requestData['maker']), function ($query) use ($requestData) {
                    if (is_array($requestData['maker'])) {
                        return $query->wherein('maker', $requestData['maker']);
                    } else {
                        return $query->where('maker', '=', $requestData['maker']);
                    }
                })
                ->where('database', '=', $database)
                ->orderBy('range')
                ->get();

            return view('partials.homepage-models', compact('models'));
        }


    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description Little page here to check everything is running ok
     */
    public function viewHeartbeat()
    {

        // Check how many deals we have, if its less than an amount send an email out
        $deals = DB::table('pricestable')
            ->select(DB::raw('COUNT(*) AS deals'))
            ->first();
        $numberOfDeals = $deals->deals;

        $dwv = DB::table('dwv_submissions')
            ->count();

        $vandeals = DB::table('van_pricestable')
            ->select(DB::raw('COUNT(*) AS deals'))
            ->first();
        $numberOfVanDeals = $vandeals->deals;

        // Check how many cars we have
        $caps = DB::table('capreference')
            ->select(DB::raw('COUNT(*) as caps'))
            ->where('database', '=', 'CARS')
            ->first();
        $numberOfCaps = $caps->caps;

        // Check how many cars we have
        $vancaps = DB::table('capreference')
            ->select(DB::raw('COUNT(*) as caps'))
            ->where('database', '=', 'LCV')
            ->first();
        $numberOfVanCaps = $vancaps->caps;

        // Check how many dealers we have
        $dealers = DB::table('pricestable')
            ->select(DB::raw('COUNT(DISTINCT(deal_id)) as dealers'))
            ->first();
        $numberOfDealers = $dealers->dealers;

        // Check how many users we have
        $users = DB::table('users')
            ->SELECT(DB::raw('COUNT(*) as users'))
            ->first();
        $numberOfUsers = $users->users;

        // Check how many deals we have today CARS
        $deals = DB::table('quote')
            ->SELECT(DB::raw('COUNT(*) as deals'))
            ->whereDate('created_at', Carbon::today())
            ->where('database', '!=', 'LCV')
            ->first();
        $todaysDeals = $deals->deals;

        $deals = DB::table('quote')
            ->SELECT(DB::raw('COUNT(*) as deals'))
            ->whereDate('created_at', Carbon::yesterday())
            ->where('database', '!=', 'LCV')
            ->first();
        $yesterdaysDeals = $deals->deals;

        // Check how many calls we have today
        $deals = DB::table('ttnc')
            ->SELECT(DB::raw('COUNT(*) as deals'))
            ->whereDate('c_date', Carbon::today())
            ->first();
        if(is_null($deals)) {
            $todaysCalls = 0;
        }else{
            $todaysCalls = $deals->deals;
        }

        // Check how many calls we have yesterday
        $deals = DB::table('ttnc')
            ->SELECT(DB::raw('COUNT(*) as deals'))
            ->whereDate('c_date', Carbon::yesterday())
            ->first();
        if(is_null($deals)) {
            $yesterdaysCalls = 0;
        }else{
            $yesterdaysCalls = $deals->deals;
        }

        // Check how many deals we have this month
        $month = date('m');
        $year = date('Y');
        $deals = DB::table('quote')
            ->SELECT(DB::raw('COUNT(*) as deals'))
            ->whereMonth('created_at', $month)
            ->whereYear('created_at', $year)
            ->where('database', '!=', 'LCV')
            ->first();
        $thisMonthDeals = $deals->deals;

        // Check how many calls we have this month
        $month = date('m');
        $year = date('Y');
        $deals = DB::table('ttnc')
            ->SELECT(DB::raw('COUNT(*) as deals'))
            ->whereMonth('c_date', $month)
            ->whereYear('c_date', $year)
            ->first();
        $thisMonthCalls = $deals->deals;

        // Check how many deals we have this month
        $month = date('m', strtotime('first day of last month'));
        $year = date('Y', strtotime('first day of last month'));
        $deals = DB::table('quote')
            ->SELECT(DB::raw('COUNT(*) as deals'))
            ->whereMonth('created_at', $month)
            ->whereYear('created_at', $year)
            ->where('database', '!=', 'LCV')
            ->first();
        $lastMonthDeals = $deals->deals;

        // Check how many calls we have last month
        $month = date('m', strtotime('first day of last month'));
        $year = date('Y', strtotime('first day of last month'));
        $deals = DB::table('ttnc')
            ->SELECT(DB::raw('COUNT(*) as deals'))
            ->whereMonth('c_date', $month)
            ->whereYear('c_date', $year)
            ->first();
        $lastMonthCalls = $deals->deals;


        // ================= VANS =============== //
        // Check how many deals we have today
        $deals = DB::table('quote')
            ->SELECT(DB::raw('COUNT(*) as deals'))
            ->whereDate('created_at', Carbon::today())
            ->where('database', '=', 'LCV')
            ->first();
        if(is_null($deals)) {
            $todaysVanDeals = 0;
        }else {
            $todaysVanDeals = $deals->deals;
        }
        $deals = DB::table('quote')
            ->SELECT(DB::raw('COUNT(*) as deals'))
            ->whereDate('created_at', Carbon::yesterday())
            ->where('database', '=', 'LCV')
            ->first();
        if(is_null($deals)) {
            $yesterdaysVanDeals = 0;
        }else {
            $yesterdaysVanDeals = $deals->deals;
        }

        // Check how many deals we have this month
        $month = date('m');
        $year = date('Y');
        $deals = DB::table('quote')
            ->SELECT(DB::raw('COUNT(*) as deals'))
            ->whereMonth('created_at', $month)
            ->whereYear('created_at', $year)
            ->where('database', '=', 'LCV')
            ->first();
        $thisMonthVanDeals = $deals->deals;

        // Check how many deals we have this month
        $month = date('m', strtotime('first day of last month'));
        $year = date('Y', strtotime('first day of last month'));
        $deals = DB::table('quote')
            ->SELECT(DB::raw('COUNT(*) as deals'))
            ->whereMonth('created_at', $month)
            ->whereYear('created_at', $year)
            ->where('database', '=', 'LCV')
            ->first();
        $lastMonthVanDeals = $deals->deals;

        $garage = DB::table('garage')
            ->selectRaw('DISTINCT(`user_id`)')
            ->get();
        $totalGarage = count($garage);


        // pull through all van brokers + number of deals
        $vBrokers = [];
        $vanBrokers = DB::table('van_pricestable')
            ->select(DB::raw('DISTINCT(`deal_id`)'))
            ->get();

        foreach($vanBrokers as $broker) {
            if($broker->deal_id == 'Dealer Testing 1') {continue;}
            $count = DB::table('van_pricestable')
                ->where('deal_id', '=', $broker->deal_id)
                ->count();
            $vBrokers[$broker->deal_id] = $count;
        }


        $vars = compact(
            'numberOfDeals',
            'numberOfCaps',
            'numberOfDealers',
            'numberOfUsers',
            'todaysDeals',
            'thisMonthDeals',
            'lastMonthDeals',
            'todaysCalls',
            'thisMonthCalls',
            'lastMonthCalls',
            'todaysVanDeals',
            'lastMonthVanDeals',
            'thisMonthVanDeals',
            'totalGarage',
            'numberOfVanDeals',
            'dwv',
            'vBrokers',
            'numberOfVanCaps',
            'yesterdaysDeals',
            'yesterdaysVanDeals',
            'yesterdaysCalls'
        );

        If(Auth::check() && Auth::user()->isAdmin()) {
            return view('pages.heartbeat-admin', $vars);
        }else{
            return view('pages.heartbeat');
        }
    }

    /**
     * @param null $cap_id
     * @param $database
     * @return false|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    private function fetchVehicleDefaultImage($cap_id = null, $database) {

        if(isset($cap_id) && !is_null($cap_id)) {
            $image = DB::table('cap_images')
                ->where('cap_id', '=', $cap_id)
                ->where('is_default', '=', 1)
                ->where('database', '=', $database)
                ->first();

            return $image;
        }else{
            return false;
        }

    }

}
