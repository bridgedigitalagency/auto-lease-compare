<?php

namespace App\Http\Controllers;

use App\Libraries\TTNCApi;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Console\Input\Input;


class CallsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function processPayload(Request $request)
    {

        $data = $request->all();
        if ($data['event']['event_type'] == 'cdr') {
            // call connected
            if (isset($data['event']['duration'])) {

                if($data['event']['duration'] > 0) {
                    $status = 1;
                }else{
                    $status = 0;
                }

                // Fetch from user table by the phone number, if it matches send out the email
                $searchQuery = User::select()
                    ->where('phone', '=', $data['event']['bnumber'])
                    ->first();

                DB::table('ttnc')->insert([
                    'data' => serialize($data),
                    'c_ref' => $data['event']['ref'],
                    'i_number' => $data['event']['anumber'],
                    'o_number' => $data['event']['bnumber'],
                    'c_date' => $data['event']['call_date'],
                    'c_time' => $data['event']['call_time'],
                    'c_duration' => $data['event']['duration'],
                    'status' => $status,
                    'deal_id' => $searchQuery->name
                ]);

                $emailTemplate = 'emails.phone_enquiry';
                $subject = 'Auto Lease Compare - New phone enquiry';
                $contact_company = 'Auto Lease Compare';
//                $email = 'scott@bridgedigitalagency.net';
                $email = $searchQuery->email;


                if (isset($email) && isset($emailTemplate) && !is_null($email)) {

                    $message = '';

                    Mail::send(array('html' => $emailTemplate), array('callInfo' => $data['event']), function ($message) use (
                        $email,
                        $subject,
                        $contact_company
                    ) {
                        $message->to($email, $contact_company)->subject($subject);
                    });
                }

		if($searchQuery->name == 'Just Drive') {

                    $confirm_quoteid = $data['event']['ref'];

                    echo "<script type='text/javascript'src=\"https://track.omguk.com/2103151/e/ss/?APPID=call-{{$confirm_quoteid}}&AID=2136424&MID=2103151&PID=39626&Status=&Action=Lead&ex1=&ex2=&Channel=optimise\"></script><noscript><img src=\"https://track.omguk.com/e/si/?APPID=call-{{$confirm_quoteid}}&AID=2136424&MID=2103151&PID=39626&Status=&Action=Lead&ex1=&ex2=&Channel=optimise\" border=\"0\" height=\"1\" width=\"1\"></noscript>";
                }

            }


        }
    }
}
