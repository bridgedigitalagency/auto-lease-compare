<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use SoapClient;
use SimpleXMLElement;


class CapUpdateControllerold extends Controller
{

    // move this into config file
    public $subscriberId = 175356;
    public $password = "Jlerl67";

    public $wsdl = "https://soap.cap.co.uk/Vehicles/CapVehicles.asmx?wsdl";
    public $database = "LCV";

    public function update_van_leasing() {

        // DB clearup first
//        $this->prepDatabase();

        $models = array();
        $derivs = array();

        // First fetch manufacturers
//        $makers = $this->fetchManufacturers();
//
//        // Now for each maker go and grab the models
//        foreach ($makers as $make) {
//            $models[] = $this->fetchModels($make);
//        }

        $derivs[] = $this->fetchDerivatives();

    }

    private function prepDatabase() {

//        DB::table('van_manufacturers')->truncate();
//        DB::table('van_models')->truncate();
//        DB::table('van_derivatives')->truncate();
    }

    private function executeQuery($url) {

        $wsdl = $this->wsdl;
        $client = new SoapClient($wsdl, array('soap_version' => SOAP_1_1,'trace' => true,));

        return $client;

    }

    /**
     * @return array
     */
    private function fetchManufacturers() {

        $makers = array();

        $url = "https://soap.cap.co.uk/Vehicles/CapVehicles.asmx/GetCapidFromCapcode";
        $args = array(
            'subscriberId'=> $this->subscriberId,
            'password'=> $this->password,
            'database'=> $this->database,
            'justCurrentManufacturers'=> true,
            'bodyStyleFilter'=> '',
        );
        $client = $this->executeQuery($url, $args);

        try {
            $result = $client->GetCapMan($args);
            $result = $result->GetCapManResult->Returned_DataSet->any;

        } catch (SoapFault $e) {
            echo "Error: {$e}";
        }

        $xml = simplexml_load_string($result);

        foreach($xml as $item) {
            foreach($item as $manufacturer) {

                $makers[] = trim($manufacturer->CMan_Code->__toString());

                $makerdb = DB::table('van_manufacturers')
                    ->where('id', $manufacturer->CMan_Code)
                    ->exists();

                if(!$makerdb) {
                    DB::table('van_manufacturers')->insert([
                        'id' => $manufacturer->CMan_Code,
                        'name' => trim($manufacturer->CMan_Name->__toString())
                    ]);
                }
            }
        }
        return $makers;
    }

    /**
     * @param null $maker
     * @return array|bool
     */
    private function fetchModels($maker=null) {

        if(!$maker) {
            return false;
        }
        $models = array();

        $url = "https://soap.cap.co.uk/Vehicles/CapVehicles.asmx/GetCapMod";
        $args = array(
            'subscriberId'=> $this->subscriberId,
            'password'=> $this->password,
            'database'=> $this->database,
            'manRanCode'=>$maker,
            'manRanCodeIsMan'=>true,
            'justCurrentModels'=> true,
            'bodyStyleFilter'=> '',
        );
        $client = $this->executeQuery($url);
        try {
            $result = $client->GetCapMod($args);
            $result = $result->GetCapModResult->Returned_DataSet->any;

        } catch (SoapFault $e) {
            echo "Error: {$e}";
        }

        $xml = simplexml_load_string($result);

        foreach($xml as $item) {
            foreach($item as $model) {

                $models[] = trim($model->CMod_Code->__toString());
                $modeldb = DB::table('van_models')
                    ->where('id', $model->CMod_Code)
                    ->exists();
                if(!$modeldb) {
                    DB::table('van_models')->insert([
                        'id' => $model->CMod_Code,
                        'maker' => $maker,
                        'model' => trim($model->CMod_Name->__toString()),
                        'year' => trim($model->CMod_Introduced->__toString())
                    ]);
                }

            }
        }

        return $models;
    }

    /**
     * @param $model
     * @return array|bool
     */
    private function fetchDerivatives() {

        $models = DB::select(DB::raw("SELECT * FROM van_models WHERE id NOT IN (SELECT model FROM van_derivatives)"));
        foreach($models as $model) {


            $derivs = array();

            $url = "https://soap.cap.co.uk/Vehicles/CapVehicles.asmx/GetCapDer";
            $args = array(
                'subscriberId' => $this->subscriberId,
                'password' => $this->password,
                'database' => $this->database,
                'modCode' => $model->id,
                'justCurrentDerivatives' => true,
            );
            $client = $this->executeQuery($url);
            try {
                $result = $client->GetCapDer($args);
                $result = $result->GetCapDerResult->Returned_DataSet->any;

            } catch (SoapFault $e) {
                echo "Error: {$e}";
            }

            $xml = simplexml_load_string($result);

            foreach ($xml as $item) {
                foreach ($item as $deriv) {

                    $derivs[] = trim($deriv->CMod_Code->__toString());
                    $modeldb = DB::table('van_derivatives')
                        ->where('cap_id', $deriv->CDer_ID)
                        ->exists();

                    if(!$modeldb) {
                        DB::table('van_derivatives')->insert([
                            'cap_id' => $deriv->CDer_ID,
                            'model_id' => $model->id,
                            'derivative' => trim($deriv->CDer_Name->__toString()),
                            'introduced' => trim($deriv->CDer_Introduced->__toString()),
                            'year' => trim($deriv->ModelYearRef->__toString())
                        ]);
                    }


                }
            }
        }

    }

}