<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Pricing;
use App\Models\Quote;
use App\Models\Cap;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Mail;
use Carbon\Carbon;
use DateTime;
use App\Models\PriceHistory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use jeremykenedy\LaravelRoles\Models\Role;
use SoapClient;
use SimpleXMLElement;

class QuoteController extends Controller
{

    CONST HOUR = 60;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public $nvdWsdl = "https://soap.cap.co.uk/Nvd/CapNvd.asmx?wsdl";

    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    public function view_quote(Request $request)
    {

        $stored_cookies = [
            'name' => $request->cookie('name'),
            'email' => $request->cookie('email'),
            'phone_number' => $request->cookie('phone_number'),
            'contact_preference' =>  $request->cookie('contact_preference'),
            'comment' => $request->cookie('comment'),
            'subject' => $request->cookie('subject')
        ];



        $showRelated = 1;

        if(isset($_SERVER['HTTP_REFERER'])) {
            $prevUrl = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);
            $prevUrl = str_replace('/', '', $prevUrl);
        }else{
            $prevUrl = '';
        }

        // Check url has /cars || /lcv at end of it, if not work out what deal this is.
        if(isset($request['database']) && !is_null($request['database'])) {
            $database = strtoupper($request['database']);
        }else{

            if(isset($request['id'])) {
                $database = $this->findDealDatabase($request['id']);

                if($database) {
                    if (!strpos($_SERVER['HTTP_USER_AGENT'], 'bot')) {
                        Log::info('Quote URL error.', ['prevUrl' => $prevUrl, 'ID' => $request['id'], 'User Agent' => $_SERVER['HTTP_USER_AGENT']]);
                    }

                    return redirect('/quote/enquiry/' . $request['id'] . '/' . $database);
                }else{
                    if(!strpos($_SERVER['HTTP_USER_AGENT'], 'bot')) {
                        Log::info('Deal doesn\'t exist - 404', ['prevUrl' => $prevUrl, 'ID' => $request['id']]);
                    }
                    return redirect('/quote/enquiry/404');
                }		  
            }else{
                return redirect('/quote/enquiry/404');
            }
	}

        $excludedManufacturers = explode(',', env('EXCLUDED_MANUFACTURERS'));
        $excludedDealers = explode(',', env('EXCLUDED_DEALERS'));

        $data = $request;

        if($database == 'LCV') {

            $single_quote = DB::table('van_pricestable')
                ->join('capreference', 'van_pricestable.cap_id', '=', 'capreference.cap_id')
                ->select('van_pricestable.*','capreference.*')
                ->where('capreference.database','=',$database)
                ->when(isset($data['id']), function ($query) use ($data) {
                    return $query->where('id', '=', $data['id']);
                })
                ->first();


            if(is_null($single_quote)) {
                return redirect('/quote/enquiry/404');
            }
        }else{
            /*Get Price and cap*/
            $single_quote = DB::table('pricestable')
                ->join('capreference', 'pricestable.cap_id', '=', 'capreference.cap_id')
                ->select('pricestable.*','capreference.*')
                ->where('capreference.database','=',$database)
                ->when(isset($data['id']), function ($query) use ($data) {
                    return $query->where('id', '=', $data['id']);
                })
                ->first();
        }
	    if(!isset($single_quote->deal_id)) {
		    return redirect('quote/enquiry/404');
	    }

	    $usercheck = DB::table('users')
            ->where('name', '=', $single_quote->deal_id)
            ->first();

	    if(!$usercheck) {
            return redirect('quote/enquiry/404');
        }


//            $database = $single_quote->database;

//            $single_quote = ($single_quote1[0]);

        if(in_array($single_quote->deal_id, $excludedDealers)) {
            return redirect('/quote/enquiry/404');
        }
        if(in_array($single_quote->maker, $excludedManufacturers)) {
            return redirect('/quote/enquiry/404');
        }

        if($single_quote->deal_id == 'dealer paused') {
            return redirect('/quote/enquiry/404');
        }

        if($prevUrl == 'hybrid') {
            $single_quote->breadcrumb ='H';
        }elseif($prevUrl == 'adverse-credit') {
            $single_quote->breadcrumb ='A';
        }elseif($prevUrl == 'performance') {
            $single_quote->breadcrumb ='PERFORMANCE';
        }elseif($prevUrl == 'personal-vans') {
            $single_quote->breadcrumb ='PV';
        }elseif($prevUrl == 'business-vans') {
            $single_quote->breadcrumb ='BV';
        }
        // Fetch optional extras from CAP
        $this->subscriberId = env('CAP_SUB_ID');
        $this->password = env('CAP_SUB_PASSWORD');

        // check DB for optional extras for this cap id. If nothing from time period
        $options = $this->checkOptionsDB($single_quote->cap_id, $database);

        if(!$options) {
            $options = $this->fetchOptionalExtras($single_quote->cap_id, $database);
        }

        $techData = $this->checkTechData($single_quote->cap_id, $database);

        if(!$techData) {
            $techData = $this->getTechnicalData($single_quote->cap_id, $database);
        }

        // Logic to generate UID for testing new deal ids
        $uid = hash('ripemd160',$database . $single_quote->cap_id . $single_quote->deal_id . $single_quote->term . $single_quote->finance_type . $single_quote->deposit_value . $single_quote->annual_mileage);

        if($database == 'LCV') {
            $canddCapId = ($single_quote->cap_id + 500000);
        }else{
            $canddCapId = $single_quote->cap_id;
        }

            /* Get Car and Driving Review and Image Data */
        $candd = "https://www.caranddriving.com/ws/A041/ws.asmx/GetReviewsFromCapID?user_id=autoleasecompare&capid=$canddCapId&type=&getsummaries=true&liveonly=true";
//        $responseCandD = '';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_URL,$candd);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
        $canddInfo = curl_getinfo($ch);
        if($canddInfo['http_code'] == '0') {
            $responseCandD = curl_exec($ch);
        }
        curl_close($ch);

        if(isset($responseCandD)) {

            if(strpos($responseCandD, 'NullReferenceException')) {
                $json_output1 = array();
            }else {

                $xml = (array)simplexml_load_string($responseCandD);

                if (isset($xml['code']) && $xml['code'] == 0) {

                    $json_output = $xml['reviewlist'];

                    if (isset($json_output->review[1])) {
                        $json_output1 = $json_output->review[1];
                    } else {
                        $json_output1 = $json_output->review[0];
                    }

                } else {
                    $json_output1 = array();
                }
            }
        }else{
            $json_output1 = array();
        }


        // Deal with vehicle ratings
        if(isset($json_output1->scoringlist)) {
            $scoringList = $json_output1->scoringlist;

            foreach ($scoringList->scoring as $score) {
                if ($score->isdefault == 'true') {
                    $vehicleRating = $score;
                }
            }
        }else{
            $vehicleRating = false;
        }

        $cdnUrl = env('DO_IMAGE_PATH') . $database . '/';

        if($single_quote->imageid > 0) {
            $vehicleImages = $this->fetchAllVehicleImages($single_quote->cap_id, $database);
            foreach($vehicleImages as $image) {
                $imageUrl = $cdnUrl . 'big/' . $image->image_name;
                $capImages[] = $imageUrl;
            }

        }else{
            $capImages[] = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
        }


        // Get technical info from API
        $urlColour = "http://api.autoleasecompare.com/cap/get-by-id/$single_quote->cap_id";
        $responseColour = file_get_contents($urlColour);
        $jsond = json_decode($responseColour);
        $jsonColour = $jsond->equipment->options;
        //dd($jsonColour);

        /*Get all prices for same car by same dealer for customise deal*/
        if(strtoupper($database) == 'CARS') {
            $customiseDeal = Pricing::where('cap_id', '=', $single_quote->cap_id)
                ->Where('deal_id', '=', $single_quote->deal_id)
                ->Where('finance_type', '=', $single_quote->finance_type)
                ->get();
        }else{
            $customiseDeal = DB::table('van_pricestable')
                ->where('cap_id', '=', $single_quote->cap_id)
                ->Where('deal_id', '=', $single_quote->deal_id)
                ->Where('finance_type', '=', $single_quote->finance_type)
                ->get();
        }

        // Set default options
        $origDeposit = [1, 3, 6, 9, 12];
        $origMileage = [5000, 6000, 8000, 10000, 12000, 15000, 20000, 25000, 30000];
        $origTerm = [12,24, 36, 48];

        $depositValues = [];
        $term = [];
        $mileages = [];

        // build lease customisation options
        foreach($customiseDeal as $customOptions) {
            if(!in_array($customOptions->deposit_value, $depositValues)) {
                $depositValues[] = $customOptions->deposit_value;
            }
            if(!in_array($customOptions->annual_mileage, $mileages)) {
                $mileages[] = $customOptions->annual_mileage;
            }
            if(!in_array($customOptions->term, $term)) {
                $term[] = $customOptions->term;
            }
        }

        foreach($origDeposit as $deposit) {
            if(in_array($deposit, $depositValues)) {
                $depositVal[$deposit] = true;
            }else{
                $depositVal[$deposit] = false;
            }
        }

        foreach($origMileage as $mileage) {
            if(in_array($mileage, $mileages)) {
                $mileageVal[$mileage] = true;
            }else{
                $mileageVal[$mileage] = false;
            }
        }

        foreach($origTerm as $termLength) {
            if(in_array($termLength, $term)) {
                $termVal[$termLength] = true;
            }else{
                $termVal[$termLength] = false;
            }
        }

        /*Get Dealer Info*/
        $dealerInfo = user::where('name', '=', $single_quote->deal_id)
        ->first();
        $dealerInfoJSON = json_decode($dealerInfo);

        /* Get Trustpilot */
        $trustpilotRatings1 = $dealerInfoJSON->trustpilot;
        $trustpilotRatings = json_decode($trustpilotRatings1);

        // opening times
        $openingTimes = $this->getOpeningTimes($single_quote->deal_id);

        $carsToShow = array();

	    if($showRelated == 2) {
            $relatedCars = $this->getRelatedDeals($single_quote, $carsToShow, 0);
        }

        if(isset($relatedCars)) {
            foreach($relatedCars as $car) {

                if(strtoupper($database)=='CARS') {
                    $relatedToShow[] = DB::table('pricestable')
                        ->join('capreference', 'pricestable.cap_id', '=', 'capreference.cap_id')
                        ->select('pricestable.*', 'capreference.*')
                        ->where('id', '=', $car->id)
                        ->first();
                }else{
                    $relatedToShow[] = DB::table('van_pricestable')
                        ->join('capreference', 'van_pricestable.cap_id', '=', 'capreference.cap_id')
                        ->select('van_pricestable.*', 'capreference.*')
                        ->where('id', '=', $car->id)
                        ->first();
                }

            }
            $RelatedModelQuery = $relatedToShow;
        }else{
            $RelatedModelQuery = false;
        }

        // Grab Optional extras
        $optionalExtras = [];
        $standardEquip = [];

        $optionalExtrasCap = DB::table('cap_id_optional_equipment')
            ->where('cap_id', '=', $single_quote->cap_id)
            ->where('database', '=', $database)
            ->first();

        if($optionalExtrasCap) {
            $assignedExtras = json_decode($optionalExtrasCap->ce_ids);

            $optionalExtrasEquipment = DB::table('cap_optional_equipment')
                ->whereIn('OptionCode', $assignedExtras)
                ->where('cap_id', '=', $single_quote->cap_id)
                ->where('database', '=', $database)
                ->whereNotIn('OptionCode', array(0, 1, -1))
                ->get();

            foreach ($optionalExtrasEquipment as $oee) {
                $optionalExtras[$oee->Dc_Description][] = array(
                    'description' => $oee->Do_Description,
                    'price' => $oee->price
                );
            }

            // Grab standard equipment assigned to the CAP id
            $standardEquip = [];
            $assignedStandardEquipment = [];
            $standardEquipmentCap = DB::table('cap_id_equipment')
                ->where('cap_id', '=', $single_quote->cap_id)
                ->where('database', '=', $database)
                ->first();
            if($standardEquipmentCap) {
                $assignedStandardEquipment = json_decode($standardEquipmentCap->ce_ids);
            }

            // Grab the standard equipment description etc
            $standardEquipment = DB::table('cap_equipment')
                ->whereIn('OptionCode', $assignedStandardEquipment)
                ->get();
            foreach ($standardEquipment as $oee) {
                $standardEquip[$oee->Dc_Description][] = $oee->Do_Description;
            }
        }

        // Get reviews from Car & Driving
        $reviews = DB::table('cap_reviews')
            ->select('*')
            ->where('cap_id', '=', $single_quote->cap_id)
            ->where('database', '=', $database)
            ->get();

        // Manufacturer links to display on technical info modal
        $manufacturerLinks = $this->getManufacturerLink($single_quote->maker);

        // Customer phone number check
        $phone_number = '';

        if (Auth::check()) {
            $phone_number = \Auth::user()->phone;
        }elseif(Cookie::get('phone_number') != null && !Auth::check()) {
            $phone_number = Cookie::get('phone_number');
        }

        $vars = compact('single_quote',
            'database',
            'json_output1',
            'jsonColour',
            'customiseDeal',
            'dealerInfoJSON',
            'openingTimes',
            'trustpilotRatings',
            'RelatedModelQuery',
            'dealerInfo',
            'capImages',
            'reviews',
            'depositVal',
            'mileageVal',
            'termVal',
            'optionalExtras',
            'standardEquip',
            'techData',
            'vehicleRating',
            'manufacturerLinks',
            'phone_number',
            'uid'
        );

        return View('quote_views.show-quote', $vars);

    }

    /* *
     * Form Submission For Quote
     * */

    public function store(Request $request)
    {
        $storeTheseCookies = [
            'name',
            'email',
            'phone_number',
            'contact_preference',
            'comment',
            'subject'
        ];

        foreach ($request->except('_token') as $key => $value) {
            if(in_array($key, $storeTheseCookies)) {
                Cookie::queue(Cookie::make( $key, $value, self::HOUR ));
            }else{

            }
        }

        $fail = 0;
        // Basic spam protection
        if(isset($request['comment'])) {
            if(!is_null($request['comment'])) {
                $fail = 1;
            }
        }
        if(isset($request['subject'])) {
            if(!is_null($request['subject'])) {
                $fail = 1;
            }
        }

        if(isset($request['database']) && $request['database']=='LCV') {
            $pricing = DB::table('van_pricestable')
                ->where('id', '=', $request['model_id'])
                ->first();

            $database = "LCV";
        }else{
            $database = "CARS";
            $pricing = Pricing::where('id', $request['model_id']) -> first();
        }

        if($fail == 1) {
            return redirect('/quote/enquiry/' . $request['prices_id'] . '/' . $database . '?enqErr=1');
        }

        if($request->maintenance && $request->maintenance == 'on') {
            $submittedMessage = $request['message'] . "
            Please quote for maintenance package.";
        }else{
            $submittedMessage = $request['message'];
        }

        $quote = new Quote;
        $quote->cap_id= $pricing->cap_id;
        $quote->model_id= $request['model_id'];
        $quote->make= $request['make'];
        $quote->model= $request['model'];
        $quote->deal_id = $request['deal_id'];
        $quote->transmission= $request['transmission'];
        $quote->fuel_type= $request['fuel_type'];
        $quote->prices_id= $request['prices_id'];
        $quote->price= $request['price'];
        $quote->profile= $request['profile'];
        $quote->finance_type= $request['finance_type'];
        $quote->document_fee= $request['document_fee'];
        $quote->name= $request['name'];
        $quote->email= $request['email'];
        $quote->phone_number = str_replace(' ', '', $request['phone_number']);
        $quote->enquiry= $submittedMessage;
        $quote->contact_preference= $request['contact_preference'];
        $quote->status= '6';
        $quote->markassold= '0';
        $quote->withdraw_enquiry= '';
        $quote->hotdeals = $request['hotdeals'];
        $quote->database = $database;
        $quote->business_name = $request['businessName'];
        $quote->business_type = $request['businessType'];

        // User check to see if a user is in the DB or not. If they are then display a login message
        // on the enquiry email, if not display a register message
        $userCheck = DB::table('users')
            ->where('email', '=', $request['email'])
            ->first();

        if ($userCheck) {
            $quote->created_by= $userCheck->id;
        }else{
            $quote->created_by = 0;
        }

        $quote->created_at= now();
        $quote->save();

        // Grab dealer information to display in emails
        $dealer = User::where('name',$quote->deal_id) -> first();
        // grab the vehicle information to display in emails
        $cap = Cap::where('cap_id', $pricing->cap_id)
            ->where('database', '=', $database)
            -> first();

        $imageId = $cap->image_id;

        $cdnUrl = env('DO_IMAGE_PATH') . $database . '/';
        if($cap->imageid >= 1) {
            $vehicleImage = $this->fetchVehicleDefaultImage($cap->cap_id, $database);
            $imageUrl = $cdnUrl . 'big/' . $vehicleImage->image_name;
            if (@getimagesize($imageUrl)) {
                $cap->imageid = $imageUrl;
            }else{
                $cap->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
            }
        }else{
            $cap->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
        }

        $image = $cap->imageid;

        $contactPreferences = $this->getContactPreferences($quote->contact_preference);

        // Check for webhooks and fire off requests if so.
        $this->checkWebhooks($quote);


//
//        // Email the user
        $email = $quote->email;
        $contact_company = 'Auto Lease Compare';
        $message = '';
        $subject = 'Auto Lease Compare - Enquiry Sent to dealer';
        // Moving TrustPilot invites to the "quote sold" email
//        $trustpilotReviewEmail = "www.autoleasecompare.com+9b863e5159@invite.trustpilot.com";
        Mail::send(array('html' => 'emails.enquiry-customer'), array('quote' => $quote,'model' => $quote, 'dealer' => $dealer, 'pricing' => $pricing, 'cap' => $cap, 'image' => $image), function($message) use ($email, $subject, $contact_company,$quote)
        {
            $message->to($email, $quote->name)->subject($subject);
//            $message->bcc($trustpilotReviewEmail);
        });

        // Email the dealer
//        $email = 'scott@profiledigitalagency.co.uk';
        $email = $dealer->email;
        $contact_company = 'Auto Lease Compare';
        $message = '';
        $date = date('d-m-Y H:i:s');



        $subject = 'Auto Lease Compare - Enquiry - ' . $date;
        Mail::send(array('html' => 'emails.enquiry'), array('quote' => $quote,'model' => $quote, 'dealer' => $dealer, 'pricing' => $pricing, 'cap' => $cap, 'contact_preference' => $contactPreferences), function($message) use ($email, $subject, $contact_company,$quote)
        {
            $message->to($email, $contact_company)->subject($subject);
            // Millhouse have an issue internally where they cant receive emails from us
            // As a work around, when millhouse is live, we send the emails to a gmail account
            // The gmail account forwards the emails onto millhouse and just acts as a middle-man
//            if($quote->deal_id == 'Millhouse Leasing') {
//                $message->bcc('millhouseleasing.alc@gmail.com');
//            }
        });

        return redirect('/thankyou?id=' . $quote->id);
    }

    public function thankyou(Request $request)
    {
        $data = $request->all();
        $capImages = array();
        $capImage = '';

        $quote = DB::table('quote')
            ->when(isset($data['id']), function ($query) use ($data) {
                return $query->where('id', '=', $data['id']);
            })
            ->first();

        if($quote->database == 'LCV') {
            $pricesTable = 'van_pricestable';
        }else{
            $pricesTable = 'pricestable';
        }
        $confirm_quote = ($quote);

        // Only display the thank you page if the quote date is today
        if(date('Ymd') == date('Ymd', strtotime($quote->created_at))) {

            $single_quote = DB::table($pricesTable)
                ->join('capreference', $pricesTable . '.cap_id', '=', 'capreference.cap_id')
                ->select($pricesTable . '.*','capreference.*')
                ->where('id', '=', $quote->prices_id)
                ->where('capreference.database','=',$quote->database)
//                ->when(isset($data['id']), function ($query) use ($data) {
//                    return $query->where('id', '=', $data['id']);
//                })
                ->first();

            if(count($capImages) == 0) {
                for ($i = 1; $i <= 6; $i++) {

                    $image = '/images/capimages/' . $single_quote->imageid . '_' . $i . '.JPG';
                    if(file_exists($_SERVER["DOCUMENT_ROOT"] . $image)) {
                        $capImages[] = $image;
                    }
                }

                // Add image that doesn't have _NUM at the end
                if(file_exists($_SERVER["DOCUMENT_ROOT"] . '/' . '/images/capimages/' . $single_quote->imageid . '.JPG')) {
                    $capImages[] = '/images/capimages/' . $single_quote->imageid . '.JPG';
                }
            }

            if(count($capImages) > 0) {
                $capImage = $capImages[0];
            }

            $user = DB::table('users')
                ->where('email', '=', $confirm_quote->email)
                ->first();
            if($user) {
                $userAvail = 1;
            }else{
                $userAvail = 0;
            }

            $vars = compact('confirm_quote', 'userAvail', 'single_quote', 'capImage');

            return View('quote_views.thanks', $vars);
        }else{
            return redirect('/');
        }
    }

    public function delete_quote(Request $request)
    {
        if (Auth::user()->hasRole(['admin'])) {
            $pricestable = 'pricestable';
            if ($request['database'] == 'LCV') {
                $pricestable = 'van_pricestable';
            }

            $priceId = $request['id'];

            $deal = DB::table($pricestable)
                ->where('id', '=', $priceId)
                ->first();

            $delete = DB::table($pricestable)
                ->where('deal_id', '=', $deal->deal_id)
                ->where('cap_id', '=', $deal->cap_id)
                ->delete();

            if ($delete) {
                return redirect('/quote/enquiry/404?del=1');
            }
        }else{
            return redirect('/quote/enquiry/' . $priceId . '/' . $request['database']);
        }

    }

    /**
     * @description admin method to update deal and set featured flag so deals appear on homepage hot deals section
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function feature_quote(Request $request)
    {
        if (Auth::user()->hasRole(['admin'])) {
            $pricestable = 'pricestable';
            if ($request['database'] == 'LCV') {
                $pricestable = 'van_pricestable';
            }

            $priceId = $request['id'];

            $deal = DB::table($pricestable)
                ->where('id', '=', $priceId)
                ->first();

            if($deal->featured==1) {
                $featured = 0;
            }else{
                $featured = 1;
            }

            $update = DB::table($pricestable)->where('id', $priceId)->update(['featured' => $featured]);
            if ($update) {
                return redirect('/quote/enquiry/' . $request['id'] . '/' . $request['database'] . '/?hot=1');
            }
        }else{
            return redirect('/quote/enquiry/' . $request['id'] . '/' . $request['database']);
        }

    }

    public function tracking(Request $request) {
        $priceId = $request['priceId'];
        $database = $request['database'];
        $type = $request['type'];

        // log DB
        $check = DB::table('tracking')
            ->where('priceId', '=', $priceId)
            ->where('database', '=', $database)
            ->where('type', '=', $type)
            ->wheredate('date', Carbon::today())
            ->first();

        if(!$check) {
            // Insert
            DB::table('tracking')->insert(array(
                'priceId'=>$priceId,
                'database'=>$database,
                'type'=>$type,
                'clickCount'=>1,
                'date'=>now()
            ));
        }else{
            DB::table('tracking')
                ->where('id', '=', $check->id)
                ->update(array(
                'priceId'=>$priceId,
                'database'=>$database,
                'type'=>$type,
                'clickCount'=>$check->clickCount + 1,
            ));
        }
    }

    private function getContactPreferences($contactPref)
    {

        $contactPreferences = array(
            1 => 'Call me back',
            2 => 'Call me back (Morning)',
            3 => 'Call me back (Afternoon)',
            4 => 'Email me back',
            5 => 'No Contact Preference',
        );

        return $contactPreferences[$contactPref];

    }

    public function viewPriceHistory($priceid, $mileage, $term, $deposit_value, $finance_type)
    {
        $debug = env('APP_DEBUG');

        if (!request()->ajax()) {
            if(!$debug) {
//                return redirect('/');
            }
        }

        $car = $priceid;
        $prices = DB::table('pricestable')
            ->where('id', $priceid)
            ->get();

        if(count($prices)==0) {
            $prices = DB::table('van_pricestable')
                ->where('id', $priceid)
                ->get();
        }

        $hide = 0;
        $cars = [];

        foreach ($prices as $res) {
            $cap = DB::table('capreference')
                ->where('cap_id', '=', $res->cap_id)
                ->get();

            foreach ($cap as $gar) {

                // price history query
                $priceHistory =PriceHistory::where('cap_id', '=', $res->cap_id)
                    ->Where('annual_mileage', '=', $mileage)
                    ->Where('finance_type', '=', $finance_type)
                    ->Where('term', '=', $term)
                    ->Where('deposit_value', '=', $deposit_value)
                    ->Whereraw('DATE_FORMAT(updated_at, "%Y-%m-%d") > "'.date('Y-m-d', strtotime("-4 weeks")).'"')
                    ->orderBy('updated_at', 'asc')
                    ->get();

                $priceHistory2 =PriceHistory::where('cap_id', '=', $res->cap_id)
                    ->Where('annual_mileage', '=', $mileage)
                    ->Where('finance_type', '=', $finance_type)
                    ->Where('term', '=', $term)
                    ->Where('deposit_value', '=', $deposit_value)
                    ->Whereraw('DATE_FORMAT(updated_at, "%Y-%m-%d") > "'.date('Y-m-d', strtotime("-3 months")).'"')
                    ->orderBy('updated_at', 'asc')
                    ->get();

                if(count($priceHistory) < 4) {
                    $hide = 1;
                }
                foreach($priceHistory as $hist) {

                    if(isset($priceHigh)) {

                        if($hist->monthly_payment > $priceHigh) {
                            $priceHigh = $hist->monthly_payment;
                        }
                    }else{
                        $priceHigh = $hist->monthly_payment;
                    }

                    if(isset($priceLow)) {

                        if($hist->monthly_payment < $priceLow) {
                            $priceLow = $hist->monthly_payment;
                        }
                    }else{
                        $priceLow = $hist->monthly_payment;
                    }

                }

                if(!isset($priceLow) && !isset($priceHigh)) {
                    $hide = 1;
                }else{

                    $cars[$car]['id'] = $car;
                    $cars[$car]['finance_type'] = $res->finance_type;
                    $cars[$car]['maker'] = $gar->maker;
                    $cars[$car]['range'] = $gar->range;
                    $cars[$car]['model'] = $gar->model;
                    $cars[$car]['cap_id'] = $res->cap_id;
                    $cars[$car]['deal_id'] = $res->deal_id;
                    $cars[$car]['deposit_value'] = $res->deposit_value;
                    $cars[$car]['term'] = $res->term;
                    $cars[$car]['monthly_payment'] = $res->monthly_payment;
                    $cars[$car]['annual_mileage'] = $res->annual_mileage;
                    $cars[$car]['in_stock'] = $res->in_stock;
                    $cars[$car]['document_fee'] = $res->document_fee;
                    $cars[$car]['expires_at'] = $res->expires_at;
                    $cars[$car]['referenceTrim'] = $gar->trim;
                    $cars[$car]['co2'] = $gar->co2;
                    $cars[$car]['referenceFuel'] = $gar->fuel;
                    $cars[$car]['mpgcombined'] = $gar->mpgcombined;
                    $cars[$car]['referenceTransmission'] = $gar->transmission;
                    $cars[$car]['referenceBodystyle'] = $gar->bodystyle;
                    $cars[$car]['referenceDerivative'] = $gar->derivative;
                    $cars[$car]['deposit_months'] = $res->deposit_months;
                    $cars[$car]['imageid'] = $gar->imageid;
                    $cars[$car]['priceHigh'] = $priceHigh;
                    $cars[$car]['priceLow'] = $priceLow;
                    $cars[$car]['priceHistory'] = $priceHistory;
                    $cars[$car]['priceHistoryMonthly'] = $priceHistory2;
                }
            }
        }



        if(isset($cars[$priceid]['priceHistoryMonthly'])) {
            $histCount = count($cars[$priceid]['priceHistoryMonthly']);

            if ($histCount > 28) {
                $showWeekly = 1;
            } else {
                $showWeekly = 0;
            }

            if ($histCount > 84) {
                $showMonthly = 1;
            } else {
                $showMonthly = 0;
            }
        }else{
            $showWeekly = 0;
            $showMonthly = 0;
        }

        return View('quote_views.price-history' , compact('cars', 'hide', 'showWeekly', 'showMonthly'));

    }


    public function viewGaragePriceHistory(Request $request) {
//        $debug = env('APP_DEBUG');
//
//        if (!request()->ajax()) {
//            if(!$debug) {
//                return redirect('/');
//            }
//        }

        $data = $request->all();
        $cars = array();
        $hide = 0;
        foreach($data['car'] as $car) {

            $priceLow = 9999;
            $priceHigh = 0;

            $carDetails = explode('-', $car);
            $car = $carDetails[0];
            $database = $carDetails[5];

            if($database == 'CARS') {
                $pricestable = 'pricestable';
                $prisesHistoryTable = 'priceshistory_cars';
                $prices = DB::table('pricestable')
                    ->where('id', $carDetails[0])
                    ->get();
            }elseif($database == 'LCV') {
                $pricestable = 'van_pricestable';
                $prisesHistoryTable = 'priceshistory_vans';
                $prices = DB::table('van_pricestable')
                    ->where('id', $carDetails[0])
                    ->get();
            }

            foreach ($prices as $res) {
                $cap = DB::table('capreference')
                    ->where('cap_id', '=', $res->cap_id)
                    ->where('database', '=', $database)
                    ->get();

                foreach ($cap as $gar) {

                    // last 5 months worth of data so we only have to do 1 query
                    $priceHistory =DB::table($prisesHistoryTable)->where('cap_id', '=', $res->cap_id)
                        ->Where('annual_mileage', '=', $carDetails[1])
                        ->Where('term', '=', $carDetails[2])
                        ->Where('deposit_value', '=', $carDetails[3])
                        ->Where('finance_type', '=', $carDetails[4])
                        ->Whereraw('DATE_FORMAT(updated_at, "%Y-%m-%d") > "'.date('Y-m-d', strtotime("-4 weeks")).'"')
                        ->orderBy('updated_at', 'asc')
                        ->get();


                    foreach($priceHistory as $hist) {

                        if(isset($priceHigh)) {

                            if($hist->monthly_payment > $priceHigh) {
                                $priceHigh = $hist->monthly_payment;
                            }
                        }else{
                            $priceHigh = $hist->monthly_payment;
                        }

                        if(isset($priceLow)) {

                            if($hist->monthly_payment < $priceLow) {
                                $priceLow = $hist->monthly_payment;
                            }
                        }else{
                            $priceLow = $hist->monthly_payment;
                        }

                    }

                    $cars[$car]['id'] = $car;
                    $cars[$car]['finance_type'] = $res->finance_type;
                    $cars[$car]['maker'] = $gar->maker;
                    $cars[$car]['range'] = $gar->range;
                    $cars[$car]['model'] = $gar->model;
                    $cars[$car]['cap_id'] = $res->cap_id;
                    $cars[$car]['deal_id'] = $res->deal_id;
                    $cars[$car]['deposit_value'] = $res->deposit_value;
                    $cars[$car]['term'] = $res->term;
                    $cars[$car]['monthly_payment'] = $res->monthly_payment;
                    $cars[$car]['annual_mileage'] = $res->annual_mileage;
                    $cars[$car]['in_stock'] = $res->in_stock;
                    $cars[$car]['document_fee'] = $res->document_fee;
                    $cars[$car]['expires_at'] = $res->expires_at;
                    $cars[$car]['referenceTrim'] = $gar->trim;
                    $cars[$car]['co2'] = $gar->co2;
                    $cars[$car]['referenceFuel'] = $gar->fuel;
                    $cars[$car]['mpgcombined'] = $gar->mpgcombined;
                    $cars[$car]['referenceTransmission'] = $gar->transmission;
                    $cars[$car]['referenceBodystyle'] = $gar->bodystyle;
                    $cars[$car]['referenceDerivative'] = $gar->derivative;
                    $cars[$car]['deposit_months'] = $res->deposit_months;
                    $cars[$car]['imageid'] = $gar->imageid;
                    $cars[$car]['priceHigh'] = $priceHigh;
                    $cars[$car]['priceLow'] = $priceLow;
                    $cars[$car]['priceHistory'] = $priceHistory;
                }
            }
        }

        return View('quote_views.price-history-garage' , compact('cars'));

    }

    public function viewDeal404() {
        return response()->view('quote_views.not-found', [], 404);
    }

    public function viewActivityLog()
    {

        $activity = DB::table('activity_log')
            ->paginate(50);

        $vars = compact(
            'activity'
        );
        return View('pages.activity-log', $vars);

    }

    private function getRelatedDeals($single_quote) {

        $excludedManufacturers = explode(',', env('EXCLUDED_MANUFACTURERS'));
        $excludedDealers = explode(',', env('EXCLUDED_DEALERS'));
        $counter = 0;

        $database = $single_quote->database;
        if($database == 'LCV') {
            $pricestable = 'van_pricestable';
        }else{
            $pricestable = 'pricestable';
        }

        $RelatedModelQuery = DB::table($pricestable)
            ->join('capreference', $pricestable.'.cap_id', '=', 'capreference.cap_id')
            ->select('*')
            ->where('finance_type', '=', $single_quote->finance_type)
            ->where('model', '!=', "$single_quote->model")
            ->whereNotIn('maker', $excludedManufacturers)
            ->WhereNotIn('deal_id',$excludedDealers)
            ->whereBetween('monthly_payment', [$single_quote->monthly_payment - (($single_quote->monthly_payment / 100) * 40), $single_quote->monthly_payment + (($single_quote->monthly_payment / 100) * 40)])
            ->where('bodystyle', '=', $single_quote->bodystyle)
            ->where('fuel', '=', $single_quote->fuel)
            ->where('transmission', '=', $single_quote->transmission)
            ->inRandomOrder()
            ->limit(2000)
            ->get();


        $carsToShow = array();

        foreach($RelatedModelQuery as $result) {
            if($counter < 7) {
                if(array_search($result->range, array_column($carsToShow, 'range')) === false) {
                    $carsToShow[] = $result;
                    $counter ++;
                }
            }

        }

        return $carsToShow;
    }



/**
     * @param $pricesId
     * @return mixed
     */
    private function findDealDatabase($pricesId) {

        $prices = DB::table('pricestable')
            ->where('id', $pricesId)
            ->get();

        if(count($prices)==0) {
            $prices = DB::table('van_pricestable')
                ->where('id', $pricesId)
                ->get();

            if(count($prices) > 0) {
                $database = 'lcv';
            }

        }else{
            $database = 'cars';
        }

        if(isset($database)) {
            return $database;
        }else{
            return false;
        }
    }

    /**
     * @param $deal_id
     * @return array|false
     */
    private function getOpeningTimes($deal_id) {

        $dowMap = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');

        if(!isset($deal_id)) {
            return false;
        }

        $times = DB::table('dealer_opening_times')
            ->where('deal_id', $deal_id)
            ->orderby('day_id')
            ->get();

        if(!count($times)>0) {
            return false;
        }

        $storeSchedule = [];
        $opening = [];
        foreach($times as $open) {

            $openTime = date("H:i", strtotime($open->open));
            $closeTime = date("H:i", strtotime($open->close));

            $storeSchedule[$dowMap[($open->day_id - 1)]][] = array(
                $openTime, $closeTime
            );

        }


        $timestamp = time();
        $open = false;
        $openAt = false;
        $currentTime = (new DateTime())->setTimestamp($timestamp);
        $currentDay = date('D', $timestamp);

        if(isset($storeSchedule[$currentDay])){

            // loop through time ranges for current day
            foreach ($storeSchedule[$currentDay] as $key => $dateGroup) {
                $startTime = $dateGroup[0];
                $endTime = $dateGroup[1];

                // create time objects from start/end times
                $startTime = DateTime::createFromFormat('H:i', $startTime);
                $endTime   = DateTime::createFromFormat('H:i', $endTime);

                // check if current time is within a range
                if (($startTime < $currentTime) && ($currentTime < $endTime)) {
                    $open = true;
                    break;
                }elseif($currentTime < $startTime){
                    // Opening Later
                    $openAt = $startTime;
                }
            }
        }

        // if open
        if($open){
            return array(
                'status'=>1,
                'schedule'=>$storeSchedule
            );
        }else{
            // If closed but open again today
            if($openAt){
                $status = 2;
                $time = $openAt->format('H:i');

                return array(
                    'status' => $status,
                    'time' => $time,
                    'schedule'=>$storeSchedule
                );

            }else{
                // if closed today grab next open day

                $iteration = 0;
                $checked = [];
                // loop through opening times and grab next day data
                foreach ($storeSchedule as $key => $dateGroup) {
                    $iteration ++;

                    if($iteration < date("N")) {
                        continue;
                    }
                    $checked[] = $key;

                    $nextOpenDay = $key;
                    foreach($dateGroup as $times) {

                        $nextOpenTime = $times[0];
                    }

                    break;
                }

                // Looped through the week so go back to monday as all dealers are open monday
                if(!isset($nextOpenTime) && !isset($nextOpenDay)) {
                    return false;
                }
                // return
                return array(
                    'status'=>3,
                    'day'=>date("l", strtotime($nextOpenDay)),
                    'time'=>$nextOpenTime,
                    'schedule'=>$storeSchedule
                );
            }
        }
    }

    /**
     * @param $quote
     * @return false
     */
    private function checkWebhooks($quote)
    {

        $dealer = DB::table('dealer_webhook')
            ->where('deal_id', '=', $quote->deal_id)
            ->first();

        $pricestable = 'pricestable';
        if($quote->database == 'LCV') {
            $pricestable = 'van_pricestable';
        }

        $deal = DB::table($pricestable)
            ->where('id', '=', $quote->prices_id)
            ->first();

        $cap = DB::table('capreference')
            ->where('cap_id', '=', $deal->cap_id)
            ->where('database', '=', $quote->database)
            ->first();


        if(!$dealer) {
            return false;
        }
        if(!$deal) {
            return false;
        }

        if($dealer->enabled != 1) {
            return false;
        }

        if(isset($dealer->token)) {
            $token = $dealer->token;
        }else {
            $token = base64_encode("ALC" . $deal->deal_id);
        }

        $name = trim($quote->name);
        $surname = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $firstname = trim( preg_replace('#'.preg_quote($surname,'#').'#', '', $name ) );

        $email = $quote->email;
        $phone = $quote->phone_number;
        $cap_id = $deal->cap_id;
        $deal_id = $quote->prices_id;
        $database = $quote->database;
        $annual_mileage = $deal->annual_mileage;
        $term = $deal->term;
        $monthly_payment = $quote->price;
        $document_fee = $quote->document_fee;
        $deposit_value = $deal->deposit_value;
        $message = $quote->enquiry;
        $profile = $quote->profile;
        $maker = $cap->maker;
        $model = $quote->model;
        $transmission = $quote->transmission;
        $fuel_type = $quote->fuel_type;
        $finance_type = $quote->finance_type;
        $in_stock = ($quote->in_stock == 1) ? "In Stock" : "Factory Order";
        $business_name = $quote->business_name;
        $business_type = $quote->business_type;
        $details = '';
        $package = [];

        $fields = json_decode($dealer->data);

        foreach($fields as $field) {
            $package[$field] = $$field;
            $details .= $field . ": " . $$field . "\n\r ";
        }
        $package['quote_id'] = $quote->id;
        $package['details'] = $details;
        $package['token'] = $token;

        // dealer url
        $url = $dealer->endpoint;
        // ALC test url
        $url2 = "https://17b08902-63ee-4a03-b0c8-69f320fb273e.mock.pstmn.io";

        // Broker specific stuff
        switch ($quote->deal_id) {
            // === RIVERVALE ===
            case "Rivervale":
            case "Car Leasing Made Simple":
            case "Vanarama":
                $package = $this->getDataPackage($package);
                $options = array(
                    'http' => array(
                        'header'=>  "Content-Type: application/json\r\n" . "Accept: application/json\r\n" . "Api-Key: " . $token,
                        'method'  => 'POST',
                        'content' => json_encode($package)
                    ),
                );
            break;
            case 'Carparison':

                $package = $this->getDataPackage($package);

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $package,
                    CURLOPT_HTTPHEADER => array(
                        'X-API-KEY: ' . $token
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);

                return $response;
            break;
            case "Car Leasing-Online":

                $package = $this->getDataPackage($package);

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $package,
                    CURLOPT_HTTPHEADER => array(
                        'X-API-KEY: K89G075DF6B20D88QWBD5BF74C45C41BE6BCV12h'
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);

                return $response;

            break;
            case "Millhouse Leasing":
                $dealer->endpoint = str_replace('{id}', 'ALC' . $quote->id, $dealer->endpoint);
                $package = $this->getDataPackage($package);
                $package['brand'] = 'Just Drive leasing';
                $package['opportunity_source'] = 'Auto Lease Compare';
                $package['opportunity_status'] = 'New Opportunity';
                $package['opportunity_type'] = 'Single Car';
                $package['quality'] = 'Medium';
                $options = array(
                    'http' => array(
                        'header'=>  "Content-Type: application/json\r\n" . "Accept: application/json\r\n" . "Api-Key: " . $token,
                        'method'  => 'POST',
                        'content' => json_encode($package)
                    ),
                );

            break;

            // === DEFAULT ===
            default:
                $options = array(
                    'http' => array(
                        'header'=>  "Content-Type: application/json\r\n" . "Accept: application/json\r\n",
                        'method'  => 'POST',
                        'content' => json_encode($package),
                    ),
                );
            break;

        }

        // go go go
        $context  = stream_context_create($options);

        // alc testing postman url
//        $result2 = file_get_contents($url2, false, $context);

        // Dealer post
        $result = file_get_contents($url, false, $context);

    }

    /**
     * @param $package
     * @return mixed
     */
    private function getDataPackage($package)
    {
        // This is in here as a debugging thing while rolling out to all brokers
        // Pointless method after rollout and will get deleted
//        print_r(json_encode($package));
        return $package;
    }

    /**
     * @param null $cap_id
     * @param null $database
     * @return bool
     */
    private function checkTechData($cap_id = null, $database = null) {
        if(is_null($cap_id) || is_null($database)) {
            return false;
        }

        $dateToCheck = now();

        $check = DB::table('cap_tech_data')
            ->where('cap_id', '=', $cap_id)
            ->where('database', '=', $database)
            ->whereDate('date', '>=', Carbon::now()->subDays(7))
            ->first();
        if($check) {
            return $check;
        }
        return false;

    }

    /**
     * @param null $cap_id
     * @param null $database
     * @return boolean
     * @description Method to check the optional extras for a given cap id / DB.
     * If date is older than $dateToCheck then re-fetch the data from CAP and store in DB
     */
    private function checkOptionsDB($cap_id = null, $database = null) {

        if(is_null($cap_id) || is_null($database)) {
            return false;
        }

        $dateToCheck = now();

        $check = DB::table('cap_id_optional_equipment')
            ->where('cap_id', '=', $cap_id)
            ->where('database', '=', $database)
            ->whereDate('date', '>=', Carbon::now()->subDays(7))
            ->first();

        if($check) {
            return true;
        }
        return false;

    }

    /**
     * @param $capid
     * @param $database
     */
    private function getTechnicalData($cap_id, $database) {

        if(!$cap_id) {
            return false;
        }
        $techDataList = 'seats,cc,enginepower_bhp,topspeed,0TO62,mpg_combined,mpg_urban,mpg_extraurban,length,width,height,fueltankcapacity,insurancegroup1-50,standardmanwarranty_mileage,standardmanwarranty_years';
        if($database == 'LCV') {
            $techDataList = 'seats,cc,enginepower_bhp,topspeed,0TO62,mpg_combined,mpg_urban,mpg_extraurban,length,width,height,fueltankcapacity,insurancegroup1,standardmanwarranty_mileage,standardmanwarranty_years';
        }
        $args = array(
            'subscriberId' => $this->subscriberId,
            'password' => $this->password,
            'database' => $database,
            'capidList' => $cap_id,
            'specDateList' => date('Y-m-d'),
            'techDataList' => $techDataList,
            'returnVehicleDescription' => true,
            'returnCaPcodeTechnicalItems' => true,
            'returnCostNew' => true,
        );

        $wsdl = $this->nvdWsdl;
        $client = new SoapClient($wsdl, array('soap_version' => SOAP_1_1, 'trace' => true, 'keep_alive' => false));

        try {
            $result = $client->GetBulkTechnicalData($args);
            $result = $result->GetBulkTechnicalDataResult->Returned_DataSet->any;

        } catch (SoapFault $e) {
            return false;
        }

        $xml = simplexml_load_string($result);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);

        $techData = array_map('trim', $array['TechData']['Tech_Table']);

        // first remove existing extras
        DB::table('cap_tech_data')
            ->where('cap_id', '=', $cap_id)
            ->where('database', '=', $database)
            ->delete();

        // insert updated
        DB::table('cap_tech_data')->insert(
            array(
                'cc' => $techData['cc'] ?? '',
                'enginepower_bhp' => $techData['enginepower_bhp'] ?? '',
                'topspeed' => $techData['topspeed'] ?? '',
                '0to62' => $techData['OTO62'] ?? '',
                'mpg_urban' => $techData['mpg_urban'] ?? '',
                'length' => $techData['length'] ?? '',
                'width' => $techData['width'] ?? '',
                'height' => $techData['height'] ?? '',
                'fueltankcapacity' => $techData['fueltankcapacity'] ?? '',
                'insurancegroup1' => $techData['insurancegroup150'] ?? '',
                'standardmanwarranty_mileage' => $techData['standardmanwarranty_mileage'] ?? '',
                'standardmanwarranty_years' => $techData['standardmanwarranty_years'] ?? '',
                'cap_id' => $cap_id,
                'database' => $database,
                'date' => now()
            )
        );
        // update capreference table with the fields in there
        // should probably be moved into this table at some point
        DB::table('capreference')
            ->where('cap_id', '=', $cap_id)
            ->where('database', '=', $database)
            ->update(
                array(
                    'seats'=>$techData['seats'] ?? '',
                    'mpgextraurban'=>$techData['mpg_extraurban'] ?? '',
                    'mpgcombined'=>$techData['mpg_combined'] ?? '',
                )
            );

        return true;

    }

    /**
     * @param false $cap_id
     * @param $database
     * @return false|mixed
     * @throws \SoapFault
     */
    private function fetchOptionalExtras($cap_id = false, $database) {
        if(!$cap_id) {
            return false;
        }

        $args = array(
            'subscriberId' => $this->subscriberId,
            'password' => $this->password,
            'database' => $database,
            'capid' => $cap_id,
            'optionDate' => date('Y-m-d'),
            'justCurrent' => true,
            'descriptionRs' => true,
            'optionsRs' => true,
            'relationshipsRs' => true,
            'packRs' => true,
            'technicalRs' => true,
        );


        $wsdl = $this->nvdWsdl;
        try {
            $client = new SoapClient($wsdl, array('soap_version' => SOAP_1_1, 'trace' => true, 'keep_alive' => false));
        }
        catch(\Exception $e) {
            return false;
        }

        try {
            $result = $client->GetCapOptionsBundle($args);
            $result = $result->GetCapOptionsBundleResult->Returned_DataSet->any;

        } catch (SoapFault $e) {
            return false;
        }

        $xml = simplexml_load_string($result);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);

        $options = $array['NVDBundle']['Options'];

        // Update db
        $optionCodes = [];
        foreach($options as $option) {
            $optionCodes[] = $option['Opt_OptionCode'];
        }

        // first remove existing extras
        DB::table('cap_optional_equipment')
            ->whereIn('OptionCode', $optionCodes)
            ->where('cap_id', '=', $cap_id)
            ->delete();

        // save new data
        foreach($options as $option) {
            DB::table('cap_optional_equipment')->insert(
                array(
                    'OptionCode' => $option['Opt_OptionCode'],
                    'Do_Description' => $option['Do_Description'],
                    'Dc_Description' => $option['Dc_Description'],
                    'price'=> $option['Opt_Basic'],
                    'cap_id' => $cap_id,
                    'database'=>$database
                )
            );
        }
        // remove old entry if exists
        DB::table('cap_id_optional_equipment')
            ->where('cap_id', '=', $cap_id)
            ->where('database', '=', $database)
            ->delete();
        // save cap assignment
        DB::table('cap_id_optional_equipment')->insert(
            array(
                'ce_ids' => json_encode($optionCodes),
                'cap_id' => $cap_id,
                'database' => $database,
                'date' => now()
            )
        );

        return $options;

    }

    /**
     * @param $maker
     */
    private function getManufacturerLink($maker) {

        $makers = [
            'ABARTH' => 'https://www.abarthcars.co.uk',
            'ALFA ROMEO' => 'https://www.alfaromeo.co.uk',
            'ALPINE' => 'https://www.alpinecars.com',
            'AUDI' => 'https://www.audi.co.uk',
            'BMW' => 'https://www.bmw.co.uk',
            'CITROEN' => 'https://www.citroen.co.uk',
            'CUPRA' => 'https://www.cupraofficial.co.uk',
            'DACIA' => 'https://www.dacia.co.uk',
            'DS' => 'https://www.dsautomobiles.co.uk',
            'FIAT' => 'https://www.fiat.co.uk',
            'FORD' => 'https://www.ford.co.uk',
            'HONDA' => 'https://www.honda.co.uk',
            'HYUNDAI' => 'https://www.hyundai.co.uk',
            'ISUZU' => 'https://www.isuzu.co.uk',
            'IVECO' => 'https://www.iveco.com',
            'JAGUAR' => 'https://www.jaguar.co.uk',
            'JEEP' => 'https://www.jeep.co.uk',
            'KIA' => 'https://www.kia.com',
            'LAND ROVER' => 'https://www.landrover.co.uk',
            'LEVC' => 'https://www.levc.com',
            'LEXUS' => 'https://www.lexus.co.uk',
            'MAN' => 'https://www.man.eu',
            'MASERATI' => 'https://www.maserati.com',
            'MAXUS' => 'https://saicmaxus.co.uk',
            'MAZDA' => 'https://www.mazda.co.uk',
            'MERCEDES-BENZ' => 'https://www.mercedes-benz.co.uk',
            'MG MOTOR UK' => 'https://mg.co.uk',
            'MINI' => 'https://www.mini.co.uk',
            'MITSUBISHI' => 'https://mitsubishi-motors.co.uk',
            'NISSAN' => 'https://www.nissan.co.uk',
            'PEUGEOT' => 'https://www.peugeot.co.uk',
            'POLESTAR' => 'https://www.polestar.com',
            'PORSCHE' => 'https://www.porsche.com',
            'RENAULT' => 'https://www.renault.co.uk',
            'SEAT' => 'https://www.seat.co.uk',
            'SKODA' => 'https://www.skoda.co.uk',
            'SMART' => 'https://www.smart.com',
            'SSANGYONG' => 'https://www.ssangyonggb.co.uk',
            'SUBARU' => 'https://subaru.co.uk',
            'SUZUKI' => 'https://www.suzuki.co.uk',
            'TESLA' => 'https://www.tesla.com',
            'TOYOTA' => 'https://www.toyota.co.uk',
            'VAUXHALL' => 'https://www.vauxhall.co.uk',
            'VOLKSWAGEN' => 'https://www.volkswagen.co.uk',
            'VOLVO' => 'https://www.volvocars.com',
        ];
        if(array_key_exists($maker, $makers)) {
            return $makers[$maker];
        }else{
            return false;
        }
    }

    /**
     * @param null $cap_id
     * @param $database
     * @return false|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    private function fetchVehicleDefaultImage($cap_id = null, $database) {

        if(isset($cap_id) && !is_null($cap_id)) {
            $image = DB::table('cap_images')
                ->where('cap_id', '=', $cap_id)
                ->where('is_default', '=', 1)
                ->where('database', '=', $database)
                ->first();

            return $image;
        }else{
            return false;
        }

    }

    /**
     * @param null $cap_id
     * @param $database
     * @return false|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    private function fetchAllVehicleImages($cap_id = null, $database) {

        if(isset($cap_id) && !is_null($cap_id)) {
            $image = DB::table('cap_images')
                ->where('cap_id', '=', $cap_id)
                ->where('database', '=', $database)
                ->get();

            return $image;
        }else{
            return false;
        }

    }

    // Method to find missing C&D images
    public function candd() {

        echo 'cap id, maker, range, derivative, database<br>';
        foreach(array('CARS', 'LCV') as $database) {
            $not = DB::table('capreference')
                ->where('imageid', '=', 0)
                ->where('database', '=', $database)
                ->get();

            foreach ($not as $no) {

                $check = DB::table('capreference')
                    ->where('maker', '=', $no->maker)
                    ->where('range', '=', $no->range)
                    ->where('model', '=', $no->model)
                    ->where('imageid', '>', 0)
                    ->get();

                if(count($check) > 0) {
                    echo $no->cap_id . ',' . $no->maker . ',' . $no->range . ',' . $no->derivative . ',' . $database . ', (';

                    foreach ($check as $yes) {

                        echo $yes->cap_id . ',';

                    }
                    echo ')<br>';
                }


            }
        }

    }

}
