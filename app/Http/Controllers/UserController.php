<?php

namespace App\Http\Controllers;

use Auth;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return view('pages.admin.home');
        }

//        return view('pages.user.home');
        return redirect('/my-account');
    }
    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
