<?php

namespace App\Http\Controllers;

ini_set('memory_limit', '2048M');
ini_set('max_execution_time', 0);


use App\Models\Cap;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use SoapClient;
use SimpleXMLElement;


class CapUpdateController extends Controller
{

    // move this into config file
    public $subscriberId = 175356;
    public $password = "Jlerl67";

    public $wsdl = "https://soap.cap.co.uk/Vehicles/CapVehicles.asmx?wsdl";
    public $nvdWsdl = "https://soap.cap.co.uk/Nvd/CapNvd.asmx?wsdl";
    public $imageWsdl = "https://soap.cap.co.uk/images/VehicleImage.aspx?wsdl";
    public $database = "LCV";

    public $feedDir = "";

    /**
     * @throws \SoapFault
     */
    public function update_van_leasing()
    {

        $caps = $this->update_van_cap_ids();

        if ($caps) {
            $updateCaps = $this->update_capreference();
        }

        $updateCaps = $this->update_optional_extras();


        if($updateCaps) {
            $images = $this->update_images();
        }

        $this->addToCapReference();

    }

    /**
     * @return bool
     * @throws \SoapFault
     */
    private function update_van_cap_ids()
    {
        $deal = DB::table('van_pricestable')
            ->distinct('cap_code')
            ->whereRaw('cap_code NOT IN (SELECT DISTINCT(cap_code) FROM van_capreference)')
            ->whereRaw('cap_code NOT IN (SELECT DISTINCT(cap_code) FROM van_invalid_cap)')
            ->get();

        $counter = 0;

        foreach ($deal as $capCode) {

            $url = "https://soap.cap.co.uk/Vehicles/CapVehicles.asmx/GetCapidFromCapcode";
            $args = array(
                'subscriberId' => $this->subscriberId,
                'password' => $this->password,
                'database' => $this->database,
                'caPcode' => $capCode->cap_code

            );
            $client = $this->executeQuery($url, $args);

            try {
                $result = $client->GetCapidFromCapcode($args);
                $result = $result->GetCapidFromCapcodeResult->Returned_DataSet->any;

            } catch (SoapFault $e) {
                echo "Error: {$e}";
            }

            $xml = simplexml_load_string($result);

            if (count($xml) < 1) {

                DB::table('van_invalid_cap')->insert(
                    array(
                        'cap_code' => $capCode->cap_code,
                    )
                );

            } else {
                foreach ($xml as $item) {
                    foreach ($item as $cap) {
                        $capId = $cap->CDer_ID;

                        $capCheck = DB::table('van_capreference')
                            ->where('cap_id', '=', $capId)
                            ->get();

                        if (count($capCheck) == 0) {

                            DB::table('van_capreference')->insert(
                                array(
                                    'cap_id' => $capId,
                                    'cap_code' => $capCode->cap_code,
                                    'updated_at' => now()
                                )
                            );
                        }

                        $counter++;
                    }
                }
            }

        }
        return true;
    }

    /**
     * @return bool
     * @throws \SoapFault
     */
    private function update_capreference()
    {


        $cap = DB::table('van_capreference')
            ->whereNull('maker')
            ->get();


        foreach ($cap as $capReference) {


            // Grab first set of data
            $url = "https://soap.cap.co.uk/Vehicles/CapVehicles.asmx/GetCapDescriptionFromId";
            $args = array(
                'subscriberId' => $this->subscriberId,
                'password' => $this->password,
                'database' => $this->database,
                'capid' => $capReference->cap_id

            );
            $client = $this->executeQuery($url, $args);

            try {
                $result = $client->GetCapDescriptionFromId($args);
                $result = $result->GetCapDescriptionFromIdResult->Returned_DataSet->any;

            } catch (SoapFault $e) {
                echo "Error: {$e}";
                return false;
            }

            $xml = simplexml_load_string($result);

            if (count($xml) < 1) {


            } else {

                foreach ($xml as $item) {
                    foreach ($item as $cap) {


                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update([
                            'maker' => $cap->CVehicle_ManText,
                            'range' => $cap->CVehicle_ShortModText,
                            'model' => $cap->CVehicle_ModText,
                            'trim' => $cap->CVehicle_ShortDerText,
                            'derivative' => $cap->CVehicle_DerText,

                        ]);

                    }
                }
            }


            // grab all the technical info
            $url = "https://soap.cap.co.uk/Nvd/CapNvd.asmx/GetTechnicalData";
            $args = array(
                'subscriberId' => $this->subscriberId,
                'password' => $this->password,
                'database' => $this->database,
                'capid' => $capReference->cap_id,
                'techDate' => date('Y-m-d'),
                'justCurrent' => true

            );
            $client = $this->executeNVDQuery($url, $args);

            try {
                $result = $client->GetTechnicalData($args);
                $result = $result->GetTechnicalDataResult->Returned_DataSet->any;

            } catch (SoapFault $e) {
                echo "Error: {$e}";
                return false;
            }

            $xml = simplexml_load_string($result);

            if (count($xml) < 1) {


            } else {
                foreach ($xml as $item) {

                    foreach ($item as $cap) {

                        switch ($cap->DT_LongDescription) {
                            case "CO2":
                                if (count($cap->tech_value_string) > 0) {
                                    if($cap->tech_value_string != 'Not Available') {
                                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['co2' => $cap->tech_value_string]);
                                    }
                                }
                                break;
                            case "Badge Engine CC":
                                if (count($cap->tech_value_string) > 0) {
                                    DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['enginesize' => $cap->tech_value_string]);
                                }
                                break;
                            case "EC Extra Urban (mpg)":
                                if (count($cap->tech_value_string) > 0) {
                                    DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['mpgextraurban' => $cap->tech_value_string]);
                                }
                                break;
                            case "EC Combined (mpg)":
                                if (count($cap->tech_value_string) > 0) {
                                    DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['mpgcombined' => $cap->tech_value_string]);
                                }
                                break;
                        }

                    }
                }
            }
        }

        $cap = DB::table('van_capreference')
            ->whereNull('transmission')
            ->orWhereNull('fuel')
            ->orWhereNull('doors')
            ->get();


        foreach ($cap as $capReference) {

            $url = "https://soap.cap.co.uk/Nvd/CapNvd.asmx/GetBulkTechnicalData";
            $args = array(
                'subscriberId' => $this->subscriberId,
                'password' => $this->password,
                'database' => $this->database,
                'capidList' => "$capReference->cap_id",
                'specDateList' => date('Y/m/d'),
                'techDataList' => '',
                'returnVehicleDescription' => false,
                'returnCaPcodeTechnicalItems' => true,
                'returnCostNew' => false,


            );

            $client = $this->executeNVDQuery($url, $args);

            try {
                $result = $client->GetBulkTechnicalData($args);
                $result = $result->GetBulkTechnicalDataResult->Returned_DataSet->any;

            } catch (SoapFault $e) {
                echo "Error: {$e}";
                return false;
            }

            $xml = simplexml_load_string($result);
            if (count($xml) < 1) {


            } else {
                foreach ($xml as $item) {
                    foreach ($item as $cap) {

                        // Fuel Update
                        if (isset($cap->FuelType) && !is_null($cap->FuelType)) {

                            switch ($cap->FuelType) {
                                case "D":
                                    $fuel = 'Diesel';
                                break;
                                case "P":
                                    $fuel = 'Petrol';
                                break;
                                case "E":
                                    $fuel = 'Electric';
                                break;
                                case "X":
                                    $fuel = 'PlugIn Elec Hybrid';
                                break;
                                default:
                                    var_dump($cap);
                                    die();
                                break;
                            }

                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['fuel' => $fuel]);
                        }

                        // Transmission update
                        if (isset($cap->FuelType) && !is_null($cap->Transmission)) {
                            if($cap->Transmission == 'M') {
                                $transmission = 'Manual';
                            }else{
                                $transmission = 'Automatic';
                            }
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['transmission' => $transmission]);
                        }

                        // Doors update
                        if (isset($cap->FuelType) && !is_null($cap->Doors)) {
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['doors' => $cap->Doors]);
                        }

                    }
                }
            }

        }

        return true;
    }

    private function update_images() {

        $imageDir = 'public/images/capimages/lcv/';

        $cap = DB::table('van_capreference')
            ->whereNull('imageid')
            ->get();

        foreach($cap as $capReference) {

            $imageId = $capReference->cap_id;

            $strtomd5 = $this->subscriberId . $this->password  . $this->database . $capReference->cap_id;
            $hashcode = strtoupper(md5($strtomd5));

            $url = "https://soap.cap.co.uk/images/VehicleImage.aspx";
            $url .= "?subid=" . $this->subscriberId;
            $url .= "&db=" . $this->database;
            $url .= "&hashcode=" . $hashcode;
            $url .= "&capid=" . $capReference->cap_id;
            $url .= "&date=" . date('Y-m-d');


            $destination = public_path() . "/images/capimages/LCV/" . $imageId . ".jpg";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $data = curl_exec ($ch);
            curl_close ($ch);

            $file = fopen($destination, "w+");
            fputs($file, $data);
            fclose($file);

            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['imageid' => $imageId]);

        }

    }

    private function update_optional_extras() {

        $cap = DB::table('van_capreference')
            ->whereRaw('cap_id NOT IN (SELECT cap_id FROM van_checked_optionals)')
            ->get();

        foreach($cap as $capReference) {
            $url = "https://soap.cap.co.uk/Nvd/CapNvd.asmx/GetCapOptionsBundle";
            $args = array(
                'subscriberId' => $this->subscriberId,
                'password' => $this->password,
                'database' => $this->database,
                'capid' => $capReference->cap_id,
                'optionDate' => date('Y-m-d'),
                'justCurrent' => true,
                'descriptionRs' => true,
                'optionsRs' => true,
                'relationshipsRs' => true,
                'packRs' => true,
                'technicalRs' => true

            );

            $client = $this->executeNVDQuery($url, $args);

            try {
                $result = $client->GetCapOptionsBundle($args);
                $result = $result->GetCapOptionsBundleResult->Returned_DataSet->any;

            } catch (SoapFault $e) {
                echo "Error: {$e}";
                return false;
            }

            $xml = simplexml_load_string($result);
            if (count($xml) < 1) {


            } else {
                foreach ($xml->NVDBundle->Options as $item) {

                    if(strpos($item->Do_Description, 'bluetooth')) {
                        if($item->Opt_Default == 'true') {
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['bluetooth' => 1]);
                        }
                    }
                    if(strpos($item->Do_Description, 'Reversing camera')) {
                        if($item->Opt_Default == 'true') {
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['parking_camera' => 1]);
                        }
                    }

                    if(strpos($item->Do_Description, 'alloy wheels')) {
                        if($item->Opt_Default == 'true') {
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['alloys' => 1]);
                        }
                    }
                    if(strpos($item->Do_Description, "Heated driver's seat")) {
                        if($item->Opt_Default == 'true') {
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['heated_seats' => 1]);
                        }
                    }
                    if(strpos($item->Do_Description, "Leatherette")) {
                        if($item->Opt_Default == 'true') {
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['leather_seats' => 1]);
                        }
                    }
                }
            }


            DB::table('van_checked_optionals')->insert(
                array(
                    'cap_id' => $capReference->cap_id,
                )
            );

        }
    }

    private function addToCapReference() {

        $vanCap = DB::table('van_capreference')
        ->get();

        foreach($vanCap as $cap) {

            $updateCheck = DB::table('capreference')
                ->where('cap_id', '=', $cap->cap_id)
                ->first();
            if($updateCheck) {
                DB::table('capreference')
                    ->where('cap_id', $cap->cap_id)
                    ->update([
                        'maker' => $cap->maker,
                        'range' => $cap->range,
                        'model' => $cap->model,
                        'trim' => $cap->trim,
                        'derivative' => $cap->derivative,
                        'fuel' => $cap->fuel,
                        'transmission' => $cap->transmission,
                        'doors' => $cap->doors,
                        'co2' => $cap->co2,
                        'imageid' => $cap->imageid,
                        'enginesize' => $cap->enginesize,
                        'mpgextraurban' => $cap->mpgextraurban,
                        'mpgcombined' => $cap->mpgcombined,
                        'database' => 'LCV',
                    ]);
            }else {

                $capreference = new Cap();
                $capreference->cap_id = $cap->cap_id;
                $capreference->maker = $cap->maker;
                $capreference->range = $cap->range;
                $capreference->model = $cap->model;
                $capreference->trim = $cap->trim;
                $capreference->derivative = $cap->derivative;
                $capreference->fuel = $cap->fuel;
                $capreference->transmission = $cap->transmission;
                $capreference->doors = $cap->doors;
                $capreference->co2 = $cap->co2;
                $capreference->imageid = $cap->imageid;
                $capreference->enginesize = $cap->enginesize;
                $capreference->mpgextraurban = $cap->mpgextraurban;
                $capreference->mpgcombined = $cap->mpgcombined;
                $capreference->database = 'LCV';
                $capreference->save();
            }

        }

        return true;

    }


    /**
     * @param $url
     * @return SoapClient
     * @throws \SoapFault
     */
    private function executeQuery($url)
    {

        $wsdl = $this->wsdl;
        $client = new SoapClient($wsdl, array('soap_version' => SOAP_1_1, 'trace' => true,));

        return $client;

    }

    /**
     * @param $url
     * @return SoapClient
     * @throws \SoapFault
     */
    private function executeNVDQuery($url)
    {

        $wsdl = $this->nvdWsdl;
        $client = new SoapClient($wsdl, array('soap_version' => SOAP_1_1, 'trace' => true,));

        return $client;

    }

    /**
     * @param $url
     * @return SoapClient
     * @throws \SoapFault
     */
    private function executeImageQuery($url)
    {

        $wsdl = $this->imageWsdl;
        $client = new SoapClient($wsdl, array('soap_version' => SOAP_1_1, 'trace' => true,));

        return $client;

    }

}