<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Cap;
use App\Models\Pricing;
use App\Models\PriceHistory;
use Illuminate\Support\Facades\Redirect;
use Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use jeremykenedy\LaravelRoles\Models\Role;

class GarageControllerold extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function view_garage()
    {

        $data = Request::all();
        $garage = array();
        $user = Auth::user();
        $userId = $user->id;
        $garageContent = DB::table('users')
            ->where('id', '=', $userId)
            ->first();

        $garage = unserialize($garageContent->garage);

        $cars = array();

        if (!is_null($garage) && is_array($garage)) {
            foreach ($garage as $car) {
                // fetch price details for car
                $prices = DB::table('pricestable')
                    ->where('id', $car)
                    ->get();

                if (count($prices)==0) {
                    $prices = DB::table('van_pricestable')
                        ->where('id', $car)
                        ->get();
                    if (count($prices)==0) {
                        $this->delete_car($car);
                    }
                }
                foreach ($prices as $res) {
                    $cap = DB::table('capreference')
                        ->where('cap_id', '=', $res->cap_id)
                        ->get();

                    foreach ($cap as $gar) {

                        $cars[$car]['id'] = $car;
                        $cars[$car]['finance_type'] = $res->finance_type;
                        $cars[$car]['maker'] = $gar->maker;
                        $cars[$car]['range'] = $gar->range;
                        $cars[$car]['model'] = $gar->model;
                        $cars[$car]['cap_id'] = $res->cap_id;
                        $cars[$car]['deal_id'] = $res->deal_id;
                        $cars[$car]['deposit_value'] = $res->deposit_value;
                        $cars[$car]['term'] = $res->term;
                        $cars[$car]['monthly_payment'] = $res->monthly_payment;
                        $cars[$car]['annual_mileage'] = $res->annual_mileage;
                        $cars[$car]['in_stock'] = $res->in_stock;
                        $cars[$car]['document_fee'] = $res->document_fee;
                        $cars[$car]['expires_at'] = $res->expires_at;
                        $cars[$car]['referenceTrim'] = $gar->trim;
                        $cars[$car]['co2'] = $gar->co2;
                        $cars[$car]['referenceFuel'] = $gar->fuel;
                        $cars[$car]['mpgcombined'] = $gar->mpgcombined;
                        $cars[$car]['referenceTransmission'] = $gar->transmission;
                        $cars[$car]['referenceBodystyle'] = $gar->bodystyle;
                        $cars[$car]['referenceDerivative'] = $gar->derivative;
                        $cars[$car]['deposit_months'] = $res->deposit_months;
                        $cars[$car]['imageid'] = $gar->imageid;
                        $cars[$car]['database'] = $gar->database;
                    }
                }

            }

        }

        return View('garage_views.view-garage', compact('cars'));
    }

    public function delete_garage($carId)
    {
        if($this->delete_car($carId)) {
            return Redirect::to('/my-account/garage?m=2');
        }

    }

    public function add_garage($carId)
    {
        if (request()->ajax()) {
            $garage = array();
            $user = Auth::user();

            if (is_null($user)) {
                return "L";
            } else {

                $userId = $user->id;

                $garageContent = DB::table('users')
                    ->where('id', '=', $userId)
                    ->first();
                $existingGarage = unserialize($garageContent->garage);


                if ($existingGarage) {

                    $garage = $existingGarage;

                    foreach($garage as $k=>$v) {

                        // fetch price details for car
                        $prices = DB::table('pricestable')
                            ->where('id', $k)
                            ->get();

                        if (count($prices)==0) {

                            $prices = DB::table('van_pricestable')
                                ->where('id', $k)
                                ->get();

                            if (count($prices)==0) {
                                $this->delete_car($k);
                                $return = 'd';
                            }

                        }
                    }

                }

                if (!array_key_exists($carId, $garage)) {
                    if ($existingGarage) {

                        $garage = $existingGarage;

                        // maximum cars allowed in garage
                        if (count($garage) >= 3) {
                            return "C";
                        } else {

                            // check if the car is already in the garage or not
                            if (!array_key_exists($carId, $garage)) {
                                $garage[$carId] = $carId;
                            }
                        }

                    } else {
                        // no cars in garage so add it
                        $garage[$carId] = $carId;
                    }
                }

                // encode for database input
                $garage = serialize($garage);

                $user = User::find($userId);
                $user->garage = $garage;
                $user->save();

                if(isset($return) && $return == 'd') {
                    return "S1";
                }else{
                    return "S";
                }
            }
        }

    }

    private function delete_car($carId)
    {

        $user = Auth::user();
        $userId = $user->id;


        $garageContent = DB::table('users')
            ->where('id', '=', $userId)
            ->first();
        $existingGarage = unserialize($garageContent->garage);

        if (in_array($carId, $existingGarage)) {
            unset($existingGarage[$carId]);
        }

        $existingGarage = serialize($existingGarage);

        $user = User::find($userId);
        $user->garage = $existingGarage;
        $user->save();

        return true;
    }

    public function switchPriceDropNotifications () {
        $data = Request::all();
        $notification = $data['checkbox'];

        $user = Auth::user();
        $userId = $user->id;

        $user = User::find($userId);
        $user->garage_price_notification = $notification;
        $user->save();
    }


}