<?php
namespace App\Http\Controllers;

ini_set('memory_limit', '4G');

use App\Models\Quote;
use App\Models\User;
use App\Models\Cap;
use App\Models\Pricing;

use Illuminate\Support\Facades\Cache;
use Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use jeremykenedy\LaravelRoles\Models\Role;

class LeasingController extends Controller
{

    public function __construct()
    {

    }

    public function vanLeasing(Request $request, $maker, $database="LCV") {
        return $this->carLeasing($request, $maker, 'LCV');
    }

    /**
     * @param Request $request
     * @param $maker
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description controller method for displaying manufacturer pages
     */
    public function carLeasing(Request $request, $maker, $database="cars")
    {
        $requestData = Request::all();
        $getMake = $maker;

        $results = array();
        $getModels = array();

        // get rid of spacing in url except for mercs
        if($getMake != 'mercedes-benz') {
            $getMake = str_replace('-',' ',$getMake);
        }
        $maker = $getMake;
        if($database == 'LCV') {
            $pricestable = 'van_pricestable';
        }else{
            $pricestable = 'pricestable';
        }
        // Get each model for the maker
        $getModels = DB::table('manufacturers_search_ranges')
            ->where('maker', '=', $maker)
            ->where('status','=', 1)
            ->where('database', '=', $database)
            ->orderBy('range')
            ->get();

        if(count($getModels) > 0) {

            // get pricing details for the models
            foreach ($getModels as $model) {
                $pricing = DB::table('manufacturers_search_prices')
                    ->where('range_id', '=', $model->id)
                    ->get();

                $results[$model->range]['maker'] = $model->maker;
                $results[$model->range]['range'] = $model->range;

                $cdnUrl = env('DO_IMAGE_PATH') . strtoupper($database) . '/';
                if(isset($model->image_cap_id)) {
                    $vehicleImage = $this->fetchVehicleDefaultImage($model->image_cap_id, $database);
                    if(is_null($vehicleImage)) {
                        $model->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
                    }

                    if (isset($vehicleImage->image_name) && !is_null($vehicleImage->image_name)) {
                        $imageUrl = $cdnUrl . 'big/' . $vehicleImage->image_name;
                        if (@getimagesize($imageUrl)) {
                            $model->imageid = $imageUrl;
                        } else {
                            $model->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
                        }
                    }
                }else{
                    $model->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
                }


                $results[$model->range]['imageid'] = $model->imageid;

                foreach ($pricing as $price) {
                    $results[$model->range]['personal']['monthly_payment'] = $price->personal_price;
                    $results[$model->range]['business']['monthly_payment'] = $price->business_price;
                }

            }

            // get rid of spacing in url except for mercs
            if ($maker != 'mercedes-benz') {
                $make = str_replace('-', ' ', $maker);
                $make = ucwords(strtolower($make));
            } else {
                $make = 'Mercedes-Benz';
            }

            if ($make == 'Bmw' || $make == 'Ldv' || strtolower($make) == 'man') {
                $make = strtoupper($make);
            }
            if(strtolower($make) == 'mg motor uk') {
                $make = "MG Motor UK";
            }

            return View('leasing_views.models', compact('getModels', 'results', 'make', 'database'));

        }else{
            $counter = 0;
            $make = ucfirst($getMake);
            // get rid of spacing in url except for mercs
            if ($maker != 'mercedes-benz') {
                $make = str_replace('-', ' ', $maker);
                $make = ucwords(strtolower($make));
            } else {
                $make = 'Mercedes-Benz';
            }

            if ($make == 'Bmw' || $make == 'Ldv' || strtolower($make) == 'man') {
                $make = strtoupper($make);
            }
            if(strtolower($make) == 'mg motor uk') {
                $make = "MG Motor UK";
            }
            return View('leasing_views.models', compact('counter', 'database', 'make'));
        }
    }


    public function viewPersonalVans() {

        return $this->viewPersonal('LCV');
    }

    public function viewPersonal($database = 'CARS')
    {

        $data = Request::all();

        $ranges = array();
        if (!isset($data['finance_type'])) {
            $data['finance_type'] = 'P';
        }

        if($data['finance_type']== 'H') {
            $listing_type = 'H';
            $data['listing_type'] = 'H';
        }elseif($data['finance_type'] == 'A') {
            $listing_type = 'A';
            $data['listing_type'] = 'A';
        }elseif($data['finance_type'] == 'PERFORMANCE') {
            $listing_type = 'PERFORMANCE';
            $data['listing_type'] = 'PERFORMANCE';
        }else{
            $listing_type = 'P';
            $data['listing_type'] = 'P';
        }

        $data['database'] = $database;

        $finance_type = $data['finance_type'];
        if($database == 'LCV') {
            $pageName = "Compare Personal Van Lease Deals";
        }else{
            $pageName = "Compare Personal Lease Deals";
        }

//        $makes = $this->generateFilterSql($data, 'maker');
        $makes = $this->getMakes($listing_type, $database);
        $trims = [];
        $rangeData = $this->generateFilterSql($data, 'range');
        foreach ($rangeData as $range) {
            $ranges[$range->maker][] = $range->range;
        }
        $trimData = $this->generateFilterSql($data, 'trim');
        foreach ($trimData as $trim) {
            $trims[$trim->range][] = $trim->trim;
        }
        $fuels = $this->generateFilterSql($data, 'fuel');
        $transmissions = $this->generateFilterSql($data, 'transmission');
        $bodystyles = $this->generateFilterSql($data, 'bodystyle');
        $doors = $this->generateFilterSql($data, 'doors');
        $enginesizes = $this->generateFilterSql($data, 'enginesize');
        $en = array();
        foreach($enginesizes as $engine)
        {
            $en[] = $engine->enginesize;
        }
        $enginesizes = $en;

        $monthly_payments = '';//$this->generateFilterSql($data, null, 'monthly_payment');



//        $deposit_values = $this->generateFilterSql($data, null, 'deposit_value');
//        $terms = $this->generateFilterSql($data, null, 'term');
//        $annual_mileages = $this->generateFilterSql($data, null, 'annual_mileage');
//        $annual_mileages = $this->fetchAnnualMiles($data['finance_type']);
        $deposit_values = $this->fetchDepositValues();
        $terms = $this->fetchTerms();
        $annual_mileages = $this->fetchAnnualMiles();



        $data = $this->generateData($data, $data['finance_type']);

        $summaryCap = $data['summaryCap'];

        $vars = compact(
            'pageName',
            'summaryCap',
            'makes',
            'ranges',
            'trims',
            'fuels',
            'transmissions',
            'bodystyles',
            'doors',
            'enginesizes',
            'monthly_payments',
            'deposit_values',
            'terms',
            'annual_mileages',
            'finance_type',
            'listing_type',
            'database'
        );

        return View('leasing_views/listing', $vars);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewBusinessVans() {
        return $this->viewBusiness('LCV');
    }

    /**
     * @param string $database
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewBusiness($database = 'cars')
    {
        $data = Request::all();

        $ranges = array();
        if (!isset($data['finance_type'])) {
            $data['finance_type'] = 'B';
        }

        if($data['finance_type']== 'H') {
            $listing_type = 'H';
            $data['listing_type'] = 'H';
        }elseif($data['finance_type'] == 'A') {
            $listing_type = 'A';
            $data['listing_type'] = 'A';
        }elseif($data['finance_type'] == 'PERFORMANCE') {
            $listing_type = 'PERFORMANCE';
            $data['listing_type'] = 'PERFORMANCE';
        }else{
            $listing_type = 'P';
            $data['listing_type'] = 'P';
        }

        $data['database'] = $database;

        $finance_type = $data['finance_type'];
        if($database == 'LCV') {
            $pageName = "Compare Business Van Lease Deals";
        }else{
            $pageName = "Compare Business Lease Deals";
        }

        $makes = $this->generateFilterSql($data, 'maker');
        $makes = $this->getMakes($listing_type, $database);
        $rangeData = $this->generateFilterSql($data, 'range');
        foreach ($rangeData as $range) {
            $ranges[$range->maker][] = $range->range;
        }
        $trimData = $this->generateFilterSql($data, 'trim');
        foreach ($trimData as $trim) {
            $trims[$trim->range][] = $trim->trim;
        }
        $fuels = $this->generateFilterSql($data, 'fuel');
        if($database !='LCV') {
            $doors = $this->generateFilterSql($data, 'doors');
        }
        $transmissions = $this->generateFilterSql($data, 'transmission');
        $bodystyles = $this->generateFilterSql($data, 'bodystyle');
        $enginesizes = $this->generateFilterSql($data, 'enginesize');

        $monthly_payments = '';//$this->generateFilterSql($data, null, 'monthly_payment');
//        $deposit_values = $this->generateFilterSql($data, null, 'deposit_value');
//        $terms = $this->generateFilterSql($data, null, 'term');
//        $annual_mileages = $this->generateFilterSql($data, null, 'annual_mileage');
//        $annual_mileages = $this->fetchAnnualMiles($data['finance_type']);

        $deposit_values = $this->fetchDepositValues();
        $terms = $this->fetchTerms();
        $annual_mileages = $this->fetchAnnualMiles();

//        $extras = array();
        $data = $this->generateData($data, $data['finance_type']);

        $summaryCap = $data['summaryCap'];

        $vars = compact(
            'pageName',
            'summaryCap',
            'makes',
            'ranges',
            'trims',
            'fuels',
            'transmissions',
            'bodystyles',
            'doors',
            'enginesizes',
            'monthly_payments',
            'deposit_values',
            'terms',
            'annual_mileages',
            'finance_type',
            'listing_type',
            'database'
        );

        return View('leasing_views/listing', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function viewHybrid(Request $request, $database = 'CARS')
    {
        $data = Request::all();
        $pageName = "Compare Electric & Hybrid lease deals";

        $ranges = array();
        if (!isset($data['finance_type'])) {
            $data['finance_type'] = 'H';
        }

        $data['database'] = $database;

        if(!isset($data['listing_type'])) {
            if($data['finance_type']== 'H') {
                $listing_type = 'H';
                $data['listing_type'] = 'H';
            }elseif($data['finance_type'] == 'A') {
                $listing_type = 'A';
                $data['listing_type'] = 'A';
            }elseif($data['finance_type'] == 'PERFORMANCE') {
                $listing_type = 'PERFORMANCE';
                $data['listing_type'] = 'PERFORMANCE';
            }else{
                $listing_type = 'P';
                $data['listing_type'] = 'P';
            }
        }else{
            $listing_type = $data['listing_type'];
        }


        $finance_type = $data['finance_type'];
        


//        $makes = $this->generateFilterSql($data, 'maker');
        $makes = $this->getMakes($listing_type);
        $rangeData = $this->generateFilterSql($data, 'range');
        foreach ($rangeData as $range) {
            $ranges[$range->maker][] = $range->range;
        }
        $trimData = $this->generateFilterSql($data, 'trim');
        foreach ($trimData as $trim) {
            $trims[$trim->range][] = $trim->trim;
        }
        $fuels = $this->generateFilterSql($data, 'fuel');

        $transmissions = $this->generateFilterSql($data, 'transmission');
        $doors = $this->generateFilterSql($data, 'doors');
        $bodystyles = $this->generateFilterSql($data, 'bodystyle');
        $enginesizes = $this->generateFilterSql($data, 'enginesize');
        $monthly_payments = '';//$this->generateFilterSql($data, null, 'monthly_payment');
//        $deposit_values = $this->generateFilterSql($data, null, 'deposit_value');
//        $terms = $this->generateFilterSql($data, null, 'term');
//        $annual_mileages = $this->generateFilterSql($data, null, 'annual_mileage');

        $deposit_values = $this->fetchDepositValues();
        $terms = $this->fetchTerms();
        $annual_mileages = $this->fetchAnnualMiles();

//        $extras = array();

        $data = $this->generateData($data, $data['finance_type']);


        $summaryCap = $data['summaryCap'];

        $vars = compact(
            'pageName',
            'summaryCap',
            'makes',
            'doors',
            'ranges',
            'trims',
            'fuels',
            'transmissions',
            'bodystyles',
            'enginesizes',
            'monthly_payments',
            'deposit_values',
            'terms',
            'annual_mileages',
            'finance_type',
            'listing_type',
            'database'
        );

        return View('leasing_views/listing', $vars);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function viewPerformance(Request $request, $database = 'CARS')
    {

        $data = Request::all();

        $pageName = "Compare Performance Lease Deals";

        $ranges = array();
        if (!isset($data['finance_type'])) {
            $data['finance_type'] = 'PERFORMANCE';
        }

        if($data['finance_type']== 'H') {
            $listing_type = 'H';
            $data['listing_type'] = 'H';
        }elseif($data['finance_type'] == 'A') {
            $listing_type = 'A';
            $data['listing_type'] = 'A';
        }elseif($data['finance_type'] == 'PERFORMANCE') {
            $listing_type = 'PERFORMANCE';
            $data['listing_type'] = 'PERFORMANCE';
        }else{
            $listing_type = 'P';
            $data['listing_type'] = 'P';
        }

        $data['database'] = $database;

        $finance_type = $data['finance_type'];


//        $makes = $this->generateFilterSql($data, 'maker');
        $makes = $this->getMakes($listing_type);
        $rangeData = $this->generateFilterSql($data, 'range');
        foreach ($rangeData as $range) {
            $ranges[$range->maker][] = $range->range;
        }
        $trimData = $this->generateFilterSql($data, 'trim');
        foreach ($trimData as $trim) {
            $trims[$trim->range][] = $trim->trim;
        }
        $fuels = $this->generateFilterSql($data, 'fuel');

        $transmissions = $this->generateFilterSql($data, 'transmission');
        $bodystyles = $this->generateFilterSql($data, 'bodystyle');
        $enginesizes = $this->generateFilterSql($data, 'enginesize');
        $doors = $this->generateFilterSql($data, 'doors');
        $monthly_payments = '';//$this->generateFilterSql($data, null, 'monthly_payment');
//        $deposit_values = $this->generateFilterSql($data, null, 'deposit_value');
//        $terms = $this->generateFilterSql($data, null, 'term');
//        $annual_mileages = $this->generateFilterSql($data, null, 'annual_mileage');

        $deposit_values = $this->fetchDepositValues();
        $terms = $this->fetchTerms();
        $annual_mileages = $this->fetchAnnualMiles();

//        $extras = array();
        $data = $this->generateData($data, $data['finance_type']);


        $summaryCap = $data['summaryCap'];

        $vars = compact(
            'pageName',
            'summaryCap',
            'makes',
            'doors',
            'ranges',
            'trims',
            'fuels',
            'transmissions',
            'bodystyles',
            'enginesizes',
            'monthly_payments',
            'deposit_values',
            'terms',
            'annual_mileages',
            'finance_type',
            'listing_type',
            'database'
        );

        return View('leasing_views/listing', $vars);
    }

    public function viewAdverse()
    {
        $data = Request::all();


        if (!isset($data['finance_type'])) {
            $data['finance_type'] = 'A';
        }


        $listing_type = 'A';
        $data['listing_type'] = 'A';

        $finance_type = $data['finance_type'];
        $pageName = "Compare Adverse Credit Lease Deals";
//        $data = $this->generateData($data, $data['finance_type']);
//        $data['finance_type'] = $finance_type;
//        $makes = $this->generateFilterSql($data, 'maker');
        $makes = $this->getMakes($listing_type);
        $rangeData = $this->generateFilterSql($data, 'range');
        foreach ($rangeData as $range) {
            $ranges[$range->maker][] = $range->range;
        }
        $trimData = $this->generateFilterSql($data, 'trim');
        foreach ($trimData as $trim) {
            $trims[$trim->range][] = $trim->trim;
        }
        $fuels = $this->generateFilterSql($data, 'fuel');

        $transmissions = $this->generateFilterSql($data, 'transmission');
        $bodystyles = $this->generateFilterSql($data, 'bodystyle');
        $doors = $this->generateFilterSql($data, 'doors');
        $enginesizes = $this->generateFilterSql($data, 'enginesize');
//        $monthly_payments = '';//$this->generateFilterSql($data, null, 'monthly_payment');
//        $deposit_values = $this->generateFilterSql($data, null, 'deposit_value');
//        $terms = $this->generateFilterSql($data, null, 'term');
//        $annual_mileages = $this->generateFilterSql($data, null, 'annual_mileage');

//        $extras = array();
        $data = $this->generateData($data, $data['finance_type']);
        $data['finance_type'] = $finance_type;

        $summaryCap = $data['summaryCap'];

        $vars = compact(
            'pageName',
            'summaryCap',
            'makes',
            'ranges',
            'trims',
            'fuels',
            'transmissions',
            'bodystyles',
            'doors',
            'enginesizes',
            'monthly_payments',
            'deposit_values',
            'terms',
            'annual_mileages',
            'finance_type',
            'listing_type'
        );


        return View('leasing_views/listing', $vars);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewResults(Request $request)
    {

        $debug = env('APP_DEBUG');

//        if (!request()->ajax()) {
//            if(!$debug) {
//                return redirect('/');
//            }
//        }
        $requestData = Request::all();


        if (!isset($data['finance_type'])) {
            $data['finance_type'] = 'P';
        }
        if(!isset($data['database'])) {
            $data['database'] = 'CARS';
        }

        $currentUrl = \Route::current()->uri;
        $currentUrl = explode('/', $currentUrl);
        switch ($currentUrl[0]) {
            case "hybrid":
                $data['finance_type'] = 'H';
                $data['listing_type'] = 'H';
                break;
            case "business":
                $data['finance_type'] = 'B';
                $data['listing_type'] = 'B';
                break;
            case "performance":
                $data['finance_type'] = 'PERFORMANCE';
                $data['listing_type'] = 'PERFORMANCE';
                break;
            case "adverse-credit":
                $data['finance_type'] = 'A';
                $data['listing_type'] = 'A';
                break;
            case "personal-vans":
                $data['finance_type'] = 'P';
                $data['listing_type'] = 'P';
                break;
            case "business-vans":
                $data['finance_type'] = 'B';
                $data['listing_type'] = 'B';
                break;
        }

        $data = $this->generateData($requestData, $data['finance_type']);

        $summaryCap = $data['summaryCap'];
        $makes = $data['makes'];
        $ranges = $data['ranges'];
        $trims = $data['trims'];
        $filtered = $data['filtered'];
        $filters = $data['filters'];

        $vars = compact(
            'summaryCap',
            'makes',
            'ranges',
            'trims',
            'filtered',
            'filters'
        );

        return View('partials/filter-results', $vars);
    }

    public function viewMobileFilters(Request $request)
    {


        $data = Request::all();
        $ranges = array();

        if($data['listing_type']== 'H') {
            $listing_type = 'H';
            $data['listing_type'] = 'H';
        }elseif($data['listing_type'] == 'A') {
            $listing_type = 'A';
            $data['listing_type'] = 'A';
        }elseif($data['listing_type'] == 'PERFORMANCE') {
            $listing_type = 'PERFORMANCE';
            $data['listing_type'] = 'PERFORMANCE';
        }else{
            $listing_type = 'P';
            $data['listing_type'] = 'P';
        }


        if (!isset($data['finance_type'])) {
            $data['finance_type'] = 'P';
        }
        $currentUrl = \Route::current()->uri;
        $currentUrl = explode('/', $currentUrl);

        switch ($currentUrl[0]) {
            case "hybrid":
                $data['finance_type'] = 'H';
                $data['listing_type'] = 'H';
                break;
            case "adverse-credit":
                $data['finance_type'] = 'P';
                $data['listing_type'] = 'A';
                break;
            case "business":
                $data['finance_type'] = 'B';
                $data['listing_type'] = 'B';
                break;
            case "PERFORMANCE":
                $data['finance_type'] = 'PERFORMANCE';
                $data['listing_type'] = 'PERFORMANCE';
                break;
        }

        if(!isset($data['database'])) {
            $data['database'] = 'CARS';
        }

        $database = $data['database'];

//        if($data['finance_type']== 'H') {
//            $listing_type = 'H';
//            $data['listing_type'] = 'H';
//        }elseif($data['finance_type'] == 'A') {
//            $listing_type = 'A';
//            $data['listing_type'] = 'A';
//        }elseif($data['finance_type'] == 'PERFORMANCE') {
//            $listing_type = 'PERFORMANCE';
//            $data['listing_type'] = 'PERFORMANCE';
//        }else{
//            $listing_type = 'P';
//            $data['listing_type'] = 'P';
//        }

        if(isset($data['finance_type_show']) && $data['finance_type_show'] != '') {
            $data['finance_type'] = $data['finance_type_show'];
        }


        $finance_type = $data['finance_type'];


//        $makes = $this->generateFilterSql($data, 'maker');
        $makes = $this->getMakes($listing_type, $database);

	if(isset($data['range']) && is_array($data['range'])) {
            foreach($data['range'] as $range) {

                if(isset($data['maker']) && is_array($data['maker']) && !in_array($this->getMakerFromRange($range), $data['maker'])) {

                    if (($key = array_search($range, $data['range'])) !== false) {
                        unset($data['range'][$key]);
                    }

                }

            }
        }

        $rangeData = $this->generateFilterSql($data, 'range');
        foreach ($rangeData as $range) {
            $ranges[$range->maker][] = $range->range;
        }

	if(isset($data['trim']) && is_array($data['trim'])) {
	
		foreach($data['trim'] as $trim) {
            $trimEx = explode('_', $trim);
	    if(isset($data['range'])) {
		    if(!in_array($trimEx[0], $data['range'])) {

                if (($key = array_search($trim, $data['trim'])) !== false) {
                    unset($data['trim'][$key]);
                }

		    }
            }
        }

	$selTrim = $data['trim'];
	        }else{
            $selTrim = array();
        }


        $trimData = $this->generateFilterSql($data, 'trim');

        foreach ($trimData as $trim) {
            $trims[$trim->range][] = $trim->trim;
        }

        $fuels = $this->generateFilterSql($data, 'fuel');
        $transmissions = $this->generateFilterSql($data, 'transmission');
        $doors = $this->generateFilterSql($data, 'doors');
        $bodystyles = $this->generateFilterSql($data, 'bodystyle');
        $enginesizes = $this->generateFilterSql($data, 'enginesize');
        $en = array();
        foreach($enginesizes as $engine)
        {
            $en[] = $engine->enginesize;
        }
        $enginesizes = $en;

//        $monthly_payments = $this->generateFilterSql($data, null, 'monthly_payment');
//        $deposit_values = $this->generateFilterSql($data, null, 'deposit_value');
//        $terms = $this->generateFilterSql($data, null, 'term');
//        $annual_mileages = $this->generateFilterSql($data, null, 'annual_mileage');

        $deposit_values = $this->fetchDepositValues();
        $terms = $this->fetchTerms();
        $annual_mileages = $this->fetchAnnualMiles();

        $vars = compact(
            'makes',
            'ranges',
            'trims',
            'fuels',
            'transmissions',
            'bodystyles',
            'doors',
            'enginesizes',
            'deposit_values',
            'terms',
            'annual_mileages',
            'finance_type',
            'deposit_values',
            'terms',
            'annual_mileages',
	        'listing_type',
	        'selTrim',
            'database'
        );

        return View('partials/mobile-filter-list', $vars);
    }

    public function viewFilters(Request $request)
    {
        $debug = env('APP_DEBUG');


//        if (!request()->ajax()) {
//            if(!$debug) {
//                return redirect('/');
//            }
//        }

        $data = Request::all();

        if($data['listing_type']== 'H') {
            $listing_type = 'H';
            $data['listing_type'] = 'H';
        }elseif($data['listing_type'] == 'A') {
            $listing_type = 'A';
            $data['listing_type'] = 'A';
        }elseif($data['listing_type'] == 'PERFORMANCE') {
            $listing_type = 'PERFORMANCE';
            $data['listing_type'] = 'PERFORMANCE';
        }else{
            $listing_type = 'P';
            $data['listing_type'] = 'P';
        }

        if(!isset($data['database'])) {
            $data['database'] = 'CARS';
        }

        $database = $data['database'];

        $ranges = array();

        if (!isset($data['finance_type'])) {
            $data['finance_type'] = 'P';
            $data['listing_type'] = 'P';
        }
        $currentUrl = \Route::current()->uri;
        $currentUrl = explode('/', $currentUrl);

        switch ($currentUrl[0]) {
            case "hybrid":
                $data['finance_type'] = 'H';
                $data['listing_type'] = 'H';
                break;
            case "adverse-credit":
                $data['finance_type'] = 'A';
                $data['listing_type'] = 'A';
                break;
            case "business":
                $data['finance_type'] = 'B';
                $data['listing_type'] = 'B';
                break;
            case "PERFORMANCE":
                $data['finance_type'] = 'PERFORMANCE';
                $data['listing_type'] = 'PERFORMANCE';
                break;
            case "business-vans":
                $data['finance_type'] = 'B';
                $data['listing_type'] = 'B';
                break;
            case "personal-vans":
                $data['finance_type'] = 'P';
                $data['listing_type'] = 'P';
                break;
        }

        if(isset($data['finance_type_show']) && $data['finance_type_show'] != '') {
            $data['finance_type'] = $data['finance_type_show'];
        }


        $finance_type = $data['finance_type'];

//        $makes = $this->generateFilterSql($data, 'maker');
        $makes = $this->getMakes($listing_type, $data['database']);

	if(isset($data['range']) && is_array($data['range'])) {
            foreach($data['range'] as $range) {

                if(isset($data['maker']) && is_array($data['maker']) && !in_array($this->getMakerFromRange($range), $data['maker'])) {

                    if (($key = array_search($range, $data['range'])) !== false) {
                        unset($data['range'][$key]);
                    }

                }

            }
	}

        $rangeData = $this->generateFilterSql($data, 'range');
        foreach ($rangeData as $range) {
            $ranges[$range->maker][] = $range->range;
        }
	
	if(isset($data['trim']) && is_array($data['trim'])) {
	foreach($data['trim'] as $trim) {
            $trimEx = explode('_', $trim);
	    if(isset($data['range'])) {
		    if(!in_array($trimEx[0], $data['range'])) {

                if (($key = array_search($trim, $data['trim'])) !== false) {
                    unset($data['trim'][$key]);
                }

		    }
            }
        }

	$selTrim = $data['trim'];
	}else{
		$selTrim = array();
	}

        $trimData = $this->generateFilterSql($data, 'trim');

        foreach ($trimData as $trim) {
            $trims[$trim->range][] = $trim->trim;
        }

        //        if(isset($data['trim'])) {
//            $index = 0;
//            foreach ($data['trim'] as $selectedTrim) {
//                $trims = explode("_", $selectedTrim);
//
//                $range = $trims[0];
//                $selTrim = $trims[1];
//
//                if (!array_key_exists($range, $trimss)) {
//                    unset($data['trim'][$index]);
//                }
//
//                $index++;
//            }
//            if (count($data['trim']) == 0) {
//                unset($data['trim']);
//            }
//        }

        $fuels = $this->generateFilterSql($data, 'fuel');
        $transmissions = $this->generateFilterSql($data, 'transmission');
        $doors = $this->generateFilterSql($data, 'doors');
        $bodystyles = $this->generateFilterSql($data, 'bodystyle');
        $enginesizes = $this->generateFilterSql($data, 'enginesize');
        $en = array();
        foreach($enginesizes as $engine)
        {
            $en[] = $engine->enginesize;
        }
        $enginesizes = $en;

        $monthly_payments = $this->generateFilterSql($data, null, 'monthly_payment');
//        $deposit_values = $this->generateFilterSql($data, null, 'deposit_value');
//        $terms = $this->generateFilterSql($data, null, 'term');
//        $annual_mileages = $this->generateFilterSql($data, null, 'annual_mileage');

        $deposit_values = $this->fetchDepositValues();
        $terms = $this->fetchTerms();
        $annual_mileages = $this->fetchAnnualMiles();

        $vars = compact(
            'makes',
            'ranges',
            'trims',
            'fuels',
            'transmissions',
            'bodystyles',
            'doors',
            'enginesizes',
            'monthly_payments',
            'deposit_values',
            'annual_mileages',
            'finance_type',
            'monthly_payments',
            'deposit_values',
            'terms',
            'annual_mileages',
            'listing_type',
            'selTrim',
            'database'
        );

        return View('partials/filter-list', $vars);

    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewCounter() {
	    return false;


        $data = Request::all();

        if($data['listing_type']== 'H') {
            $listing_type = 'H';
            $data['listing_type'] = 'H';
        }elseif($data['listing_type'] == 'A') {
            $listing_type = 'A';
            $data['listing_type'] = 'A';
        }elseif($data['listing_type'] == 'PERFORMANCE') {
            $listing_type = 'PERFORMANCE';
            $data['listing_type'] = 'PERFORMANCE';
        }else{
            $listing_type = 'P';
            $data['listing_type'] = 'P';
        }

        if(!isset($data['database'])) {
            $data['database'] = 'CARS';
        }

        $database = $data['database'];

        $ranges = array();

        if (!isset($data['finance_type'])) {
            $data['finance_type'] = 'P';
            $data['listing_type'] = 'P';
        }
        $currentUrl = \Route::current()->uri;
        $currentUrl = explode('/', $currentUrl);

        switch ($currentUrl[0]) {
            case "hybrid":
                $data['finance_type'] = 'H';
                $data['listing_type'] = 'H';
                break;
            case "adverse-credit":
                $data['finance_type'] = 'A';
                $data['listing_type'] = 'A';
                break;
            case "business":
            case "business-vans":
                $data['finance_type'] = 'B';
                $data['listing_type'] = 'B';
                break;
            case "PERFORMANCE":
                $data['finance_type'] = 'PERFORMANCE';
                $data['listing_type'] = 'PERFORMANCE';
                break;
            case "personal-vans":
                $data['finance_type'] = 'P';
                $data['listing_type'] = 'P';
                break;
        }

        if(isset($data['finance_type_show']) && $data['finance_type_show'] != '') {
            $data['finance_type'] = $data['finance_type_show'];
        }
        $financeType = $data['finance_type'];
        $counter = $this->getResults($data, $financeType, 1);

        if(isset($data['page'])) {
            $page = $data['page'];
        }else{
            $page = 1;
        }

        $total = number_format($counter->count());

        $vars = compact(
            'total'
        );

        return View('partials/counter', $vars);
    }

    /**
     * @param $data
     * @param null $returnCapField
     * @param null $returnPriceField
     * @return array
     */
    public function generateFilterSql($data, $returnCapField = null, $returnPriceField = null)
    {

        $selectedMakers = array();

        // SET TRIMS FOR PERFORMANCE
        if (isset($data['listing_type']) && $data['listing_type'] == 'PERFORMANCE') {
            $data['trim'] = array(
                'S3',
                'S1',
                'S1 Competition',
                'S3 Black Edition',
                'S4',
                'S5',
                'S5 Black Edition',
                'S6',
                'S6 Black Edition',
                'S7',
                'S7 Black Edition',
                'S8',
                'S8 Plus',
                'SQ5',
                'SQ7',
                'TTS',
                'TTS Black Edition',
                'Performance',
                'R',
                'Protronic Silver Edition',
                'M',
                'RS',
                'ST',
                'ST-2',
                'ST-3',
                'ST-200',
                'ST-1',
                'Type R',
                'Type R GT',
                'Type R Black Edition',
                '250 AMG',
                '250 AMG WhiteArt',
                'AMG',
                'AMG Yellow Night Edition',
                'AMG S',
                'S Edition 1',
                'S Edition 1 Motorsport',
                'AMG S Edition 1',
                'GT S',
                'GT R',
                'GT C',
                'John Cooper Works',
                'Nismo RS',
                'Nismo',
                'GTi',
                'GTi Prestige',
                'GTi by Peugeot Sport',
                'RenaultSport Trophy Nav',
                'Renaultsport Nav',
                'GT Nav',
                'Renaultsport 275 Cup S',
                'Renaultsport Nav 275',
                'Renaultsport 275',
                'Cupra',
                'Cupra Black',
                'vRS',
                'vRS 230',
                'VXR',
                'VXR SuperSport Nav',
                'R',
                'GTI Clubsport 40',
                'GTS',
                'M140i',
                'M235i',
                'M240i',
                'M2',
                'M3',
                'M3 30 Jahre Edition',
                'M4',
                'M5',
                'M6',
                'M',
            );
        }

        if(isset($data['range'])) {
            $counter = 0;
            foreach($data['range'] as $rangeData) {
                $thisMaker = $this->getMakerFromRange($rangeData);

		if(isset($data['maker'])) {
			if(!in_array($thisMaker, $data['maker'])) {
                    unset($data['range'][$counter]);
			}
		}else{
			unset($data['range'][$counter]);
		}

                $counter ++;
            }

        }

        // Work around for deselecting trims when maker has been changed
        if (isset($data['listing_type']) && $data['listing_type'] == 'PERFORMANCE') {
            // TODO add some checks for performance too
        }else {
            if (isset($data['trim'])) {
                $counter = 0;
                foreach ($data['trim'] as $trimData) {
                    $thisMakers = $this->getMakersFromTrim($trimData);

                    if (isset($data['maker'])) {
                        foreach ($data['maker'] as $maker) {
                            if (in_array($maker, $thisMakers)) {
                                $counter++;
                            }
                        }

                        if ($counter == 0) {
                            unset($data['trim'][$counter]);
                        }

                    }
                }

            }
        }

        if(empty($data['range'])) {
            unset($data['range']);
        }
        if(empty($data['trim'])) {
            unset($data['trim']);
        }

        $capWhere = "SELECT cap_id FROM capreference WHERE ";

        $capWhere .= " `database` = '" . $data['database'] . "'";

        if ($returnCapField != 'maker') {
            if (isset($data['maker'])) {
                $string = '';
                foreach ($data['maker'] as $maker) {
                    $selectedMakers[$maker] = 0;
                    $string .= "'" . $maker . "', ";
                }
                $string = substr($string, 0, strlen($string) - 2);
                $capWhere .= " AND maker IN (" . $string . ")";

            }else{
                unset($data['range']);
                if($data['listing_type']!='PERFORMANCE') {
                    unset($data['trim']);
                }
            }
        }else{
            if(isset($data['maker'])) {
                foreach ($data['maker'] as $maker) {
                    $selectedMakers[$maker] = 0;
                }
            }
        }

        if ($returnCapField != 'range') {
            if (isset($data['range'])) {
                $string = '';
                foreach ($data['range'] as $range) {

                    $selectedMaker = $this->getMakerFromRange($range);

                    if(!array_key_exists($selectedMaker, $data['maker']))    {
                        if (($key = array_search($range, $data['range'])) !== false) {
                            unset($data['range'][$key]);
                            continue;
                        }
                    }


                    if (array_key_exists($selectedMaker, $selectedMakers)) {
                        if (isset($selectedMakers[$selectedMaker])) {
                            $selectedMakers[$selectedMaker] = $selectedMakers[$selectedMaker] + 1;
                        }
                        $string .= "'" . $range . "', ";
                    }
                }

                if (!empty($selectedMakers)) {
                    foreach ($selectedMakers as $make => $maker) {
                        if ($maker == 0) {
                            foreach ($this->getRanges($make) as $range) {
                                $string .= "'" . $range->range . "', ";
                            }
                        }
                    }
                    $string = substr($string, 0, strlen($string) - 2);
                    if($string) {
                        $capWhere .= " AND `range` IN (" . $string . ")";
                    }
                }
            }

            if ($returnCapField != 'trim') {
                if (isset($data['trim'])) {
                    if ($data['listing_type'] == 'PERFORMANCE') {

                        foreach ($data['trim'] as $trim) {
                            if(strpos($trim, '_')) {
                                $explode = explode('_', $trim);
                                $trim = $explode[1];
                            }
                            $capWhere .= " OR trim = '" . $trim . "'";
                        }
                    } else {

                        $string = '';
                        foreach ($data['trim'] as $trim) {
                            if(strpos($trim, '_')) {
                                $explode = explode('_', $trim);
                                $trim = $explode[1];
                            }
                            $string .= "'" . $trim . "', ";
                        }
                        $string = substr($string, 0, strlen($string) - 2);
                        $capWhere .= " AND trim IN (" . $string . ")";
                    }
                }
            }else{
                    // If we are on performance listing page make sure we force the trims into the query
                    if ($data['listing_type'] == 'PERFORMANCE') {
                        $capWhere .= 'AND (';
                        $trimwhere='';
                        foreach ($data['trim'] as $trim) {
                            if(strpos($trim, '_')) {
                                $explode = explode('_', $trim);
                                $trim = $explode[1];
                            }
                            $trimwhere .= " OR trim = '" . $trim . "'";
                        }
                        $trimwhere = substr($trimwhere, 4, strlen($trimwhere));
                        $capWhere .= $trimwhere;
                        $capWhere .= ')';
                    }

            }

        }else{
            // If we are on performance listing page make sure we force the trims into the query
            if ($data['listing_type'] == 'PERFORMANCE') {
                $capWhere .= 'AND (';
                $trimwhere='';
                foreach ($data['trim'] as $trim) {
                    if(strpos($trim, '_')) {
                        $explode = explode('_', $trim);
                        $trim = $explode[1];
                    }
                    $trimwhere .= " OR trim = '" . $trim . "'";
                }
                $trimwhere = substr($trimwhere, 4, strlen($trimwhere));
                $capWhere .= $trimwhere;
                $capWhere .= ')';
            }

        }


            if (isset($data['listing_type']) && $data['listing_type'] == 'H') {

                if(!isset($data['fuel'])) {
                    $fuelType = array(
                        'Petrol Electric Hybrid',
                        'Petrol PlugIn Elec Hybrid',
                        'Diesel PlugIn Elec Hybrid',
                        'Diesel Electric Hybrid',
                        'PlugIn Elec Hybrid',
                        'Electric',
                    );
                    $string = '';
                    foreach ($fuelType as $fuel) {
                        $string .= "'" . $fuel . "', ";
                    }
                    $string = substr($string, 0, strlen($string) - 2);
                    $capWhere .= " AND fuel IN (" . $string . ")";
                }else{
                    $string = '';
                    foreach ($data['fuel'] as $fuel) {
                        $string .= "'" . $fuel . "', ";
                    }

                    $string = substr($string, 0, strlen($string) - 2);
                    $capWhere .= " AND fuel IN (" . $string . ")";
                }
            } else {
                if ($returnCapField != 'fuel') {
                    if (isset($data['fuel'])) {
                        $string = '';
                        foreach ($data['fuel'] as $fuel) {
                            $string .= "'" . $fuel . "', ";
                        }

                        $string = substr($string, 0, strlen($string) - 2);
                        $capWhere .= " AND fuel IN (" . $string . ")";
                    }
                }

            }
        if ($returnCapField != 'transmission') {
            if (isset($data['transmission'])) {
                $string = '';
                foreach ($data['transmission'] as $transmission) {
                    $string .= "'" . $transmission . "', ";
                }
                $string = substr($string, 0, strlen($string) - 2);
                $capWhere .= " AND transmission IN (" . $string . ")";
            }
        }
        if ($returnCapField != 'bodystyle') {
            if (isset($data['bodystyle'])) {
                $string = '';
                foreach ($data['bodystyle'] as $bodystyle) {
                    $string .= "'" . $bodystyle . "', ";
                }
                $string = substr($string, 0, strlen($string) - 2);
                $capWhere .= " AND bodystyle IN (" . $string . ")";
            }
        }
        if ($returnCapField != 'doors') {
            if (isset($data['doors'])) {
                if (is_array($data['doors'])) {
                    $string = '';
                    foreach ($data['doors'] as $door) {
                        $string .= "'" . $door . "', ";
                    }
                    $string = substr($string, 0, strlen($string) - 2);
                    $capWhere .= " AND doors IN (" . $string . ")";
                } else {
                    $capWhere .= " AND doors = " . $data['doors'] . "";
                }
            }
        }

        if ($returnCapField != 'engineSize') {
            if (isset($data['engineSize'])) {
                if (is_array($data['engineSize'])) {
                    foreach ($data['engineSize'] as $engine) {
                        if ($engine == '0') {
                            $engineStart = 0;
                            $engineFinish = 999999;
                        } else {
                            $engineSize = explode('-', $engine);
                            $engineStart = $engineSize[0];
                            if ($engineStart == "5.0+") {
                                $engineStart = "5.0";
                                $engineFinish = 99999;
                            } else {
                                if(count($engineSize) == 1) {
                                    $engineFinish = 99999;
                                }else{
                                    $engineFinish = $engineSize[1];
                                }
                            }
                        }
                        $capWhere .= " AND enginesize BETWEEN " . $engineStart . " AND " . $engineFinish . "";
                    }
                } else {
                    if ($data['engineSize'] == '0') {
                        $engineStart = 0;
                        $engineFinish = 999999;
                    } else {
                        $engineSize = explode('-', $data['engineSize']);
                        $engineStart = $engineSize[0];
                        if ($engineStart == "5.0+") {
                            $engineStart = "5.0";
                            $engineFinish = 99999;
                        } else {
                            if(count($engineSize) == 1) {
                                $engineFinish = 99999;
                            }else{
                                $engineFinish = $engineSize[1];
                            }
                        }
                    }
                    $capWhere .= " AND enginesize BETWEEN " . $engineStart . " AND " . $engineFinish . "";
                }
            }
        }


            // PRICE TABLE WHERE CLAUSES
            if(isset($data['database']) && $data['database']=='LCV') {
                $priceWhere = "SELECT cap_id FROM van_pricestable WHERE cap_id IN (" . $capWhere . ")";
            }else{
                $priceWhere = "SELECT cap_id FROM pricestable WHERE cap_id IN (" . $capWhere . ")";
            }


// Adverse Credit
//            if (isset($data['listing_type']) && $data['listing_type'] == "A") {
//                $priceWhere .= " AND adverse_credit = 1";
//            } else {
//                $priceWhere .= " AND adverse_credit IS NULL ";
//            }

            if (isset($data['pricemin'])) {
                $pricemin = $data['pricemin'];
                if (!isset($data['pricemax']) || $data['pricemax'] == 0) {
                    $pricemax = 9999999;
                } else {
                    $pricemax = $data['pricemax'];
                }
                if($pricemin == 0 && $pricemax == 99999) {
                    // No need to include payment details if they're not set

                }else {
    //                $priceWhere .= " AND monthly_payment BETWEEN " . $pricemin . " AND " . $pricemax . "";/**/
                    $priceWhere .= " AND monthly_payment > " . $pricemin . " AND monthly_payment < ". $pricemax . "";
                }
            }


        if(isset($data['search'])) {
            $searchString = explode(' ', $data['search']);
            foreach ($searchString as $k=>$v) {

                if($v == 'class') {
                    $mercs = " AND `range` = '" . $data['search'] . "'";
//                    unset($searchString[$k]);
//                    $searchString[$k-1] = $searchString[$k-1] . ' class';
                }
            }
            $searchstr = $data['search'];
            if(isset($mercs)) {
                $match = $mercs;
            }else {
//                foreach ($searchString as $string) {
//                    $match = " AND MATCH (maker,model,derivative) AGAINST ('\"$string\"' IN BOOLEAN MODE)";
//                }
                $match = " AND MATCH (maker,model,derivative) AGAINST ('\" $searchstr \"' IN BOOLEAN MODE)";
//              $match = " AND CONCAT(capreference.maker,capreference.model,capreference.derivative) LIKE '%" . str_replace(' ', '%', $data['search']) . "%'";
            }

        }else{
            $match = '';
        }

        if ($returnPriceField) {

            $where = $this->fetchPriceFieldFilters($data, $priceWhere, $returnPriceField);
//            $where = "SELECT DISTINCT(`" . $returnPriceField . "`)
//            FROM pricestable WHERE cap_id IN (" . $priceWhere . ")";
//
//            $where .= "ORDER BY `" . $returnPriceField . "`";
        } else {
            if ($returnCapField == "range") {
                $where = "SELECT DISTINCT(`" . $returnCapField . "`), maker
                FROM capreference WHERE cap_id IN (" . $priceWhere . ")";
                $where .= $match;
                $where .= " AND `database` = '" . $data['database'] . "'";
                $where .= "ORDER BY `range`";
            } elseif ($returnCapField == "trim") {
                $where = "SELECT DISTINCT(`" . $returnCapField . "`), `range`
                FROM capreference WHERE cap_id IN (" . $priceWhere . ")";
                $where .= $match;
                $where .= " AND `database` = '" . $data['database'] . "'";
                $where .= "ORDER BY `range`, `trim`";
            } else {
                $where = "SELECT DISTINCT(`" . $returnCapField . "`)
                FROM capreference WHERE cap_id IN (" . $priceWhere . ")";
                $where .= $match;
                $where .= " AND `database` = '" . $data['database'] . "'";
                $where .= "ORDER BY `".$returnCapField."`";
            }
        }
        $results = DB::select(DB::raw($where));
        // Order results
        if ($returnPriceField) {
            $results = $this->processPriceFields($results);
        }

        return $results;
    }


    private function processPriceFields($results) {
        sort($results);

        return $results;
    }


    /**
     * @param $data
     * @param $where
     * @param $returnPriceField
     * @return string|string[]
     *
     * Deal with the price fields in the filters as the queries need to be slightly different to be able to hit the DB indexing
     */
    private function fetchPriceFieldFilters($data, $where, $returnPriceField) {

        if(isset($data['database']) && $data['database']=='LCV') {
            $pricestable = 'van_pricestable';
        }else{
            $pricestable = 'pricestable';
        }

        if($returnPriceField == "annual_mileage") {

            $where = str_replace("SELECT cap_id FROM $pricestable WHERE", "SELECT distinct(annual_mileage) FROM $pricestable WHERE", $where);
            $where = str_replace("ORDER BY annual_mileage", "", $where);

//            $where = $where . " ORDER BY annual_mileage";
        }

        if($returnPriceField == "deposit_value") {
            $where = str_replace("SELECT cap_id FROM $pricestable WHERE", "SELECT DISTINCT(deposit_value) FROM $pricestable WHERE", $where);
            $where = str_replace("ORDER BY deposit_value", "", $where);

//            $where = $where . " ORDER BY deposit_value";

        }

        if($returnPriceField == "term") {
            $where = str_replace("SELECT cap_id FROM $pricestable WHERE", "SELECT DISTINCT(term) FROM $pricestable WHERE", $where);
            $where = str_replace("ORDER BY term", "", $where);

//            $where = $where . " ORDER BY term";

        }

        return $where;

    }

    /**
     * @param $request
     * @param string $financeType
     * @param string $redirectTo
     * @return array|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function generateData($data, $financeType = "p", $redirectTo = "/personal", $fuelType = "")
    {

//        $data = serialize($data);
//        $data = str_replace('+', ' ', $data);
//        $data = unserialize($data);


        if ($fuelType != '') {
            $data['fuel'] = $fuelType;
        }

        // Check query filters, if nothing is selected then redirect to clean url ($redirectTo) instead
        $redirect = 0;

        if (isset($data) && count($data) > 0) {
            foreach ($data as $d) {
                if (!empty($d)) {
                    $redirect++;
                }
            }
            if ($redirect == 0) {
                return redirect($redirectTo);
            }
        }

        // Build up an array of selected filters
        $filtered = array();
        $filtered['fuel'] = array();
        $filtered['transmission'] = array();
        $filtered['bodystyle'] = array();
        $filtered['deposit_value'] = array();
        $filtered['term'] = array();
        $filtered['annual_mileage'] = array();
        $filtered['in_stock'] = array();
        $ranges = array();
        $trims = array();
        $summaryCap = array();

        // Logic for filtering system
        // get makes for initial page load
        if(isset($data['listing_type'])) {
            $listing_type = $data['listing_type'];
        }else {
            if(isset($data['finance_type'])) {
                $listing_type = $data['finance_type'];
            }else{
                $listing_type = $financeType;
            }
        }


        // Get the resultset
        $summaryCap = $this->getResults($data, $financeType);
        $count = 0;
        foreach($summaryCap as $deal) {
            $cdnUrl = env('DO_IMAGE_PATH') . $deal->database . '/';
            if($deal->imageid != 0) {
                $vehicleImage = $this->fetchVehicleDefaultImage($deal->cap_id, $deal->database);
                if(is_null($vehicleImage)) {
                    $summaryCap[$count]->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
                }

                if (isset($vehicleImage->image_name) && !is_null($vehicleImage->image_name)) {
                    $imageUrl = $cdnUrl . 'big/' . $vehicleImage->image_name;
                    if (@getimagesize($imageUrl)) {
                        $summaryCap[$count]->imageid = $imageUrl;
                    } else {
                        $summaryCap[$count]->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
                    }
                }
            }else{
                $summaryCap[$count]->imageid = 'https://www.autoleasecompare.com/images/comingsooncar.jpg';
            }
            $count = $count + 1;
        }

        $makes = $this->getMakes($listing_type);

        // if there is a make selected get model for that make
        if (isset($data['maker'])) {
            foreach ($data['maker'] as $maker) {
                $ranges[$maker] = $this->getRanges($maker);
            }
        }

        // Get a list of trims
        if (isset($data['range'])) {
            foreach ($data['range'] as $range) {
                $trims[$range] = $this->getTrims($data['range']);
            }
        }

        $filters = $data;

        $returnArr = array(
            'summaryCap' => $summaryCap,
            'makes' => $makes,
            'ranges' => $ranges,
            'trims' => $trims,
            'filtered' => $filtered,
            'filters' => $filters);
        return $returnArr;
    }

    /**
     * @param $data
     * @return mixed
     */
    private function getResults($data, $finance_type, $returnCounter = null)
    {
        $data['finance_type'] = $finance_type;

        if(isset($data['listing_type'])) {
            $listing_type = $data['listing_type'];
        }else {
            if(isset($data['finance_type'])) {
                $listing_type = $data['finance_type'];
            }else{
                $listing_type = $financeType;
            }
        }

        if(!isset($data['database'])) {
            $data['database'] = 'CARS';
        }

        if (isset($data['listing_type']) && $data['listing_type'] == 'H') {
            if(!isset($data['fuel']))
            {
                $data['fuel'] = array(
                    'Petrol Electric Hybrid',
                    'Petrol PlugIn Elec Hybrid',
                    'Diesel PlugIn Elec Hybrid',
                    'Diesel Electric Hybrid',
                    'PlugIn Elec Hybrid',
                    'Electric',
                );
            }

        }
        // SET TRIMS FOR PERFORMANCE
        if ($data['finance_type'] == 'PERFORMANCE') {
            $data['trim'] = array(
                'S3',
                'S1',
                'S1 Competition',
                'S3 Black Edition',
                'S4',
                'S5',
                'S5 Black Edition',
                'S6',
                'S6 Black Edition',
                'S7',
                'S7 Black Edition',
                'S8',
                'S8 Plus',
                'SQ5',
                'SQ7',
                'TTS',
                'TTS Black Edition',
                'Performance',
                'R',
                'Protronic Silver Edition',
                'M',
                'RS',
                'ST',
                'ST-2',
                'ST-3',
                'ST-200',
                'ST-1',
                'Type R',
                'Type R GT',
                'Type R Black Edition',
                '250 AMG',
                '250 AMG WhiteArt',
                'AMG',
                'AMG Yellow Night Edition',
                'AMG S',
                'S Edition 1',
                'S Edition 1 Motorsport',
                'AMG S Edition 1',
                'GT S',
                'GT R',
                'GT C',
                'John Cooper Works',
                'Nismo RS',
                'Nismo',
                'GTi',
                'GTi Prestige',
                'GTi by Peugeot Sport',
                'RenaultSport Trophy Nav',
                'Renaultsport Nav',
                'GT Nav',
                'Renaultsport 275 Cup S',
                'Renaultsport Nav 275',
                'Renaultsport 275',
                'Cupra',
                'Cupra Black',
                'vRS',
                'vRS 230',
                'VXR',
                'VXR SuperSport Nav',
                'R',
                'GTI Clubsport 40',
                'GTS',
                'M140i',
                'M235i',
                'M240i',
                'M2',
                'M3',
                'M3 30 Jahre Edition',
                'M4',
                'M5',
                'M6',
                'M',
            );
        }

        if(isset($data['search'])) {
            $searchedString = $data['search'];
            $searchString = explode(' ', $data['search']);
        }else{
            $searchString = '';
        }

        if(!isset($data['maker'])) {
            unset($data['range']);
            if(isset($data['listing_type']) && $data['listing_type']!='PERFORMANCE') {
                unset($data['trim']);
            }
        }

        if(isset($data['range'])) {
            foreach ($data['range'] as $range) {

                $selectedMaker = $this->getMakerFromRange($range, $data);
                if (!in_array($selectedMaker, $data['maker'])) {
                    if (($key = array_search($range, $data['range'])) !== false) {
                        unset($data['range'][$key]);
                        continue;
                    }
                }
            }
        }

        if(empty($data['range'])) {
            unset($data['range']);
        }

        // Work around for deselecting trims when maker has been changed
        if(isset($data['trim'])) {
            $counter = 0;
            foreach($data['trim'] as $trimData) {
                $thisMakers = $this->getMakersFromTrim($trimData);

                if(isset($data['maker'])) {
                    foreach($data['maker'] as $maker) {
                        if(is_array($thisMakers) && in_array($maker, $thisMakers)) {
                            $counter++;
                        }
                    }

                    if($counter == 0) {
                        unset($data['trim'][$counter]);
                    }

                }
            }

        }

        if(empty($data['range'])) {
            unset($data['range']);
        }
        if(empty($data['trim'])) {
            unset($data['trim']);
        }




        if(isset($data['database']) && $data['database']=='LCV') {
            $dbTable = 'van_pricestable';
        }else{
            $dbTable = 'pricestable';
        }

//        $seconds = 21600;
//        $summaryCap1 = Cache::remember('listing_get_results', $seconds, function () use ($dbTable, $data, $finance_type, $searchString) {


//        return DB::table($dbTable)
            $summaryCap1 = DB::table($dbTable)
//                ->select(DB::raw('SUM((pricestable.document_fee) + (pricestable.deposit_value)) AS initial_payment'))
            ->select(
                $dbTable . '.id',
                $dbTable . '.deal_id',
                $dbTable . '.cap_id',
                $dbTable . '.finance_type',
                $dbTable . '.deposit_value',
                $dbTable . '.term',
                $dbTable . '.monthly_payment',
                $dbTable . '.annual_mileage',
                $dbTable . '.document_fee',
                $dbTable . '.in_stock',
                $dbTable . '.deposit_months',
                $dbTable . '.average_cost',
                $dbTable . '.total_cost',
                $dbTable . '.featured',
                $dbTable . '.total_initial_cost',
                $dbTable . '.insurance',
                'capreference.*'
            )
                ->join('capreference', $dbTable . '.cap_id', '=', 'capreference.cap_id')
                ->join('users', $dbTable . '.deal_id', '=', 'users.name')
//            ->whereNotNull('pricestable.monthly_payment')


            ->where('database', '=', $data['database'])
            ->when(isset($finance_type), function ($query) use ($finance_type, $data) {
                $adverse = 0;
                if ($finance_type == 'H') {
                    $finance_type = "P";
                }
                if ($finance_type == 'PERFORMANCE') {
                    $finance_type = "P";
                }
                if ($finance_type == 'A') {
                    $finance_type = "P";
                    $adverse = 1;
                }

                if(isset($data['finance_type_show']) && $data['finance_type_show'] != '') {
                    $finance_type = $data['finance_type_show'];
                }

                if($adverse == 0) {
                    return $query->where('finance_type', '=', $finance_type);
//                        ->wherenull('adverse_credit');
//                        ->whereraw('adverse_credit IS NULL ');
                }else{
                    return $query->where('finance_type', '=', $finance_type);
                }
            })
            ->when(isset($data['maker']), function ($query) use ($data) {
                return $query->whereIn('maker', $data['maker']);
            })
            ->when(isset($data['range']), function ($query) use ($data) {

                // Check to see if all of the selected manufacturers have
                // got ranges selected, if they haven't then include all
                // ranges for that manufacturer

                $tempMaker = $data['maker'];

                foreach($data['range'] as $range) {
                    $selectedMaker = $this->getMakerFromRange($range, $data);

                    if(in_array($selectedMaker, $tempMaker)) {
                        $key = array_search($selectedMaker, $tempMaker);
                        unset($tempMaker[$key]);
                    }
                }
                foreach($tempMaker as $make) {
                    $ranges = $this->getRanges($make, true);
                    foreach($ranges as $range) {
                        $data['range'][] = $range->range;
                    }
                }

                return $query->whereIn('range', $data['range']);
            })
            ->when(isset($data['trim']), function ($query) use ($data) {
                if ($data['listing_type'] == 'PERFORMANCE') {

                    $where = '(';
                    foreach ($data['trim'] as $trim) {
                        if (strpos($trim, '_')) {
                            $explode = explode('_', $trim);
                            $trim = $explode[1];
                        }
                        $where .= "trim = '" . $trim . "' OR ";
                    }

                    $where = substr($where, 0, strlen($where)-4) . ')';

                    $query->whereraw($where);

                    return $query;
                }else {


                    $trims = array();
                    if(isset($data['range'])) {
                        $tempRanges = $data['range'];
                    }else{
                        $tempRanges = array();
                    }


                    foreach($data['trim'] as $trim) {
                        if (strpos($trim, '_')) {
                            $explode = explode('_', $trim);
                            $trims[] = $explode[1];
                            if(in_array($explode[0], $tempRanges)) {
                                $key = array_search($explode[0], $tempRanges);
                                unset($tempRanges[$key]);
                            }
                        }
                    }


                        foreach ($tempRanges as $range) {
                            foreach ($this->getTrims($range) as $additionalTrims) {
                                $trims[] = $additionalTrims->trim;
                            }
                        }


                    return $query->whereIn('trim', $trims);
                }
            })
            ->when(isset($data['term']), function ($query) use ($data) {
                    return $query->whereIn('term', $data['term']);
            })
            ->when(isset($data['fuel']), function ($query) use ($data) {
                return $query->whereIn('fuel', $data['fuel']);
            })
            ->when(isset($data['transmission']), function ($query) use ($data) {
                return $query->whereIn('transmission', $data['transmission']);
            })
            ->when(isset($data['bodystyle']), function ($query) use ($data) {
                return $query->whereIn('bodystyle', $data['bodystyle']);
            })

            ->when(isset($data['doors']), function ($query) use ($data) {
                return $query->where('doors', '=', $data['doors']);
            })
            ->when(isset($data['engineSize']), function ($query) use ($data) {
                if ($data['engineSize'] == '0') {
                    return false;
                } else {
                    if(is_array($data['engineSize'])) {
                        foreach($data['engineSize'] as $engine) {

                            $engineSize = explode('-', $engine);
                            $engineStart = $engineSize[0];
                            if ($engineStart == "5.0+") {
                                $engineStart = "5.0";
                                $engineFinish = 99999;
                            } else {
                                if(count($engineSize) == 1) {
                                    $engineFinish = 99999;
                                }else{
                                    $engineFinish = $engineSize[1];
                                }
                            }

                            $query->whereBetween('engineSize', [$engineStart, $engineFinish]);

                        }
                    }else {
                        $engineSize = explode('-', $data['engineSize']);
                        $engineStart = $engineSize[0];

                        if ($engineStart == "5.0+") {
                            $engineStart = "5.0";
                            $engineFinish = 99999;
                        } else {
                            if(count($engineSize) == 1) {
                                $engineFinish = 99999;
                            }else{
                                $engineFinish = $engineSize[1];
                            }
                        }

                        return $query->whereBetween('engineSize', [$engineStart, $engineFinish]);
                    }
                }
            })
            ->when(isset($data['pricemin']), function ($query) use ($data) {
                if (isset($data['pricemax']) && $data['pricemax'] == 0) {
                    $data['pricemax'] = 99999;
                }
                if(!isset($data['pricemax'])) {
                    $data['pricemax'] = 99999;
                }

                if(isset($data['pricemin']) && $data['pricemin']==0) {
                    unset($data['pricemin']);
                }
                if(isset($data['pricemax']) && $data['pricemax']==99999) {
                    unset($data['pricemax']);
                }

                if(isset($data['pricemin']) && !isset($data['pricemax'])) {
                    return $query->where('monthly_payment', '>', $data['pricemin']);
                }elseif(isset($data['pricemax']) && !isset($data['pricemin'])) {
                    return $query->where('monthly_payment', '<', $data['pricemax']);
                }elseif(isset($data['pricemin']) && isset($data['pricemax'])) {
                    return $query->where('monthly_payment', '>', $data['pricemin'])
                        ->where('monthly_payment', '<', $data['pricemax']);
                }

//                return $query->whereBetween('monthly_payment', [$data['pricemin'], $data['pricemax']]);
            })
            ->when(isset($data['annual_mileage']), function ($query) use ($data) {
                return $query->whereIn('annual_mileage', $data['annual_mileage']);
            })
            ->when(isset($data['deposit_value']), function ($query) use ($data) {
                return $query->whereIn('deposit_value', $data['deposit_value']);
            })
            ->when(isset($data['in_stock']), function ($query) use ($data) {
                if ($data['in_stock'] == 'on') {
                    $data['in_stock'] = 1;
                    return $query->where('in_stock', '=', $data['in_stock']);
                }
            })

            ->when(isset($data['specs']), function($query) use ($data) {
                if(is_array($data['specs'])) {
                    foreach($data['specs'] as $spec) {
                        return $query->where($spec, '=', 1);
                    }
                }
            })

            ->when(isset($data['android_apple']), function ($query) use ($data) {
                return $query->where('android_apple', '=', 1);
            })
            ->when(isset($data['bluetooth']), function ($query) use ($data) {
                return $query->where('bluetooth', '=', 1);
            })
            ->when(isset($data['parking_camera']), function ($query) use ($data) {
                return $query->where('parking_camera', '=', 1);
            })
            ->when(isset($data['dab']), function ($query) use ($data) {
                return $query->where('dab', '=', 1);
            })
            ->when(isset($data['parking_sensors']), function ($query) use ($data) {
                return $query->where('parking_sensors', '=', 1);
            })
            ->when(isset($data['satnav']), function ($query) use ($data) {
                return $query->where('sat_nav', '=', 1);
            })
            ->when(isset($data['wireless_charge']), function ($query) use ($data) {
                return $query->where('wireless_charge', '=', 1);
            })
            ->when(isset($data['alloys']), function ($query) use ($data) {
                return $query->where('alloys', '=', 1);
            })
            ->when(isset($data['climate_control']), function ($query) use ($data) {
                return $query->where('climate_control', '=', 1);
            })
            ->when(isset($data['cruise_control']), function ($query) use ($data) {
                return $query->where('cruise_control', '=', 1);
            })
            ->when(isset($data['heated_seats']), function ($query) use ($data) {
                return $query->where('heated_seats', '=', 1);
            })
            ->when(isset($data['leather_seats']), function ($query) use ($data) {
                return $query->where('leather_seats', '=', 1);
            })
            ->when(isset($data['panoramic_roof']), function ($query) use ($data) {
                return $query->where('panoramic_roof', '=', 1);
            })
                ->when(isset($data['sunroof']), function ($query) use ($data) {
                    return $query->where('sunroof', '=', 1);
                })
                ->when(isset($data['deal_id']), function ($query) use ($data) {
                    return $query->where('deal_id', '=', $data['deal_id']);
                })
//            ->when($data['listing_type'] == 'A', function ($query) use ($data) {
//                return $query->where('adverse_credit', '=', 1);
//            })
            ->when(isset($data['sort']), function ($query) use ($data) {
                if($data['sort'] == 'highest_price') {
                    return $query->orderby('monthly_payment', 'desc');
                }elseif($data['sort'] == 'deposit_months') {
                    return $query->orderby('total_initial_cost');
                }else {
                    return $query->orderby($data['sort']);
                }
            })
            ->when(!isset($data['sort']), function ($query) use ($data) {
                return $query->orderby('monthly_payment');
            })

            ->when(isset($searchString), function ($query) use ($data, $searchString) {
                if(isset($searchString) && is_array($searchString)) {
                    $searchStringImploded = implode(' ', $searchString);
                }
                if(is_array($searchString)) {
                    // manipulate input data

                    foreach ($searchString as $k=>$v) {
                        if($v == 'class') {
                            $mercs = 1;
                        }
                    }

                    if(isset($mercs) && $mercs == 1) {
                        $query->whereRaw(" AND `range` = '" . $data['search'] . "'");
                    }else{
//                        foreach ($searchString as $string) {
//                            $query->whereRaw("MATCH (maker,model,derivative) AGAINST ('\"$string\"' IN BOOLEAN MODE)");
//                        }
                            $query->whereRaw("MATCH (maker,model,derivative) AGAINST ('\"$searchStringImploded\"' IN BOOLEAN MODE)");
                    }
                }
            });

//        });

        if($returnCounter) {

        }else{
            $summaryCap1 = $summaryCap1->limit(15)
                ->simplePaginate(15);
        }

        $summaryCap = ($summaryCap1);
//        dd(DB::getQueryLog());
        return $summaryCap;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function getMakes($listing_type, $database = 'CARS')
    {
        $data['database'] = $database;
        $data['trims'] = array(
            'S3',
            'S1',
            'S1 Competition',
            'S3 Black Edition',
            'S4',
            'S5',
            'S5 Black Edition',
            'S6',
            'S6 Black Edition',
            'S7',
            'S7 Black Edition',
            'S8',
            'S8 Plus',
            'SQ5',
            'SQ7',
            'TTS',
            'TTS Black Edition',
            'Performance',
            'R',
            'Protronic Silver Edition',
            'M',
            'RS',
            'ST',
            'ST-2',
            'ST-3',
            'ST-200',
            'ST-1',
            'Type R',
            'Type R GT',
            'Type R Black Edition',
            '250 AMG',
            '250 AMG WhiteArt',
            'AMG',
            'AMG Yellow Night Edition',
            'AMG S',
            'S Edition 1',
            'S Edition 1 Motorsport',
            'AMG S Edition 1',
            'GT S',
            'GT R',
            'GT C',
            'John Cooper Works',
            'Nismo RS',
            'Nismo',
            'GTi',
            'GTi Prestige',
            'GTi by Peugeot Sport',
            'RenaultSport Trophy Nav',
            'Renaultsport Nav',
            'GT Nav',
            'Renaultsport 275 Cup S',
            'Renaultsport Nav 275',
            'Renaultsport 275',
            'Cupra',
            'Cupra Black',
            'vRS',
            'vRS 230',
            'VXR',
            'VXR SuperSport Nav',
            'R',
            'GTI Clubsport 40',
            'GTS',
            'M140i',
            'M235i',
            'M240i',
            'M2',
            'M3',
            'M3 30 Jahre Edition',
            'M4',
            'M5',
            'M6',
            'M',
        );
        $data['fuels'] = array(
            'Petrol Electric Hybrid',
            'Petrol PlugIn Elec Hybrid',
            'Diesel PlugIn Elec Hybrid',
            'Diesel Electric Hybrid',
            'PlugIn Elec Hybrid',
            'Electric',
        );

        $makes = DB::table('capreference')
                ->select(DB::raw('DISTINCT(maker)'))
            ->whereNotNull('cap_id')
            ->when(isset($listing_type), function ($query) use ($data, $listing_type) {
                // Hybrid so filter fuels
                if ($listing_type == 'H') {
                    return $query->whereIn('fuel', $data['fuels']);
                }
                // Performance so filter trims
                if ($listing_type == 'PERFORMANCE') {
                    $where = '(';
                    foreach ($data['trims'] as $trim) {
                        if (strpos($trim, '_')) {
                            $explode = explode('_', $trim);
                            $trim = $explode[1];
                        }
                        $where .= "trim = '" . $trim . "' OR ";
                    }

                    $where = substr($where, 0, strlen($where) - 4) . ')';

                    $query->whereraw($where);

                    return $query;

                }
            })
            ->where('database', '=', $data['database'])
            ->orderBy('maker');

        return $makes->get();
    }

    /**
     * @param $maker
     * @return \Illuminate\Support\Collection
     */
    public function getRanges($maker, $returnArr = false)
    {
        if (isset($maker)) {
            $models = DB::table('capreference')
                ->select(DB::raw('DISTINCT(`range`)'))
                ->whereNotNull('cap_id')
                ->where('maker', $maker)
                ->get();

        }

        if($returnArr === true) {
            $models = DB::table('capreference')
                ->select(DB::raw('DISTINCT(`range`)'))
                ->whereNotNull('cap_id')
                ->where('maker', $maker)
                ->get();

//            return $models->toArray();
        }
        return $models;

    }

    /**
     * @param $range
     * @return \Illuminate\Support\Collection
     */
    private function getTrims($range)
    {
        $trims = DB::table('capreference')
            ->select(DB::raw('DISTINCT(`trim`)'))
            ->where('range', $range)
            ->get();

        return $trims;
    }



    private function generateMakeRangeArray()
    {
        $query = DB::table('capreference')
            ->select(DB::raw('DISTINCT(`range`), maker'))
            ->groupBy('maker', 'range')
            ->get();

        return $query->toArray();
    }

    private function getMakerFromRange($range, $data = null)
    {

        $range = urldecode($range);

        if(isset($data)) {
            $query = DB::table('capreference')
                ->select(DB::raw('DISTINCT(maker)'))
                ->when(isset($data['maker']), function ($query) use ($data) {
                    return $query->wherein('maker', $data['maker']);
                })
                ->where('range', $range)
                ->first();
        }else {
            $query = DB::table('capreference')
                ->select(DB::raw('DISTINCT(maker)'))
                ->where('range', $range)
                ->first();
        }

        if(is_null($query)) {
            return false;
        }else{
//            if(isset($data)) {
//                foreach($query as $maker) {
//                    if (($key = array_search($maker, $data['maker'])) !== false) {
//                        unset($data['maker'][$key]);
//                    }
//                }
//            }

            return $query->maker;
        }
    }

    private function getMakersFromTrim($range) {

        $range = urldecode($range);
        $maker = [];

        $rangeExploded = explode('_', $range);
        if(count($rangeExploded) > 1) {
            $range = $rangeExploded[1];

            $query = DB::table('capreference')
                ->select('maker')
                ->where('trim', $range)
                ->get();

            if (is_null($query)) {
                return false;
            } else {
                foreach($query as $makers) {
                    $maker[] = $makers->maker;
                }
                return $maker;
            }
        }else{
            return false;
        }
    }

    private function getMakerFromTrim($range)
    {

	    $range = urldecode($range);

        $rangeExploded = explode('_', $range);
        if(count($rangeExploded) > 1) {
            $range = $rangeExploded[1];

            $query = DB::table('capreference')
                ->select('maker')
                ->where('trim', $range)
                ->first();

            if (is_null($query)) {
                return false;
            } else {
                return $query->maker;
            }
        }else{
            return false;
        }
    }

    public function updateFeatured(\Illuminate\Http\Request $request)
    {
        $capId  = $request['cap-id'];
        $updateFeatured  = $request['update-featured'];

        $prices = DB::table('pricestable')
            ->where('id', '=', $capId)
            ->first();

        if(is_null($prices)) {
            $prices = DB::table('van_pricestable')
                ->where('id', '=', $capId)
                ->first();
            DB::table('van_pricestable')->where('id', $prices->id)->update(['featured' => $updateFeatured]);
        }else{
            DB::table('pricestable')->where('id', $prices->id)->update(['featured' => $updateFeatured]);
        }

        return response()->json([
            'Status Updated',
        ], Response::HTTP_OK);
    }

    public function getMobileTrims() {
        $requestData = Request::all();

        $trims = DB::table('capreference')
            ->select(DB::raw('DISTINCT(`trim`)'))
            ->wherein('range', $requestData['range'])
            ->get();

        return view('partials.mobile-trims', compact('trims'));
    }

    public function viewAdverseTemp() {
        return view('pages.adverse-temp');
    }

//    private function fetchAnnualMiles($finance_type) {
//
//        $miles = DB::table('daily_imported_mileage')
//            ->where('finance_type', '=', $finance_type)
//            ->get();
//        return $miles;
//
//    }

    private function fetchDepositValues() {
        $qry = DB::table('pricestable')
            ->SELECT(db::raw("DISTINCT(deposit_value)"))
            ->get();

        return $qry;
    }
    private function fetchTerms() {
        $qry = DB::table('pricestable')
            ->SELECT(db::raw("DISTINCT(term)"))
            ->get();

        return $qry;
    }
    private function fetchAnnualMiles() {
        $qry = DB::table('pricestable')
            ->SELECT(db::raw("DISTINCT(annual_mileage)"))
            ->get();

        return $qry;
    }

    /**
     * @param null $cap_id
     * @param $database
     * @return false|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    private function fetchVehicleDefaultImage($cap_id = null, $database) {

        if(isset($cap_id) && !is_null($cap_id)) {
            $image = DB::table('cap_images')
                ->where('cap_id', '=', $cap_id)
                ->where('is_default', '=', 1)
                ->where('database', '=', strtoupper($database))
                ->first();

            return $image;
        }else{
            return false;
        }

    }
}

