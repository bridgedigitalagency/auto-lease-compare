<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Illuminate\Http\Request;
use Validator;
use Mail;
use Carbon\Carbon;
use App\Models\Cap;

class PagesController extends Controller
{

    public function __construct()
    {
    }

    public function vehicleRepair()
    {
        return view('pages.vehicle-repair');
    }

    public function vehicleRepairPost(Request $request) {

        $data = $request;

        $allowedUploadTypes = array('jpg', 'JPG', 'JPEG', 'PNG', 'png');
        $destination = storage_path('uploads/dwv');

        $dmgFile = $request->file('damage');
        $refFile = $request->file('reference');

        $curUser = Auth::id();

        $filename = md5($data->name . $data->email . $data->phone);
        if(!is_null($dmgFile)) {
            $filesize = filesize($dmgFile);
            $size = round($filesize / 1024 / 1024, 1);


            $ext = $dmgFile->getClientOriginalExtension();
            if(in_array($ext, $allowedUploadTypes)) {
                $damage = 'damage-' . $filename . '.' . $ext;
                $dmgFile->move($destination, $damage);
            }else{
                return redirect('/vehicle-repair?e=1');
            }
        }else{
            // return with error
//            return redirect('/vehicle-repair?e=2');
        }

        if(!is_null($refFile)) {
            $filesize = filesize($refFile);
            $size = round($filesize / 1024 / 1024, 1);

            $ext = $refFile->getClientOriginalExtension();
            if(in_array($ext, $allowedUploadTypes)) {
                $ref = 'reference-' . $filename . '.' . $refFile->getClientOriginalExtension();
                $refFile->move($destination, $ref);
            }else{
                return redirect('/vehicle-repair?e=1');
            }
        }else{
//            return redirect('/vehicle-repair?e=2');
        }

        $alloy = 0;
        $dent = 0;
        $paint = 0;

        if(!is_null($data->damageSelect)) {
            foreach ($data->damageSelect as $sel) {
                $$sel = 1;
            }
        }

        DB::table('dwv_submissions')->insert([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'postcode' =>$data['postcode'],
            'damage' => $damage,
            'reference' => $ref,
            'registration' => $data['registration'],
            'alloy' => $alloy,
            'dent' => $dent,
            'paint' => $paint,
            'additional_details' => $data['additional'],
            'created_at' => now(),
            'uid' => $curUser,
            'make'=> $data['make'],
            'model'=> $data['model'],
            'discount_code' => '06ALC2021'
        ]);
        $id = DB::getPdo()->LastInsertId();
        $submission = DB::table('dwv_submissions')->where('id', '=', $id)->first();
                // Image compression
        $valid_ext = array('png','jpeg','jpg');
        $file_extension = pathinfo($destination . '/' . $damage, PATHINFO_EXTENSION);
        $file_extension = strtolower($file_extension);

        // Check extension
        if(in_array($file_extension,$valid_ext)){

            // compress damage image
            $dmgResize = $this->compressImage($destination . '/' . $damage,$destination . '/' . $damage,50);
            // compress reference image
            $refResize = $this->compressImage($destination . '/' . $ref,$destination . '/' . $ref,50);

        }

        if($dmgResize == false || $refResize == false) {
            return redirect('/vehicle-repair?e=1')->withInput();
        }

        // Email
        $email = 'repairmycar@dwv.co.uk';
        $subject = "[EXTERNAL] Quote Request | " . $data['registration'] . " | ALC" . $id;

        // Adding logic in here so if I submit the form, I get the emails instead of being sent off to DWV
        if($data['email'] == 'scott@autoleasecompare.com') {
            $email = 'scott@autoleasecompare.com';
        }

        Mail::send(array('html' => 'emails.dwv-submission'), array('submission' => $submission, 'destination'=>$destination, 'ref'=>$ref, 'damage'=>$damage), function($message) use ($email, $subject,$submission, $ref, $damage,$destination)
        {
            $message->to($email, $submission->name)->subject($subject);
            $message->attach($destination . '/' . $damage);
            $message->attach($destination . '/' . $ref);

        });

        return redirect('/vehicle-repair?s=1');

    }

    public function carleasing() {

        $database = 'CARS';
        $makers = DB::table('manufacturers_search')
            ->where('database', '=', 'CARS')
            ->where('status', '=', 1)
            ->get();

        // this generates the right query but no results in laravel 6.2
        // Newer versions of laravel this works fine so leaving in here for now
        // and when we upgrade laravel we can implement this instead
//        $popularDeals = DB::table('quote')
//            ->select(DB::raw('quote.make, quote.model,sum(quote.price),quote.created_at'))
//            ->where([['deal_id', '!=', 'Dealer Testing 1'], ['created_at', '>=', Carbon::now()->subDays(30)]])
//            ->groupBy('make', 'model')
//            ->orderByRaw('sum(quote.price)')
//            ->limit(10)
//            ->get();

        $popularDeals = DB::select(DB::raw("SELECT quote.make, quote.model,sum(quote.price) FROM quote WHERE deal_id != 'Dealer Testing 1'
        AND created_at >= '" . Carbon::now()->subDays(190) . "' group by quote.make,quote.model order by sum(quote.price) desc LIMIT 10"));

        $deals = [];

        foreach($popularDeals as $popDeal) {
            $capDeals = DB::table('capreference')
                ->where('maker', '=', $popDeal->make)
                ->whereRaw("CONCAT(capreference.model,' ', capreference.derivative) = '" . $popDeal->model . "'")
                ->first();

            if(!is_null($capDeals)) {
                $deals[] = $capDeals;
            }
        }

        $vars = compact(
            'makers',
            'database',
            'deals');

        return view('pages.car-leasing', $vars);
    }
// Compress image
    private function compressImage($source, $destination, $quality) {

        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg') {
            $image = imagecreatefromjpeg($source);
        }elseif ($info['mime'] == 'image/png') {
            $image = imagecreatefrompng($source);

        }else{
            return false;
        }

        return imagejpeg($image, $destination, $quality);

    }

}