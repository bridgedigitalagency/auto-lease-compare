<?php
namespace App\Http\Controllers;

use App\Console\Commands\GaragePriceUpdate;
use App\Exports\CallsExport;
use App\Exports\DwvExport;
use App\Models\User;
use Mail;
use App\Libraries\TTNCApi;
use App\Models\Pricing;
use App\Models\Quote;
use App\Models\Calls;
use App\Exports\QuoteExport;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use jeremykenedy\LaravelRoles\Models\Role;

class EnquiriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewEnquiries(Request $request)
    {
        $data = $request->all();


        if (Auth::user()->hasRole(['admin'])) {
            $getQuotes = Quote::whereNotNull('quote.id')
                ->leftJoin('users', 'users.name', 'deal_id')->select(
                    'quote.*',
                    'users.phone as dealer_phone',
                    'users.email as dealer_email',
                    'users.opening_hours as dealer_opening_hours',
                    'users.name as dealer_name'
                )
                ->when(isset($data['status']), function ($query) use ($data) {
                    return $query->where('status', 'LIKE', $data['status']);
                })
                ->orderBy('created_at', 'DESC')
                ->paginate(20);


//            $getQuotes = Quote::whereNotNull('id')
//                ->when(isset($data['status']), function ($query) use ($data) {
//                    $query->where('status', 'LIKE', $data['status']);
//                })
//                ->orderBy('created_at', 'DESC')
//                ->paginate(20);
//
           $quotes = $getQuotes;


        } elseif (Auth::user()->hasRole(['dealer'])) {
            $getQuoteAuth = Auth::user()->name;
            $getQuoteAuth = trim(str_replace('OFFLINE', '', $getQuoteAuth));
            $getQuotes = Quote::where('deal_id', '=', $getQuoteAuth)
                ->when(isset($data['status']), function ($query) use ($data) {
                    return $query->where('status', 'LIKE', $data['status']);
                })
                ->orderBy('created_at', 'DESC')
                ->paginate(20);
            $quotes = $getQuotes;
        } else {

            $getQuoteEmail = Auth::user()->email;

            $quotes = Quote::query()
                ->leftJoin('users', 'users.name', 'deal_id')->select(
                    'quote.*',
                    'users.phone as dealer_phone',
                    'users.email as dealer_email',
                    'users.opening_hours as dealer_opening_hours',
                    'users.name as dealer_name'
                )->where('quote.email', '=', $getQuoteEmail)
                ->when(isset($data['status']), function ($query) use ($data) {
                    return $query->where('status', 'LIKE', $data['status']);
                })
                ->orderBy('quote.created_at', 'DESC')
                ->paginate(20);

        }

        return View('my_account_views.view_enquiries', compact('quotes'));
    }

    /**
     * @param Request $request
     */
    public function searchEnquiries(Request $request)
    {

        $data = $request->all();

        if (isset($data['autosearch']) && $data['autosearch'] != '') {
            $data['autosearch'] = "%" . $data['autosearch'] . "%";
        }
        if (isset($data['status']) && $data['status'] == '999') {
            unset($data['status']);
        }

        if (Auth::user()->hasRole(['admin'])) {
            $searchQuery = Quote::select('id')
                ->when(isset($data['autosearch']), function ($query) use ($data) {
                    return $query->where('name', 'LIKE', $data['autosearch'])
                        ->orwhere('email', 'LIKE', $data['autosearch'])
                        ->orwhere('make', 'LIKE', $data['autosearch'])
                        ->orwhere('model', 'LIKE', $data['autosearch'])
                        ->orwhere('id', 'LIKE', str_replace('ALC', '', $data['autosearch']));
                })
                ->get();

            $getQuotes = Quote::whereNotNull('id')
                ->when(isset($data['status']), function ($query) use ($data) {
                    return $query->where('status', '=', $data['status']);
                })
                ->wherein('id', $searchQuery)
                ->orderBy('created_at', 'DESC')
                ->get();

            $quotes = $getQuotes;
        } else {
            $searchQuery = Quote::select('id')
            ->when(isset($data['autosearch']), function ($query) use ($data) {
                return $query->where('name', 'LIKE', $data['autosearch'])
                    ->orwhere('email', 'LIKE', $data['autosearch'])
                    ->orwhere('make', 'LIKE', $data['autosearch'])
                    ->orwhere('model', 'LIKE', $data['autosearch'])
                    ->orwhere('id', 'LIKE', str_replace('ALC', '', $data['autosearch']));
            })
                ->get();
            $getQuoteAuth = Auth::user()->name;
            $getQuoteAuth = trim(str_replace('OFFLINE', '', $getQuoteAuth));
            $getQuotes = Quote::where('deal_id', '=', $getQuoteAuth)
                ->when(isset($data['status']), function ($query) use ($data) {
                    return $query->where('status', '=', $data['status']);
                })
                ->wherein('id', $searchQuery)
                ->orderBy('created_at', 'DESC')
                ->get();
            $quotes = $getQuotes;
        }
        return View('my_account_views.search_enquiries', compact('quotes'));
    }

    /**
     * Method for updating status for quote.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(Request $request)
    {
        $id = $request->id;

        $searchQuery = Quote::select()
            ->where('id', '=', $id)
            ->first();

        $dealer = User::where('name', $searchQuery->deal_id) -> first();

        // Fix to find dealers even when they're marked as offline
        if(is_null($dealer)) {
            $dealer = User::where('name', $searchQuery->deal_id . ' OFFLINE') -> first();
        }

        $trustpilotEmail = 0;
        $trustpilotReviewEmail = "www.autoleasecompare.com+9b863e5159@invite.trustpilot.com";
        switch($request->statusChoice) {
            case '1':
                $emailTemplate = 'emails.quote_sent';
                $subject = 'Auto Lease Compare - Quote Sent';
                $contact_company = 'Auto Lease Compare';
//                $searchQuery->email = 'scott@profiledigitalagency.co.uk';
            break;
            case '2':
                $emailTemplate = 'emails.quote_sold';
                $subject = 'Auto Lease Compare - Thank You!';
                $contact_company = 'Auto Lease Compare';
                $trustpilotEmail = 1;
//                $searchQuery->email = 'scott@profiledigitalagency.co.uk';
            break;
        }

        if (isset($searchQuery->email) && isset($emailTemplate) && !is_null($searchQuery->email)) {
            $email = $searchQuery->email;
            $message = '';

            Mail::send(array('html' => $emailTemplate), array('model'=>$searchQuery, 'dealer'=>$dealer), function ($message) use (
                $email,
                $subject,
                $contact_company, $trustpilotEmail, $trustpilotReviewEmail) {

                $message->to($email, $contact_company)->subject($subject);
                if($trustpilotEmail == 1) {
                    $message->bcc($trustpilotReviewEmail);
                }
            });
        }

        DB::table('quote')->where('id', $id)->update(['status' => $request->statusChoice]);
        switch ($request->statusChoice) {
            case 1:
                $statusUpdate = 'Quote Sent';
                break;
            case 2:
                $statusUpdate = 'Sold';
                break;
            case 3:
                $statusUpdate = 'Pending Payment';
                break;
            case 4:
                $statusUpdate = 'Paid';
                break;
            case 5:
                $statusUpdate = 'Closed';
                break;
            case 6:
                $statusUpdate = 'Open';
                break;
            case 7:
                $statusUpdate = 'Widthdrawn';
                break;
        }
        $this->updateActivity($id, $request->statusChoice);
        return response()->json([
            'Status Updated',
        ], Response::HTTP_OK);
    }

    public function updateWithdraw(Request $request)
    {
        $id = $request->id;
        $reason = $request->withdrawChoice;

        DB::table('quote')->where('id', $id)->update(['withdraw_enquiry' => $reason, 'status'=>7]);
        $this->updateActivity($id, '7');
        return response()->json([
            'Status Updated',
        ], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @description Update deals to be marked as completed or assisted by the live chat representatives
     */
    public function updateLiveChat(Request $request)
    {
        $id = $request->id;

        DB::table('quote')->where('id', $id)->update(['chat' => 1]);
        return response()->json([
            'Status Updated',
        ], Response::HTTP_OK);
    }

    public function updateQuoteSent(Request $request)
    {
        $id = $request->id;

        DB::table('quote')->where('id', $id)->update(['status' => $request->quotesentChoice]);
        $this->updateActivity($id, $request->quotesentChoice);
        return response()->json([
            'Status Updated',
        ], Response::HTTP_OK);
    }

    public function updateSold(Request $request)
    {
        $id = $request->id;

        DB::table('quote')->where('id', $id)->update(['status' => $request->soldChoice]);
        $this->updateActivity($id, $request->soldChoice);
        return response()->json([
            'Status Updated',
        ], Response::HTTP_OK);
    }

    public function export()
    {
        return Excel::download(new QuoteExport(), 'enquiries.xlsx');
    }
    public function exportdwv()
    {
        return Excel::download(new DwvExport(), 'enquiries.xlsx');
    }

    public function exportcalls()
    {
        return Excel::download(new CallsExport(), 'calls.xlsx');
    }

    public function viewCalls(Request $request) {
        $dealers = array();
        $getCallsAuth = Auth::user()->phone;

        if (Auth::user()->hasRole(['admin'])) {

            $allNumbers = DB::select( DB::raw("SELECT DISTINCT(o_number) FROM ttnc"));
            foreach($allNumbers as $number) {
                $num = str_replace(' ', '', $number->o_number);
                $users = DB::select(DB::raw("SELECT * FROM users WHERE deleted_at IS NULL AND Replace(phone, ' ', '') = \"" . $num . "\" LIMIT 1"));
                foreach($users as $user) {
                    $dealers["$num"] = $user->name;
                    break;
                }
            }

            $calls = Calls::whereNotNull('id')
                ->when(isset($request->dateorder), function ($calls) use ($request) {
                    return $calls->orderBy('c_date', $request->dateorder)
                        ->orderBy('c_time', $request->dateorder);
                })
                ->when(isset($request->duration), function ($calls) use ($request) {
                    return $calls->orderBy('c_duration', $request->duration);
                })
                ->when(isset($request->status), function ($calls) use ($request) {
                    return $calls->orderBy('status', $request->status);
                })
                ->when(!isset($request->dateorder) && !isset($request->duration) && !isset($request->status), function ($calls) use ($request) {
                    return $calls->orderBy('c_date', 'desc')
                                ->orderBy('c_time', 'desc');
                })
                ->paginate(20);

            return View('my_account_views.view_calls', compact('calls', 'dealers'));

        } elseif (Auth::user()->hasRole(['dealer'])) {
            $lastMonth = date('d-M-Y', strtotime('-1 months'));
            $thisMonth = date('m');

            // Get list of all calls
            $calls = Calls::where('o_number', '=', $getCallsAuth)
                ->orderBy('c_date', 'desc')
                ->orderBy('c_time', 'desc')
                ->paginate(20);

            // Get a count of the total calls for this number
            $totalCalls = Calls::where('o_number', '=', $getCallsAuth)
                ->count();

            // Get a list of total calls last month for this number
            $callsLastMonth = Calls::where('o_number', '=', $getCallsAuth)
                ->whereRaw("YEAR(c_date) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(c_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)")
                ->count();

            // get a list of total calls this month for this number
            $callsThisMonth = Calls::where('o_number', '=', $getCallsAuth)
                ->whereRaw("MONTH(c_date) = MONTH(CURRENT_DATE()) AND YEAR(c_date) = YEAR(CURRENT_DATE())")
                ->count();

            return View('my_account_views.view_calls', compact('calls', 'callsThisMonth', 'callsLastMonth', 'totalCalls'));
        }
    }

    public function viewCallsOld(Request $request) {

        $getCallsAuth = Auth::user()->phone;

        if (Auth::user()->hasRole(['admin'])) {

            $calls = Calls::whereNotNull('id')
                ->when(isset($request->dateorder), function ($calls) use ($request) {
                    return $calls->orderBy('c_date', $request->dateorder)
                        ->orderBy('c_time', $request->dateorder);
                })
                ->when(isset($request->duration), function ($calls) use ($request) {
                    return $calls->orderBy('c_duration', $request->duration);
                })
                ->when(isset($request->status), function ($calls) use ($request) {
                    return $calls->orderBy('status', $request->status);
                })
                ->when(!isset($request->dateorder) && !isset($request->duration) && !isset($request->status), function ($calls) use ($request) {
                    return $calls->orderBy('c_date', 'desc')
                                ->orderBy('c_time', 'desc');
                })
                ->paginate(20);

            $dealer = array();
            $allCalls = Calls::distinct('i_number')
                ->get();
            foreach($allCalls as $call) {
                $allUsers = User::where('phone', '=', $call->o_number)
                    ->get();
                foreach($allUsers as $dealers) {
                    $dealer[$dealers->name] = array(
                        'id'=>$dealers->id,
                        'name'=>$dealers->name,
                    );
                }
            }

            return View('my_account_views.view_calls', compact('calls', 'dealer'));

        } elseif (Auth::user()->hasRole(['dealer'])) {
            $lastMonth = date('d-M-Y', strtotime('-1 months'));
            $thisMonth = date('m');

            // Get list of all calls
            $calls = Calls::where('o_number', '=', $getCallsAuth)
                ->orderBy('c_date', 'desc')
                ->orderBy('c_time', 'desc')
                ->paginate(20);

            // Get a count of the total calls for this number
            $totalCalls = Calls::where('o_number', '=', $getCallsAuth)
                ->count();

            // Get a list of total calls last month for this number
            $callsLastMonth = Calls::where('o_number', '=', $getCallsAuth)
                ->whereRaw("YEAR(c_date) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(c_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)")
                ->count();

            // get a list of total calls this month for this number
            $callsThisMonth = Calls::where('o_number', '=', $getCallsAuth)
                ->whereRaw("MONTH(c_date) = MONTH(CURRENT_DATE()) AND YEAR(c_date) = YEAR(CURRENT_DATE())")
                ->count();

            return View('my_account_views.view_calls', compact('calls', 'callsThisMonth', 'callsLastMonth', 'totalCalls'));
        }
    }

    private function updateActivity($quoteId, $status)
    {

        $user = array(
            'id' => Auth::user()->id,
            'email' => Auth::user()->email,
            'name' => Auth::user()->name,
            'fullname' => Auth::user()->first_name . ' ' . Auth::user()->last_name
        );
        DB::table('activity_log')->insert(
            array('user' => serialize($user),
                'quote_id' => $quoteId,
                'status' => $status,
                'created_at' => now())
        );
    }

//    public function pricedropemail() {
//        $var = New GaragePriceUpdate;
//        $var->handle();
//    }


}
