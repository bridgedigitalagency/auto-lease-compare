<?php

namespace App\Jobs;

use App\Interfaces\CDNInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class DeleteCDNCapImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */

    public $tries = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */

    public $timeout = 60;
    protected $fileName;
    protected $database;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fileName, $database)
    {
        $this->fileName = $fileName;
        $this->database = $database;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CDNInterface $CDN)
    {
        //dd($this->fileName,$this->database);
        $CDN->delete($this->fileName,$this->database);
    }

    public function failed(\Exception $exception)
    {
        Log::info('failed to delete file '.$this->fileName.' with error: '.$exception->getMessage());
    }
}
