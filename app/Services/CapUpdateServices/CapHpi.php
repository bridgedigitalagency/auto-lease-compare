<?php

namespace App\Services\CapUpdateServices;



use App\Helpers\HttpRequest;
use App\Helpers\SoapRequest;

class CapHpi extends AbstractCapUpdateService
{
    protected $soapRequest;
    protected $httpRequest;

    public function __construct()
    {
        $this->soapRequest = new SoapRequest();
        $this->soapRequest->setParams([
            'subscriberId' => config('capupdateservice.clients.cap_hpi.base_wsdl.params.api_key'),
            'password' => config('capupdateservice.clients.cap_hpi.base_wsdl.params.api_password'),
        ]);
        $this->httpRequest = new HttpRequest();
    }

    public function getCapData($url, $function, $params)
    {
        $response = $this->soapRequest->setWSDL($url)->setFunction($function);
        foreach ($params as $key=>$param)
        {
            $response->addParam($key, $param);
        }
        $response = $response->send();
        $jsonResponse = json_encode(simplexml_load_string($response->Returned_DataSet->any));
        return json_decode($jsonResponse, true);
    }
}