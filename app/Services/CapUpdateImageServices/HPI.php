<?php

namespace App\Services\CapUpdateImageServices;

use App\Helpers\HttpRequest;

class HPI extends AbstractCapUpdateImagesService
{
    protected $httpRequest;

    public function __construct()
    {
        $this->httpRequest = new HttpRequest();
    }

    /**
     * @param $url
     * @param $function
     * @param $params
     * @return mixed
     */
    public function getCapImage($url, $function, $params)
    {
        $strToMD5 = config('capupdateservice.clients.cap_hpi.base_wsdl.params.api_key') . config('capupdateservice.clients.cap_hpi.base_wsdl.params.api_password');
        foreach ($params as $param) {
            $strToMD5 .= $param;
        }
        $hashCode = strtoupper(md5($strToMD5));
        $params ['subid'] = config('capupdateservice.clients.cap_hpi.base_wsdl.params.api_key');
        $params ['hashcode'] = $hashCode;
        $params['stream'] = true;

        $this->httpRequest->setURL(config('capupdateservice.clients.cap_hpi.base_wsdl.images.url'))
            ->setMethod(config('capupdateservice.clients.cap_hpi.base_wsdl.images.method'))
            ->setParams($params);
        $image = $this->httpRequest->send()->getContents();
        return $image;
    }
}