<?php

namespace App\Services\CapUpdateImageServices;

use App\Helpers\HttpRequest;
use App\Helpers\SoapRequest;
use App\Interfaces\CapUpdateImageGateway;

class CAD implements CapUpdateImageGateway
{

    protected $soapRequest;

    public function __construct()
    {
        $this->soapRequest = new SoapRequest();
        $this->soapRequest->setParams([
            'subscriberId' => config('capupdateservice.clients.cap_hpi.base_wsdl.params.api_key'),
            'password' => config('capupdateservice.clients.cap_hpi.base_wsdl.params.api_password'),
        ]);
    }
    /**
     * @param $url
     * @param $function
     * @param $params
     * @return mixed
     */
    public function getCapImage($url, $function, $params)
    {
        $capImages = array();
        $params ['user_id'] = config('capupdateservice.clients.CAD.params.api_key');
        $params['getsummaries'] = false;
        $params['liveonly'] = true;

        $this->soapRequest->setWSDL($url)
            ->setFunction($function)
            ->setParams($params);
        $response = $this->soapRequest->send();

        if(isset($response->count) && $response->count == 1 )
            $images = $response->reviewlist->review;
        elseif (isset($response->count) && $response->count > 1)
            $images = $this->getLatestReview($response->reviewlist->review);
        else
            return $capImages;

        if(isset($images->photolist) && isset($images->photolist->photo)){
            //there is only ne photo
            if(is_object($images->photolist->photo)){
                $capImages[0]['mini'] = $images->photolist->photo->mini;
                $capImages[0]['std'] = $images->photolist->photo->std;
                $capImages[0]['big'] = $images->photolist->photo->big;
                $capImages[0]['large'] = $images->photolist->photo->large;
                $capImages[0]['is_default'] = true;
            }else{
                // there are multiple photos
                $i = 0;
                foreach ($images->photolist->photo as $image){
                        $capImages[$i]['mini'] = $image->mini;
                        $capImages[$i]['std'] = $image->std;
                        $capImages[$i]['big'] = $image->big;
                        $capImages[$i]['large'] = $image->large;
                        $capImages[$i]['is_default'] = $image->isdefault;
                    $i++;
                }
            }
        }
        return $capImages;
    }

    protected function getLatestReview($reviewsList)
    {
        //$x = array_column($reviewsList , 'dateadded');
        //array_multisort($x, SORT_DESC, $reviewsList);
        usort($reviewsList, function ($a, $b) {
            return ($a->dateadded > $b->dateadded) ? -1 : 1;
        });
        return $reviewsList[0];
    }
}