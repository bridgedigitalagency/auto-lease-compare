<?php

namespace App\Services\CDN;

use App\Helpers\HttpRequest;
use App\Interfaces\CDNInterface;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class DOService implements CDNInterface
{
    protected $HTTPRequest;

    public function __construct(HttpRequest $request)
    {
        $this->HTTPRequest = $request;
    }

    /**
     * @param $filename
     * @return mixed
     */
    public function store($fileNames, $database)
    {
        unset($fileNames['is_default']);
        $NewFileName = (string)Str::uuid();
        $folder = config('filesystems.disks.do.folder');
        foreach ($fileNames as $key => $fileName) {
            Storage::disk('do')->put(
                "{$folder}/{$database}/{$key}/{$NewFileName}",
                file_get_contents($fileName),
                'public'
            );
        }
        //returns ex "https://fra1.digitaloceanspaces.com/cdn.autoleasecompare.com/newcapimages/CARS/large/add93792-320a-4ae7-b088-d2282939f239
        //$fileUrl = Storage::disk('do')->url("{$folder}/{$database}/large/{$NewFileName}");
        return $NewFileName;
    }

    /**
     * @param $filename
     * @return mixed
     */
    public function delete($fileName,$database)
    {
        $folder = config('filesystems.disks.do.folder');

        if(Storage::disk('do')->exists("{$folder}/{$database}/large/{$fileName}")){
            Storage::disk('do')->delete("{$folder}/{$database}/large/{$fileName}");
        }

        if(Storage::disk('do')->exists("{$folder}/{$database}/big/{$fileName}")){
            Storage::disk('do')->delete("{$folder}/{$database}/big/{$fileName}");
        }

        if(Storage::disk('do')->exists("{$folder}/{$database}/std/{$fileName}")){
            Storage::disk('do')->delete("{$folder}/{$database}/std/{$fileName}");
        }

        if(Storage::disk('do')->exists("{$folder}/{$database}/mini/{$fileName}")){
            Storage::disk('do')->delete("{$folder}/{$database}/mini/{$fileName}");
        }

        return response()->json(['message' => 'File deleted'], 200);
    }

    /**
     * @param $fileName
     * @return mixed
     */
    public function purge($fileName,$database)
    {
        $folder = config('filesystems.disks.do.folder');

        $this->HTTPRequest->setURL(config('filesystems.disks.do.cdn_endpoint') . '/cache');
        $this->HTTPRequest->setMethod('delete');
        $this->HTTPRequest->setParams([
            'files' => ["{$folder}/{$database}/big/{$fileName}"],
        ]);
        $this->HTTPRequest->send();
    }

}