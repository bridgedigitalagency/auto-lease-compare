<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\DeleteExpiredActivations::class,
        Commands\GaragePriceUpdate::class,
        Commands\PriceHistory::class,
        Commands\UpdateVanCap::class,
        Commands\UpdateCapData::class,
        Commands\UpdateCapImages::class,
        Commands\UpdateCapDiscontinuedModels::class,
        Commands\UpdateCapEquipment::class,
        Commands\UpdateCalasLookup::class,
        Commands\UpdateCalasFeed::class,
        Commands\UpdateTrustPilot::class,
        Commands\Cleanup::class,
        Commands\CleanPriceHistory::class,
        Commands\UpdateCapReviews::class,
        Commands\UpdateExcludedDeals::class,
        Commands\UpdateManufactuerCache::class,
        Commands\CheckFeeds::class,

        // Car Import scripts
        Commands\Imports\import1stChoiceVehicleFinance::class,
        Commands\Imports\importAsk4Leasing::class,
        Commands\Imports\importAutorama::class,
        Commands\Imports\importBestCarFinder::class,
        Commands\Imports\importBNFS::class,
        Commands\Imports\importCarLeaseOnline::class,
        Commands\Imports\importCarLeaseUK::class,
        Commands\Imports\importCarLeasingMadeSimple::class,
        Commands\Imports\importCarparison::class,
        Commands\Imports\importChampion::class,
        Commands\Imports\importDiscover::class,
        Commands\Imports\importDreamlease::class,
        Commands\Imports\importEvans::class,
        Commands\Imports\importNewCarContract::class,
        Commands\Imports\importReady2Lease::class,
        Commands\Imports\importThinkAutoLeasing::class,
        Commands\Imports\importEastBourneAudi::class,
        Commands\Imports\importEasyLease::class,
        Commands\Imports\importEcarLeaseUK::class,
        Commands\Imports\importHorsepower::class,
        Commands\Imports\importIcarlease::class,
        Commands\Imports\importJustDrive::class,
        Commands\Imports\importLeapVehicleLeasing::class,
        Commands\Imports\importLease4Less::class,
        Commands\Imports\importLeaseCar::class,
        Commands\Imports\importLeasingOptions::class,
        Commands\Imports\importLeasingRoute::class,
        Commands\Imports\importLeasingYourWay::class,
        Commands\Imports\importLetsTalkLeasing::class,
        Commands\Imports\importLowCostVans::class,
        Commands\Imports\importMilease::class,
        Commands\Imports\importMWVC::class,
        Commands\Imports\importRGWVC::class,
        Commands\Imports\importRGWVC_Vans::class,
        Commands\Imports\importSimpleLeasing::class,
        Commands\Imports\importTilsun::class,
        Commands\Imports\importTotalVehicleLeasing::class,
        Commands\Imports\importTrioLeasing::class,
        Commands\Imports\importUKCarLine::class,
        Commands\Imports\importWessexFleet::class,
        Commands\Imports\importRivervale::class,
        Commands\Imports\importYourWayLeasing::class,
        Commands\Imports\importAA::class,
        Commands\Imports\importPeterVardy::class,
        Commands\Imports\importRadius::class,


        // Van Import Scripts
        Commands\Imports\import1stChoiceVehicleFinance_Vans::class,
        Commands\Imports\importAutorama_Vans::class,
        Commands\Imports\importCarLeaseUK_Vans::class,
        Commands\Imports\importCarparison_Vans::class,
        Commands\Imports\importBestCarFinder_Vans::class,
        Commands\Imports\importBNFS_Vans::class,
        Commands\Imports\importReady2Lease_Vans::class,
        Commands\Imports\importTilsun_Vans::class,
        Commands\Imports\importTotalVehicleLeasing_Vans::class,
        Commands\Imports\importRivervale_Vans::class,

        // CLMS automated toggle
        Commands\DealerToggles\PauseCarLeasingMadeSimple::class,
        Commands\DealerToggles\ActivateCarLeasingMadeSimple::class,

        //RGWVC evening switch
        Commands\DealerToggles\RemoveRGWVCDeals::class,
        Commands\DealerToggles\ActivateRGWVCDeals::class,

        // Leap weekend switch
        Commands\DealerToggles\ActivateLeapDeals::class,
        Commands\DealerToggles\RemoveLeapDeals::class,

        // Fleet Prices weekend switch
        Commands\DealerToggles\ActivateFleetDeals::class,
        Commands\DealerToggles\RemoveFleetDeals::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Process Calas Feed
        $schedule->command('updatecalasfeed')
            ->dailyAt('01:30')
            ->sendOutputTo('/home/autoleasecompare/dealers/logs/artisan.log');

        $schedule->command('activations:clean')
            ->daily();

        $schedule->command('UpdateCapEquipment')
            ->daily();

        $schedule->command('update:pricehistory')
            ->dailyAt('05:00');

//        $schedule->command('cleanup')
//            ->dailyAt('05:00');

        $schedule->command('GaragePriceUpdate')
            ->dailyAt('09:00');

        $schedule->command('updatetrustpilot')
            ->daily();

        $schedule->command('CheckFeeds')->dailyAt('07:30');
        $schedule->command('update:ManufacturerCache')->dailyAt('07:30');

        $schedule->command('UpdateCapData cars')->dailyat('04:00')->withoutOverlapping();
        $schedule->command('UpdateCapData lcv')->dailyat('04:30')->withoutOverlapping();
        $schedule->command('UpdateCapImages cars')->dailyat('05:00')->withoutOverlapping();
        $schedule->command('UpdateCapImages lcv')->dailyat('05:15')->withoutOverlapping();
        $schedule->command('UpdateCapReviews')->dailyat('05:20');

        // Deal with leap going on/offline
        $schedule->command('ActivateLeapDeals')->weeklyOn(1, '09:00');
        $schedule->command('ActivateLeapDeals')->weeklyOn(2, '09:00');
        $schedule->command('ActivateLeapDeals')->weeklyOn(3, '09:00');
        $schedule->command('ActivateLeapDeals')->weeklyOn(4, '09:00');
        $schedule->command('RemoveLeapDeals')->dailyAt('17:30');

        //RGWVC toggles
        $schedule->command('ActivateRGWVCDeals')->weeklyOn(1, '09:00');
        $schedule->command('ActivateRGWVCDeals')->weeklyOn(2, '09:00');
        $schedule->command('ActivateRGWVCDeals')->weeklyOn(3, '09:00');
        $schedule->command('ActivateRGWVCDeals')->weeklyOn(4, '09:00');
        $schedule->command('ActivateRGWVCDeals')->weeklyOn(5, '09:00');

        $schedule->command('RemoveRGWVCDeals')->weeklyOn(1, '17:00');
        $schedule->command('RemoveRGWVCDeals')->weeklyOn(2, '17:00');
        $schedule->command('RemoveRGWVCDeals')->weeklyOn(3, '17:00');
        $schedule->command('RemoveRGWVCDeals')->weeklyOn(4, '17:00');
        $schedule->command('RemoveRGWVCDeals')->weeklyOn(5, '15:00');


        // Deal with Fleet going on/offline
        $schedule->command('RemoveFleetDeals')->weeklyOn(5, '21:00');
        $schedule->command('ActivateFleetDeals')->weeklyOn(1, '05:00');
        // Deal with CLMS going on/offline
        $schedule->command('PauseCarLeasingMadeSimple')->weeklyOn(5, '17:00');
        $schedule->command('ActivateCarLeasingMadeSimple')->weeklyOn(1, '05:00');

//        // Import scripts
//        // KEEP IN TIME ORDER - Makes it easier to plan things out
//
//        $schedule->command('import:petervardy')->dailyAt('00:30');
//
//        $schedule->command('import:carleaseuk')->dailyAt('00:30');
//        $schedule->command('import:carleaseuk_vans p')->dailyAt('00:30');
//        $schedule->command('import:carleaseuk_vans b')->dailyAt('00:30');
//        $schedule->command('import:ecarleaseuk')->dailyAt('00:35');
//        $schedule->command('import:letstalkleasing')->dailyAt('00:40');
////        $schedule->command('import:lowcostvans')->dailyAt('00:45');
//        $schedule->command('import:milease')->dailyAt('00:45');
//        $schedule->command('import:importaa')->dailyAt('00:45');
//        $schedule->command('import:mwvc')->dailyAt('00:45');
//
//        $schedule->command('import:rgwvc')->dailyAt('00:50');
//        $schedule->command('import:rgwvcvans')->dailyAt('00:50');
//
//        $schedule->command('import:tilsun')->dailyAt('00:55');
//        $schedule->command('import:tilsun_vans')->dailyAt('00:55');
////        $schedule->command('import:totalvehicleleasing')->dailyAt('01:00');
////        $schedule->command('import:totalvehicleleasing_vans')->dailyAt('01:00');
//        $schedule->command('import:trioleasing')->dailyAt('01:05');
////        $schedule->command('import:ukcarline')->dailyAt('01:10');
//        $schedule->command('import:wessexfleet')->dailyAt('01:15');





//        $schedule->command('import:carleaseonline')->dailyAt('02:00');
//        $schedule->command('import:importautorama')->dailyAt('02:00');
//        $schedule->command('import:importautoramaVans')->dailyAt('02:00');
//        $schedule->command('import:icarlease')->dailyAt('02:15');
//        $schedule->command('import:justdrive')->dailyAt('02:15');
//        $schedule->command('import:leapleasing')->dailyAt('02:20');
//        $schedule->command('import:leasecar')->dailyAt('02:25');
//        $schedule->command('import:leasingoptions')->dailyAt('02:25');
//
////        $schedule->command('import:leasingroute')->dailyAt('02:25');
////        $schedule->command('import:leasingyourway')->dailyAt('02:25');
//        $schedule->command('import:rivervale')->dailyAt('02:25');
//        $schedule->command('import:rivervalevans')->dailyAt('02:30');




//        $schedule->command('import:import1stchoicevehiclefinance')->dailyAt('02:30');
//        $schedule->command('import:import1stchoicevehiclefinance_vans')->dailyAt('02:30');
//        $schedule->command('import:carleasingmadesimple')->dailyAt('02:30');
//        $schedule->command('import:ask4leasing')->dailyAt('02:35');
//        $schedule->command('import:radius')->dailyAt('02:35');
//        $schedule->command('import:bestcarfinder')->dailyAt('02:35');
//        $schedule->command('import:bestcarfindervans')->dailyAt('02:35');
//        $schedule->command('import:bnfs')->dailyAt('02:35');
//        $schedule->command('import:bnfsvans')->dailyAt('02:35');
//        $schedule->command('import:champion')->dailyAt('02:40');
//        $schedule->command('import:discover')->dailyAt('02:45');
//        $schedule->command('import:dreamlease')->dailyAt('02:50');
//        $schedule->command('import:lease4less')->dailyAt('02:50');
//        $schedule->command('import:evanshalshaw')->dailyAt('02:55');
//        $schedule->command('import:newcarcontract')->dailyAt('02:56');
//        $schedule->command('import:carparison')->dailyAt('03:00');
//        $schedule->command('import:ready2lease')->dailyAt('03:05');
//        $schedule->command('import:ready2leasevans')->dailyAt('03:05');
//        $schedule->command('import:thinkautoleasing')->dailyAt('03:10');
//        $schedule->command('import:yourwayleasing')->dailyAt('03:15');
//        $schedule->command('import:eastbourneaudi')->dailyAt('03:15');
//        $schedule->command('import:easylease')->dailyAt('03:15');


    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
