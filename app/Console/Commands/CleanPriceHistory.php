<?php

namespace App\Console\Commands;
ini_set('memory_limit','2048M');

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\PriceHistory;
use App\Models\VanPriceHistory;
use Illuminate\Support\Facades\DB;

class CleanPriceHistory extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:ph {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean up price history tables';

    public function __construct()
    {
        parent::__construct();

    }

    public function handle()
    {
        $startTime = time();
        $database = strtoupper($this->argument('type'));
        $this->cleanup($database);

        $endTime = time();
        $this->info('Script processed in ' . gmdate("H:i:s", ($endTime - $startTime)));
    }

    /**
     * @param $database
     * @return void
     */
    private function cleanup($database) {

        // Set correct DB tables / model for vehicle database type
        if($database == 'CARS') {
            $this->newPricehistoryTable = 'new_priceshistory_cars';
            $priceHistory = new PriceHistory;
        }elseif($database == 'LCV') {
            $this->newPricehistoryTable = 'new_priceshistory_vans';
            $priceHistory = new VanPriceHistory;
        }

        // Loop around correct dataset in chunks of 10,000
        $priceHistory::where('database', '=', $database)->orderBy('id')->chunkById(10000, function($ph)
        {
            foreach ($ph as $row)
            {
                // If price history log is older than 3 months then delete
                if($row->updated_at < Carbon::now()->subDays(90)) {
                    $row->delete();
                }else{
                    // if price history log is newer than 3 months, store in new temp table so we can sort indexes out on proper table
                    DB::table($this->newPricehistoryTable)->insert([
                        'id'=>$row->id,
                        'finance_type'=>$row->finance_type,
                        'cap_id'=>$row->cap_id,
                        'monthly_payment'=>$row->monthly_payment,
                        'annual_mileage'=>$row->annual_mileage,
                        'term'=>$row->term,
                        'deposit_value'=>$row->deposit_value,
                        'updated_at'=>$row->updated_at,
                        'database'=>$row->database,
                    ]);
                    // Delete from proper table once deleted so it doesn't get re-added
                    $row->delete();
                }
            }
        });
    }

}
