<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\VanPricing;
use Illuminate\Console\Command;
use App\Models\Pricing;
use App\Models\Cap;
use Illuminate\Support\Facades\DB;

class CheckFeeds extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckFeeds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Task created to alert about feed status in the morning';

    protected $sendSlackNotifications = 1;

    /**
     * CheckFeeds constructor.
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     *
     */
    public function handle()
    {
        // Bridge slack
//        $this->slackUrl = 'https://hooks.slack.com/services/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';
        // Autolease slack
        $this->slackUrl = 'https://hooks.slack.com/services/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';


        // Set list of dealers.
        $this->dealers = $this->fetchDealers();

        // Check online info

        $this->checkOnlineDealers('cars');
        $this->checkOnlineDealers('lcv');

        $this->checkCleanupScript('cars');
        $this->checkCleanupScript('lcv');

        $this->checkDealersLastQuote();

        // Check which dealers are not online
        $this->checkOfflineDealers();
    }

    /**
     * @param $database
     */
    private function checkOnlineDealers($database) {

        $date = date('d-m-Y');

        if($database == 'lcv') {
            $pricestable = 'van_pricestable';
            $header = 'Van dealers that are live today (' . $date . ')';
        }else{
            $pricestable = 'pricestable';
            $header = 'Dealers that are live today (' . $date . ')';
        }

        $content = array(
            'blocks' => []
        );

        // Group by doesn't work on live
        $dealers = DB::table($pricestable)
            ->select('id','deal_id')
            ->groupBy('deal_id')
            ->get();

        if($dealers) {

            $content['blocks'][] = array(
                'type'=>'header',
                'text'=>array(
                    'type'=>'plain_text',
                    'text'=>$header
                )
            );

            foreach ($dealers as $dealer) {

                if($database == 'cars') {
                    if (($key = array_search($dealer->deal_id, $this->dealers)) !== false) {
                        unset($this->dealers[$key]);
                    }
                }

                if($dealer->deal_id == 'Dealer Testing 1') {continue;}

                $content['blocks'][] = array(
                    'type' => 'section',
                    'text' => array(
                        'type' => 'plain_text',
                        'text' => $dealer->deal_id
                    )

                );

            }

            $this->sendSlackNotification($content);
        }

    }

    /**
     * @param $database
     */
    private function checkCleanupScript($database) {

        if($database == 'lcv') {
            $pricing = new VanPricing();
            $header = 'Van cleanup script processes (these should be 0)';
        }else {
            $pricing = new Pricing();
            $header = 'Cleanup script processes (these should be 0)';
        }
        $date = date('d-m-Y');

        $content = array(
            'blocks' => []
        );

        $average_cost = $pricing::whereNull('average_cost')->count();
        $total_cost = $pricing::whereNull('total_cost')->count();
        $total_initial_cost = $pricing::whereNull('total_initial_cost')->count();

        $content['blocks'][] = array(
            'type'=>'header',
            'text'=>array(
                'type'=>'plain_text',
                'text'=>$header
            )
        );

        $content['blocks'][] = array(
            'type' => 'section',
            'text' => array(
                'type' => 'plain_text',
                'text' => 'Deals without average cost set: ' . $average_cost
            )
        );
        $content['blocks'][] = array(
            'type' => 'section',
            'text' => array(
                'type' => 'plain_text',
                'text' => 'Deals without total cost set: ' . $total_cost
            )
        );
        $content['blocks'][] = array(
            'type' => 'section',
            'text' => array(
                'type' => 'plain_text',
                'text' => 'Deals without total initial cost set: ' . $total_initial_cost
            )
        );

        $this->sendSlackNotification($content);

    }

    /**
     * @param $content
     */
    private function sendSlackNotification($content) {

        $content = json_encode($content);

        if($this->sendSlackNotifications == 1) {

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $this->slackUrl);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Content-Length: ' . strlen($content)));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec($ch);

            curl_close($ch);
        }

    }

    /**
     *
     */
    private function checkOfflineDealers() {

        $header = "Dealers that are offline:";

        $content['blocks'][] = array(
            'type'=>'header',
            'text'=>array(
                'type'=>'plain_text',
                'text'=>$header
            )
        );

        foreach($this->dealers as $dealer) {
            $content['blocks'][] = array(
                'type' => 'section',
                'text' => array(
                    'type' => 'plain_text',
                    'text' => $dealer
                )

            );
        }

        $this->sendSlackNotification($content);
    }

    private function checkDealersLastQuote()
    {
        $header = "Dealers last enquiries (email):";

        $content['blocks'][] = array(
            'type'=>'header',
            'text'=>array(
                'type'=>'plain_text',
                'text'=>$header
            )
        );

        foreach($this->dealers as $dealer) {

            $check = DB::table('quote')
                ->where('deal_id', '=', $dealer)
                ->orderBy('created_at', 'desc')
                ->first();

            if($check) {

                $content['blocks'][] = array(
                    'type' => 'section',
                    'text' => array(
                        'type' => 'plain_text',
                        'text' => $dealer . ' - ' . $check->created_at
                    )

                );
            }else{
                $content['blocks'][] = array(
                    'type' => 'section',
                    'text' => array(
                        'type' => 'plain_text',
                        'text' => $dealer . ' - none'
                    )

                );
            }
        }

        $this->sendSlackNotification($content);

    }

    /**
     * @return array
     */
    private function fetchDealers() {

        $data = array();

        $dealers = User::whereHas(
            'roles', function($q){
            $q->where('name', 'Dealer');
        }
        )->orderBy('name')
            ->get();

        foreach($dealers as $dealer) {
            $data[] = $dealer->name;
        }

        return $data;
    }
}