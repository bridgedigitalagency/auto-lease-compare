<?php

namespace App\Console\Commands;

use App\Interfaces\CapUpdateGatewy;
use App\Jobs\DeleteCDNCapImage;
use App\Models\Cap;
use App\Models\CapImage;
use App\Models\CapReview;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateCapDiscontinuedModels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cap:update:discontinuedModels {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'remove all discontinued models from CAP';
    protected $capUpdateGateway;
    protected $captable = 'car_capreference';
    protected $capIDs;
    protected $capIDDates;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CapUpdateGatewy $capUpdateGatewy)
    {
        $this->capUpdateGateway = $capUpdateGatewy;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $database = strtoupper($this->argument('type'));
        $this->info('checking capreference ' . $database);
        $this->setDBTables($database);
        $this->updateDiscontinuedModels($database);
    }
    private function setDBTables($database)
    {
        $this->captable = $database == 'LCV' ? 'van_capreference' : $this->captable;
    }

    protected function updateDiscontinuedModels($database = 'CARS')
    {
        $cap = Cap::query();
        $cap->select('cap_id','maker','range','database');
        $cap->where('database',$database);
        $cap->orderBy('tblid');
        $count = $cap->count();
        $this->output->progressStart($count);
        $capData = $cap->chunk(100,function ($capData) use($database){

            foreach ($capData as $cap){
                $this->formatBulkData($cap->cap_id);
                $this->output->progressAdvance();
            }
            echo PHP_EOL.'--------------------------------------------------'.PHP_EOL;
            echo 'Getting chunk from CAP'.PHP_EOL;
            $actualCapData = $this->getBulkTechnicalData($database);
            $this->capIDs = '';
            $this->capIDDates = '';
            echo 'Map data to our local records'.PHP_EOL;
            $this->mapCapData($actualCapData);

        });
        $this->output->progressFinish();
        $this->output->success('Discontinued models cleaned up successfully');
    }

    private function formatBulkData($capID)
    {
        $this->capIDs .= ',' . $capID;
        $this->capIDDates .= ',' . date('Y-m-d');

    }

    private function getBulkTechnicalData($database = 'CARS')
    {
        $tecDataList = '';
        $data = $this->capUpdateGateway->getCapData(
            config('capupdateservice.clients.cap_hpi.base_wsdl.nvd.url'),
            'GetBulkTechnicalData',
            [
                'database' => $database,
                'capidList' => substr($this->capIDs, 1),
                'specDateList' => substr($this->capIDDates, 1),
                'techDataList' => $tecDataList,
                'returnVehicleDescription' => true,
                'returnCaPcodeTechnicalItems' => true,
                'returnCostNew' => true,
            ]
        );
        return $data;
    }

    private function mapCapData($actualCapData)
    {
        if (!empty($actualCapData) && !empty($actualCapData['TechData']['Tech_Table'])) {
            foreach ($actualCapData['TechData']['Tech_Table'] as $cap) {
                $localCap = Cap::where('cap_id', $cap['CAPID'])->first();

                if (!($cap['Manufacturer'] == $localCap['maker'] &&
                    $cap['Range'] == $localCap['range'] &&
                    $cap['Model'] == $localCap['model'] &&
                    $cap['Derivative'] == $localCap['derivative'])) {

                    echo $cap['CAPID'] . ' data from CAP does match our records, so will be deleted' . PHP_EOL;
                    $this->deleteCapImages($cap['CAPID']);
                    $this->deleteCapModel($cap['CAPID']);
                }
            }
        }
    }
    private function deleteCapModel($capID)
    {
        DB::table($this->captable)->where('cap_id',$capID)->delete();
        echo 'Deleted '.$this->captable.' for cap id '.$capID.PHP_EOL;
        CapReview::where('cap_id',$capID)->delete();
        echo 'Deleted cap reviews records for cap id '.$capID.PHP_EOL;
        DB::table('cap_id_optional_equipment')->where('cap_id',$capID)->delete();
        echo 'Deleted cap_id_optional_equipment records for cap id '.$capID.PHP_EOL;
        DB::table('cap_tech_data')->where('cap_id',$capID)->delete();
        echo 'Deleted cap_tech_data records for cap id '.$capID.PHP_EOL;
        Cap::where('cap_id',$capID)->delete();
        echo 'Deleted Capreference id '.$capID.PHP_EOL;

    }
    private function deleteCapImages($capID)
    {
        echo 'Deleting cap '.$capID.' images'.PHP_EOL;
        $capImages = CapImage::where('cap_id',$capID)->get();
        foreach ($capImages as $capImage){
            $capImage->delete();
        }
        echo 'cap '.$capID.' images deleted'.PHP_EOL;
    }
}
