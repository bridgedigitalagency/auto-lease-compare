<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Mail;


class GaragePriceUpdate extends Command
{
    protected $activationRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GaragePriceUpdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check cars in all users garages, if the prices are cheaper now than they were yesterday then send email';

    /**
     *
     * Email address to send the CSV file to
     *
     * @var array
     */

    protected $sendTo = array('scott@bridgedigitalagency.co.uk');

    /**
     * GaragePriceUpdate constructor.
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sendEmail = 0;
        $arr = array();
        $message = "";
        $sql = "SELECT * FROM users WHERE garage IS NOT NULL AND garage_price_notification = 1 LIMIT 4";
        $results = DB::select( DB::raw($sql) );
        // loop around users
        foreach($results as $result) {
            $garage = unserialize($result->garage);
            if(count($garage)==0) {
                continue;
            }

            foreach($garage as $deal) {
                $pricessql = "SELECT * FROM pricestable WHERE id=" . $deal;

                $pricesresults = DB::select( DB::raw($pricessql) );

                foreach($pricesresults as $priceTable) {
                    //$todaysDate = date('Y-m-d');
                    $todaysDate = date('Y-m-d', strtotime('yesterday'));
                    $todaysPrice = (int)$priceTable->monthly_payment;
                    $messageString = '';

                    $pricehistorysql = "SELECT monthly_payment FROM priceshistory WHERE cap_id=" . $priceTable->cap_id . " AND annual_mileage = '" . $priceTable->annual_mileage . "' AND term = '" . $priceTable->term . "' AND deposit_value = '" . $priceTable->deposit_value . "' AND updated_at='" . $todaysDate . "' LIMIT 1";

                    $pricehistoryresults = DB::select( DB::raw($pricehistorysql) );
                    foreach($pricehistoryresults as $priceHistoryTable) {
                        $yesterdaysPrice = $priceHistoryTable->monthly_payment;
                    }
                    if(!isset($yesterdaysPrice)) {
                        $yesterdaysPrice = 376;
                    }

                    if(isset($yesterdaysPrice) && $yesterdaysPrice > $todaysPrice) {
                        if($sendEmail!=1) {
                            $sendEmail = 1;
                        }

                        $cheaperCars = array();
                        $capsql = "SELECT * FROM capreference WHERE cap_id=" . $priceTable->cap_id;

                        $capresults = DB::select(DB::raw($capsql));


                            foreach ($capresults as $cap) {
                                $arr[] = $cap->maker . ' ' . $cap->range . ' and ';
                            }


                    }
                }
            }

            $messageString = '';
            foreach($arr as $msg) {
                $messageString .= $msg;
            }
            $messageString = substr($messageString, 0, strlen($messageString) - 4);


            if($sendEmail == 1) {
                // send email
//                $this->sendTo = $result->email;
                $this->sendTo = 'scott@bridgedigitalagency.net';
                $this->subject = "Auto Lease Compare: Price update";

                $this->message = $messageString;

                $emailTemplate = 'emails.garage_price_update';

                Mail::send(array('html' => $emailTemplate), array('text'=>$this->message), function ($message) {
                    $message->to($this->sendTo);
                    $message->subject($this->subject);
                    $message->setBody($this->message);

                });
            }


        }
    }
}

