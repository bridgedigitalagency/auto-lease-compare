<?php

namespace App\Console\Commands;
ini_set('memory_limit','2048M');

use Illuminate\Console\Command;
use App\Models\Pricing;
use App\Models\Cap;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Cleanup extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update price history table and clean up prices table';

    public function __construct()
    {
        parent::__construct();
        $this->dealerDirectory = '/home/autoleasecompare/dealers/dvs/';
        $this->pricing = new Pricing();
        $this->cap = new Cap();
        $this->allowedDepositValues = explode(',', env('AllowedDepositValues'));
        $this->allowedTerms = explode(',', env('AllowedTerms'));
        $this->allowedMileages = explode(',', env('AllowedMileages'));
    }

    public function handle()
    {
        foreach(array('CARS', 'LCV') as $database) {

            $this->cleanup($database);

            $this->updatePricingFields($database);


        }

        $this->insertTestDeal();


    }

    private function updatePricingFields($database) {

        if($database == 'CARS') {
            $pricestable = 'pricestable';
        }else{
            $pricestable = 'van_pricestable';
        }
        $this->info('updating average cost on ' . $pricestable);

        // set average cost fields
        DB::table($pricestable)->whereNull('average_cost')
            ->update(['average_cost' =>DB::raw("(monthly_payment*(term-1)+deposit_months+document_fee)/(term)")]);

        $this->info('updating total cost on ' . $pricestable);
        // set total cost fields
        DB::table($pricestable)->whereNull('total_cost')
            ->update(['total_cost' =>DB::raw("(monthly_payment*(term-1)+deposit_months+document_fee)")]);

        $this->info('updating total initial cost on ' . $pricestable);
        // set total initial cost field
        DB::table($pricestable)->whereNull('total_initial_cost')
            ->update(['total_initial_cost' =>DB::raw("(deposit_months+document_fee)")]);

    }

    /**
     * Clean up old history
     */
    private function cleanup($database)
    {

        $updatedDate = date('Y-m-d');
        $expiresDate = date('Y-m-d', strtotime('-1 days'));

        if($database == 'CARS') {
            $pricestable = 'pricestable';
        }else{
            $pricestable = 'van_pricestable';
        }

        // Delete any deal that isn't allowed within our set deposit values, terms and mileage
        $this->info('Removing deals with invalid deposits');
        DB::table($pricestable)->whereNotIn('deposit_value', $this->allowedDepositValues)->delete();
        $this->info('Removing deals with invalid terms');
        DB::table($pricestable)->whereNotIn('term', $this->allowedTerms)->delete();
        $this->info('Removing deals with invalid mileage');
        DB::table($pricestable)->whereNotIn('annual_mileage', $this->allowedMileages)->delete();


        $this->info('Removing deals with cap id set as 0');
        DB::table($pricestable)->where('cap_id', '=', '0')->delete();


        // Delete any deal under £70/month
        $this->info('Removing deals with prices under £70 a month');
        DB::table($pricestable)->where('monthly_payment', '<', '70')->delete();

        $this->info('Removing deals that expires');
        DB::table($pricestable)->where('expires_at', '=', $expiresDate)->delete();

        // Delete all deals that haven't been processed today
        $this->info('Removing deals that haven\'t been added today');
        DB::table($pricestable)->where('deal_id', '!=', 'Dealer Testing 1')
            ->where('updated_at', '!=', $updatedDate)
            ->delete();

        // Clean up DVS import files
        exec('mv ' . $this->dealerDirectory . '/today/*.csv ' . $this->dealerDirectory . '/processed');
        exec('mv ' . $this->dealerDirectory . '/cars/*.csv ' . $this->dealerDirectory . '/today');

    }

    private function insertTestDeal() {

        DB::table('pricestable')
            ->insert([
                'id'=>2,
                'deal_id'=>'Dealer Testing 1',
                'cap_id'=>76763,
                'finance_type'=>'B',
                'deposit_value'=>6,
                'term'=>36,
                'monthly_payment'=>1002.00,
                'annual_mileage'=>10000,
                'document_fee'=>'2249.00',
                'in_stock'=>0,
                'expires_at'=>'2022-12-31',
                'updated_at'=>date('Y-m-d'),
                'featured'=>0,
                'deposit_months'=>1255.86,
                'adverse_credit'=>null,
                'average_cost'=>1071.52,
                'total_cost'=>38574.86,
                'total_initial_cost'=>3504.86,
                'insurance'=>0,
                'uid'=>(string) Str::uuid()
            ]);

    }

}
