<?php

namespace App\Console\Commands;

use App\Interfaces\CapUpdateGatewy;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateCapData extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateCapData {type} {step?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update cap data for cars, cars/lcv, 1,2,3,4,5';
    protected $capUpdateGatewy;
    protected $pricestable = 'pricestable';
    protected $captable = 'car_capreference';
    protected $imageDir = '/images/capimages/';
    protected $capIDs;
    protected $capIDDates;

    public function __construct(CapUpdateGatewy $capUpdateGatewy)
    {
        $this->capUpdateGatewy = $capUpdateGatewy;
        parent::__construct();
    }

    public function handle()
    {
        $database = strtoupper($this->argument('type'));
        $step = $this->argument('step');

        $this->info('Processing ' . $database);
        $this->setDBTables($database);
        if (isset($step) && !empty($step)){
            switch ($step) {
                case 1:
                    $this->update_cap_ids($database);
                    break;
                case 2:
                    $this->update_capreference($database);
                    break;
                case 3:
                    $this->update_optional_extras($database);
                    break;
                case 4:
                    $this->addToCapReference($database);
                    break;
                case 5:
                    $this->cleanup();
                    break;
                default:
                    break;
            }
        }else {
            $this->update_cap_ids($database);
            $this->update_capreference($database);
            $this->update_optional_extras($database);
            $this->addToCapReference($database);
            $this->cleanup();
        }

    }

    private function setDBTables($database)
    {
        $this->pricestable = $database == 'LCV' ? 'van_pricestable' : $this->pricestable;
        $this->captable = $database == 'LCV' ? 'van_capreference' : $this->captable;
        $this->imageDir = $database == 'LCV' ? '/images/capimages/lcv/' : $this->imageDir;
    }

    /**
     * @param $database
     * @return bool
     */
    private function update_cap_ids($database)
    {
        $sql = "SELECT DISTINCT(cap_id) FROM $this->pricestable WHERE cap_id NOT IN (SELECT cap_id FROM capreference WHERE `database`='" . $database . "') 
        AND cap_id NOT IN (SELECT cap_id FROM $this->captable)";

        $deal = DB::select(DB::raw($sql));

        $this->info("Gathering list of deals that have no CAP data");
        $this->output->progressStart(count($deal));

        foreach ($deal as $cap) {
            DB::table($this->captable)->insert(
                array(
                    'cap_id' => $cap->cap_id,
                    'updated_at' => now(),
                    'invalid' => 0
                )
            );
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();
        return true;
    }

    /**
     * @param $database
     */
    private function update_capreference($database)
    {
        $this->info("Updating basic cap info");
        $count = DB::table($this->captable)
            ->where('invalid','=',0)
            ->whereNull('maker')->count();
        $this->output->progressStart($count);
        DB::table($this->captable)
            ->where('invalid','=',0)
            ->whereNull('maker')
            ->orderBy('cap_id')
            ->chunk(100, function ($cap) use ($database) {
                foreach ($cap as $capReference) {
                    // Grab first set of data
                    $data = $this->capUpdateGatewy->getCapData(
                        config('capupdateservice.clients.cap_hpi.base_wsdl.vehicles.url'),
                        'GetCapDescriptionFromId',
                        ['database' => $database, 'capid' => $capReference->cap_id]
                    );
                    //dd($data);
                    // Cap doesn't have this cap id on record or vehicle type is wrong so remove and add to invalid caps
                    if (empty($data) || empty($data['NewDataSet']) || empty($data['NewDataSet']['Table'])) {
                        $this->info('invalid cap - ' . $capReference->cap_id);
                        DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['invalid'=>1]);
                    } else {
                        $trim = '';
                        if($data['NewDataSet']['Table']['CVehicle_ShortDerText'] && !empty($data['NewDataSet']['Table']['CVehicle_ShortDerText']))
                            $trim = $data['NewDataSet']['Table']['CVehicle_ShortDerText'];
                        else
                            $trim = $data['NewDataSet']['Table']['CVehicle_ModText'];
                        DB::table($this->captable)
                            ->where('cap_id', $capReference->cap_id)
                            ->update([
                                'maker' => $data['NewDataSet']['Table']['CVehicle_ManText'],
                                'range' => $data['NewDataSet']['Table']['CVehicle_ShortModText'],
                                'model' => $data['NewDataSet']['Table']['CVehicle_ModText'],
                                'trim' => $database == 'CARS'? $trim : '',
                                'derivative' => $data['NewDataSet']['Table']['CVehicle_DerText'],
                            ]);

                        $this->formatBulkData($capReference->cap_id);
                    }
                    $this->output->progressAdvance();
                }
                $this->getBulkTechnicalData($database);
                unset($this->capIDs);
                unset($this->capIDDates);
                $this->capIDs = '';
                $this->capIDDates = '';
            });
        $this->output->progressFinish();
    }

    private function formatBulkData($capID)
    {
        $this->capIDs .= ',' . $capID;
        $this->capIDDates .= ',' . date('Y-m-d');
    }

    private function getBulkTechnicalData($database = 'CARS')
    {
        $tecDataList = '0TO62, CC, CO, CO2, MPG_COMBINED, MPG_URBAN, MPG_EXTRAURBAN, ENGINEPOWER_BHP, ENGINEPOWER_KW, ENGINEPOWER_RPM, FUELDELIVERY, FUELTANKCAPACITY, GROSSVEHICLEWEIGHT, HC, HC+NOX, HEIGHT, INSURANCEGROUP1, INSURANCEGROUP2, LENGTH, LUGGAGECAPACITYSEATSDOWN, LUGGAGECAPACITYSEATSUP, NCAPADULTOCCUPANT, NCAPCHILDOCCUPANT, NCAPOVERALLRATING, NCAPPEDESTRIAN, NCAPSAFETYASSIST, SEATS, NOX, STANDARDEMISSIONS, STANDARDMANWARRANTY_MILEAGE, STANDARDMANWARRANTY_YEARS, TOPSPEED, WIDTH';
        $data = $this->capUpdateGatewy->getCapData(
            config('capupdateservice.clients.cap_hpi.base_wsdl.nvd.url'),
            'GetBulkTechnicalData',
            [
                'database' => $database,
                'capidList' => substr($this->capIDs, 1),
                'specDateList' => substr($this->capIDDates, 1),
                'techDataList' => $tecDataList,
                'returnVehicleDescription' => true,
                'returnCaPcodeTechnicalItems' => true,
                'returnCostNew' => true,
            ]
        );

        $disconDate = date('Y') - 3;

        if (!empty($data) && !empty($data['TechData']['Tech_Table'])) {
            foreach ($data['TechData']['Tech_Table'] as $cap) {
                if (isset($cap['ModDiscontinued']) && $cap['ModDiscontinued'] != 0) {
                    if ($cap['ModDiscontinued'] < $disconDate) {
                        DB::table($this->captable)->where('cap_id', $cap['CAPID'])->delete();
                        DB::table('capreference')->where('cap_id', $cap['CAPID'])->delete();
                        DB::table($this->pricestable)->where('cap_id', $cap['CAPID'])->delete();
                        $this->info('Dropping cap id ' . $cap['CAPID'] . ' because of discontinued date ' . $cap['ModDiscontinued']);
                        continue;
                    }

                }
                if (isset($cap['FuelType']) && !is_null($cap['FuelType'])) {
                    if ($database == 'LCV') {
                        switch ($cap['FuelType']) {
                            case "D":
                                $fuel = 'Diesel';
                                break;
                            case "P":
                            case "R":
                                $fuel = 'Petrol';
                                break;
                            case "E":
                                $fuel = 'Electric';
                                break;
                            case "X":
                                $fuel = 'PlugIn Elec Hybrid';
                                break;
                            default:
                                break;
                        }
                    } else {
                        switch ($cap['FuelType']) {
                            case "D":
                                $fuel = 'Diesel';
                                break;
                            case "P":
                            case "G":
                                $fuel = 'Petrol';
                                break;
                            case "E":
                                $fuel = 'Electric';
                                break;
                            case "X":
                                $fuel = 'Petrol PlugIn Elec Hybrid';
                                break;
                            case "H":
                            case "B":
                                $fuel = 'Petrol Electric Hybrid';
                                break;
                            case "C":
                                $fuel = 'Hydrogen';
                                break;
                            case "F":
                                $fuel = "Petrol - Bio Ethanol (E85)";
                                break;
                            case "Y":
                                $fuel = "Diesel Electric Hybrid";
                                break;
                            case "Z":
                                $fuel = 'Diesel PlugIn Elec Hybrid';
                                break;
                            default:
                                break;
                        }
                    }
                }

                if (isset($cap['BodyStyle']) && !is_null($cap['BodyStyle'])) {
                    if ($database == 'CARS') {
                        switch ($cap['BodyStyle']) {
                            case "Hatchback":
                                $bodyStyle = 'Hatchback';
                                break;

                            case "Estate":
                                $bodyStyle = 'Estate';
                                break;

                            case "Saloon":
                                $bodyStyle = 'Saloon';
                                break;

                            case "Coupe":
                                $bodyStyle = 'Coupe';
                                break;

                            case "City-Car":
                                if (strpos(strtoupper($cap['Model']), 'COUPE')) {
                                    $bodyStyle = 'Coupe';
                                }

                                if (strpos(strtoupper($cap['Model']), 'CABRIO')) {
                                    $bodyStyle = 'Convertible';
                                }
                                break;

                            case "4x4":
                                $bodyStyle = 'SUV';
                                break;
                            case "MPV":
                                $bodyStyle = 'People Carrier';
                                break;
                            case 'Sports':
                            case "Cabriolet":
                            case "Convertible":
                                $bodyStyle = 'Convertible';
                                break;
                            default:
                                break;
                        }
                    } else {
                        switch ($cap['BodyStyle']) {
                            case "Hard Top":
                                $bodyStyle = 'Car Van';
                                break;
                            case "Double Cab Chassis":
                            case "Double Cab Dropside":
                            case "Double Cab Tipper":
                            case "Dropside":
                            case "Platform Cab":
                                $bodyStyle = 'Chassis Cab';
                                break;
                            case "Extended Frame Chassis Cab":
                            case "Medium Roof Van":
                            case "Window Van":
                                $bodyStyle = "Van";
                                break;
                            case "Extra High Roof":
                            case "High Volume/High Roof Van":
                                $bodyStyle = 'Box Van';
                                break;
                            case "Medium Roof Window Van":
                            case "High Roof Window Van":

                                $bodyStyle = 'Window Van';
                                break;
                            case "Standard Roof Minibus":
                            case "High Roof Minibus":
                            case "Crew Bus":
                                $bodyStyle = 'Minibus';
                                break;
                            case "Double Cab Pick-up":
                                $bodyStyle = "Pick-up";
                                break;
                            default:
                                $bodyStyle = "Van";
                        }
                    }
                    if ($cap['Manufacturer'] == 'Land Rover') {
                        $bodyStyle = 'SUV';
                    }
                }
                $transmission = '';
                if (isset($cap['Transmission']) && !is_null($cap['Transmission'])) {
                    if ($cap['Transmission'] == 'M'){
                        $transmission = 'Manual';
                    }elseif($cap['Transmission'] == 'A'){
                        $transmission = 'Automatic';
                    }
                }
                DB::table($this->captable)
                    ->where('cap_id', '=', $cap['CAPID'])
                    ->update([
                        'fuel' => $fuel,
                        'transmission' => $transmission,
                        'doors' => isset($cap['Doors']) ? $cap['Doors'] : '',
                        'seats' => isset($cap['SEATS']) ? $cap['SEATS'] : '',
                        'bodystyle' => $bodyStyle,
                        'co2' => isset($cap['CO2']) ? $cap['CO2'] : '',
                        'enginesize' => isset($cap['CC']) ? number_format((float)$cap['CC']/1000, 1) : '',
                        'mpgextraurban' => isset($cap['MPG_EXTRAURBAN']) ? $cap['MPG_EXTRAURBAN'] : '',
                        'mpgcombined' => isset($cap['MPG_COMBINED']) ? $cap['MPG_COMBINED'] : '',
                    ]);
            }
        }
    }

    private function update_optional_extras($database)
    {
        $this->info("Updating Optional Extras");
        $count = DB::table($this->captable)
            ->where('checked_optionals', '=' , 0)
            ->where('invalid', '=' , 0)
            ->count();
        $this->output->progressStart($count);
        $cap = DB::table($this->captable)
            ->where('checked_optionals', '=' , 0)
            ->where('invalid', '=' , 0)
            ->orderBy('cap_id')
            ->chunk(100, function ($cap) use ($database) {
                foreach ($cap as $capReference) {
                    $data = $this->capUpdateGatewy->getCapData(
                        config('capupdateservice.clients.cap_hpi.base_wsdl.nvd.url'),
                        'GetStandardEquipment',
                        [
                            'database' => $database,
                            'capid' => $capReference->cap_id,
                            'seDate' => date('Y-m-d'),
                            'justCurrent' => true
                        ]
                    );

                    if (!empty($data) && !empty($data['StandardEquipement'])) {
                        foreach ($data['StandardEquipement']['SE'] as $item) {
                            $description = strtolower($item['Do_Description']);

                            if (preg_match('(apple)', $description) === 1) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['android_apple' => 1]);
                            }
                            if (preg_match('(android)', $description) === 1) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['android_apple' => 1]);
                            }
                            if (preg_match('(bluetooth)', $description) === 1) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['bluetooth' => 1]);
                            }
                            if (preg_match('(rear)', $description) === 1 && preg_match('(camera)', $description) === 1) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['parking_camera' => 1]);
                            }
                            if (strpos($description, "dab")) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['dab' => 1]);
                            }
                            if (strpos($description, "park") && strpos($description, 'distance') && strpos($description, "control")) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['parking_sensors' => 1]);
                            }
                            if (strpos($description, "park") && strpos($description, 'sensor')) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['parking_sensors' => 1]);
                            }
                            if (strpos($description, "navigation")) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['sat_nav' => 1]);
                            }
                            if (strpos($description, "wireless") && strpos($description, 'charging')) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['wireless_charge' => 1]);
                            }
                            if (preg_match('(alloy)', $description) === 1 && preg_match('(wheel)', $description) === 1) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['alloys' => 1]);
                            }
                            if (preg_match('(climate)', $description) === 1 && preg_match('(control)', $description) === 1) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['climate_control' => 1]);
                            }
                            if (strpos($description, "cruise") && strpos($description, "control")) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['cruise_control' => 1]);
                            }
                            if (preg_match('(heated)', $description) === 1 && preg_match('(seat)', $description) === 1) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['heated_seats' => 1]);
                            }
                            if (preg_match('(leather)', $description) === 1 && preg_match('(seat)', $description) === 1) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['leather_seats' => 1]);
                            }
                            if (preg_match('(leather)', $description) === 1 && preg_match('(upholstery)', $description) === 1) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['leather_seats' => 1]);
                            }
                            if (preg_match('(panoramic)', $description) === 1 && preg_match('(sunroof)', $description) === 1) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['panoramic_roof' => 1]);
                            }
                            if (preg_match('(sunroof)', $description) === 1) {
                                DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['sunroof' => 1]);
                            }
                        }
                    }

                    DB::table($this->captable)->where('cap_id',$capReference->cap_id)
                        ->update(['checked_optionals' => 1]);

                    $this->output->progressAdvance();
                }
            });
        $this->output->progressFinish();
    }

    /**
     * not working code
     * @param $database
     */
    private function update_images($database)
    {
        $this->info("Updating CAP images - " . $database);
        $count = DB::table($this->captable)
            ->whereNull('imageid')
            ->where('invalid', '=' , 0)
            ->count();
        $this->output->progressStart($count);

        $cap = DB::table($this->captable)
            ->whereNull('imageid')
            ->where('invalid', '=' , 0)
            ->orderBy('cap_id')
            ->chunk(100, function ($cap) use ($database) {
                foreach ($cap as $capReference) {
                    $imageId = $capReference->cap_id;
                    $image = $this->capUpdateGatewy->getCapImage(
                        config('capupdateservice.clients.cap_hpi.base_wsdl.images.url'),
                        config('capupdateservice.clients.cap_hpi.base_wsdl.images.method'),
                        [
                            'db' => $database,
                            'capid' => $capReference->cap_id,
                        ]
                    );
                    $destination = public_path() . $this->imageDir . $imageId . '.jpg';

                    if (file_exists($destination)) {
                        unlink($destination);
                    }
                    $fp = fopen($destination, 'x');
                    fwrite($fp, $image);
                    fclose($fp);

                    DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['imageid' => $imageId]);

                    $this->output->progressAdvance();
                }
            });
        $this->output->progressFinish();
    }

    private function addToCapReference($database)
    {
        $this->info("Adding to cap reference table");
        $count = DB::table($this->captable)
            ->whereNotNull('maker')
            ->whereNotNull('range')
            ->whereNotNull('derivative')
            ->whereNotNull('fuel')
            ->whereNotNull('transmission')
            ->whereNotNull('bodystyle')
            ->whereNotNull('doors')
            ->whereNotNull('seats')
            ->where('invalid', '=' , 0)
            ->count();
        $this->output->progressStart($count);
        $carCap = DB::table($this->captable)
            ->whereNotNull('maker')
            ->whereNotNull('range')
            ->whereNotNull('derivative')
            ->whereNotNull('fuel')
            ->whereNotNull('transmission')
            ->whereNotNull('bodystyle')
            ->whereNotNull('doors')
            ->whereNotNull('seats')
            ->where('invalid', '=' , 0)
            ->orderBy('cap_id')
            ->chunk(100, function ($carCap) use ($database) {
                foreach ($carCap as $cap) {
                    $updateCheck = DB::table('capreference')
                        ->where('database', $database)
                        ->where('cap_id', '=', $cap->cap_id)
                        ->first();
                    if ($updateCheck) {
                        $this->info('update cap id ' . $cap->cap_id );

                        DB::table('capreference')
                            ->where('cap_id', $cap->cap_id)
                            ->update([
                                'maker' => $cap->maker,
                                'range' => $cap->range,
                                'model' => $cap->model,
                                'trim' => $cap->trim,
                                'derivative' => $cap->derivative,
                                'fuel' => $cap->fuel,
                                'transmission' => $cap->transmission,
                                'bodystyle' => $cap->bodystyle,
                                'co2' => $cap->co2,
                                'imageid' => $cap->imageid,
                                'enginesize' => $cap->enginesize,
                                'mpgextraurban' => $cap->mpgextraurban,
                                'mpgcombined' => $cap->mpgcombined,
                                'database' => $database,
                                'doors' => $cap->doors,
                                'seats' => $cap->seats,
                                'android_apple' => $cap->android_apple,
                                'bluetooth' => $cap->bluetooth,
                                'parking_camera' => $cap->parking_camera,
                                'dab' => $cap->dab,
                                'parking_sensors' => $cap->parking_sensors,
                                'sat_nav' => $cap->sat_nav,
                                'wireless_charge' => $cap->wireless_charge,
                                'alloys' => $cap->alloys,
                                'climate_control' => $cap->climate_control,
                                'cruise_control' => $cap->cruise_control,
                                'heated_seats' => $cap->heated_seats,
                                'leather_seats' => $cap->leather_seats,
                                'panoramic_roof' => $cap->panoramic_roof,
                                'sunroof' => $cap->sunroof,
                            ]);
                    } else {
                        $this->info('insert cap id ' . $cap->cap_id );

                        DB::table('capreference')->insert(
                            [
                                'cap_id' => $cap->cap_id,
                                'maker' => $cap->maker,
                                'range' => $cap->range,
                                'model' => $cap->model,
                                'trim' => $cap->trim,
                                'derivative' => $cap->derivative,
                                'fuel' => $cap->fuel,
                                'transmission' => $cap->transmission,
                                'bodystyle' => $cap->bodystyle,
                                'co2' => $cap->co2,
                                'imageid' => $cap->imageid,
                                'enginesize' => $cap->enginesize,
                                'mpgextraurban' => $cap->mpgextraurban,
                                'mpgcombined' => $cap->mpgcombined,
                                'doors' => $cap->doors,
                                'seats' => $cap->seats,
                                'android_apple' => $cap->android_apple,
                                'bluetooth' => $cap->bluetooth,
                                'parking_camera' => $cap->parking_camera,
                                'dab' => $cap->dab,
                                'parking_sensors' => $cap->parking_sensors,
                                'sat_nav' => $cap->sat_nav,
                                'wireless_charge' => $cap->wireless_charge,
                                'alloys' => $cap->alloys,
                                'climate_control' => $cap->climate_control,
                                'cruise_control' => $cap->cruise_control,
                                'heated_seats' => $cap->heated_seats,
                                'leather_seats' => $cap->leather_seats,
                                'panoramic_roof' => $cap->panoramic_roof,
                                'sunroof' => $cap->sunroof,
                                'database' => $database,
                            ]
                        );
                    }

                    $this->output->progressAdvance();
                }
            });

        $this->output->progressFinish();

        $this->info("Finished running script!");
        return true;

    }

    private function cleanup()
    {
        $this->info("cleanup empty rows");
        $count = DB::table($this->captable)
            ->whereNull('maker')
            ->whereNull('range')
            ->whereNull('derivative')
            ->whereNull('fuel')
            ->whereNull('transmission')
            ->whereNull('bodystyle')
            ->whereNull('doors')
            ->whereNull('seats')
            ->count();
        $this->output->progressStart($count);

        DB::table($this->captable)
            ->whereNull('maker')
            ->whereNull('range')
            ->whereNull('derivative')
            ->whereNull('fuel')
            ->whereNull('transmission')
            ->whereNull('bodystyle')
            ->whereNull('doors')
            ->whereNull('seats')
            ->delete();
        $this->output->progressFinish();

        $this->info("Finished cleaning up!");
        return true;
    }
}