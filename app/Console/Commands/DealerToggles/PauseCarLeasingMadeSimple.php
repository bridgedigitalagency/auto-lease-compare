<?php

namespace App\Console\Commands\DealerToggles;
ini_set('memory_limit','2048M');

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PauseCarLeasingMadeSimple extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'PauseCarLeasingMadeSimple';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scheduled task to remove deals for Car Leasing Made Simple';
    public function __construct()
    {
        parent::__construct();
        $this->dealerId = 'Car Leasing Made Simple';

    }

    public function handle()
    {
        DB::table('users')
            ->where('name', '=', $this->dealerId)
            ->update(['name'=>$this->dealerId . ' OFFLINE']);
    }
}
