<?php

namespace App\Console\Commands\DealerToggles;
ini_set('memory_limit','2048M');

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ActivateRGWVCDeals extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ActivateRGWVCDeals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scheduled task to add deals for FleetPrices';
    public function __construct()
    {
        parent::__construct();
        $this->dealerId = 'RGW Vehicle Contracts';

    }

    public function handle()
    {

        DB::table('users')
            ->where('name', '=', $this->dealerId . ' OFFLINE')
            ->update(['name'=>$this->dealerId]);

    }
}
