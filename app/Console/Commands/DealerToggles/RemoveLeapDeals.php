<?php

namespace App\Console\Commands\DealerToggles;
ini_set('memory_limit','2048M');

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RemoveLeapDeals extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RemoveLeapDeals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scheduled task to remove deals for Leap Vehicle Leasing';
    public function __construct()
    {
        parent::__construct();
        $this->dealerId = 'Leap Vehicle Leasing';

    }

    public function handle()
    {
        // We used to remove all the deals, now we're changing the username to {dealer_id} OFFLINE
//        DB::table('pricestable')
//            ->where('deal_id', '=', $this->dealerId)
//            ->delete();

        DB::table('users')
            ->where('name', '=', 'Leap Vehicle Leasing')
            ->update(['name'=>'Leap Vehicle Leasing OFFLINE']);

    }
}
