<?php

namespace App\Console\Commands\DealerToggles;
ini_set('memory_limit','2048M');

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ActivateLeapDeals extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ActivateLeapDeals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scheduled task to add deals for Leap Vehicle Leasing';
    public function __construct()
    {
        parent::__construct();
        $this->dealerId = 'Leap Vehicle Leasing';

    }

    public function handle()
    {
        // We used to remove all the deals, now we're changing the username to {dealer_id} OFFLINE
//        DB::table('pricestable')
//            ->where('deal_id', '=', $this->dealerId)
//            ->delete();

        DB::table('users')
            ->where('name', '=', 'Leap Vehicle Leasing OFFLINE')
            ->update(['name'=>'Leap Vehicle Leasing']);

    }
}
