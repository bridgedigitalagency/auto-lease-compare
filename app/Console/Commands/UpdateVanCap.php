<?php

namespace App\Console\Commands;
ini_set('memory_limit', '2048M');
ini_set('max_execution_time', 0);


use App\Models\Cap;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use SoapClient;
use SimpleXMLElement;

class UpdateVanCap extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateVanCap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update cap data for vans';

    // move this into config file
    public $subscriberId = 175356;
    public $password = "Jlerl67";

    public $wsdl = "https://soap.cap.co.uk/Vehicles/CapVehicles.asmx?wsdl";
    public $nvdWsdl = "https://soap.cap.co.uk/Nvd/CapNvd.asmx?wsdl";
    public $imageWsdl = "https://soap.cap.co.uk/images/VehicleImage.aspx?wsdl";
    public $database = "LCV";

    public $feedDir = "";

    public function __construct()
    {

        parent::__construct();
    }

    public function handle()
    {
$this->info('updating cap ids');
        $this->update_van_cap_ids(1);
        $caps = $this->update_van_cap_ids(2);
        $this->info('updating capreferebce');
        $updateCaps = $this->update_capreference();
        $this->info('updating optional extras');
        $updateCaps = $this->update_optional_extras();


//        if($updateCaps) {
//            $images = $this->update_images();
//        }

//        $this->cleanup();
        $this->info('adding to capreference');
//        $this->addToCapReference();

    }



    /**
     * @return bool
     * @throws \SoapFault
     */
    private function update_van_cap_ids($method)
    {
        if($method == 1) {

            $deal = DB::table('van_pricestable')
                ->distinct('cap_code')
                ->whereRaw('cap_code NOT IN (SELECT DISTINCT(cap_code) FROM van_capreference)')
                ->whereRaw('cap_code NOT IN (SELECT DISTINCT(cap_code) FROM van_invalid_cap)')
                ->get();


            $counter = 0;

            foreach ($deal as $capCode) {

                $url = "https://soap.cap.co.uk/Vehicles/CapVehicles.asmx/GetCapidFromCapcode";
                $args = array(
                    'subscriberId' => $this->subscriberId,
                    'password' => $this->password,
                    'database' => $this->database,
                    'caPcode' => $capCode->cap_code

                );
                $client = $this->executeQuery($url, $args);

                try {
                    $result = $client->GetCapidFromCapcode($args);
                    $result = $result->GetCapidFromCapcodeResult->Returned_DataSet->any;

                } catch (SoapFault $e) {
                    echo "Error: {$e}";
                }

                $xml = simplexml_load_string($result);

                if (count($xml) < 1) {

                    DB::table('van_invalid_cap')->insert(
                        array(
                            'cap_code' => $capCode->cap_code,
                        )
                    );

                } else {
                    foreach ($xml as $item) {
                        foreach ($item as $cap) {
                            $capId = $cap->CDer_ID;

                            $capCheck = DB::table('van_capreference')
                                ->where('cap_id', '=', $capId)
                                ->get();

                            if (count($capCheck) == 0) {

                                DB::table('van_capreference')->insert(
                                    array(
                                        'cap_id' => $capId,
                                        'cap_code' => $capCode->cap_code,
                                        'updated_at' => now()
                                    )
                                );
                            }

                            $counter++;
                        }
                    }
                }

            }

        }elseif($method == 2) {

            $deal = DB::table('van_pricestable')
                ->whereraw("cap_id NOT IN (SELECT cap_id FROM capreference WHERE `database`='LCV')")
                ->get();

            foreach($deal as $cap) {

                $capCheck = DB::table('van_capreference')
                    ->where('cap_id', '=', $cap->cap_id)
                    ->get();

                if (count($capCheck) == 0) {

                    DB::table('van_capreference')->insert(
                        array(
                            'cap_id' => $cap->cap_id,
                            'updated_at' => now()
                        )
                    );
                }
            }
        }


        return true;
    }

    /**
     * @return bool
     * @throws \SoapFault
     */
    private function update_capreference()
    {


        $cap = DB::table('van_capreference')
            ->whereNull('maker')
            ->get();


        foreach ($cap as $capReference) {


            // Grab first set of data
            $url = "https://soap.cap.co.uk/Vehicles/CapVehicles.asmx/GetCapDescriptionFromId";
            $args = array(
                'subscriberId' => $this->subscriberId,
                'password' => $this->password,
                'database' => $this->database,
                'capid' => $capReference->cap_id

            );
            $client = $this->executeQuery($url, $args);

            try {
                $result = $client->GetCapDescriptionFromId($args);
                $result = $result->GetCapDescriptionFromIdResult->Returned_DataSet->any;

            } catch (SoapFault $e) {
                echo "Error: {$e}";
                return false;
            }

            $xml = simplexml_load_string($result);

            if (count($xml) < 1) {


            } else {

                foreach ($xml as $item) {
                    foreach ($item as $cap) {


                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update([
                            'maker' => $cap->CVehicle_ManText,
                            'range' => $cap->CVehicle_ShortModText,
                            'model' => $cap->CVehicle_ModText,
                            'trim' => $cap->CVehicle_ShortDerText,
                            'derivative' => $cap->CVehicle_DerText,

                        ]);

                    }
                }
            }


            // grab all the technical info
            $url = "https://soap.cap.co.uk/Nvd/CapNvd.asmx/GetTechnicalData";
            $args = array(
                'subscriberId' => $this->subscriberId,
                'password' => $this->password,
                'database' => $this->database,
                'capid' => $capReference->cap_id,
                'techDate' => date('Y-m-d'),
                'justCurrent' => true

            );
            $client = $this->executeNVDQuery($url, $args);

            try {
                $result = $client->GetTechnicalData($args);
                $result = $result->GetTechnicalDataResult->Returned_DataSet->any;

            } catch (SoapFault $e) {
                echo "Error: {$e}";
                return false;
            }

            $xml = simplexml_load_string($result);

            if (count($xml) < 1) {


            } else {
                foreach ($xml as $item) {

                    foreach ($item as $cap) {

                        switch ($cap->DT_LongDescription) {
                            case "CO2":
                                if (count($cap->tech_value_string) > 0) {
                                    if($cap->tech_value_string != 'Not Available') {
                                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['co2' => $cap->tech_value_string]);
                                    }
                                }
                                break;
                            case "Badge Engine CC":
                                if (count($cap->tech_value_string) > 0) {
                                    DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['enginesize' => $cap->tech_value_string]);
                                }
                                break;
                            case "EC Extra Urban (mpg)":
                                if (count($cap->tech_value_string) > 0) {
                                    DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['mpgextraurban' => $cap->tech_value_string]);
                                }
                                break;
                            case "EC Combined (mpg)":
                                if (count($cap->tech_value_string) > 0) {
                                    DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['mpgcombined' => $cap->tech_value_string]);
                                }
                                break;
                        }

                    }
                }
            }
        }

        $cap = DB::table('van_capreference')

            ->get();


        foreach ($cap as $capReference) {

            $url = "https://soap.cap.co.uk/Nvd/CapNvd.asmx/GetBulkTechnicalData";
            $args = array(
                'subscriberId' => $this->subscriberId,
                'password' => $this->password,
                'database' => $this->database,
                'capidList' => "$capReference->cap_id",
                'specDateList' => date('Y/m/d'),
                'techDataList' => '',
                'returnVehicleDescription' => true,
                'returnCaPcodeTechnicalItems' => true,
                'returnCostNew' => true,


            );

            $client = $this->executeNVDQuery($url, $args);

            try {
                $result = $client->GetBulkTechnicalData($args);
                $result = $result->GetBulkTechnicalDataResult->Returned_DataSet->any;

            } catch (SoapFault $e) {
                echo "Error: {$e}";
                return false;
            }

            $xml = simplexml_load_string($result);
            if (count($xml) < 1) {


            } else {
                foreach ($xml as $item) {
                    foreach ($item as $cap) {

                        // Fuel Update
                        if (isset($cap->FuelType) && !is_null($cap->FuelType)) {

                            switch ($cap->FuelType) {
                                case "D":
                                    $fuel = 'Diesel';
                                    break;
                                case "P":
                                case "R":
                                    $fuel = 'Petrol';
                                    break;
                                case "E":
                                    $fuel = 'Electric';
                                    break;
                                case "X":
                                    $fuel = 'PlugIn Elec Hybrid';
                                    break;
                                default:
                                    var_dump($cap);
                                    die();
                                    break;
                            }

                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['fuel' => $fuel]);
                        }

                        // Transmission update
                        if (isset($cap->FuelType) && !is_null($cap->Transmission)) {
                            if($cap->Transmission == 'M') {
                                $transmission = 'Manual';
                            }else{
                                $transmission = 'Automatic';
                            }
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['transmission' => $transmission]);
                        }

                        // Doors update
                        if (isset($cap->FuelType) && !is_null($cap->Doors)) {
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['doors' => $cap->Doors]);
                        }

                        if (isset($cap->BodyStyle) && !is_null($cap->BodyStyle)) {

                            switch ($cap->BodyStyle) {
                                case "Hard Top":
                                    $bodyStyle = 'Car Van';
                                break;
                                case "Double Cab Chassis":
                                case "Double Cab Dropside":
                                case "Double Cab Tipper":
                                case "Dropside":
                                case "Platform Cab":
                                    $bodyStyle = 'Chassis Cab';
                                break;
                                case "Extended Frame Chassis Cab":
                                case "Medium Roof Van":
                                case "Window Van":
                                    $bodyStyle = "Van";
                                break;
                                case "Extra High Roof":
                                case "High Volume/High Roof Van":
                                    $bodyStyle = 'Box Van';
                                break;
                                case "Medium Roof Window Van":
                                case "High Roof Window Van":

                                    $bodyStyle = 'Window Van';
                                break;
                                case "Standard Roof Minibus":
                                case "High Roof Minibus":
                                case "Crew Bus":
                                    $bodyStyle = 'Minibus';
                                break;
                                case "Double Cab Pick-up":
                                    $bodyStyle = "Pick-up";
                                    break;
                                default:
                                    $bodyStyle = "Van";
                            }

                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['bodystyle' => $bodyStyle]);
                        }

                    }
                }
            }

        }

        return true;
    }

    private function update_images() {

        $imageDir = 'public/images/capimages/lcv/';

        $cap = DB::table('van_capreference')
            ->whereNull('imageid')
            ->get();

        foreach($cap as $capReference) {

            $imageId = $capReference->cap_id;

            $strtomd5 = $this->subscriberId . $this->password  . $this->database . $capReference->cap_id;
            $hashcode = strtoupper(md5($strtomd5));

            $url = "https://soap.cap.co.uk/images/VehicleImage.aspx";
            $url .= "?subid=" . $this->subscriberId;
            $url .= "&db=" . $this->database;
            $url .= "&hashcode=" . $hashcode;
            $url .= "&capid=" . $capReference->cap_id;
            $url .= "&date=" . date('Y-m-d');


            $destination = public_path() . "/images/capimages/LCV/" . $imageId . ".jpg";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $data = curl_exec ($ch);
            curl_close ($ch);

            $file = fopen($destination, "w+");
            fputs($file, $data);
            fclose($file);

            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['imageid' => $imageId]);

        }

    }

    private function update_optional_extras() {

        $cap = DB::table('van_capreference')
            ->whereRaw('cap_id NOT IN (SELECT cap_id FROM checked_optionals WHERE `database`="LCV")')
            ->get();
        $this->info("Updating Optional Extras");
        $this->output->progressStart(count($cap));

        foreach($cap as $capReference) {

            $url = "https://soap.cap.co.uk/Nvd/CapNvd.asmx/GetCapOptionsBundle";
            $args = array(
                'subscriberId' => $this->subscriberId,
                'password' => $this->password,
                'database' => $this->database,
                'capid' => $capReference->cap_id,
                'seDate' => date('Y-m-d'),
                'justCurrent' => true,
            );

            $client = $this->executeNVDQuery($url, $args);

            try {
                $result = $client->GetStandardEquipment($args);
                $result = $result->GetStandardEquipmentResult->Returned_DataSet->any;

            } catch (SoapFault $e) {
                echo "Error: {$e}";
                return false;
            }

            $xml = simplexml_load_string($result);

            $this->info('======' . $capReference->cap_id . '=======');

            if (count($xml) < 1 || is_null($xml)) {


            } else {

                foreach ($xml->StandardEquipement->SE as $item) {
//                    $this->info($item->Do_Description);

                    $description = strtolower($item->Do_Description);

                    if(preg_match('(sunroof)', $description) === 1) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['sunroof' => 1]);
                    }

                    if(preg_match('(panoramic)', $description) === 1 && preg_match('(sunroof)', $description) === 1) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['panoramic_roof' => 1]);
                    }
                    if(preg_match('(climate)', $description) === 1 && preg_match('(sunroof)', $description) === 1) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['climate_control' => 1]);
                    }
                    if(preg_match('(bluetooth)', $description) === 1) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['bluetooth' => 1]);
                    }
                    if(preg_match('(apple)', $description) === 1) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['android_apple' => 1]);
                    }
                    if(preg_match('(android)', $description) === 1) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['android_apple' => 1]);
                    }
                    if(preg_match('(reverse)', $description) === 1 && preg_match('(camera)', $description) === 1) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['parking_camera' => 1]);
                    }
                    if(preg_match('(alloy)', $description) === 1 && preg_match('(wheel)', $description) === 1) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['alloys' => 1]);
                    }
                    if(preg_match('(heated)', $description) === 1 && preg_match('(seat)', $description) === 1) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['heated_seats' => 1]);
                    }
                    if(preg_match('(leather)', $description) === 1 && preg_match('(seat)', $description) === 1) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['leather_seats' => 1]);
                    }
                    if(preg_match('(leather)', $description) === 1 && preg_match('(upholstery)', $description) === 1) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['leather_seats' => 1]);
                    }

                    if(strpos(strtolower($item->Do_Description), "park") && strpos(strtolower($item->Do_Description), 'distance') && strpos(strtolower($item->Do_Description), "control")) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['parking_sensors' => 1]);
                    }
                    if(strpos(strtolower($item->Do_Description), "park") && strpos(strtolower($item->Do_Description), 'sensor')) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['parking_sensors' => 1]);
                    }
                    if(strpos(strtolower($item->Do_Description), "dab")) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['dab' => 1]);
                    }
                    if(strpos(strtolower($item->Do_Description), "navigation")) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['sat_nav' => 1]);
                    }
                    if(strpos(strtolower($item->Do_Description), "wireless") && strpos(strtolower($item->Do_Description), 'charging')) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['wireless_charge' => 1]);
                    }
                    if(strpos(strtolower($item->Do_Description), "cruise") && strpos(strtolower($item->Do_Description), "control")) {
                        DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['cruise_control' => 1]);
                    }
                }
            }


            DB::table('checked_optionals')->insert(
                array(
                    'cap_id' => $capReference->cap_id,
                    'database' => 'LCV'
                )
            );

            $this->output->progressAdvance();
        }
        $this->output->progressFinish();
    }


    private function update_optional_extrasold() {

        $cap = DB::table('van_capreference')
            ->whereRaw('cap_id NOT IN (SELECT cap_id FROM van_checked_optionals)')
            ->get();

        foreach($cap as $capReference) {
            $url = "https://soap.cap.co.uk/Nvd/CapNvd.asmx/GetCapOptionsBundle";
            $args = array(
                'subscriberId' => $this->subscriberId,
                'password' => $this->password,
                'database' => $this->database,
                'capid' => $capReference->cap_id,
                'optionDate' => date('Y-m-d'),
                'justCurrent' => true,
                'descriptionRs' => true,
                'optionsRs' => true,
                'relationshipsRs' => true,
                'packRs' => true,
                'technicalRs' => true

            );

            $client = $this->executeNVDQuery($url, $args);

            try {
                $result = $client->GetCapOptionsBundle($args);
                $result = $result->GetCapOptionsBundleResult->Returned_DataSet->any;

            } catch (SoapFault $e) {
                echo "Error: {$e}";
                return false;
            }

            $xml = simplexml_load_string($result);
            if (count($xml) < 1) {


            } else {
                foreach ($xml->NVDBundle->Options as $item) {

                    if(strpos($item->Do_Description, 'bluetooth')) {
                        if($item->Opt_Default == 'true') {
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['bluetooth' => 1]);
                        }
                    }
                    if(strpos($item->Do_Description, 'Reversing camera')) {
                        if($item->Opt_Default == 'true') {
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['parking_camera' => 1]);
                        }
                    }

                    if(strpos($item->Do_Description, 'alloy wheels')) {
                        if($item->Opt_Default == 'true') {
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['alloys' => 1]);
                        }
                    }
                    if(strpos($item->Do_Description, "Heated driver's seat")) {
                        if($item->Opt_Default == 'true') {
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['heated_seats' => 1]);
                        }
                    }
                    if(strpos($item->Do_Description, "Leatherette")) {
                        if($item->Opt_Default == 'true') {
                            DB::table('van_capreference')->where('cap_id', $capReference->cap_id)->update(['leather_seats' => 1]);
                        }
                    }
                }
            }


            DB::table('van_checked_optionals')->insert(
                array(
                    'cap_id' => $capReference->cap_id,
                )
            );

        }
    }

    private function addToCapReference() {

        $vanCap = DB::table('van_capreference')
            ->get();

        foreach($vanCap as $cap) {

            $updateCheck = DB::table('capreference')
                ->where('cap_id', '=', $cap->cap_id)
                ->where('database','=','LCV')
                ->first();

            if($updateCheck) {
                DB::table('capreference')
                    ->where('cap_id', $cap->cap_id)
                    ->update([
                        'maker' => $cap->maker,
                        'range' => $cap->range,
                        'model' => $cap->model,
                        'trim' => $cap->trim,
                        'derivative' => $cap->derivative,
                        'fuel' => $cap->fuel,
                        'transmission' => $cap->transmission,
                        'doors' => $cap->doors,
                        'bodystyle' => $cap->bodystyle,
                        'co2' => $cap->co2,
                        'imageid' => $cap->imageid,
                        'enginesize' => $cap->enginesize,
                        'mpgextraurban' => $cap->mpgextraurban,
                        'mpgcombined' => $cap->mpgcombined,
                        'database' => 'LCV',
                    ]);
            }else {

                $capreference = new Cap();
                $capreference->cap_id = $cap->cap_id;
                $capreference->maker = $cap->maker;
                $capreference->range = $cap->range;
                $capreference->model = $cap->model;
                $capreference->trim = $cap->trim;
                $capreference->derivative = $cap->derivative;
                $capreference->fuel = $cap->fuel;
                $capreference->transmission = $cap->transmission;
                $capreference->doors = $cap->doors;
                $capreference->bodystyle = $cap->bodystyle;
                $capreference->co2 = $cap->co2;
                $capreference->imageid = $cap->imageid;
                $capreference->enginesize = $cap->enginesize;
                $capreference->mpgextraurban = $cap->mpgextraurban;
                $capreference->mpgcombined = $cap->mpgcombined;
                $capreference->database = 'LCV';
                $capreference->save();
            }

        }

        return true;

    }

    private function cleanup() {
        DB::table('van_capreference')
            ->update(['database' => 'LCV',
            ]);
    }


    /**
     * @param $url
     * @return SoapClient
     * @throws \SoapFault
     */
    private function executeQuery($url)
    {

        $wsdl = $this->wsdl;
        $client = new SoapClient($wsdl, array('soap_version' => SOAP_1_1, 'trace' => true, 'keep_alive' => false));

        return $client;

    }

    /**
     * @param $url
     * @return SoapClient
     * @throws \SoapFault
     */
    private function executeNVDQuery($url)
    {

        $wsdl = $this->nvdWsdl;
        $client = new SoapClient($wsdl, array('soap_version' => SOAP_1_1, 'trace' => true, 'keep_alive' => false));

        return $client;

    }

    /**
     * @param $url
     * @return SoapClient
     * @throws \SoapFault
     */
    private function executeImageQuery($url)
    {

        $wsdl = $this->imageWsdl;
        $client = new SoapClient($wsdl, array('soap_version' => SOAP_1_1, 'trace' => true, 'keep_alive' => false));

        return $client;

    }


}
