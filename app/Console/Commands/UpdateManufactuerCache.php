<?php

namespace App\Console\Commands;
ini_set('memory_limit','2048M');

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Pricing;
use App\Models\Cap;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UpdateManufactuerCache  extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:ManufacturerCache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update price history table and clean up prices table';



    public function __construct()
    {
        parent::__construct();

        $this->databases = array('CARS', 'LCV');
    }

    public function handle()
    {

        $this->slackUrl = 'https://hooks.slack.com/services/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';
//        $this->slackUrl = 'https://hooks.slack.com/services/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';

        $sendToSlack = 0;

        $header = 'Manufacturer Cache Script:';
        $content = array();
        $content['blocks'][] = array(
            'type'=>'header',
            'text'=>array(
                'type'=>'plain_text',
                'text'=>$header
            )
        );

        $start = time();
        // First clear out yesterdays results as not needed
        $this->truncateTables();

        // Grab all the manufacturers for each database type and store
        $this->updateManufacturers();

        // Check each manufacturer, if we dont have a deal for them change the status to 0
        $this->checkManufacturerStatus();

        // Grab ranges for each active maker
        $this->updateRanges();

        // grab the prices
        $this->grabPrices();

        $end = time();
        $this->info('Script processed in ' . gmdate("H:i:s", ($end - $start)));
        if($sendToSlack == 1) {
            $content['blocks'][] = array(
                'type' => 'section',
                'text' => array(
                    'type' => 'plain_text',
                    'text' => 'Script processed in ' . gmdate("H:i:s", ($end - $start))
                )
            );
            $content = json_encode($content);


            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $this->slackUrl);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Content-Length: ' . strlen($content)));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec($ch);

            curl_close($ch);
        }

    }

    /**
     * Clear out yesterdays data
     */
    private function truncateTables() {

        DB::table('manufacturers_search')->truncate();
        DB::table('manufacturers_search_ranges')->truncate();
        DB::table('manufacturers_search_prices')->truncate();

    }

    /**
     *
     */
    private function updateManufacturers()
    {

        foreach ($this->databases as $database) {

            // Update cars first
            $cars = Cap::Where('database', '=', $database)
                ->SELECT(DB::raw('DISTINCT(maker)'))
                ->get();

            foreach ($cars as $carMaker) {

                DB::table('manufacturers_search')->insert(
                    [
                        'maker' => $carMaker->maker,
                        'status' => 1,
                        'database' => $database
                    ]
                );

            }
        }
    }

    /**
     *
     */
    private function checkManufacturerStatus() {

        $manuSearch = DB::table('manufacturers_search')
            ->get();

        foreach($manuSearch as $manu) {

            if($manu->database == 'CARS') {
                $pricestable = 'pricestable';
            }else{
                $pricestable = 'van_pricestable';
            }

            $priceCheck = DB::table($pricestable)
                ->whereIn('cap_id', function($query) use ($manu) {

                    $query->select('cap_id')->from('capreference')
                        ->where('maker', '=', $manu->maker)
                        ->where('database', '=', $manu->database)
                        ->get();

                    return $query;
                })
                ->join('users', $pricestable .'.deal_id', '=', 'users.name')
                ->first();

            if($priceCheck) {
                $this->info('Manufacturer ' . $manu->maker . ' deals found ');
            }else{

                DB::table('manufacturers_search')->where('id', '=', $manu->id)->update(
                    ['status' => 0]
                );
                $this->info('Manufacturer ' . $manu->maker . ' no deals found ');
            }
        }
    }

    /**
     *
     */
    private function updateRanges() {

        $manuSearch = DB::table('manufacturers_search')
            ->where('status', '=', 1)
            ->get();

        foreach($manuSearch as $maker) {

            $ranges = DB::table('capreference')
                ->select(DB::raw('DISTINCT(`range`)'))
                ->Where('maker', '=', $maker->maker)
                ->where('database', '=', $maker->database)


                ->get();

            if($ranges) {

                foreach ($ranges as $range) {

                    DB::table('manufacturers_search_ranges')->insert(
                        [
                            'maker' => $maker->maker,
                            'range' => $range->range,
                            'status' => 1,
                            'database' => $maker->database
                        ]
                    );
                }
            }
        }
    }

    /**
     *
     */
    private function grabPrices() {

        $ranges = DB::table('manufacturers_search_ranges')
            ->where('status', '=', 1)
            ->get();

        foreach($ranges as $range) {

            $this->info("Grabbing price info for " . $range->maker . " - " . $range->range);

            if($range->database == 'LCV') {
                $pricestable = "van_pricestable";
                $imgurl = 'LCV/';
            }else{
                $pricestable = "pricestable";
                $imgurl = '';
            }

            // query prices table to get business and personal lowest price for range

            $personal = DB::table($pricestable)
                ->join('capreference', 'capreference.cap_id', '=', $pricestable . '.cap_id')
                ->whereRaw($pricestable . ".cap_id IN (SELECT cap_id FROM capreference WHERE `range` = '$range->range' AND `maker` = '$range->maker')")
                ->where('finance_type', '=', 'P')
                ->join('users', $pricestable .'.deal_id', '=', 'users.name')
                ->orderBy('monthly_payment')
                ->first();

            $business = DB::table($pricestable)
                ->join('capreference', 'capreference.cap_id', '=', $pricestable . '.cap_id')
                ->whereRaw($pricestable . ".cap_id IN (SELECT cap_id FROM capreference WHERE `range` = '$range->range' AND `maker` = '$range->maker')")
                ->where('finance_type', '=', 'B')
                ->join('users', $pricestable .'.deal_id', '=', 'users.name')
                ->orderBy('monthly_payment')
                ->first();

            $imageCheck = DB::table('capreference')
                ->where('maker', '=', $range->maker)
                ->where('range', '=', $range->range)
                ->where('database', '=', $range->database)
                ->where('imageid', '>', 0)
                ->first();

            // We add £1 to the prices
            if(isset($personal->monthly_payment)) {
                $personalPayment = ($personal->monthly_payment + 1);
            }else{
                $personalPayment = null;
            }

            if(isset($business->monthly_payment)) {
                $businessPayment = ($business->monthly_payment + 1);
            }else{
                $businessPayment = null;
            }
            // if we have deals then store prices
            if(!is_null($personalPayment) && !is_null($businessPayment)) {
                DB::table('manufacturers_search_prices')->insert(
                    [
                        'range_id' => $range->id,
                        'personal_price' => $personalPayment,
                        'business_price' => $businessPayment
                    ]
                );

                if(isset($imageCheck->imageid)) {
                    DB::table('manufacturers_search_ranges')->where('id', '=', $range->id)->update(
                        [
                            'imageid' => $imageCheck->imageid,
                            'image_cap_id' => $imageCheck->cap_id
                        ]
                    );
                }


            }else{
                // if we dont have deals then disable this range
                DB::table('manufacturers_search_ranges')->where('id', '=', $range->id)->update(
                    ['status' => 0]
                );
            }
        }
    }
}