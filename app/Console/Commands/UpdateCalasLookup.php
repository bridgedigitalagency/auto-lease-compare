<?php

namespace App\Console\Commands;
ini_set('memory_limit', '2048M');

use Illuminate\Console\Command;
use App\Models\PriceHistory;
use App\Models\Pricing;
use App\Models\Cap;
use Illuminate\Support\Facades\DB;

class UpdateCalasLookup extends Command
{

    protected $signature = 'updatecalaslookup';

    public function __construct()
    {
        parent::__construct();
        $this->lookupFile = '/home/autoleasecompare/dealers/calas/cars/calas-vehicle-lookup.csv';
    }

    public function handle()
    {

        $this->processLookupFile();

        $this->fetchCapData();

    }

    private function processLookupFile() {


        DB::table('calas_lookup')->truncate();


        $reader = fopen($this->lookupFile, "r");

        // read the first line and ignore it
        fgets($reader);

        while (($file = fgetcsv($reader, 500, ',')) !== FALSE) {

            if(!isset($file[11]) || is_null($file[11]) || $file[11]=="") {
                $co2 = 0;
            }else{
                $co2 = $file[11];
            }

            if(!isset($file[14]) || is_null($file[14]) || $file[14]=="") {
                $combinedMPG = '0.0';
            }else{
                $combinedMPG = $file[14];
            }

            if(!isset($file[11]) || is_null($file[11]) || $file[11]=="") {
                $listPrice = '0.0';
            }else{
                $listPrice = $file[11];
            }

            if(!isset($file[13]) || is_null($file[13]) || $file[13]=="") {
                $p11d = '0.0';
            }else{
                $p11d = $file[13];
            }

            DB::table('calas_lookup')->insert(
                [
                    "CalasVehicleId" => $file[0],
                    "VehicleType" => $file[1],
                    "Manufacturer" => $file[2],
                    "ModelRange" => $file[3],
                    "Model" => $file[4],
                    "Derivative" => $file[5],
                    "BodyStyle" => $file[6],
                    "RangeBody" => $file[7],
                    "ModelYear" => $file[8],
                    "ReleaseDate" => $file[9],
                    "DiscontinuedDate" => $file[10],
                    "ListPrice" => $listPrice,
                    "Co2" => $co2,
                    "P11D" => $p11d,
                    "CombinedMPG" => $combinedMPG,
                    "GrossVehicleWeight" => $file[15],
                    "KerbWeight" => $file[16]
                ]
            );

        }

    }
}