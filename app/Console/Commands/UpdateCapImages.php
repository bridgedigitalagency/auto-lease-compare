<?php

namespace App\Console\Commands;

use App\Interfaces\CapUpdateImageGateway;
use App\Interfaces\CDNInterface;
use App\Repositories\CapImageRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UpdateCapImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateCapImages {type} {step?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Cap Images from car and driving apis, cars/lcv';
    protected $capUpdateImageGateway;
    protected $cdnInterface;
    protected $capImageRepository;
    protected $pricestable = 'pricestable';
    protected $captable = 'car_capreference';
    protected $imageDir = '/images/capimages/';
    protected $capIDs;
    protected $capIDDates;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CapUpdateImageGateway $capUpdateImageGateway , CDNInterface $cdnInterface, CapImageRepository $capImageRepository)
    {
        $this->capUpdateImageGateway = $capUpdateImageGateway;
        $this->cdnInterface = $cdnInterface;
        $this->capImageRepository = $capImageRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $database = strtoupper($this->argument('type'));
        $step = $this->argument('step');

        $this->info('Processing ' . $database);
        $this->setDBTables($database);

        //update images with 0 value to NULL to be rechecked again weekly
        if(Carbon::today()->dayName == 'Sunday')
            $this->weeklyRecheckImagelessModels();

        if(isset($step) && !empty($step)){
            switch ($step) {
                case 1:
                    $this->update_images($database);
                    break;
                case 2:
                    $this->addToCapReference($database);
                    break;
                default:
                    break;
            }
        }else{
            $this->update_images($database);
            $this->addToCapReference($database);
        }

    }

    private function setDBTables($database)
    {
        $this->pricestable = $database == 'LCV' ? 'van_pricestable' : $this->pricestable;
        $this->captable = $database == 'LCV' ? 'van_capreference' : $this->captable;
        $this->imageDir = $database == 'LCV' ? '/images/capimages/lcv/' : $this->imageDir;
    }

    private function update_images($database)
    {
        $this->info("Updating CAP images - " . $database);
        $count = DB::table($this->captable)
            ->whereNull('imageid')
            ->where('invalid', '=' , 0)
            ->count();
        $this->output->progressStart($count);

        $cap = DB::table($this->captable)
            ->whereNull('imageid')
            ->where('invalid', '=' , 0)
            ->orderBy('cap_id')
            ->chunk(100, function ($cap) use ($database) {
                foreach ($cap as $capReference) {
                    $imageUrls = $this->capUpdateImageGateway->getCapImage(
                        config('capupdateservice.clients.CAD.images.url'),
                        'GetReviewsFromCapID',
                        [
                            'capid' => $capReference->cap_id,
                            'type' => $database == 'CARS' ? 'all' : 'van'
                        ]
                    );

                    if(!empty($imageUrls)){
                        foreach ($imageUrls as $image){
                            $image_name = $this->cdnInterface->store($image, $database);
                            $this->capImageRepository->store($capReference->cap_id, $image_name, $image['is_default'],$database);
                        }
                    }

                    DB::table($this->captable)->where('cap_id', $capReference->cap_id)->update(['imageid' => count($imageUrls)]);
                    $this->output->progressAdvance();
                }
            });
        $this->output->progressFinish();
    }

    private function addToCapReference($database)
    {
        $this->info("Adding to cap reference table");
        $count = DB::table($this->captable)
            ->where('invalid', '=' , 0)
            ->count();
        $this->output->progressStart($count);
        $carCap = DB::table($this->captable)
            ->where('invalid', '=' , 0)
            ->orderBy('cap_id')
            ->chunk(100, function ($carCap) use ($database) {
                foreach ($carCap as $cap) {
                    $updateCheck = DB::table('capreference')
                        ->where('database', $database)
                        ->where('cap_id', '=', $cap->cap_id)
                        ->first();
                    if ($updateCheck) {
                        $this->info('update cap id ' . $cap->cap_id );

                        DB::table('capreference')
                            ->where('cap_id', $cap->cap_id)
                            ->update([
                                'imageid' => $cap->imageid,
                            ]);
                    }
                    $this->output->progressAdvance();
                }
            });

        $this->output->progressFinish();

        $this->info("Finished running script!");
        return true;

    }

    private function weeklyRecheckImagelessModels()
    {
        DB::table($this->captable)
            ->where('imageid', '=' , 0)
            ->update(['imageid'=> NULL]);
        return true;
    }
}
