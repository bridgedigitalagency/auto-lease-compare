<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class UpdateTrustPilot extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updatetrustpilot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update trust pilot details for each dealer';

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * @var array[] of dealers
     */
    protected $dealers = array(
        'lease4less' => array(
            'did' => '51facf6700006400056df48c',
            'uid' => 23
        ),

        'Car Leasing-Online' => array(
            'did' => '58dc09290000ff00059f7819',
            'uid' => 623
        ),
        'Blue Chilli Cars' => array(
            'did' => '5c8927fbe271f70001bac468',
            'uid' => 688
        ),
//
        'Leap Vehicle Leasing' => array(
            'did' => '5c420dc67ba5ed0001fe51d4',
            'uid' => 870
        ),
        'GB Vehicle Leasing' => array(
            'did' => '59ce0a580000ff0005ac5712',
            'uid' => 875
        ),
        'LeaseShop' => array(
            'did' => '5a33cd16b894c90820307a12',
            'uid' => 706
        ),
        'Simple Leasing' => array(
            'did' => '5cd697f1705d540001f48185',
            'uid' => 517
        ),
        'Car Lease UK' => array(
            'did' => '533582d800006400057878bb',
            'uid' => 675
        ),

        'TotalVehicleLeasing' => array(
            'did' => '5bd70e27862e17000125c831',
            'uid' => 2577
        ),
        'FleetPrices' => array(
            'did' => '598ccfdb0000ff0005a897c8',
            'uid' => 40
        ),
        'Rivervale' => array(
            'did' => '5166b4aa000064000525e04b',
            'uid' => 3839
        ),
        'Autorama' => array(
            'did' => '594a982f0000ff0005a50d80',
            'uid' => 3955
        ),

        'Dreamlease' => array(
            'did' => '5ca6402552585d00018f91f9',
            'uid' => 5536
        ),

        'Champion Car Leasing' => array(
            'did' => '5f7f64a06e2f860001bc7b4a',
            'uid' => 7328
        ),

        'Car Leasing Made Simple' => array(
            'did' => '51fa9e4700006400056df0b0',
            'uid' => 7942
        ),
        'Ask 4 Leasing' => array(
            'did' => '5c780717e6713b000151e9f2',
            'uid' => 8033
    ),

        'Britannia Car Leasing' => array(
            'did' => '5b8e4a86e0ee2e0001c369c3',
            'uid' => 10478
        ),
        'Leasing Options' => array(
            'did' => '54352e8600006400057acca9',
            'uid' => 10831
        ),
	    'Car Lease Agent' => array(
            'did' => '5d442275331984000126641f',
            'uid' => 11695
        ),
        'LeaseCar'=>array(
            'did'=>'51f791ee00006400056cb5f4',
            'uid'=>12923
        ),
        'Peter Vardy Leasing' => array (
            'did'=> '4dd636a700006400050fbb02',
            'uid'=>13295
        ),
        'Car Lease Special Offers'=> array (
                'did'=>'5891ab0d0000ff00059bdefb',
                'uid'=>15731,
        ),
        'e-car lease'=> array (
            'did'=>'533582d800006400057878bb',
            'uid'=>22647,
        ),
        'MWVC' => array (
            'did' => '5c7e74ce9ff6dc000192ce10',
            'uid' => 24730
        ),
        'Amber Car Leasing' => array(
            'did' => '51be52c700006400053d7aa2',
            'uid' => 25482
        )

    );


    public function handle()
    {
        foreach ($this->dealers as $k=>$v) {
            $this->info('Updating ' . $k);
            $businessUnitId = $v['did'];
            $trustpilot = file_get_contents('https://widget.trustpilot.com/trustbox-data/5419b6a8b0d04a076446a9ad?businessUnitId='.$businessUnitId.'&locale=en-GB&theme=light');

            $user = User::find($v['uid']);
            $user->trustpilot = $trustpilot;
            $user->save();

        }
    }
}
