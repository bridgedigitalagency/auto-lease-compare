<?php

namespace App\Console\Commands;
ini_set('memory_limit','2048M');

use Illuminate\Console\Command;
use App\Models\Pricing;
use App\Models\Cap;
use Illuminate\Support\Facades\DB;

class PriceHistory extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:pricehistory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update price history table and clean up prices table';

    public function __construct()
    {
        parent::__construct();
        $this->pricing = new Pricing();
        $this->cap = new Cap();
    }

    public function handle()
    {

        // clean up first
        $this->cleanup();

        // Generate price history
        $this->generateDailyPriceHistory();

//        $this->generateWeeklyPriceHistory();
    }

    /**
     * Clean up old history
     */
    private function cleanup()
    {
        $databases = array('CARS', 'LCV');

        foreach($databases as $database) {

            if($database == 'CARS') {
                $selectTableName = 'priceshistory_cars';
            }else{
                $selectTableName = 'priceshistory_vans';
            }
            $this->info("Deleting deals from " . $database . " that are older than 3 months");
            DB::raw("DELETE FROM " . $selectTableName . " WHERE updated_at < DATE_SUB(NOW(), INTERVAL 3 MONTH)");

        }

    }

    private function generateDailyPriceHistory()
    {

        $databases = array('CARS', 'LCV');

        $financeTypes = array(
            'P'=>'Personal',
            'B'=>'Business'
        );
        foreach($databases as $database) {
            if($database == 'CARS') {
                $tablename = 'priceshistory_cars';
                $pricestableName = 'pricestable';
            }else{
                $tablename = 'priceshistory_vans';
                $pricestableName = 'van_pricestable';
            }
            foreach ($financeTypes as $finance_type => $name) {
                $this->info('Generating price history for ' . $database . ' ' . $name);
                $sql = "INSERT INTO " . $tablename . " (cap_id,annual_mileage,term,deposit_value,monthly_payment,updated_at, finance_type, `database`)
                    SELECT capreference.cap_id, annual_mileage, term, deposit_value, MIN(monthly_payment), CURRENT_TIMESTAMP, finance_type, `database` FROM $pricestableName
                    JOIN capreference ON $pricestableName.cap_id = capreference.cap_id WHERE finance_type = '" . $finance_type . "'
                    AND capreference.database = '" . $database . "'
                    GROUP BY $pricestableName.cap_id, annual_mileage, term, deposit_value;
                    ";

                DB::insert($sql);
            }
        }
    }
}
