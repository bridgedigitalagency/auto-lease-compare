<?php

namespace App\Console\Commands;
ini_set('memory_limit','2048M');

use Illuminate\Console\Command;
use App\Models\Pricing;
use App\Models\Cap;
use Illuminate\Support\Facades\DB;

class UpdateExcludedDeals extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateexcludeddeals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update price history table and clean up prices table';

    public function __construct()
    {
        parent::__construct();
        $this->pricing = new Pricing();
        $this->cap = new Cap();

        $this->excludedManufacturers = explode(',', env('EXCLUDED_MANUFACTURERS'));
        $this->excludedDealers = explode(',', env('EXCLUDED_DEALERS'));

    }

    public function handle()
    {

        // clean up first
        $this->update();

    }

    private function update()
    {
        $capWhere = '';
        foreach ($this->excludedManufacturers as $excludedMan) {
            $capWhere .= "'" . $excludedMan . "',";
        }
        $capWhere = substr($capWhere, 0, strlen($capWhere) - 1);

        $this->info('Clearing deals that are included in exclude manufactuers');
        $clearDealsExcludedManufactuers = DB::raw("DELETE FROM pricestable WHERE cap_id IN (SELECT cap_id FROM capreference WHERE maker IN (" . $capWhere . "))");

        // Remove dealers that are disabled
        $sql = '';
        foreach ($this->excludedDealers as $excludedDealer) {
            $sql .= '"' . $excludedDealer . '"' . ',';
        }
        $excludedDealerSql = substr($sql, 0, strlen($sql) - 1);

        $this->info("Removing deals from dealers that have been paused");
//        $clearDealers = DB::raw("DELETE FROM pricestable WHERE deal_id IN (" . $excludedDealerSql . ")");}
    }
}
