<?php

namespace App\Console\Commands;
ini_set('memory_limit','2048M');

use Illuminate\Console\Command;
use App\Models\PriceHistory;
use App\Models\VanPricing;
use App\Models\Cap;
use Illuminate\Support\Facades\DB;

class UpdateVanPriceHistory extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updatevanpricehistory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update price history table and clean up prices table';

    public function __construct()
    {
        parent::__construct();
        $this->pricing = new VanPricing();
        $this->cap = new Cap();
    }

    public function handle()
    {

        // clean up first
        $this->cleanup();

        // Generate price history
        $this->generatePriceHistory();
    }

    /**
     * Clean up
     */
    private function cleanup()
    {
        $this->info('Updating average costs');
        $averageCost = DB::raw("UPDATE van_pricestable 
                    SET average_cost = (monthly_payment*(term-1)+deposit_months+document_fee)/(term)");
        $this->info('Updating total costs');
        $totalCost = DB::raw("UPDATE van_pricestable
                    SET total_cost = (monthly_payment*(term-1)+deposit_months+document_fee)");

        $this->info('Updating initial costs');
        $totalCost = DB::raw("UPDATE van_pricestable
                    SET total_initial_cost = (deposit_months+document_fee)");

        $this->info('Deleting deals with empty values');
        $deleteDepositValues = $this->pricing::whereNotIn('deposit_value', [1,3,6,9,12])->delete();
        $deleteTerms = $this->pricing::whereNotIn('term', [24,36,48])->delete();
        $deleteEmptyCost = $this->pricing::where('monthly_payment', '=', 0)->delete();
        $deleteAnnualMileage = $this->pricing::whereNotIn('annual_mileage', [
            5000,
            8000,
            10000,
            12000,
            15000,
            20000,
            25000,
            30000
        ])->delete();
        $deleteEmptyCaps = $this->pricing::Where('cap_id', '=', '')->delete();
        $this->info('Clearing prices table of deals that have cap ids that are invalid');
        $clearPricesTableInvalidCapIds = DB::raw("DELETE FROM van_pricestable
                        WHERE NOT EXISTS (
                            SELECT NULL FROM capreference WHERE van_pricestable.cap_id = capreference.cap_id)");
    }

    private function generatePriceHistory()
    {

        $financeTypes = array(
            'P'=>'Personal',
            'B'=>'Business'
        );
        foreach($financeTypes as $finance_type=>$name)
        {
            $this->info('Generating price history for ' . $name);
            $sql = "INSERT INTO priceshistory (cap_id,annual_mileage,term,deposit_value,monthly_payment,updated_at, finance_type)
            SELECT cap_id, annual_mileage, term, deposit_value, MIN(monthly_payment), CURRENT_TIMESTAMP, finance_type
            FROM van_pricestable
            WHERE finance_type='" . $finance_type . "'
            GROUP BY cap_id, annual_mileage, term, deposit_value";

            DB::insert($sql);

        }
    }
}
