<?php

namespace App\Console\Commands;
ini_set('memory_limit','2048M');

use Illuminate\Console\Command;
use App\Models\Pricing;
use App\Models\Cap;
use Illuminate\Support\Facades\DB;

class RemoveChampionDeals extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RemoveChampionDeals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scheduled task to remove deals for Champion Car Leasing';
    public function __construct()
    {
        parent::__construct();
        $this->dealerId = 'Champion Car Leasing';

    }

    public function handle()
    {
        DB::table('pricestable')
            ->where('deal_id', '=', $this->dealerId)
            ->delete();
    }
}
