<?php

namespace App\Console\Commands\Imports;
ini_set('memory_limit','2048M');

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class importRadius extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:radius';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scheduled task to add deals for Ask 4 Leasing';
    public function __construct()
    {
        parent::__construct();

        $this->dealerId = 'Radius Vehicle Solutions';
        $dvsDealerId = 7759;
        $fileNameDate = date('Y') . '-' . date('m') . '-' . date('d');
        $this->fileName = env('DEALER_DIRECTORY') . 'dvs/cars/autolease-' . $dvsDealerId . '-' . $fileNameDate . '.csv';

        $this->updated_at = date('Y-m-d');

        $this->pricestable = 'pricestable';

        $this->allowedMileages =    explode(',', env('AllowedMileages'));
        $this->allowedTerms =       explode(',', env('AllowedTerms'));
        $this->allowedDeposits =    explode(',', env('AllowedDepositValues'));

    }

    /**
     * @return void
     */
    public function handle()
    {

        if(file_exists($this->fileName)) {
            $this->processFeeds();
        }
        $this->cleanup();
    }

    /**
     * @return void
     */
    public function processFeeds()
    {
        $reader = fopen($this->fileName, 'r');

        if(empty($reader)) {
            $this->info('Empty File');
            die();
        }

        $this->counter = 0;
        // Loop around file contents
        while (($file = fgetcsv($reader, 500, ',')) !== FALSE):
            if($this->counter == 0) {$this->counter = 1; continue;}

            // If there are blank lines, skip them
            if(!isset($file[11])) {
                continue;
            }

            $depositValue = $file[11];
            $depositMonths = str_replace(',', '', $file[4]);
            $monthlyPayments = str_replace(',', '', $file[6]);
            $adminFee  = str_replace(',', '', $file[8]);
            $capId = $file[2];
            $term = $file[5];
            $financeType = $file[3];
            $annualMileage = $file[7];
            $inStock = $file[9];
            $file[10] = str_replace('/', '-', $file[10]);

            // Check deal is valid
            if(!in_array($depositValue, $this->allowedDeposits)) {  $this->info('skipped deposit' . $depositValue); continue;}
            if(!in_array($term, $this->allowedTerms)) {             $this->info('skipped term' . $term); continue;}
            if(!in_array($annualMileage, $this->allowedMileages)) { $this->info('skipped mileage ' . $annualMileage); continue;}
            if($monthlyPayments == 0 || $monthlyPayments == 0.00) { $this->info('skipped price'); continue;}

            $averageCost = ($monthlyPayments * ($term - 1 ) + $depositMonths + $adminFee) / ($term);
            $totalCost = ($monthlyPayments * ( $term - 1 ) + $depositMonths + $adminFee);
            $totalInitialCost = ($depositMonths + $adminFee);

            // Expiry date
            $exp = explode('-', $file[10]);
            $expDate = $exp[2] . '-' . $exp[1] . '-' . $exp[0];
            $expiryDate = date('Y-m-d', strtotime($expDate));

            $database = 'CARS';
            $uid = (string) Str::uuid();

            $dealCheck = DB::table($this->pricestable)
                ->where('deal_id', '=', $this->dealerId)
                ->where('cap_id', '=', $capId)
                ->where('term', '=', $term)
                ->where('finance_type', '=', $financeType)
                ->where('deposit_value', '=', $depositValue)
                ->where('annual_mileage', '=', $annualMileage)
                ->first();

            // Insert
            if (!$dealCheck) {

                DB::table($this->pricestable)->insert([
                    'cap_id' =>$capId,
                    'updated_at' => $this->updated_at,
                    'deal_id'=>$this->dealerId,
                    'finance_type' => $financeType,
                    'deposit_value'=>$depositValue,
                    'term'=>$term,
                    'deposit_months'=>$depositMonths,
                    'monthly_payment'=>$monthlyPayments,
                    'annual_mileage'=>$annualMileage,
                    'document_fee'=>$adminFee,
                    'in_stock'=>$inStock,
                    'expires_at'=>$expiryDate,
                    'average_cost'=>$averageCost,
                    'total_cost'=>$totalCost,
                    'total_initial_cost'=>$totalInitialCost,
                    'uid'=>$uid
                ]);

            }else{
                // update
                $this->info('updating cap id ' . $capId);
                DB::table($this->pricestable)->where('id', '=', $dealCheck->id)->update([
                    'updated_at' => $this->updated_at,
                    'finance_type' => $financeType,
                    'deposit_value'=>$depositValue,
                    'term'=>$term,
                    'deposit_months'=>$depositMonths,
                    'monthly_payment'=>$monthlyPayments,
                    'annual_mileage'=>$annualMileage,
                    'document_fee'=>$adminFee,
                    'in_stock'=>$inStock,
                    'expires_at'=>$expiryDate,
                    'average_cost'=>$averageCost,
                    'total_cost'=>$totalCost,
                    'total_initial_cost'=>$totalInitialCost,
                    'uid'=>$uid
                ]);
            }
            // end file loop
        endwhile;


    }

    private function cleanup() {
        // Remove out of date deals
        $this->info('Deleting deals that were not added today and not valid');
        DB::table($this->pricestable)->where('deal_id', '=', $this->dealerId)->where('updated_at', '!=', $this->updated_at)->delete();
    }
}
