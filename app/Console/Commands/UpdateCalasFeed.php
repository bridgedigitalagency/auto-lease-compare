<?php

namespace App\Console\Commands;
ini_set('memory_limit', '12G');

use Illuminate\Console\Command;
use App\Models\PriceHistory;
use App\Models\Pricing;
use App\Models\Cap;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UpdateCalasFeed extends Command
{

    protected $signature = 'updatecalasfeed';

    public function __construct()
    {
        parent::__construct();
        $this->dealerDirectory = '/home/autoleasecompare/dealers/calas/';
//        $this->dealerDirectory = '/Users/scottmokler/sites/dealers/calas/';

    }

    public function handle()
    {
        $start = time();
        $this->info('Removing old csv files');
        exec('rm ' . $this->dealerDirectory . '/cars/*.csv');

        // Start by unzipping everything
        $this->info('Unzipping files');
        $this->unzipEverything();

        // Process csv files
        $this->info('Processing files');
        $this->processCsvs();

        // Delete old deals
        $this->info('Cleaning up');
        $this->deleteOldDeals();

        $end = time();
        $this->info('Entire Calas script processed in ' . gmdate("H:i:s", ($end - $start)));

    }

    public function deleteDeals() {

        return false;
        $dealers = array(
            "FleetPrices",
            "Fleet UK",
            "J&R Leasing",
            "Milease",
            "Britannia Car Leasing",
            'Lunar Vehicle Leasing',
            'Loaded Car Leasing',
            "Sherwood Car and Van Leasing",
            "Maxi-Lease",
            'Lets Talk Finance',
            'Amber Car Leasing'
        );

        DB::table('pricestable')
            ->whereIn('deal_id', $dealers)
            ->delete();

        DB::table('van_pricestable')
            ->whereIn('deal_id', $dealers)
            ->delete();
    }

    private function unzipEverything()
    {

        $zipFiles = glob($this->dealerDirectory . '/cars/*.zip');
        $calasFiles = array();

        foreach($zipFiles as $file) {
            if(!strpos($file, '00590')
                && !strpos($file, '00879')
                && !strpos($file, '00700')
                && !strpos($file, '00345')
                && !strpos($file, '00332')
                && !strpos($file, '00569')
                && !strpos($file, '01049')
                // removed discover 02 sept 2021 - moved to dvs
                && !strpos($file, '00920')
                && !strpos($file, '01019') // Removed milease. Moved to fifty2one
            )
            {
                $calasFiles[] = $file;
            }
        }

        foreach ($calasFiles as $zippedFile) {
            $zip = new \ZipArchive();
            $res = $zip->open($zippedFile, \ZipArchive::CREATE);
            if ($res === true) {
                $zip->extractTo($this->dealerDirectory . '/cars/');
                $zip->close();
                $this->info('Extracted ' . $zippedFile);
            } else {
                //  Give me a reason for the failure
                $this->warning('Failed to unzip ' . $zippedFile);
            }
        }

        exec("rm " . $this->dealerDirectory . "cars/calas-vehicle-lookup.csv");

    }

    private function deleteOldDeals() {

        $this->info("Deleting old deals");
        $updatedDate = date('Y-m-d');
        $dealers = array(
            "FleetPrices",
            "Fleet UK",
            "J&R Leasing",
            "Sherwood Car and Van Leasing",
            "Maxi-Lease",
            "Britannia Car Leasing",
            'Lunar Vehicle Leasing',
            'Loaded Car Leasing',
            'Car Lease Kings',
            '21st Century Motors',
            'Lets Talk Finance',
            'Amber Car Leasing'
        );


        DB::table('pricestable')
            ->whereIn('deal_id', $dealers)
            ->whereRaw('updated_at != "' . $updatedDate . '"')
            ->delete();

        DB::table('van_pricestable')
            ->whereIn('deal_id', $dealers)
            ->whereRaw('updated_at != "' . $updatedDate . '"')
            ->delete();

        // Clean up files
        $this->info("Removing CSV files and storing a backup of zip files");
        exec('rm ' . $this->dealerDirectory . '/cars/*.csv');
        exec('mv ' . $this->dealerDirectory . '/today/*.zip ' . $this->dealerDirectory . '/processed');
        exec('mv ' . $this->dealerDirectory . '/cars/*.zip ' . $this->dealerDirectory . '/today');

    }

    /**
     *
     */
    private function fetchLookupData()
    {
        $lookup = DB::table('calas_lookup')
            ->select('CalasVehicleId', 'CapId', 'Manufacturer')
            ->get();

        $data = array();
        foreach($lookup as $calas) {
            $data[$calas->CalasVehicleId] = array(
                'cap_id'=>$calas->CapId,
                'maker'=>$calas->Manufacturer
            );

        }

        return $data;

    }

    /**
     * @param $type
     */
    private function processCsvs()
    {

        $date = date('Y-m-d H:i:s');
        $startTime = time();
        $this->info("Start Time " . $date);

        // Fetch cap Ids
        $lookupArray = $this->fetchLookupData();

        $csvFiles = array_reverse(glob($this->dealerDirectory . '/cars/*.csv'));


        foreach ($csvFiles as $filePath) {

            $this->info("Processing " . $filePath);

            $reader = fopen($filePath, 'r');

            if (empty($reader)) {
                $this->info('Empty File');
                die();
            }

            $this->counter = 0;
            // Loop around file contents
            while (($file = fgetcsv($reader, 500, ',')) !== false):

                $CLIENTID = $file[0];
                $PRICE_ID = $file[1];
                $TYPE = $file[2];
                $CALASID = $file[3];
                $METPAINT_INC = $file[4];
                $INSTOCK = $file[5];
                $EXPIRES = $file[6];
                $MONTHLY = $file[7];
                $DEPOSIT = $file[8];
                $EXCESS_PPM = $file[9];
                $MILEAGE = $file[10];
                $MAINT_INCL = $file[11];
                $PROFILE = $file[12];
                $MONTHS = $file[13];
                $BKRDOCFEE = $file[14];

                if ($TYPE === 'lcv') {
                    $dbTable = "van_pricestable";
                    $database = 'LCV';
                } else {
                    $dbTable = "pricestable";
                    $database = 'CARS';
                }

                switch ($CLIENTID) {
                    case 20:
                        $this->dealerId = "FleetPrices";
                        break;
                    case 36:
                        $this->dealerId = 'Amber Car Leasing';
                        break;
                    case 1040:
                        $this->dealerId = "Loaded Car Leasing";
                        break;
                    case 174:
                        $this->dealerId = "Fleet UK";
                        break;
                    case 1061:
                        $this->dealerId = "Lunar Vehicle Leasing";
                        break;
                    case 95:
                        $this->dealerId = "J&R Leasing";
                        break;
                    case 1018:
                        $this->dealerId = "Greenleaps";
                        break;
                    case 1080:
                        $this->dealerId = 'Car Lease Kings';
                        break;
                    case 351:
                        $this->dealerId = '21st Century Motors';
                        break;
                    case 788:
                        $this->dealerId = 'Lets Talk Finance';
                        break;
                    case 1115:
                        $this->dealerId = "Sherwood Car and Van Leasing";
                        break;
                    case 1092:
                        $this->dealerId = "Maxi-Lease";
                        break;
                    default:
                        $this->dealerId = 'fail';
                        break;

                }

                if($this->dealerId == 'fail') {
                    continue;
                }

                if (substr($PROFILE, 0, 1) === "I") {
                    $PROFILE = "";
                    continue;
                } else {
                    $depositValue = trim(substr($PROFILE, 0, 2));
                }

                $depositMonths = $DEPOSIT;
                $monthlyPayments = $MONTHLY;
                $adminFee = $BKRDOCFEE;

                $PRICE_ID = strtolower($PRICE_ID);
                if (substr($PRICE_ID, 0, 3) === "bch") {
                    $priceID = 'B';
                } elseif (substr($PRICE_ID, 0, 1) === "b") {
                    $priceID = 'B';
                } else {
                    $priceID = 'P';
                    $depositMonths = $DEPOSIT;
                    $monthlyPayments = $MONTHLY;
                    $adminFee = $BKRDOCFEE;
                }

                $expiryDate = date('Y-m-d', strtotime($EXPIRES));

                // As Calas don't use the cap id anymore we need to look up their id and
                // Match it to a cap id for each deal.
                $calasId = $CALASID;
                if (!array_key_exists($calasId, $lookupArray)) {
                    continue;
                }

                if(!isset($lookupArray[$calasId])) {
                    continue;
                }

                $capId = $lookupArray[$calasId]['cap_id'];
                $capCheckMaker = $lookupArray[$calasId]['maker'];
                // cap check
                $capCheck = DB::table('capreference')
                    ->where('cap_id', '=', $capId)
                    ->where('maker', '=', strtoupper($capCheckMaker))
                    ->count();

                if($capCheck == 0) {
                    // Log this somewhere
                    DB::table('calas_feed_fails')->insert(
                        [
                            'client_id'=>$CLIENTID,
                            'price_id'=>$PRICE_ID,
                            'type'=>$TYPE,
                            'cap_id'=>$capId,
                            'profile'=>$PROFILE
                        ]);
                    continue;
                }

                $term = $MONTHS;
                $averageCost = ($monthlyPayments*($term-1)+$depositMonths+$adminFee)/($term);
                $totalCost = ($monthlyPayments*($term-1)+$depositMonths+$adminFee);
                $totalInitialCost = ($depositMonths+$adminFee);

                $uid = (string) Str::uuid();

                $checkRowExisting = DB::table($dbTable)
                    ->where('cap_id', '=', $capId)
                    ->where('deal_id', '=', $this->dealerId)
                    ->where('term', '=', $MONTHS)
                    ->where('finance_type', '=', $priceID)
                    ->where('deposit_value', '=', $depositValue)
                    ->where('annual_mileage', '=', $MILEAGE)
                    ->first();

                // There is already a record for this deal in the DB so update it with new details
                if (!is_null($checkRowExisting)) {

                    DB::table($dbTable)
                        ->where('id', $checkRowExisting->id)
                        ->update([

                            'finance_type' => $priceID,
                            'deposit_value' => $depositValue,
                            'term' => $MONTHS,
                            'monthly_payment' => $monthlyPayments,
                            'annual_mileage' => $MILEAGE,
                            'document_fee' => $adminFee,
                            'in_stock' => $INSTOCK,
                            'expires_at' => $expiryDate,
                            'updated_at' => $date,
                            'deposit_months' => $depositMonths,
                            'uid' => $uid
                        ]);

                } else {
                    // Nothing to update so it is a new deal, lets get it inserted
                    DB::table($dbTable)->insert(
                        [
                            'deal_id' => $this->dealerId,
                            'cap_id' => $capId,
                            'finance_type' => $priceID,
                            'deposit_value' => $depositValue,
                            'term'=>$term,
                            'monthly_payment' => $monthlyPayments,
                            'annual_mileage' => $MILEAGE,
                            'document_fee' => $adminFee,
                            'in_stock' => $INSTOCK,
                            'expires_at' => $expiryDate,
                            'updated_at' => $date,
                            'deposit_months' => $depositMonths,
                            'average_cost' => $averageCost,
                            'total_cost' => $totalCost,
                            'total_initial_cost' => $totalInitialCost,
                            'uid' => $uid

                        ]
                    );

                }// end update / insert if statement
            endwhile;
        }

        $endTime = time();
        $this->info('Script processed in ' . gmdate("H:i:s", ($endTime - $startTime)));
    }

}
