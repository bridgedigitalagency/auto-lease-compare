<?php

namespace App\Console\Commands;
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);


use App\Models\Cap;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use SoapClient;
use SimpleXMLElement;

class UpdateCapEquipment extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateCapEquipment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update cap data for cars';

    public $nvdWsdl = "https://soap.cap.co.uk/Nvd/CapNvd.asmx?wsdl";
    public $database = array('CARS', 'LCV');

    public $feedDir = "";

    public function __construct()
    {
        $this->subscriberId = env('CAP_SUB_ID');
        $this->password = env('CAP_SUB_PASSWORD');
        parent::__construct();
    }

    public function handle()
    {
        $update = 1;

        foreach($this->database as $database) {

            $this->getStandardEquipment($database, $update);
//            $this->getOptionalEquipment($database, $update);

        }
    }


    private function getStandardEquipment($vehicleDatabase, $update = 1) {
        $this->info("Updating Standard Equipment - " . $vehicleDatabase);

        $capreferenceTable = 'car_capreference';
        if($vehicleDatabase == 'LCV') {
            $capreferenceTable = 'van_capreference';
        }

        if($update == 1) {
            $capreferenceTable = 'capreference';
            $cap = DB::table($capreferenceTable)
                ->get();
        }else{
            $cap = DB::table($capreferenceTable)
                ->get();
        }

        $this->output->progressStart(count($cap));

        foreach($cap as $capReference) {

            $capIdEquipment = DB::table('cap_id_equipment')
                ->where('database', '=', $vehicleDatabase)
                ->count();

            if($update == 0 && $capIdEquipment > 0) {
                $this->output->progressAdvance();
                continue;
            }

            $url = "https://soap.cap.co.uk/nvd/capnvd.asmx/GetStandardEquipment";
            $args = array(
                'subscriberId' => $this->subscriberId,
                'password' => $this->password,
                'database' => $vehicleDatabase,
                'capid' => $capReference->cap_id,
                'seDate' => date('Y-m-d'),
                'justCurrent' => true,
            );

            $client = $this->executeNVDQuery($url, $args);

            try {
                $result = $client->GetStandardEquipment($args);
                $result = $result->GetStandardEquipmentResult->Returned_DataSet->any;

            } catch (SoapFault $e) {
                echo "Error: {$e}";
                return false;
            }
            $xml = simplexml_load_string($result);
            if (count($xml) < 1 || is_null($xml)) {
            } else {

                $equip = [];

                foreach ($xml->StandardEquipement->SE as $item) {

                    // Check if item is in equipment table
                    $capEquipment = DB::table('cap_equipment')
                        ->where('OptionCode', '=', $item->Se_OptionCode)
                        ->count();

                    if($capEquipment == 0) {
                            DB::table('cap_equipment')->insert(
                                array(
                                    'OptionCode' => $item->Se_OptionCode,
                                    'Do_Description' => $item->Do_Description,
                                    'Dc_Description' => $item->Dc_Description,
                                    'price'=> $item->Opt_Basic
                                )
                            );
                    }else{
                        DB::table('cap_equipment')
                            ->where('OptionCode', '=', $item->Se_OptionCode)
                            ->update(
                                array(
                                    'Do_Description' => $item->Do_Description,
                                    'Dc_Description' => $item->Dc_Description,
                                    'price'=> $item->Opt_Basic
                                )
                            );
                    }

                    $equip[] = (string)$item->Se_OptionCode[0];
                }

                // add equipment to cap id
                $capIdEquipment = DB::table('cap_id_equipment')
                    ->where('cap_id', '=', $capReference->cap_id)
                    ->where('database', '=', $vehicleDatabase)
                    ->count();

                if($capIdEquipment == 0) {
                    DB::table('cap_id_equipment')->insert(
                        array(
                            'ce_ids' => json_encode($equip),
                            'cap_id' => $capReference->cap_id,
                            'database'=>$vehicleDatabase
                        )
                    );
                }else{
                    DB::table('cap_id_equipment')->where('cap_id', '=', $capReference->cap_id)->update(
                        [
                            'ce_ids' => json_encode($equip),
                            'cap_id' => $capReference->cap_id,
                            'database'=>$vehicleDatabase,
                        ]
                    );
                }

                $equip = [];

            }
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();
    }

    private function getOptionalEquipment($vehicleDatabase, $update = 0) {
        $this->info("Updating Optional Extras - " . $vehicleDatabase);

        $captable = 'car_capreference';
        if($vehicleDatabase == 'LCV') {
            $captable = 'van_capreference';
        }
        if($update == 1) {
            $captable = 'capreference';
        }

        $cap = DB::table($captable)
            ->get();

        $this->output->progressStart(count($cap));

        foreach($cap as $capReference) {

            $capIdEquipment = DB::table('cap_id_optional_equipment')
                ->count();

            if($capIdEquipment > 0 && $update != 1) {
                $this->output->progressAdvance();
                continue;
            }

            $url = "https://soap.cap.co.uk/nvd/capnvd.asmx/GetCapOptionsBundle";
            $args = array(
                'subscriberId' => $this->subscriberId,
                'password' => $this->password,
                'database' => $vehicleDatabase,
                'capid' => $capReference->cap_id,
                'optionDate' => date('Y-m-d'),
                'justCurrent' => true,
                'descriptionRs' => true,
                'optionsRs' => true,
                'relationshipsRs' => true,
                'packRs' => true,
                'technicalRs' => true,
            );

            $client = $this->executeNVDQuery($url, $args);

            try {
                $result = $client->GetCapOptionsBundle($args);
                $result = $result->GetCapOptionsBundleResult->Returned_DataSet->any;

            } catch (SoapFault $e) {
                echo "Error: {$e}";
                return false;
            }
            $xml = simplexml_load_string($result);

            if (count($xml) < 1 || is_null($xml)) {
            } else {

                $equip = [];

                foreach ($xml->NVDBundle->Options as $item) {
                    // Check if item is in equipment table
                    $capEquipment = DB::table('cap_optional_equipment')
                        ->where('OptionCode', '=', $item->Opt_OptionCode)
                        ->count();

                    if($capEquipment == 0) {
                        DB::table('cap_optional_equipment')->insert(
                            array(
                                'OptionCode' => $item->Opt_OptionCode,
                                'Do_Description' => $item->Do_Description,
                                'Dc_Description' => $item->Dc_Description,
                                'price'=> $item->Opt_Basic
                            )
                        );
                    }else{
                        if($update == 1) {
                            $up = DB::table('cap_optional_equipment')
                                ->where('OptionCode', '=', $item->Opt_OptionCode)
                                ->update([
                                    'OptionCode' => $item->Opt_OptionCode,
                                    'Do_Description' => $item->Do_Description,
                                    'Dc_Description' => $item->Dc_Description,
                                    'price'=> $item->Opt_Basic
                                    ]);

                        }
                    }
                    $equip[] = (string)$item->Opt_OptionCode;
                }

                // add equipment to cap id
                $capIdEquipment = DB::table('cap_id_optional_equipment')
                    ->where('cap_id', '=', $capReference->cap_id)
                    ->count();

                if($capIdEquipment == 0) {
                    DB::table('cap_id_optional_equipment')->insert(
                        array(
                            'ce_ids' => json_encode($equip),
                            'cap_id' => $capReference->cap_id,
                            'database'=>$vehicleDatabase
                        )
                    );
                }else{
                    DB::table('cap_id_optional_equipment')->where('cap_id', '=', $capReference->cap_id)->update(
                        [
                            'ce_ids' => json_encode($equip),
                            'cap_id' => $capReference->cap_id,
                            'database'=>$vehicleDatabase,
                        ]
                    );
                }

                $equip = [];

            }
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();
    }


    /**
     * @param $url
     * @return SoapClient
     * @throws \SoapFault
     */
    private function executeNVDQuery($url)
    {

        $wsdl = $this->nvdWsdl;
        $client = new SoapClient($wsdl, array('soap_version' => SOAP_1_1, 'trace' => true, 'keep_alive' => false));

        return $client;

    }
}