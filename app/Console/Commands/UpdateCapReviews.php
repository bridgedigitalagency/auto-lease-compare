<?php

namespace App\Console\Commands;

use App\Models\CapReview;
use App\Models\Profile;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateCapReviews extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updatecapreviews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch and update database of reviews fetched from Car and Driving';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->excludedManufacturers = explode(',', env('EXCLUDED_MANUFACTURERS'));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $caps = DB::table('capreference')
            ->select('cap_id', 'database')
	       ->whereRaw('cap_id NOT IN (SELECT cap_id FROM cap_reviews)')
            ->limit('2000') 
            ->offset('0')
            ->get();

        $updated = $inserted = 0;

        $this->output->progressStart(count($caps));

        foreach($caps as $cap) {

            // Beacuse CAP reuse IDs for cars and vans, C&D add 500,000 to the CAP ID for vans
            if($cap->database == 'CARS') {
                $capId = $cap->cap_id;
            }else{
                $capId = ($cap->cap_id + 500000);
            }

            $urlArray = array(
                "https://www.caranddriving.com/ws/A041/ws.asmx/GetReviewsFromCapID?user_id=autoleasecompare&capid=$capId&type=&getsummaries=true&liveonly=true",
                "https://www.caranddriving.com/ws/A041/ws.asmx/GetReviewsFromCapID?user_id=autoleasecompare&capid=$capId&type=&getsummaries=true&liveonly=false",
                "https://www.caranddriving.com/ws/A041/ws.asmx/GetReviewsFromCapID?user_id=autoleasecompare&capid=$capId&type=new&getsummaries=true&liveonly=true",
                "https://www.caranddriving.com/ws/A041/ws.asmx/GetReviewsFromCapID?user_id=autoleasecompare&capid=$capId&type=new&getsummaries=true&liveonly=false",
            );
            foreach ($urlArray as $candd) {


                $ch = curl_init();
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_URL, $candd);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                curl_setopt($ch, CURLOPT_TIMEOUT, 2);
                curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
                $responseCandD = curl_exec($ch);
                curl_close($ch);

                $xml = (array)simplexml_load_string($responseCandD);

                $introduction = '';
                $history = '';
                $what_you_get = '';
                $what_you_pay = '';
                $what_to_look_for = '';
                $replacement_parts = '';
                $on_the_road = '';
                $overall = '';
                $short_summary = '';
                $mid_summary = '';
                $long_summary = '';
                $tenSecondReview = '';
                $background = '';
                $design_and_build = '';
                $driving_experience = '';
                if (!isset($xml['code']) || $xml['code'] != 0) {
                    continue;
                }

                // get the reviews
                foreach ($xml['reviewlist'] as $review) {
                    foreach ($review->paragraphlist as $paragraphList) {

                        foreach ($paragraphList->paragraph as $paragraph) {

                            if ($paragraph->type == 'Introduction') {
                                $introduction = $paragraph->text;
                            }

                            if ($paragraph->type == 'Ten Second Review') {
                                $tenSecondReview = $paragraph->text;
                            }

                            if ($paragraph->type == 'Background') {
                                $background = $paragraph->text;
                            }

                            if ($paragraph->type == 'History') {
                                $history = $paragraph->text;
                            }
                            if ($paragraph->type == 'What You Get') {
                                $what_you_get = $paragraph->text;
                            }
                            if ($paragraph->type == 'What You Pay') {
                                $what_you_pay = $paragraph->text;
                            }
                            if ($paragraph->type == 'What To Look For') {
                                $what_to_look_for = $paragraph->text;
                            }
                            if ($paragraph->type == 'Replacement Parts') {
                                $replacement_parts = $paragraph->text;
                            }
                            if ($paragraph->type == 'On The Road') {
                                $on_the_road = $paragraph->text;
                            }
                            if ($paragraph->type == 'Overall') {
                                $overall = $paragraph->text;
                            }
                            if ($paragraph->type == '150 Word Summary') {
                                $short_summary = $paragraph->text;
                            }
                            if ($paragraph->type == '250 Word Summary') {
                                $mid_summary = $paragraph->text;
                            }
                            if ($paragraph->type == '500 Word Summary') {
                                $long_summary = $paragraph->text;
                            }
                            if ($paragraph->type == 'Design and Build') {
                                $design_and_build = $paragraph->text;
                            }
                            if ($paragraph->type == 'Driving Experience') {
                                $driving_experience = $paragraph->text;
                            }
                        }
                    }
                }

                // query to check database for existing info on this cap
                $checkExisting = DB::table('cap_reviews')
                    ->select('*')
                    ->where('cap_id', '=', $cap->cap_id)
                    ->first();

                //check if we have existing data in the database, if so update, if not insert
                if (isset($checkExisting) && !is_null($checkExisting)) {
                    //update

                    if ($checkExisting->introduction != '') {
                        $introduction = $checkExisting->introduction;
                    }

                    if ($checkExisting->introduction != '') {
                        $history = $checkExisting->history;
                    }
                    if ($checkExisting->ten_second_review != '') {
                        $tenSecondReview = $checkExisting->ten_second_review;
                    }
                    if ($checkExisting->background != '') {
                        $background = $checkExisting->background;
                    }
                    if ($checkExisting->what_you_get != '') {
                        $what_you_get = $checkExisting->what_you_get;
                    }
                    if ($checkExisting->what_you_pay != '') {
                        $what_you_pay = $checkExisting->what_you_pay;
                    }
                    if ($checkExisting->what_to_look_for != '') {
                        $what_to_look_for = $checkExisting->what_to_look_for;
                    }
                    if ($checkExisting->replacement_parts != '') {
                        $replacement_parts = $checkExisting->replacement_parts;
                    }
                    if ($checkExisting->on_the_road != '') {
                        $on_the_road = $checkExisting->on_the_road;
                    }
                    if ($checkExisting->overall != '') {
                        $overall = $checkExisting->overall;
                    }
                    if ($checkExisting->short_summary != '') {
                        $short_summary = $checkExisting->short_summary;
                    }
                    if ($checkExisting->mid_summary != '') {
                        $mid_summary = $checkExisting->mid_summary;
                    }
                    if ($checkExisting->long_summary != '') {
                        $long_summary = $checkExisting->long_summary;
                    }
                    if ($checkExisting->driving_experience != '') {
                        $driving_experience = $checkExisting->driving_experience;
                    }
                    if ($checkExisting->design_and_build != '') {
                        $design_and_build = $checkExisting->design_and_build;
                    }

                    $update = DB::table('cap_reviews')
                        ->where('cap_id', '=', $cap->cap_id)
                        ->update([
                            'introduction' => $introduction,
                            'history' => $history,
                            'ten_second_review' => $tenSecondReview,
                            'background' => $background,
                            'what_you_get' => $what_you_get,
                            'what_you_pay' => $what_you_pay,
                            'what_to_look_for' => $what_to_look_for,
                            'replacement_parts' => $replacement_parts,
                            'on_the_road' => $on_the_road,
                            'overall' => $overall,
                            'short_summary' => $short_summary,
                            'mid_summary' => $mid_summary,
                            'long_summary' => $long_summary,
                            'design_and_build' => $design_and_build,
                            'driving_experience' => $driving_experience,
                            'database' => $cap->database,
                            'updated_at' => now()
                        ]);
                    $updated++;
                } else {
                    // insert
                    $capReview = new CapReview();
                    $capReview->cap_id = $cap->cap_id;
                    $capReview->introduction = $introduction;
                    $capReview->ten_second_review = $tenSecondReview;
                    $capReview->background = $background;
                    $capReview->history = $history;
                    $capReview->what_you_get = $what_you_get;
                    $capReview->what_you_pay = $what_you_pay;
                    $capReview->what_to_look_for = $what_to_look_for;
                    $capReview->replacement_parts = $replacement_parts;
                    $capReview->on_the_road = $on_the_road;
                    $capReview->overall = $overall;
                    $capReview->short_summary = $short_summary;
                    $capReview->mid_summary = $mid_summary;
                    $capReview->long_summary = $long_summary;
                    $capReview->database = $cap->database;
                    $capReview->updated_at = now();
                    $capReview->save();
                    $inserted++;
                }
            }
            $this->output->progressAdvance();
        }

        $this->output->progressFinish();
        echo "Inserted: " . $inserted . " & Updated: " . $updated;
    }
}
